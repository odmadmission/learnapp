﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiChamps.Models
{
    public class NotificationMessage
    {
        public String title { set; get; }
        public String text { set; get; }
       
    }
}