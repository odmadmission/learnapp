﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigiChamps.Models
{
    public class NotificationType
    {
      public static int REG_REFERRAL = 1;
      public static int TEST_RESULT = 2;
      public static int ORDER_CONFIRM = 3;
      public static int DOUBT_ANSWER = 4;
      public static int NEW_FEED = 5;
      public static int MENTOR_ASSIGN = 6;
      public static int NEW_MENTOR_TASK = 7;
      public static int UPDATE_MENTOR_TASK = 8;
      public static int OVERDUE_MENTOR_TASK = 9;


    }
}
