﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiChamps.Models
{
    public class SchoolmodelCls
    {
    }
    public class AssignTeacherCls
    {
        public int AssignId { get; set; }
        public Decimal TeacherId { get; set; }
        public string TeacherName { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public int BoardId { get; set; }
        public string BoardName { get; set; }
        public Guid? SchoolId { get; set; }
        public Guid? SectionId { get; set; }
        public string SectionName { get; set; }
        public string SchoolName { get; set; }

        

    }
    public class ExamPDFCls
    {
        public int PdfExam_Id { get; set; }
        public System.DateTime StartTime { get; set; }
        public System.DateTime EndTime { get; set; }
        public string FilePath { get; set; }
        public string Name { get; set; }
        public int Total_Mark { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public int BoardId { get; set; }
        public string BoardName { get; set; }
        public Guid? SchoolId { get; set; }
        public Guid? SectionId { get; set; }
        public string SectionName { get; set; }

    }
}