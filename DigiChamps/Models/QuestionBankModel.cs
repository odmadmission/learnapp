﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiChamps.Models
{
    public class QuestionBankModel
    {
       
            public int QuestionBank_Id { get; set; }
            public int Regd_ID { get; set; }
            public string Student_Name { get; set; }
            public string Student_Mobile { get; set; }
            public int Board_Id { get; set; }
            public string Board_Name { get; set; }
            public int Class_Id { get; set; }
        public Guid School_Id { get; set; }
        public string Class_Name { get; set; }
            public Nullable<System.Guid> SectionId { get; set; }
            public string Section_Name { get; set; }
            public int Subject_Id { get; set; }
            public string Subject_Name { get; set; }
            public int Chapter_Id { get; set; }
            public string Chapter_Name { get; set; }
            public bool Verified { get; set; }
            public int Verified_By { get; set; }
            public string Verified_By_Name { get; set; }
            public int Total_Mark { get; set; }
            public Nullable<System.DateTime> Inserted_Date { get; set; }

            public Nullable<System.DateTime> Modified_Date { get; set; }
            public string Upload_Date { get; set; }
            public string Verify_Date { get; set; }
            public int Module_Id { get; set; }
            public string Module_Name { get; set; }
            public string File_Type { get; set; }
            public int TotalVerified { get; set; }
            public int TotalPending { get; set; }
            public int TotalSubmitted { get; set; }

            public string Status { get; set; }
            public List<tbl_DC_QuestionBank_Attachment> attachments { get; set; }
        
    }
}