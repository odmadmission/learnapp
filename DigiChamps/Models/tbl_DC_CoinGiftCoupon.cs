//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigiChamps.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_DC_CoinGiftCoupon
    {
        public int GiftID { get; set; }
        public string Title { get; set; }
        public Nullable<int> Coins { get; set; }
        public Nullable<System.DateTime> InsertedOn { get; set; }
        public Nullable<bool> Expired { get; set; }
    }
}
