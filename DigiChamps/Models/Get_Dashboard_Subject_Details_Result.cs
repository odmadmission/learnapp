//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigiChamps.Models
{
    using System;
    
    public partial class Get_Dashboard_Subject_Details_Result
    {
        public Nullable<int> total_pdfs { get; set; }
        public Nullable<int> subjectid { get; set; }
        public Nullable<int> total_chapters { get; set; }
        public Nullable<int> total_videos { get; set; }
        public Nullable<int> Total_Pre_req_test { get; set; }
        public Nullable<int> Total_question_pdf { get; set; }
        public Nullable<int> Total_question { get; set; }
        public string subject { get; set; }
    }
}
