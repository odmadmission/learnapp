﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace DigiChamps.Models
{
    public class Backgroundtaskcls
    {
        private Timer _timer;
        private Timer _weeklytimer;
        private Timer _monthlytimer;
        public void StartAsync()
        {
            DateTime now = DateTime.Now;

            DateTime Daily = DateTime.Now.AddMinutes(12.0);

            if (now > Daily)
            {
                Daily = Daily.AddDays(1.0);
            }




            var logs = new List<string>();
            logs.Add(Daily.ToString());
            var timeToGo = Daily - now;




            logs.Add(timeToGo.ToString());


            _timer = new Timer(DoWorkAsync, null, timeToGo, TimeSpan.FromDays(1));


        }
        private async void DoWorkAsync(object state)
        {
            //var mylog = Path.Combine(Path.GetTempPath(), "mylog.txt");
            //var logs = new List<string>();
            //logs.Add("Resync started at" + DateTime.UtcNow.ToString("s"));
            //_taskServices.sendmailtaskdeadlines();
            //File.AppendAllLines(mylog, logs);
            //my task here
            try
            {
                DateTime dDate;
                DigiChampsEntities DbContext = new DigiChampsEntities();
                var zoomvideos = DbContext.ZoomVideos.ToList();
                var res = zoomvideos.Where(a => GetDateTime(a.InsertedOn) == true && a.IsActive == true).ToList();
                foreach (var a in res)
                {
                    a.IsActive = false;
                    a.ModifiedOn = DateTime.Now;
                    DbContext.SaveChanges();
                }
            }
            catch(Exception e)
            {

            }
        }
        public bool GetDateTime(DateTime? date)
        {
            DateTime insertdate = Convert.ToDateTime(date).Date.AddDays(7);
            DateTime currentdate = DateTime.Now.Date;
            if(insertdate < currentdate)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}