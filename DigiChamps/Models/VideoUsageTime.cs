//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigiChamps.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class VideoUsageTime
    {
        public int UdageId { get; set; }
        public Nullable<int> VideoId { get; set; }
        public string SchoolId { get; set; }
        public Nullable<System.DateTime> UsageDate { get; set; }
        public Nullable<int> PlayTime { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<System.DateTime> InsertedOn { get; set; }
        public string SectionId { get; set; }
        public Nullable<int> ClassId { get; set; }
        public Nullable<int> RegdId { get; set; }
    }
}
