﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiChamps.Models
{
    public class ObjectiveExamAPIModel
    {
       
        public Nullable<long> onlineObjectiveExamId { get; set; }
        public string onlineExamDateName { get; set; }
        public Nullable<DateTime> onlineExamDate { get; set; }
        public string onlineExamTypeName { get; set; }      
        public Nullable<TimeSpan> startTime { get; set; }
        public string startTimeName { get; set; }
        public Nullable<TimeSpan> endTime { get; set; }
        public string endTimeName { get; set; }
        public string subjectName { get; set; }
        public List<objectiveexamQuescls> onlineObjectivequesList { get; set; }

    }
    public class objectiveexamQuescls
    {
        public long onlineObjectiveExamQuestionId { get; set; }
        public Nullable<long> onlineObjectiveExamId { get; set; }
        public string question { get; set; }
        public string questionImage { get; set; }
        public Nullable<decimal> mark { get; set; }
        public List<OnlineObjectiveExamAnswerModel> answers { get; set; }
    }
    public class OnlineObjectiveExamAnswerModel
    {
        public long onlineObjectiveExamAnswerId { get; set; }
        public string option { get; set; }
        public string optionImage { get; set; }
        public bool IsAnswer { get; set; }
    }
    public partial class OnlineObjectiveExamModel
    {
        public long onlineObjectiveExamId { get; set; }
        public string onlineObjectiveExamCode { get; set; }
        //public Nullable<long> OnlineExamTypeId { get; set; }
        //public Nullable<long> OnlineExamDateId { get; set; }
        //public Nullable<long> OnlineExamTimeId { get; set; }
        public Nullable<System.Guid> schoolId { get; set; }
        public Nullable<int> boardId { get; set; }
        public Nullable<int> classId { get; set; }
        public Nullable<int> subjectId { get; set; }
        public string exam_Instruction { get; set; }
   
    }
    public class OnlineObjectiveExamAPIModel
    {
        public long ID { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public long ExampTypeId { get; set; }
        public string ExampTypeName { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public Nullable<System.Guid> SectionId { get; set; }
        public string SectionName { get; set; }
        public long OnlineExamDateId { get; set; }
        public Nullable<DateTime> OnlineExamDates { get; set; }
        public long ExamTimeTableId { get; set; }
        public Nullable<TimeSpan> StartTime { get; set; }
        public string StartTimeName { get; set; }
        public Nullable<TimeSpan> EndTime { get; set; }
        public string EndTimeName { get; set; }
        public string Score { get; set; }
        public string Instruction { get; set; }
    }

    public class OnlineExamObjectiveResponseModel
    {
        public List<OnlineObjectiveExamAPIModel> UpcommingList { get; set; }
        public List<OnlineObjectiveExamAPIModel> GivenExamList { get; set; }
        public List<OnlineObjectiveExamAPIModel> Result { get; set; }
    }

    public class OnlineObjectiveExamQuestionAndAnswar
    {
        public long ID { get; set; }
        public string Subject { get; set; }
        public Nullable<DateTime> DateOfExam { get; set; }
        public decimal Score { get; set; }
        public int TotalCorrectAnswar { get; set; }
        public int TotalIncorrectAnswar { get; set; }
        public List<QuestionAndAnswarModel> QuestionAndAnswar { get; set; } 
        public string Answar { get; set; }
    }

    public class QuestionAndAnswarModel
    { 
        public string Question { get; set; }
        public string QuestionImage { get; set; }
        public string GivenAnswar { get; set; }
        public string GivenAnswarImage { get; set; }
        public string Answar { get; set; }
        public string AnswarImage { get; set; }
    } 
    
    public class BaseExamResultModel
    {
        public int? StudentId { get; set; }
        public long OnlineObjectiveExamId { get; set; } 
        public List<Questions> Question { get; set; }
    }
    public class Questions
    {
        public int QuestionId { get; set; }
        public int Answer { get; set; }
    }
    //public class Answer
    //{
    //    public int AnswerId { get; set; }
    //}

    public class totalworksheetscoreclass
    {
        public int Regd_ID { get; set; }
        public string Customer_Name { get; set; }
        public Guid SectionId { get; set; }
        public Guid SchoolId { get; set; }
        public int Board_ID { get; set; }
        public int Class_ID { get; set; }
        public string Board_Name { get; set; }
        public string SchoolName { get; set; }
        public string Class_Name { get; set; }
        public string SectionName { get; set; }
        public int Subject_Id { get; set; }
        public string Subject { get; set; }
        public decimal? subjectivescore { get; set; }
        public decimal? objectivescore { get; set; }
        public double worksheetscore { get; set; }
    }
}