//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigiChamps.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_DC_DIY_Video
    {
        public int DIYVideo_ID { get; set; }
        public string DIYVideo_Name { get; set; }
        public string DIYVideo_Upload { get; set; }
        public string DIYImages { get; set; }
        public string DIYVideo_Description { get; set; }
        public Nullable<System.DateTime> Inserted_On { get; set; }
        public string Inserted_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public string Modified_By { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<bool> Is_Delete { get; set; }
        public Nullable<int> Module_ID { get; set; }
        public Nullable<int> Regd_ID { get; set; }
    
        public virtual tbl_DC_Module tbl_DC_Module { get; set; }
    }
}
