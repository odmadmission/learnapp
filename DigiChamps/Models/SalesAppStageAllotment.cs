//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigiChamps.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SalesAppStageAllotment
    {
        public int AllotmentId { get; set; }
        public Nullable<int> StudentId { get; set; }
        public Nullable<int> StageId { get; set; }
        public Nullable<int> AccessId { get; set; }
        public Nullable<int> AssignedBy { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> PreviousAllotmentId { get; set; }
        public Nullable<int> Active { get; set; }
        public Nullable<System.DateTime> InsertedOn { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<int> ProcessId { get; set; }
        public string Reason { get; set; }
        public string RemDate { get; set; }
        public string RemTime { get; set; }
        public Nullable<int> NotConnected { get; set; }
        public Nullable<int> Mode { get; set; }
        public Nullable<int> EventId { get; set; }
    }
}
