﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiChamps.Models
{
    //public class BaseOnlineClassModel
    //{
    //    public IEnumerable<OnlineClass> OnlinesCls { get; set; }
    //}
    public class OnlineClassModel
    {
        public long ID { get; set; }
        public long InsertedId { get; set; }
        public string InsertedName { get; set; }
        public Nullable<System.Guid> SchoolId { get; set; }
        public string SchoolName { get; set; }
        public Nullable<int> TeacherId { get; set; }
        public string TeacherName { get; set; }
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public Nullable<System.Guid> SectionId { get; set; }
        public string SectionName { get; set; }
        public Nullable<int> SubjectId { get; set; }
        public string SubjectName { get; set; }
        public Nullable<DateTime> FromDate { get; set; }
        public Nullable<DateTime> ToDate { get; set; }
        public Nullable<TimeSpan> StartTime { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public Nullable<TimeSpan> EndTime { get; set; }

        public IEnumerable<OnlineClass> OnlinesCls { get; set; }

        public string SyllabusTitle { get; set; }
        public string SyllabusDescription { get; set; }
        public long ChapterId { get; set; }
        public string Zoom_Url { get; set; }
        public string Zoom_Password { get; set; }
       
    }
    public class onlineclassvideos
    {
        public long OnlineClassVideoId { get; set; }
        public Nullable<int> ChapterId { get; set; }
        public string ChapterName { get; set; }
        public string TeacherName { get; set; }
        public Nullable<int> OnlineClassId { get; set; }
        public string Topic_Name { get; set; }
        public string Topic_Description { get; set; }
        public Nullable<bool> Is_Taken { get; set; }
        public string Video_Link { get; set; }
        public string Password { get; set; }
        public Nullable<System.DateTime> Taken_Date { get; set; }
        public Nullable<System.DateTime> Inserted_On { get; set; }
    }
    public class onlineclassvideosapi
    {
        public Nullable<int> ChapterId { get; set; }
        public Nullable<int> SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string ChapterName { get; set; }
        public string TeacherName { get; set; }
        public Nullable<int> OnlineClassId { get; set; }
        public List<OnlineclassVideo> VideoLink { get; set; }

    }
    public class OnlineClassSyllabusModel
    {
        public string status { get; set; }
        public long ID { get; set; }
        public long InsertedId { get; set; }
        public string InsertedName { get; set; }
        public long ModifiedId { get; set; }
        public string ModifiedName { get; set; }
        public string SyllabusTitle { get; set; }
        public string SyllabusDescription { get; set; }
        public long ChapterId { get; set; }
        public string ChapterName { get; set; }
        public long OnlineClassId { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public Nullable<int> SubjectId { get; set; }
        public string SubjectName { get; set; }
        public Nullable<DateTime> modifieddate { get; set; }
    }
    public class OnlineClassViewDetailaModel
    {
        public OnlineClassModel ClassModel { get; set; }
        public List<OnlineClassSyllabusModel> SyllabusModel { get; set; }
        public List<OnlineHomeWorkModel> HomeWorks { get; set; }
    }


    public class onlinestudentattendance
    {
        public long ID { get; set; }
        public long OnlineClassId { get; set; }
        public string Student_Name { get; set; }
        public long StudentId { get; set; }
        public int Rating { get; set; }
        public string Review { get; set; }
        public bool IsPresent { get; set; }
        public Nullable<DateTime> modifieddate { get; set; }
    }
    public class adminviewonlineclasses
    {
        public OnlineClassModel onlineClass { get; set; }
        public List<OnlineClassSyllabusModel> classSyllabusModel { get; set; }
        public List<onlinestudentattendance> onlinestudentattendance { get; set; }
        public List<OnlineHomeWorkModel> HomeWorks { get; set; }
        public List<onlineclassvideos> onlineclassvideos { get; set; }

        public List<OnlineClassFeedback> Feedbacks { get; set; }
        
    }
    public class OnlineHomeWorkModel
    {
        public long ID { get; set; }
        public long InsertedId { get; set; }
        public string InsertedName { get; set; }
        public DateTime InsertedDate { get; set; }
        public long ModifiedId { get; set; }
        public string ModifiedName { get; set; }
        public long OnlineClassId { get; set; }
        public string HomeWorkPdf { get; set; }
        public string AnswerPdf { get; set; }
        public string verifiedteachername { get; set; }
        public bool? Verified { get; set; }
        public long? VerifiedTeacherId { get; set; }
        public long TotalStudentCount { get; set; }
        public long UploadedStudentCount { get; set; }
        public long NotUploaded { get; set; }

    }



    public class StudentDetails
    {
        public long ID { get; set; }
        public long StudentId { get; set; }
        public string StudentName { get; set; }
        public string Mobile { get; set; }
        public string uploadedpdf { get; set; }
        public string verifiedteachername { get; set; }
        public bool? Verified { get; set; }
        public long? VerifiedTeacherId { get; set; }
        public Nullable<DateTime> UplodedDate { get; set; }
    }

    public class OnlineClassHomeWorkDetails
    {
        public OnlineHomeWorkModel WorkModel { get; set; }
        public List<StudentDetails> UploadedDetails { get; set; }
        public List<StudentDetails> NotUploadedDetails { get; set; }
    }
}