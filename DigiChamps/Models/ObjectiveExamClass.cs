﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiChamps.Models
{
    public class ObjectiveExamClass
    {
    }
    public class onlineobjexamcls
    {
        public long objectiveid { get; set; }
        public int board { get; set; }
        public Guid[] school { get; set; }
        public long[] remvques { get; set; }
        public Guid schoolid { get; set; }
        public int cls { get; set; }
        public int subject { get; set; }
        public long examdate { get; set; }
        public long examtime { get; set; }
        public long examtype { get; set; }
        public string instruction { get; set; }
        public bool isnegative { get; set; }
        public long? negativemark { get; set; }
        public HttpPostedFileBase filebulkques { get; set; }
        public List<objexamquescls> question { get; set; }
    }
    public class objexamquescls
    {
        public long questionid { get; set; }
        public string question { get; set; }
        public decimal mark { get; set; }
        public HttpPostedFileBase questionimage { get; set; }
        public string option1 { get; set; }
        public string option2 { get; set; }
        public string option3 { get; set; }
        public string option4 { get; set; }
        public HttpPostedFileBase option1image { get; set; }
        public HttpPostedFileBase option2image { get; set; }
        public HttpPostedFileBase option3image { get; set; }
        public HttpPostedFileBase option4image { get; set; }
        public int CorrectAns { get; set; }
    }
    public class objectiveallquescls
    {
        public OnlineObjectiveExamQuestion examQuestion { get; set; }
        public List<OnlineObjectiveExamAnswer> examAnswers { get; set; }
    }
    public class ViewObjectiveExamcls
    {
        public ObjectiveExamDetailsModel objectiveExam { get; set; }
        public List<objectiveallquescls> questions { get; set; }
    }
    public class editobjectiveexamcls
    {
        public OnlineObjectiveExam exam { get; set; }
        public List<objectiveallquescls> questions { get; set; }
    }
    public class ObjectiveExamDetailsModel
    {

        public long OnlineObjectiveExamQuestionId { get; set; }
        public Nullable<long> OnlineObjectiveExamId { get; set; }
        public Nullable<long> OnlineExamTypeId { get; set; }
        public Nullable<long> OnlineExamDateId { get; set; }
        public Nullable<long> OnlineExamTimeId { get; set; }
        public Nullable<System.Guid> SchoolId { get; set; }
        public Nullable<int> BoardId { get; set; }
        public string BoardName { get; set; }
        public Nullable<int> ClassId { get; set; }
        public Nullable<int> SubjectId { get; set; }
        public string SchoolName { get; set; }
        public string ClassName { get; set; }
        public string Question { get; set; }
        public string QuestionImage { get; set; }
        public Nullable<decimal> Mark { get; set; }
        public bool IsNegative { get; set; }
        public string NegativeMark { get; set; }
        public string OnlineExamDateName { get; set; }
        public Nullable<DateTime> OnlineExamDate { get; set; }
        public string OnlineExamTypeName { get; set; }
        public Nullable<TimeSpan> StartTime { get; set; }
        public string StartTimeName { get; set; }
        public Nullable<TimeSpan> EndTime { get; set; }
        public string EndTimeName { get; set; }
        public string SubjectName { get; set; }

       
    }
}