﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiChamps.Models
{
    public class OnlineExamAPIModel
    {
        public long ID { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public long ExampTypeId { get; set; }
        public string ExampTypeName { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public Nullable<System.Guid> SectionId { get; set; }
        public string SectionName { get; set; }
        public long OnlineExamDateId { get; set; }
        public Nullable<DateTime> OnlineExamDates { get; set; }
        public long ExamTimeTableId { get; set; }
        public Nullable<TimeSpan> StartTime { get; set; }
        public string StartTimeName { get; set; } 
        public Nullable<TimeSpan> EndTime { get; set; }
        public string EndTimeName { get; set; }
        public long RemarkId { get; set; }
        public decimal Score { get; set; }
        public decimal TotalScore { get; set; }
        public long VerifiedId { get; set; }
        public string VerifiedName { get; set; }
        public string Category { get; set; }
        public Nullable<DateTime> VarifiedOn { get; set; }
        public bool IsVerified { get; set; }
        public bool IsObjective { get; set; }
        public bool IsSubmitted { get; set; }
        public string Instruction { get; set; }
        public string Pdf { get; set; }
    }

    public class OnlineExamResponseModel
    {
        public List<OnlineExamAPIModel> UpcommingList { get; set; }
        public List<OnlineExamAPIModel> GivenExamList { get; set; }
        public List<OnlineExamAPIModel> Result { get; set; }
    }
    public class ExamInstructionModel
    {
        public long id { get; set; }
        public string instruction { get; set; }
        public string pdf { get; set; }
    }
    public class ExamTypeModel
    {
        public long OnlineExamTypeId { get; set; }
        public string OnlineExamTypeName { get; set; } 
    }

    public class RemarksModel
    {
        public long sheetID { get; set; }
        public string totalScore { get; set; }
        public string questionPdf { get; set; }
        public string answarPdf { get; set; }
        public List<ScoreModel> scoreDetails { get; set; }
    }
    public class ScoreModel
    {
        public string questionNo { get; set; }
        public string marks{ get; set; }
        public string remarks { get; set; }
    }
    public class answersheetModel
    {
        public long answersheetId { get; set; }
        public string status { get; set; }
        public string subject { get; set; }
        public string examType { get; set; }
        public string className { get; set; }
        public string section { get; set; }
        public string dateOfExam { get; set; }
        public string score { get; set; }
        public string verifiedBy { get; set; }
        public string VerifiedDateTime { get; set; }
    }
}