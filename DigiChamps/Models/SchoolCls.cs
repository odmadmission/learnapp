﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;

namespace DigiChamps.Models
{
    public class SchoolCls
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();
        public List<Activity_Time_Class> GetActivityTimeDetails(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id, Guid school_id)
        {
            try
            {
                string sch = Convert.ToString(school_id);
                var school = DbContext.tbl_DC_Activity_Time.Where(x => x.SchoolId == sch && x.Reported_Date >= f_Date && x.Reported_Date <= t_Date).ToList();
                var ReportType = DbContext.tbl_DC_ReportType.ToList();
                var Registration = DbContext.tbl_DC_Registration.ToList();
                var module = DbContext.tbl_DC_Module.ToList();
                List<Activity_Time_Class> data = (from a in school
                                                  join b in ReportType on a.ReportType_ID equals b.ReportType_ID
                                                  join d in Registration on a.Regd_ID equals d.Regd_ID
                                                  // join e in module on a.Module_ID equals e.Module_ID
                                                  select new Activity_Time_Class
                                                  {
                                                      ActivityTime_Id = a.ActivityTime_Id,
                                                      StartDate = a.Start_Time,
                                                      EndDate = a.End_Time,
                                                      Inserted_On = a.Inserted_Date,
                                                      Reported_Date = a.Reported_Date,
                                                      Total_Time = a.Total_Time,
                                                      ReportType_ID = b.ReportType_ID,
                                                      ReportType_Name = b.ReportType_Name,
                                                      Regd_ID = a.Regd_ID,
                                                      Customer_Name = d.Customer_Name,
                                                      Class_ID = a.Class_ID,
                                                      SchoolId = Guid.Parse(a.SchoolId),
                                                      SectionId = d.SectionId,
                                                      ReportDate = Convert.ToDateTime(a.Reported_Date).ToString("dd/MM/yyyy"),
                                                      InsertedOn = Convert.ToDateTime(a.Inserted_Date).ToString("dd/MM/yyyy")
                                                  }).OrderByDescending(x => x.ActivityTime_Id).ToList();
                if (C_Id != null)
                {
                    data = data.Where(a => a.Class_ID == C_Id).ToList();
                }
                if (S_Id != null)
                {
                    data = data.Where(a => a.SectionId == S_Id).ToList();
                }
                return data;
            }
            catch
            {
                return null;
            }
        }

        public List<DigiChamps.Controllers.SchoolController.DataPoint> GetActivityChartTotalUser(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id, Guid school_id)
        {
            try
            {
                List<DigiChamps.Controllers.SchoolController.DataPoint> datapoints = new List<DigiChamps.Controllers.SchoolController.DataPoint>();
                var reportList = DbContext.Database.SqlQuery<ReportChartModel>(@"EXEC [odm_lms].[dbo].[SP_DC_Get_Activity_Time_Report] '" + f_Date + "','" + t_Date + "'," + Convert.ToInt32(C_Id) + ",'" + S_Id.ToString() + "','" + school_id.ToString() + "'").ToList<ReportChartModel>();
                for (int i = 0; i < reportList.Count; i++)
                {
                    datapoints.Add(new DigiChamps.Controllers.SchoolController.DataPoint(reportList[i].ReportDate.ToString(), Convert.ToInt32(reportList[i].Count)));
                }
                return datapoints;
            }
            catch { return null; }
        }
        public List<DigiChamps.Controllers.SchoolController.DataPoint> GetActivityChartTotalTime(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id, Guid school_id)
        {
            try
            {
                List<DigiChamps.Controllers.SchoolController.DataPoint> datapoints = new List<DigiChamps.Controllers.SchoolController.DataPoint>();
                var reportList = DbContext.Database.SqlQuery<ReportChartModel>(@"EXEC [odm_lms].[dbo].[SP_DC_Get_Activity_Total_Time_Report] '" + f_Date + "','" + t_Date + "'," + Convert.ToInt32(C_Id) + ",'" + S_Id.ToString() + "','" + school_id.ToString() + "'").ToList<ReportChartModel>();
                for (int i = 0; i < reportList.Count; i++)
                {
                    datapoints.Add(new DigiChamps.Controllers.SchoolController.DataPoint(reportList[i].ReportDate.ToString(), Convert.ToInt32(reportList[i].Count)));
                }
                return datapoints;
            }
            catch
            {
                return null;
            }
        }
        public class School_Details_Class
        {
            public Guid SchoolId { get; set; }
            public string SchoolLogo { get; set; }
            public string SchoolName { get; set; }
            public string SchoolThumbnail { get; set; }
            public string SchoolDescription { get; set; }

        }
        public List<School_Details_Class> GetSchoolData(Guid School_id)
        {
            try
            {
                List<School_Details_Class> school = (from a in DbContext.tbl_DC_School_Info.ToList().Where(a => a.SchoolId == School_id).ToList()
                                                     select new School_Details_Class
                                                   {
                                                       SchoolId = a.SchoolId,
                                                       SchoolLogo = a.SchoolLogo,
                                                       SchoolName = a.SchoolName,
                                                       SchoolThumbnail = a.SchoolThumbnail,
                                                       SchoolDescription = a.SchoolDescription
                                                   }).ToList();
                return school;
            }
            catch
            {
                return null;
            }
        }
        public class Total_User_Class
        {
            public string ReportDate { get; set; }
            public string Customer_Name { get; set; }
            public string Class_Name { get; set; }
            public string SectionName { get; set; }
            public int Class_ID { get; set; }
            public string SectionId { get; set; }
            public int Count { get; set; }
        }
        public class user_details_cls
        {
            public string Reported_Date { get; set; }
            public string Customer_Name { get; set; }
            public int Class_ID { get; set; }
            public string SectionId { get; set; }
            public int Regd_id { get; set; }
        }
        public class Total_User_Names_Class
        {
            public string Reported_Date { get; set; }
            public string Class_Name { get; set; }
            public string Section_Name { get; set; }
            public string Customer_Name { get; set; }
            public int? RegdId { get; set; }
            public int Count { get; set; }
        }
        public class Total_Time_User_Names_Class
        {
            public int Total_Time { get; set; }
            public string Customer_Name { get; set; }
            public string ReportDate { get; set; }
            public int Class_ID { get; set; }
            public string SectionId { get; set; }
            public string Class_Name { get; set; }
            public string SectionName { get; set; }


        }

        public List<ReportChartModel> GetActivityTotalUser(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id, Guid school_id)
        {
            try
            {

                var reportList = DbContext.Database.SqlQuery<ReportChartModel>(@"EXEC [odm_lms].[dbo].[SP_DC_Get_Activity_Time_Report] '" + f_Date + "','" + t_Date + "'," + Convert.ToInt32(C_Id) + ",'" + S_Id.ToString() + "','" + school_id.ToString() + "'").ToList<ReportChartModel>();

                return reportList;
            }
            catch { return null; }
        }
        public List<ReportChartModel> GetActivityTotalTime(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id, Guid school_id)
        {
            try
            {
                var reportList = DbContext.Database.SqlQuery<ReportChartModel>(@"EXEC [odm_lms].[dbo].[SP_DC_Get_Activity_Total_Time_Report] '" + f_Date + "','" + t_Date + "'," + Convert.ToInt32(C_Id) + ",'" + S_Id.ToString() + "','" + school_id.ToString() + "'").ToList<ReportChartModel>();
                return reportList;
            }
            catch
            {
                return null;
            }
        }
        public List<Total_User_Class> GetAllUserDetails(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id, Guid school_id)
        {
            try
            {

                List<Total_User_Class> result = DbContext.Database.SqlQuery<Total_User_Class>(@"SELECT * FROM [odm_lms].[dbo].[FN_DC_Get_Activity_Users_Report](@From_Date,@To_Date,@Class_Id,@SectionId,@School_Id)", new SqlParameter("@From_Date", f_Date), new SqlParameter("@To_Date", t_Date), new SqlParameter("@Class_Id", Convert.ToInt32(C_Id)), new SqlParameter("@SectionId", S_Id.ToString()), new SqlParameter("@School_Id", school_id.ToString())).ToList<Total_User_Class>();
                return result;
            }
            catch
            {
                return null;
            }
        }
        public List<Total_User_Class> GetAllUsernames(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id, Guid school_id)
        {
            try
            {
                List<Total_User_Class> res = DbContext.Database.SqlQuery<Total_User_Class>(@" SELECT A.[Count],A.ReportDate,A.Class_ID,A.SectionId,A.Class_Name,A.SectionName,B.Customer_Name FROM [odm_lms].[dbo].[FN_DC_Get_Activity_Users_Report](@From_Date,@To_Date,@Class_Id,@SectionId,@School_Id) AS A 
  INNER JOIN [odm_lms].[dbo].[FN_DC_Get_Total_Activity_Time_Report](@From_Date,@To_Date,@Class_Id,@SectionId,@School_Id) AS B ON A.ReportDate=B.ReportDate AND A.Class_ID=A.Class_ID AND A.SectionId=B.SectionId", new SqlParameter("@From_Date", f_Date), new SqlParameter("@To_Date", t_Date), new SqlParameter("@Class_Id", Convert.ToInt32(C_Id)), new SqlParameter("@SectionId", S_Id.ToString()), new SqlParameter("@School_Id", school_id.ToString())).ToList<Total_User_Class>();

                return res;
            }
            catch
            {
                return null;
            }


        }

        public List<Total_Time_User_Names_Class> GetAlltimeUsernames(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id, Guid school_id)
        {
            try
            {
                List<Total_Time_User_Names_Class> result = DbContext.Database.SqlQuery<Total_Time_User_Names_Class>(@"SELECT * FROM [odm_lms].[dbo].[FN_DC_Get_Total_Activity_Time_Report](@From_Date,@To_Date,@Class_Id,@SectionId,@School_Id)", new SqlParameter("@From_Date", f_Date), new SqlParameter("@To_Date", t_Date), new SqlParameter("@Class_Id", Convert.ToInt32(C_Id)), new SqlParameter("@SectionId", S_Id.ToString()), new SqlParameter("@School_Id", school_id.ToString())).ToList<Total_Time_User_Names_Class>();
                return result;
            }
            catch
            {
                return null;
            }
        }
        public DataSet GetReportData(DateTime fdt, DateTime tdt, int C_Id, string s_Id, Guid school_id)
        {
            //DateTime result = DateTime.ParseExact'', "yyyy-MM-dd", CultureInfo.InvariantCulture);
            //DateTime fdt = Convert.ToDateTime("2018-06-05 16:32:00.000");
            //DateTime tdt = Convert.ToDateTime("2018-07-05 16:32:00.000");
            //int? C_Id =Convert.ToInt32(c_Id);
            Guid? S_Id;
            if (s_Id == "" || s_Id == "0")
            {
                S_Id = null;
            }
            else
            {
                S_Id = Guid.Parse(s_Id);
            }
            // Guid school_id = new Guid(School_id);


            DataSet ds = new DataSet("Usage_Reports");

            DataTable dt1 = new DataTable();
            dt1 = ConvertToDataTable(GetSchoolData(school_id));
            dt1.TableName = "School_Details";
            ds.Tables.Add(dt1.Copy());

            DataTable dt2 = new DataTable();
            dt2 = ConvertToDataTable(GetActivityTotalUser(fdt, tdt, C_Id, S_Id, school_id));
            dt2.TableName = "All_Users";
            ds.Tables.Add(dt2.Copy());

            DataTable dt3 = new DataTable();
            dt3 = ConvertToDataTable(GetActivityTotalTime(fdt, tdt, C_Id, S_Id, school_id));
            dt3.TableName = "All_Activity_Time";
            ds.Tables.Add(dt3.Copy());

            DataTable dt4 = new DataTable();
            dt4 = ConvertToDataTable(GetAllUsernames(fdt, tdt, C_Id, S_Id, school_id));
            dt4.TableName = "All_Users_List";
            ds.Tables.Add(dt4.Copy());

            DataTable dt5 = new DataTable("All_Activity_Time_List");
            dt5 = ConvertToDataTable(GetAlltimeUsernames(fdt, tdt, C_Id, S_Id, school_id));
            dt5.TableName = "All_Activity_Time_List";
            ds.Tables.Add(dt5.Copy());
            return ds;
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        public class Test_Mark_Class
        {
            public int SumMark { get; set; }
            public int Totalnum { get; set; }
            public string names { get; set; }
        }
        public class Mark_Detail_class
        {
            public string Class_Name { get; set; }
            public string Section_Name { get; set; }
            public string Subject_Name { get; set; }
            public string Chapter_Name { get; set; }
            public string Test_Name { get; set; }
            public string Board_Name { get; set; }
        }
        public class Test_Individual_Report_Class
        {
            public int Topic_ID { get; set; }
            public int Mark { get; set; }
            public string Student_Name { get; set; }
            public int Result_ID { get; set; }
            public int Regd_ID { get; set; }
            public string Topic_Name { get; set; }
        }
        public class Test_Individual_Report1_Class
        {
            public int Topic_ID { get; set; }
            public int Mark { get; set; }
            public string Student_Name { get; set; }
            public int Result_ID { get; set; }
            public int Regd_ID { get; set; }
            public string Topic_Name { get; set; }
        }
        public class Test_Cumulative_Report_Class
        {
            public int Topic_ID { get; set; }
            public string Topic_Name { get; set; }
            public string Mark_Percent { get; set; }
            public string Student_Name { get; set; }
        }
        public List<Test_Mark_Class> GetTestMarkData(int C_Id, string S_Id, Guid school_id, int Sub_Id, int Chap_Id, int T_Id, int B_Id)
        {
            List<Test_Mark_Class> s = DbContext.Database.SqlQuery<Test_Mark_Class>(@"SELECT * FROM [odm_lms].[dbo].[FN_DC_GET_TEST_ANALYSIS](@Exam_ID,@Board_Id,@Class_Id,@Subject_Id,@Chapter_Id,@SchoolId,@SectionId)", new SqlParameter("@Exam_ID", T_Id), new SqlParameter("@Board_Id", B_Id), new SqlParameter("@Class_Id", C_Id), new SqlParameter("@Subject_Id", Sub_Id), new SqlParameter("@Chapter_Id", Chap_Id), new SqlParameter("@SchoolId", school_id), new SqlParameter("@SectionId", S_Id)).ToList<Test_Mark_Class>();
            int cnt = 1;
            foreach (var a in s.ToList())
            {
                a.Totalnum = cnt;
                cnt++;
            }
            return s;
        }
        public List<Mark_Detail_class> GetMarkDetails(int C_Id, string S_Id, int Sub_Id, int Chap_Id, int T_Id, int B_Id)
        {
            List<Mark_Detail_class> ln = new List<Mark_Detail_class>();
            Mark_Detail_class n = new Mark_Detail_class();
            n.Class_Name = DbContext.tbl_DC_Class.Where(a => a.Class_Id == C_Id).FirstOrDefault().Class_Name;
            Guid sid = Guid.Parse(S_Id);
            n.Section_Name = DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == sid).FirstOrDefault().SectionName;
            n.Subject_Name = DbContext.tbl_DC_Subject.Where(a => a.Subject_Id == Sub_Id).FirstOrDefault().Subject;
            n.Test_Name = DbContext.tbl_DC_Exam.Where(a => a.Exam_ID == T_Id).FirstOrDefault().Exam_Name;
            n.Chapter_Name = DbContext.tbl_DC_Chapter.Where(a => a.Chapter_Id == Chap_Id).FirstOrDefault().Chapter;
            n.Board_Name = DbContext.tbl_DC_Board.Where(a => a.Board_Id == B_Id).FirstOrDefault().Board_Name;
            ln.Add(n);
            return ln;
        }

        public List<Test_Individual_Report_Class> GetTestindividualData(int C_Id, string S_Id, Guid school_id, int Sub_Id, int Chap_Id, int T_Id, int B_Id)
        {
            List<Test_Individual_Report_Class> s = DbContext.Database.SqlQuery<Test_Individual_Report_Class>(@"SELECT * FROM [odm_lms].[dbo].[FN_DC_GET_TEST_INDIVIDUAL_REPORT](@Exam_ID,@Board_Id,@Class_Id,@Subject_Id,@Chapter_Id,@SchoolId,@SectionId)", new SqlParameter("@Exam_ID", T_Id), new SqlParameter("@Board_Id", B_Id), new SqlParameter("@Class_Id", C_Id), new SqlParameter("@Subject_Id", Sub_Id), new SqlParameter("@Chapter_Id", Chap_Id), new SqlParameter("@SchoolId", school_id), new SqlParameter("@SectionId", S_Id)).ToList<Test_Individual_Report_Class>();
            if (s.ToList().Count > 0)
            {
                return s;
            }
            else
            {
                List<Test_Individual_Report_Class> g = new List<Test_Individual_Report_Class>();
                Test_Individual_Report_Class h = new Test_Individual_Report_Class();
                h.Mark = 0;
                h.Regd_ID = 0;
                h.Result_ID = 0;
                h.Student_Name = "No Records";
                h.Topic_ID = 0;
                h.Topic_Name = "No Records";
                g.Add(h);
                return g;
            }
        }
        public List<Test_Cumulative_Report_Class> GetTestcumulativeData(int C_Id, string S_Id, Guid school_id, int Sub_Id, int Chap_Id, int T_Id, int B_Id)
        {
            List<Test_Cumulative_Report_Class> s = DbContext.Database.SqlQuery<Test_Cumulative_Report_Class>(@"SELECT * FROM [odm_lms].[dbo].[FN_DC_GET_TEST_CUMULATIVE_CLASS_REPORT](@Exam_ID,@Board_Id,@Class_Id,@Subject_Id,@Chapter_Id,@SchoolId,@SectionId)", new SqlParameter("@Exam_ID", T_Id), new SqlParameter("@Board_Id", B_Id), new SqlParameter("@Class_Id", C_Id), new SqlParameter("@Subject_Id", Sub_Id), new SqlParameter("@Chapter_Id", Chap_Id), new SqlParameter("@SchoolId", school_id), new SqlParameter("@SectionId", S_Id)).ToList<Test_Cumulative_Report_Class>();
            return s;
        }
        public List<Test_Cumulative_Report_Class> GetTestcumulative(int C_Id, string S_Id, Guid school_id, int Sub_Id, int Chap_Id, int T_Id, int B_Id)
        {
            List<Test_Cumulative_Report_Class> s = GetTestcumulativeData(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id);
            List<Test_Individual_Report_Class> m = GetTestindividualData(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id);
            if (m.ToList().Count > 0 && m.ToList()[0].Regd_ID != 0)
            {
                var r = (from a in s.ToList()
                         join b in m.ToList() on a.Topic_ID equals b.Topic_ID
                         select new Test_Cumulative_Report_Class
                         {
                             Mark_Percent = a.Mark_Percent,
                             Student_Name = b.Student_Name,
                             Topic_ID = a.Topic_ID,
                             Topic_Name = a.Topic_Name
                         }).ToList();
                return r;
            }
            else
            {
                var r = (from a in s.ToList()
                         select new Test_Cumulative_Report_Class
                         {
                             Mark_Percent = a.Mark_Percent,
                             Student_Name = "No Students",
                             Topic_ID = a.Topic_ID,
                             Topic_Name = a.Topic_Name
                         }).ToList();
                return r;
            }
        }

        public DataSet GetSchoolTestReport(int C_Id, string S_Id, Guid school_id, int Sub_Id, int Chap_Id, int T_Id, int B_Id)
        {
            DataSet ds = new DataSet("Test_Report");

            DataTable dt1 = new DataTable();
            dt1 = ConvertToDataTable(GetTestMarkData(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id));
            dt1.TableName = "All_Test_Mark";
            ds.Tables.Add(dt1.Copy());

            DataTable dt2 = new DataTable();
            dt2 = ConvertToDataTable(GetSchoolData(school_id));
            dt2.TableName = "School_Details";
            ds.Tables.Add(dt2.Copy());

            DataTable dt3 = new DataTable();
            dt3 = ConvertToDataTable(GetMarkDetails(C_Id, S_Id, Sub_Id, Chap_Id, T_Id, B_Id));
            dt3.TableName = "Basic_Details";
            ds.Tables.Add(dt3.Copy());

            DataTable dt4 = new DataTable();
            dt4 = ConvertToDataTable(GetTestindividualData(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id));
            dt4.TableName = "Individual_Details";
            ds.Tables.Add(dt4.Copy());

            DataTable dt5 = new DataTable();
            dt5 = ConvertToDataTable(GetTestcumulative(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id));
            dt5.TableName = "Cumulative_Details";
            ds.Tables.Add(dt5.Copy());

            return ds;
        }
        public DataSet GetAllSchoolTestReport(int C_Id, string S_Id, Guid school_id, int Sub_Id, int Chap_Id, int T_Id, int B_Id)
        {
            DataSet ds = new DataSet("All_Test_Report");

            DataTable dt2 = new DataTable();
            dt2 = ConvertToDataTable(GetSchoolData(school_id));
            dt2.TableName = "School_Details";
            ds.Tables.Add(dt2.Copy());

            DataTable dt3 = new DataTable();
            dt3 = ConvertToDataTable(GetMarkDetails(C_Id, S_Id, Sub_Id, Chap_Id, T_Id, B_Id));
            dt3.TableName = "Basic_Details";
            ds.Tables.Add(dt3.Copy());

            DataTable dt4 = new DataTable();
            dt4 = ConvertToDataTable(GetAllTestDetailsData(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id));
            dt4.TableName = "Test_Details";
            ds.Tables.Add(dt4.Copy());

            return ds;
        }
        public List<All_Test_Details_Class> GetAllTestDetailsData(int C_Id, string S_Id, Guid school_id, int Sub_Id, int Chap_Id, int T_Id, int B_Id)
        {
            List<All_Test_Details_Class> s = DbContext.Database.SqlQuery<All_Test_Details_Class>(@"SELECT * FROM [odm_lms].[dbo].[FN_DC_GET_ALL_TEST_REPORT](@Exam_ID,@Board_Id,@Class_Id,@Subject_Id,@Chapter_Id,@SchoolId,@SectionId)", new SqlParameter("@Exam_ID", T_Id), new SqlParameter("@Board_Id", B_Id), new SqlParameter("@Class_Id", C_Id), new SqlParameter("@Subject_Id", Sub_Id), new SqlParameter("@Chapter_Id", Chap_Id), new SqlParameter("@SchoolId", school_id), new SqlParameter("@SectionId", S_Id)).ToList<All_Test_Details_Class>();
            return s;
        }
    }
    public class All_Test_Details_Class
    {
        public int Question_Nos { get; set; }
        public int Total_Correct_Ans { get; set; }
        public string Customer_Name { get; set; }
        public int Regd_ID { get; set; }
        public string TOPIC_NAME { get; set; }

    }

    //[dbo].[FN_DC_GET_ALL_TEST_REPORT]
    public class Activity_Time_Class
    {
        public int ActivityTime_Id { get; set; }
        public int? Regd_ID { get; set; }
        public DateTime? Inserted_On { get; set; }
        public string InsertedOn { get; set; }
        public int? Class_ID { get; set; }
        public Guid? SchoolId { get; set; }
        public DateTime? Reported_Date { get; set; }
        public string ReportDate { get; set; }
        public string Customer_Name { get; set; }
        public Guid? SectionId { get; set; }
        public string ReportType_Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Total_Time { get; set; }
        public int ReportType_ID { get; set; }
    }
}