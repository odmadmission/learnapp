﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiChamps.Models
{
    public class RevisionMaterialModel
    {
        public long ID { get; set; }
        public Guid? SchoolId { get; set; }
        public string SchoolName { get; set; }
        public int? BoardId { get; set; }
        public string BoardName { get; set; }
        public int? ClassId { get; set; }
        public string ClassName { get; set; }
        public Guid? SectionId { get; set; }
        public string SectionName { get; set; }
        public int? SubjectId { get; set; }
        public string SubjectName { get; set; }
        public int? ChapaterId { get; set; }
        public string ChapterName { get; set; }
        public long RevisionPdfID { get; set; }
        public string Pdf { get; set; }
    }
}