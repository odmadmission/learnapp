﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiChamps.Models
{
    public class ApiModel
    {
       
    }
    public class AssignedSubjectListByTeacherId
    {
        public long ID { get; set; }
        public long AssignedSubjectId { get; set; }
        public long TeacherId { get; set; }
        public string TeacherName { get; set; }
        public Nullable<System.Guid> SchoolId { get; set; }
        public string SchoolName { get; set; }
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        public long ClassId { get; set; }
        public string ClassName { get; set; }
        public Nullable<System.Guid> SectionId { get; set; }
        public string SectionName { get; set; }
        public long SubjectId { get; set; }
        public string SubjectName { get; set; }
    }

    public class StudentModel
    {
        public long ID { get; set; }
        public string StudentName { get; set; }
        public string Mobile { get; set; }
        public long SchoolId { get; set; }
        public string SchoolName { get; set; }
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        public long ClassId { get; set; }
        public string ClassName { get; set; }
        public Nullable<System.Guid> SectionId { get; set; }
        public string SectionName { get; set; }

    }
}