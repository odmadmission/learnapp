//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigiChamps.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Questionbank_Review
    {
        public int QuestionbankReview_ID { get; set; }
        public string Message { get; set; }
        public string Attachment { get; set; }
        public Nullable<bool> Is_Student { get; set; }
        public Nullable<int> Posted_By_ID { get; set; }
        public string Inserted_By { get; set; }
        public Nullable<System.DateTime> Inserted_On { get; set; }
        public string Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_On { get; set; }
        public Nullable<int> Questionbank_ID { get; set; }
        public Nullable<bool> Is_Active { get; set; }
    }
}
