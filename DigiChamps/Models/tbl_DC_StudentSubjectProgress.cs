//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigiChamps.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_DC_StudentSubjectProgress
    {
        public int StudentSubjectProgress_ID { get; set; }
        public Nullable<int> Subject_ID { get; set; }
        public Nullable<int> Chapter_ID { get; set; }
        public Nullable<int> Class_ID { get; set; }
        public Nullable<System.Guid> School_ID { get; set; }
        public Nullable<System.Guid> Section_ID { get; set; }
        public Nullable<int> Regd_ID { get; set; }
        public Nullable<int> Module_ID { get; set; }
        public string Module_Type { get; set; }
        public Nullable<System.DateTime> Inserted_On { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<bool> Is_Delete { get; set; }
    }
}
