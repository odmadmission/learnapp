﻿$(function () {
    debugger;
    var i = 0;
    $('.sliderContainer p').eq(0).css({ "left": "0%" });
    //when the next button is clicked on
    $('.nex').on("click", function () {
        debugger;
        //increase the display picture index by 1
        i = i + 1;
        //if we are at the end of the image queue, set the index back to 0
        if (i == $('.sliderContainer img').length && i == $('.sliderContainer p').length) {
            i = 0;
        }
        //set current image and previous image
        var currentImg = $('.sliderContainer img').eq(i);
        var prevImg = $('.sliderContainer img').eq(i - 1);

        var currentpara = $('.sliderContainer p').eq(i);
        var prevpara = $('.sliderContainer p').eq(i - 1);
        //call function to animate the rotation of the images to the right
        animateImage(prevImg, currentImg, currentpara, prevpara);
    });
    //when the previous button is clicked on
    $('.previous').on("click", function () {
        debugger;
        //if we are at the beginning of the image queue, set the previous image to the first image and the current image to the last image of the queue
        if (i == 0) {
            prevImg = $('.sliderContainer img').eq(0);
            prevpara = $('.sliderContainer p').eq(0);
            i = $('.sliderContainer img').length - 1;
            currentImg = $('.sliderContainer img').eq(i);
            currentpara = $('.sliderContainer p').eq(i);
        }
            //decrease the display picture index by 1
        else {
            i = i - 1;
            //set current and previous images
            currentImg = $('.sliderContainer img').eq(i);
            prevImg = $('.sliderContainer img').eq(i + 1);

            currentpara = $('.sliderContainer p').eq(i);
            prevpara = $('.sliderContainer p').eq(i + 1);
        }
        //call function to animate the rotation of the images to the left
        animateImageLeft(prevImg, currentImg, prevpara, currentpara);
    });
    //function to animate the rotation of the images to the left
    function animateImageLeft(prevImg, currentImg, prevpara, currentpara) {
        //move the image to be displayed off the visible container to the right

        //currentImg.css({ "left": "100%"});
        //currentImg.css({ "display": "block" });
        //currentpara.css({ "left": "100%" });
        //currentpara.css({ "display": "block" });

        currentImg.css({ "left": "100%" });
        currentImg.fadeIn(300);
        currentpara.css({ "left": "100%" });
        currentpara.fadeIn(300);


        //slide the image to be displayed from off the container onto the visible container to make it slide from the right to left
        currentImg.animate({ "left": "0%" }, 150);
       
        currentpara.animate({ "left": "0%" }, 150);
      
        //slide the previous image off the container from right to left

        //prevImg.animate({ "left": "-100%" }, 150);
        //prevImg.animate({ "display": "none" });
        //prevpara.animate({ "left": "-100%" }, 150);
        //prevpara.animate({ "display": "none" });

        prevImg.animate({ "left": "-100%" }, 150);
        prevImg.fadeOut(300);
        prevpara.animate({ "left": "-100%" }, 150);
        prevpara.fadeOut(300);
    }
    //function to animate the rotation of the images to the right
    function animateImage(prevImg, currentImg, currentpara, prevpara) {
        //move the image to be displayed off the container to the left
        currentImg.css({ "left": "-100%" });
       // currentImg.css({ "display": "block" });
        currentImg.fadeIn(300);

        currentpara.css({ "left": "-100%" });
       // currentpara.css({ "display": "block" });
        currentpara.fadeIn(300);

        //slide the image to be displayed from off the container onto the container to make it slide from left to right
        currentImg.animate({ "left": "0%" }, 150);
       
        currentpara.animate({ "left": "0%" }, 150);
       
        //slide the image from on the container to off the container to make it slide from left to right
        prevImg.animate({ "left": "100%" }, 150);
       // prevImg.css({ "display": "none" });
        prevImg.fadeOut(300);
        prevpara.animate({ "left": "100%" }, 150);
        prevpara.fadeOut(300);
       // prevpara.css({ "display": "none" });
    }
    
    $('.tabheader').click(function () {
        $(this).next('div').slideToggle();
        $(this).find('.growthsteps').slideToggle();
        $(this).parent().siblings().children().next().slideUp();
        $(this).parent().siblings().children().find('.growthsteps').slideUp();
        return false;
    });
    $('.menubarline').click(function () {
        $('.menupanelbar').slideToggle();
    })

});