package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/13/2018.
 */

public class AddDiscussionRequest implements Parcelable{

    private String SchoolId;
    private int CreatedBy;
    private int RoleID;
    private String Title;

    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int createdBy) {
        CreatedBy = createdBy;
    }

    public int getRoleID() {
        return RoleID;
    }

    public void setRoleID(int roleID) {
        RoleID = roleID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.SchoolId);
        dest.writeInt(this.CreatedBy);
        dest.writeInt(this.RoleID);
        dest.writeString(this.Title);
    }

    public AddDiscussionRequest() {
    }

    protected AddDiscussionRequest(Parcel in) {
        this.SchoolId = in.readString();
        this.CreatedBy = in.readInt();
        this.RoleID = in.readInt();
        this.Title = in.readString();
    }

    public static final Creator<AddDiscussionRequest> CREATOR = new Creator<AddDiscussionRequest>() {
        @Override
        public AddDiscussionRequest createFromParcel(Parcel source) {
            return new AddDiscussionRequest(source);
        }

        @Override
        public AddDiscussionRequest[] newArray(int size) {
            return new AddDiscussionRequest[size];
        }
    };
}
