package com.sseduventures.digichamps.adapter;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.Model.Basic_info_model;
import com.sseduventures.digichamps.R;

import java.util.ArrayList;

public class Email_List_DialogAdapter extends ArrayAdapter<String> {

    private ArrayList<String> dataSet;
    Context mContext;

    private static class ViewHolder {
        TextView board_tv;

        ImageView checkin_img;
        RelativeLayout relative;
    }

    public Email_List_DialogAdapter(ArrayList<String> data, Context context) {
        super(context, R.layout.email_list_adapter, data);
        this.dataSet = data;
        this.mContext=context;

    }



    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String value= dataSet.get(position);
        final ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.email_list_adapter, parent, false);
            viewHolder.board_tv = (TextView) convertView.findViewById(R.id.board_tv);


            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }


        viewHolder.board_tv.setText(value);


        return convertView;
    }
}