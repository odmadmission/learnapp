package com.sseduventures.digichamps.activities;


import com.sseduventures.digichamps.R;

public enum ModelObject_Onboard {
    PINK(R.string.pink, R.layout.pager_item5),
    BLUE(R.string.blue, R.layout.pager_item1),
    YELLOW(R.string.black, R.layout.pager_item4),
    BLACK(R.string.black, R.layout.pager_item3),
    RED(R.string.red, R.layout.pager_item);
 ;

    private int mTitleResId;
    private int mLayoutResId;

    ModelObject_Onboard(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}