package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 9/1/2018.
 */

public class OrderConfResponse implements Parcelable {

    private OrderConfSuccess success;

    public OrderConfSuccess getSuccess() {
        return success;
    }

    public void setSuccess(OrderConfSuccess success) {
        this.success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) this.success, flags);
    }

    public OrderConfResponse() {
    }

    protected OrderConfResponse(Parcel in) {
        this.success = in.readParcelable(OrderConfSuccess.class.getClassLoader());
    }

    public static final Creator<OrderConfResponse> CREATOR = new Creator<OrderConfResponse>() {
        @Override
        public OrderConfResponse createFromParcel(Parcel source) {
            return new OrderConfResponse(source);
        }

        @Override
        public OrderConfResponse[] newArray(int size) {
            return new OrderConfResponse[size];
        }
    };
}
