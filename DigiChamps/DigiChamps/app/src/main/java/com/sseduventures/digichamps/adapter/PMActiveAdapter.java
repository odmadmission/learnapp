package com.sseduventures.digichamps.adapter;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.PersonalMentorActivity;
import com.sseduventures.digichamps.domain.PMActivetaskDataList;

import java.util.List;


public class PMActiveAdapter extends RecyclerView.Adapter<PMActiveAdapter.MyTestView> {
    private List<PMActivetaskDataList> activelist;
    LinearLayout container, container1;
    PersonalMentorActivity context;
    public static int position;


    public static class MyTestView extends RecyclerView.ViewHolder {

        TextView text_header;
        LinearLayout container;
        ImageView imageview;


        public MyTestView(View view) {
            super(view);

            text_header = (TextView) view.findViewById(R.id.text_header);
            container = view.findViewById(R.id.container);
            imageview = view.findViewById(R.id.imageview);
            imageview.setBackgroundResource(R.drawable.ic_presentation);


        }
    }

    public PMActiveAdapter(List<PMActivetaskDataList> activelist,
                           PersonalMentorActivity context) {
        this.activelist = activelist;
        this.context = context;


    }

    @Override
    public MyTestView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.personal_mentor_adapter1,
                parent, false);

        MyTestView myTestView = new MyTestView(view);
        return myTestView;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final MyTestView holder, final int listPosition) {
        position = listPosition;
        TextView text_header = holder.text_header;


        text_header.setText(activelist.get(position).getTaskHeder());


        this.container1 = container;

        holder.container.setOnClickListener(onClickListener(position));

    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.showItemDialog(activelist.get(position).getTaskHeder(), "Active Task",
                        activelist.get(position).getTaskDetails(), activelist.get(position).getStartDate(),
                        activelist.get(position).getEndDate());

            }
        };
    }

    @Override
    public int getItemCount() {
        return activelist.size();
    }

}

