
package com.sseduventures.digichamps.config.ccavenue;

public class Constants {
	public static final String PARAMETER_SEP = "&";
	public static final String PARAMETER_EQUALS = "=";
	public static final String TRANS_URL = "https://secure.ccavenue.com/transaction/initTrans";

	// payment success/failure address on your server
	public static final String REGISTER_URL = "https://thedigichamps.com/student/paymentinfo";

}