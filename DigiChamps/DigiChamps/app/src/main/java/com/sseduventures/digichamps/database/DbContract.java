package com.sseduventures.digichamps.database;

import android.net.Uri;
import android.provider.BaseColumns;




/**
 * Created by Raju Satheesh on 1/17/2017.
 */

public class DbContract {


    private DbContract()
    {

    }

    public  static class Schema
    {
        public static final int DATABASE_VERSION = 5;

        public static final String DATABASE_NAME = "com.sseduventures.digichamps.twiggo_schema.db";
        public static final String CONTENT_AUTHORITY = "com.sseduventures.digichamps.provider.digichamps.authority";

        public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

        public static final String PATH_USER = UserTable.TABLE_NAME;
        public static final String PATH_COIN = CoinTable.TABLE_NAME;
        public static final String PATH_NOTIFICATION = NotificationTable.TABLE_NAME;


        public static final String PATH_VIDEO_LOG = VideoLogTable.TABLE_NAME;

        public static final String PATH_PROGRESS_LOG = ProgressTable.TABLE_NAME;
        public static final String PATH_ACTIVITY_LOG = ActivityTimeTable.TABLE_NAME;

        public static final String PATH_EXAM = ExamTable.TABLE_NAME;
        public static final String PATH_DICTIONARY = DictionaryTable.TABLE_NAME;
        public static final String PATH_FEED = FeedTable.TABLE_NAME;
        public static final String PATH_BOOKMARKS = BookMarksTable.TABLE_NAME;


        public static final String PATH_BOARD = BoardTable.TABLE_NAME;
        public static final String PATH_CLASS = ClassTable.TABLE_NAME;
        public static final String PATH_SUBJECT = SubjectTable.TABLE_NAME;
        public static final String PATH_CHAPTER = ChapterTable.TABLE_NAME;
        public static final String PATH_CONTENT = ContentTable.TABLE_NAME;



        public static final String TYPE_TEXT = " TEXT";
        public static final String TYPE_BLOB = " BLOB";
        public static final String TYPE_INTEGER = " INTEGER";
        public static final String TYPE_REAL = " REAL";
        public static final String CAMMA= " , ";
        public static final String CREATE_TABLE= " CREATE TABLE ";
        public static final String OPEN_BRACKET= " ( ";
        public static final String CLOSE_BRACKET= " ); ";
        public static final String CLOSE_BRACKET_WITHOUT_END= " ) ";
        public static final String PRIMARY_KEY= " PRIMARY KEY ";
        public static final String UNIQUE_KEY = " UNIQUE";
        public static final String NOT_NULL = " NOT NULL";
        public static final String DROP_TABLE= "DROP TABLE IF EXISTS ";
        public static final String DEFAULT = " DEFAULT ";
        public static final String FORIEGN_KEY = " FORIEGN KEY  ";
        public static final String REFERENCES = " REFERENCES  ";




    }


    public  static class UserTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_USER).
                build();
        public static final String TABLE_NAME = "UserTable";
        public static final String USER_ID = "user_id";
        public static final String NAME = "name";
        public static final String  MOBILE= "MOBILE";
        public static final String EMAIL = "EMAIL";
        public static final String BOARDID = "BOARDID";
        public static final String CLASSID = "CLASSID";

        public static final String SCHOOL = "SCHOOL";
        public static final String BIRTHDATE = "BIRTHDATE";


        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+Schema.CAMMA+
                        USER_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+Schema.UNIQUE_KEY+Schema.CAMMA+
                        NAME +Schema.TYPE_TEXT+Schema.CAMMA+
                        MOBILE +Schema.TYPE_TEXT+Schema.CAMMA+
                        EMAIL +Schema.TYPE_TEXT+Schema.CAMMA+
                        BOARDID +Schema.TYPE_TEXT+Schema.CAMMA+
                        CLASSID +Schema.TYPE_TEXT+Schema.CAMMA+

                        SCHOOL +Schema.TYPE_TEXT +Schema.CAMMA+
                        BIRTHDATE +Schema.TYPE_TEXT+
                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] MEMBER_PROJECTION=new String[]
                {
                        _ID,
                        USER_ID,
                        NAME,
                        MOBILE,
                        EMAIL,
                        BOARDID,
                        CLASSID,
                        SCHOOL,
                        BIRTHDATE
                };


    }

    public  static class FeedTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_FEED).
                build();
        public static final String TABLE_NAME = "FeedTable";
        public static final String FEED_ID = "feed_id";
        public static final String TITLE = "title";
        public static final String  TEXT= "text";
        public static final String  IMAGE_URL= "image_url";
        public static final String LINK = "link";
        public static final String INSERTEDON = "inserted";


        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+
                        Schema.CAMMA+
                        FEED_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+
                        Schema.UNIQUE_KEY+Schema.CAMMA+
                        TITLE +Schema.TYPE_TEXT+Schema.CAMMA+
                        TEXT +Schema.TYPE_TEXT+Schema.CAMMA+
                        LINK +Schema.TYPE_TEXT+Schema.CAMMA+
                        IMAGE_URL +Schema.TYPE_TEXT+Schema.CAMMA+
                        INSERTEDON +Schema.TYPE_TEXT+
                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] MEMBER_PROJECTION=new String[]
                {
                        _ID,
                        FEED_ID,
                        TITLE,
                        TEXT,
                        LINK,
                        IMAGE_URL,
                        INSERTEDON
                };


    }
    public  static class DictionaryTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_DICTIONARY).
                build();

        public static final String TABLE_NAME = "DictionaryTable";
        public static final String DICTIONARY_ID = "DICTIONARY_ID";
        public static final String WORD = "WORD";
        public static final String IMAGE_URL= "IMAGE_URL";
        public static final String PATH= "PATH";
        public static final String DESCRIPTION = "DESCRIPTION";
        public static final String INSERTED_ON = "INSERTED_ON";
        public static final String INSERTED_BY = "INSERTED_BY";
        public static final String ACTIVE = "ACTIVE";
        public static final String DELETED = "DELETED";



        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+
                        Schema.CAMMA+
                        DICTIONARY_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+
                        Schema.UNIQUE_KEY+Schema.CAMMA+
                        WORD +Schema.TYPE_TEXT+Schema.CAMMA+
                        IMAGE_URL +Schema.TYPE_TEXT+Schema.CAMMA+
                        PATH +Schema.TYPE_TEXT+Schema.CAMMA+
                        DESCRIPTION +Schema.TYPE_TEXT+Schema.CAMMA+
                        INSERTED_BY +Schema.TYPE_TEXT+Schema.CAMMA+
                        ACTIVE +Schema.TYPE_INTEGER+Schema.CAMMA+
                        DELETED +Schema.TYPE_INTEGER+Schema.CAMMA+
                        INSERTED_ON +Schema.TYPE_INTEGER+
                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] PROJECTION=new String[]
                {
                        _ID,
                        DICTIONARY_ID,
                        WORD,
                        IMAGE_URL,
                        PATH,
                        DESCRIPTION,
                        INSERTED_BY,
                        ACTIVE,
                        DELETED,
                        INSERTED_ON
                };


    }
    public  static class BoardTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_BOARD).
                build();

        public static final String TABLE_NAME = "BoardTable";
        public static final String BOARD_ID = "BOARD_ID";
        public static final String NAME = "NAME";




        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+
                        Schema.CAMMA+
                        BOARD_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+
                        Schema.UNIQUE_KEY+Schema.CAMMA+
                        NAME +Schema.TYPE_TEXT+
                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] PROJECTION=new String[]
                {
                        _ID,
                        BOARD_ID,
                        NAME
                };


    }
    public  static class ClassTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_CLASS).
                build();

        public static final String TABLE_NAME = "ClassTable";
        public static final String CLASS_ID = "CLASS_ID";
        public static final String NAME = "NAME";
        public static final String BOARD_ID = "BOARD_ID";



        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+
                        Schema.CAMMA+
                        CLASS_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+
                        Schema.UNIQUE_KEY+Schema.CAMMA+
                        BOARD_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+Schema.CAMMA+
                        NAME +Schema.TYPE_TEXT+
                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] PROJECTION=new String[]
                {
                        _ID,
                        CLASS_ID,
                        BOARD_ID,
                        NAME
                };


    }
    public  static class SubjectTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_SUBJECT).
                build();

        public static final String TABLE_NAME = "SubjectTable";
        public static final String SUBJECT_ID = "SUBJECT_ID";
        public static final String CLASS_ID = "CLASS_ID";
        public static final String NAME = "NAME";
        public static final String BOARD_ID = "BOARD_ID";



        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+
                        Schema.CAMMA+
                        CLASS_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+Schema.CAMMA+
                        BOARD_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+Schema.CAMMA+
                        SUBJECT_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+
                        Schema.UNIQUE_KEY+Schema.CAMMA+

                        NAME +Schema.TYPE_TEXT+
                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] PROJECTION=new String[]
                {
                        _ID,
                        CLASS_ID,
                        BOARD_ID,
                        SUBJECT_ID,
                        NAME
                };


    }
    public  static class ChapterTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_CHAPTER).
                build();

        public static final String TABLE_NAME = "SubjectTable";
        public static final String CHAPTER_ID = "CHAPTER_ID";
        public static final String SUBJECT_ID = "SUBJECT_ID";
        public static final String CLASS_ID = "CLASS_ID";
        public static final String NAME = "NAME";
        public static final String BOARD_ID = "BOARD_ID";



        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+
                        Schema.CAMMA+
                        CLASS_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+Schema.CAMMA+
                        BOARD_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+Schema.CAMMA+
                        SUBJECT_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+Schema.CAMMA+
                        CHAPTER_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+
                        Schema.UNIQUE_KEY+Schema.CAMMA+

                        NAME +Schema.TYPE_TEXT+
                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] PROJECTION=new String[]
                {
                        _ID,
                        CLASS_ID,
                        BOARD_ID,
                        SUBJECT_ID,
                        CHAPTER_ID,
                        NAME
                };


    }
    public  static class ContentTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_CHAPTER).
                build();

        public static final String TABLE_NAME = "ContentTable";
        public static final String CHAPTER_ID = "CHAPTER_ID";
        public static final String SUBJECT_ID = "SUBJECT_ID";
        public static final String CLASS_ID = "CLASS_ID";
        public static final String NAME = "NAME";
        public static final String BOARD_ID = "BOARD_ID";



        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+
                        Schema.CAMMA+
                        CLASS_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+Schema.CAMMA+
                        BOARD_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+Schema.CAMMA+
                        SUBJECT_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+Schema.CAMMA+
                        CHAPTER_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+
                        Schema.UNIQUE_KEY+Schema.CAMMA+

                        NAME +Schema.TYPE_TEXT+
                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] PROJECTION=new String[]
                {
                        _ID,
                        CLASS_ID,
                        BOARD_ID,
                        SUBJECT_ID,
                        CHAPTER_ID,
                        NAME
                };


    }
    public  static class BookMarksTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_BOOKMARKS).
                build();
        public static final String TABLE_NAME = "BookMarksTable";
        public static final String BOOKMARK_ID = "BOOKMARK_ID";
        public static final String TYPE = "TYPE";
        public static final String CLASS_ID= "CLASS_ID";
        public static final String SAVED= "SAVED";
        public static final String MODULE_ID = "MODULE_ID";
        public static final String MODULE_NAME = "MODULE_NAME";
        public static final String MODULE_DESC = "MODULE_DESC";
        public static final String MODULE_IMAGE_URL = "MODULE_IMAGE_URL";
        public static final String LINK = "LINK";
        public static final String AVAIL = "AVAIL";
        public static final String FREE = "FREE";
        public static final String EXPIRE = "EXPIRE";
        public static final String VIDEO_KEY = "VIDEO_KEY";
        public static final String INSERTED_ON = "INSERTED_ON";


        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+
                        Schema.CAMMA+
                        BOOKMARK_ID +Schema.TYPE_INTEGER+Schema.NOT_NULL+
                        Schema.UNIQUE_KEY+Schema.CAMMA+
                        TYPE +Schema.TYPE_INTEGER+Schema.CAMMA+
                        CLASS_ID +Schema.TYPE_INTEGER+Schema.CAMMA+
                        LINK +Schema.TYPE_TEXT+Schema.CAMMA+
                        MODULE_IMAGE_URL +Schema.TYPE_TEXT+Schema.CAMMA+
                        MODULE_DESC +Schema.TYPE_TEXT+Schema.CAMMA+
                        MODULE_NAME +Schema.TYPE_TEXT+Schema.CAMMA+
                        MODULE_ID +Schema.TYPE_TEXT+Schema.CAMMA+
                        AVAIL +Schema.TYPE_INTEGER+Schema.CAMMA+
                        FREE +Schema.TYPE_INTEGER+Schema.CAMMA+
                        EXPIRE +Schema.TYPE_INTEGER+Schema.CAMMA+
                        VIDEO_KEY +Schema.TYPE_TEXT+Schema.CAMMA+
                        INSERTED_ON +Schema.TYPE_INTEGER+
                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] MEMBER_PROJECTION=new String[]
                {
                        _ID,
                        BOOKMARK_ID,
                        TYPE,
                        CLASS_ID,
                        LINK,
                        LINK,
                        MODULE_IMAGE_URL,
                        MODULE_DESC,
                        MODULE_NAME,
                        MODULE_ID,
                        AVAIL,
                        FREE,
                        EXPIRE,
                        VIDEO_KEY,
                        INSERTED_ON






                };


    }




    public  static class CoinTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_COIN).
                build();
        public static final String TABLE_NAME = "CoinTable";
        public static final String COIN_ID = "COIN_ID";
        public static final String MODULE_ID = "MODULE_ID";
        public static final String  INSERTED_DATE= "INSERTED_DATE";
        public static final String COIN_TYPE = "COIN_TYPE";
        public static final String COINS = "COINS";

        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+Schema.CAMMA+
                        COIN_ID +Schema.TYPE_INTEGER+Schema.CAMMA+
                        MODULE_ID +Schema.TYPE_TEXT+Schema.CAMMA+
                        INSERTED_DATE +Schema.TYPE_INTEGER+Schema.CAMMA+
                        COINS +Schema.TYPE_INTEGER+Schema.CAMMA+
                        COIN_TYPE +Schema.TYPE_INTEGER+
                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] MEMBER_PROJECTION=new String[]
                {
                        _ID,
                        COIN_ID,
                        MODULE_ID,
                        INSERTED_DATE,
                        COIN_TYPE,
                        COINS
                };


    }

    public  static class ExamTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_EXAM).
                build();
        public static final String TABLE_NAME = "ExamTable";
        public static final String answerId = "answerId";
        public static final String Question = "Question";
        public static final String  ques_img1= "ques_img1";
        public static final String ques_img2 = "ques_img2";
        public static final String skip = "skip";
        public static final String option = "option";

        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+Schema.CAMMA+
                        answerId +Schema.TYPE_TEXT+Schema.CAMMA+
                        Question +Schema.TYPE_TEXT+Schema.CAMMA+
                        ques_img1 +Schema.TYPE_INTEGER+Schema.CAMMA+
                        ques_img2 +Schema.TYPE_INTEGER+Schema.CAMMA+
                        skip +Schema.TYPE_INTEGER+
                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] MEMBER_PROJECTION=new String[]
                {
                        _ID,
                        answerId,
                        Question,
                        ques_img1,
                        ques_img2,
                        skip
                };


    }

    public  static class NotificationTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_NOTIFICATION).
                build();
        public static final String TABLE_NAME = "NotificationTable";
        public static final String NOTIFY_ID = "NOTIFY_ID";
        public static final String TYPE = "TYPE";
        public static final String INSERTED_DATE= "INSERTED_DATE";
        public static final String TITLE = "TITLE";
        public static final String MESSAGE = "MESSAGE";
        public static final String DATA = "DATA";
        public static final String UNREAD= "UNREAD";



        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+Schema.CAMMA+
                        NOTIFY_ID +Schema.TYPE_INTEGER+Schema.CAMMA+
                        TYPE +Schema.TYPE_TEXT+Schema.CAMMA+
                        INSERTED_DATE +Schema.TYPE_INTEGER+
                        Schema.CAMMA+
                        TITLE +Schema.TYPE_TEXT+Schema.CAMMA+
                        MESSAGE +Schema.TYPE_TEXT+Schema.CAMMA+
                        UNREAD +Schema.TYPE_INTEGER+Schema.CAMMA+
                        DATA +Schema.TYPE_TEXT+
                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] MEMBER_PROJECTION=new String[]
                {
                        _ID,
                        NOTIFY_ID,
                        TYPE,
                        INSERTED_DATE,
                        TITLE,
                        MESSAGE,
                        UNREAD,
                        DATA
                };


    }

    public  static class VideoLogTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_VIDEO_LOG).
                build();
        public static final String TABLE_NAME = "VideoLogTable";
        public static final String MODULEID = "MODULEID";
        public static final String REGDID = "REGDID";
        public static final String SEEN_TIME = "SEEN_TIME";
        public static final String SCHOOL_CODE = "SCHOOL_CODE";
        public static final String CLASS_CODE = "CLASS_CODE";
        public static final String START_TIME = "START_TIME";
        public static final String END_TIME = "END_TIME";
        public static final String SECTION_CODE = "SECTION_CODE";
        public static final String CREATE_DATE = "CREATE_DATE";
        public static final String SYNC_DATE = "SYNC_DATE";
        public static final String STATUS = "STATUS";



        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+Schema.CAMMA+
                        MODULEID +Schema.TYPE_TEXT+Schema.CAMMA+
                        SCHOOL_CODE +Schema.TYPE_TEXT+Schema.CAMMA+
                        SECTION_CODE +Schema.TYPE_TEXT+Schema.CAMMA+
                        CLASS_CODE +Schema.TYPE_INTEGER+Schema.CAMMA+
                        CREATE_DATE +Schema.TYPE_INTEGER+Schema.CAMMA+
                        START_TIME +Schema.TYPE_INTEGER+Schema.CAMMA+
                        END_TIME +Schema.TYPE_INTEGER+Schema.CAMMA+
                        SYNC_DATE +Schema.TYPE_INTEGER+Schema.CAMMA+
                        REGDID +Schema.TYPE_INTEGER+Schema.CAMMA+
                        STATUS +Schema.TYPE_INTEGER+Schema.CAMMA+
                        SEEN_TIME +Schema.TYPE_INTEGER+

                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] MEMBER_PROJECTION=new String[]
                {
                        _ID,
                        MODULEID,
                        SEEN_TIME,
                        SCHOOL_CODE,
                        SECTION_CODE,
                        CLASS_CODE,
                        CREATE_DATE,
                        SYNC_DATE,
                        REGDID,
                        STATUS,
                        START_TIME,
                        END_TIME,
                        SEEN_TIME
                };


    }

    public  static class ProgressTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_PROGRESS_LOG).
                build();
        public static final String TABLE_NAME = "ProgressTable";


        public static final String KEY_ID = "id";
        public static final String KEY_REG_ID = "_reg_id";
        public static final String KEY_ROW_ID = "_rowid";
        public static final String KEY_MODULE_ID = "_moduleId";
        public static final String KEY_CLASS_ID = "_classId";
        public static final String KEY_SECTION_ID= "_sectionId";
        public static final String KEY_SCHOOL_ID= "_schoolId";

        public static final String KEY_MODULE_TYPE="_moduleType";
        public static final String KEY_SUBJECT_ID = "subjectId";
        public static final String KEY_CHAPTER_ID = "chapterId";
        public static final String KEY_INSERT_DATE_TIME = "insertDateTime";



       public static String CREATE_PROGRESS_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"  +
                KEY_MODULE_TYPE + " TEXT,"+
                KEY_MODULE_ID + " INTEGER,"+
                KEY_SUBJECT_ID + " TEXT,"
                + KEY_CHAPTER_ID + " INTEGER," +
                KEY_REG_ID + " INTEGER," +
                KEY_ROW_ID + " INTEGER, " +
                KEY_CLASS_ID + " INTEGER," +
               KEY_SCHOOL_ID + " INTEGER," +
                KEY_INSERT_DATE_TIME + " INTEGER," +
                KEY_SECTION_ID + "  TEXT " + ")";

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] MEMBER_PROJECTION=new String[]
                {

                        KEY_ID,
                        KEY_MODULE_TYPE,
                        KEY_MODULE_ID,
                        KEY_SUBJECT_ID,
                        KEY_CHAPTER_ID,
                        KEY_REG_ID,
                        KEY_ROW_ID,
                        KEY_CLASS_ID,
                        KEY_SCHOOL_ID,
                        KEY_INSERT_DATE_TIME,
                        KEY_SECTION_ID
                };


    }


    public  static class ActivityTimeTable implements BaseColumns
    {
        public static final Uri CONTENT_URI = Schema.BASE_CONTENT_URI.buildUpon().
                appendPath( Schema.PATH_ACTIVITY_LOG).
                build();
        public static final String TABLE_NAME = "ActivityTimeTable";
        public static final String MODULEID = "MODULEID";
        public static final String REGDID = "REGDID";
        public static final String SEEN_TIME = "SEEN_TIME";
        public static final String START_TIME = "START_TIME";
        public static final String END_TIME = "END_TIME";
        public static final String SCHOOL_CODE = "SCHOOL_CODE";
        public static final String CLASS_CODE = "CLASS_CODE";
        public static final String SECTION_CODE = "SECTION_CODE";
        public static final String CREATE_DATE = "CREATE_DATE";
        public static final String SYNC_DATE = "SYNC_DATE";
        public static final String STATUS = "STATUS";



        public static final String SQL_CREATE_STATEMENT=
                Schema.CREATE_TABLE +TABLE_NAME + Schema.OPEN_BRACKET+

                        _ID +Schema.TYPE_INTEGER +Schema.PRIMARY_KEY+Schema.CAMMA+
                        MODULEID +Schema.TYPE_TEXT+Schema.CAMMA+
                        SCHOOL_CODE +Schema.TYPE_TEXT+Schema.CAMMA+
                        SECTION_CODE +Schema.TYPE_TEXT+Schema.CAMMA+
                        CLASS_CODE +Schema.TYPE_INTEGER+Schema.CAMMA+
                        CREATE_DATE +Schema.TYPE_INTEGER+Schema.CAMMA+
                        SYNC_DATE +Schema.TYPE_INTEGER+Schema.CAMMA+
                        START_TIME +Schema.TYPE_INTEGER+Schema.CAMMA+
                        END_TIME +Schema.TYPE_INTEGER+Schema.CAMMA+
                        REGDID +Schema.TYPE_INTEGER+Schema.CAMMA+
                        STATUS +Schema.TYPE_INTEGER+Schema.CAMMA+
                        SEEN_TIME +Schema.TYPE_INTEGER+

                        Schema.CLOSE_BRACKET;

        public static final String SQL_DELETE_STATEMENT=
                Schema.DROP_TABLE +TABLE_NAME ;
        public static final String[] MEMBER_PROJECTION=new String[]
                {
                        _ID,
                        MODULEID,
                        SEEN_TIME,
                        SCHOOL_CODE,
                        SECTION_CODE,
                        CLASS_CODE,
                        CREATE_DATE,
                        SYNC_DATE,
                        REGDID,
                        STATUS,
                        SEEN_TIME,
                        START_TIME,
                        END_TIME
                };


    }




}
