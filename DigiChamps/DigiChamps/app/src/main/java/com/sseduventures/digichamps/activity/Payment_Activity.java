package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.SpotsDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;



public class Payment_Activity extends FormActivity {

    private ImageButton email_btn, sms_btn;
    private ImageView payment_success_image;
    private Button resend_btn, backhome_btn;
    private SpotsDialog dialog;
    private String order_id,user_id;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_payment_status);
        setModuleName(LogEventUtil.EVENT_FinalPayment_PAGE);
        logEvent(LogEventUtil.KEY_FinalPayment_PAGE,LogEventUtil.EVENT_FinalPayment_PAGE);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        Intent intent = getIntent();
        order_id = intent.getStringExtra("order_id");
        init();
        resend_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // resendEmail();
            }
        });

        backhome_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NewDashboardActivity.class));
                finish();

            }
        });

    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    protected void init() {
        email_btn = (ImageButton) findViewById(R.id.email_receipt_btn);
        sms_btn = (ImageButton) findViewById(R.id.sms_receipt_btn);
        resend_btn = (Button) findViewById(R.id.resend_receipt_btn);
        payment_success_image = (ImageView) findViewById(R.id.success_image);
        backhome_btn = (Button) findViewById(R.id.btn_back_home);

        dialog = new SpotsDialog(this, getResources().getString(R.string.slide_1_descr), R.style.Custom);



    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(),NewDashboardActivity.class));

        finish();
    }






}
