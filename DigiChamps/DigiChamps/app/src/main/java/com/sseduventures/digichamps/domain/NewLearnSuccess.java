package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NISHIKANT on 7/20/2018.
 */

public class NewLearnSuccess implements Parcelable {


    private boolean Is_Video;
    private String Mentorid;
    private int PreviousRating;
    private int Boardid;
    private int classid;
    private String schoolid;
    private String asignteacherid;
    private String sectionid;
    private int roleid;
    private String isSectionEnabled;
    private boolean Is_School;
    private boolean Is_Doubt;
    private boolean Is_Mentor;
    private boolean Is_Dictionary;
    private boolean Is_Feed;
    private List<NewLearnSubjectlists> Subjectlists;
    private List<NewLearnDiyModuleLists> DiyModuleLists;
    private List<NewLearnRecentwatchedvideos> Recentwatchedvideos;
    private List<NewLearnBannerList> BannerList;

    public boolean isIs_Video() {
        return Is_Video;
    }

    public void setIs_Video(boolean is_Video) {
        Is_Video = is_Video;
    }

    public String getMentorid() {
        return Mentorid;
    }

    public void setMentorid(String mentorid) {
        Mentorid = mentorid;
    }

    public int getPreviousRating() {
        return PreviousRating;
    }

    public void setPreviousRating(int previousRating) {
        PreviousRating = previousRating;
    }

    public int getBoardid() {
        return Boardid;
    }

    public void setBoardid(int boardid) {
        Boardid = boardid;
    }

    public int getClassid() {
        return classid;
    }

    public void setClassid(int classid) {
        this.classid = classid;
    }

    public String getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(String schoolid) {
        this.schoolid = schoolid;
    }

    public String getAsignteacherid() {
        return asignteacherid;
    }

    public void setAsignteacherid(String asignteacherid) {
        this.asignteacherid = asignteacherid;
    }

    public String getSectionid() {
        return sectionid;
    }

    public void setSectionid(String sectionid) {
        this.sectionid = sectionid;
    }

    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    public String getIsSectionEnabled() {
        return isSectionEnabled;
    }

    public void setIsSectionEnabled(String isSectionEnabled) {
        this.isSectionEnabled = isSectionEnabled;
    }

    public boolean isIs_School() {
        return Is_School;
    }

    public void setIs_School(boolean is_School) {
        Is_School = is_School;
    }

    public boolean isIs_Doubt() {
        return Is_Doubt;
    }

    public void setIs_Doubt(boolean is_Doubt) {
        Is_Doubt = is_Doubt;
    }

    public boolean isIs_Mentor() {
        return Is_Mentor;
    }

    public void setIs_Mentor(boolean is_Mentor) {
        Is_Mentor = is_Mentor;
    }

    public boolean isIs_Dictionary() {
        return Is_Dictionary;
    }

    public void setIs_Dictionary(boolean is_Dictionary) {
        Is_Dictionary = is_Dictionary;
    }

    public boolean isIs_Feed() {
        return Is_Feed;
    }

    public void setIs_Feed(boolean is_Feed) {
        Is_Feed = is_Feed;
    }

    public List<NewLearnSubjectlists> getSubjectlists() {
        return Subjectlists;
    }

    public void setSubjectlists(List<NewLearnSubjectlists> subjectlists) {
        Subjectlists = subjectlists;
    }

    public List<NewLearnDiyModuleLists> getDiyModuleLists() {
        return DiyModuleLists;
    }

    public void setDiyModuleLists(List<NewLearnDiyModuleLists> diyModuleLists) {
        DiyModuleLists = diyModuleLists;
    }

    public List<NewLearnRecentwatchedvideos> getRecentwatchedvideos() {
        return Recentwatchedvideos;
    }

    public void setRecentwatchedvideos(List<NewLearnRecentwatchedvideos> recentwatchedvideos) {
        Recentwatchedvideos = recentwatchedvideos;
    }

    public List<NewLearnBannerList> getBannerList() {
        return BannerList;
    }

    public void setBannerList(List<NewLearnBannerList> bannerList) {
        BannerList = bannerList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.Is_Video ? (byte) 1 : (byte) 0);
        dest.writeString(this.Mentorid);
        dest.writeInt(this.PreviousRating);
        dest.writeInt(this.Boardid);
        dest.writeInt(this.classid);
        dest.writeString(this.schoolid);
        dest.writeString(this.asignteacherid);
        dest.writeString(this.sectionid);
        dest.writeInt(this.roleid);
        dest.writeString(this.isSectionEnabled);
        dest.writeByte(this.Is_School ? (byte) 1 : (byte) 0);
        dest.writeByte(this.Is_Doubt ? (byte) 1 : (byte) 0);
        dest.writeByte(this.Is_Mentor ? (byte) 1 : (byte) 0);
        dest.writeByte(this.Is_Dictionary ? (byte) 1 : (byte) 0);
        dest.writeByte(this.Is_Feed ? (byte) 1 : (byte) 0);
        dest.writeList(this.Subjectlists);
        dest.writeList(this.DiyModuleLists);
        dest.writeList(this.Recentwatchedvideos);
        dest.writeList(this.BannerList);
    }

    public NewLearnSuccess() {
    }

    protected NewLearnSuccess(Parcel in) {
        this.Is_Video = in.readByte() != 0;
        this.Mentorid = in.readString();
        this.PreviousRating = in.readInt();
        this.Boardid = in.readInt();
        this.classid = in.readInt();
        this.schoolid = in.readString();
        this.asignteacherid = in.readString();
        this.sectionid = in.readString();
        this.roleid = in.readInt();
        this.isSectionEnabled = in.readString();
        this.Is_School = in.readByte() != 0;
        this.Is_Doubt = in.readByte() != 0;
        this.Is_Mentor = in.readByte() != 0;
        this.Is_Dictionary = in.readByte() != 0;
        this.Is_Feed = in.readByte() != 0;
        this.Subjectlists = new ArrayList<NewLearnSubjectlists>();
        in.readList(this.Subjectlists, NewLearnSubjectlists.class.getClassLoader());
        this.DiyModuleLists = new ArrayList<NewLearnDiyModuleLists>();
        in.readList(this.DiyModuleLists, NewLearnDiyModuleLists.class.getClassLoader());
        this.Recentwatchedvideos = new ArrayList<NewLearnRecentwatchedvideos>();
        in.readList(this.Recentwatchedvideos, NewLearnRecentwatchedvideos.class.getClassLoader());
        this.BannerList = new ArrayList<NewLearnBannerList>();
        in.readList(this.BannerList, NewLearnBannerList.class.getClassLoader());
    }

    public static final Creator<NewLearnSuccess> CREATOR = new Creator<NewLearnSuccess>() {
        @Override
        public NewLearnSuccess createFromParcel(Parcel source) {
            return new NewLearnSuccess(source);
        }

        @Override
        public NewLearnSuccess[] newArray(int size) {
            return new NewLearnSuccess[size];
        }
    };
}
