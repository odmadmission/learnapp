package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class CheckoutDataModel implements Parcelable {

    protected CheckoutDataModel(Parcel in) {
    }

    public static final Creator<CheckoutDataModel> CREATOR = new Creator<CheckoutDataModel>() {
        @Override
        public CheckoutDataModel createFromParcel(Parcel in) {
            return new CheckoutDataModel(in);
        }

        @Override
        public CheckoutDataModel[] newArray(int size) {
            return new CheckoutDataModel[size];
        }
    };

    public CheckoutSuccess getSuccess() {
        return success;
    }

    public void setSuccess(CheckoutSuccess success) {
        this.success = success;
    }

    private CheckoutSuccess success;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public class CheckoutSuccess implements Parcelable {

        protected CheckoutSuccess(Parcel in) {
        }

        public final Creator<CheckoutSuccess> CREATOR = new Creator<CheckoutSuccess>() {
            @Override
            public CheckoutSuccess createFromParcel(Parcel in) {
                return new CheckoutSuccess(in);
            }

            @Override
            public CheckoutSuccess[] newArray(int size) {
                return new CheckoutSuccess[size];
            }
        };

        public GetCheckoutData getGetcheckoutdata() {
            return getcheckoutdata;
        }

        public void setGetcheckoutdata(GetCheckoutData getcheckoutdata) {
            this.getcheckoutdata = getcheckoutdata;
        }

        private GetCheckoutData getcheckoutdata;


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
        }
    }

    public class GetCheckoutData implements Parcelable {

        private ArrayList<CartData> cartdata = null;
        private Double Original_price;
        private Double discountprice;
        private Double discpercentage;

        protected GetCheckoutData(Parcel in) {
            if (in.readByte() == 0) {
                Original_price = null;
            } else {
                Original_price = in.readDouble();
            }
            if (in.readByte() == 0) {
                discountprice = null;
            } else {
                discountprice = in.readDouble();
            }
            if (in.readByte() == 0) {
                discpercentage = null;
            } else {
                discpercentage = in.readDouble();
            }
            if (in.readByte() == 0) {
                payblamt = null;
            } else {
                payblamt = in.readDouble();
            }
            if (in.readByte() == 0) {
                taxx = null;
            } else {
                taxx = in.readDouble();
            }
            Customer_Name = in.readString();
            Mobile_Number = in.readString();
            Email_Id = in.readString();
            coupon_code = in.readString();
            cart_item = in.readInt();
            Order_Id = in.readInt();
            Order_No = in.readString();
            if (in.readByte() == 0) {
                afterdicount = null;
            } else {
                afterdicount = in.readDouble();
            }
            msg = in.readString();
        }

        public final Creator<GetCheckoutData> CREATOR = new Creator<GetCheckoutData>() {
            @Override
            public GetCheckoutData createFromParcel(Parcel in) {
                return new GetCheckoutData(in);
            }

            @Override
            public GetCheckoutData[] newArray(int size) {
                return new GetCheckoutData[size];
            }
        };

        public ArrayList<CartData> getCartdata() {
            return cartdata;
        }

        public void setCartdata(ArrayList<CartData> cartdata) {
            this.cartdata = cartdata;
        }

        public Double getOriginal_price() {
            return Original_price;
        }

        public void setOriginal_price(Double original_price) {
            Original_price = original_price;
        }

        public Double getDiscountprice() {
            return discountprice;
        }

        public void setDiscountprice(Double discountprice) {
            this.discountprice = discountprice;
        }

        public Double getDiscpercentage() {
            return discpercentage;
        }

        public void setDiscpercentage(Double discpercentage) {
            this.discpercentage = discpercentage;
        }

        public Double getPayblamt() {
            return payblamt;
        }

        public void setPayblamt(Double payblamt) {
            this.payblamt = payblamt;
        }

        public Double getTaxx() {
            return taxx;
        }

        public void setTaxx(Double taxx) {
            this.taxx = taxx;
        }

        public String getCustomer_Name() {
            return Customer_Name;
        }

        public void setCustomer_Name(String customer_Name) {
            Customer_Name = customer_Name;
        }

        public String getMobile_Number() {
            return Mobile_Number;
        }

        public void setMobile_Number(String mobile_Number) {
            Mobile_Number = mobile_Number;
        }

        public String getEmail_Id() {
            return Email_Id;
        }

        public void setEmail_Id(String email_Id) {
            Email_Id = email_Id;
        }

        public String getCoupon_code() {
            return coupon_code;
        }

        public void setCoupon_code(String coupon_code) {
            this.coupon_code = coupon_code;
        }

        public int getCart_item() {
            return cart_item;
        }

        public void setCart_item(int cart_item) {
            this.cart_item = cart_item;
        }

        public int getOrder_Id() {
            return Order_Id;
        }

        public void setOrder_Id(int order_Id) {
            Order_Id = order_Id;
        }

        public String getOrder_No() {
            return Order_No;
        }

        public void setOrder_No(String order_No) {
            Order_No = order_No;
        }

        public Double getAfterdicount() {
            return afterdicount;
        }

        public void setAfterdicount(Double afterdicount) {
            this.afterdicount = afterdicount;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        private Double payblamt;
        private Double taxx;
        private String Customer_Name;
        private String Mobile_Number;
        private String Email_Id;
        private String coupon_code;
        private int cart_item;
        private int Order_Id;
        private String Order_No;
        private Double afterdicount;
        private String msg;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (Original_price == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(Original_price);
            }
            if (discountprice == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(discountprice);
            }
            if (discpercentage == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(discpercentage);
            }
            if (payblamt == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(payblamt);
            }
            if (taxx == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(taxx);
            }
            dest.writeString(Customer_Name);
            dest.writeString(Mobile_Number);
            dest.writeString(Email_Id);
            dest.writeString(coupon_code);
            dest.writeInt(cart_item);
            dest.writeInt(Order_Id);
            dest.writeString(Order_No);
            if (afterdicount == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(afterdicount);
            }
            dest.writeString(msg);
        }
    }

    public class CartData implements Parcelable {

        private String Package_Image;
        private String Package_Name;
        private int Validity;
        private int Subscripttion_Limit;
        private int Package_ID;
        private Double Price;
        private Double Discounted_Price;

        protected CartData(Parcel in) {
            Package_Name = in.readString();
            Validity = in.readInt();
            Subscripttion_Limit = in.readInt();
            Package_ID = in.readInt();
            if (in.readByte() == 0) {
                Price = null;
            } else {
                Price = in.readDouble();
            }
            if (in.readByte() == 0) {
                Discounted_Price = null;
            } else {
                Discounted_Price = in.readDouble();
            }
            if (in.readByte() == 0) {
                Package_Price = null;
            } else {
                Package_Price = in.readDouble();
            }
            byte tmpIs_Offline = in.readByte();
            Is_Offline = tmpIs_Offline == 0 ? null : tmpIs_Offline == 1;
            Cart_ID = in.readInt();
            Order_Id = in.readInt();
            Order_No = in.readString();
            if (in.readByte() == 0) {
                discountpercentage = null;
            } else {
                discountpercentage = in.readDouble();
            }
        }

        public final Creator<CartData> CREATOR = new Creator<CartData>() {
            @Override
            public CartData createFromParcel(Parcel in) {
                return new CartData(in);
            }

            @Override
            public CartData[] newArray(int size) {
                return new CartData[size];
            }
        };

        public String getPackage_Image() {
            return Package_Image;
        }

        public void setPackage_Image(String package_Image) {
            Package_Image = package_Image;
        }

        public String getPackage_Name() {
            return Package_Name;
        }

        public void setPackage_Name(String package_Name) {
            Package_Name = package_Name;
        }

        public int getValidity() {
            return Validity;
        }

        public void setValidity(int validity) {
            Validity = validity;
        }

        public int getSubscripttion_Limit() {
            return Subscripttion_Limit;
        }

        public void setSubscripttion_Limit(int subscripttion_Limit) {
            Subscripttion_Limit = subscripttion_Limit;
        }

        public int getPackage_ID() {
            return Package_ID;
        }

        public void setPackage_ID(int package_ID) {
            Package_ID = package_ID;
        }

        public Double getPrice() {
            return Price;
        }

        public void setPrice(Double price) {
            Price = price;
        }

        public Double getDiscounted_Price() {
            return Discounted_Price;
        }

        public void setDiscounted_Price(Double discounted_Price) {
            Discounted_Price = discounted_Price;
        }

        public Double getPackage_Price() {
            return Package_Price;
        }

        public void setPackage_Price(Double package_Price) {
            Package_Price = package_Price;
        }

        public Boolean getIs_Offline() {
            return Is_Offline;
        }

        public void setIs_Offline(Boolean is_Offline) {
            Is_Offline = is_Offline;
        }

        public int getCart_ID() {
            return Cart_ID;
        }

        public void setCart_ID(int cart_ID) {
            Cart_ID = cart_ID;
        }

        public int getOrder_Id() {
            return Order_Id;
        }

        public void setOrder_Id(int order_Id) {
            Order_Id = order_Id;
        }

        public String getOrder_No() {
            return Order_No;
        }

        public void setOrder_No(String order_No) {
            Order_No = order_No;
        }

        public Double getDiscountpercentage() {
            return discountpercentage;
        }

        public void setDiscountpercentage(Double discountpercentage) {
            this.discountpercentage = discountpercentage;
        }

        private Double Package_Price;
        private Boolean Is_Offline;
        private int Cart_ID;
        private int Order_Id;
        private String Order_No;
        private Double discountpercentage;


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(Package_Name);
            dest.writeInt(Validity);
            dest.writeInt(Subscripttion_Limit);
            dest.writeInt(Package_ID);
            if (Price == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(Price);
            }
            if (Discounted_Price == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(Discounted_Price);
            }
            if (Package_Price == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(Package_Price);
            }
            dest.writeByte((byte) (Is_Offline == null ? 0 : Is_Offline ? 1 : 2));
            dest.writeInt(Cart_ID);
            dest.writeInt(Order_Id);
            dest.writeString(Order_No);
            if (discountpercentage == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(discountpercentage);
            }
        }
    }
}
