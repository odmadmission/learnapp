package com.sseduventures.digichamps.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.helper.PickerBuilder;
import com.sseduventures.digichamps.utils.Profile_utility;
import com.sseduventures.digichamps.utils.Utility;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.sseduventures.digichamps.utils.Profile_utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;
import static com.sseduventures.digichamps.utils.Profile_utility.REQUEST_STORAGE_READ_ACCESS_PERMISSION;

/**
 * Created by ntspl22 on 5/29/2017.
 */
@SuppressLint("ValidFragment")
public class Signup_Header extends Fragment {

    private ImageView profile_image;
    private TextView img_error;
    private ImageButton camera_icon;
    private Uri uri;


    private String userChoosenTask;
    private static final int REQUEST_SELECT_PICTURE = 0x01;


    private File outPutFile = null;

    public TextView ImageError;
    String fbpic;
    boolean check = false;

    Uri file;
    @SuppressLint("ValidFragment")
    public Signup_Header(String fbpic, boolean check) {
        this.fbpic = fbpic;
        this.check = check;
    }

    public Signup_Header(){}

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.signup_header, container,false);
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        profile_image = (ImageView)view.findViewById(R.id.signup_image);
        ImageError = (TextView) view.findViewById(R.id.text_error);
        camera_icon = (ImageButton)view.findViewById(R.id.camera_btn) ;



        if(check == true){
            Glide.with(getActivity())
                    .load(fbpic)
                    .into(profile_image);
        }else {
            profile_image.setImageResource(R.drawable.boy);
        }




        camera_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageError.setText("");
                selectImage();
            }
        });

        return view;

    }
    /**
     * Callback received when a permissions request has been completed.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void pickFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), REQUEST_SELECT_PICTURE);
        }
    }




    private void selectImage() {
        final CharSequence[] items = {"Take Photo" , "Choose from Library"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //builder.setTitle("Add Photo!");
        builder.setTitle( Html.fromHtml("<font color='#05ab9a'>Add Photo</font>"));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Profile_utility.checkPermission(getActivity());

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        new PickerBuilder(getActivity(), PickerBuilder.SELECT_FROM_CAMERA)
                                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                                    @Override
                                    public void onImageReceived(Uri imageUri) {
                                        try{
                                            uri = imageUri;
                                            Bitmap bitmap = Utility.handleSamplingAndRotationBitmap(getActivity(),imageUri);
                                            profile_image.setBackgroundResource(0);
                                            profile_image.setImageBitmap(bitmap);
                                            ImageError.setText("");
                                        } catch(Exception e){
                                            Toast.makeText(getActivity(),"Please insert photo again",Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                })
                                .withTimeStamp(false)
                                .setCropScreenColor(Color.GREEN)
                                .start();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        new PickerBuilder(getActivity(), PickerBuilder.SELECT_FROM_GALLERY)
                                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                                    @Override
                                    public void onImageReceived(Uri imageUri) {
                                        try{
                                            uri = imageUri;
                                            Bitmap bitmap = Utility.handleSamplingAndRotationBitmap(getActivity(),imageUri);
                                            profile_image.setBackgroundResource(0);
                                            profile_image.setImageBitmap(bitmap);
                                            ImageError.setText("");
                                        } catch(Exception e){
                                            Toast.makeText(getActivity(),"Please insert photo again",Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setCropScreenColor(Color.GREEN)
                                .setOnPermissionRefusedListener(new PickerBuilder.onPermissionRefusedListener() {
                                    @Override
                                    public void onPermissionRefused() {
                                        Toast.makeText(getActivity(),"This permision is necessary",Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .start();

                } else {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }







    // for setting image to image view
    public Bitmap getImage()
    {
        return    ((BitmapDrawable) profile_image.getDrawable()).getBitmap();
    }


}
