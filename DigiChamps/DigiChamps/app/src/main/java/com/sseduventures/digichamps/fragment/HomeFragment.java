package com.sseduventures.digichamps.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.utils.FontManage;


/**
 * Created by user on 06-12-2017.
 */

public class HomeFragment extends Fragment {

    Context mContext;
    int backMe = 1;
    TextView hello,nowhave,schoolupdate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_home, container, false);
        mContext = getActivity();
        if(getActivity() instanceof FormActivity)
        {
            ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_difficulty_basis_frag);
            ((FormActivity)getActivity()).logEvent(LogEventUtil.KEY_difficulty_basis_frag,
                    LogEventUtil.EVENT_difficulty_basis_frag);
        }
        hello = view.findViewById(R.id.hello);
        nowhave = view.findViewById(R.id.nowhave);
        schoolupdate = view.findViewById(R.id.schoolupdate);

        FontManage.setFontHomeBold(mContext,nowhave);
        FontManage.setFontHomeBold(mContext,schoolupdate);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                    backMe();
                    return true;
                } else {
                    return false;
                }
            }
        });

        return view;
    }

    public void backMe(){


        Intent intent = new Intent(getActivity(), NewDashboardActivity.class);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();


    }

}
