package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.sseduventures.digichamps.Model.HomeWorkList;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.HomeworkActivity;
import com.sseduventures.digichamps.activity.PackageActivityNew;
import com.sseduventures.digichamps.domain.HomeworkList;

import java.util.ArrayList;
import java.util.List;



public class CardviewHomework extends RecyclerView.Adapter<CardviewHomework.DataObjectHolder> {

    Context context;
    private List<HomeworkList> list = new ArrayList<>();

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView tv_subject,tv_homework;
        LinearLayout container;

        public DataObjectHolder(View itemView) {
            super(itemView);
            tv_subject = (TextView) itemView.findViewById(R.id.tv_subject);
            tv_homework = (TextView) itemView.findViewById(R.id.tv_homework);
            container = (LinearLayout) itemView.findViewById(R.id.lnr_container);
        }
    }

    public CardviewHomework(List<HomeworkList> getlist, Context context) {
        this.list = getlist;
        this.context = context;
    }

    @Override
    public CardviewHomework.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_homework, parent, false);

        CardviewHomework.DataObjectHolder dataObjectHolder = new CardviewHomework.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final CardviewHomework.DataObjectHolder holder, final int position) {
      holder.tv_subject.setText(list.get(position).getSubject());
      holder.tv_homework.setText(list.get(position).getHomework());

      holder.container.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              //Toast.makeText(context, "Clicked Ok", Toast.LENGTH_SHORT).show();

              ((HomeworkActivity) context).AddressDialogTwo(list.get(position).getHomework());

          }
      });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}

