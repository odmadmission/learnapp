package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/20/2018.
 */

public class NewLearnResponse implements Parcelable {

    private NewLearnSuccess success;

    public NewLearnSuccess getSuccess() {
        return success;
    }

    public void setSuccess(NewLearnSuccess success) {
        this.success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.success, flags);
    }

    public NewLearnResponse() {
    }

    protected NewLearnResponse(Parcel in) {
        this.success = in.readParcelable(NewLearnSuccess.class.getClassLoader());
    }

    public static final Creator<NewLearnResponse> CREATOR = new Creator<NewLearnResponse>() {
        @Override
        public NewLearnResponse createFromParcel(Parcel source) {
            return new NewLearnResponse(source);
        }

        @Override
        public NewLearnResponse[] newArray(int size) {
            return new NewLearnResponse[size];
        }
    };
}
