package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.NewDashboardActivity;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;



public class FeedbackSuccessActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageView closeBtn, toolbarBackBtn;
    private TextView tv1, tv2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);


        setContentView(R.layout.activity_feedback_submit);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


        init();

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(FeedbackSuccessActivity.this, NewDashboardActivity.class));

                finish();


            }
        });

        toolbarBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(FeedbackSuccessActivity.this, NewDashboardActivity.class));

                finish();


            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void init(){

        toolbar = (Toolbar)findViewById(R.id.toolbar_feedback);
        closeBtn = (ImageView)findViewById(R.id.closeButton);
        toolbarBackBtn = (ImageView)findViewById(R.id.feedback_submit_backBtn);
        tv1 = (TextView)findViewById(R.id.tv1);
        tv2 = (TextView)findViewById(R.id.tv2);

    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(getApplicationContext(), NewDashboardActivity.class));

        finish();

    }

}
