package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 9/5/2018.
 */

public class BookmarkQuestionList implements Parcelable {

    private int Module_Id;
    private String ModuleName;
    private String Module_Title;
    private String Module_Image;
    private String Description;
    private String Question;
    private String Is_Expire;
    private String Is_Free;
    private String Is_Avail;

    protected BookmarkQuestionList(Parcel in) {
        Module_Id = in.readInt();
        ModuleName = in.readString();
        Module_Title = in.readString();
        Module_Image = in.readString();
        Description = in.readString();
        Question = in.readString();
        Is_Expire = in.readString();
        Is_Free = in.readString();
        Is_Avail = in.readString();
    }

    public static final Creator<BookmarkQuestionList> CREATOR = new Creator<BookmarkQuestionList>() {
        @Override
        public BookmarkQuestionList createFromParcel(Parcel in) {
            return new BookmarkQuestionList(in);
        }

        @Override
        public BookmarkQuestionList[] newArray(int size) {
            return new BookmarkQuestionList[size];
        }
    };

    public int getModule_Id() {
        return Module_Id;
    }

    public void setModule_Id(int module_Id) {
        Module_Id = module_Id;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public void setModuleName(String moduleName) {
        ModuleName = moduleName;
    }

    public String getModule_Title() {
        return Module_Title;
    }

    public void setModule_Title(String module_Title) {
        Module_Title = module_Title;
    }

    public String getModule_Image() {
        return Module_Image;
    }

    public void setModule_Image(String module_Image) {
        Module_Image = module_Image;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getIs_Expire() {
        return Is_Expire;
    }

    public void setIs_Expire(String is_Expire) {
        Is_Expire = is_Expire;
    }

    public String getIs_Free() {
        return Is_Free;
    }

    public void setIs_Free(String is_Free) {
        Is_Free = is_Free;
    }

    public String getIs_Avail() {
        return Is_Avail;
    }

    public void setIs_Avail(String is_Avail) {
        Is_Avail = is_Avail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(Module_Id);
        parcel.writeString(ModuleName);
        parcel.writeString(Module_Title);
        parcel.writeString(Module_Image);
        parcel.writeString(Description);
        parcel.writeString(Question);
        parcel.writeString(Is_Expire);
        parcel.writeString(Is_Free);
        parcel.writeString(Is_Avail);
    }
}
