package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewNotificationActivity;
import com.sseduventures.digichamps.activities.PersonalMentorActivity;
import com.sseduventures.digichamps.domain.TestHighlightsResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.CircularSeekBar;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Created by RKB on 6/23/2017.
 */

public class  TestHighlight_Activity extends FormActivity implements ServiceReceiver.Receiver {


    private String user_id;
    private SpotsDialog dialog;
    private Toolbar toolbar;
    private String result_id, exam_id;
    private TextView score, total_score,correct, incorrect, skip,attempted_id,correct_id,rank,performance;
    private CircularSeekBar circle_progress_view;
    private RelativeLayout relMaster1123,highlightsRltv;
    private RelativeLayout pmButton;
    private TextView prog_text_leader;
    private Button leader_btn,ansReview_btn,btnStats;
    private CoordinatorLayout coordinatorLayout;
    String chapIdid;
    String module_typetype;
    String subJectId;
    String subNameName;
    String flagNameName;
    String  navigation;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private TestHighlightsResponse success;
    private NestedScrollView nsvLay;
    private ShimmerFrameLayout mShimmerViewContainer;


    private CollapsingToolbarLayout collapsing_toolbar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_test_highlights_new);
        setModuleName(LogEventUtil.EVENT_test_highlights);

        logEvent(LogEventUtil.KEY_TestAnalytics_PAGE,
                LogEventUtil.EVENT_TestAnalytics_PAGE);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        prog_text_leader = (TextView) findViewById(R.id.prog_text_leader);

        Intent intent_res = getIntent();
        String activity = intent_res.getStringExtra("activity");
         navigation = intent_res.getStringExtra("navigation");


         chapIdid = intent_res.getStringExtra("chapter_id");
         module_typetype = intent_res.getStringExtra("module_type");
         subJectId = intent_res.getStringExtra("subject_id");
         subNameName = intent_res.getStringExtra("subName");
         flagNameName = intent_res.getStringExtra("flagName");

        if (activity.equalsIgnoreCase("LeaderBoard")) {
            result_id = intent_res.getStringExtra("Result_ID");

        } else {
            result_id = intent_res.getStringExtra("Result_ID");
        }
        if (activity.equalsIgnoreCase("LeaderBoard")) {
            exam_id = intent_res.getStringExtra("exam_id");

        } else {
            exam_id = intent_res.getStringExtra("exam_id");
        }


        init();


        leader_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestHighlight_Activity.this,
                        Activity_LeaderBoard.class);
                intent.
                        putExtra("exam_id", exam_id).
                        putExtra("Result_ID", result_id)
                .putExtra("chapter_id",chapIdid)
                        .putExtra("module_type",module_typetype)
                .putExtra("subName",subNameName)
                .putExtra("subject_id",subJectId)
                        .putExtra("navigation",navigation)
                .putExtra("flagName",flagNameName);
                startActivity(intent);

            }
        });

        ansReview_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestHighlight_Activity.this,
                        ReviewAnswers.class);
                intent.putExtra("Result_ID", result_id)
                        .putExtra("chapter_id",chapIdid)
                        .putExtra("module_type",module_typetype)
                        .putExtra("subName",subNameName)
                        .putExtra("subject_id",subJectId)
                        .putExtra("navigation",navigation)
                .putExtra("exam_id", exam_id)
                        .putExtra("flagName",flagNameName);
                startActivity(intent);

            }
        });

        btnStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestHighlight_Activity.this,
                        TestStatisticsActivity.class);
                intent.putExtra("Result_ID", result_id)
                        .putExtra("chapter_id",chapIdid)
                        .putExtra("module_type",module_typetype)
                        .putExtra("subName",subNameName)
                        .putExtra("subject_id",subJectId)
                        .putExtra("navigation",navigation)
                        .putExtra("exam_id", exam_id)
                        .putExtra("flagName",flagNameName);
                startActivity(intent);

            }
        });




        pmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(TestHighlight_Activity.this,PersonalMentorActivity.class);
                startActivity(in);

            }
        });


        if (AppUtil.isInternetConnected(this)) {

            getResultNew();

        } else {
            Intent intent = new Intent(TestHighlight_Activity.this, Internet_Activity.class);
            startActivity(intent);


        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void init() {

        mHandler=new Handler();
        mServiceReceiver=new ServiceReceiver(mHandler);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        nsvLay = findViewById(R.id.nsvLay);
        nsvLay.setVisibility(View.INVISIBLE);
        highlightsRltv = findViewById(R.id.highlightsRltv);
        highlightsRltv.setVisibility(View.VISIBLE);
        score = (TextView) findViewById(R.id.score);
        total_score = (TextView) findViewById(R.id.total_score);
        correct = (TextView) findViewById(R.id.correct);
        incorrect = (TextView) findViewById(R.id.incorrect);
        skip = (TextView) findViewById(R.id.skip);
        attempted_id  = (TextView) findViewById(R.id.attempted_id);
        correct_id = (TextView) findViewById(R.id.correct_id);

        collapsing_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        circle_progress_view = (CircularSeekBar) findViewById(R.id.circle_progress_view);

        circle_progress_view.getProgress();
        circle_progress_view.setMax(100);
        circle_progress_view.setPointerHaloColor(R.color.white);

        rank  = (TextView) findViewById(R.id.rank);
        performance = (TextView) findViewById(R.id.txtdfasd);
        relMaster1123 = (RelativeLayout) findViewById(R.id.relMaster1123);
        pmButton = (RelativeLayout) findViewById(R.id.rltv_pm);
        pmButton.setVisibility(View.GONE);



        dialog = new SpotsDialog(this, getResources().getString(R.string.dialog_text), R.style.Custom);


        toolbar = (Toolbar) findViewById(R.id.result_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(navigation==null) {
                    Intent in = new Intent(
                            TestHighlight_Activity.this,
                            Chapter_Details.class);
                    in.putExtra("chapter_id", chapIdid);
                    in.putExtra("subject_name", subNameName);
                    in.putExtra("subject_id", subJectId);
                    in.putExtra("flagNewKey", flagNameName);
                    startActivity(in);

                }
                else
                {
                    Intent in = new Intent(
                            TestHighlight_Activity.this,
                            NewNotificationActivity.class);
                    in.putExtra("chapter_id", chapIdid);
                    in.putExtra("subject_name", subNameName);
                    in.putExtra("subject_id", subJectId);
                    in.putExtra("flagNewKey", flagNameName);
                    startActivity(in);

                }
                finish();
            }
        });

        toolbar.setTitle("Test Analysis");
        collapsing_toolbar.setTitle("Test Analysis");
        leader_btn = (Button) findViewById(R.id.leader_btn);
        ansReview_btn = (Button) findViewById(R.id.btnSoln);
        btnStats = (Button) findViewById(R.id.btnStats);


        // get services
        user_id = String.valueOf(RegPrefManager.getInstance(this).getRegId());
        SharedPreferences preferencePaper = getSharedPreferences("paper_zone", MODE_PRIVATE);
        String mentorId = preferencePaper.getString("Mentorid",null);
        if(mentorId.equals(null) || mentorId.equalsIgnoreCase("null")){
            relMaster1123.setVisibility(View.GONE);
        }else{
            relMaster1123.setVisibility(View.GONE);
        }

    }

    @Override
    public void onBackPressed() {
        if(navigation==null) {
            Intent in = new Intent(
                    TestHighlight_Activity.this,
                    Chapter_Details.class);
            in.putExtra("chapter_id", chapIdid);
            in.putExtra("subject_name", subNameName);
            in.putExtra("subject_id", subJectId);
            in.putExtra("flagNewKey", flagNameName);
            startActivity(in);
            finish();
        }
        else
        {
            Intent in = new Intent(
                    TestHighlight_Activity.this,
                    NewNotificationActivity.class);
            in.putExtra("chapter_id", chapIdid);
            in.putExtra("subject_name", subNameName);
            in.putExtra("subject_id", subJectId);
            in.putExtra("flagNewKey", flagNameName);
            startActivity(in);
            finish();
        }
    }

    private void getResultNew(){

        int resultId = 0;
        if(result_id!=null)
        {
            resultId=Integer.parseInt(result_id);
        }
        Bundle bundle = new Bundle();
        bundle.putInt("Result_ID",resultId);
        if(AppUtil.isInternetConnected(this)){

            NetworkService.startActionGetTestHighlights(this,mServiceReceiver,bundle);
        }else{
            Intent in = new Intent(TestHighlight_Activity.this,Internet_Activity.class);
            startActivity(in);
        }
    }



    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        mShimmerViewContainer.stopShimmer();
        //mShimmerViewContainer.setVisibility(View.GONE);
        highlightsRltv.setVisibility(View.GONE);
        nsvLay.setVisibility(View.VISIBLE);

        switch (resultCode)
        {
            case ResponseCodes.SUCCESS:
                success=resultData.getParcelable(IntentHelper.RESULT_DATA);

                if(success!=null&&success.getSuccess()!=null
                        ){
                    score.setText(success.getSuccess().getTotal_Correct_Ans()+"");
                    total_score.setText(success.getSuccess().getQuestion_Nos()+"");
                    correct.setText(success.getSuccess().getTotal_Correct_Ans()+"");
                    incorrect.setText(success.getSuccess().getTotal_Incorrec_Ans()+"");
                    skip.setText(success.getSuccess().getUnanswered()+"");
                    attempted_id.setText(success.getSuccess().getQuestion_Attempted()+"");
                    correct_id.setText(success.getSuccess().getTotal_Correct_Ans()+"");

                    circle_progress_view.setProgress(success.getSuccess().getAccuracy());
                    circle_progress_view.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
                            circle_progress_view.setProgress(success.getSuccess().getAccuracy());
                        }

                        @Override
                        public void onStopTrackingTouch(CircularSeekBar seekBar) {
                            circle_progress_view.setProgress(success.getSuccess().getAccuracy());
                        }

                        @Override
                        public void onStartTrackingTouch(CircularSeekBar seekBar) {
                            circle_progress_view.setProgress(success.getSuccess().getAccuracy());
                        }


                    });
                    prog_text_leader.setText(success.getSuccess().getAccuracy()+"%");
                    rank.setText("Rank "+success.getSuccess().getRank());


                    if(success.getSuccess().getPerformance() == 0){
                        performance.setText("Very Poor");
                    }else if(success.getSuccess().getPerformance() == 1){

                        performance.setText("Poor");

                    }else if(success.getSuccess().getPerformance() == 2){

                        performance.setText("Below Average");

                    }else if(success.getSuccess().getPerformance() == 3){

                        performance.setText("Good");

                    }else if(success.getSuccess().getPerformance() == 4){

                        performance.setText("Very Good");

                    }else if(success.getSuccess().getPerformance() == 5) {
                        performance.setText("Outstanding");
                    }else {
                        performance.setText("Not Found");
                    }


                }
                else
                {
                    Toast.makeText(this, "working", Toast.LENGTH_SHORT).show();
                }
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(TestHighlight_Activity.this,ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(TestHighlight_Activity.this,ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(TestHighlight_Activity.this,Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
    }

}
