package com.sseduventures.digichamps.helper;

/**
 * Created by NTSPL-19 on 7/8/2017.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.widget.Toast;

public class SmsReceiver extends BroadcastReceiver {
    private static SmsListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = intent.getExtras();
        SmsMessage[] smsMessages = null;

        if (bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            smsMessages = new SmsMessage[pdus.length];

            for(int i=0;i<smsMessages.length;i++) {
                smsMessages[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                String sender = smsMessages[i].getOriginatingAddress();
                if(sender.contains("RM-WAYSMS") || sender.contains("rm-waysms")) {
                    String message = smsMessages[i].getMessageBody();
                    String otp = extractOTP(message);
                    if(otp != null) {
                        Toast.makeText(context, otp, Toast.LENGTH_LONG).show();
                    }
                }
            }
        }

    }


    public String extractOTP(String sms) {
        String[] nbs = sms.split("\\D+");
        if (nbs.length != 0) {
            for (String number : nbs) {
                if (number.matches("^[0-9]+$")) {
                    return number;
                }
            }
        }
        return null;
    }

    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }

}