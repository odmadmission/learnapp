package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/17/2018.
 */

public class BoardClassName implements Parcelable {

    private int classId;
    private String boardId;
    private String className;
    private String insertedDate;
    private String insertedBy;
    private String modifiedDate;
    private String modifiedBy;
    private String active;
    private String deleted;


    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getInsertedDate() {
        return insertedDate;
    }

    public void setInsertedDate(String insertedDate) {
        this.insertedDate = insertedDate;
    }

    public String getInsertedBy() {
        return insertedBy;
    }

    public void setInsertedBy(String insertedBy) {
        this.insertedBy = insertedBy;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.classId);
        dest.writeString(this.boardId);
        dest.writeString(this.className);
        dest.writeString(this.insertedDate);
        dest.writeString(this.insertedBy);
        dest.writeString(this.modifiedDate);
        dest.writeString(this.modifiedBy);
        dest.writeString(this.active);
        dest.writeString(this.deleted);
    }

    public BoardClassName() {
    }

    protected BoardClassName(Parcel in) {
        this.classId = in.readInt();
        this.boardId = in.readString();
        this.className = in.readString();
        this.insertedDate = in.readString();
        this.insertedBy = in.readString();
        this.modifiedDate = in.readString();
        this.modifiedBy = in.readString();
        this.active = in.readString();
        this.deleted = in.readString();
    }

    public static final Creator<BoardClassName> CREATOR = new Creator<BoardClassName>() {
        @Override
        public BoardClassName createFromParcel(Parcel source) {
            return new BoardClassName(source);
        }

        @Override
        public BoardClassName[] newArray(int size) {
            return new BoardClassName[size];
        }
    };
}
