package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class Order implements Parcelable
{

    private int Order_ID;
    private String order_date;
    private String Order_No;
    private int Regd_ID;
    private double tax_Amt;
    private double Total;
    private int Count;
    private String Discount;
    private double Grand_Total;

    public int getOrder_ID() {
        return Order_ID;
    }

    public void setOrder_ID(int order_ID) {
        Order_ID = order_ID;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_No() {
        return Order_No;
    }

    public void setOrder_No(String order_No) {
        Order_No = order_No;
    }

    public int getRegd_ID() {
        return Regd_ID;
    }

    public void setRegd_ID(int regd_ID) {
        Regd_ID = regd_ID;
    }

    public double getTax_Amt() {
        return tax_Amt;
    }

    public void setTax_Amt(double tax_Amt) {
        this.tax_Amt = tax_Amt;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(double total) {
        Total = total;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public double getGrand_Total() {
        return Grand_Total;
    }

    public void setGrand_Total(int grand_Total) {
        Grand_Total = grand_Total;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Order_ID);
        dest.writeString(this.order_date);
        dest.writeString(this.Order_No);
        dest.writeInt(this.Regd_ID);
        dest.writeDouble(this.tax_Amt);
        dest.writeDouble(this.Total);
        dest.writeInt(this.Count);
        dest.writeString(this.Discount);
        dest.writeDouble(this.Grand_Total);
    }

    public Order() {
    }

    protected Order(Parcel in) {
        this.Order_ID = in.readInt();
        this.order_date = in.readString();
        this.Order_No = in.readString();
        this.Regd_ID = in.readInt();
        this.tax_Amt = in.readInt();
        this.Total = in.readInt();
        this.Count = in.readInt();
        this.Discount = in.readString();
        this.Grand_Total = in.readInt();
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel source) {
            return new Order(source);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };
}