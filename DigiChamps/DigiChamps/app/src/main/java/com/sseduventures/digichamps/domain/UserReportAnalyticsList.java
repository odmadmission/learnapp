package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class UserReportAnalyticsList implements Parcelable{



        protected UserReportAnalyticsList(Parcel in) {
        }

        public final Creator<UserReportAnalyticsList> CREATOR = new Creator<UserReportAnalyticsList>() {
            @Override
            public UserReportAnalyticsList createFromParcel(Parcel in) {
                return new UserReportAnalyticsList(in);
            }

            @Override
            public UserReportAnalyticsList[] newArray(int size) {
                return new UserReportAnalyticsList[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
        }


    }