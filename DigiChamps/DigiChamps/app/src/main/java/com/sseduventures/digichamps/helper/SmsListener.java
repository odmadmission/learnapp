package com.sseduventures.digichamps.helper;

/**
 * Created by NTSPL-19 on 7/8/2017.
 */
public interface SmsListener {
    public void messageReceived(String messageText);
}