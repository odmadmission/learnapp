package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 9/1/2018.
 */

public class OrderConf implements Parcelable{

    private String Order_Id;

    protected OrderConf(Parcel in) {
        Order_Id = in.readString();
    }

    public static final Creator<OrderConf> CREATOR = new Creator<OrderConf>() {
        @Override
        public OrderConf createFromParcel(Parcel in) {
            return new OrderConf(in);
        }

        @Override
        public OrderConf[] newArray(int size) {
            return new OrderConf[size];
        }
    };

    public String getOrder_Id() {
        return Order_Id;
    }

    public void setOrder_Id(String order_Id) {
        Order_Id = order_Id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Order_Id);
    }
}
