package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.PersistableBundle;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.domain.TestHighlightsResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ScoreActivity extends FormActivity implements ServiceReceiver.Receiver {

    private ImageView score_back_image;
    private SpotsDialog dialog;
    String resp, error;
    String user_id, TAG = "SCORE", result_id, exam_id;
    Button analysis_btn;
    int ratting;
    int i = 0;
    TextView txt_total_question, txt_correct_ans, txt_ques_attempted, txt_total_mark, txt_score_total;
    String chapIdid;
    String module_typetype;
    String subJectId;
    String subNameName;
    String flagNameName;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private TestHighlightsResponse success;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_score_new2);
        setModuleName(LogEventUtil.EVENT_Score_page);
        logEvent(LogEventUtil.KEY_Score_page,LogEventUtil.EVENT_Score_page);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        txt_total_question = (TextView) findViewById(R.id.txt_total_question);
        txt_ques_attempted = (TextView) findViewById(R.id.txt_ques_attempted);
        txt_correct_ans = (TextView) findViewById(R.id.txt_correct_ans);
        txt_total_mark = (TextView) findViewById(R.id.txt_total_mark);
        txt_score_total = (TextView) findViewById(R.id.txt_score_total);
        analysis_btn = (Button) findViewById(R.id.analysis_btn);
        score_back_image = (ImageView) findViewById(R.id.score_back_image);
        dialog = new SpotsDialog(this, getResources().getString(R.string.dialog_text), R.style.Custom);

        Intent intent_res = getIntent();
        String activity = intent_res.getStringExtra("activity");

        chapIdid = intent_res.getStringExtra("chapter_id");
        module_typetype = intent_res.getStringExtra("module_type");
        subJectId = intent_res.getStringExtra("subject_id");
        subNameName = intent_res.getStringExtra("subName");
        flagNameName = intent_res.getStringExtra("flagName");


        if (activity.equalsIgnoreCase("LeaderBoard")) {
            result_id = intent_res.getStringExtra("Result_ID");

        } else {
            result_id = intent_res.getStringExtra("Result_ID");
        }
        if (activity.equalsIgnoreCase("LeaderBoard")) {
            exam_id = intent_res.getStringExtra("exam_id");

        } else {
            exam_id = intent_res.getStringExtra("exam_id");
        }

        user_id = String.valueOf(RegPrefManager.getInstance(this).getRegId());
        SharedPreferences preferencea = getApplicationContext().getSharedPreferences("paper_zone", getApplicationContext().MODE_PRIVATE);
        ratting = preferencea.getInt("previousRating", 0);

        if (AppUtil.isInternetConnected(this)) {

            getResultNew();

        } else {
            Intent inten = new Intent(ScoreActivity.this, Internet_Activity.class);
            startActivity(inten);


        }


        analysis_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ScoreActivity.this, TestHighlight_Activity.class);
                i.putExtra("Result_ID", result_id);
                i.putExtra("exam_id", exam_id);
                i.putExtra("activity", "NewExamActivity");
                i.putExtra("chapter_id", chapIdid);
                i.putExtra("module_type", module_typetype);
                i.putExtra("subject_id", subJectId);
                i.putExtra("subName", subNameName);
                i.putExtra("flagName", flagNameName);


                startActivity(i);
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
            }
        });

        score_back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ScoreActivity.this, Chapter_Details.class);
                in.putExtra("chapter_id", chapIdid);
                in.putExtra("subject_name", subNameName);
                in.putExtra("subject_id", subJectId);
                in.putExtra("flagNewKey", flagNameName);
                startActivity(in);

                finish();
            }
        });

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        Intent in = new Intent(ScoreActivity.this,
                Chapter_Details.class);
        in.putExtra("chapter_id", chapIdid);
        in.putExtra("subject_name", subNameName);
        in.putExtra("subject_id", subJectId);
        in.putExtra("flagNewKey", flagNameName);
        startActivity(in);

        finish();

    }


    private void getResultNew() {

        int resultId = 0;
        if (result_id != null) {
            resultId = Integer.parseInt(result_id);
        }
        Bundle bundle = new Bundle();
        bundle.putInt("Result_ID", resultId);
        if (AppUtil.isInternetConnected(this)) {

            showDialog(null, "Please wait...");
            NetworkService.startActionGetTestHighlights(this, mServiceReceiver, bundle);
        } else {
            Intent in = new Intent(ScoreActivity.this, Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if (success != null && success.getSuccess() != null
                        ) {

                    txt_total_question.setText(success.getSuccess().getQuestion_Nos() + "");
                    txt_ques_attempted.setText(success.getSuccess().getQuestion_Attempted() + "");
                    txt_correct_ans.setText(success.getSuccess().getTotal_Correct_Ans() + "");
                    txt_total_mark.setText(success.getSuccess().getQuestion_Nos() + "");
                    txt_score_total.setText(success.getSuccess().getTotal_Correct_Ans() + "");

                } else {

                }
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(ScoreActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(ScoreActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(ScoreActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }


    @Override
    protected void onStop() {
        hideDialog();
        super.onStop();
        mServiceReceiver.setReceiver(null);

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        hideDialog();
        super.onSaveInstanceState(outState, outPersistentState);
    }
}
