package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/25/2018.
 */

public class PMResponse implements Parcelable {

    private PmSuccess SuccessresultTask;

    public PmSuccess getSuccessresultTask() {
        return SuccessresultTask;
    }

    public void setSuccessresultTask(PmSuccess successresultTask) {
        SuccessresultTask = successresultTask;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.SuccessresultTask, flags);
    }

    public PMResponse() {
    }

    protected PMResponse(Parcel in) {
        this.SuccessresultTask = in.readParcelable(PmSuccess.class.getClassLoader());
    }

    public static final Creator<PMResponse> CREATOR = new Creator<PMResponse>() {
        @Override
        public PMResponse createFromParcel(Parcel source) {
            return new PMResponse(source);
        }

        @Override
        public PMResponse[] newArray(int size) {
            return new PMResponse[size];
        }
    };
}
