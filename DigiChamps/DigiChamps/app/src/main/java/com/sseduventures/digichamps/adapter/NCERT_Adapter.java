package com.sseduventures.digichamps.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.domain.NcertSolutionListData;

import java.util.ArrayList;
import java.util.List;


public class NCERT_Adapter extends RecyclerView.Adapter<NCERT_Adapter.MyLearnViewHolder> {


    public static ArrayList<NcertSolutionListData> dataSet = new ArrayList<>();

    public static int position;
    static CardView container, container1;
    String sub_name;
    View view;
    private Context context;
    private Typeface tf;

    public NCERT_Adapter(ArrayList<NcertSolutionListData> data, String sub_name, Context context) {
        this.context = context;
        this.dataSet = data;
        this.sub_name = sub_name;
        tf = Typeface.createFromAsset(context.getAssets(),"fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");

    }


    @Override
    public MyLearnViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_ncert, parent, false);
        MyLearnViewHolder myViewHolder = new MyLearnViewHolder(view);

        return myViewHolder;
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final MyLearnViewHolder holder, final int listPosition) {
        TextView chap_names = holder.chap_names;
        chap_names.setText(dataSet.get(holder.getAdapterPosition()).getNCERTSolutions_PDFName());
        container1 = holder.container;

        holder.container.setOnClickListener(onClickListener(listPosition));


    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }
        public void filterList(ArrayList<NcertSolutionListData> filteredList) {
            dataSet = filteredList;
            notifyDataSetChanged();
        }


    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String PDF_URL = dataSet.get(position).getPDFOnline();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(PDF_URL));
                context.startActivity(browserIntent);

            }
        };
    }

    public static class MyLearnViewHolder extends RecyclerView.ViewHolder {
        public TextView chap_names;
        CardView container;

        public MyLearnViewHolder(View itemView) {
            super(itemView);
            container = (CardView) itemView.findViewById(R.id.card_view_ncert);
            this.chap_names = (TextView) itemView.findViewById(R.id.chap_text);


        }
    }


}

