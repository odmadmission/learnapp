package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class AssignedTeacherSuccess implements Parcelable{

    private String AssignDetail;
    private String Message;
    private int ResultCount;
    private List<AssignedTeacherDetails> AssignDetailList;

    public String getAssignDetail() {
        return AssignDetail;
    }

    public void setAssignDetail(String assignDetail) {
        AssignDetail = assignDetail;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getResultCount() {
        return ResultCount;
    }

    public void setResultCount(int resultCount) {
        ResultCount = resultCount;
    }

    public List<AssignedTeacherDetails> getAssignDetailList() {
        return AssignDetailList;
    }

    public void setAssignDetailList(List<AssignedTeacherDetails> assignDetailList) {
        AssignDetailList = assignDetailList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.AssignDetail);
        dest.writeString(this.Message);
        dest.writeInt(this.ResultCount);
        dest.writeTypedList(this.AssignDetailList);
    }

    public AssignedTeacherSuccess() {
    }

    protected AssignedTeacherSuccess(Parcel in) {
        this.AssignDetail = in.readString();
        this.Message = in.readString();
        this.ResultCount = in.readInt();
        this.AssignDetailList = in.createTypedArrayList(AssignedTeacherDetails.CREATOR);
    }

    public static final Creator<AssignedTeacherSuccess> CREATOR = new Creator<AssignedTeacherSuccess>() {
        @Override
        public AssignedTeacherSuccess createFromParcel(Parcel source) {
            return new AssignedTeacherSuccess(source);
        }

        @Override
        public AssignedTeacherSuccess[] newArray(int size) {
            return new AssignedTeacherSuccess[size];
        }
    };
}
