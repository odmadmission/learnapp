package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class TestAnalysisAnalyticsList implements Parcelable{
        private int TotalExamsAppeared;
        private int AverageMarks;
        private int Accuracy;
        private int SubjectId;
        private String Subject;

        public int getTotalExamsAppeared() {
            return TotalExamsAppeared;
        }

        public void setTotalExamsAppeared(int totalExamsAppeared) {
            TotalExamsAppeared = totalExamsAppeared;
        }

        public int getAverageMarks() {
            return AverageMarks;
        }

        public void setAverageMarks(int averageMarks) {
            AverageMarks = averageMarks;
        }

        public int getAccuracy() {
            return Accuracy;
        }

        public void setAccuracy(int accuracy) {
            Accuracy = accuracy;
        }

        public int getSubjectId() {
            return SubjectId;
        }

        public void setSubjectId(int subjectId) {
            SubjectId = subjectId;
        }

        public String getSubject() {
            return Subject;
        }

        public void setSubject(String subject) {
            Subject = subject;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.TotalExamsAppeared);
            dest.writeInt(this.AverageMarks);
            dest.writeInt(this.Accuracy);
            dest.writeInt(this.SubjectId);
            dest.writeString(this.Subject);
        }

        public TestAnalysisAnalyticsList() {
        }

        protected TestAnalysisAnalyticsList(Parcel in) {
            this.TotalExamsAppeared = in.readInt();
            this.AverageMarks = in.readInt();
            this.Accuracy = in.readInt();
            this.SubjectId = in.readInt();
            this.Subject = in.readString();
        }

        public final Creator<TestAnalysisAnalyticsList> CREATOR = new Creator<TestAnalysisAnalyticsList>() {
            @Override
            public TestAnalysisAnalyticsList createFromParcel(Parcel source) {
                return new TestAnalysisAnalyticsList(source);
            }

            @Override
            public TestAnalysisAnalyticsList[] newArray(int size) {
                return new TestAnalysisAnalyticsList[size];
            }
        };
    }