package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/4/2018.
 */

public class TestHighlightsSubconcept implements Parcelable {

    private int SubConceptId;
    private String SubConceptName;
    private int TotalQuestions;
    private int TotalCorrect;
    private int TotalInCorrect;
    private int TotalSkipped;
    private int Accuracy;


    public int getSubConceptId() {
        return SubConceptId;
    }

    public void setSubConceptId(int subConceptId) {
        SubConceptId = subConceptId;
    }

    public String getSubConceptName() {
        return SubConceptName;
    }

    public void setSubConceptName(String subConceptName) {
        SubConceptName = subConceptName;
    }

    public int getTotalQuestions() {
        return TotalQuestions;
    }

    public void setTotalQuestions(int totalQuestions) {
        TotalQuestions = totalQuestions;
    }

    public int getTotalCorrect() {
        return TotalCorrect;
    }

    public void setTotalCorrect(int totalCorrect) {
        TotalCorrect = totalCorrect;
    }

    public int getTotalInCorrect() {
        return TotalInCorrect;
    }

    public void setTotalInCorrect(int totalInCorrect) {
        TotalInCorrect = totalInCorrect;
    }

    public int getTotalSkipped() {
        return TotalSkipped;
    }

    public void setTotalSkipped(int totalSkipped) {
        TotalSkipped = totalSkipped;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.SubConceptId);
        dest.writeString(this.SubConceptName);
        dest.writeInt(this.TotalQuestions);
        dest.writeInt(this.TotalCorrect);
        dest.writeInt(this.TotalInCorrect);
        dest.writeInt(this.TotalSkipped);
        dest.writeInt(this.Accuracy);
    }

    public TestHighlightsSubconcept() {
    }

    protected TestHighlightsSubconcept(Parcel in) {
        this.SubConceptId = in.readInt();
        this.SubConceptName = in.readString();
        this.TotalQuestions = in.readInt();
        this.TotalCorrect = in.readInt();
        this.TotalInCorrect = in.readInt();
        this.TotalSkipped = in.readInt();
        this.Accuracy = in.readInt();
    }

    public static final Creator<TestHighlightsSubconcept> CREATOR = new Creator<TestHighlightsSubconcept>() {
        @Override
        public TestHighlightsSubconcept createFromParcel(Parcel source) {
            return new TestHighlightsSubconcept(source);
        }

        @Override
        public TestHighlightsSubconcept[] newArray(int size) {
            return new TestHighlightsSubconcept[size];
        }
    };
}
