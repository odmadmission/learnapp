package com.sseduventures.digichamps.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.adapter.Package_Adapter;
import com.sseduventures.digichamps.domain.PackageModel;

import java.util.ArrayList;

public class BottomSheetFragment extends BottomSheetDialogFragment {


    private Button mButton1;
    private String TAG = BottomSheetFragment.class.getSimpleName();


    private RecyclerView recyclerView;
    private Package_Adapter adapter;
    private ArrayList<PackageModel> list;
    private RelativeLayout re11new;
    private ShimmerFrameLayout mShimmerViewContainer;

    public static BottomSheetFragment newInstance(ArrayList<PackageModel> u, int p, int discount) {
        // Required empty public constructor

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("data", u);
        bundle.putInt("index", p);
        bundle.putInt("discount", discount);
        BottomSheetFragment bt = new BottomSheetFragment();
        bt.setArguments(bundle);

        return bt;
    }

    public BottomSheetFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.layout_package_bottom, container, false);


        //for recycler view
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),
                OrientationHelper.VERTICAL, false);
        re11new = (RelativeLayout) view.findViewById(R.id.re11new);

        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        list = getArguments().getParcelableArrayList("data");

        final ArrayList<PackageModel> fList = new ArrayList<>();

        switch (getArguments().getInt("index")) {
            case 0:
                if (list.size() > 0) {
                    fList.add(list.get(0));
                    re11new.setBackgroundColor(getResources().
                            getColor(R.color.bottomone));

                }
                break;
            case 1:
                if (list.size() > 1) {
                    fList.add(list.get(1));
                    re11new.setBackgroundColor(getResources().
                            getColor(R.color.bottomtwo));
                }
                break;
            case 2:
                if (list.size() > 2) {
                    fList.add(list.get(2));
                    fList.add(list.get(3));
                    re11new.setBackgroundColor(getResources().
                            getColor(R.color.bottomthree));
                }
                break;
            case 3:
                if (list.size() > 3) {
                    fList.add(list.get(4));
                    fList.add(list.get(5));
                    fList.add(list.get(6));
                    fList.add(list.get(7));
                    re11new.setBackgroundColor(getResources().
                            getColor(R.color.bottomfour));
                }
                break;
        }

        Log.v(TAG, fList.size() + "");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                adapter = new Package_Adapter(fList, getActivity(),
                        getArguments().getInt("discount"));
                recyclerView.setAdapter(adapter);
            }
        },2000);
     /*   if(fList.size()>0) {

            mShimmerViewContainer.stopShimmer();
            mShimmerViewContainer.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter = new Package_Adapter(fList, getActivity(),
                    getArguments().getInt("discount"));
            recyclerView.setAdapter(adapter);
        }
        else {
            recyclerView.setVisibility(View.GONE);
        }*/
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

}