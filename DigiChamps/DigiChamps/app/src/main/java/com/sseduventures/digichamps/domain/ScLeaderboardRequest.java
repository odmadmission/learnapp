package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class ScLeaderboardRequest implements Parcelable{

    private String SchoolId;
    private int ClassId;


    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public int getClassId() {
        return ClassId;
    }

    public void setClassId(int classId) {
        ClassId = classId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.SchoolId);
        dest.writeInt(this.ClassId);
    }

    public ScLeaderboardRequest() {
    }

    protected ScLeaderboardRequest(Parcel in) {
        this.SchoolId = in.readString();
        this.ClassId = in.readInt();
    }

    public static final Creator<ScLeaderboardRequest> CREATOR = new Creator<ScLeaderboardRequest>() {
        @Override
        public ScLeaderboardRequest createFromParcel(Parcel source) {
            return new ScLeaderboardRequest(source);
        }

        @Override
        public ScLeaderboardRequest[] newArray(int size) {
            return new ScLeaderboardRequest[size];
        }
    };
}
