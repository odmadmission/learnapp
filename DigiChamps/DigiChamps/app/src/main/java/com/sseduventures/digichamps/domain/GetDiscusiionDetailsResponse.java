package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NISHIKANT on 7/13/2018.
 */

public class GetDiscusiionDetailsResponse implements Parcelable {

    private List<GetDiscussionDetailsList> DiscussionDetail;
    private String Message;
    private int DiscussionDetailID;
    private int ResultCount;

    public List<GetDiscussionDetailsList> getDiscussionDetail() {
        return DiscussionDetail;
    }

    public void setDiscussionDetail(List<GetDiscussionDetailsList> discussionDetail) {
        DiscussionDetail = discussionDetail;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getDiscussionDetailID() {
        return DiscussionDetailID;
    }

    public void setDiscussionDetailID(int discussionDetailID) {
        DiscussionDetailID = discussionDetailID;
    }

    public int getResultCount() {
        return ResultCount;
    }

    public void setResultCount(int resultCount) {
        ResultCount = resultCount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.DiscussionDetail);
        dest.writeString(this.Message);
        dest.writeInt(this.DiscussionDetailID);
        dest.writeInt(this.ResultCount);
    }

    public GetDiscusiionDetailsResponse() {
    }

    protected GetDiscusiionDetailsResponse(Parcel in) {
        this.DiscussionDetail = new ArrayList<GetDiscussionDetailsList>();
        in.readList(this.DiscussionDetail, GetDiscussionDetailsList.class.getClassLoader());
        this.Message = in.readString();
        this.DiscussionDetailID = in.readInt();
        this.ResultCount = in.readInt();
    }

    public static final Creator<GetDiscusiionDetailsResponse> CREATOR = new Creator<GetDiscusiionDetailsResponse>() {
        @Override
        public GetDiscusiionDetailsResponse createFromParcel(Parcel source) {
            return new GetDiscusiionDetailsResponse(source);
        }

        @Override
        public GetDiscusiionDetailsResponse[] newArray(int size) {
            return new GetDiscusiionDetailsResponse[size];
        }
    };
}
