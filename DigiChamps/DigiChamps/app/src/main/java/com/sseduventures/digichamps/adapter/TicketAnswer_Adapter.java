package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.DataModel_NotificationDetails;
import com.sseduventures.digichamps.Model.DataModel_TicketAnswers;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.DoubtActivity;
import com.sseduventures.digichamps.activity.FullImageActivity;
import com.sseduventures.digichamps.activity.TicketDetailsActivity;
import com.sseduventures.digichamps.utils.DateTimeUtil;

import java.util.List;

import jp.wasabeef.richeditor.RichEditor;



public class TicketAnswer_Adapter extends RecyclerView.Adapter<TicketAnswer_Adapter.MyTicketAnswerView> {

    private List<DataModel_TicketAnswers> ticketAnswersList;
    CardView container, container1;
    Context context;


    public static class MyTicketAnswerView extends RecyclerView.ViewHolder {

        TextView teacherName, ticket_Date, ticket_Time; //answerText;
        ImageView teacher_image, answerPhoto, left_image, right_image;
        CardView container;
        RichEditor answerText;


        public MyTicketAnswerView(View view) {
            super(view);



            answerText = (RichEditor) view.findViewById(R.id.text_answer);
            teacherName = (TextView) view.findViewById(R.id.text_name);
            ticket_Date = (TextView) view.findViewById(R.id.text_date);
            teacher_image = (ImageView) view.findViewById(R.id.teacher_image_photo);
            ticket_Time = (TextView) view.findViewById(R.id.text_time);
           answerPhoto = (ImageView) view.findViewById(R.id.answer_image);


            container = (CardView) view.findViewById(R.id.card_view_ticket);

        }
    }

    public TicketAnswer_Adapter(List<DataModel_TicketAnswers> ticketAnswersList, Context context) {
        this.ticketAnswersList = ticketAnswersList;
        this.context = context;
    }


    public void show_images(final Drawable images) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialogTheme);


        builder.setView(R.layout.alert_showimage);
        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        PhotoView photoView = (PhotoView) dialog.findViewById(R.id.photo_view);
        photoView.setImageDrawable(images);

        ImageView next = (ImageView) dialog.findViewById(R.id.bt_ok);
        if (next != null) {
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        } else {
            dialog.dismiss();
        }


        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.gravity = Gravity.CENTER_VERTICAL;
        window.setAttributes(wlp);
    }

    @Override
    public MyTicketAnswerView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_answerlist, parent, false);

        MyTicketAnswerView myTicketAnswerView = new MyTicketAnswerView(view);
        return myTicketAnswerView;
    }

    @Override
    public void onBindViewHolder(final MyTicketAnswerView holder, final int listPosition) {

        TextView teacher_Name = holder.teacherName;
        RichEditor answer_Text = holder.answerText;
        TextView ticket_date = holder.ticket_Date;
        TextView ticket_time = holder.ticket_Time;
        ImageView teacher_Image_Photo = holder.teacher_image;
        final ImageView answer_Photo = holder.answerPhoto;


        answer_Text.setFocusable(false);
        answer_Text.setClickable(false);
        answer_Text.setInputEnabled(false);
        answer_Text.setEnabled(false);

        teacher_Name.setText(ticketAnswersList.get(holder.getAdapterPosition()).getTeacherName());
        String reply_date=ticketAnswersList.get(holder.getAdapterPosition()).getDate();
        if(reply_date!=null&&reply_date.contains("-")){
            String year=reply_date.split("-")[0];
            String month=reply_date.split("-")[1];
            String date=reply_date.split("-")[2];
            String total_date=date+"/"+month+"/"+year;
            ticket_date.setText(total_date);
        }


        String reply_time=ticketAnswersList.get(holder.getAdapterPosition()).getTime();
        if(reply_time!=null&&reply_time.contains(":")){
            String hour=reply_time.split(":")[0];
            String minute=reply_time.split(":")[1];
            String final_time=hour+":"+minute;
            if(Integer.parseInt(hour)>12){
                ticket_time.setText(final_time +" PM");
            }
            if(Integer.parseInt(hour)==12) {
                ticket_time.setText(final_time+" PM");
            }
            if(Integer.parseInt(hour)<12){
                ticket_time.setText(final_time+" AM");
            }

        }


        Picasso.with(context).load(ticketAnswersList.get(listPosition).getTeacher_Image())
                .into(teacher_Image_Photo);

        answer_Text.setHtml(ticketAnswersList.get(holder.getAdapterPosition()).getAnswer());

        if (ticketAnswersList.get(listPosition).getAnswer_Photo().equalsIgnoreCase("")) {
            answer_Photo.setVisibility(View.GONE);
        } else {
            answer_Photo.setVisibility(View.VISIBLE);
            Picasso.with(context).
                    load(ticketAnswersList.get(listPosition).getAnswer_Photo()).
                    resize(50,50).
                    into(answer_Photo);
        }

        this.container1 = container;

        holder.container.setOnClickListener(onClickListener(listPosition));

        answer_Photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // show_images(answer_Photo.getDrawable());
                Intent intent = new Intent(context, FullImageActivity.class);
                intent.putExtra("url", ticketAnswersList.get(listPosition).getAnswer_Photo());
                intent.putExtra("activity", "FromDoubtDetails");
                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return ticketAnswersList.size();
    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        };
    }
}
