package com.sseduventures.digichamps.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sseduventures.digichamps.Model.DataModel_TestList;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.Exam_desc;
import com.sseduventures.digichamps.activity.TicketDetailsActivity;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.domain.DoubtListData;

import java.util.List;


public class DoubtListNewAdapter extends RecyclerView.Adapter<DoubtListNewAdapter.MyTestView> {
    private List<DoubtListData> testLists;
    CardView container, container1;
    Context context;
    public static int position;



    public static class MyTestView extends RecyclerView.ViewHolder {

        TextView txt_chap_details, txt_chap_name, status;
        RelativeLayout container;
        ImageView imv;

        public MyTestView(View view) {
            super(view);

            txt_chap_name = (TextView) view.findViewById(R.id.txt_chap_name);
            txt_chap_details = (TextView) view.findViewById(R.id.txt_chap_details);
            status = (TextView) view.findViewById(R.id.status);
            container = (RelativeLayout) view.findViewById(R.id.container);
            imv = view.findViewById(R.id.imv);

        }
    }

    public DoubtListNewAdapter(List<DoubtListData> testLists,
                               Context context) {
        this.testLists = testLists;
        this.context = context;

    }

    @Override
    public MyTestView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.doubt_list_adapter, parent, false);

        MyTestView myTestView = new MyTestView(view);
        return myTestView;
    }

    @Override
    public void onBindViewHolder(final MyTestView holder, final int listPosition) {
        position = listPosition;
        TextView txt_chap_details = holder.txt_chap_details;
        TextView txt_chap_name = holder.txt_chap_name;
        TextView status = holder.status;
        ImageView imv = holder.imv;


        txt_chap_details.setText(testLists.get(position).getQuestion());
        txt_chap_name.setText(testLists.get(position).getChapName());
        String stat = testLists.get(position).getStatus();
        String statusNew = null;


        if (testLists.get(position).getSubId() == 1025 || testLists.get(position).getSubId() == 1053) {
            imv.setBackgroundResource(R.drawable.ic_m_01);

        } else {
            imv.setBackgroundResource(R.drawable.ic_s_01);
        }
        if (stat.equalsIgnoreCase("O")) {
            if (testLists.get(position).isIs_Answred()) {
                statusNew = "Answered";
                status.setTextColor(context.getResources().getColor(R.color.green));
            } else {
                statusNew = "Unanswered";
                status.setTextColor(context.getResources().getColor(R.color.blue));
            }
        } else if (stat.equalsIgnoreCase("R")) {
            status.setTextColor(context.getResources().getColor(R.color.red));
        } else if (stat.equalsIgnoreCase("C")) {
            statusNew = "Solved";
            status.setTextColor(context.getResources().getColor(R.color.greenans));
        } else if (stat.equalsIgnoreCase("D")) {
            statusNew = "Overdue";
        }


        status.setText(statusNew);

        this.container1 = container;
        holder.container.setOnClickListener(onClickListener(position));

    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String ticketId = String.valueOf(testLists.get(position).getTicket_id());
                Intent in = new Intent(context, TicketDetailsActivity.class);
                in.putExtra("ticket_id",ticketId);
                in.putExtra("activity","Ticket_list");
                Activity activity = (Activity) context;
                activity.startActivity(in);

            }
        };
    }

    @Override
    public int getItemCount() {
        return testLists.size();
    }

}
