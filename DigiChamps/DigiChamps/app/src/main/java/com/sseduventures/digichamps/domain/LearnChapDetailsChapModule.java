package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/26/2018.
 */

public class LearnChapDetailsChapModule implements Parcelable {

    private Integer Module_Id;
    private String Module_Title;
    private String  Description;
    private String  Module_Image;
    private String  Image_Key;
    private Boolean Is_Avail;
    private String pdf_file;
    private String pdf_name;
    private Boolean Is_Free;
    private String Validity;
    private String Question_Pdf;
    private String Question_Pdf_Name;
    private Integer No_Of_Question;
    private Boolean Is_Free_Test;
    private String Media_Id;
    private String VideoKey;
    private String template_id;
    private String thumbnail_key;
    private Boolean is_Video_bookmarked;
    private Boolean is_question_bookmarked;
    private Boolean is_studynotebookmarked;
    private Boolean Is_Expire;

    public Integer getModule_Id() {
        return Module_Id;
    }

    public void setModule_Id(Integer module_Id) {
        Module_Id = module_Id;
    }

    public String getModule_Title() {
        return Module_Title;
    }

    public void setModule_Title(String module_Title) {
        Module_Title = module_Title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getModule_Image() {
        return Module_Image;
    }

    public void setModule_Image(String module_Image) {
        Module_Image = module_Image;
    }

    public String getImage_Key() {
        return Image_Key;
    }

    public void setImage_Key(String image_Key) {
        Image_Key = image_Key;
    }

    public Boolean getIs_Avail() {
        return Is_Avail;
    }

    public void setIs_Avail(Boolean is_Avail) {
        Is_Avail = is_Avail;
    }

    public String getPdf_file() {
        return pdf_file;
    }

    public void setPdf_file(String pdf_file) {
        this.pdf_file = pdf_file;
    }

    public String getPdf_name() {
        return pdf_name;
    }

    public void setPdf_name(String pdf_name) {
        this.pdf_name = pdf_name;
    }

    public Boolean getIs_Free() {
        return Is_Free;
    }

    public void setIs_Free(Boolean is_Free) {
        Is_Free = is_Free;
    }

    public String getValidity() {
        return Validity;
    }

    public void setValidity(String validity) {
        Validity = validity;
    }

    public String getQuestion_Pdf() {
        return Question_Pdf;
    }

    public void setQuestion_Pdf(String question_Pdf) {
        Question_Pdf = question_Pdf;
    }

    public String getQuestion_Pdf_Name() {
        return Question_Pdf_Name;
    }

    public void setQuestion_Pdf_Name(String question_Pdf_Name) {
        Question_Pdf_Name = question_Pdf_Name;
    }

    public Integer getNo_Of_Question() {
        return No_Of_Question;
    }

    public void setNo_Of_Question(Integer no_Of_Question) {
        No_Of_Question = no_Of_Question;
    }

    public Boolean getIs_Free_Test() {
        return Is_Free_Test;
    }

    public void setIs_Free_Test(Boolean is_Free_Test) {
        Is_Free_Test = is_Free_Test;
    }

    public String getMedia_Id() {
        return Media_Id;
    }

    public void setMedia_Id(String media_Id) {
        Media_Id = media_Id;
    }

    public String getVideoKey() {
        return VideoKey;
    }

    public void setVideoKey(String videoKey) {
        VideoKey = videoKey;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getThumbnail_key() {
        return thumbnail_key;
    }

    public void setThumbnail_key(String thumbnail_key) {
        this.thumbnail_key = thumbnail_key;
    }

    public Boolean getIs_Video_bookmarked() {
        return is_Video_bookmarked;
    }

    public void setIs_Video_bookmarked(Boolean is_Video_bookmarked) {
        this.is_Video_bookmarked = is_Video_bookmarked;
    }

    public Boolean getIs_question_bookmarked() {
        return is_question_bookmarked;
    }

    public void setIs_question_bookmarked(Boolean is_question_bookmarked) {
        this.is_question_bookmarked = is_question_bookmarked;
    }

    public Boolean getIs_studynotebookmarked() {
        return is_studynotebookmarked;
    }

    public void setIs_studynotebookmarked(Boolean is_studynotebookmarked) {
        this.is_studynotebookmarked = is_studynotebookmarked;
    }

    public Boolean getIs_Expire() {
        return Is_Expire;
    }

    public void setIs_Expire(Boolean is_Expire) {
        Is_Expire = is_Expire;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.Module_Id);
        dest.writeString(this.Module_Title);
        dest.writeString(this.Description);
        dest.writeString(this.Module_Image);
        dest.writeString(this.Image_Key);
        dest.writeValue(this.Is_Avail);
        dest.writeString(this.pdf_file);
        dest.writeString(this.pdf_name);
        dest.writeValue(this.Is_Free);
        dest.writeString(this.Validity);
        dest.writeString(this.Question_Pdf);
        dest.writeString(this.Question_Pdf_Name);
        dest.writeValue(this.No_Of_Question);
        dest.writeValue(this.Is_Free_Test);
        dest.writeString(this.Media_Id);
        dest.writeString(this.VideoKey);
        dest.writeString(this.template_id);
        dest.writeString(this.thumbnail_key);
        dest.writeValue(this.is_Video_bookmarked);
        dest.writeValue(this.is_question_bookmarked);
        dest.writeValue(this.is_studynotebookmarked);
        dest.writeValue(this.Is_Expire);
    }

    public LearnChapDetailsChapModule() {
    }

    protected LearnChapDetailsChapModule(Parcel in) {
        this.Module_Id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Module_Title = in.readString();
        this.Description = in.readString();
        this.Module_Image = in.readString();
        this.Image_Key = in.readString();
        this.Is_Avail = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.pdf_file = in.readString();
        this.pdf_name = in.readString();
        this.Is_Free = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.Validity = in.readString();
        this.Question_Pdf = in.readString();
        this.Question_Pdf_Name = in.readString();
        this.No_Of_Question = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Is_Free_Test = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.Media_Id = in.readString();
        this.VideoKey = in.readString();
        this.template_id = in.readString();
        this.thumbnail_key = in.readString();
        this.is_Video_bookmarked = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.is_question_bookmarked = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.is_studynotebookmarked = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.Is_Expire = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Creator<LearnChapDetailsChapModule> CREATOR = new Creator<LearnChapDetailsChapModule>() {
        @Override
        public LearnChapDetailsChapModule createFromParcel(Parcel source) {
            return new LearnChapDetailsChapModule(source);
        }

        @Override
        public LearnChapDetailsChapModule[] newArray(int size) {
            return new LearnChapDetailsChapModule[size];
        }
    };
}
