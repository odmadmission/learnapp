package com.sseduventures.digichamps.adapter;

import java.util.HashMap;
import java.util.List;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sseduventures.digichamps.R;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    private HashMap<String, List<String>> _listDataChild;
    ImageView expandButton;
    private Typeface tf;

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        tf = Typeface.createFromAsset(context.getAssets(), "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        expandButton.setImageResource(R.drawable.uparrow);
        final String childText = (String) getChild(groupPosition, childPosition);
        String[] separated = childText.split("\\*");
        String details = separated[0];
        String startDate = separated[1];
        String endDate = separated[2];

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_expand, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.expandedListItem);
        TextView txtStartDate = (TextView) convertView.findViewById(R.id.txt_start_date);
        TextView txtEndDtae = (TextView) convertView.findViewById(R.id.txt_deadline);
        TextView taskDetailsTxtHead = (TextView) convertView.findViewById(R.id.taskDetailsTxtHead);
        TextView deadlineTxtHead = (TextView) convertView.findViewById(R.id.deadlineTxtHead);
        TextView startDateHead = (TextView) convertView.findViewById(R.id.startDateHead);



        txtListChild.setText(details);
        txtStartDate.setText(startDate);
        txtEndDtae.setText(endDate);
        txtEndDtae.setTypeface(tf);
        txtStartDate.setTypeface(tf);
        txtListChild.setTypeface(tf);
        taskDetailsTxtHead.setTypeface(tf);
        deadlineTxtHead.setTypeface(tf);
        startDateHead.setTypeface(tf);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_expand, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.listTitle);
        expandButton= (ImageView) convertView.findViewById(R.id.image_arrow);
        int pos = groupPosition+1;
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(+pos + ". " + headerTitle);
        expandButton.setImageResource(R.drawable.expandbutton);
        lblListHeader.setTypeface(tf);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}