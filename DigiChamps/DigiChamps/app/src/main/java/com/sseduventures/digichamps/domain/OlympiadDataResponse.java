package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/3/2018.
 */

public class OlympiadDataResponse implements Parcelable{

private OlympiadDataSuccess Success;

    public OlympiadDataSuccess getSuccess() {
        return Success;
    }

    public void setSuccess(OlympiadDataSuccess success) {
        Success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.Success, flags);
    }

    public OlympiadDataResponse() {
    }

    protected OlympiadDataResponse(Parcel in) {
        this.Success = in.readParcelable(OlympiadDataSuccess.class.getClassLoader());
    }

    public static final Creator<OlympiadDataResponse> CREATOR = new Creator<OlympiadDataResponse>() {
        @Override
        public OlympiadDataResponse createFromParcel(Parcel source) {
            return new OlympiadDataResponse(source);
        }

        @Override
        public OlympiadDataResponse[] newArray(int size) {
            return new OlympiadDataResponse[size];
        }
    };
}
