package com.sseduventures.digichamps.Model;

/**
 * Created by khushbu on 1/4/2017.
 * This models class used for storing and getting data.
 */

public class board_spinner_model {

    String name;
    String id_;
    private boolean isSelected;


    public board_spinner_model(String name, String id_) {
        this.name = name;
        this.id_ = id_;

    }
    public board_spinner_model(String name) {
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id_;
    }
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
