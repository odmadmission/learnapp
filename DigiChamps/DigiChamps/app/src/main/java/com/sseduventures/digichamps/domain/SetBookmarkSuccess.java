package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 9/5/2018.
 */

public class SetBookmarkSuccess implements Parcelable{

    private String message;

    protected SetBookmarkSuccess(Parcel in) {
        message = in.readString();
    }

    public static final Creator<SetBookmarkSuccess> CREATOR = new Creator<SetBookmarkSuccess>() {
        @Override
        public SetBookmarkSuccess createFromParcel(Parcel in) {
            return new SetBookmarkSuccess(in);
        }

        @Override
        public SetBookmarkSuccess[] newArray(int size) {
            return new SetBookmarkSuccess[size];
        }
    };

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(message);
    }
}
