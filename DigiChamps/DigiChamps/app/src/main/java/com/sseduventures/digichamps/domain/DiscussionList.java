package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class DiscussionList implements Parcelable{

    private int DiscussionID;
    private String Title;
    private int RoleID;
    private int CreatedBy;
    private String CreatedDate;
    private boolean IsActive;
    private boolean IsDelete;
    private int SchoolID;

    public int getDiscussionID() {
        return DiscussionID;
    }

    public void setDiscussionID(int discussionID) {
        DiscussionID = discussionID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getRoleID() {
        return RoleID;
    }

    public void setRoleID(int roleID) {
        RoleID = roleID;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public boolean isDelete() {
        return IsDelete;
    }

    public void setDelete(boolean delete) {
        IsDelete = delete;
    }

    public int getSchoolID() {
        return SchoolID;
    }

    public void setSchoolID(int schoolID) {
        SchoolID = schoolID;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.DiscussionID);
        dest.writeString(this.Title);
        dest.writeInt(this.RoleID);
        dest.writeInt(this.CreatedBy);
        dest.writeString(this.CreatedDate);
        dest.writeByte(this.IsActive ? (byte) 1 : (byte) 0);
        dest.writeByte(this.IsDelete ? (byte) 1 : (byte) 0);
        dest.writeInt(this.SchoolID);
    }

    public DiscussionList() {
    }

    protected DiscussionList(Parcel in) {
        this.DiscussionID = in.readInt();
        this.Title = in.readString();
        this.RoleID = in.readInt();
        this.CreatedBy = in.readInt();
        this.CreatedDate = in.readString();
        this.IsActive = in.readByte() != 0;
        this.IsDelete = in.readByte() != 0;
        this.SchoolID = in.readInt();
    }

    public static final Creator<DiscussionList> CREATOR = new Creator<DiscussionList>() {
        @Override
        public DiscussionList createFromParcel(Parcel source) {
            return new DiscussionList(source);
        }

        @Override
        public DiscussionList[] newArray(int size) {
            return new DiscussionList[size];
        }
    };
}
