package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/11/2018.
 */

public class ExamScheduleDetailsRequest implements Parcelable{

    private String SchoolId;
    private int ClassId;
    private String ExamId;

    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public int getClassId() {
        return ClassId;
    }

    public void setClassId(int classId) {
        ClassId = classId;
    }

    public String getExamId() {
        return ExamId;
    }

    public void setExamId(String examId) {
        ExamId = examId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.SchoolId);
        dest.writeInt(this.ClassId);
        dest.writeString(this.ExamId);
    }

    public ExamScheduleDetailsRequest() {
    }

    protected ExamScheduleDetailsRequest(Parcel in) {
        this.SchoolId = in.readString();
        this.ClassId = in.readInt();
        this.ExamId = in.readString();
    }

    public static final Creator<ExamScheduleDetailsRequest> CREATOR = new Creator<ExamScheduleDetailsRequest>() {
        @Override
        public ExamScheduleDetailsRequest createFromParcel(Parcel source) {
            return new ExamScheduleDetailsRequest(source);
        }

        @Override
        public ExamScheduleDetailsRequest[] newArray(int size) {
            return new ExamScheduleDetailsRequest[size];
        }
    };
}
