package com.sseduventures.digichamps.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.Bookmarks_frag2_adapter;
import com.sseduventures.digichamps.domain.BookmarkStudyNotesList;
import com.sseduventures.digichamps.domain.BookmarkStudyNotesResponse;
import com.sseduventures.digichamps.domain.SetBookmarkResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;

/**
 * Created by khushbu on 12/30/2017.
 */

public class Fragment_Bookmarks2 extends android.support.v4.app.Fragment
implements ServiceReceiver.Receiver{
    private RecyclerView recyclerView;
    ArrayList<BookmarkStudyNotesList> list;
    private Bookmarks_frag2_adapter mAdapter;
    private String Module_Id,TAG = "Videos";
    SpotsDialog pDialog;
    private String sub_name;
    private ArrayList<String>pdfs_list;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String chapter_id,prtTest = "false";
    private long User_Id;
    private TextView no_study;

    private Handler mHandler;
    private ServiceReceiver mSericeReceiver;
    private ShimmerFrameLayout mShimmerViewContainer;


    private BookmarkStudyNotesResponse success;

    public Fragment_Bookmarks2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bookmarks_frag2, container, false);
        if(getActivity() instanceof FormActivity)
        {
            ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_bookmark_sn);
            ((FormActivity)getActivity()).logEvent(LogEventUtil.KEY_bookmark_sn,
                    LogEventUtil.EVENT_bookmark_sn);
        }
        init(view);

        return view;

    }
    private void init(View view) {

        mHandler = new Handler();
        mSericeReceiver = new ServiceReceiver(mHandler);

        final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.sn_recycler_view);
        recyclerView.setHasFixedSize(true);

        no_study = view.findViewById(R.id.no_study);

        no_study.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        //list
        list = new ArrayList<>();
        pdfs_list = new ArrayList<>();

        //Progress dialog initialisation
        pDialog = new SpotsDialog(getActivity(), "Fun+Education", R.style.Custom);
        mAdapter = new Bookmarks_frag2_adapter(list,sub_name,getActivity(),Fragment_Bookmarks2.this);
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);


    }

    public void setVisibility(){
        recyclerView.setVisibility(View.GONE);
        no_study.setVisibility(View.VISIBLE);

    }


    public void setUnsetBookMark(String dataid,String dataType){
        if (AppUtil.isInternetConnected(getActivity())){

            Bundle bun = new Bundle();
            bun.putString("dataType",dataType);
            bun.putString("dataId",dataid);

            NetworkService.startActionGetSetUsetBookmark(getActivity()
                    ,mSericeReceiver,bun);
        }else{
            Intent in = new Intent(getActivity(), Internet_Activity.class);
            startActivity(in);
        }
    }

    private void getBookmarkDetails(){
        if(AppUtil.isInternetConnected(getActivity())){

            NetworkService.startActionGetBookmarkStudyNotes(getActivity(),mSericeReceiver);


        }else{


            Intent in = new Intent(getActivity(),Internet_Activity.class);
            startActivity(in);

        }
    }

    @Override
    public void onStart() {

        mSericeReceiver.setReceiver(this);
        super.onStart();
    }


    @Override
    public void onStop() {
        mSericeReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        switch(resultCode){

            case ResponseCodes.SUCCESS:
                no_study.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);
               if(success!=null &&
                       success.getSuccessBookmarkedStudynote()!=null &&
                       success.getSuccessBookmarkedStudynote().getStudynoteList()!=null &&
                       success.getSuccessBookmarkedStudynote().getStudynoteList().size()>0){

                   new Handler().postDelayed(new Runnable() {
                       @Override
                       public void run() {
                           mShimmerViewContainer.stopShimmer();
                           mShimmerViewContainer.setVisibility(View.GONE);
                           list.clear();
                           list.addAll(success.getSuccessBookmarkedStudynote().getStudynoteList());
                           mAdapter.notifyDataSetChanged();

                       }
                   },1000);





               }


                break;

            case ResponseCodes.BOOKMARKED:
                SetBookmarkResponse successmm = resultData.getParcelable(IntentHelper.RESULT_DATA);
                Toast.makeText(getActivity(), ""+successmm.getSuccess().getMessage(), Toast.LENGTH_SHORT).show();
                break;
            case ResponseCodes.UNBOOKMARKED:
                SetBookmarkResponse successm =
                        resultData.getParcelable(IntentHelper.RESULT_DATA);

                ArrayList<BookmarkStudyNotesList> l=new ArrayList<>();
                for(int i=0;i<list.size();i++)
                {
                    String id=resultData.getString("dataId");

                    if(id.equals(String.valueOf(list.get(i).getModule_Id())))
                    {

                    }
                    else {
                        l.add(list.get(i));
                    }
                }
                Toast.makeText(getActivity(), ""+successm.getSuccess().getMessage(), Toast.LENGTH_SHORT).show();

                list.clear();
                list.addAll(l);
                mAdapter.notifyDataSetChanged();

                if(list.size()==0)
                {
                    no_study.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
                break;

            case ResponseCodes.NOLIST:
                no_study.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);

                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(),ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(),ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(),Internet_Activity.class);
                startActivity(intent);
                break;

            case ResponseCodes.SERVER_ERROR:
                break;


        }
    }

    @Override
    public void onResume() {
        getBookmarkDetails();
        super.onResume();
    }
}



