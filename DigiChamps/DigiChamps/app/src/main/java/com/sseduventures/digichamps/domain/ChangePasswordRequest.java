package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/2/2018.
 */

public class ChangePasswordRequest implements Parcelable
{


    private int id;
    private String old_password;
    private String new_password;
    private String confirm_password;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOld_password() {
        return old_password;
    }

    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getConfirm_password() {
        return confirm_password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.old_password);
        dest.writeString(this.new_password);
        dest.writeString(this.confirm_password);
    }

    public ChangePasswordRequest() {
    }

    protected ChangePasswordRequest(Parcel in) {
        this.id = in.readInt();
        this.old_password = in.readString();
        this.new_password = in.readString();
        this.confirm_password = in.readString();
    }

    public static final Creator<ChangePasswordRequest> CREATOR = new Creator<ChangePasswordRequest>() {
        @Override
        public ChangePasswordRequest createFromParcel(Parcel source) {
            return new ChangePasswordRequest(source);
        }

        @Override
        public ChangePasswordRequest[] newArray(int size) {
            return new ChangePasswordRequest[size];
        }
    };
}
