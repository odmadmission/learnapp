package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/25/2018.
 */

public class PMCompletedtaskDataList implements Parcelable {

    private int taskId;
    private String taskHeder;
    private String taskDetails;
    private String StartDate;
    private String EndDate;

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTaskHeder() {
        return taskHeder;
    }

    public void setTaskHeder(String taskHeder) {
        this.taskHeder = taskHeder;
    }

    public String getTaskDetails() {
        return taskDetails;
    }

    public void setTaskDetails(String taskDetails) {
        this.taskDetails = taskDetails;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.taskId);
        dest.writeString(this.taskHeder);
        dest.writeString(this.taskDetails);
        dest.writeString(this.StartDate);
        dest.writeString(this.EndDate);
    }

    public PMCompletedtaskDataList() {
    }

    protected PMCompletedtaskDataList(Parcel in) {
        this.taskId = in.readInt();
        this.taskHeder = in.readString();
        this.taskDetails = in.readString();
        this.StartDate = in.readString();
        this.EndDate = in.readString();
    }

    public static final Creator<PMCompletedtaskDataList> CREATOR = new Creator<PMCompletedtaskDataList>() {
        @Override
        public PMCompletedtaskDataList createFromParcel(Parcel source) {
            return new PMCompletedtaskDataList(source);
        }

        @Override
        public PMCompletedtaskDataList[] newArray(int size) {
            return new PMCompletedtaskDataList[size];
        }
    };
}
