package com.sseduventures.digichamps.frags;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.sseduventures.digichamps.helper.ServiceReceiver;


public class BaseFragment extends Fragment
{

    protected Handler mHandler;
    protected ServiceReceiver mServiceReceiver;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    protected void setUpReciever()
    {
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
    }
}
