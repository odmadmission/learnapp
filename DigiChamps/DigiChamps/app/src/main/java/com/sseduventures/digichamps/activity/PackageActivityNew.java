package com.sseduventures.digichamps.activity;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.rd.PageIndicatorView;
import com.rd.animation.type.AnimationType;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.adapter.CustomPagerAdapter_Package;
import com.sseduventures.digichamps.domain.PackageModel;
import com.sseduventures.digichamps.domain.PackageSuccess;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.fragment.BottomSheetFragment;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PackageActivityNew extends FormActivity implements ServiceReceiver.Receiver {

    private LinearLayout mButton1;
    private static final String TAG = "Package";
    PageIndicatorView pageIndicatorView;
    private ArrayList<PackageModel> list;
    ImageView learn_arrow, cart;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);


        setContentView(R.layout.packagenew_activity);
        logEvent(LogEventUtil.KEY_Packages_PAGE,
                LogEventUtil.EVENT_Packages_PAGE);
        setModuleName(LogEventUtil.EVENT_Packages_PAGE);
        list = new ArrayList<>();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        updateStatusBarColor("#FF298F96");
        learn_arrow = (ImageView) findViewById(R.id.learn_arrow);
        cart = (ImageView) findViewById(R.id.cart);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager_package);
        viewPager.setAdapter(new CustomPagerAdapter_Package(this, this));

        pageIndicatorView = findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setCount(4); // specify total count of indicators
        pageIndicatorView.setAnimationType(AnimationType.DROP);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {/*empty*/}

            @Override
            public void onPageSelected(int p) {

                pageIndicatorView.setSelection(p);
                if (p == 0)
                    updateStatusBarColor("#FF298F96");
                else if (p == 1)
                    updateStatusBarColor("#FFD74E3D");
                else if (p == 2)
                    updateStatusBarColor("#FFC98E2A");
                else if (p == 3)
                    updateStatusBarColor("#FF6F5084");
            }

            @Override
            public void onPageScrollStateChanged(int state) {/*empty*/}
        });

        learn_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(PackageActivityNew.this,
                        Cart_Activity.class);
                in.putExtra("discount",
                        RegPrefManager.getInstance(PackageActivityNew.this).getReedeem());
                startActivity(in);

            }
        });


        mButton1 = (LinearLayout) findViewById(R.id.but);

        mButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBottomSheetDialogFragment();
            }
        });

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);


    }


    private BottomSheetFragment bottomSheetFragment;

    public void showBottomSheetDialogFragment() {
        bottomSheetFragment = BottomSheetFragment.
                newInstance(list, pageIndicatorView.getSelection(),

                        RegPrefManager.getInstance(this).getReedeem());
        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
        getPackages();

    }


    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
        hideDialog();
        mServiceReceiver.setReceiver(null);
    }


    private void getPackages() {
        list.clear();
        if (AppUtil.isInternetConnected(this)) {
            try {

                showDialog(null, "please wait");
                NetworkService.startActionGetPackages(this,
                        mServiceReceiver);

            } catch (Exception e) {
                showToast("User ID parsing failed");
            }
        } else {
            showToast("No internet connection");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState,
                                    PersistableBundle outPersistentState) {

        hideDialog();
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode) {
            case ResponseCodes.NO_INTERNET:
                showAlertDialog("No Internet Connection",
                        "Please check ur mobile data / wifi");
                break;

            case ResponseCodes.UNEXPECTED_ERROR:
                showAlertDialog("Something went wrong",
                        "Please try again later " +
                                resultData.getString(IntentHelper.RESULT_EXCEPTION));
                break;

            case ResponseCodes.PACKAGES_SUCCESS:
                List<PackageModel> l =
                        ((PackageSuccess) resultData.getParcelable(IntentHelper.RESULT_DATA))
                                .getPackage();
                list.clear();
                list.addAll(l);
                Log.v(TAG, l.size() + "");
                break;


            case ResponseCodes.PACKAGES_FAILURE:
                showToast("Getting packages failed");
                break;
            case ResponseCodes.EXCEPTION:
                showToast("Exception : " +
                        resultData.getString(IntentHelper.RESULT_EXCEPTION));
                break;

            case ResponseCodes.ADD_TO_CART_SUCCESS:
                openCartActivity();

                break;


            case ResponseCodes.ADD_TO_CART_FAILURE:
                showToast("Add to cart fail");
                break;
        }
    }


    private void openCartActivity() {
        Intent intent = new Intent(this, Cart_Activity.class);
        intent.putExtra("discount", RegPrefManager.getInstance(this).getReedeem());
        startActivity(intent);
    }


    private void showAlertDialog(String title, String text) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setTitle(title);
        builder1.setMessage(text);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "CLOSE",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    public void updateStatusBarColor(String color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.
                    FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
    }

    public void addToCart(Bundle bundle) {
        if (bottomSheetFragment != null) {
            bottomSheetFragment.dismiss();
            bottomSheetFragment = null;
        }

        if (AppUtil.isInternetConnected(this)) {


            showDialog(null, "please wait");
            NetworkService.startActionAddToCart(
                    this,
                    mServiceReceiver, bundle);

        } else {
            showToast("No internet connection");
        }
    }
}

