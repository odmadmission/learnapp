package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/26/2018.
 */

public class EditProfileSuccess implements Parcelable {


    private String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Message);
    }

    public EditProfileSuccess() {
    }

    protected EditProfileSuccess(Parcel in) {
        this.Message = in.readString();
    }

    public static final Creator<EditProfileSuccess> CREATOR = new Creator<EditProfileSuccess>() {
        @Override
        public EditProfileSuccess createFromParcel(Parcel source) {
            return new EditProfileSuccess(source);
        }

        @Override
        public EditProfileSuccess[] newArray(int size) {
            return new EditProfileSuccess[size];
        }
    };
}
