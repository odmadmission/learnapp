package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.firebase.LogEventUtil;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MentorshipExamResultActivity extends FormActivity {

    private TextView tvResult;
    private TextView tvResult2;

    private TextView tvResult3;
    private TextView tvResult4;
    private TextView tvResult5;
    private TextView tvResult6;
    private TextView tvResult7;
    private TextView tvResult8;
    private LinearLayout shimmer_liner,liner;

    private ShimmerFrameLayout mShimmerViewContainer;

    private ImageView back_arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mentorship_exam_result);
        setModuleName(LogEventUtil.EVENT_MentorTest_PAGE);
        logEvent(LogEventUtil.KEY_MentorTest_PAGE,LogEventUtil.EVENT_MentorTest_PAGE);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        tvResult = findViewById(R.id.academic);
        tvResult2 = findViewById(R.id.doubt);
        tvResult3 = findViewById(R.id.interest);
        tvResult4 = findViewById(R.id.time);
        tvResult5 = findViewById(R.id.self);
        tvResult6 = findViewById(R.id.school);
        tvResult7 = findViewById(R.id.peronal);
        tvResult8 = findViewById(R.id.mentorship);
        liner=findViewById(R.id.liner);
        shimmer_liner=findViewById(R.id.shimmer_liner);
        back_arrow = findViewById(R.id.back_arrow);

        new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            shimmer_liner.setVisibility(View.GONE);
                            liner.setVisibility(View.VISIBLE);

                            tvResult.append(getIntent().getStringExtra("academic"));
                            tvResult2.append(getIntent().getStringExtra("doubt"));

                            tvResult3.append(getIntent().getStringExtra("interest"));
                            tvResult4.append(getIntent().getStringExtra("time"));

                            tvResult5.append(getIntent().getStringExtra("self"));
                            tvResult6.append(getIntent().getStringExtra("school"));

                            tvResult7.append(getIntent().getStringExtra("personal"));
                            tvResult8.append(getIntent().getStringExtra("mentorship"));
                        }
                    },2000);



        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }
}
