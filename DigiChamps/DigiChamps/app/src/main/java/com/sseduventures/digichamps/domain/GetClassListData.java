package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class GetClassListData implements Parcelable{

     private int ClassID;
     private String ClassName;
     private String boardName;
     private int boardID;

    public int getClassID() {
        return ClassID;
    }

    public void setClassID(int classID) {
        ClassID = classID;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public int getBoardID() {
        return boardID;
    }

    public void setBoardID(int boardID) {
        this.boardID = boardID;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.ClassID);
        dest.writeString(this.ClassName);
        dest.writeString(this.boardName);
        dest.writeInt(this.boardID);
    }

    public GetClassListData() {
    }

    protected GetClassListData(Parcel in) {
        this.ClassID = in.readInt();
        this.ClassName = in.readString();
        this.boardName = in.readString();
        this.boardID = in.readInt();
    }

    public static final Creator<GetClassListData> CREATOR = new Creator<GetClassListData>() {
        @Override
        public GetClassListData createFromParcel(Parcel source) {
            return new GetClassListData(source);
        }

        @Override
        public GetClassListData[] newArray(int size) {
            return new GetClassListData[size];
        }
    };
}
