
package com.sseduventures.digichamps.webservice.model;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Success implements Parcelable
{

private List<ExamDatum> ExamData;
private String Start_Time;
private String End_Time;
private int Result_ID;


    protected Success(Parcel in) {
        ExamData = in.createTypedArrayList(ExamDatum.CREATOR);
        Start_Time = in.readString();
        End_Time = in.readString();
        Result_ID = in.readInt();
    }

    public static final Creator<Success> CREATOR = new Creator<Success>() {
        @Override
        public Success createFromParcel(Parcel in) {
            return new Success(in);
        }

        @Override
        public Success[] newArray(int size) {
            return new Success[size];
        }
    };

    public List<ExamDatum> getExamData() {
        return ExamData;
    }

    public void setExamData(List<ExamDatum> examData) {
        ExamData = examData;
    }

    public String getStart_Time() {
        return Start_Time;
    }

    public void setStart_Time(String start_Time) {
        Start_Time = start_Time;
    }

    public String getEnd_Time() {
        return End_Time;
    }

    public void setEnd_Time(String end_Time) {
        End_Time = end_Time;
    }

    public int getResult_ID() {
        return Result_ID;
    }

    public void setResult_ID(int result_ID) {
        Result_ID = result_ID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(ExamData);
        dest.writeString(Start_Time);
        dest.writeString(End_Time);
        dest.writeInt(Result_ID);
    }



}