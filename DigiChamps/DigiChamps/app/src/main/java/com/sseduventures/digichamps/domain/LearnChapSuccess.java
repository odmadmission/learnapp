package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by NISHIKANT on 6/26/2018.
 */

public class LearnChapSuccess implements Parcelable
{

    private ArrayList<LearnChapQuestionBank> Quesbank;
    private ArrayList<LearnChapDetailsPdfs> pdfs;
    private Integer Chapterid;
    private String Chapter_Name;
    private String ChapterImage;
    private ArrayList<LearnChapDetailsChapModule> ChapterModules;
    private String Today_date;
    private Boolean Is_Offline;
    private String DIYModules;


    public ArrayList<LearnChapQuestionBank> getQuesbank() {
        return Quesbank;
    }

    public void setQuesbank(ArrayList<LearnChapQuestionBank> quesbank) {
        Quesbank = quesbank;
    }

    public ArrayList<LearnChapDetailsPdfs> getPdfs() {
        return pdfs;
    }

    public void setPdfs(ArrayList<LearnChapDetailsPdfs> pdfs) {
        this.pdfs = pdfs;
    }

    public Integer getChapterid() {
        return Chapterid;
    }

    public void setChapterid(Integer chapterid) {
        Chapterid = chapterid;
    }

    public String getChapter_Name() {
        return Chapter_Name;
    }

    public void setChapter_Name(String chapter_Name) {
        Chapter_Name = chapter_Name;
    }

    public String getChapterImage() {
        return ChapterImage;
    }

    public void setChapterImage(String chapterImage) {
        ChapterImage = chapterImage;
    }

    public ArrayList<LearnChapDetailsChapModule> getChapterModules() {
        return ChapterModules;
    }

    public void setChapterModules(ArrayList<LearnChapDetailsChapModule> chapterModules) {
        ChapterModules = chapterModules;
    }

    public String getToday_date() {
        return Today_date;
    }

    public void setToday_date(String today_date) {
        Today_date = today_date;
    }

    public Boolean getIs_Offline() {
        return Is_Offline;
    }

    public void setIs_Offline(Boolean is_Offline) {
        Is_Offline = is_Offline;
    }

    public String getDIYModules() {
        return DIYModules;
    }

    public void setDIYModules(String DIYModules) {
        this.DIYModules = DIYModules;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.Quesbank);
        dest.writeList(this.pdfs);
        dest.writeValue(this.Chapterid);
        dest.writeString(this.Chapter_Name);
        dest.writeString(this.ChapterImage);
        dest.writeList(this.ChapterModules);
        dest.writeString(this.Today_date);
        dest.writeValue(this.Is_Offline);
        dest.writeString(this.DIYModules);
    }

    public LearnChapSuccess() {
    }

    protected LearnChapSuccess(Parcel in) {
        this.Quesbank = new ArrayList<LearnChapQuestionBank>();
        in.readList(this.Quesbank, LearnChapQuestionBank.class.getClassLoader());
        this.pdfs = new ArrayList<LearnChapDetailsPdfs>();
        in.readList(this.pdfs, LearnChapDetailsPdfs.class.getClassLoader());
        this.Chapterid = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Chapter_Name = in.readString();
        this.ChapterImage = in.readString();
        this.ChapterModules = new ArrayList<LearnChapDetailsChapModule>();
        in.readList(this.ChapterModules, LearnChapDetailsChapModule.class.getClassLoader());
        this.Today_date = in.readString();
        this.Is_Offline = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.DIYModules = in.readString();
    }

    public static final Creator<LearnChapSuccess> CREATOR = new Creator<LearnChapSuccess>() {
        @Override
        public LearnChapSuccess createFromParcel(Parcel source) {
            return new LearnChapSuccess(source);
        }

        @Override
        public LearnChapSuccess[] newArray(int size) {
            return new LearnChapSuccess[size];
        }
    };
}
