package com.sseduventures.digichamps.frags;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.DoubtListActivity;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.activity.AnalysisDetail;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.activity.Sub_concept_analysis;
import com.sseduventures.digichamps.domain.AnalyticsResponse;
import com.sseduventures.digichamps.domain.SubConceptAnalyticsList;
import com.sseduventures.digichamps.domain.TestAnalysisAnalyticsList;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.CircularCompletionView;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class AnalyticsFragment extends BaseFragment
        implements ServiceReceiver.Receiver {
    private BarChart barChart, barChartM, barChartS;

    private FormActivity mContext;
    private ImageView dash_but, bookmarks_but, search_but, feed_but,
            more_but;
    TextView appeared, average, av_time, subConcept, weakSubConcepts, TotalDoubtsRejected,
            TotalDoubtsAnswerded, TotalDoubtsAsked;
    Button more, but_report_new, but_doubtlist;
    private ShimmerFrameLayout mShimmerViewContainer;


    private SpotsDialog dialog;

    com.sseduventures.digichamps.helper.CircularSeekBar seekbar;
    long user_id;

    int classId;
    private CollapsingToolbarLayout collapsing_toolbar;
    private Toolbar toolbar;
    AppBarLayout appBarLayout;

    ArrayList<String> reportlist;
    private LinearLayout analytics_linear;
    private RelativeLayout analytic_relative;

    ArrayList<String> sub;


    TextView progress_text;
    CircularCompletionView ccv;

    private TextView subReport, subTest, subSub;

    // board spinner data
    public ArrayAdapter<String> subject_adapter;
    public ArrayAdapter<String> subject_adapterAnalysis;
    public ArrayAdapter<String> subject_adapterSubconcept;

    public ArrayList<String> subjectReport;
    public ArrayList<String> subjectConcept;
    public ArrayList<String> subjectTest;


    public ArrayList<TestAnalysisAnalyticsList> mSubjectTest;
    public ArrayList<SubConceptAnalyticsList> mSubjectConcept;

    ArrayList<String> labels;

    boolean Is_Doubt;


    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private AnalyticsResponse success;


    private View mView;


    public static AnalyticsFragment getInstance() {
        AnalyticsFragment fragment = new AnalyticsFragment();

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((NewDashboardActivity) getActivity())
                .logEvent(LogEventUtil.KEY_Analytics_PAGE,
                        LogEventUtil.EVENT_Analytics_PAGE);
        ((NewDashboardActivity) getActivity()).setModuleName(LogEventUtil.EVENT_Analytics_PAGE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,

                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.analytics_activity,
                container, false);

        ((NewDashboardActivity) getActivity()).updateStatusBarColor("#FFA0AC2E");


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        init(mView);
        mContext = (NewDashboardActivity) getActivity();

        classId = (int) RegPrefManager.getInstance(getActivity()).getKeyClassId();


        Is_Doubt = RegPrefManager.getInstance(getActivity()).getIsDoubt();


        user_id = RegPrefManager.getInstance(getActivity()).getRegId();


        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AnalysisDetail.class);
                startActivity(intent);


            }
        });


        but_report_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), Sub_concept_analysis.class);
                startActivity(in);


            }
        });


        but_doubtlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getActivity(), DoubtListActivity.class);
                intent.putExtra("analytics", "analytics");
                startActivity(intent);


            }
        });

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsing_toolbar.setTitle("Analytics");
                    isShow = true;
                } else if (isShow) {
                    collapsing_toolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });


        return mView;
    }

    private TestAnalysisAnalyticsList getTestAnalysisObject(String subName) {
        TestAnalysisAnalyticsList object = null;

        try {

            if (success != null && success.getSuccess() != null &&
                    success.getSuccess()
                            .getTestAnalysisAnalyticsList() != null
                    && success.getSuccess()
                    .getTestAnalysisAnalyticsList().size() > 0) {

                for (int i = 0; i < success.getSuccess()
                        .getTestAnalysisAnalyticsList().size(); i++) {
                    if (success.getSuccess()
                            .getTestAnalysisAnalyticsList().get(i).getSubject() != null &&
                            success.getSuccess()
                                    .getTestAnalysisAnalyticsList().get(i).getSubject().equals(subName))
                        object = success.getSuccess()
                                .getTestAnalysisAnalyticsList().get(i);

                }

            } else {

                Toast.makeText(getActivity(), "Object null", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(),
                    "Parse Failed", Toast.LENGTH_SHORT).show();
        }

        return object;
    }

    private SubConceptAnalyticsList getSubConceptObject(String subName) {
        SubConceptAnalyticsList object = null;
        try {
            if (success != null
                    && success.getSuccess() != null && success.getSuccess().getSubConceptAnalyticsList() != null
                    && success.getSuccess().getSubConceptAnalyticsList().size() > 0) {


                for (int i = 0; i < success.getSuccess().getSubConceptAnalyticsList().size(); i++) {
                    if (success.getSuccess().getSubConceptAnalyticsList().get(i)
                            .getSubject() != null &&
                            success.getSuccess().getSubConceptAnalyticsList().get(i)
                                    .getSubject().equals(subName))
                        object = success.getSuccess().getSubConceptAnalyticsList().get(i);


                }

            } else {

                Toast.makeText(getActivity(), "Object null", Toast.LENGTH_SHORT).show();

            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Parse Failed", Toast.LENGTH_SHORT).show();
        }

        return object;
    }

    //
    public void init(View view) {


        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        analytics_linear = view.findViewById(R.id.analytics_linear);
        analytic_relative = view.findViewById(R.id.analytic_relative);
        ccv = (CircularCompletionView) view.findViewById(R.id.ccv);
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        seekbar = (com.sseduventures.digichamps.helper.CircularSeekBar)
                view.findViewById(R.id.accuracy_circle_progress_view);
        seekbar.getProgress();
        seekbar.setMax(100);


        dash_but = (ImageView) view.findViewById(R.id.home_dash);
        bookmarks_but = (ImageView) view.findViewById(R.id.bookmark_dash);
        search_but = (ImageView) view.findViewById(R.id.search_dash);
        feed_but = (ImageView) view.findViewById(R.id.feed_dash);
        more_but = (ImageView) view.findViewById(R.id.more_dash);
        appBarLayout = (AppBarLayout) mView.findViewById(R.id.appbar);
        collapsing_toolbar = (CollapsingToolbarLayout) mView.findViewById(R.id.collapsing_toolbar);

        barChart = (BarChart) view.findViewById(R.id.chart);
        barChartM = (BarChart) view.findViewById(R.id.chartM);
        barChartS = (BarChart) view.findViewById(R.id.chartS);
        subjectReport = new ArrayList<>();
        subjectTest = new ArrayList<>();
        mSubjectConcept = new ArrayList<>();
        mSubjectTest = new ArrayList<>();
        subjectConcept = new ArrayList<>();
        reportlist = new ArrayList<>();
        sub = new ArrayList<>();


        progress_text = (TextView) view.findViewById(R.id.prog_text);
        progress_text.setText("");

        more = (Button) view.findViewById(R.id.more);
        but_report_new = (Button) view.findViewById(R.id.but_report_new);
        but_doubtlist = (Button) view.findViewById(R.id.but_doubtlist);

        dialog = new SpotsDialog(getActivity(), "Fun+Education", R.style.Custom);
        subReport = (TextView) view.findViewById(R.id.tvReportSub);
        subReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showReportSubjects();
            }
        });
        subTest = (TextView) view.findViewById(R.id.tvAnalysisSub);
        subSub = (TextView) view.findViewById(R.id.tvSubSub);

        subSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWeakSubjectDialog();
            }
        });

        subTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAnalysisDialog();
            }
        });


        subject_adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, sub);
        subject_adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);


        subject_adapterAnalysis = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, sub);
        subject_adapterAnalysis.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);


        subject_adapterSubconcept = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, sub);
        subject_adapterSubconcept.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        appeared = (TextView) view.findViewById(R.id.appeared);
        av_time = (TextView) view.findViewById(R.id.av_time);
        average = (TextView) view.findViewById(R.id.average);
        subConcept = (TextView) view.findViewById(R.id.subConcept);
        weakSubConcepts = (TextView) view.findViewById(R.id.weakSubConcepts);
        TotalDoubtsRejected = (TextView) view.findViewById(R.id.TotalDoubtsRejected);
        TotalDoubtsAnswerded = (TextView) view.findViewById(R.id.TotalDoubtsAnswerded);
        TotalDoubtsAsked = (TextView) view.findViewById(R.id.TotalDoubtsAsked);


    }


    private JSONObject suJsonObject;


    private ArrayList<String> indexes = new ArrayList<>();
    ;
    private ArrayList<String> values = new ArrayList<>();
    ;


    private void resetReportGraph(String subName) {

        subReport.setText(subName);

        ArrayList<BarEntry> entries = new ArrayList();
        final ArrayList<String> dates = new ArrayList<>();
        ;
        dates.clear();

        try {
            if (suJsonObject != null) {
                JSONArray jsonArray = suJsonObject.getJSONArray("userReportAnalyticsList");

                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {

                        if (jsonArray.getJSONObject(i).get("Subject").equals(subName)) {
                            String reportDate = jsonArray.getJSONObject(i).getString("ReportDate");
                            int time = jsonArray.getJSONObject(i).getInt("TotalTime");
                            if (time > 0) {


                                String reportDateSplit = reportDate.split("T")[0];
                                dates.add(reportDateSplit);

                                if (time == 1 || time == 0)
                                    values.add(time + " Min");
                                else
                                    values.add(time + " Mins");

                                entries.add(new BarEntry(i, time));
                                indexes.add(reportDateSplit);
                            }
                        }


                    }
                }
            }
        } catch (Exception e) {
        }


        if (entries.size() > 0) {

            BarDataSet dataset = new BarDataSet(entries, "videos");

            dataset.setAxisDependency(YAxis.AxisDependency.LEFT);
            BarData data = new BarData(dataset);


            YAxis yAxis = barChart.getAxisLeft();
            yAxis.setAxisMinimum(0);
            yAxis.setDrawAxisLine(false);


            YAxis rightYAxis = barChart.getAxisRight();
            rightYAxis.setEnabled(false);

            XAxis xAxis = barChart.getXAxis();


            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setDrawGridLines(false);
            xAxis.setGranularity(1f);
            xAxis.setValueFormatter(new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {

                    return indexes.get((int) value);
                }

            });

            barChart.setData(data);
            barChart.getDescription().setEnabled(false);
            dataset.setColors(ColorTemplate.COLORFUL_COLORS);
            barChart.animateY(2000);
        }


    }


    private void resetReportGraphM(String subName) {

        subReport.setText(subName);
        ArrayList<BarEntry> entries = new ArrayList();
        final ArrayList<String> dates = new ArrayList<>();
        ;
        dates.clear();
        try {
            if (suJsonObject != null) {
                JSONArray jsonArray = suJsonObject.getJSONArray("userReportAnalyticsList");

                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {

                        if (jsonArray.getJSONObject(i).get("Subject").equals(subName)) {
                            String reportDate = jsonArray.getJSONObject(i).getString("ReportDate");

                            int time = jsonArray.getJSONObject(i).getInt("TotalTime");
                            if (time > 0) {
                                String reportDateSplit = reportDate.split("T")[0];
                                dates.add(reportDateSplit);
                                entries.add(new BarEntry(i, time));
                                indexes.add(reportDateSplit);
                                if (time == 1 || time == 0)
                                    values.add(time + " Min");
                                else
                                    values.add(time + " Mins");

                            }
                        }


                    }

                    if (entries.size() > 0) {
                        BarDataSet dataset = new BarDataSet(entries, "videos");

                        BarData data = new BarData(dataset);


                        YAxis yAxis = barChartM.getAxisLeft();
                        yAxis.setAxisMinimum(0);
                        yAxis.setDrawAxisLine(false);


                        YAxis rightYAxis = barChartM.getAxisRight();
                        rightYAxis.setEnabled(false);
                        XAxis xAxis = barChartM.getXAxis();


                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxis.setDrawGridLines(false);
                        xAxis.setGranularity(1f);
                        xAxis.setValueFormatter(new IAxisValueFormatter() {
                            @Override
                            public String getFormattedValue(float value, AxisBase axis) {

                                return indexes.get((int) value);
                            }

                        });

                        barChartM.setData(data);
                        barChartM.getDescription().setEnabled(false);
                        dataset.setColors(ColorTemplate.COLORFUL_COLORS);
                        barChartM.animateY(2000);
                    }

                } else {
                }

            } else {
            }

        } catch (Exception e) {
        }


    }

    private void resetReportGraphSc(String subName) {
        subReport.setText(subName);

        ArrayList<BarEntry> entries = new ArrayList();
        final ArrayList<String> dates = new ArrayList<>();
        ;
        dates.clear();
        try {
            if (suJsonObject != null) {
                JSONArray jsonArray = suJsonObject.getJSONArray("userReportAnalyticsList");

                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {

                        if (jsonArray.getJSONObject(i).get("Subject").equals(subName)) {
                            String reportDate = jsonArray.getJSONObject(i).getString("ReportDate");
                            int time = jsonArray.getJSONObject(i).getInt("TotalTime");
                            if (time > 0) {

                                String reportDateSplit = reportDate.split("T")[0];
                                dates.add(reportDateSplit);
                                entries.add(new BarEntry(i, time));
                                indexes.add(reportDateSplit);
                                if (time == 1 || time == 0)
                                    values.add(time + " Min");
                                else
                                    values.add(time + " Mins");
                            }
                        }


                    }

                    if (entries.size() > 0) {

                        BarDataSet dataset = new BarDataSet(entries, "videos");

                        BarData data = new BarData(dataset);


                        YAxis yAxis = barChartS.getAxisLeft();
                        yAxis.setAxisMinimum(0);
                        yAxis.setDrawAxisLine(false);


                        YAxis rightYAxis = barChartS.getAxisRight();
                        rightYAxis.setEnabled(false);

                        XAxis xAxis = barChartS.getXAxis();


                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxis.setDrawGridLines(false);
                        xAxis.setGranularity(1f);
                        xAxis.setValueFormatter(new IAxisValueFormatter() {
                            @Override
                            public String getFormattedValue(float value, AxisBase axis) {


                                return indexes.get((int) value);
                            }

                        });

                        barChartS.setData(data);
                        barChartS.getDescription().setEnabled(false);
                        dataset.setColors(ColorTemplate.COLORFUL_COLORS);
                        barChartS.animateY(2000);
                    }

                } else {
                    Toast.makeText(getActivity(), "Empty Report Data", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), "Data null", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Parse Failed : " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }


    //back navigation
    public void ReturnHome(View view) {
        Intent in = new Intent(getActivity(), NewDashboardActivity.class);
        startActivity(in);

        getActivity().finish();
    }

    String[] days = null;

    private void showReportSubjects() {
        AlertDialog.Builder myDialog =
                new AlertDialog.Builder(getActivity());
        myDialog.setTitle("Select Subject");
        myDialog.setItems(days, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                String mSelectedText = days[which];

                prepareReport(mSelectedText);


            }
        });


        myDialog.show();
    }

    String[] weak = null;

    private void showWeakSubjectDialog() {


        AlertDialog.Builder myDialog =
                new AlertDialog.Builder(getActivity());
        myDialog.setTitle("Select Subject");
        myDialog.setItems(weak, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                String mSelectedText = weak[which];

                prepareWeakConcepts(mSelectedText);

            }
        });


        myDialog.show();
    }

    String[] tests = null;

    private void showAnalysisDialog() {


        AlertDialog.Builder myDialog =
                new AlertDialog.Builder(getActivity());
        myDialog.setTitle("Select Subject");
        myDialog.setItems(tests, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                String mSelectedText = tests[which];

                prepareAnalysis(mSelectedText);

            }
        });


        myDialog.show();
    }

    private void prepareReport(String mSelectedText) {
        subReport.setText(mSelectedText);
        if (mSelectedText.equals("All")) {
            barChart.setVisibility(View.VISIBLE);
            barChartM.setVisibility(View.GONE);
            barChartS.setVisibility(View.GONE);
        } else if (mSelectedText.contains("Mathematics")) {
            barChart.setVisibility(View.GONE);
            barChartM.setVisibility(View.VISIBLE);
            barChartS.setVisibility(View.GONE);

        } else if (mSelectedText.contains("Science")) {
            barChart.setVisibility(View.GONE);
            barChartM.setVisibility(View.GONE);
            barChartS.setVisibility(View.VISIBLE);
        }


        if (mSelectedText.equals("All")) {
            resetReportGraph(mSelectedText);
        } else if (mSelectedText.contains("Mathematics")) {
            resetReportGraphM(mSelectedText);
        } else if (mSelectedText.contains("Science")) {
            resetReportGraphSc(mSelectedText);
        }


    }


    private void prepareAnalysis(String selectedText) {
        subTest.setText(selectedText);

        try {
            TestAnalysisAnalyticsList jsonObject =
                    getTestAnalysisObject
                            (selectedText);
            if (jsonObject != null) {
                appeared.setText(jsonObject.getTotalExamsAppeared() + "");
                average.setText(jsonObject.getAverageMarks() + "");
                final int pb = (jsonObject.getAccuracy());
                seekbar.setProgress(pb);
                seekbar.setOnSeekBarChangeListener(
                        new com.sseduventures.digichamps.helper.CircularSeekBar.OnCircularSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged(com.sseduventures.digichamps.helper.CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
                                seekbar.setProgress(pb);
                            }

                            @Override
                            public void onStopTrackingTouch(com.sseduventures.digichamps.helper.CircularSeekBar seekBar) {
                                seekbar.setProgress(pb);
                            }

                            @Override
                            public void onStartTrackingTouch(com.sseduventures.digichamps.helper.CircularSeekBar seekBar) {
                                seekbar.setProgress(pb);
                            }


                        });


                progress_text.setText(pb + "%");
            } else {
                appeared.setText("0");
                average.setText("0");
                seekbar.setProgress(0);
                progress_text.setText(0 + "%");


            }
        } catch (Exception e) {


        }


    }

    private void prepareWeakConcepts(String mSelectedText) {
        subSub.setText(mSelectedText);
        try {
            SubConceptAnalyticsList jsonObject = getSubConceptObject(mSelectedText.toString());
            if (jsonObject != null) {

                subConcept.setText(jsonObject.getSubConceptsAppeared() + "");

                subConcept.setText("0");

                subConcept.setText(jsonObject.getSubConceptsAppeared() + "");
                weakSubConcepts.setText(jsonObject.getTotalWeakSubConcepts() + "");



            } else {
                subConcept.setText("0");
                weakSubConcepts.setText("0");


            }
        } catch (Exception e) {

        }
    }


    private void getAnalytics() {
        if (AppUtil.isInternetConnected(getActivity())) {


            NetworkService.startActionGetAnalyticsDetailsData(getActivity(), mServiceReceiver);

        } else {
            Intent in = new Intent(getActivity(), Internet_Activity.class);
            startActivity(in);
        }
    }

    @Override
    public void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }


    @Override
    public void onStop() {
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        getAnalytics();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        analytic_relative.setVisibility(View.GONE);
        analytics_linear.setVisibility(View.VISIBLE);

        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                suJsonObject = new JSONObject();
                AnalyticsResponse resp = success;
                Gson gson = new Gson();
                String jsonString = gson.toJson(resp);
                try {
                    suJsonObject = new JSONObject(jsonString);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                if (success != null && success.getSuccess() != null
                        && success.getSuccess().getSubjectList().size() > 0) {
                    sub.clear();
                    sub.addAll(success.getSuccess().getSubjectList());
                }


                if (success != null && success.getSuccess() != null
                        && success.getSuccess().
                        getTestAnalysisAnalyticsList().size() > 0) {

                    mSubjectTest.clear();
                    mSubjectTest.addAll(success.
                            getSuccess().
                            getTestAnalysisAnalyticsList());

                    subject_adapterAnalysis.notifyDataSetChanged();
                    subject_adapter.notifyDataSetChanged();

                    if (sub.size() > 0) {
                        tests = new String[sub.size()];
                        tests = sub.toArray(tests);
                        prepareAnalysis(sub.get(0));

                    }

                }


                if (success != null && success.getSuccess() != null
                        && success.getSuccess().getSubConceptAnalyticsList().size() > 0) {
                    mSubjectConcept.clear();
                    mSubjectConcept.addAll(success.getSuccess().getSubConceptAnalyticsList());

                    subject_adapterSubconcept.notifyDataSetChanged();

                    if (sub.size() > 0) {
                        weak = new String[sub.size()];
                        weak = sub.toArray(weak);
                        prepareWeakConcepts(sub.get(0));

                    }
                }

                if (success != null && success.getSuccess() != null
                        && success.getSuccess().getDoubtsAnalytics() != null) {
                    TotalDoubtsAsked.setText(success.getSuccess().getDoubtsAnalytics().getDoubtsAsked() + "");
                    TotalDoubtsAnswerded.setText(success.getSuccess().getDoubtsAnalytics().getDoubtsAnswered() + "");
                    TotalDoubtsRejected.setText(success.getSuccess().getDoubtsAnalytics().getDoubtsRejected() + "");

                    int doubtAskedInt = 0;

                    try {
                        doubtAskedInt = success.getSuccess().getDoubtsAnalytics().getDoubtsAsked();
                    } catch (NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                    }
                }


                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(), ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(), ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(), Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }
}
