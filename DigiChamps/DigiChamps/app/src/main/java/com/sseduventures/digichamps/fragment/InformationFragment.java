package com.sseduventures.digichamps.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activity.AssignedTeachersActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.activity.MainActivity;
import com.sseduventures.digichamps.activity.SchoolInfoActivity;
import com.sseduventures.digichamps.domain.InformationRequest;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.utils.Preferences;
import com.sseduventures.digichamps.webservice.ResponseCodes;


/**
 * Created by user on 06-12-2017.
 */

public class InformationFragment extends Fragment implements
        ServiceReceiver.Receiver {

    Context mContext;
    RelativeLayout btnAssignedTeacher, btnSchoolInfo;
    TextView tv_school_name, llToolbar, tv_assign, ll_want, tv_track, tv_about, ll_want1;
    String schoolName = "", schoolInfo = "", schoolLogo = "", schoolDocumentaryVideo = "", schoolid;
    TextView click1, here1, click2, here2;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private InformationRequest success;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_information, container, false);
        if(getActivity() instanceof FormActivity)
        {
            ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_infor_frag);
            ((FormActivity)getActivity()).logEvent(LogEventUtil.KEY_infor_frag,
                    LogEventUtil.EVENT_infor_frag);
        }

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        schoolid = RegPrefManager.getInstance(getActivity()).getSchoolId();

        mContext = getActivity();
        ll_want = (TextView) view.findViewById(R.id.ll_want);
        tv_track = (TextView) view.findViewById(R.id.tv_track);
        tv_about = (TextView) view.findViewById(R.id.tv_about);
        ll_want1 = (TextView) view.findViewById(R.id.ll_want1);

        click1 = (TextView) view.findViewById(R.id.click1);
        here1 = (TextView) view.findViewById(R.id.here1);
        click2 = (TextView) view.findViewById(R.id.click2);
        here2 = (TextView) view.findViewById(R.id.here2);

        FontManage.setFontTextLandingBold(mContext, ll_want);
        FontManage.setFontTextLandingBold(mContext, tv_track);
        FontManage.setFontTextLandingBold(mContext, tv_about);
        FontManage.setFontTextLandingBold(mContext, ll_want1);
        FontManage.setFontMeiryo(mContext, click1);
        FontManage.setFontMeiryo(mContext, here1);
        FontManage.setFontMeiryo(mContext, click2);
        FontManage.setFontMeiryo(mContext, here2);

        llToolbar = (TextView) view.findViewById(R.id.llToolbar);
        FontManage.setFontHeaderBold(mContext, llToolbar);
        tv_school_name = (TextView) view.findViewById(R.id.tv_school_name);
        FontManage.setFontImpact(mContext, tv_school_name);
        tv_assign = (TextView) view.findViewById(R.id.tv_assign);
        FontManage.setFontImpact(mContext, tv_assign);
        btnSchoolInfo = view.findViewById(R.id.btnSchoolInfo);
        btnAssignedTeacher = view.findViewById(R.id.btnAssignedTeacher);
        btnAssignedTeacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, AssignedTeachersActivity.class);
                startActivity(i);
            }
        });


        btnSchoolInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, SchoolInfoActivity.class);
                startActivity(i);
            }
        });


        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                    Intent i = new Intent(mContext, MainActivity.class);
                    mContext.startActivity(i);
                    return true;
                } else {
                    return false;
                }
            }
        });

        //getSchoolInformation();
        getSchoolInfoNew();
        return view;
    }


    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mContext)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }


    private void getSchoolInfoNew() {
        if (AppUtil.isInternetConnected(getActivity())) {


            NetworkService.startActionGetInfo(getActivity(), mServiceReceiver);

        } else {
            Intent in = new Intent(getActivity(), Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    public void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    public void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        //hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(), ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(), Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(), ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:

                success = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if (success != null && success.getResultCount() == 1
                        && success.getMessage().equalsIgnoreCase("Successfull")) {
                    if (success.getSchoolInformation() != null &&
                            success.getSchoolInformation().size() > 0) {


                        for (int k = 0; k < success.getSchoolInformation().size(); k++) {

                            tv_school_name.setText(success.getSchoolInformation().get(k).getSchoolName());

                            schoolName = success.getSchoolInformation().get(k).getSchoolName();
                            schoolInfo = success.getSchoolInformation().get(k).getInformation();
                            schoolLogo = success.getSchoolInformation().get(k).getLogo();
                            schoolDocumentaryVideo = success.getSchoolInformation().get(k).getDocumentaryVideo();

                            Preferences.save(mContext, Preferences.KEY_SCHOOL_NAME, schoolName);
                            Preferences.save(mContext, Preferences.KEY_SCHOOL_INFO, schoolInfo);
                            Preferences.save(mContext, Preferences.KEY_SCHOOL_LOGO, schoolLogo);
                            Preferences.save(mContext, Preferences.KEY_SCHOOL_VIDEO, schoolDocumentaryVideo);
                        }
                    } else {
                        AskOptionDialog("No School Zone Found").show();
                    }
                } else {
                    AskOptionDialog("No School Zone Found").show();
                }
                break;

        }
    }
}
