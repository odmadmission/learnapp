package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/11/2018.
 */

public class ExamScheduleResponse implements Parcelable{

    private List<ExamScheduleListNew> ExamScheduleList;
    private String Successfull;
    private int ResultCount;

    public List<ExamScheduleListNew> getExamScheduleList() {
        return ExamScheduleList;
    }

    public void setExamScheduleList(List<ExamScheduleListNew> examScheduleList) {
        ExamScheduleList = examScheduleList;
    }

    public String getSuccessfull() {
        return Successfull;
    }

    public void setSuccessfull(String successfull) {
        Successfull = successfull;
    }

    public int getResultCount() {
        return ResultCount;
    }

    public void setResultCount(int resultCount) {
        ResultCount = resultCount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.ExamScheduleList);
        dest.writeString(this.Successfull);
        dest.writeInt(this.ResultCount);
    }

    public ExamScheduleResponse() {
    }

    protected ExamScheduleResponse(Parcel in) {
        this.ExamScheduleList = in.createTypedArrayList(ExamScheduleListNew.CREATOR);
        this.Successfull = in.readString();
        this.ResultCount = in.readInt();
    }

    public static final Creator<ExamScheduleResponse> CREATOR = new Creator<ExamScheduleResponse>() {
        @Override
        public ExamScheduleResponse createFromParcel(Parcel source) {
            return new ExamScheduleResponse(source);
        }

        @Override
        public ExamScheduleResponse[] newArray(int size) {
            return new ExamScheduleResponse[size];
        }
    };
}
