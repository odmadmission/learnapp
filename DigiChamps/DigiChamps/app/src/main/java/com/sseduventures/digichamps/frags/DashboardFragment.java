package com.sseduventures.digichamps.frags;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.DoubtActivity;
import com.sseduventures.digichamps.activities.EventActivity;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.LearnActivity;
import com.sseduventures.digichamps.activities.LearnAndEarnActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.activities.PersonalMentorActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.domain.DashboardResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DashboardFragment extends BaseFragment implements ServiceReceiver.Receiver {


    private View mView;

    private RelativeLayout rlv_learn_tdy, rlv_mentor, rlv_doubts, know_more;

    private Toolbar toolbar;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private AppBarLayout appBarLayout;
    private RelativeLayout dash_relt;

    private RelativeLayout refer_earn;
    private LinearLayout dash_linear;
    private ShimmerFrameLayout mShimmerViewContainer;
    private Handler mHandler;
    boolean b;
    private ServiceReceiver mServiceReceiver;
    private DashboardResponse success;
    private TextView coinsCount, hello;

    boolean checkMentor = false;
    boolean doubtCheck = false;
    private Button event;

    private FormActivity mContext;
    private TextView know_more_free_video;
    private ShareFragment shareFragmnet;
    private TextView know_more_free_video1;
    private TextView know_more_free_video2;
    private TextView know_more_free_video4;


    public static DashboardFragment getInstance() {
        DashboardFragment fragment = new DashboardFragment();

        return fragment;
    }

    private Toolbar t;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_dashboard,
                container, false);

        ((NewDashboardActivity) getActivity())
                .logEvent(LogEventUtil.KEY_DASHBOARD,
                        LogEventUtil.EVENT_DASHBOARD);
        ((NewDashboardActivity) getActivity())
                .setModuleName(LogEventUtil.EVENT_DASHBOARD);
        init();

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        ((NewDashboardActivity) getActivity()).updateStatusBarColor("#2390b4");


        mContext = (NewDashboardActivity) getActivity();
        rlv_learn_tdy = (RelativeLayout) mView.findViewById(R.id.rlv_learn_tdy);
        dash_relt = mView.findViewById(R.id.dash_relative);
        event = mView.findViewById(R.id.event);
        mShimmerViewContainer = mView.findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        dash_linear = mView.findViewById(R.id.dash_linear);
        dash_linear.setVisibility(View.GONE);
        toolbar = (Toolbar) mView.findViewById(R.id.toolbar);
        appBarLayout = mView.findViewById(R.id.appbar);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) mView.findViewById(R.id.collapsing_toolbar);
        rlv_mentor = (RelativeLayout) mView.findViewById(R.id.rlv_mentor);
        rlv_doubts = (RelativeLayout) mView.findViewById(R.id.rlv_doubts);
        refer_earn = (RelativeLayout) mView.findViewById(R.id.refer_earn);
        coinsCount = (TextView) mView.findViewById(R.id.coinsCount);
        hello = (TextView) mView.findViewById(R.id.hello);
        know_more_free_video1 = mView.findViewById(R.id.know_more_free_video1);
        know_more_free_video = mView.findViewById(R.id.know_more_free_video);
        know_more_free_video2 = mView.findViewById(R.id.know_more_free_video2);
        know_more = (RelativeLayout) mView.findViewById(R.id.know_more);
        know_more_free_video4 = mView.findViewById(R.id.know_more_free_video4);

        String name = RegPrefManager.getInstance(getActivity()).getUserName();
        String firstName = "";

        if (name != null && name.length() > 1) {
            Pattern p = Pattern.compile("[^a-z0-9 \\. \\< \\( \\[ \\{ \\^ \\- \\= \\$ \\! \\| \\] \\} \\) \\? \\* \\+ \\> \\@]", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(name);
            b = m.find();
        }else {
            name="";
        }

        if (b) {

            if (name != null && !name.equals("null") && name.length() > 1 && name.contains("\\w+")) {


                if (name.split("\\w+").length > 1) {

                    firstName = name.substring(0, name.lastIndexOf(' '));

                } else {

                    firstName = name;

                }
            }

        } else {

            firstName = name;


        }

        hello.setText("Hello " + firstName + ", want to");

        toolbar.setTitle("DIGICHAMPS");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        mCollapsingToolbarLayout.setTitle(" ");

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                    toolbar.setBackgroundColor(getResources().getColor(R.color.transparent));
                }
                if (scrollRange + verticalOffset == 0) {
                    mCollapsingToolbarLayout.setTitle("      DIGICHAMPS");
                    isShow = true;
                    toolbar.setBackgroundColor(getResources().getColor(R.color.bluePrimary));
                } else if (isShow) {
                    mCollapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                    toolbar.setBackgroundColor(getResources().getColor(R.color.transparent));
                }
            }
        });

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Dashboard");


        coinsCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), LearnAndEarnActivity.class);
                startActivity(intent);
            }
        });


        know_more_free_video1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ((NewDashboardActivity) getActivity()).selectAnalytics();

            }
        });

        know_more_free_video4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getActivity(), PersonalMentorActivity.class);
                startActivity(intent);

            }
        });

        event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EventActivity.class);
                startActivity(intent);

            }
        });

        know_more_free_video2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), LearnAndEarnActivity.class);
                startActivity(intent);
            }
        });


        know_more_free_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LearnActivity.class);
                startActivity(intent);
            }
        });

        rlv_learn_tdy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(), LearnActivity.class);
                startActivity(in);
            }
        });

        rlv_mentor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), PersonalMentorActivity.class));


            }
        });

        rlv_doubts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(getActivity(),
                        DoubtActivity.class));


            }
        });


        refer_earn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {


                ((NewDashboardActivity) getActivity()).openBottomSheet();


            }
        });

        know_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getActivity(), LearnAndEarnActivity.class);
                startActivity(in);
            }
        });

        return mView;
    }


    private void init() {

    }

    @Override
    public void onResume() {
        super.onResume();
        getNewDashboardData();
        mShimmerViewContainer.startShimmer();
    }


    private void getNewDashboardData() {

        if (AppUtil.isInternetConnected(getActivity())) {

            NetworkService.startActionPostNewDashboard(getActivity(),
                    mServiceReceiver);

        } else {

            startActivity(new Intent(getActivity(), Internet_Activity.class));


        }
    }

    @Override
    public void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    private int doubtCount = 0;

    public void setCount() {

        ((NewDashboardActivity) getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                coinsCount.setText(RegPrefManager.getInstance(getActivity()).getCoinCount() + "");

                coinsCount.invalidate();
                coinsCount.requestLayout();
            }
        });

    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        mShimmerViewContainer.stopShimmer();
        dash_relt.setVisibility(View.GONE);
        mShimmerViewContainer.setVisibility(View.GONE);
        dash_linear.setVisibility(View.VISIBLE);
        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if (success != null) {

                    checkMentor = success.getSuccess().isIs_Mentor();
                    doubtCheck = success.getSuccess().isIs_Doubt();
                    doubtCount = success.getSuccess().getDoubtCount();

                    RegPrefManager.getInstance(getActivity()).setDoubtCount(doubtCount);

                    coinsCount.setText(success.getSuccess().getTotalCoins() + "");

                    String Question_date = success.getSuccess().getTrialDate();

                    if (Question_date.equals(null) && Question_date.equalsIgnoreCase("null")) {

                        RegPrefManager.getInstance(getActivity()).setExpireDate("null");

                    } else {

                        StringTokenizer que_date_time = new StringTokenizer(Question_date, "T");
                        String Date_ques = que_date_time.nextToken();
                        RegPrefManager.getInstance(getActivity()).setExpireDate(Date_ques);

                    }


                    ((NewDashboardActivity) getActivity()).setTrialDate();


                    RegPrefManager.getInstance(getActivity()).setIsDoubt(success.getSuccess().isIs_Doubt());
                    SharedPreferences preference = getContext().getSharedPreferences("paper_zone",
                            getActivity().MODE_PRIVATE);
                    int classID = success.getSuccess().getClassid();
                    preference.edit().putString("Boardid", String.valueOf(success.getSuccess().getBoardid())).apply();
                    preference.edit().putInt("classid", success.getSuccess().getClassid()).apply();
                    preference.edit().putString("schoolid", success.getSuccess().getSchoolid()).apply();
                    preference.edit().putString("sectionId", success.getSuccess().getSectionid()).apply();
                    preference.edit().putString("Is_Doubt", String.valueOf(success.getSuccess().isIs_Doubt())).apply();
                    preference.edit().putString("Is_Video", String.valueOf(success.getSuccess().isIs_Video())).apply();
                    preference.edit().putString("Is_Mentor", String.valueOf(success.getSuccess().isIs_Mentor())).apply();
                    preference.edit().putString("asignteacherid", success.getSuccess().getAsignteacherid()).apply();
                    preference.edit().putString("roleid", String.valueOf(success.getSuccess().getRoleid())).apply();
                    preference.edit().putString("Mentorid", String.valueOf(success.getSuccess().getMentorid())).apply();


                    RegPrefManager.getInstance(getActivity()).setSchoolId(
                            success.getSuccess().getSchoolid());

                    RegPrefManager.getInstance(getActivity()).setSectionId(
                            success.getSuccess().getSectionid());

                    if (success.getSuccess().getSchoolid() != null &&
                            success.getSuccess().getSectionid() != null)
                        RegPrefManager.getInstance(getActivity()).setIsSectionEnabled(true);


                    ((NewDashboardActivity) getActivity()).savePackageData(success.getSuccess().getPack());

                }


                break;


            case ResponseCodes.LOGOUTFAILURE:

                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(), ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(), ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(), Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }


}
