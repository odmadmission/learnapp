package com.sseduventures.digichamps.utils;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.sseduventures.digichamps.services.MyIntentService;

import java.util.Calendar;

public class AlarmManagerUtils
{

    //Registering sync service with alarm manager


    private static  String TAG=AlarmManagerUtils.class.getSimpleName();
    public static boolean isMyServiceRunning(Context context,Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static void setSyncAlarmMDM(Context context, boolean cancel) {

        if(!isMyServiceRunning(context,MyIntentService.class)) {
            ////Log.v(TAG, "setSyncAlarmMDM started");

            boolean alarmUp = (PendingIntent.getService(context, 100,
                    new Intent(Constants.ACTION_SET_ALARM),
                    PendingIntent.FLAG_NO_CREATE) != null);

            if (!alarmUp) {
                final AlarmManager am = (AlarmManager) context
                        .getSystemService(Context.ALARM_SERVICE);
                final Intent localIntent = new Intent(context, MyIntentService.class);
                localIntent.setAction(Constants.ACTION_SET_ALARM);
                final PendingIntent pi = PendingIntent.getService(context, 100,
                        localIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                if (pi == null) {
                    return;
                }
                if (cancel) {
                    pi.cancel();
                }
                final Calendar cal = Calendar.getInstance();
                final long millis = System.currentTimeMillis();
                cal.setTimeInMillis(millis);

                int interval =30*60*1000;
                am.setRepeating(AlarmManager.RTC_WAKEUP, millis,
                        (interval), pi);

            }

        }
    }

    public static void cancelSyncAlarmMDM(Context context, boolean cancel) {
        //  //Log.v(DEBUG_TAG,"cancelSyncAlarmMDM started");
        final AlarmManager am = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);
        final Intent localIntent = new Intent(context, MyIntentService.class);
        localIntent.setAction(Constants.ACTION_SET_ALARM);
        final PendingIntent pi = PendingIntent.getService(context, 100,
                localIntent, PendingIntent.FLAG_NO_CREATE);
        if (pi == null) {
            return;
        }
        if (cancel) {
            // //Log.v(DEBUG_TAG,"cancelAlarmMDM canceles");
            am.cancel(pi);
        }

    }


}
