package com.sseduventures.digichamps.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.Chapter_detail_Model;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.OnlinePlayerActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.activity.PackageActivityNew;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.utils.AppUtil;

import java.util.ArrayList;



public class Chapter_Details_Adapter extends RecyclerView.Adapter<Chapter_Details_Adapter.MyViewHolder> {
    View view;

    String chap_id, User_Id, sub_name, sub_id;
    private String  TAG = "Orders";

    private SpotsDialog dialog;
    private ImageView listener;

    private Context context;
    private ArrayList<Chapter_detail_Model> moviesList;

    public Chapter_Details_Adapter(ArrayList<Chapter_detail_Model> moviesList, Context context, String chap_id, String sub_name, String sub_id) {
        this.moviesList = moviesList;
        this.context = context;
        this.chap_id = chap_id;
        this.sub_name = sub_name;
        this.sub_id = sub_id;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_chaps_deatils_adapter, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Chapter_detail_Model chap_detail = moviesList.get(position);
        holder.title.setText(chap_detail.getModule_Title());
        Log.v(TAG, chap_detail.getModule_Image());
        if (chap_detail.getModule_Image() != null && !chap_detail.getModule_Image().isEmpty())
            Picasso.with(context).load(chap_detail.getModule_Image()).into(holder.thumbnail_image);


        User_Id = String.valueOf(RegPrefManager.getInstance(context).getRegId());

        holder.container.setOnClickListener(onClickListener(position, holder));




    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private View.OnClickListener onClickListener(final int position, final MyViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init();
                if (AppUtil.isInternetConnected(context)) {
                    if (moviesList.get(position).getVideoExpiry().equalsIgnoreCase("true")) {
                        if (moviesList.get(position).getIs_Free().equalsIgnoreCase("false")) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialogTheme_buy);
                            LayoutInflater factory = LayoutInflater.from(context);
                            final View view = factory.inflate(R.layout.dialog_buy, null);
                            Button ok = (Button) view.findViewById(R.id.button2);
                            final AlertDialog alertDialog = builder.create();
                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    Intent i = new Intent(context, PackageActivityNew.class);
                                    Activity activity = (Activity) context;
                                    context.startActivity(i);


                                }
                            });

                            builder.setView(view);
                            builder.setCancelable(true);


                            builder.show();//showing the dialog
                        } else {

                            Intent i = new Intent(context, OnlinePlayerActivity.class);
                            Activity activity = (Activity) context;
                            i.putParcelableArrayListExtra("list", moviesList);
                            i.putExtra("videoid", moviesList.get(position).get_file_resource());
                            i.putExtra("subjectid",sub_id);
                            i.putExtra("chapterid",chap_id);
                            i.putExtra("moduleid", moviesList.get(position).getModule_Id());
                            context.startActivity(i);


                        }
                    } else {

                        Intent i = new Intent(context, OnlinePlayerActivity.class);
                        Activity activity = (Activity) context;
                        i.putExtra("subjectid",sub_id);
                        i.putExtra("chapterid",chap_id);
                        i.putExtra("moduleid", moviesList.get(position).getModule_Id());
                        i.putParcelableArrayListExtra("list", moviesList);
                        i.putExtra("videoid", moviesList.get(position).get_file_resource());
                        context.startActivity(i);
                    }

                } else {
                    // redirect to No internet activity
                    Intent i = new Intent(context, Internet_Activity.class);
                    Activity activity = (Activity) context;

                    context.startActivity(i);

                }
            }

        };
    }


    private void init() {



        dialog = new SpotsDialog(context, "FUN + EDUCATION", R.style.Custom);


    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;
        ImageView left_drawable, right_drawable;
        LinearLayout container;
        private ImageView thumbnail_image;

        public MyViewHolder(View view) {
            super(view);
            container =  itemView.findViewById(R.id.card_dash);
            title = (TextView) view.findViewById(R.id.chap);

            thumbnail_image = (ImageView) itemView.findViewById(R.id.thumbnail_imagev);


        }
    }


}
