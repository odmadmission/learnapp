package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/13/2018.
 */

public class PostDisscussionDetailsRequest implements Parcelable{

    private int DiscussionId;
    private int CreatedBy;
    private int RoleID;
    private String DetailText;

    public int getDiscussionId() {
        return DiscussionId;
    }

    public void setDiscussionId(int discussionId) {
        DiscussionId = discussionId;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int createdBy) {
        CreatedBy = createdBy;
    }

    public int getRoleID() {
        return RoleID;
    }

    public void setRoleID(int roleID) {
        RoleID = roleID;
    }

    public String getDetailText() {
        return DetailText;
    }

    public void setDetailText(String detailText) {
        DetailText = detailText;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.DiscussionId);
        dest.writeInt(this.CreatedBy);
        dest.writeInt(this.RoleID);
        dest.writeString(this.DetailText);
    }

    public PostDisscussionDetailsRequest() {
    }

    protected PostDisscussionDetailsRequest(Parcel in) {
        this.DiscussionId = in.readInt();
        this.CreatedBy = in.readInt();
        this.RoleID = in.readInt();
        this.DetailText = in.readString();
    }

    public static final Creator<PostDisscussionDetailsRequest> CREATOR = new Creator<PostDisscussionDetailsRequest>() {
        @Override
        public PostDisscussionDetailsRequest createFromParcel(Parcel source) {
            return new PostDisscussionDetailsRequest(source);
        }

        @Override
        public PostDisscussionDetailsRequest[] newArray(int size) {
            return new PostDisscussionDetailsRequest[size];
        }
    };
}
