package com.sseduventures.digichamps.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activity.ClassListActivity;
import com.sseduventures.digichamps.activity.LeaderBoardActivity;
import com.sseduventures.digichamps.activity.MainActivity;
import com.sseduventures.digichamps.activity.TooperWayActivity;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.utils.FontManage;

import static com.sseduventures.digichamps.R.id.lblheading1;
import static com.sseduventures.digichamps.R.id.lblheading2;


/**
 * Created by user on 06-12-2017.
 */

public class TopperFragment extends Fragment {

    Context mContext;
    RelativeLayout btnLeader,btnTopperSheet;
    TextView llToolbar,lblheading1,ll_want,ll_want1,ll_data,ll_data1;
    TextView click1,click2,here1,here2,lblheading2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_topper, container, false);
        mContext = getActivity();
        if(getActivity() instanceof FormActivity)
        {
            ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_tooper_info);
            ((FormActivity)getActivity()).logEvent(LogEventUtil.KEY_tooper_info,
                    LogEventUtil.EVENT_tooper_info);
        }
        llToolbar = view.findViewById(R.id.llToolbar);
        ll_want = view.findViewById(R.id.ll_want);
        ll_want1 = view.findViewById(R.id.ll_want1);
        ll_data = view.findViewById(R.id.ll_data);
        ll_data1 = view.findViewById(R.id.ll_data1);
        click1 = view.findViewById(R.id.click1);
        click2 = view.findViewById(R.id.click2);
        here1 = view.findViewById(R.id.here1);
        here2 = view.findViewById(R.id.here2);
        lblheading1 = view.findViewById(R.id.lblheading1);
        lblheading2 = view.findViewById(R.id.lblheading2);

        btnTopperSheet =  view.findViewById(R.id.btnTopperSheet);
        btnLeader =  view.findViewById(R.id.btnLeader);
        btnLeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, LeaderBoardActivity.class);
                startActivity(i);
            }
        });

        btnTopperSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, TooperWayActivity.class);
                startActivity(i);
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                    Intent i = new Intent(mContext, MainActivity.class);
                    mContext.startActivity(i);
                    return true;
                } else {
                    return false;
                }
            }
        });


        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FontManage.setFontHeaderBold(mContext,llToolbar);
        FontManage.setFontImpact(mContext,lblheading1);
        FontManage.setFontTextLandingBold(mContext,ll_want);
        FontManage.setFontTextLandingBold(mContext,ll_data);
        FontManage.setFontTextLandingBold(mContext,ll_data1);
        FontManage.setFontTextLandingBold(mContext,ll_want1);
        FontManage.setFontMeiryo(mContext,click1);
        FontManage.setFontMeiryo(mContext,here1);
        FontManage.setFontMeiryo(mContext,click2);
        FontManage.setFontMeiryo(mContext,here2);
        FontManage.setFontImpact(mContext,lblheading2);

    }


}
