package com.sseduventures.digichamps.frags;



import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.BasicInfomationActivity;
import com.sseduventures.digichamps.activities.ChangePasswordActivity;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.OnBoardActivity;
import com.sseduventures.digichamps.activities.ResetPasswordActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.DateTimeUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.Random;

public class SignupSheetFragment extends BottomSheetDialogFragment
        implements ServiceReceiver.Receiver {

    private static final String TAG = "otp";
    private Button button;

    private TextInputLayout phone;
    private TextInputEditText input_phone;
    private String phone_number;

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private TextInputLayout otp_layout;
    private TextInputEditText otp_edit;
    private FormActivity mContext;
    private int otp;
    private TextView terms;
    private TextView otp_msg,resend_tv,error_phone,error_otp;

    private IntentFilter intentFilter;

    private ServiceReceiver mServiceReceiver;
    private Handler mHandler;

    private ImageView close_btn;
    boolean flag_resend=true;
    boolean flag_deny=false;

    public static SignupSheetFragment newInstance(int discount, String mobile) {

        Bundle bundle = new Bundle();
        bundle.putString("mobile", mobile);
        bundle.putInt("discount", discount);
        SignupSheetFragment bt = new SignupSheetFragment();
        bt.setArguments(bundle);

        return bt;
    }

    public SignupSheetFragment() {

    }




    private void init(View view) {
        EnableRuntimePermission();
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        intentFilter = new IntentFilter();
        intentFilter.addAction(IntentHelper.ACTION_SMS_RECIEVED);

        close_btn = view.findViewById(R.id.cloae_btn);

        button = view.findViewById(R.id.login_btn);
        input_phone = view.findViewById(R.id.input_phone);
        phone = view.findViewById(R.id.phone);

        error_phone=view.findViewById(R.id.error_phone);

        if(getArguments()!=null){

            input_phone.setText(getArguments().getString("mobile"));

        }

        TextView login = view.findViewById(R.id.alreadymember);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobile = input_phone.getText().toString().trim();
                dismiss();
                ((OnBoardActivity) getActivity()).showBottomSheetDialogFragment1(null);
            }
        });


        terms = view.findViewById(R.id.terms);


        input_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {

                error_phone.setVisibility(View.GONE);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag_resend=true;
                if (validation()) {
                    getSignupOTP();

                }
            }


        });

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
            }
        });

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse("http://www.thedigichamps.com/student/TermsAndConditions"));
                startActivity(browserIntent);
            }
        });


    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() != null && intent.getAction().
                    equals(IntentHelper.ACTION_SMS_RECIEVED)) {

                fillOtp();
            }

        }
    };

    private void fillOtp() {
        int rCode = RegPrefManager.getInstance(getActivity()).getSmsTestRecievedCode();
        otp_edit.setText(rCode + "");

    }

    private void dialog() {
        flag_resend=false; //open this dialog open or not
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.custom_dialog_otp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        Button btn = dialog.findViewById(R.id.btn);
        otp_layout = dialog.findViewById(R.id.otp_layout);
        otp_edit = dialog.findViewById(R.id.otp_edit);
        error_otp=dialog.findViewById(R.id.error_otp);

        otp_msg = dialog.findViewById(R.id.otp_msg);
        resend_tv= dialog.findViewById(R.id.resend_tv);

        otp_msg.setText("We've sent an sms with the OTP to " + input_phone.getText().toString().trim());
        resend_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //   Log.d("Tag","Tag");
                getSignupOTP();
            }
        });

        otp_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                error_otp.setVisibility(View.GONE);
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate(otp_layout, otp_edit)) {
                    String checkOtp = RegPrefManager.getInstance(getActivity())
                            .getSmsTestSavedCode() + "";

                    if (checkOtp.equals(otp_edit.getText().toString().trim())) {
                        dialog.dismiss();
                        Intent intent = new Intent(getActivity(),
                                BasicInfomationActivity.class);

                        intent.putExtra("mobileNum", input_phone.getText().toString().trim());

                        startActivity(intent);
                    } else {
                           Toast.makeText(getActivity(), "Wrong OTP", Toast.LENGTH_SHORT).show();

                    }
                }

            }
        });

        dialog.show();
    }

    private Boolean validation() {

        phone_number = input_phone.getText().toString().trim();

        if (phone_number.isEmpty()) {

            error_phone.setVisibility(View.VISIBLE);
            error_phone.setText("Please Enter Your Phone Number");
            return false;
        }

        if (phone_number.length() != 10) {

            error_phone.setVisibility(View.VISIBLE);
            error_phone.setText("Please Enter 10 Digit Mobile Number");
            return false;

        }

        return true;
    }
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        logEvent();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.activity_signup, container, false);
        ((FormActivity)getActivity()).logEvent(LogEventUtil.
                        KEY_sign_up,
                LogEventUtil.EVENT_sign_up);

        ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_sign_up);

        init(view);
        mContext = (OnBoardActivity) getActivity();

        return view;
    }


    private Boolean validate(final TextInputLayout otp_layout, final TextInputEditText otp_edit) {

        String otp_text = otp_edit.getText().toString().trim();

        if (otp_text.isEmpty() || otp_text.equals(null)) {

            error_otp.setVisibility(View.VISIBLE);
            error_otp.setText("Please Enter OTP");
            return false;
        }

        if (otp_text.length() != 6) {

            error_otp.setVisibility(View.VISIBLE);
            error_otp.setText("Enter 6 Digits OTP");
            return false;
        }

        return true;
    }


    private void getSignupOTP() {


        Random r = new Random(System.currentTimeMillis());
        otp = ((1 + r.nextInt(2)) * 100000 + r.nextInt(100000));


        Bundle bun = new Bundle();
        bun.putString("mobile", input_phone.getText().toString().trim());
        bun.putString("otp", otp + "");

        if (AppUtil.isInternetConnected(getActivity())) {

            mContext.showDialog(null, "The secret of getting started is getting ahead");
            NetworkService.startActionGetSignUpMobile(getActivity(), mServiceReceiver
                    , bun);

        } else {

            AskOptionDialogNew("Please check your Internet Connection").show();
        }

    }

    @Override
    public void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver,
                intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).
                unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        mContext.hideDialog();
       // dialog();
        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                RegPrefManager.getInstance(getActivity()).setSmsTestSavedCode(otp);
                if(flag_resend==true) {
                    if(flag_deny==false) {
                       dialog();
                    }
                }

                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(), ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(), ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(), Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.SERVER_ERROR:
                break;
            case ResponseCodes.USER_ALREADY_DONE:
                 AskOptionDialog("You are already an existing user, please login").show();


                break;
        }
    }

    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getContext())
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        dismiss();
                        ((OnBoardActivity) getActivity()).showBottomSheetDialogFragment1(input_phone.getText().toString().trim());
                    }
                })

                .create();
        return myQuittingDialogBox;
    }

    private AlertDialog AskOptionDialogNew(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getContext())
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();

                    }
                })

                .create();
        return myQuittingDialogBox;
    }





    private void EnableRuntimePermission() {
        int hasWriteContactsPermission =ActivityCompat. checkSelfPermission(getActivity(),Manifest.permission.RECEIVE_SMS);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {Manifest.permission.RECEIVE_SMS},
                    REQUEST_CODE_ASK_PERMISSIONS);
            return;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    flag_deny=true;
                    AskOptionDialogNew("Please Allow Permission").show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void logEvent()
    {
        Bundle bundle = new Bundle();
        bundle.putString("OnSignUp", DateTimeUtil.convertMillisAsDateTimeString(
                System.currentTimeMillis()));
        mFirebaseAnalytics.logEvent("Screen_OnSignUp", bundle);
    }

}
