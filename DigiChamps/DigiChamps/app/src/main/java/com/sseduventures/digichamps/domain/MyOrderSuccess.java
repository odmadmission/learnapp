package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class MyOrderSuccess implements Parcelable
{

    private List<Order> order;

    public List<Order> getOrder() {
        return order;
    }

    public void setOrder(List<Order> order) {
        this.order = order;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.order);
    }

    public MyOrderSuccess() {
    }

    protected MyOrderSuccess(Parcel in) {
        this.order = in.createTypedArrayList(Order.CREATOR);
    }

    public static final Creator<MyOrderSuccess> CREATOR = new Creator<MyOrderSuccess>() {
        @Override
        public MyOrderSuccess createFromParcel(Parcel source) {
            return new MyOrderSuccess(source);
        }

        @Override
        public MyOrderSuccess[] newArray(int size) {
            return new MyOrderSuccess[size];
        }
    };
}