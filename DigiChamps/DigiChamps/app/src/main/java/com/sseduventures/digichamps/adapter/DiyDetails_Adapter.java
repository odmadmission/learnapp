package com.sseduventures.digichamps.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.DIYOnlineVideoPlayer;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.domain.NewLearnDiyModuleLists;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.utils.AppUtil;

import java.util.ArrayList;


public class DiyDetails_Adapter  extends RecyclerView.Adapter<DiyDetails_Adapter.MyViewHolder> {
    View view;
    private boolean isInternetPresent = false;



    private SpotsDialog dialog;
    private ImageView listener;


    private Context context;
    private ArrayList<NewLearnDiyModuleLists> moviesList;

    public DiyDetails_Adapter(ArrayList<NewLearnDiyModuleLists> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static boolean checkImageResource(Context ctx, ImageView imageView,int imageResource) {
        boolean result = false;

        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }

            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }

        return result;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_diyvideo, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NewLearnDiyModuleLists chap_detail = moviesList.get(position);
        holder.title.setText(chap_detail.getDIYVideo_Name());
        Picasso.with(context).load(moviesList.get(position).getDiyPosterImage_production()).into(holder.thumbnail_image);
        listener = holder.play;
        holder.container.setOnClickListener(onClickListener(position, holder));


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private View.OnClickListener onClickListener(final int position, final MyViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init();
                if (isInternetPresent) {
//
                   Intent i = new Intent(context, DIYOnlineVideoPlayer.class);
                    Activity activity = (Activity) context;
                    i.putParcelableArrayListExtra("list",moviesList);
                    i.putExtra("videoId", moviesList.get(position).getDIYVideo_Upload());

                    context.startActivity(i);


                } else {
                    Intent i = new Intent(context, Internet_Activity.class);
                    Activity activity = (Activity) context;
                    context.startActivity(i);

                }
            }

        };
    }


    private void init() {



        isInternetPresent = AppUtil.isInternetConnected(context);

        dialog = new SpotsDialog(context, "FUN + EDUCATION", R.style.Custom);


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;
        public ImageView play;
        ImageView left_drawable, right_drawable;
        CardView container;
        private ImageView thumbnail_image;

        public MyViewHolder(View view) {
            super(view);
            container = (CardView) itemView.findViewById(R.id.card_view_chapter_diy);
            title = (TextView) view.findViewById(R.id.chapdiy);

            thumbnail_image = (ImageView) itemView.findViewById(R.id.thumbnail_imagev_diy);


        }
    }


}