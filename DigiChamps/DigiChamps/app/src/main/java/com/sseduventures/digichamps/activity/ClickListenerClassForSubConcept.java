package com.sseduventures.digichamps.activity;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by khushbu on 10/05/18.
 */

public class ClickListenerClassForSubConcept implements View.OnClickListener {

    private LinearLayout linearHidable;
    private boolean isClicked = false;

    public ClickListenerClassForSubConcept(LinearLayout linearHidable) {
        this.linearHidable = linearHidable;
    }

    @Override
    public void onClick(View v) {
        if(isClicked){
            linearHidable.setVisibility(View.GONE);
            isClicked = false;
        }else {
            linearHidable.setVisibility(View.VISIBLE);
            isClicked = true;
        }
    }
}
