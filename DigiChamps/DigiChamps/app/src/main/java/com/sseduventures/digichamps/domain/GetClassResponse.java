package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class GetClassResponse implements Parcelable{

    private List<GetClassListData> list;
    private boolean status;
    private String message;

    public List<GetClassListData> getList() {
        return list;
    }

    public void setList(List<GetClassListData> list) {
        this.list = list;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.list);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.message);
    }

    public GetClassResponse() {
    }

    protected GetClassResponse(Parcel in) {
        this.list = in.createTypedArrayList(GetClassListData.CREATOR);
        this.status = in.readByte() != 0;
        this.message = in.readString();
    }

    public static final Creator<GetClassResponse> CREATOR = new Creator<GetClassResponse>() {
        @Override
        public GetClassResponse createFromParcel(Parcel source) {
            return new GetClassResponse(source);
        }

        @Override
        public GetClassResponse[] newArray(int size) {
            return new GetClassResponse[size];
        }
    };
}
