package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/26/2018.
 */

public class EditProfileRequest implements Parcelable {


    private String Customer_Name;
    private int Board_ID;
    private int Class_ID;
    private String Email;
    private String Phone;
    private String Organisation_Name;
    private int Security_Question_ID;
    private String Security_Answer;
    private long Regd_ID;


    public String getCustomer_Name() {
        return Customer_Name;
    }

    public void setCustomer_Name(String customer_Name) {
        Customer_Name = customer_Name;
    }

    public int getBoard_ID() {
        return Board_ID;
    }

    public void setBoard_ID(int board_ID) {
        Board_ID = board_ID;
    }

    public int getClass_ID() {
        return Class_ID;
    }

    public void setClass_ID(int class_ID) {
        Class_ID = class_ID;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getOrganisation_Name() {
        return Organisation_Name;
    }

    public void setOrganisation_Name(String organisation_Name) {
        Organisation_Name = organisation_Name;
    }

    public int getSecurity_Question_ID() {
        return Security_Question_ID;
    }

    public void setSecurity_Question_ID(int security_Question_ID) {
        Security_Question_ID = security_Question_ID;
    }

    public String getSecurity_Answer() {
        return Security_Answer;
    }

    public void setSecurity_Answer(String security_Answer) {
        Security_Answer = security_Answer;
    }

    public long getRegd_ID() {
        return Regd_ID;
    }

    public void setRegd_ID(long regd_ID) {
        Regd_ID = regd_ID;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Customer_Name);
        dest.writeInt(this.Board_ID);
        dest.writeInt(this.Class_ID);
        dest.writeString(this.Email);
        dest.writeString(this.Phone);
        dest.writeString(this.Organisation_Name);
        dest.writeInt(this.Security_Question_ID);
        dest.writeString(this.Security_Answer);
        dest.writeLong(this.Regd_ID);
    }

    public EditProfileRequest() {
    }

    protected EditProfileRequest(Parcel in) {
        this.Customer_Name = in.readString();
        this.Board_ID = in.readInt();
        this.Class_ID = in.readInt();
        this.Email = in.readString();
        this.Phone = in.readString();
        this.Organisation_Name = in.readString();
        this.Security_Question_ID = in.readInt();
        this.Security_Answer = in.readString();
        this.Regd_ID = in.readLong();
    }

    public static final Creator<EditProfileRequest> CREATOR = new Creator<EditProfileRequest>() {
        @Override
        public EditProfileRequest createFromParcel(Parcel source) {
            return new EditProfileRequest(source);
        }

        @Override
        public EditProfileRequest[] newArray(int size) {
            return new EditProfileRequest[size];
        }
    };
}
