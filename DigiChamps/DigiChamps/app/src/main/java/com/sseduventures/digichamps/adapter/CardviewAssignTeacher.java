package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.AssignDetail;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.domain.AssignedTeacherDetails;
import com.sseduventures.digichamps.utils.FontManage;

import java.util.ArrayList;
import java.util.List;



public class CardviewAssignTeacher extends RecyclerView.Adapter<CardviewAssignTeacher.DataObjectHolder> {

    Context context;
    private List<AssignedTeacherDetails> list = new ArrayList<>();

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView tv_subject,tv_name,tv_email;
        ImageView imageView;

        public DataObjectHolder(View itemView) {
            super(itemView);
            tv_subject = (TextView) itemView.findViewById(R.id.tv_subject);

            imageView = (ImageView) itemView.findViewById(R.id.iv_maths);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_email = (TextView) itemView.findViewById(R.id.tv_email);
        }
    }

    public CardviewAssignTeacher(List<AssignedTeacherDetails> getlist, Context context) {
        this.list = getlist;
        this.context = context;
    }

    @Override
    public CardviewAssignTeacher.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_assign_teacher, parent, false);

        CardviewAssignTeacher.DataObjectHolder dataObjectHolder = new CardviewAssignTeacher.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final CardviewAssignTeacher.DataObjectHolder holder, final int position) {
        holder.tv_subject.setText(list.get(position).getSubjectName());
        holder.tv_name.setText(list.get(position).getTeacherName());
        holder.tv_email.setText(list.get(position).getEmailAddress());

        FontManage.setFontImpact(context,holder.tv_subject);
        FontManage.setFontMeiryo(context,holder.tv_name);
        FontManage.setFontMeiryo(context,holder.tv_email);

        if(list.get(position).getImageUrl()!=null)
        {
            Picasso.with(context).load(list.get(position).getImageUrl()).
                    error(context.getResources().getDrawable(R.mipmap.ic_launcher)).
            placeholder(context.getResources().getDrawable(R.mipmap.ic_launcher)).
                    into(holder.imageView)
            ;
        }
        else
        {
            Picasso.with(context).load(R.mipmap.ic_launcher).
                    error(context.getResources().getDrawable(R.mipmap.ic_launcher)).
                    placeholder(context.getResources().getDrawable(R.mipmap.ic_launcher)).
                    into(holder.imageView)
            ;
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}

