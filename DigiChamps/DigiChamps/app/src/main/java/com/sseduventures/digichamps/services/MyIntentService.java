package com.sseduventures.digichamps.services;

import android.Manifest;
import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sseduventures.digichamps.Model.ProgressModule;
import com.sseduventures.digichamps.Model.ReportModule;
import com.sseduventures.digichamps.Model.VideoUsageTime;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.database.DbContract;
import com.sseduventures.digichamps.domain.CoinLeaderboardResponse;
import com.sseduventures.digichamps.domain.ReportModel;
import com.sseduventures.digichamps.domain.StudentActivityTime;
import com.sseduventures.digichamps.domain.UserContact;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.webservice.RestService;
import com.sseduventures.digichamps.webservice.RestServiceBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class MyIntentService   extends IntentService {

    private static String DEBUG_TAG=MyIntentService.class.getSimpleName();

    public MyIntentService() {
        // Used to name the worker thread
        // Important only for debugging
        super(MyIntentService.class.getName());

    }


        @Override
        public void onCreate() {
            super.onCreate(); // if you override onCreate(), make sure to call super().
        }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Invoked on the worker thread
        // Do some work in background without affecting the UI thread

        //Log.v(DEBUG_TAG,"started");
        if(intent!=null&&intent.getAction()!=null)
        {
            if(intent.getAction().equals(Constants.ACTION_SET_ALARM))
            {
                syncVideoUsageTime();
                // syncReports();
                 syncActivityTimeLogs();

                 syncReports();
                if(!getSharedPreferences("user_data",
                        getApplicationContext().MODE_PRIVATE).
                        getBoolean("UploadContacts",false)) {
                   // uploadContacts();
                }
            }
        }
      }

    private void syncActivityTimeLogs()
    {

        ArrayList<StudentActivityTime> list=new ArrayList<>();
        try
        {
            Cursor cursor=getContentResolver()
                    .query(DbContract.ActivityTimeTable.CONTENT_URI,
                            DbContract.ActivityTimeTable.MEMBER_PROJECTION,
                            null,null,null);

            if(cursor!=null&&cursor.getCount()>0)
            {
               // Log.v(DEBUG_TAG,cursor.getCount()+"");

                 cursor.moveToFirst();
                 do {

                    StudentActivityTime t=new StudentActivityTime();
                    t.setModuleName(cursor.getString(cursor
                    .getColumnIndex(DbContract.ActivityTimeTable.MODULEID)));


                     t.setActive(true);

                     t.setActivityTime(
                             cursor.getLong(cursor
                                     .getColumnIndex(DbContract.
                                             ActivityTimeTable.END_TIME))-
                             cursor.getLong(cursor
                             .getColumnIndex(DbContract.
                                     ActivityTimeTable.START_TIME)));

                     t.setSchoolId(cursor.getString(cursor
                             .getColumnIndex(DbContract.ActivityTimeTable.SCHOOL_CODE)));


                     t.setSectionId(cursor.getString(cursor
                             .getColumnIndex(DbContract.ActivityTimeTable.SECTION_CODE)));


                     t.setClassId(cursor.getLong(cursor
                             .getColumnIndex(DbContract.
                                     ActivityTimeTable.CLASS_CODE)));

                     t.setRegdId(cursor.getLong(cursor
                             .getColumnIndex(DbContract.
                                     ActivityTimeTable.REGDID)));


                     t.setStartTime(new Date(cursor.getLong(cursor
                             .getColumnIndex(DbContract.
                                     ActivityTimeTable.START_TIME))));

                     t.setEndTime(new Date(cursor.getLong(cursor
                             .getColumnIndex(DbContract.
                                     ActivityTimeTable.END_TIME))));


                     t.setCreateDate(new Date(cursor.getLong(cursor
                             .getColumnIndex(DbContract.
                                     ActivityTimeTable.CREATE_DATE))));


                     t.setSyncDate(new Date(cursor.getLong(cursor
                             .getColumnIndex(DbContract.
                                     ActivityTimeTable.SYNC_DATE))));

                     t.setRowId(cursor.getInt(cursor
                             .getColumnIndex(DbContract.
                                     ActivityTimeTable._ID)));



                     list.add(t);



                }
                while (cursor.moveToNext());
                 cursor.close();

            }


           // Log.v(DEBUG_TAG,"Activity Time Size : "+list.size());

            if(list.size()>0)
            {
                RestService restService=RestServiceBuilder.get().getRestService();
               retrofit2.Response<ResponseBody> resp=
                       restService.activityTimeCreate(list).execute();

               if(resp!=null&&resp.code()==201)
               {
                   JSONArray jsonObject = new JSONArray(resp.body().string());

                   if (jsonObject != null) {
                       ArrayList<StudentActivityTime> myclass1Object = new Gson().
                               fromJson(jsonObject.toString(),
                                       new TypeToken<ArrayList<StudentActivityTime>>() {
                                       }.getType());
                       if (myclass1Object != null) {
                           ArrayList<ContentProviderOperation> ops = new ArrayList<>();
                           for (int i = 0; i < myclass1Object.size(); i++) {
                               ops.add(ContentProviderOperation
                                       .newDelete(DbContract.ActivityTimeTable.CONTENT_URI)
                                       .withSelection(DbContract.ActivityTimeTable._ID + " like ? ",
                                               new String[]{myclass1Object.get(i).getRowId() + ""})


                                       .build());

                           }

                           if (ops.size() > 0) {
                               try {

                                   ContentProviderResult[] results= getContentResolver().
                                           applyBatch(DbContract.Schema.CONTENT_AUTHORITY, ops);

                                   int count=0;
                                   for (int i=0;i<results.length;i++)
                                   {
                                       if(results[i].uri!=null)
                                           count=count+1;
                                       else
                                       if(results[i].count>0)
                                           count=count+1;

                                   }
                                 //  Log.v(DEBUG_TAG,"results count : "+count+"");



                               } catch (RemoteException e) {
                                   // do s.th.
                                  // Log.v(DEBUG_TAG, e.getMessage());
                               } catch (OperationApplicationException e) {
                                   // do s.th.
                                  // Log.v(DEBUG_TAG, e.getMessage());
                               }
                           }

                       }
                   }
               }

            }

        }
        catch (Exception e)
        {
           // Log.v(DEBUG_TAG,e.getMessage()+"");
        }
    }

    public ArrayList<ProgressModule> getAllProgressModules() {
        ArrayList<ProgressModule> contactList = new ArrayList<ProgressModule>();
        Cursor cursor = getContentResolver()
                .query(DbContract.ProgressTable.CONTENT_URI,
                        DbContract.ProgressTable.MEMBER_PROJECTION,
                        null,null,null);

        if (cursor!=null&&cursor.moveToFirst()) {
            try {
                do {
                    ProgressModule contact = new ProgressModule();

                    contact.setRowId(cursor.getInt(cursor.
                            getColumnIndex(DbContract.ProgressTable.KEY_ID)));

                    contact.setId(cursor.getInt(cursor.
                            getColumnIndex(DbContract.ProgressTable.KEY_ID)));
                    //    contact.setImageCode(cursor.getBlob(cursor.getColumnIndex(Datas.KEY_IMAGECODE)));

                    contact.setModule_Type(cursor.getString(cursor.getColumnIndex(
                            DbContract.ProgressTable.KEY_MODULE_TYPE
                    )));
                    contact.setSchool_ID(cursor.getString(cursor.getColumnIndex(
                            DbContract.ProgressTable.KEY_SCHOOL_ID
                    )));
                    contact.setSubject_ID(cursor.getInt(cursor.getColumnIndex(
                            DbContract.ProgressTable.KEY_SUBJECT_ID
                    )));
                    contact.setChapter_ID(cursor.getInt(cursor.getColumnIndex(
                            DbContract.ProgressTable.KEY_CHAPTER_ID
                    )));
                    contact.setClass_ID(cursor.getInt(cursor.getColumnIndex(
                            DbContract.ProgressTable.KEY_CLASS_ID
                    )));
                    contact.setSection_ID(cursor.getString(cursor.getColumnIndex(
                            DbContract.ProgressTable.KEY_SECTION_ID
                    )));
                    contact.setRegd_ID(cursor.getInt(cursor.getColumnIndex(
                            DbContract.ProgressTable.KEY_REG_ID
                    )));
                    contact.setModule_ID(cursor.getInt(cursor.getColumnIndex(
                            DbContract.ProgressTable.KEY_MODULE_ID
                    )));
                    contact.setInserted_On(new Date(cursor.getLong(cursor.getColumnIndex(
                            DbContract.ProgressTable.KEY_INSERT_DATE_TIME)
                    )));
                    // contact.setR(cursor.getString(cursor.getColumnIndex(KEY_SECTION_ID)));


                   // Log.v(DEBUG_TAG, contact.toString());
                    // Adding contact to list
                    contactList.add(contact);

                } while (cursor.moveToNext());
                cursor.close();
            } catch (Exception e) {
                //Log.v(DEBUG_TAG, "Exception : "+e.getMessage());
            }
        }
        //Log.v(DEBUG_TAG, "Progress list size: "+contactList.size());


        // return contact list
        return contactList;
    }
    private ArrayList<VideoUsageTime> getData()
    {
        ArrayList<VideoUsageTime> contactList = new ArrayList<VideoUsageTime>();
        Cursor cursor =
                getContentResolver().query(DbContract.VideoLogTable.CONTENT_URI,
                        DbContract.VideoLogTable.MEMBER_PROJECTION,
                        DbContract.VideoLogTable.STATUS+" LIKE ? ",new String[]{2+""},null);

        if (cursor!=null&&cursor.moveToFirst()) {
            try {
                do {
                    VideoUsageTime contact = new VideoUsageTime();


                    contact.setRowId(cursor.getLong(cursor.
                            getColumnIndex(DbContract.VideoLogTable._ID)));
                    contact.setInsertedOn(
                            new Date());
                    contact.setUsageDate(
                            new Date(cursor.getLong(cursor.
                                    getColumnIndex(DbContract.VideoLogTable.START_TIME))));

                    contact.setStartDate(
                            new Date(cursor.getLong(cursor.
                            getColumnIndex(DbContract.VideoLogTable.START_TIME))));
                    contact.setEndDate(new Date(cursor.getLong(cursor.
                            getColumnIndex(DbContract.VideoLogTable.END_TIME))));
                    contact.setPlayTime(cursor.getLong(cursor.
                            getColumnIndex(DbContract.VideoLogTable.SEEN_TIME)));
                    contact.setRegdId(cursor.getLong(cursor.
                            getColumnIndex(DbContract.VideoLogTable.REGDID)));
                    contact.setVideoId(cursor.getLong(cursor.
                            getColumnIndex(DbContract.VideoLogTable.MODULEID)));
                    contact.setClassId(cursor.getLong(cursor.
                            getColumnIndex(DbContract.VideoLogTable.CLASS_CODE)));
                    contact.setSectionId(cursor.getString(cursor.
                            getColumnIndex(DbContract.VideoLogTable.SECTION_CODE)));
                    contact.setSchoolId(cursor.getString(cursor.
                            getColumnIndex(DbContract.VideoLogTable.SCHOOL_CODE)));


                  //  Log.v(DEBUG_TAG, contact.toString());
                    // Adding contact to list
                    contactList.add(contact);

                } while (cursor.moveToNext());
            } catch (Exception e) {
                //Log.v(DEBUG_TAG, "Exception : "+e.getMessage());
            }
        }
    //   Log.v(DEBUG_TAG, "Report list size: "+contactList.size());
        cursor.close();
        // return contact list
        return contactList;
    }



    private void syncVideoUsageTime()
    {
        try {
            ArrayList<VideoUsageTime> reportModules =getData();


            boolean status=true;
            if(reportModules.size()>0) {

                RestService restService = RestServiceBuilder.
                        get().getRestService();
                retrofit2.Response<ResponseBody> resp = restService.
                        saveVideoUsage(reportModules).execute();


                if (resp != null && resp.code() == 201) {

                    JSONArray jsonObject = new JSONArray(resp.body().string());

                    if (jsonObject != null) {
                        ArrayList<VideoUsageTime> myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        new TypeToken<ArrayList<VideoUsageTime>>() {
                                        }.getType());

                        if (myclass1Object != null) {
                            ArrayList<ContentProviderOperation> ops = new ArrayList<>();
                            for (int i = 0; i < myclass1Object.size(); i++)
                            {
                                if(myclass1Object.get(i).getRowId()>0) {
                                    ops.add(ContentProviderOperation
                                            .newDelete(DbContract.VideoLogTable.CONTENT_URI)
                                            .withSelection(DbContract.
                                                            VideoLogTable._ID + " like ? ",
                                                    new String[]{myclass1Object
                                                            .get(i).getRowId() + ""})


                                            .build());
                                }

                            }


                            if (ops.size() > 0) {
                                try {

                                    ContentProviderResult[] results= getContentResolver().
                                            applyBatch(DbContract.Schema.CONTENT_AUTHORITY, ops);

                                    int count=0;
                                    for (int i=0;i<results.length;i++)
                                    {
                                        if(results[i].uri!=null)
                                            count=count+1;
                                        else
                                        if(results[i].count>0)
                                            count=count+1;

                                    }
                                 //   Log.v(DEBUG_TAG,"results count : "+count+"");


                                } catch (RemoteException e) {
                                    // do s.th.
                                    //  Log.v(DEBUG_TAG, e.getMessage());
                                } catch (OperationApplicationException e) {
                                    // do s.th.
                                   //  Log.v(DEBUG_TAG, e.getMessage());
                                }
                            }

                        }

                    }
                }
            }

        }
        catch (Exception e)
        {
            // Log.v(DEBUG_TAG,e.getMessage()+"");
        }
    }


      private void syncReports()
      {
          try {
              ArrayList<ReportModule> reportModules =new ArrayList<>();
              ArrayList<ProgressModule> progressModules = getAllProgressModules();

             // Log.v(DEBUG_TAG,"Report Module size : "+reportModules.size());
             // Log.v(DEBUG_TAG,"Progress Module size : "+progressModules.size()+"");
              ReportModel reportModel=new ReportModel();
              reportModel.setReportModules(reportModules);
              reportModel.setProgressModules(progressModules);

               boolean status=true;
              if(progressModules.size()>0) {

                  RestService restService = RestServiceBuilder.
                          get().getRestService();
                  retrofit2.Response<ResponseBody> resp =
                          restService.saveReports(reportModel).execute();


                  if (resp != null && resp.code() == 200) {

                      JSONObject jsonObject = new JSONObject(resp.body().string());

                      if (jsonObject != null) {
                          ReportModel myclass1Object = new Gson().
                                  fromJson(jsonObject.toString(), ReportModel.class);
                          if (myclass1Object != null) {
                              ArrayList<ContentProviderOperation> ops = new ArrayList<>();
                              for (int i = 0; i < myclass1Object.getProgressModules().size(); i++)
                              {
                                  if(myclass1Object.getProgressModules().get(i).getRowId()==-1) {
                                      ops.add(ContentProviderOperation
                                              .newDelete(DbContract.ProgressTable.CONTENT_URI)
                                              .withSelection(DbContract.
                                                              ProgressTable.KEY_ID + " like ? ",
                                                      new String[]{myclass1Object.getProgressModules()
                                                              .get(i).getId() + ""})


                                              .build());
                                  }

                              }


                              if (ops.size() > 0) {
                                  try {

                                    ContentProviderResult[] results= getContentResolver().
                                              applyBatch(DbContract.Schema.CONTENT_AUTHORITY, ops);

                                      int count=0;
                                      for (int i=0;i<results.length;i++)
                                      {
                                          if(results[i].uri!=null)
                                              count=count+1;
                                          else
                                          if(results[i].count>0)
                                              count=count+1;

                                      }
                                     // Log.v(DEBUG_TAG,"results count : "+count+"");
                                      if(count>0)
                                      {
                                          status=true;
                                      }

                                  } catch (RemoteException e) {
                                      // do s.th.
                                      //  Log.v(DEBUG_TAG, e.getMessage());
                                  } catch (OperationApplicationException e) {
                                      // do s.th.
                                      // Log.v(DEBUG_TAG, e.getMessage());
                                  }
                              }

                          }
                      }
                  }
              }

          }
          catch (Exception e)
          {
             // Log.v(DEBUG_TAG,e.getMessage()+"");
          }

      }


    private void uploadContacts()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED) {
            //   Log.v(TAG, "uploadContacts started");

            ArrayList<UserContact> contactArrayList = new ArrayList<>();
            String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                    + " COLLATE LOCALIZED ASC";
            ;
            String[] projection = new String[]{
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER
                    //plus any other properties you wish to query
            };
            SharedPreferences preference =getSharedPreferences("user_data",
                    getApplicationContext().MODE_PRIVATE);
            String user_id = preference.getString("User_ID", null);
            int userId=0;
            if(user_id!=null)
            {
                userId=Integer.parseInt(user_id);
            }
            Cursor cursor = getContentResolver().
                    query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, null, null, null);
            if (cursor != null) {

                try {
                    HashSet<String> normalizedNumbersAlreadyFound = new HashSet<>();
                    //int indexOfNormalizedNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER);
                    int indexOfDisplayName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                    int indexOfDisplayNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

                    while (cursor.moveToNext()) {

                        //String normalizedNumber = cursor.getString(indexOfNormalizedNumber);
                        String normalizedNumber = cursor.getString(indexOfDisplayNumber);
                        if (normalizedNumber != null) {
                            normalizedNumber = normalizedNumber.replaceAll("[^0-9]", "");
                        }
                        if (normalizedNumbersAlreadyFound.add(normalizedNumber)) {
                            String displayName = cursor.getString(indexOfDisplayName);
                            String displayNumber = cursor.getString(indexOfDisplayNumber);
                            displayNumber = displayNumber.replaceAll("[^0-9]", "");
                            //haven't seen this number yet: do something with this contact!
                            UserContact contact = new UserContact();
                            contact.setRegId(userId);
                            contact.setName(displayName);
                            contact.setMobile(displayNumber);
                            contactArrayList.add(contact);
                        } else {
                            //don't do anything with this contact because we've already found this number
                        }
                    }
                } finally {
                    cursor.close();
                }
            }

            try {

                if (contactArrayList.size() > 0) {
                    //Log.v(TAG, "Contacts to be upload  List size : " + contactArrayList.size());
                    Call<List<UserContact>> response = RestServiceBuilder.get().
                            getRestService().UploadContacts(contactArrayList);

                    retrofit2.Response<List<UserContact>> contactListResponse = response.execute();
                    if (contactListResponse.code() == HttpURLConnection.HTTP_OK) {
                        //Log.v(TAG, "Saved List size : " + contactListResponse.body().size());
                        preference.edit().putBoolean("UploadContacts",true).commit();


                    } else if (contactListResponse.code() == HttpURLConnection.HTTP_NOT_FOUND) {

                     //   DataManager.clearDatabase(this);
                    }

                }

            } catch (Exception e) {
                // Log.v(TAG, e.getMessage() + "");
            }

        }



    }

}
