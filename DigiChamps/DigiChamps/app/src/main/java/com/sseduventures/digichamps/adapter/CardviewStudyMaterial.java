package com.sseduventures.digichamps.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.sseduventures.digichamps.Model.StudyMaterialList;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.StudyMaterialDetailActivity;
import com.sseduventures.digichamps.domain.StudyMaterialsList;
import com.sseduventures.digichamps.utils.FontManage;

import java.util.ArrayList;
import java.util.List;



public class CardviewStudyMaterial extends RecyclerView.Adapter<CardviewStudyMaterial.DataObjectHolder> {

    Context context;
    private List<StudyMaterialsList> list = new ArrayList<>();

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        RelativeLayout rel_master;
        android.widget.RadioButton RadioButton;
        TextView tv_subject,tv_material_type,tv_topic_name,tv_date_lbl,tv_material_lbl;

        public DataObjectHolder(View itemView) {
            super(itemView);
            tv_subject = (TextView) itemView.findViewById(R.id.tv_subject);
            tv_material_type = (TextView) itemView.findViewById(R.id.tv_material_type);
            tv_topic_name = (TextView) itemView.findViewById(R.id.tv_topic_name);
            tv_date_lbl = (TextView) itemView.findViewById(R.id.tv_date_lbl);
            tv_material_lbl = (TextView) itemView.findViewById(R.id.tv_material_lbl);
            rel_master = (RelativeLayout) itemView.findViewById(R.id.rel_master);
        }
    }

    public CardviewStudyMaterial(List<StudyMaterialsList> getlist, Context context) {
        this.list = getlist;
        this.context = context;
    }

    @Override
    public CardviewStudyMaterial.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_study_material, parent, false);

        CardviewStudyMaterial.DataObjectHolder dataObjectHolder = new CardviewStudyMaterial.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final CardviewStudyMaterial.DataObjectHolder holder, final int position) {

        if(position%2==0){
            holder.tv_topic_name.setBackground(context.getResources().getDrawable(R.drawable.syudymaterielsvoilet));
        }else{
            holder.tv_topic_name.setBackground(context.getResources().getDrawable(R.drawable.studymaterielspink));
        }
        String topic_name="",material_type="",subject="";
        if(!list.get(position).getTopic().equals("")){
             topic_name = list.get(position).getTopic().substring(0,1).toUpperCase() + list.get(position).getTopic().substring(1);
        }
        if(!list.get(position).getMaterialType().equals("")){
            material_type = list.get(position).getMaterialType().substring(0,1).toUpperCase() + list.get(position).getMaterialType().substring(1);
        }
        if(!list.get(position).getSubject().equals("")){
            subject = list.get(position).getSubject().substring(0,1).toUpperCase() + list.get(position).getSubject().substring(1);
        }



        holder.tv_topic_name.setText(topic_name);
        holder.tv_subject.setText(subject);
        holder.tv_material_type.setText(material_type);

        FontManage.setFontImpact(context,holder.tv_topic_name);
        FontManage.setFontImpact(context,holder.tv_date_lbl);
        FontManage.setFontImpact(context,holder.tv_material_lbl);
        FontManage.setFontMeiryoBold(context,holder.tv_subject);
        FontManage.setFontMeiryoBold(context,holder.tv_material_type);

        holder.rel_master.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,StudyMaterialDetailActivity.class);
                i.putExtra("createdDate",list.get(position).getCreatedDate());
                i.putExtra("fileURL",list.get(position).getFileURL());
                i.putExtra("fileType",list.get(position).getFileType());
                i.putExtra("materialType",list.get(position).getMaterialType());
                i.putExtra("subject",list.get(position).getSubject());
                i.putExtra("topic",list.get(position).getTopic());
                i.putExtra("studyMaterial",list.get(position).getStudyMaterial());
                context.startActivity(i);
                Activity activity = (Activity) context;
                activity.overridePendingTransition(R.anim.from_middle, R.anim.to_middle);

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}

