package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.Preferences;


public class VideoPlayActivity extends AppCompatActivity {

    Context mContext;
    WebView ww_webview;
    RecyclerView recyclerview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        mContext = this;
        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ww_webview = findViewById(R.id.ww_webview);
        ww_webview.getSettings().setJavaScriptEnabled(true);
        ww_webview.loadUrl(Constants.IMAGE_URL+Preferences.get(mContext, Preferences.KEY_SCHOOL_VIDEO));

    }

}
