package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/25/2018.
 */

public class PMCompletedTask implements Parcelable {

    private List<PMCompletedtaskDataList> taskDataList;


    public List<PMCompletedtaskDataList> getTaskDataList() {
        return taskDataList;
    }

    public void setTaskDataList(List<PMCompletedtaskDataList> taskDataList) {
        this.taskDataList = taskDataList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.taskDataList);
    }

    public PMCompletedTask() {
    }

    protected PMCompletedTask(Parcel in) {
        this.taskDataList = in.createTypedArrayList(PMCompletedtaskDataList.CREATOR);
    }

    public static final Creator<PMCompletedTask> CREATOR = new Creator<PMCompletedTask>() {
        @Override
        public PMCompletedTask createFromParcel(Parcel source) {
            return new PMCompletedTask(source);
        }

        @Override
        public PMCompletedTask[] newArray(int size) {
            return new PMCompletedTask[size];
        }
    };
}
