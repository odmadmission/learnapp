package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/5/2018.
 */

public class GetCartDataResponse implements Parcelable
{
   private GetCartDataSuccess success;

    public GetCartDataSuccess getSuccess() {
        return success;
    }

    public void setSuccess(GetCartDataSuccess success) {
        this.success = success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.success, flags);
    }

    public GetCartDataResponse() {
    }

    protected GetCartDataResponse(Parcel in) {
        this.success = in.readParcelable(GetCartDataSuccess.class.getClassLoader());
    }

    public static final Creator<GetCartDataResponse> CREATOR = new Creator<GetCartDataResponse>() {
        @Override
        public GetCartDataResponse createFromParcel(Parcel source) {
            return new GetCartDataResponse(source);
        }

        @Override
        public GetCartDataResponse[] newArray(int size) {
            return new GetCartDataResponse[size];
        }
    };
}
