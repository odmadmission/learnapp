package com.sseduventures.digichamps.helper;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sseduventures.digichamps.Model.Model_Subject;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.adapter.CustomExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ntspl22 on 7/5/2017.
 */


public class SpotsDialog {

    private String value;
    private Context context;
    private int theme;
    private AlertDialog dialog;


    //constructor for spots dialog containing context,value,theme
 public SpotsDialog(Context context, String value, int theme){
     this.context = context;
     this.value = value;
     this.theme = theme;

     AlertDialog.Builder builder = new AlertDialog.Builder(context);
     LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
     View view = layoutInflater.inflate(R.layout.alert_dialogue_srv_desc, null );
     builder.setView(view);
     view.setBackgroundColor(Color.TRANSPARENT);

     dialog = builder.create();
     ImageView webview = (ImageView) view.findViewById(R.id.web_view);
     if(webview != null){
         //webview.loadUrl("file:///android_res/drawable/splash.gif");
       //  Glide.with(context).load(R.drawable.splash).into(webview);
     }

     //webview.setBackgroundColor(context.getResources().getColor(R.color.transparent));
     webview.setBackgroundColor(Color.TRANSPARENT);
     Window window = dialog.getWindow();
     WindowManager.LayoutParams wlp = window.getAttributes();
    // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
     dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
     window.setAttributes(wlp);
     dialog.setCancelable(false);
 }
     public void showDialog(){
       dialog.show();

         }



    public void hideDialog(){

         if(dialog.isShowing())
             dialog.dismiss();


    }
    public String convert(String val){
        int number = (int) Double.parseDouble(val);
        return String.valueOf(number);
    }

    public String discount(String val1,String val2){
        int number1 = (int) Double.parseDouble(val1);
        int number2 = (int) Double.parseDouble(val2);
        String result = String.valueOf(number1-number2);
        return result;
    }

    public void ShowCondition(String title,String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.alert_dialogue_menu, null );
        builder.setView(view);
        //view.setBackgroundColor(Color.TRANSPARENT);

        final AlertDialog dialog1 = builder.create();
        TextView header = (TextView) view.findViewById(R.id.tv_header);
        TextView text = (TextView) view.findViewById(R.id.tv_text);
        Button ok = (Button) view.findViewById(R.id.bt_ok);
        header.setText(title);

        Typeface face = Typeface.createFromAsset(context.getAssets(),
                "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");
        header.setTypeface(face);
        text.setText(Message);
        text.setTypeface(face);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();

        //webview.setBackgroundColor(context.getResources().getColor(R.color.transparent));

        Window window = dialog1.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setAttributes(wlp);

    }

}
