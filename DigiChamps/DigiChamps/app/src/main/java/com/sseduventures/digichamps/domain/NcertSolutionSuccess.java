package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/3/2018.
 */

public class NcertSolutionSuccess implements Parcelable{

    private List<NcertSolutionListData> list;

    public List<NcertSolutionListData> getList() {
        return list;
    }

    public void setList(List<NcertSolutionListData> list) {
        this.list = list;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.list);
    }

    public NcertSolutionSuccess() {
    }

    protected NcertSolutionSuccess(Parcel in) {
        this.list = in.createTypedArrayList(NcertSolutionListData.CREATOR);
    }

    public static final Creator<NcertSolutionSuccess> CREATOR = new Creator<NcertSolutionSuccess>() {
        @Override
        public NcertSolutionSuccess createFromParcel(Parcel source) {
            return new NcertSolutionSuccess(source);
        }

        @Override
        public NcertSolutionSuccess[] newArray(int size) {
            return new NcertSolutionSuccess[size];
        }
    };
}
