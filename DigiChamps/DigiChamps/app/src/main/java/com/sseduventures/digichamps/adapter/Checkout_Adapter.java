package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.DataModel_Checkout;
import com.sseduventures.digichamps.Model.Package_Model;
import com.sseduventures.digichamps.R;

import java.util.ArrayList;
import java.util.List;



public class Checkout_Adapter extends RecyclerView.Adapter<Checkout_Adapter.MyViewHolder> {

    private List<DataModel_Checkout> cartList;
    public static int position;
    static CardView container, container1;
    public static RelativeLayout relative;
    public ArrayList<Package_Model> selected_usersList = new ArrayList<>();

    Context mContext;
    View view;
    private int discount=0;



    private Context context;



    public Checkout_Adapter(ArrayList<DataModel_Checkout> data,
                            Context context,int discount) {
        this.context = context;
        this.cartList = data;
        this.discount=discount;

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.checkout_card_new, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView text_discount = holder.text_discount;
        TextView text_price = holder.text_price;
        TextView month = holder.month;
        TextView chap = holder.chap;
        TextView status = holder.status;
        TextView txt_saving = holder.txt_saving;

        month.setText(cartList.get(holder.getAdapterPosition()).getMonth());
        chap.setText(cartList.get(holder.getAdapterPosition()).getchap());
        status.setText(cartList.get(holder.getAdapterPosition()).getStatus());

        container1 = holder.container;

        holder.container.setOnClickListener(onClickListener(listPosition));


            holder.imageView_pack.setImageResource(R.drawable.champ_pack);
            holder.chapter.setText(cartList.get(listPosition).getpackage_name());
            holder.imageView_offer.setImageResource(R.drawable.offer_pack);
            Picasso.with(context).load(cartList.get(listPosition).getImage()).into( holder.imageView_pack);
            holder.offer_name.setText("");

            holder.linear_pack.setVisibility(View.VISIBLE);
        holder.text_price.setPaintFlags(holder.text_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


        if(cartList.get(listPosition).getpackage_name().contains("BEGINNER"))
            holder.chapter.setText("BEGINNER");
        else
        if(cartList.get(listPosition).getpackage_name().contains("PRO"))
            holder.chapter.setText("PRO");
        else
        if(cartList.get(listPosition).getpackage_name().contains("EXPERT"))
            holder.chapter.setText("EXPERT");
        else
        if(cartList.get(listPosition).getpackage_name().contains("CHAMPION"))
            holder.chapter.setText("CHAMPION");

        if(discount==1){


            String newPrice = cartList.get(listPosition).getActual_price();


            int afterPrice = Integer.parseInt(newPrice);
            double discPrice = .65*afterPrice;

            text_discount.setText("\u20B9 "+(int)discPrice);

            int totalSavingNew = afterPrice - (int)discPrice;
            txt_saving.setText("\u20B9 "+(int)totalSavingNew);

            holder.offer.setText("35"+"% Discount");
            holder.text_price.setText(Html.fromHtml("<strike>\u20B9 "+cartList.get(listPosition).getActual_price()+"</strike>"));



        }else{


            holder.text_price.setText(Html.fromHtml("<strike>\u20B9 "+cartList.get(listPosition).getActual_price()+"</strike>"));
            holder.text_discount.setText("\u20B9"+cartList.get(listPosition).getDiscounted_price());

            int actual = Integer.parseInt(cartList.get(listPosition).getActual_price());
            int disc = Integer.parseInt(cartList.get(listPosition).getDiscounted_price());

            int saving = actual-disc;


            holder.txt_saving.setText("\u20B9"+saving);

            holder.offer.setText(cartList.get(listPosition).getDiscounPercentage()+"% Discount");


        }
    }


    @Override
    public int getItemCount() {
        return cartList.size();
    }
    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        };
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView feature, val, sub, type;
        public LinearLayout linear_pack;
        CardView container;
        TextView offer, off, offer_name, chapter, month, chap, status,text_discount,text_price,txt_saving;

        ImageView imageView_offer, imageView_pack;


        public MyViewHolder(View itemView) {
            super(itemView);
            container = (CardView) itemView.findViewById(R.id.card_checkout);
            this.offer = (TextView) itemView.findViewById(R.id.offer_discount_check);
            this.off = (TextView) itemView.findViewById(R.id.offer_check);
            this.offer_name = (TextView) itemView.findViewById(R.id.test_check_intro);
            this.chapter = (TextView) itemView.findViewById(R.id.text_champ_check);
            this.feature = (TextView) itemView.findViewById(R.id.text_feature_check);
            this.val = (TextView) itemView.findViewById(R.id.text_val_check);
            this.month = (TextView) itemView.findViewById(R.id.text_month_check);
            this.sub = (TextView) itemView.findViewById(R.id.text_subcrib_check);
            this.chap = (TextView) itemView.findViewById(R.id.text_chap_check);
            this.type = (TextView) itemView.findViewById(R.id.text_type_check);
            this.status = (TextView) itemView.findViewById(R.id.text_status_check);
            this.text_discount = (TextView) itemView.findViewById(R.id.text_price_cart);
            this.text_price = (TextView) itemView.findViewById(R.id.text_price_cross);
            this.txt_saving = (TextView) itemView.findViewById(R.id.txt_saving);
            this.linear_pack=(LinearLayout) itemView.findViewById(R.id.liner_offer_check);

            imageView_offer = (ImageView) itemView.findViewById(R.id.back_image_check);
            imageView_pack = (ImageView) itemView.findViewById(R.id.check_header);
        }

    }
}
