package com.sseduventures.digichamps.Model;

/**
 * Created by NISHIKANT on 7/13/2018.
 */

public class ExamDetailsSqlModel {

    long id;
    String answerId;
    String Question;

    int ques_img1;
    int ques_img2;
    int skip;

    int option;

    public ExamDetailsSqlModel() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public int getQues_img1() {
        return ques_img1;
    }

    public void setQues_img1(int ques_img1) {
        this.ques_img1 = ques_img1;
    }

    public int getQues_img2() {
        return ques_img2;
    }

    public void setQues_img2(int ques_img2) {
        this.ques_img2 = ques_img2;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public int getOption() {
        return option;
    }

    public void setOption(int option) {
        this.option = option;
    }
}
