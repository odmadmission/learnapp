package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.LeaderBoardList;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.domain.ScLeaderboardListData;
import com.sseduventures.digichamps.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;




public class CardviewLeader extends RecyclerView.Adapter<CardviewLeader.DataObjectHolder> {

    Context context;
    private List<ScLeaderboardListData> list = new ArrayList<>();

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView tv_number;
        CircleImageView mImageView,mfirstImageView;
        TextView tv_name,tv_points;
        CircleImageView rel_icon,rel_first;

        public DataObjectHolder(View itemView) {
            super(itemView);
            tv_name =  itemView.findViewById(R.id.tv_name);
            tv_points =  itemView.findViewById(R.id.tv_points);
            mImageView =  itemView.findViewById(R.id.mImageView);
            mfirstImageView =  itemView.findViewById(R.id.mfirstImageView);
            tv_number =  itemView.findViewById(R.id.tv_number);
            rel_icon =  itemView.findViewById(R.id.rel_icon);
            rel_first =  itemView.findViewById(R.id.rel_first);
        }
    }

    public CardviewLeader(List<ScLeaderboardListData> getlist, Context context) {
        this.list = getlist;
        this.context = context;
    }

    @Override
    public CardviewLeader.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                    int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_leaderboard, parent, false);

        CardviewLeader.DataObjectHolder dataObjectHolder = new CardviewLeader.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final CardviewLeader.DataObjectHolder holder, final int position) {
       if(position == 0){
           holder.tv_number.setText("");
           holder.tv_name.setTypeface(null, Typeface.BOLD);
           holder.tv_points.setTypeface(null, Typeface.BOLD);

           holder.rel_first.setVisibility(View.VISIBLE);
           holder.rel_icon.setVisibility(View.GONE);
           Picasso.with(context).load(R.drawable.first_icon)
                   .placeholder(R.drawable.first_icon)
                   .into(holder.rel_first);

           holder.mfirstImageView.setVisibility(View.VISIBLE);
           holder.mImageView.setVisibility(View.GONE);

           if(!list.get(position).getImageURL().equals("")){
               Picasso.with(context).load(Constants.SRV_URL+list.get(position).getImageURL())
                       .placeholder(R.drawable.default_img)
                       .into(holder.mfirstImageView);
           }

       }else{
           holder.mfirstImageView.setVisibility(View.GONE);
           holder.mImageView.setVisibility(View.VISIBLE);

           holder.tv_name.setTypeface(null, Typeface.NORMAL);
           holder.tv_points.setTypeface(null, Typeface.NORMAL);
           holder.rel_first.setVisibility(View.GONE);
           holder.rel_icon.setVisibility(View.VISIBLE);
           Picasso.with(context).load(R.color.blue)
                   .placeholder(R.color.blue)
                   .into(holder.rel_icon);
           holder.tv_number.setText(position+1+"");

           if(!list.get(position).getImageURL().equals("")){
               Picasso.with(context).load(Constants.SRV_URL+list.get(position).getImageURL())
                       .placeholder(R.drawable.default_img)
                       .into(holder.mImageView);
           }

       }

       holder.tv_name.setText(list.get(position).getName());
       holder.tv_points.setText(list.get(position).getMarks()+"");


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}

