package com.sseduventures.digichamps.Model;

/**
 * Created by CHANDRA BHANU on 19-Jan-18.
 */

public class StudyNotes_Model {

    String moduleName,pdfUrl,moduleId,isSNBookmarked;

    public StudyNotes_Model(String moduleName, String pdfUrl, String moduleId,String isSNBookmarked) {
        this.moduleName = moduleName;
        this.pdfUrl = pdfUrl;
        this.moduleId = moduleId;
        this.isSNBookmarked = isSNBookmarked;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getIsSNBookmarked() {
        return isSNBookmarked;
    }

    public void setIsSNBookmarked(String isSNBookmarked) {
        this.isSNBookmarked = isSNBookmarked;
    }
}
