package com.sseduventures.digichamps.Model;

public class DifficultBasis_Model {

    private String TotalQuestions, TotalCorrect, TotalInCorrect, TotalSkipped,Acurasy,type;


    public DifficultBasis_Model(String TotalQuestions,String TotalCorrect,String TotalInCorrect,String TotalSkipped,String Acurasy) {

        this.TotalQuestions = TotalQuestions;
        this.TotalCorrect = TotalCorrect;
        this.TotalInCorrect = TotalInCorrect;
        this.TotalSkipped = TotalSkipped;
        this.Acurasy = Acurasy;

    }

    public String getTotalQuestions() {
        return TotalQuestions;
    }

    public String getTotalCorrect() {
        return TotalCorrect;
    }

    public String getTotalInCorrect() {
        return TotalInCorrect;
    }

    public String getTotalSkipped() {
        return TotalSkipped;
    }

    public String getAcurasy() {
        return Acurasy;
    }

    public DifficultBasis_Model(String type) {

        this.type = type;

    }

    public String getType() {
        return type;
    }
}
