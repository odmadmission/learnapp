package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/2/2018.
 */

public class ProfileDataSuccess implements Parcelable {

    private int resultCount;
    private int Regd_ID;
    private String Customer_Name;
    private String Email;
    private String Gender;
    private String DateOfBirth;
    private String DOB;
    private String SchoolName;
    private String SectionName;
    private String Mobile;
    private String Image_Url;
    private String Profile_Status;
    private int Board_ID;
    private int Class_ID;
    private String Board_Name;
    private String Class_Name;
    private String testdate;
    private String testtime;
    private String remain_time;
    private int Exam_ID;
    private int Exam_Type;


    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    public int getRegd_ID() {
        return Regd_ID;
    }

    public void setRegd_ID(int regd_ID) {
        Regd_ID = regd_ID;
    }

    public String getCustomer_Name() {
        return Customer_Name;
    }

    public void setCustomer_Name(String customer_Name) {
        Customer_Name = customer_Name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }

    public String getSectionName() {
        return SectionName;
    }

    public void setSectionName(String sectionName) {
        SectionName = sectionName;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getImage_Url() {
        return Image_Url;
    }

    public void setImage_Url(String image_Url) {
        Image_Url = image_Url;
    }

    public String getProfile_Status() {
        return Profile_Status;
    }

    public void setProfile_Status(String profile_Status) {
        Profile_Status = profile_Status;
    }

    public int getBoard_ID() {
        return Board_ID;
    }

    public void setBoard_ID(int board_ID) {
        Board_ID = board_ID;
    }

    public int getClass_ID() {
        return Class_ID;
    }

    public void setClass_ID(int class_ID) {
        Class_ID = class_ID;
    }

    public String getBoard_Name() {
        return Board_Name;
    }

    public void setBoard_Name(String board_Name) {
        Board_Name = board_Name;
    }

    public String getClass_Name() {
        return Class_Name;
    }

    public void setClass_Name(String class_Name) {
        Class_Name = class_Name;
    }

    public String getTestdate() {
        return testdate;
    }

    public void setTestdate(String testdate) {
        this.testdate = testdate;
    }

    public String getTesttime() {
        return testtime;
    }

    public void setTesttime(String testtime) {
        this.testtime = testtime;
    }

    public String getRemain_time() {
        return remain_time;
    }

    public void setRemain_time(String remain_time) {
        this.remain_time = remain_time;
    }

    public int getExam_ID() {
        return Exam_ID;
    }

    public void setExam_ID(int exam_ID) {
        Exam_ID = exam_ID;
    }

    public int getExam_Type() {
        return Exam_Type;
    }

    public void setExam_Type(int exam_Type) {
        Exam_Type = exam_Type;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.resultCount);
        dest.writeInt(this.Regd_ID);
        dest.writeString(this.Customer_Name);
        dest.writeString(this.Email);
        dest.writeString(this.Gender);
        dest.writeString(this.DateOfBirth);
        dest.writeString(this.DOB);
        dest.writeString(this.SchoolName);
        dest.writeString(this.SectionName);
        dest.writeString(this.Mobile);
        dest.writeString(this.Image_Url);
        dest.writeString(this.Profile_Status);
        dest.writeInt(this.Board_ID);
        dest.writeInt(this.Class_ID);
        dest.writeString(this.Board_Name);
        dest.writeString(this.Class_Name);
        dest.writeString(this.testdate);
        dest.writeString(this.testtime);
        dest.writeString(this.remain_time);
        dest.writeInt(this.Exam_ID);
        dest.writeInt(this.Exam_Type);
    }

    public ProfileDataSuccess() {
    }

    protected ProfileDataSuccess(Parcel in) {
        this.resultCount = in.readInt();
        this.Regd_ID = in.readInt();
        this.Customer_Name = in.readString();
        this.Email = in.readString();
        this.Gender = in.readString();
        this.DateOfBirth = in.readString();
        this.DOB = in.readString();
        this.SchoolName = in.readString();
        this.SectionName = in.readString();
        this.Mobile = in.readString();
        this.Image_Url = in.readString();
        this.Profile_Status = in.readString();
        this.Board_ID = in.readInt();
        this.Class_ID = in.readInt();
        this.Board_Name = in.readString();
        this.Class_Name = in.readString();
        this.testdate = in.readString();
        this.testtime = in.readString();
        this.remain_time = in.readString();
        this.Exam_ID = in.readInt();
        this.Exam_Type = in.readInt();
    }

    public static final Creator<ProfileDataSuccess> CREATOR = new Creator<ProfileDataSuccess>() {
        @Override
        public ProfileDataSuccess createFromParcel(Parcel source) {
            return new ProfileDataSuccess(source);
        }

        @Override
        public ProfileDataSuccess[] newArray(int size) {
            return new ProfileDataSuccess[size];
        }
    };
}
