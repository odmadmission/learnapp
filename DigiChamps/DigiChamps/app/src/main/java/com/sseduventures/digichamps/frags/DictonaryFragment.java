package com.sseduventures.digichamps.frags;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.SearchDictonaryModule;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.SearchDictonaryAdapter;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.config.AppController;
import com.sseduventures.digichamps.domain.DictonaryListData;
import com.sseduventures.digichamps.domain.DictonaryResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;



public class DictonaryFragment extends BaseFragment implements ServiceReceiver.Receiver {


    ImageView dicimg;
    TextView pertName, pertDescrption, referedLink, refferedlink;

    LinearLayout textView5, how_use_linear;
    RelativeLayout rlv_mentor;



    AutoCompleteTextView searchDictonary;

    ArrayList<SearchDictonaryModule> word_search_arr;
    SearchDictonaryAdapter searchDictonaryAdapter;
    boolean selectedText = false;
    String beforeTextChanged, afterTextChanged;
    ImageView learn_arrow;

    private static final String TAG = FeedFragment.class.getSimpleName();
    private View mView;

    private CollapsingToolbarLayout collapsing_toolbar;

    AppBarLayout appBarLayout;

    private FormActivity context;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;


    public static DictonaryFragment getInstance() {
        DictonaryFragment fragment = new DictonaryFragment();

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((NewDashboardActivity) getActivity())
                .logEvent(LogEventUtil.KEY_Dictionary_PAGE,
                        LogEventUtil.EVENT_Dictionary_PAGE);
        ((NewDashboardActivity) getActivity())
                .setModuleName(LogEventUtil.EVENT_Dictionary_PAGE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             Bundle savedInstanceState) {


        mView = inflater.inflate(R.layout.activity_terminology_dictonary,
                container, false);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ((NewDashboardActivity) getActivity()).updateStatusBarColor("#FF4545B9");


        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        context = (NewDashboardActivity) getActivity();
        refferedlink = (TextView) mView.findViewById(R.id.refferedlink);
        textView5 = (LinearLayout) mView.findViewById(R.id.textView5);
        dicimg = (ImageView) mView.findViewById(R.id.img_dictonary);
        pertName = (TextView) mView.findViewById(R.id.dicto_heading);
        pertDescrption = (TextView) mView.findViewById(R.id.dicto_description_txt);
        referedLink = (TextView) mView.findViewById(R.id.link);
        searchDictonary = (AutoCompleteTextView) mView.findViewById(R.id.auto_srch_med);
        learn_arrow = (ImageView) mView.findViewById(R.id.learn_arrow);
        appBarLayout = (AppBarLayout) mView.findViewById(R.id.appbar);
        collapsing_toolbar = (CollapsingToolbarLayout) mView.findViewById(R.id.collapsing_toolbar);

        how_use_linear = (LinearLayout) mView.findViewById(R.id.how_use_linear);
        rlv_mentor = (RelativeLayout) mView.findViewById(R.id.rlv_mentor);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsing_toolbar.setTitle("Terminology Dictionary");
                    isShow = true;
                } else if (isShow) {
                    collapsing_toolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });

        word_search_arr = new ArrayList<>();

        searchDictonaryAdapter = new SearchDictonaryAdapter(getActivity(),
                android.R.layout.simple_list_item_1, word_search_arr);
        searchDictonary.setAdapter(searchDictonaryAdapter);
        learn_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReturnHome(view);
            }
        });


        referedLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String url = referedLink.getText().toString();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }


        });
        searchDictonary.setThreshold(1);
        searchDictonary.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                beforeTextChanged = s.toString();

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if(isSameWord(s.toString())) {
                    selectedText=false;
                    getCartData();
                     searchDictonary.showDropDown();
                    afterTextChanged = s.toString();
                }


            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
        referedLink.setVisibility(View.GONE);
        dicimg.setVisibility(View.GONE);
        pertName.setVisibility(View.GONE);
        pertDescrption.setVisibility(View.GONE);
        refferedlink.setVisibility(View.GONE);


        searchDictonary.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedText=true;
                textView5.setVisibility(View.GONE);
                referedLink.setVisibility(View.GONE);
                dicimg.setVisibility(View.VISIBLE);
                pertName.setVisibility(View.VISIBLE);
                pertDescrption.setVisibility(View.VISIBLE);
                rlv_mentor.setVisibility(View.GONE);

                final SearchDictonaryModule searchDictonaryModule = word_search_arr.get(position);
                pertName.setText(word_search_arr.get(position).getDictonaryName().toString());
                pertDescrption.setText(word_search_arr.get(position).getDictonaryDeatils());
                searchDictonary.setText(word_search_arr.get(position).getDictonaryName().toString());

                searchDictonary.setSelection(searchDictonary.getText().length());
                String img = searchDictonaryModule.getDictonaryImage();
                if (img.equals("http://beta.thedigichamps.com/Images/Dictionary/")) {
                    dicimg.setVisibility(View.GONE);
                } else {
                    dicimg.setVisibility(View.VISIBLE);
                    Picasso.with(getActivity()).load(img).into(dicimg);
                }

                if (searchDictonaryModule.getDictonaryPath().equalsIgnoreCase("")) {
                    refferedlink.setVisibility(View.GONE);
                } else if (searchDictonaryModule.getDictonaryPath().equalsIgnoreCase(null)) {
                    refferedlink.setVisibility(View.GONE);
                } else if (!searchDictonaryModule.getDictonaryPath().equalsIgnoreCase(null)) {
                    refferedlink.setVisibility(View.VISIBLE);
                }

                refferedlink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String PDF_URL = searchDictonaryModule.getDictonaryPath();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(PDF_URL));
                        startActivity(browserIntent);
                    }
                });
                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                View v = getActivity().getCurrentFocus();

                if (v != null) {

                    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                    inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                }

            }
        });


        searchDictonary.post(new Runnable() {
            @Override
            public void run() {
                searchDictonary.dismissDropDown();
            }
        });

        return mView;
    }

    private boolean isSameWord(String word)
    {
        boolean st=true;
        for (int i=0;i<word_search_arr.size();i++)
        {

            if(word_search_arr.get(i).getDictonaryName().equalsIgnoreCase(word))
            {
                st=false;
            }
        }
        return st;
    }
    public void ReturnHome(View view) {
        Intent in = new Intent(getActivity(), NewDashboardActivity.class);
        startActivity(in);


    }


    private void getCartData() {

        if (AppUtil.isInternetConnected(getActivity())) {

            context.showDialog(null, "Please wait...");

            Bundle bun = new Bundle();
            bun.putString("word", searchDictonary.getText().toString().trim());
            NetworkService.startActionPostDictonaryData(getActivity(),
                    bun, mServiceReceiver);


        } else {
            context.showToast("No internet");
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        context.hideDialog();
        switch (resultCode) {
            case ResponseCodes.NO_INTERNET:

                Intent in = new Intent(getActivity(), Internet_Activity.class);
                startActivity(in);
                break;

            case ResponseCodes.UNEXPECTED_ERROR:

                Intent inin = new Intent(getActivity(), ErrorActivity.class);
                startActivity(inin);
                break;


            case ResponseCodes.EXCEPTION:

                context.showToast("Exception : " +
                        resultData.getString(IntentHelper.RESULT_EXCEPTION));
                break;

            case ResponseCodes.SUCCESS:

                DictonaryResponse response = resultData.
                        getParcelable(IntentHelper.ACTION_DICTONARY_POST);
                int size= response.getSuccess().getList().size();
                if (response != null &&
                        response.getSuccess() != null &&
                        response.getSuccess().getList() != null&&
                        response.getSuccess().getList().size()>0
                        ) {


                    word_search_arr.clear();

                    ArrayList<SearchDictonaryModule> list=new ArrayList<>();
                    for (int i = 0; i < response.getSuccess().getList().size(); i++) {

                        String Module_Id = String.valueOf(response.getSuccess().getList().get(i).getDictionary_ID());
                        String Module_Title = response.getSuccess().getList().get(i).getDictionary_WordName();
                        String Description = response.getSuccess().getList().get(i).getDictionary_Description();
                        String Module_Image = response.getSuccess().getList().get(i).getDictionaryBeta();
                        String Image_Key = response.getSuccess().getList().get(i).getDictionary_Path();

                        list.add(new SearchDictonaryModule(Module_Id, Module_Title, Description, Module_Image, Image_Key));


                    }

                    word_search_arr.addAll(list);
                    searchDictonaryAdapter.notifyDataSetChanged();
                    searchDictonary.showDropDown();


                }
                else {
                    if(!selectedText)
                    Toast.makeText(getActivity(), "No such word in your curriculum", Toast.LENGTH_SHORT).show();
                }

                break;


            case ResponseCodes.FAILURE:

                context.showToast("Something went wrong");

                break;
        }
    }


    @Override
    public void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    public void onStop() {

        mServiceReceiver.setReceiver(null);

        super.onStop();
    }
}
