package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class NoticesResponse implements Parcelable{

    private List<NoticesList> list;
    private boolean status;
    private String message;

    public List<NoticesList> getList() {
        return list;
    }

    public void setList(List<NoticesList> list) {
        this.list = list;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.list);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.message);
    }

    public NoticesResponse() {
    }

    protected NoticesResponse(Parcel in) {
        this.list = new ArrayList<NoticesList>();
        in.readList(this.list, NoticesList.class.getClassLoader());
        this.status = in.readByte() != 0;
        this.message = in.readString();
    }

    public static final Creator<NoticesResponse> CREATOR = new Creator<NoticesResponse>() {
        @Override
        public NoticesResponse createFromParcel(Parcel source) {
            return new NoticesResponse(source);
        }

        @Override
        public NoticesResponse[] newArray(int size) {
            return new NoticesResponse[size];
        }
    };
}
