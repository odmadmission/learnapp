package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 9/5/2018.
 */

public class SetBookmarkRequest implements Parcelable{

    private long RegID;
    private String DatatypeId;
    private String DataId;

    public long getRegID() {
        return RegID;
    }

    public void setRegID(long regID) {
        RegID = regID;
    }

    public String getDatatypeId() {
        return DatatypeId;
    }

    public void setDatatypeId(String datatypeId) {
        DatatypeId = datatypeId;
    }

    public String getDataId() {
        return DataId;
    }

    public void setDataId(String dataId) {
        DataId = dataId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.RegID);
        dest.writeString(this.DatatypeId);
        dest.writeString(this.DataId);
    }

    public SetBookmarkRequest() {
    }

    protected SetBookmarkRequest(Parcel in) {
        this.RegID = in.readLong();
        this.DatatypeId = in.readString();
        this.DataId = in.readString();
    }

    public static final Creator<SetBookmarkRequest> CREATOR = new Creator<SetBookmarkRequest>() {
        @Override
        public SetBookmarkRequest createFromParcel(Parcel source) {
            return new SetBookmarkRequest(source);
        }

        @Override
        public SetBookmarkRequest[] newArray(int size) {
            return new SetBookmarkRequest[size];
        }
    };
}
