package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/25/2018.
 */

public class PMActiveTask implements Parcelable {

    private List<PMActivetaskDataList> taskDataList;


    public List<PMActivetaskDataList> getTaskDataList() {
        return taskDataList;
    }

    public void setTaskDataList(List<PMActivetaskDataList> taskDataList) {
        this.taskDataList = taskDataList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.taskDataList);
    }

    public PMActiveTask() {
    }

    protected PMActiveTask(Parcel in) {
        this.taskDataList = in.createTypedArrayList(PMActivetaskDataList.CREATOR);
    }

    public static final Creator<PMActiveTask> CREATOR = new Creator<PMActiveTask>() {
        @Override
        public PMActiveTask createFromParcel(Parcel source) {
            return new PMActiveTask(source);
        }

        @Override
        public PMActiveTask[] newArray(int size) {
            return new PMActiveTask[size];
        }
    };
}
