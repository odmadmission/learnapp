package com.sseduventures.digichamps.adapter;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.Model.Basic_info_model;
import com.sseduventures.digichamps.R;

import java.util.ArrayList;

public class School_listview_DialogAdapter extends ArrayAdapter<Basic_info_model> {

    private ArrayList<Basic_info_model> dataSet;
    Context mContext;

    private static class ViewHolder {
        TextView board_tv;

        ImageView checkin_img;
        RelativeLayout relative;
    }

    public School_listview_DialogAdapter(ArrayList<Basic_info_model> data, Context context) {
        super(context, R.layout.board_listview_dialog, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Basic_info_model dataModel = dataSet.get(position);
        final ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.board_listview_dialog, parent, false);
            viewHolder.board_tv = (TextView) convertView.findViewById(R.id.board_tv);
            viewHolder.checkin_img = (ImageView) convertView.findViewById(R.id.checkin_img);
            viewHolder.relative = (RelativeLayout) convertView.findViewById(R.id.relative_id);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }



        viewHolder.board_tv.setText(dataModel.getName());
        if(dataModel.isFlag())
        {
            viewHolder.board_tv.setTextColor(Color.parseColor("#2CB3E1"));
            viewHolder.checkin_img.setVisibility(View.VISIBLE);
        }
        else
        {
            viewHolder.board_tv.setTextColor(Color.parseColor("#797676"));
            viewHolder.checkin_img.setVisibility(View.INVISIBLE);
        }



        return convertView;
    }
    public ArrayList<Basic_info_model> basic_info_modelArrayList(){
        return dataSet;
    }
}