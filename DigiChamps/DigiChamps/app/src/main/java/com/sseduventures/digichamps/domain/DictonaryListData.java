package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/28/2018.
 */

public class DictonaryListData implements Parcelable{

    private int Dictionary_ID;
    private String Dictionary_WordName;
    private String Dictionary_Images;
    private String Dictionary_Path;
    private String Dictionary_Description;
    private String DictionaryOnline;
    private String DictionaryBeta;

    public int getDictionary_ID() {
        return Dictionary_ID;
    }

    public void setDictionary_ID(int dictionary_ID) {
        Dictionary_ID = dictionary_ID;
    }

    public String getDictionary_WordName() {
        return Dictionary_WordName;
    }

    public void setDictionary_WordName(String dictionary_WordName) {
        Dictionary_WordName = dictionary_WordName;
    }

    public String getDictionary_Images() {
        return Dictionary_Images;
    }

    public void setDictionary_Images(String dictionary_Images) {
        Dictionary_Images = dictionary_Images;
    }

    public String getDictionary_Path() {
        return Dictionary_Path;
    }

    public void setDictionary_Path(String dictionary_Path) {
        Dictionary_Path = dictionary_Path;
    }

    public String getDictionary_Description() {
        return Dictionary_Description;
    }

    public void setDictionary_Description(String dictionary_Description) {
        Dictionary_Description = dictionary_Description;
    }

    public String getDictionaryOnline() {
        return DictionaryOnline;
    }

    public void setDictionaryOnline(String dictionaryOnline) {
        DictionaryOnline = dictionaryOnline;
    }

    public String getDictionaryBeta() {
        return DictionaryBeta;
    }

    public void setDictionaryBeta(String dictionaryBeta) {
        DictionaryBeta = dictionaryBeta;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Dictionary_ID);
        dest.writeString(this.Dictionary_WordName);
        dest.writeString(this.Dictionary_Images);
        dest.writeString(this.Dictionary_Path);
        dest.writeString(this.Dictionary_Description);
        dest.writeString(this.DictionaryOnline);
        dest.writeString(this.DictionaryBeta);
    }

    public DictonaryListData() {
    }

    protected DictonaryListData(Parcel in) {
        this.Dictionary_ID = in.readInt();
        this.Dictionary_WordName = in.readString();
        this.Dictionary_Images = in.readString();
        this.Dictionary_Path = in.readString();
        this.Dictionary_Description = in.readString();
        this.DictionaryOnline = in.readString();
        this.DictionaryBeta = in.readString();
    }

    public static final Creator<DictonaryListData> CREATOR = new Creator<DictonaryListData>() {
        @Override
        public DictonaryListData createFromParcel(Parcel source) {
            return new DictonaryListData(source);
        }

        @Override
        public DictonaryListData[] newArray(int size) {
            return new DictonaryListData[size];
        }
    };
}
