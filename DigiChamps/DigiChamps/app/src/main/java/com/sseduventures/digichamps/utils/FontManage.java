package com.sseduventures.digichamps.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by shubham on 10/21/16.
 */
public class FontManage {

    public static void setFontImpact(Context context, TextView txtView) {

        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "impact.ttf");
        txtView.setTypeface(custom_font);
    }


    public static void setFontMeiryo(Context context, TextView txtView) {
        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");

        txtView.setTypeface(custom_font);
    }

    public static void setFontMeiryoBold(Context context, TextView txtView) {

        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "homefont.TTF");
        txtView.setTypeface(custom_font);
    }

    public static void setFontHomeBold(Context context, TextView txtView) {

        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "homefont.TTF");
        txtView.setTypeface(custom_font);
    }

    public static void setFontHeaderBold(Context context, TextView txtView) {
        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "header.ttf");
        txtView.setTypeface(custom_font);
    }

    public static void setFontTextLandingBold(Context context, TextView txtView) {
        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "text.ttf");
        txtView.setTypeface(custom_font);
    }

    public static void setFontButton(Context context, TextView txtView) {
        Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "buttonfont.ttf");
        txtView.setTypeface(custom_font);
    }





}
