package com.sseduventures.digichamps.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import android.widget.EditText;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.sseduventures.digichamps.Model.DataModel_Checkout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.adapter.Checkout_Adapter;
import com.sseduventures.digichamps.adapter.RecyclerTouchListener;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.config.AppController;
import com.sseduventures.digichamps.domain.DiscountCouponSuccess;
import com.sseduventures.digichamps.domain.OrderConfResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.model.CheckoutDataModel;


/**
 * Created by ntspl24 on 4/12/2017.
 */

public class Checkout_Activity extends FormActivity implements PaymentResultListener,
        ServiceReceiver.Receiver {

    String TAG = "Checkout_Activity", Customer_Name, Mobile_Number, Email_Id;
    private RecyclerView recyclerView;
    private Checkout_Adapter mAdapter;
    ImageButton notification;
    Button checkout, detail, coupon_btn;
    ImageView back_arrow;
    String payblamt, Order_Id;
    private Toolbar toolbar;
    TextView amount, main_price, off_price, cost, cost_price, coupon, coupon_price, tax, coupon_name, tax_price, total, total_price, save, save_price, promo, term, term_con, term_concoupon_name, discounted_price;
    EditText promo_code;
    LinearLayout detail_lin, layout;
    RelativeLayout pay_rel, check_rel;
    private String resp, error, type, user_id;
    private SpotsDialog dialog;
    ArrayList<DataModel_Checkout> list;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.checkout_activity_new);
        logEvent(LogEventUtil.KEY_Checkout_PAGE,
                LogEventUtil.EVENT_Checkout_PAGE);
        setModuleName(LogEventUtil.EVENT_Checkout_PAGE);
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        init();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);

        if (AppUtil.isInternetConnected(this)) {

            Bundle bundle = new Bundle();

            NetworkService.startcheckoutData(Checkout_Activity.this,
                    mServiceReceiver, bundle);

        } else {
            startActivity(new Intent(Checkout_Activity.this, Internet_Activity.class));

            finish();
        }

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Checkout_Activity.this, Cart_Activity.class);
                startActivity(intent);

                finish();

            }
        });


        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppController.getInstance().trackEvent("Checkout", "Checkout", "Checkout Clicked");

                startPayment();
            }
        });

        detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pay_rel.setVisibility(View.GONE);
                check_rel.setVisibility(View.GONE);
                detail_lin.setVisibility(View.VISIBLE);
            }
        });

        coupon_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AppUtil.isInternetConnected(Checkout_Activity.this)) {
                    String coupon = promo_code.getText().toString().trim();

                    if (coupon.equalsIgnoreCase("")) {
                        showMessage("Please enter coupon code");

                    } else if (coupon.length() > 20) {
                        showMessage("Coupon code cannot exceed more than 20 characters");

                    } else {
                        applycoupn(coupon);
                    }

                } else {
                    startActivity(new Intent(Checkout_Activity.this, Internet_Activity.class));

                    finish();
                }
            }
        });
        term_con.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                termsconditions();
            }
        });
    }


    private void init() {


        user_id = String.valueOf(RegPrefManager.getInstance(this).getRegId());
        checkout = (Button) findViewById(R.id.bt_order_cart);
        detail = (Button) findViewById(R.id.details);
        coupon_btn = (Button) findViewById(R.id.apply);
        toolbar = (Toolbar) findViewById(R.id.toolbar_checkout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        layout = (LinearLayout) findViewById(R.id.content_cart);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view_checkout);
        back_arrow = (ImageView) findViewById(R.id.checkout_back_image);
        promo_code = (EditText) findViewById(R.id.enter_code);
        detail_lin = (LinearLayout) findViewById(R.id.check_linear_detail);
        pay_rel = (RelativeLayout) findViewById(R.id.pay_linear);
        check_rel = (RelativeLayout) findViewById(R.id.check_reletive);
        discounted_price = (TextView) findViewById(R.id.tax_price_discounted);
        coupon_name = (TextView) findViewById(R.id.text_coupon_aplly);
        amount = (TextView) findViewById(R.id.amount);
        main_price = (TextView) findViewById(R.id.text_price_check);
        off_price = (TextView) findViewById(R.id.text_price_check2);
        cost = (TextView) findViewById(R.id.text_pack_cost);
        cost_price = (TextView) findViewById(R.id.cost_price);
        coupon = (TextView) findViewById(R.id.text_coupon_aplly);
        coupon_price = (TextView) findViewById(R.id.coupon_price);
        tax = (TextView) findViewById(R.id.text_tax);
        tax_price = (TextView) findViewById(R.id.tax_price);
        total = (TextView) findViewById(R.id.text_total);
        total_price = (TextView) findViewById(R.id.total_price);
        save = (TextView) findViewById(R.id.text_save);
        save_price = (TextView) findViewById(R.id.save_price);
        promo = (TextView) findViewById(R.id.text_promo);
        term = (TextView) findViewById(R.id.text_term);
        term_con = (TextView) findViewById(R.id.text_term_con);
        list = new ArrayList<>();
        dialog = new SpotsDialog(this, "FUN + EDUCATION", R.style.Custom);

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        finish();

    }


    private int payAmount;


    private void applycoupn(String cuopon) {

        if (AppUtil.isInternetConnected(this)) {


            Bundle bun = new Bundle();
            bun.putString("coupon", cuopon);


            NetworkService.startActionDiscountCoupon(this, mServiceReceiver, bun);

        } else {
            Intent in = new Intent(Checkout_Activity.this, Internet_Activity.class);
            startActivity(in);
        }

    }

    public void termsconditions() {
        dialog.ShowCondition("Terms & conditions", AppConfig.terms);
    }


    public void showMessage(String Message) {
        Snackbar snackbar = Snackbar
                .make(layout, Message, Snackbar.LENGTH_LONG);

        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);

        snackbarView.setBackgroundColor(Color.DKGRAY);
        snackbar.show();
    }

    public String convert(String val) {
        int number = (int) Double.parseDouble(val);
        return String.valueOf(number);
    }

    public void startPayment() {
        setModuleName( LogEventUtil.EVENT_FinalPayment_PAGE);
        logEvent(LogEventUtil.KEY_FinalPayment_PAGE,
                LogEventUtil.EVENT_FinalPayment_PAGE);

        final Activity activity = this;

        final Checkout co = new Checkout();

        int amount = 0;
        if (payAmount > 0)
            amount = (int) payAmount * 100;
        else
            amount = (int) Double.parseDouble(payblamt) * 100;


        try {
            JSONObject options = new JSONObject();
            options.put("name", "Digichamps");
            options.put("description",
                    "Package Charges");
            options.put("currency", "INR");
            options.put("amount", amount);

            JSONObject preFill = new JSONObject();
            preFill.put("email", Email_Id);
            preFill.put("contact", Mobile_Number);

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }


    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            SharedPreferences preference = getApplicationContext().getSharedPreferences("user_data", getApplicationContext().MODE_PRIVATE);
            preference.edit().putString("razor", razorpayPaymentID).apply();
            order_processing_new(Order_Id, razorpayPaymentID);
        } catch (Exception e) {
showToast(""+e.getMessage());
        }
    }


    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        logEvent(LogEventUtil.KEY_PAYMENT_FAIL_PAGE,
                LogEventUtil.EVENT_PAYMENT_FAIL_PAGE);
        setModuleName( LogEventUtil.EVENT_PAYMENT_FAIL_PAGE);
        try {
            Toast.makeText(this, "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }
    }

    public void order_processing_new(final String order_id, final String tracking_id) {

        if (AppUtil.isInternetConnected(this)) {
            Bundle bun = new Bundle();

            bun.putString("OrderID", order_id);
            bun.putString("TrackID", tracking_id);
            bun.putInt("Discount", getIntent().getIntExtra("discount", 0));

            showDialog(null, "Please wait...");
            NetworkService.startActionOrderConfirmation(this, mServiceReceiver, bun);

        } else {
            Intent in = new Intent(Checkout_Activity.this, Internet_Activity.class);
            startActivity(in);
        }


    }

    @Override
    public void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
        hideDialog();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }


    //Added By Abhishek

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {


       hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(Checkout_Activity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(Checkout_Activity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(Checkout_Activity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.ORDER_CONFIRM_FAILURE:
                Intent inin2 = new Intent(Checkout_Activity.this, ErrorActivity.class);
                startActivity(inin2);
                break;
            case ResponseCodes.ORDERCONF:
                OrderConfResponse orderConfResponse = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if (orderConfResponse != null &&
                        orderConfResponse.getSuccess().getGet_confdata() != null) {

                    String order_ID = orderConfResponse.getSuccess().getGet_confdata().getOrder_Id();
                    Intent ininin = new Intent(Checkout_Activity.this, Payment_Activity.class);
                    ininin.putExtra("order_id", order_ID);
                    startActivity(ininin);
                    finish();

                }

                break;
            case ResponseCodes.SUCCESS:
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);

                CheckoutDataModel checkoutDataModel = resultData.getParcelable(
                        IntentHelper.RESULT_DATA
                );
                if (checkoutDataModel == null) {

                } else {

                    payblamt = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getPayblamt());
                    String price_cart = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getOriginal_price());
                    String discountprice = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getDiscountprice());
                    String afterdicount = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getAfterdicount());
                    String taxx = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getTaxx());
                    String coupn_name = checkoutDataModel.getSuccess().getGetcheckoutdata().getCoupon_code();
                    Order_Id = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getOrder_Id());
                    Customer_Name = checkoutDataModel.getSuccess().getGetcheckoutdata().getCustomer_Name();
                    Mobile_Number = checkoutDataModel.getSuccess().getGetcheckoutdata().getMobile_Number();
                    Email_Id = checkoutDataModel.getSuccess().getGetcheckoutdata().getEmail_Id();


                    if (checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().size() > 0) {
                        for (int i = 0; i < checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().size(); i++) {

                            String package_id = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().get(i).getPackage_ID());
                            String pkg_name = checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().get(i).getPackage_Name();
                            String offline = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().get(i).getIs_Offline());
                            String discountpercentage = dialog.convert(String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().get(i).getDiscountpercentage()));
                            String subscription_limit = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().get(i).getSubscripttion_Limit());
                            String price = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().get(i).getPrice());
                            String exclude_price = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().get(i).getDiscounted_Price());
                            String subscription_period = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().get(i).getValidity());
                            String package_price = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().get(i).getPackage_Price());
                            String img = "";
                            if (checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().get(i).getPackage_Image() != null) {
                                img = checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().get(i).getPackage_Image();
                            }
                            String thumbnail = AppConfig.SRVR_URL + "Images/" + img;

                            String thumbnail1 = thumbnail.replaceAll(" ", "%20");
                            String discounted_price = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().get(i).getDiscounted_Price());
                            String cart_id = String.valueOf(checkoutDataModel.getSuccess().getGetcheckoutdata().getCartdata().get(i).getCart_ID());

                            if (offline.equalsIgnoreCase("true")) {
                                type = "Offline";
                            } else {
                                type = "Online";
                            }
                            list.add(new DataModel_Checkout(pkg_name, package_id, cart_id, subscription_period + " " + "days", subscription_limit + " Chapters",
                                    type, convert(price), convert(exclude_price), convert(package_price), convert(discounted_price), thumbnail1, discountpercentage));

                        }
                    }


                    int discount = RegPrefManager.getInstance(this).getReedeem();

                    if (discount == 1) {


                        String couponNameNew = "(DIGI35)";
                        coupon_name.setText(Html.fromHtml("<font color='#1C456D'>Discounted Price</font> <font color='#FF3D33'>" + couponNameNew + "</font color>"));
                        int MainPrice = Integer.parseInt(convert(price_cart));
                        int disPrice = (MainPrice * 35) / 100;
                        payAmount = MainPrice - disPrice;

                        discounted_price.setText("\u20B9 " + disPrice);
                        main_price.setText("\u20B9 " + convert(price_cart));
                        coupon_price.setText("\u20B9 " + disPrice);
                        cost_price.setText("\u20B9 " + convert(price_cart));
                        save_price.setText("\u20B9 " + convert(discountprice));
                        off_price.setText("\u20B9 " + convert(afterdicount));
                        tax_price.setText("\u20B9 " + convert(taxx));
                        total_price.setText("\u20B9 " + payAmount);
                        checkout.setText("PAY NOW Rs " + payAmount);

                    } else {

                        payAmount = 0;

                        checkout.setText("PAY NOW Rs " + convert(payblamt));

                        String couponNameNew = "(" + coupn_name + ")";
                        coupon_name.setText(Html.fromHtml("<font color='#1C456D'>Discounted Price</font> <font color='#FF3D33'>" + couponNameNew + "</font color>"));
                        discounted_price.setText("\u20B9 " + convert(afterdicount));
                        main_price.setText("\u20B9 " + convert(price_cart));
                        coupon_price.setText("\u20B9 " + convert(discountprice));
                        cost_price.setText("\u20B9 " + convert(price_cart));
                        save_price.setText("\u20B9 " + convert(discountprice));
                        off_price.setText("\u20B9 " + convert(afterdicount));
                        tax_price.setText("\u20B9 " + convert(taxx));
                        total_price.setText("\u20B9 " + convert(payblamt));

                    }

                    mAdapter = new Checkout_Adapter(list,
                            Checkout_Activity.this,
                            RegPrefManager.getInstance(Checkout_Activity.this)
                                    .getReedeem());
                    recyclerView.setAdapter(mAdapter);
                    hideDialog();
                }
                break;


            case ResponseCodes.DISCOUNTUNSUCCESS:

                Toast.makeText(this, "Invalid Coupon", Toast.LENGTH_SHORT).show();

                break;

            case ResponseCodes.DISCOUNTSUCCESS:

                DiscountCouponSuccess discountCouponSuccess = resultData.getParcelable(
                        IntentHelper.RESULT_DATA);

                if (discountCouponSuccess != null) {

                    payblamt = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getPayblamt());
                    String price_cart = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getOriginal_price());
                    String discountprice = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getDiscountprice());
                    String discounted_pric = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getAfterdicount());
                    String taxx = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getTaxx());
                    Order_Id = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getOrder_Id());
                    String message = discountCouponSuccess.getSuccess().getGetcheckoutdata().getMsg();
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                    Customer_Name = discountCouponSuccess.getSuccess().getGetcheckoutdata().getCustomer_Name();
                    Mobile_Number = discountCouponSuccess.getSuccess().getGetcheckoutdata().getMobile_Number();
                    Email_Id = discountCouponSuccess.getSuccess().getGetcheckoutdata().getOrder_No();
                    String coupon_code = discountCouponSuccess.getSuccess().getGetcheckoutdata().getCoupon_code();
                    list.clear();
                    for (int i = 0; i < discountCouponSuccess.getSuccess().getGetcheckoutdata().getCartdata().size(); i++) {

                        String package_id = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getCartdata().get(i).getPackage_ID());
                        String pkg_name = discountCouponSuccess.getSuccess().getGetcheckoutdata().getCartdata().get(i).getPackage_Name();
                        String img = discountCouponSuccess.getSuccess().getGetcheckoutdata().getCartdata().get(i).getPackage_Image();
                        String thumbnail = AppConfig.SRVR_URL + "Images/" + img;
                        String discountpercentage = dialog.convert(String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getCartdata().get(i).getDiscountpercentage()));
                        String thumbnail1 = thumbnail.replaceAll(" ", "%20");
                        String package_price = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getCartdata().get(i).getPackage_Price());

                        String subscription_limit = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getCartdata().get(i).getSubscripttion_Limit());
                        String price = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getCartdata().get(i).getPrice());

                        String exclude_price = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getCartdata().get(i).getDiscounted_Price());
                        String subscription_period = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getCartdata().get(i).getValidity());
                        String offline = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getCartdata().get(i).isIs_Offline());

                        String cart_id = String.valueOf(discountCouponSuccess.getSuccess().getGetcheckoutdata().getCartdata().get(i).getCart_ID());

                        if (offline.equalsIgnoreCase("true")) {
                            type = "Offline";
                        } else {
                            type = "Online";
                        }
                        list.add(new DataModel_Checkout(pkg_name, package_id, cart_id, subscription_period + " " + "days", subscription_limit + " Chapters",
                                type, convert(price), convert(exclude_price), convert(package_price), convert(discounted_pric), thumbnail1, discountpercentage));

                    }
                    coupon_price.setText("Rs " + convert(discountprice));
                    cost_price.setText("Rs " + convert(price_cart));
                    off_price.setText("Rs " + convert(payblamt));
                    tax_price.setText("Rs " + convert(taxx));
                    coupon_name.setText(coupon_code);
                    discounted_price.setText("Rs " + convert(discounted_pric));
                    checkout.setText("PAY NOW Rs " + convert(payblamt));
                    save_price.setText("Rs " + discounted_pric);
                    total_price.setText("Rs " + convert(payblamt));
                    mAdapter = new Checkout_Adapter(list, Checkout_Activity.this
                            , getIntent().getIntExtra("discount", 0));
                    recyclerView.setAdapter(mAdapter);


                }

                break;


        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }
}

