package com.sseduventures.digichamps.Model;

import java.util.List;

public class SuccessResult
{
    private List<ReportModule> ReportModules;

    public List<ReportModule> getReportModules() {
        return ReportModules;
    }

    public void setReportModules(List<ReportModule> reportModules) {
        ReportModules = reportModules;
    }
}
