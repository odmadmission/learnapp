package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/3/2018.
 */

public class FAQResponse implements Parcelable {

    private FAQSuccess Success;

    public FAQSuccess getSuccess() {
        return Success;
    }

    public void setSuccess(FAQSuccess success) {
        Success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.Success, flags);
    }

    public FAQResponse() {
    }

    protected FAQResponse(Parcel in) {
        this.Success = in.readParcelable(FAQSuccess.class.getClassLoader());
    }

    public static final Creator<FAQResponse> CREATOR = new Creator<FAQResponse>() {
        @Override
        public FAQResponse createFromParcel(Parcel source) {
            return new FAQResponse(source);
        }

        @Override
        public FAQResponse[] newArray(int size) {
            return new FAQResponse[size];
        }
    };
}
