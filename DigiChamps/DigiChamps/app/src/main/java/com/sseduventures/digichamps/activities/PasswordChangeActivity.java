package com.sseduventures.digichamps.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.BaseActivity;
import com.sseduventures.digichamps.domain.ChangePasswordRequest;
import com.sseduventures.digichamps.domain.ChangePasswordResponse;
import com.sseduventures.digichamps.domain.ChangePasswordSuccess;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

public class PasswordChangeActivity extends BaseActivity implements
        ServiceReceiver.Receiver, View.OnClickListener {


    private static final String TAG = PasswordChangeActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private AppCompatEditText mCurrentPassword;
    private AppCompatEditText mNewPassword;
    private AppCompatEditText mConfirmPassword;

    private TextInputLayout mTilCurrentPassword;
    private TextInputLayout mTilNewPassword;
    private TextInputLayout mTilConfirmPassword;

    private TextView tvSubmit;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_change_password);
        init();
    }

    private void init() {
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        mToolbar = findViewById(R.id.changePasswordLayout_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Change Password");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mTilCurrentPassword = findViewById(R.id.tilCP);
        mTilNewPassword = findViewById(R.id.tilNP);
        mTilConfirmPassword = findViewById(R.id.tilCFP);

        mCurrentPassword = findViewById(R.id.etCP);
        mNewPassword = findViewById(R.id.etNP);
        mConfirmPassword = findViewById(R.id.etCFP);

        tvSubmit = findViewById(R.id.tvSubmit);
        tvSubmit.setOnClickListener(this);
        mCurrentPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s != null && s.length() > 0) {
                    mTilCurrentPassword.setError("");
                    mTilCurrentPassword.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0) {
                    mTilNewPassword.setError("");
                    mTilNewPassword.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0) {
                    mTilConfirmPassword.setError("");
                    mTilConfirmPassword.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSubmit:
                if (validate()) {
                    changePassword();
                }
                break;
        }
    }

    private void changePassword() {
        if (AppUtil.isInternetConnected(this)) {
            ChangePasswordRequest req = new ChangePasswordRequest();
            req.setNew_password(mNewPassword.getText().toString());
            req.setOld_password(mCurrentPassword.getText().toString());
            req.setConfirm_password(mConfirmPassword.getText().toString());
            Bundle bundle = new Bundle();
            bundle.putParcelable("change_password", req);
            showDialog();
            NetworkService.startActionPostChangePassword(this,
                    mServiceReceiver, bundle);

        } else
            showNoInternetDialog();
    }

    private boolean validate() {
        if (mCurrentPassword.getText().toString().isEmpty()) {
            mTilCurrentPassword.setErrorEnabled(true);
            mTilCurrentPassword.setError("Cannot be empty");
            return false;
        }

//        if (mCurrentPassword.getText().toString().length() < 6) {
//            mTilCurrentPassword.setErrorEnabled(true);
//            mTilCurrentPassword.setError("Must be 6 digits");
//            return false;
//        }
//
//        if (mCurrentPassword.getText().toString().length() > 16) {
//            mTilCurrentPassword.setErrorEnabled(true);
//            mTilCurrentPassword.setError("cannot be greater than 16 digits");
//            return false;
//        }


        if (mNewPassword.getText().toString().isEmpty()) {
            mTilNewPassword.setErrorEnabled(true);
            mTilNewPassword.setError("Cannot be empty");
            return false;
        }

//        if (mNewPassword.getText().toString().length() < 6) {
//            mTilNewPassword.setErrorEnabled(true);
//            mTilNewPassword.setError("Must be 6 digits");
//            return false;
//        }
//        if (mNewPassword.getText().toString().length() > 16) {
//            mTilNewPassword.setErrorEnabled(true);
//            mTilNewPassword.setError("cannot be greater than 16 digits");
//            return false;
//        }

        if (mConfirmPassword.getText().toString().isEmpty()) {
            mTilConfirmPassword.setErrorEnabled(true);
            mTilConfirmPassword.setError("Cannot be empty");
            return false;
        }

//        if (mConfirmPassword.getText().toString().length() < 6) {
//            mTilConfirmPassword.setErrorEnabled(true);
//            mTilConfirmPassword.setError("Must be 6 digits");
//            return false;
//        }
//
//        if (mConfirmPassword.getText().toString().length() > 16) {
//            mTilConfirmPassword.setErrorEnabled(true);
//            mTilConfirmPassword.setError("cannot be greater than 16 digits");
//            return false;
//        }

        if (!mConfirmPassword.getText().toString().equals(mNewPassword.getText().toString())) {
            mTilConfirmPassword.setErrorEnabled(true);
            mTilConfirmPassword.setError("Password not matched");
            return false;
        }

        if (mCurrentPassword.getText().toString().equals(mNewPassword.getText().toString())) {
            mTilNewPassword.setErrorEnabled(true);
            mTilNewPassword.setError("Old Password and New Password cannot be same");
            return false;
        }

        return true;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode) {
            case ResponseCodes.FAILURE:

                break;
            case ResponseCodes.SUCCESS:
                ChangePasswordSuccess res = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if (res != null && res.getSuccess() != null)
                    showToast(res.getSuccess().getMessage() + "");
                Intent intent = new Intent(PasswordChangeActivity.this, NewDashboardActivity.class);
                startActivity(intent);
                break;
            case ResponseCodes.UNEXPECTED_ERROR:

                break;
            case ResponseCodes.EXCEPTION:

                break;
        }
    }
}
