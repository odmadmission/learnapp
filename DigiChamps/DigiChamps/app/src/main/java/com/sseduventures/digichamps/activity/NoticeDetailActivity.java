package com.sseduventures.digichamps.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.R;

import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.Utils;



public class NoticeDetailActivity  extends FormActivity {

    TextView tv_date,tv_subject,tv_notice;
    RelativeLayout rel_click;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_detail);
        setModuleName(LogEventUtil.EVENT_notice_details);
        logEvent(LogEventUtil.KEY_notice_details,LogEventUtil.EVENT_notice_details);


        tv_date =  findViewById(R.id.tv_date);
        rel_click =  findViewById(R.id.rel_click);
        tv_subject =  findViewById(R.id.tv_subject);
        tv_notice =  findViewById(R.id.tv_notice);
        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView lblDate =  findViewById(R.id.lblDate);
        TextView lblSubject =  findViewById(R.id.lblSubject);
        TextView lblNotice =  findViewById(R.id.lblNotice);
        FontManage.setFontImpact(this,lblDate);
        FontManage.setFontImpact(this,lblSubject);
        FontManage.setFontImpact(this,lblNotice);

        FontManage.setFontMeiryoBold(this,tv_date);
        FontManage.setFontMeiryoBold(this,tv_subject);
        FontManage.setFontMeiryo(this,tv_notice);

        if(getIntent() != null){
            String date = getIntent().getStringExtra("createdDate");
            final String fileURL = getIntent().getStringExtra("fileURL");
            String description = getIntent().getStringExtra("description");
            String title = getIntent().getStringExtra("title");

            tv_subject.setText(title);
            tv_notice.setText(description);
            tv_date.setText(Utils.parseDateChange(date));
            if(fileURL.equals("")){
                rel_click.setVisibility(View.GONE);
            }else{
                rel_click.setVisibility(View.VISIBLE);
            }

            rel_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.IMAGE_URL+fileURL));
                    startActivity(browserIntent);
                }
            });


        }


    }

}
