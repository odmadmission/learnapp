package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/11/2018.
 */

public class DailyTTResponse implements Parcelable{


    private List<DailyTTList> list;
    private boolean status;
    private String message;

    public List<DailyTTList> getList() {
        return list;
    }

    public void setList(List<DailyTTList> list) {
        this.list = list;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.list);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.message);
    }

    public DailyTTResponse() {
    }

    protected DailyTTResponse(Parcel in) {
        this.list = in.createTypedArrayList(DailyTTList.CREATOR);
        this.status = in.readByte() != 0;
        this.message = in.readString();
    }

    public static final Creator<DailyTTResponse> CREATOR = new Creator<DailyTTResponse>() {
        @Override
        public DailyTTResponse createFromParcel(Parcel source) {
            return new DailyTTResponse(source);
        }

        @Override
        public DailyTTResponse[] newArray(int size) {
            return new DailyTTResponse[size];
        }
    };
}
