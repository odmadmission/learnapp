package com.sseduventures.digichamps.Model;

/**
 * Created by Tech_2 on 5/7/2018.
 */

public class SubConceptChapListDataModel {

    String ChapterId,Chapter,SubConceptId,SubConcept,Accuracy;

    public SubConceptChapListDataModel(String chapterId, String chapter, String subConceptId, String subConcept, String accuracy) {
        ChapterId = chapterId;
        Chapter = chapter;
        SubConceptId = subConceptId;
        SubConcept = subConcept;
        Accuracy = accuracy;
    }

    public String getChapterId() {
        return ChapterId;
    }

    public void setChapterId(String chapterId) {
        ChapterId = chapterId;
    }

    public String getChapter() {
        return Chapter;
    }

    public void setChapter(String chapter) {
        Chapter = chapter;
    }

    public String getSubConceptId() {
        return SubConceptId;
    }

    public void setSubConceptId(String subConceptId) {
        SubConceptId = subConceptId;
    }

    public String getSubConcept() {
        return SubConcept;
    }

    public void setSubConcept(String subConcept) {
        SubConcept = subConcept;
    }

    public String getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(String accuracy) {
        Accuracy = accuracy;
    }
}
