package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.sseduventures.digichamps.Model.DataModel_TicketList;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewNotificationActivity;
import com.sseduventures.digichamps.adapter.TicketListItem_Adapter;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.domain.DoubtListData;
import com.sseduventures.digichamps.domain.DoubtListResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

/**
 * Created by Asish on 4/28/2017.
 */


public class TicketList_Activity extends FormActivity implements ServiceReceiver.Receiver {

    private List<DataModel_TicketList> ticketList = new ArrayList<>();
    private List<DoubtListData> mTicketList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TicketListItem_Adapter mAdapter;
    private FloatingActionButton fab;
    private RelativeLayout relativeLayoutDoubt;
    public ArrayList<String> ticket_number;
    private TextView no_doubt_error1;

    private boolean isInternetPresent = false;
    private String user_id;
    private SpotsDialog dialog;
    private ImageView back_arrow;
    private ImageButton notifcation_btn, cart_button;
    private NestedScrollView nsv;
    private Toolbar toolbar;
    private TextView show_errorText;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private DoubtListResponse success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_ticket_list);
        setModuleName(LogEventUtil.EVENT_Doubts_List_PAGE);
        logEvent(LogEventUtil.KEY_Doubts_List_PAGE,LogEventUtil.EVENT_Doubts_List_PAGE);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        user_id = String.valueOf(RegPrefManager.getInstance(this).getRegId());

        init();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_ticket);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (isInternetPresent) {

            getDoubtList();


        } else {
            startActivity(new Intent(TicketList_Activity.this, Internet_Activity.class));

            finish();
        }

        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isInternetPresent) {

                } else {
                    startActivity(new Intent(TicketList_Activity.this, Internet_Activity.class));

                    finish();

                }
            }

        });

        notifcation_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isInternetPresent) {

                    startActivity(new Intent(TicketList_Activity.this,
                            NewNotificationActivity.class));


                } else {
                    startActivity(new Intent(TicketList_Activity.this, Internet_Activity.class));

                    finish();

                }
            }

        });



        List<DataModel_TicketList> list = new ArrayList<>();


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);


        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);

        nsv.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > oldScrollY) {
                    fab.hide();
                } else {
                    fab.show();
                }
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
                    fab.hide();
                } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
                    fab.show();
                }
            }
        });


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void ReturnHome(View view) {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    public void init() {
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        fab = (FloatingActionButton) findViewById(R.id.fab_ticket);
        toolbar = (Toolbar) findViewById(R.id.toolbar_ticket);
        show_errorText = (TextView) findViewById(R.id.no_doubt_error1);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_ticket);
        //Progress dialog initialisation
        dialog = new SpotsDialog(this, "FUN + EDUCATION", R.style.Custom);
        back_arrow = (ImageView) findViewById(R.id.ticketlist_back_arrow);
        notifcation_btn = (ImageButton) findViewById(R.id.notif_btn);
        cart_button = (ImageButton) findViewById(R.id.cart_btn);
        relativeLayoutDoubt = (RelativeLayout) findViewById(R.id.doubtList_layout);
        nsv = (NestedScrollView) findViewById(R.id.nsv_layout);
        relativeLayoutDoubt.setVisibility(View.GONE);
        no_doubt_error1 = (TextView) findViewById(R.id.no_doubt_error1);
        mTicketList = new ArrayList<>();
        mAdapter = new TicketListItem_Adapter(mTicketList, TicketList_Activity.this);
        recyclerView.setAdapter(mAdapter);

    }


    private void getDoubtList() {
        if (AppUtil.isInternetConnected(this)) {

            showDialog(null, "Please wait...");
            NetworkService.startActionGetDoubtListData(this, mServiceReceiver);
        } else {
            Intent in = new Intent(TicketList_Activity.this, Internet_Activity.class);
            startActivity(in);
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        hideDialog();
        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if (success != null && success.getSuccess() != null
                        && success.getSuccess().getDoubt_list() != null
                        && success.getSuccess().getDoubt_list().size() > 0) {

                    relativeLayoutDoubt.setVisibility(View.VISIBLE);
                    mTicketList.clear();
                    mTicketList.addAll(success.getSuccess().getDoubt_list());
                    mAdapter.notifyDataSetChanged();


                } else {

                }
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(TicketList_Activity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(TicketList_Activity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(TicketList_Activity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.UNEXPECTED_ERROR:
                relativeLayoutDoubt.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                nsv.setVisibility(View.GONE);
                toolbar.setBackgroundResource(0);
                relativeLayoutDoubt.setBackground(getResources().getDrawable(R.drawable.gradient));
                //nodoubt_layout.setVisibility(View.VISIBLE);
                no_doubt_error1.setVisibility(View.VISIBLE);
                show_errorText.setVisibility(View.VISIBLE);
                show_errorText.setText("You have not asked any doubt");
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
    }


}

