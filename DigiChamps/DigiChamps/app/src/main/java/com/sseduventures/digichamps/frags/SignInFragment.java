package com.sseduventures.digichamps.frags;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.sseduventures.digichamps.R;

import com.sseduventures.digichamps.activities.BasicInfomationActivity;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.activities.OnBoardActivity;
import com.sseduventures.digichamps.activities.ResetPasswordActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.DateTimeUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

public class SignInFragment extends BottomSheetDialogFragment implements ServiceReceiver.Receiver {

    private TextView textView,error_phone,error_password;
    private TextInputLayout email, password;
    private TextInputEditText input_email,  input_password;
    private String emailstr, passwordstr;
    private Button login_btn;
    private FormActivity mContext;

    private ServiceReceiver mServiceReceiver;
    private Handler mHandler;


    private ImageView closeButton;
    private LinearLayout login_gmail;
    private TextView terms;



    String phone_number;

    Bundle bundle;
    private FirebaseAnalytics mFirebaseAnalytics;

    public static SignInFragment newInstance(String mobile) {

        Bundle bundle = new Bundle();
        bundle.putString("mobile", mobile);
        SignInFragment bt = new SignInFragment();
        bt.setArguments(bundle);




        return bt;
    }


    public SignInFragment() {




    }


    private void init(View view) {


        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        TextView login = view.findViewById(R.id.alreadymember);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                ((OnBoardActivity) getActivity()).showBottomSheetDialogFragment(null);
            }
        });


        closeButton = view.findViewById(R.id.closeButton);
        textView = view.findViewById(R.id.forgot);
        email = view.findViewById(R.id.email);
        password = view.findViewById(R.id.password);
        input_email = view.findViewById(R.id.input_email);



        input_password = view.findViewById(R.id.input_password);
        login_btn = view.findViewById(R.id.login_btn);
        login_gmail = view.findViewById(R.id.login_gmail);
        terms = view.findViewById(R.id.terms);
        error_phone=view.findViewById(R.id.error_phone);
        error_password=view.findViewById(R.id.error_password);

        input_email.setText(phone_number);

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validation()) {


                    callLoginService();

                }
            }
        });


        input_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {

                error_phone.setVisibility(View.GONE);
            }
        });


        input_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                error_password.setVisibility(View.GONE);
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
            }
        });

        login_gmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();

            }
        });

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse("http://www.thedigichamps.com/student/TermsAndConditions"));
                startActivity(browserIntent);
            }
        });

    }


    private Boolean validation() {


        emailstr = input_email.getText().toString().trim();

        if (emailstr.isEmpty()) {


            error_phone.setVisibility(View.VISIBLE);
            error_phone.setText("Please Enter Your Mobile Number");
            return false;
        }


        if (emailstr.length() != 10) {

            error_phone.setVisibility(View.VISIBLE);
            error_phone.setText("Please Enter 10 Digits Mobile Number");

            return false;
        }

        passwordstr = input_password.getText().toString().trim();

        if (passwordstr.isEmpty()) {



            error_password.setVisibility(View.VISIBLE);
            error_password.setText("Please Enter Your Password");

            return false;
        }

        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        logEvent();

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_signin,
                container, false);
        ((FormActivity)getActivity()).logEvent(LogEventUtil.
                        KEY_sign_in,
                LogEventUtil.EVENT_sign_in);

        ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_sign_in);

        init(view);
        mContext = (OnBoardActivity) getActivity();



        if(getArguments()!=null){

            input_email.setText(getArguments().getString("mobile"));
        }


        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ResetPasswordActivity.class);
                intent.putExtra("mobileNum",input_email.getText().toString().trim());
                startActivity(intent);

            }
        });

        return view;
    }


    private void callLoginService() {


        Bundle bun = new Bundle();
        bun.putString("MobileKey", input_email.getText().toString().trim());
        bun.putString("PasswordKey", input_password.getText().toString().trim());


        if (AppUtil.isInternetConnected(getActivity())) {

            mContext.showDialog(null, "You learn something everyday if you pay attention");
            NetworkService.startActionLogin(getActivity(),
                    mServiceReceiver, bun);

        } else {
            AskOptionDialogNew("Please Check Your Internet Connection").show();
        }

    }

    private AlertDialog AskOptionDialogNew(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })

                .create();
        return myQuittingDialogBox;
    }

    @Override
    public void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        mContext.hideDialog();
        switch (resultCode) {
            case ResponseCodes.SUCCESS:


                Intent ink = new Intent(getActivity(), NewDashboardActivity.class);
                startActivity(ink);
                if(getActivity()!=null)
                getActivity().finish();


                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(), ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(), ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(), Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.NO_USER_FOUND:

                AskOptionDialog("The number is not resgistered yet. Please Sign Up").show();


                break;
            case ResponseCodes.PASSWORD_MISMATCH:

                error_password.setVisibility(View.VISIBLE);
                error_password.setText("Password is Incorrect");

                break;
            case ResponseCodes.SERVER_ERROR:
                break;

            case 600:
                Intent intentr = new Intent(getActivity(),
                        BasicInfomationActivity.class);
                intentr.putExtra("data",resultData.
                        getParcelable(IntentHelper.RESULT_DATA));
                startActivity(intentr);
                if(getActivity()!=null)
                getActivity().finish();
                break;
        }
    }


    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mContext)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Sign Up", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                       // dialog.dismiss();
                        dialog.dismiss();
                        dismiss();
                        ((OnBoardActivity) getActivity()).
                                showBottomSheetDialogFragment(input_email.getText().toString().trim());
                    }
                })

                .create();
        return myQuittingDialogBox;
    }
    private void logEvent()
    {
        Bundle bundle = new Bundle();
        bundle.putString("OnSignIn", DateTimeUtil.convertMillisAsDateTimeString(
                System.currentTimeMillis()));
        mFirebaseAnalytics.logEvent("Screen_OnSignIn", bundle);
    }
}