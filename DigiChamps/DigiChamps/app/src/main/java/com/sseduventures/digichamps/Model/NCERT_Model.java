package com.sseduventures.digichamps.Model;

/**
 * Created by Tech_1 on 1/8/2018.
 */

public class NCERT_Model {
    private String PreviousYearPaper_ID,NCERTSolutions_PDFName,ClassName,PreviousYearPaper_PDFName,PDF_UploadPath;

    public NCERT_Model() {
    }

    public NCERT_Model( String PreviousYearPaper_ID,String ClassName,
                           String NCERTSolutions_PDFName, String PDF_UploadPath ) {

        this.PreviousYearPaper_PDFName = PreviousYearPaper_PDFName;
        this.ClassName = ClassName;
        this.PDF_UploadPath = PDF_UploadPath;
        this.NCERTSolutions_PDFName = NCERTSolutions_PDFName;
        this.PreviousYearPaper_ID=PreviousYearPaper_ID;
    }

    public String getPreviousYearPaper_ID() {
        return PreviousYearPaper_ID;
    }

    public String getNCERTSolutions_PDFName() {
        return NCERTSolutions_PDFName;
    }

    public String getClassName() {
        return ClassName;
    }

    public String getPDF_UploadPath() {
        return PDF_UploadPath;
    }


}
