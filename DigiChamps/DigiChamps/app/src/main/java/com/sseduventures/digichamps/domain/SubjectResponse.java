package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/24/2018.
 */

public class SubjectResponse implements Parcelable {

    private int subId;
    private String subName;
    private List<SubjectchapList> chapList;


    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public List<SubjectchapList> getChapList() {
        return chapList;
    }

    public void setChapList(List<SubjectchapList> chapList) {
        this.chapList = chapList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.subId);
        dest.writeString(this.subName);
        dest.writeTypedList(this.chapList);
    }

    public SubjectResponse() {
    }

    protected SubjectResponse(Parcel in) {
        this.subId = in.readInt();
        this.subName = in.readString();
        this.chapList = in.createTypedArrayList(SubjectchapList.CREATOR);
    }

    public static final Creator<SubjectResponse> CREATOR = new Creator<SubjectResponse>() {
        @Override
        public SubjectResponse createFromParcel(Parcel source) {
            return new SubjectResponse(source);
        }

        @Override
        public SubjectResponse[] newArray(int size) {
            return new SubjectResponse[size];
        }
    };
}
