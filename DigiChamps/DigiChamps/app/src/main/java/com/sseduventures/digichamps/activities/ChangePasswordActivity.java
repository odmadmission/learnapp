package com.sseduventures.digichamps.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.domain.Registration;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SharedPrefManager;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;


public class ChangePasswordActivity extends FormActivity implements
        View.OnClickListener, ServiceReceiver.Receiver {


    private Button submit_but;
    private TextInputLayout newPassword;
    private TextInputLayout ConfirmPassword;
    private String password;
    private String confirm_password;
    private TextInputEditText newPassword_edit;
    private TextInputEditText confirm_password_edit;
    private TextView error_new_password,error_confirm_password;


    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private Registration success;
    private Toolbar toolbar;


    String mobileNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.change_password);
        if (AppUtil.isInternetConnected(this)) {
            init();

            Intent in = getIntent();

            if (in != null) {

                mobileNum = in.getStringExtra("phNum");
            }
        }
        else {

           /* Intent in = new Intent(BasicInfomationActivity.this,Internet_Activity.class);
            startActivity(in);*/
            AskOptionDialogNew("Please Check Your Internet Connection").show();
        }

    }

    private void init() {


        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        newPassword = findViewById(R.id.new_pass);

        ConfirmPassword = findViewById(R.id.confirm_pass);
        newPassword_edit = findViewById(R.id.new_pass_edit);
        confirm_password_edit = findViewById(R.id.confirm_pass_edit);

        submit_but = (Button) findViewById(R.id.submit_but);
        submit_but.setOnClickListener(this);
        error_new_password=(TextView)findViewById(R.id.error_new_password);
        error_confirm_password=(TextView)findViewById(R.id.error_confirm_password);

       /* toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backarrow));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });*/

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        newPassword_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
               /* if(!newPassword_edit.getText().toString().isEmpty())
                    newPassword.setError(null);
                newPassword.setErrorEnabled(false);*/
                error_new_password.setVisibility(View.GONE);
            }
        });
        confirm_password_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
               /* if (confirm_password_edit.getError() != null) {
                    ConfirmPassword.setError(null);
                    ConfirmPassword.setErrorEnabled(false);
                }*/
                error_confirm_password.setVisibility(View.GONE);
            }
        });

    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ChangePasswordActivity.this, ResetPasswordActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit_but:
                if (validata()) {
                    changePassword();
                }
                break;
        }
    }

    private boolean validata() {

        password = newPassword_edit.getText().toString().trim();
        confirm_password = confirm_password_edit.getText().toString().trim();


        if (password.isEmpty()) {
           // AskOptionDialogNew("Please Enter New Password").show();
            //newPassword.setError("Please Enter New Password");
            error_new_password.setVisibility(View.VISIBLE);
            error_new_password.setText("Please Enter New Password");
            return false;
        }

        if (confirm_password.isEmpty()) {
           // AskOptionDialogNew("Please Enter Confirm Password").show();
          //  ConfirmPassword.setError("please Enter Confirm Password");
            error_confirm_password.setVisibility(View.VISIBLE);
            error_confirm_password.setText("please Enter Confirm Password");
            return false;
        }


        if (!password.equals(confirm_password)) {
           // AskOptionDialogNew("Password Don't Match").show();
          //  ConfirmPassword.setError("Passwords don't match");
            error_confirm_password.setVisibility(View.VISIBLE);
            error_confirm_password.setText("Passwords don't match");
            return false;
        }

        return true;

    }


    private void changePassword() {

        final String token = SharedPrefManager.getInstance(this).getDeviceToken();

        Bundle bun = new Bundle();
        bun.putString("mobile", mobileNum);
        bun.putString("password", confirm_password_edit.getText().toString().trim());
        bun.putString("deviceId", token);


        if (AppUtil.isInternetConnected(this)) {

            showDialog(null, "please wait");
            NetworkService.startActionGetChangePassword(this
                    , mServiceReceiver, bun);

        } else {
            passIntent(Internet_Activity.class);
        }

    }


    @Override
    protected void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                //success=resultData.getParcelable(IntentHelper.RESULT_DATA);
                passIntent(NewDashboardActivity.class);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(ChangePasswordActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent intent = new Intent(ChangePasswordActivity.this, ErrorActivity.class);
                startActivity(intent);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intenti = new Intent(ChangePasswordActivity.this, Internet_Activity.class);
                startActivity(intenti);
                break;
            case ResponseCodes.NO_USER_FOUND:

                AskOptionDialog("This number is not registered").show();
                break;

            case ResponseCodes.SERVER_ERROR:
                break;
            case 600:
                Intent intentr = new Intent(ChangePasswordActivity.this,
                        BasicInfomationActivity.class);
                intentr.putExtra("data",resultData.getParcelable(IntentHelper.RESULT_DATA));
                startActivity(intentr);
                break;
        }
    }

    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    private AlertDialog AskOptionDialogNew(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })

                .create();
        return myQuittingDialogBox;
    }


    private void passIntent(Class<?> cls) {


        Intent in = new Intent(ChangePasswordActivity.this, cls);
        startActivity(in);


    }


}
