package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class InfoList implements Parcelable{

    private String ThumbnailPath;
    private String SchoolId;
    private String SchoolName;
    private String Information;
    private String Logo;
    private String DocumentaryVideo;
    private boolean IsActive;
    private String ImageFileUpload;
    private String VideoFileUpload;


    public String getThumbnailPath() {
        return ThumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        ThumbnailPath = thumbnailPath;
    }

    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }

    public String getInformation() {
        return Information;
    }

    public void setInformation(String information) {
        Information = information;
    }

    public String getLogo() {
        return Logo;
    }

    public void setLogo(String logo) {
        Logo = logo;
    }

    public String getDocumentaryVideo() {
        return DocumentaryVideo;
    }

    public void setDocumentaryVideo(String documentaryVideo) {
        DocumentaryVideo = documentaryVideo;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public String getImageFileUpload() {
        return ImageFileUpload;
    }

    public void setImageFileUpload(String imageFileUpload) {
        ImageFileUpload = imageFileUpload;
    }

    public String getVideoFileUpload() {
        return VideoFileUpload;
    }

    public void setVideoFileUpload(String videoFileUpload) {
        VideoFileUpload = videoFileUpload;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ThumbnailPath);
        dest.writeString(this.SchoolId);
        dest.writeString(this.SchoolName);
        dest.writeString(this.Information);
        dest.writeString(this.Logo);
        dest.writeString(this.DocumentaryVideo);
        dest.writeByte(this.IsActive ? (byte) 1 : (byte) 0);
        dest.writeString(this.ImageFileUpload);
        dest.writeString(this.VideoFileUpload);
    }

    public InfoList() {
    }

    protected InfoList(Parcel in) {
        this.ThumbnailPath = in.readString();
        this.SchoolId = in.readString();
        this.SchoolName = in.readString();
        this.Information = in.readString();
        this.Logo = in.readString();
        this.DocumentaryVideo = in.readString();
        this.IsActive = in.readByte() != 0;
        this.ImageFileUpload = in.readString();
        this.VideoFileUpload = in.readString();
    }

    public static final Creator<InfoList> CREATOR = new Creator<InfoList>() {
        @Override
        public InfoList createFromParcel(Parcel source) {
            return new InfoList(source);
        }

        @Override
        public InfoList[] newArray(int size) {
            return new InfoList[size];
        }
    };
}
