package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 8/28/2018.
 */

public class GetPrtExamResponse implements Parcelable {

    private Integer Class_Id;
    private Integer Exam_ID;
    private String Exam_Name;

    protected GetPrtExamResponse(Parcel in) {
        if (in.readByte() == 0) {
            Class_Id = null;
        } else {
            Class_Id = in.readInt();
        }
        if (in.readByte() == 0) {
            Exam_ID = null;
        } else {
            Exam_ID = in.readInt();
        }
        Exam_Name = in.readString();
        byte tmpIs_Global = in.readByte();
        Is_Global = tmpIs_Global == 0 ? null : tmpIs_Global == 1;
        if (in.readByte() == 0) {
            Question_nos = null;
        } else {
            Question_nos = in.readInt();
        }
        Subject = in.readString();
        if (in.readByte() == 0) {
            Subject_Id = null;
        } else {
            Subject_Id = in.readInt();
        }
        if (in.readByte() == 0) {
            Time = null;
        } else {
            Time = in.readInt();
        }
        if (in.readByte() == 0) {
            Attempt_nos = null;
        } else {
            Attempt_nos = in.readInt();
        }
        if (in.readByte() == 0) {
            student_Attempt = null;
        } else {
            student_Attempt = in.readInt();
        }
        if (in.readByte() == 0) {
            Participants = null;
        } else {
            Participants = in.readInt();
        }
        byte tmpIs_free = in.readByte();
        is_free = tmpIs_free == 0 ? null : tmpIs_free == 1;
        if (in.readByte() == 0) {
            Validity = null;
        } else {
            Validity = in.readInt();
        }
        if (in.readByte() == 0) {
            Chapter_Id = null;
        } else {
            Chapter_Id = in.readInt();
        }
        if (in.readByte() == 0) {
            Board_Id = null;
        } else {
            Board_Id = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (Class_Id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Class_Id);
        }
        if (Exam_ID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Exam_ID);
        }
        dest.writeString(Exam_Name);
        dest.writeByte((byte) (Is_Global == null ? 0 : Is_Global ? 1 : 2));
        if (Question_nos == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Question_nos);
        }
        dest.writeString(Subject);
        if (Subject_Id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Subject_Id);
        }
        if (Time == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Time);
        }
        if (Attempt_nos == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Attempt_nos);
        }
        if (student_Attempt == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(student_Attempt);
        }
        if (Participants == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Participants);
        }
        dest.writeByte((byte) (is_free == null ? 0 : is_free ? 1 : 2));
        if (Validity == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Validity);
        }
        if (Chapter_Id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Chapter_Id);
        }
        if (Board_Id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(Board_Id);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GetPrtExamResponse> CREATOR = new Creator<GetPrtExamResponse>() {
        @Override
        public GetPrtExamResponse createFromParcel(Parcel in) {
            return new GetPrtExamResponse(in);
        }

        @Override
        public GetPrtExamResponse[] newArray(int size) {
            return new GetPrtExamResponse[size];
        }
    };

    public Integer getClass_Id() {
        return Class_Id;
    }

    public void setClass_Id(Integer class_Id) {
        Class_Id = class_Id;
    }

    public Integer getExam_ID() {
        return Exam_ID;
    }

    public void setExam_ID(Integer exam_ID) {
        Exam_ID = exam_ID;
    }

    public String getExam_Name() {
        return Exam_Name;
    }

    public void setExam_Name(String exam_Name) {
        Exam_Name = exam_Name;
    }

    public Boolean getIs_Global() {
        return Is_Global;
    }

    public void setIs_Global(Boolean is_Global) {
        Is_Global = is_Global;
    }

    public Integer getQuestion_nos() {
        return Question_nos;
    }

    public void setQuestion_nos(Integer question_nos) {
        Question_nos = question_nos;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public Integer getSubject_Id() {
        return Subject_Id;
    }

    public void setSubject_Id(Integer subject_Id) {
        Subject_Id = subject_Id;
    }

    public Integer getTime() {
        return Time;
    }

    public void setTime(Integer time) {
        Time = time;
    }

    public Integer getAttempt_nos() {
        return Attempt_nos;
    }

    public void setAttempt_nos(Integer attempt_nos) {
        Attempt_nos = attempt_nos;
    }

    public Integer getStudent_Attempt() {
        return student_Attempt;
    }

    public void setStudent_Attempt(Integer student_Attempt) {
        this.student_Attempt = student_Attempt;
    }

    public Integer getParticipants() {
        return Participants;
    }

    public void setParticipants(Integer participants) {
        Participants = participants;
    }

    public Boolean getIs_free() {
        return is_free;
    }

    public void setIs_free(Boolean is_free) {
        this.is_free = is_free;
    }

    public Integer getValidity() {
        return Validity;
    }

    public void setValidity(Integer validity) {
        Validity = validity;
    }

    public Integer getChapter_Id() {
        return Chapter_Id;
    }

    public void setChapter_Id(Integer chapter_Id) {
        Chapter_Id = chapter_Id;
    }

    public Integer getBoard_Id() {
        return Board_Id;
    }

    public void setBoard_Id(Integer board_Id) {
        Board_Id = board_Id;
    }

    private Boolean Is_Global;
    private Integer Question_nos;
    private String Subject;
    private Integer Subject_Id;
    private Integer Time;
    private Integer Attempt_nos;
    private Integer student_Attempt;
    private Integer Participants;
    private Boolean is_free;
    private Integer Validity;
    private Integer Chapter_Id;
    private Integer Board_Id;



}
