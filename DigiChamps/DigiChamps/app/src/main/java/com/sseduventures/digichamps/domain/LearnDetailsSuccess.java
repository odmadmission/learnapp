package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class LearnDetailsSuccess implements Parcelable{


    private LearnSubjectProgressData SubjectProgressData;
    private List<LearnChapterlist> Chapterlist;
    private int Total_Chapters;
    private int Total_Online_test;
    private int Total_Pre_req_test;
    private int Total_question_pdf;
    private int Total_Videos;
    private int Total_question;
    private int total_pdfs;


    protected LearnDetailsSuccess(Parcel in) {
        SubjectProgressData = in.readParcelable(LearnSubjectProgressData.class.getClassLoader());
        Chapterlist = in.createTypedArrayList(LearnChapterlist.CREATOR);
        Total_Chapters = in.readInt();
        Total_Online_test = in.readInt();
        Total_Pre_req_test = in.readInt();
        Total_question_pdf = in.readInt();
        Total_Videos = in.readInt();
        Total_question = in.readInt();
        total_pdfs = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(SubjectProgressData, flags);
        dest.writeTypedList(Chapterlist);
        dest.writeInt(Total_Chapters);
        dest.writeInt(Total_Online_test);
        dest.writeInt(Total_Pre_req_test);
        dest.writeInt(Total_question_pdf);
        dest.writeInt(Total_Videos);
        dest.writeInt(Total_question);
        dest.writeInt(total_pdfs);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LearnDetailsSuccess> CREATOR = new Creator<LearnDetailsSuccess>() {
        @Override
        public LearnDetailsSuccess createFromParcel(Parcel in) {
            return new LearnDetailsSuccess(in);
        }

        @Override
        public LearnDetailsSuccess[] newArray(int size) {
            return new LearnDetailsSuccess[size];
        }
    };

    public LearnSubjectProgressData getSubjectProgressData() {
        return SubjectProgressData;
    }

    public void setSubjectProgressData(LearnSubjectProgressData subjectProgressData) {
        SubjectProgressData = subjectProgressData;
    }

    public List<LearnChapterlist> getChapterlist() {
        return Chapterlist;
    }

    public void setChapterlist(List<LearnChapterlist> chapterlist) {
        Chapterlist = chapterlist;
    }

    public int getTotal_Chapters() {
        return Total_Chapters;
    }

    public void setTotal_Chapters(int total_Chapters) {
        Total_Chapters = total_Chapters;
    }

    public int getTotal_Online_test() {
        return Total_Online_test;
    }

    public void setTotal_Online_test(int total_Online_test) {
        Total_Online_test = total_Online_test;
    }

    public int getTotal_Pre_req_test() {
        return Total_Pre_req_test;
    }

    public void setTotal_Pre_req_test(int total_Pre_req_test) {
        Total_Pre_req_test = total_Pre_req_test;
    }

    public int getTotal_question_pdf() {
        return Total_question_pdf;
    }

    public void setTotal_question_pdf(int total_question_pdf) {
        Total_question_pdf = total_question_pdf;
    }

    public int getTotal_Videos() {
        return Total_Videos;
    }

    public void setTotal_Videos(int total_Videos) {
        Total_Videos = total_Videos;
    }

    public int getTotal_question() {
        return Total_question;
    }

    public void setTotal_question(int total_question) {
        Total_question = total_question;
    }

    public int getTotal_pdfs() {
        return total_pdfs;
    }

    public void setTotal_pdfs(int total_pdfs) {
        this.total_pdfs = total_pdfs;
    }
}
