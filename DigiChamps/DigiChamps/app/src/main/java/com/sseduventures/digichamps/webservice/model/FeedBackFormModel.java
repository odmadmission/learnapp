package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FeedBackFormModel implements Parcelable {

    protected FeedBackFormModel(Parcel in) {
    }

    public static final Creator<FeedBackFormModel> CREATOR = new Creator<FeedBackFormModel>() {
        @Override
        public FeedBackFormModel createFromParcel(Parcel in) {
            return new FeedBackFormModel(in);
        }

        @Override
        public FeedBackFormModel[] newArray(int size) {
            return new FeedBackFormModel[size];
        }
    };

    public SuccessClass getSuccess() {
        return Success;
    }

    public void setSuccess(SuccessClass success) {
        Success = success;
    }

    private SuccessClass Success;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public class SuccessClass implements Parcelable{

        private String Message;

        protected SuccessClass(Parcel in) {
            Message = in.readString();
        }

        public final Creator<SuccessClass> CREATOR = new Creator<SuccessClass>() {
            @Override
            public SuccessClass createFromParcel(Parcel in) {
                return new SuccessClass(in);
            }

            @Override
            public SuccessClass[] newArray(int size) {
                return new SuccessClass[size];
            }
        };

        public String getMessage() {
            return Message;
        }

        public void setMessage(String message) {
            Message = message;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(Message);
        }
    }
}
