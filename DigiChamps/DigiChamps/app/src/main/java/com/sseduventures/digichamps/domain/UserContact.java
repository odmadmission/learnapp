package com.sseduventures.digichamps.domain;

/**
 * Created by NISHIKANT on 6/29/2018.
 */

public class UserContact
{

    private String Name;
    private String Mobile;
    private int RegId;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public int getRegId() {
        return RegId;
    }

    public void setRegId(int regId) {
        RegId = regId;
    }
}
