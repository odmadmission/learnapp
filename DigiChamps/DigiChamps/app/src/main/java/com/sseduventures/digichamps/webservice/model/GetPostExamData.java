package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class GetPostExamData implements Parcelable {

    protected GetPostExamData(Parcel in) {
    }

    public static final Creator<GetPostExamData> CREATOR = new Creator<GetPostExamData>() {
        @Override
        public GetPostExamData createFromParcel(Parcel in) {
            return new GetPostExamData(in);
        }

        @Override
        public GetPostExamData[] newArray(int size) {
            return new GetPostExamData[size];
        }
    };

    public PostExamSuccess getSuccess() {
        return success;
    }

    public void setSuccess(PostExamSuccess success) {
        this.success = success;
    }

    private PostExamSuccess success;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
