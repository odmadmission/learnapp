package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class HomeworkResponse implements Parcelable {

    private List<HomeworkList> list;
    private boolean status;
    private String message;

    public List<HomeworkList> getList() {
        return list;
    }

    public void setList(List<HomeworkList> list) {
        this.list = list;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.list);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.message);
    }

    public HomeworkResponse() {
    }

    protected HomeworkResponse(Parcel in) {
        this.list = in.createTypedArrayList(HomeworkList.CREATOR);
        this.status = in.readByte() != 0;
        this.message = in.readString();
    }

    public static final Creator<HomeworkResponse> CREATOR = new Creator<HomeworkResponse>() {
        @Override
        public HomeworkResponse createFromParcel(Parcel source) {
            return new HomeworkResponse(source);
        }

        @Override
        public HomeworkResponse[] newArray(int size) {
            return new HomeworkResponse[size];
        }
    };
}
