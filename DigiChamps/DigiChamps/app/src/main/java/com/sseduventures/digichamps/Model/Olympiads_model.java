package com.sseduventures.digichamps.Model;

/**
 * Created by Tech_1 on 1/8/2018.
 */

public class Olympiads_model{
        private String CompetitiveExamQs_ID,CompetitiveExamQs_PDFName,ClassName,PDF_UploadPath;


        public Olympiads_model(String CompetitiveExamQs_ID,String ClassName,String CompetitiveExamQs_PDFName,String PDF_UploadPath) {

            this.CompetitiveExamQs_PDFName = CompetitiveExamQs_PDFName;
            this.ClassName = ClassName;
            this.PDF_UploadPath = PDF_UploadPath;
            this.CompetitiveExamQs_ID=CompetitiveExamQs_ID;
        }

        public String getCompetitiveExamQs_ID() {
            return CompetitiveExamQs_ID;
        }

        public String getCompetitiveExamQs_PDFName() {
            return CompetitiveExamQs_PDFName;
        }

        public String getClassName() {
            return ClassName;
        }

        public String getPDF_UploadPath() {
            return PDF_UploadPath;
        }


}



