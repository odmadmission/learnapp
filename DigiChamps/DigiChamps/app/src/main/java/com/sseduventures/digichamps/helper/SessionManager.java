package com.sseduventures.digichamps.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;



/**
 * Created by ntspl22 on 12/7/2016.
 */

public class  SessionManager {
    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;
    SpotsDialog pDialog;
    private static String resp, error,User_Id,sessionId;
    Editor editor;
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Shared preferences file name
    private static final String PREF_NAME = "DigiChamps";
    // logged in
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    private static final String KEY_IS_FIRST_INSTALL = "isFirstInstall";
    private static final String KEY_IS_BOARD_SELECTED = "IsBoardSelected";




    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    // for selection of board
    public void setBoardSelected(boolean IsBoardSelected) {
        editor.putBoolean(KEY_IS_BOARD_SELECTED, IsBoardSelected);
        // commit changes
        editor.commit();
        Log.d(TAG, "User login session modified!");
    }
    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        // commit changes
        editor.commit();
        Log.d(TAG, "User login session modified!");
    }

    public void setFirstLaunch(boolean isFirstInstall) {
        editor.putBoolean(KEY_IS_FIRST_INSTALL, isFirstInstall);
        // commit changes
        editor.commit();
        Log.d(TAG, "User login session modified!");
    }
    /*
        * Logout user data from session and shared preferences
        */
    public void logout() {
       //  logout_session();
        // Clearing all data from Shared Preferences
        SharedPreferences preference = _context.getSharedPreferences("user_data", _context.MODE_PRIVATE);
        preference.edit().remove("User_ID").apply();
        preference.edit().remove("UserName").apply();
        preference.edit().remove("Boardid").apply();
        preference.edit().remove("classid").apply();
        preference.edit().remove("Is_Doubt").apply();
        preference.edit().remove("Is_Mentor").apply();
        preference.edit().remove("asignteacherid").apply();
        preference.edit().remove("roleid").apply();
        preference.edit().remove("schoolid").apply();
       // preference.edit().remove("schoolName").apply();
        preference.edit().remove("sectionid").apply();



        // set login value to false
       setLogin(false);
    }
    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }
    public boolean isBoardSelected() {
        return pref.getBoolean(KEY_IS_BOARD_SELECTED, false);
    }

    public boolean isFirstInstall() {
        return pref.getBoolean(KEY_IS_FIRST_INSTALL, false);
    }

     //web service call for logout method
   /* public void logout_session() {

        SharedPreferences preference = _context.getSharedPreferences("user_data", _context.MODE_PRIVATE);
        User_Id = preference.getString("User_ID", null);
        sessionId = preference.getString("SessionID", null);

        //Progress dialog initialisation
        pDialog = new SpotsDialog(_context, "Fun+Education",R.style.Custom);


        //showDialog();
        StringRequest strReq = new StringRequest(Request.Method.GET,
                //2100 should be removed by chapter_id
                AppConfig.STATUS_LOGOUT + sessionId + "/" + User_Id, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response);
                //hide dialog
               // hideDialog();

                if (!response.isEmpty()) {
                    try {
                        JSONObject jObj = new JSONObject(response);
                        resp = jObj.optString("success");
                        error = jObj.optString("error");
                        // Check for error node in json

                        if (!resp.isEmpty()) {
                            // sending data for logout
                            JSONObject logout = jObj.getJSONObject("success");
                            String message = logout.getString("Message");
                            Toast.makeText(_context,message, Toast.LENGTH_LONG).show();

                        } else if (!error.isEmpty()) {
                            JSONObject srv_error = jObj.getJSONObject("error");
                            String error_message = srv_error.getString("Message");
                            Log.d("errrrroorrrr", error_message);
                        } else {
                            Log.d("errrrroorrrr", "Server Error");
                        }
                    } catch (JSONException e) {
                        // JSON error
                        e.printStackTrace();
                        Log.d("errrrroorrrrgfhff", e.getMessage());
                        Toast.makeText(_context, "JSON Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Server Error: " + error.getMessage());
                Toast.makeText(_context, "Connection Error", Toast.LENGTH_LONG).show();
               // hideDialog();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String Auth_key ="bearer nZURcdgCAdvIDg24xBVhJ9GquhejbpKdXf74zt4qf1_9XFZ1FUqmk__ryZs9cw620g-wVjYulYtHaGlw7jNOPsDj6COCy0fiaoDVqAMMeliMAWIPrIpeneeKShnrT7qw0ogHjaaxL7k2lCMst5aWkE8Iw6F1a5dWX8h60JhaOiWEvANCDPm2EE-iE72QmQJ4mbgNO09I07x3BdIXEytT_AkS1jwBLg0u5RU325mnRYv1cyDGx52tXhQZ5SjS_g6BdaZeYRs5jrJCiIbcde2GBF3Wx_qpBOf7LHfcDspDILgkwIBGKY66S1PrJte-K_hs7G4LKgQIuwuGgmjOqJjWQA";
                headers.put("Authorization", Auth_key);
                return headers;
            }
        };
        //To remove cache
        strReq.setShouldCache(false);
        // Adding request to request queue
        AppController.getInstance().
                addToRequestQueue(strReq);
    }*/

  /*  //method for showing progress dialog
    private void showDialog() {
        pDialog.showDialog();
    }

    //method for hiding progress dialog
    private void hideDialog() {
        pDialog.showDialog();
    }*/

}