package com.sseduventures.digichamps.Model;

public class DataModel_Testimonials {


    public String Testimonial_Name,Testimonial_Title,TotalMark,Testimonial_Images,Class_Name,Description;

    public String getTestimonial_Name() {
        return Testimonial_Name;
    }

    public void setTestimonial_Name(String testimonial_Name) {
        Testimonial_Name = testimonial_Name;
    }

    public String getTestimonial_Title() {
        return Testimonial_Title;
    }

    public void setTestimonial_Title(String testimonial_Title) {
        Testimonial_Title = testimonial_Title;
    }

    public String getTotalMark() {
        return TotalMark;
    }

    public void setTotalMark(String totalMark) {
        TotalMark = totalMark;
    }

    public String getTestimonial_Images() {
        return Testimonial_Images;
    }

    public void setTestimonial_Images(String testimonial_Images) {
        Testimonial_Images = testimonial_Images;
    }

    public String getClass_Name() {
        return Class_Name;
    }

    public void setClass_Name(String class_Name) {
        Class_Name = class_Name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public DataModel_Testimonials(String Testimonial_Name, String Testimonial_Title, String TotalMark, String Testimonial_Images, String Class_Name, String Description) {
        this.Testimonial_Name = Testimonial_Name;
        this.Testimonial_Title = Testimonial_Title;
        this.TotalMark = TotalMark;
        this.Testimonial_Images = Testimonial_Images;
        this.Class_Name = Class_Name;
        this.Description = Description;
    }
}