package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.crashlytics.android.Crashlytics;
import com.sseduventures.digichamps.activities.FormActivity;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;



public class Internet_Activity extends FormActivity {

    private Button retry;
    Boolean isInternetPresent = false;
    RelativeLayout lin;
    View.OnClickListener mOnClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_no_internet);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        lin = (RelativeLayout) findViewById(R.id.layout_internet);
        retry = (Button) findViewById(R.id.retry);
        retry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                checkSnackbar();
            }
        });

        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkSnackbar();

            }
        };
    }

    public  void checkSnackbar(){


        // TODO Auto-generated method stub
                ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                if (connectivityManager != null){
                    NetworkInfo netInfos = connectivityManager.getActiveNetworkInfo();
                    if(netInfos != null) {
                        if (netInfos.isConnected()) {
                            isInternetPresent = true;

                            if(isInternetPresent){
                                back();

                            }
                        }

                    }else {
                        Snackbar snackbar = Snackbar
                                .make(lin, "No internet connection", Snackbar.LENGTH_LONG)
                                .setAction("Retry", mOnClickListener);
                        snackbar.setActionTextColor(Color.WHITE);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.DKGRAY);
                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.WHITE);
                        snackbar.show();
                    }
                }
            }

    public  boolean checkInternet(){


        // TODO Auto-generated method stub
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null){
            NetworkInfo netInfos = connectivityManager.getActiveNetworkInfo();
            //checking for internet
            if(netInfos != null) {
                if (netInfos.isConnected()) {
                    isInternetPresent = true;
                }

            }
        }
        return isInternetPresent;
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setTitle(Html.fromHtml("<font color='#1F1F1F'>Exit?</font>"))
                .setIcon(R.drawable.alert_warning);
        AlertDialog alert = builder.create();
        alert.show();
    }
    public void back(){
        super.onBackPressed();
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    }

