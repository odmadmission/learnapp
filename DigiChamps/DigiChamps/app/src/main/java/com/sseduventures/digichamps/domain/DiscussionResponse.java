package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class DiscussionResponse implements Parcelable{

    private List<DiscussionList> Discussion;
    private String Message;
    private int DiscussionID;
    private int ResultCount;
    private boolean status;

    public List<DiscussionList> getDiscussion() {
        return Discussion;
    }

    public void setDiscussion(List<DiscussionList> discussion) {
        Discussion = discussion;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getDiscussionID() {
        return DiscussionID;
    }

    public void setDiscussionID(int discussionID) {
        DiscussionID = discussionID;
    }

    public int getResultCount() {
        return ResultCount;
    }

    public void setResultCount(int resultCount) {
        ResultCount = resultCount;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.Discussion);
        dest.writeString(this.Message);
        dest.writeInt(this.DiscussionID);
        dest.writeInt(this.ResultCount);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
    }

    public DiscussionResponse() {
    }

    protected DiscussionResponse(Parcel in) {
        this.Discussion = in.createTypedArrayList(DiscussionList.CREATOR);
        this.Message = in.readString();
        this.DiscussionID = in.readInt();
        this.ResultCount = in.readInt();
        this.status = in.readByte() != 0;
    }

    public static final Creator<DiscussionResponse> CREATOR = new Creator<DiscussionResponse>() {
        @Override
        public DiscussionResponse createFromParcel(Parcel source) {
            return new DiscussionResponse(source);
        }

        @Override
        public DiscussionResponse[] newArray(int size) {
            return new DiscussionResponse[size];
        }
    };
}
