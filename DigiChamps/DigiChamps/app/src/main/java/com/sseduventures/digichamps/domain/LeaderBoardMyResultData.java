package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/5/2018.
 */

public class LeaderBoardMyResultData implements Parcelable {

    private int Max_rid;
    private int Regd_ID;
    private int Totaltime;
    private int Question_Nos;
    private int Total_Correct_Ans;
    private int Appear;
    private String Customer_Name;
    private String Image;
    private int Rank;
    private int Incorrect;
    private int Accuracy;

    public int getMax_rid() {
        return Max_rid;
    }

    public void setMax_rid(int max_rid) {
        Max_rid = max_rid;
    }

    public int getRegd_ID() {
        return Regd_ID;
    }

    public void setRegd_ID(int regd_ID) {
        Regd_ID = regd_ID;
    }

    public int getTotaltime() {
        return Totaltime;
    }

    public void setTotaltime(int totaltime) {
        Totaltime = totaltime;
    }

    public int getQuestion_Nos() {
        return Question_Nos;
    }

    public void setQuestion_Nos(int question_Nos) {
        Question_Nos = question_Nos;
    }

    public int getTotal_Correct_Ans() {
        return Total_Correct_Ans;
    }

    public void setTotal_Correct_Ans(int total_Correct_Ans) {
        Total_Correct_Ans = total_Correct_Ans;
    }

    public int getAppear() {
        return Appear;
    }

    public void setAppear(int appear) {
        Appear = appear;
    }

    public String getCustomer_Name() {
        return Customer_Name;
    }

    public void setCustomer_Name(String customer_Name) {
        Customer_Name = customer_Name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public int getRank() {
        return Rank;
    }

    public void setRank(int rank) {
        Rank = rank;
    }

    public int getIncorrect() {
        return Incorrect;
    }

    public void setIncorrect(int incorrect) {
        Incorrect = incorrect;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Max_rid);
        dest.writeInt(this.Regd_ID);
        dest.writeInt(this.Totaltime);
        dest.writeInt(this.Question_Nos);
        dest.writeInt(this.Total_Correct_Ans);
        dest.writeInt(this.Appear);
        dest.writeString(this.Customer_Name);
        dest.writeString(this.Image);
        dest.writeInt(this.Rank);
        dest.writeInt(this.Incorrect);
        dest.writeInt(this.Accuracy);
    }

    public LeaderBoardMyResultData() {
    }

    protected LeaderBoardMyResultData(Parcel in) {
        this.Max_rid = in.readInt();
        this.Regd_ID = in.readInt();
        this.Totaltime = in.readInt();
        this.Question_Nos = in.readInt();
        this.Total_Correct_Ans = in.readInt();
        this.Appear = in.readInt();
        this.Customer_Name = in.readString();
        this.Image = in.readString();
        this.Rank = in.readInt();
        this.Incorrect = in.readInt();
        this.Accuracy = in.readInt();
    }

    public static final Creator<LeaderBoardMyResultData> CREATOR = new Creator<LeaderBoardMyResultData>() {
        @Override
        public LeaderBoardMyResultData createFromParcel(Parcel source) {
            return new LeaderBoardMyResultData(source);
        }

        @Override
        public LeaderBoardMyResultData[] newArray(int size) {
            return new LeaderBoardMyResultData[size];
        }
    };
}
