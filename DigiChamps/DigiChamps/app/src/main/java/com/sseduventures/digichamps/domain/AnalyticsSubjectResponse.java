package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NISHIKANT on 7/10/2018.
 */

public class AnalyticsSubjectResponse implements Parcelable{

    private List<AnalyticsSubjectSuccess> success;
    private ArrayList<String> subjectList;

    public List<AnalyticsSubjectSuccess> getSuccess() {
        return success;
    }

    public void setSuccess(List<AnalyticsSubjectSuccess> success) {
        this.success = success;
    }

    public ArrayList<String> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(ArrayList<String> subjectList) {
        this.subjectList = subjectList;
    }


    public class AnalyticsSubjectSuccess implements Parcelable{

        private int SubjectId;
         private String Subject;
         private List<AnalyticsSubjectChapterList> ChapterList;

        public int getSubjectId() {
            return SubjectId;
        }

        public void setSubjectId(int subjectId) {
            SubjectId = subjectId;
        }

        public String getSubject() {
            return Subject;
        }

        public void setSubject(String subject) {
            Subject = subject;
        }

        public List<AnalyticsSubjectChapterList> getChapterList() {
            return ChapterList;
        }

        public void setChapterList(List<AnalyticsSubjectChapterList> chapterList) {
            ChapterList = chapterList;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.SubjectId);
            dest.writeString(this.Subject);
            dest.writeList(this.ChapterList);
        }

        public AnalyticsSubjectSuccess() {
        }

        protected AnalyticsSubjectSuccess(Parcel in) {
            this.SubjectId = in.readInt();
            this.Subject = in.readString();
            this.ChapterList = new ArrayList<AnalyticsSubjectChapterList>();
            in.readList(this.ChapterList, AnalyticsSubjectChapterList.class.getClassLoader());
        }

        public final Creator<AnalyticsSubjectSuccess> CREATOR = new Creator<AnalyticsSubjectSuccess>() {
            @Override
            public AnalyticsSubjectSuccess createFromParcel(Parcel source) {
                return new AnalyticsSubjectSuccess(source);
            }

            @Override
            public AnalyticsSubjectSuccess[] newArray(int size) {
                return new AnalyticsSubjectSuccess[size];
            }
        };
    }

    public class AnalyticsSubjectChapterList implements Parcelable{
        private int ChapterId;
        private String Chapter;
        private int PRT_TotalQnos;
        private int CBT_TotalQnos;
        private int PRT_TotalAns;
        private int CBT_TotalAns;

        public int getChapterId() {
            return ChapterId;
        }

        public void setChapterId(int chapterId) {
            ChapterId = chapterId;
        }

        public String getChapter() {
            return Chapter;
        }

        public void setChapter(String chapter) {
            Chapter = chapter;
        }

        public int getPRT_TotalQnos() {
            return PRT_TotalQnos;
        }

        public void setPRT_TotalQnos(int PRT_TotalQnos) {
            this.PRT_TotalQnos = PRT_TotalQnos;
        }

        public int getCBT_TotalQnos() {
            return CBT_TotalQnos;
        }

        public void setCBT_TotalQnos(int CBT_TotalQnos) {
            this.CBT_TotalQnos = CBT_TotalQnos;
        }

        public int getPRT_TotalAns() {
            return PRT_TotalAns;
        }

        public void setPRT_TotalAns(int PRT_TotalAns) {
            this.PRT_TotalAns = PRT_TotalAns;
        }

        public int getCBT_TotalAns() {
            return CBT_TotalAns;
        }

        public void setCBT_TotalAns(int CBT_TotalAns) {
            this.CBT_TotalAns = CBT_TotalAns;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.ChapterId);
            dest.writeString(this.Chapter);
            dest.writeInt(this.PRT_TotalQnos);
            dest.writeInt(this.CBT_TotalQnos);
            dest.writeInt(this.PRT_TotalAns);
            dest.writeInt(this.CBT_TotalAns);
        }

        public AnalyticsSubjectChapterList() {
        }

        protected AnalyticsSubjectChapterList(Parcel in) {
            this.ChapterId = in.readInt();
            this.Chapter = in.readString();
            this.PRT_TotalQnos = in.readInt();
            this.CBT_TotalQnos = in.readInt();
            this.PRT_TotalAns = in.readInt();
            this.CBT_TotalAns = in.readInt();
        }

        public final Creator<AnalyticsSubjectChapterList> CREATOR = new Creator<AnalyticsSubjectChapterList>() {
            @Override
            public AnalyticsSubjectChapterList createFromParcel(Parcel source) {
                return new AnalyticsSubjectChapterList(source);
            }

            @Override
            public AnalyticsSubjectChapterList[] newArray(int size) {
                return new AnalyticsSubjectChapterList[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.success);
        dest.writeStringList(this.subjectList);
    }

    public AnalyticsSubjectResponse() {
    }

    protected AnalyticsSubjectResponse(Parcel in) {
        this.success = new ArrayList<AnalyticsSubjectSuccess>();
        in.readList(this.success, AnalyticsSubjectSuccess.class.getClassLoader());
        this.subjectList = in.createStringArrayList();
    }

    public static final Creator<AnalyticsSubjectResponse> CREATOR = new Creator<AnalyticsSubjectResponse>() {
        @Override
        public AnalyticsSubjectResponse createFromParcel(Parcel source) {
            return new AnalyticsSubjectResponse(source);
        }

        @Override
        public AnalyticsSubjectResponse[] newArray(int size) {
            return new AnalyticsSubjectResponse[size];
        }
    };
}
