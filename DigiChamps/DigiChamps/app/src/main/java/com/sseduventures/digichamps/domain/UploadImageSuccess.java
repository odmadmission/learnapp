package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/26/2018.
 */

public class UploadImageSuccess implements Parcelable {

    private String Message;
    private long Regd_ID;
    private String image;

    protected UploadImageSuccess(Parcel in) {
        Message = in.readString();
        Regd_ID = in.readLong();
        image = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Message);
        dest.writeLong(Regd_ID);
        dest.writeString(image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UploadImageSuccess> CREATOR = new Creator<UploadImageSuccess>() {
        @Override
        public UploadImageSuccess createFromParcel(Parcel in) {
            return new UploadImageSuccess(in);
        }

        @Override
        public UploadImageSuccess[] newArray(int size) {
            return new UploadImageSuccess[size];
        }
    };

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public long getRegd_ID() {
        return Regd_ID;
    }

    public void setRegd_ID(long regd_ID) {
        Regd_ID = regd_ID;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
