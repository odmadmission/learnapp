package com.sseduventures.digichamps.Model;

import android.media.Image;

/**
 * Created by Tech_2 on 1/6/2018.
 */

public class SearchDictonaryModule {

    String dictonaryID,dictonaryName,dictonaryDeatils;
    String dictonaryImage;
    String dictonaryPath;



    public SearchDictonaryModule(String dictonaryID, String dictonaryName, String dictonaryDeatils, String dictonaryImage, String dictonaryPath) {
        this.dictonaryID = dictonaryID;
        this.dictonaryName = dictonaryName;
        this.dictonaryDeatils = dictonaryDeatils;
        this.dictonaryImage = dictonaryImage;
        this.dictonaryPath = dictonaryPath;
    }

    public String getDictonaryID() {
        return dictonaryID;
    }

    public void setDictonaryID(String dictonaryID) {
        this.dictonaryID = dictonaryID;
    }

    public String getDictonaryName() {
        return dictonaryName;
    }

    public void setDictonaryName(String dictonaryName) {
        this.dictonaryName = dictonaryName;
    }

    public String getDictonaryDeatils() {
        return dictonaryDeatils;
    }

    public void setDictonaryDeatils(String dictonaryDeatils) {
        this.dictonaryDeatils = dictonaryDeatils;
    }

    public String getDictonaryImage() {
        return dictonaryImage;
    }

    public void setDictonaryImage(String dictonaryImage) {
        this.dictonaryImage = dictonaryImage;
    }

    public String getDictonaryPath() {
        return dictonaryPath;
    }

    public void setDictonaryPath(String dictonaryPath) {
        this.dictonaryPath = dictonaryPath;
    }
}
