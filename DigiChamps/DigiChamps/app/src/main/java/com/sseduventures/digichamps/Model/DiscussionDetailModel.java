
package com.sseduventures.digichamps.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DiscussionDetailModel
{



    @SerializedName("DiscussionDetailID")
    @Expose
    private Integer discussionDetailID;
    @SerializedName("DiscussionID")
    @Expose
    private Integer discussionID;
    @SerializedName("DetailText")
    @Expose
    private String detailText;
    @SerializedName("CreatedDate")
    @Expose
    private String createdDate;
    @SerializedName("IsActive")
    @Expose
    private Integer isActive;
    @SerializedName("CreatedBy")
    @Expose
    private Integer createdBy;

    @SerializedName("RoleID")
    @Expose
    private Integer roleID;
    @SerializedName("CreatedName")
    @Expose
    private String createdName;
    @SerializedName("photoURL")
    @Expose
    private String photoURL;
    @SerializedName("colorCode")
    @Expose
    private Integer colorCode;
    @SerializedName("hashColorCode")
    @Expose
    private String hashColorCode;



    public String getHashColorCode() {
        return hashColorCode;
    }

    public void setHashColorCode(String hashColorCode) {
        this.hashColorCode = hashColorCode;
    }

    public Integer getColorCode() {
        return colorCode;
    }

    public void setColorCode(Integer colorCode) {
        this.colorCode = colorCode;
    }

    public Integer getDiscussionDetailID() {
        return discussionDetailID;
    }

    public void setDiscussionDetailID(Integer discussionDetailID) {
        this.discussionDetailID = discussionDetailID;
    }

    public Integer getDiscussionID() {
        return discussionID;
    }

    public void setDiscussionID(Integer discussionID) {
        this.discussionID = discussionID;
    }

    public String getDetailText() {
        return detailText;
    }

    public void setDetailText(String detailText) {
        this.detailText = detailText;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getRoleID() {
        return roleID;
    }

    public void setRoleID(Integer roleID) {
        this.roleID = roleID;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }





}
