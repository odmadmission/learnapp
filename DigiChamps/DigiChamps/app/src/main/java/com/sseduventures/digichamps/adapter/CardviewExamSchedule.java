package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.sseduventures.digichamps.Model.ExamScheduleList;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.ExamDetailActivity;
import com.sseduventures.digichamps.domain.ExamScheduleListNew;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class CardviewExamSchedule extends RecyclerView.Adapter<CardviewExamSchedule.DataObjectHolder> {

    Context context;
    private List<ExamScheduleListNew> list = new ArrayList<>();

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        RelativeLayout rel_detail;
        android.widget.RadioButton RadioButton;
        TextView tv_exam_type,tv_date_name,tv_date_lbl;

        public DataObjectHolder(View itemView) {
            super(itemView);
            tv_exam_type = (TextView) itemView.findViewById(R.id.tv_exam_type);
            tv_date_name = (TextView) itemView.findViewById(R.id.tv_date_name);
            tv_date_lbl = (TextView) itemView.findViewById(R.id.tv_date_lbl);
            rel_detail = (RelativeLayout) itemView.findViewById(R.id.rel_detail);
        }
    }

    public CardviewExamSchedule(List<ExamScheduleListNew> getlist, Context context) {
        this.list = getlist;
        this.context = context;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_exam_schedule, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {
        holder.tv_exam_type.setText(list.get(position).getExamName());
        holder.tv_date_name.setText(Utils.parseDateChange(list.get(position).getStartDate()));

        FontManage.setFontImpact(context,holder.tv_exam_type);
        FontManage.setFontMeiryoBold(context,holder.tv_date_name);
        FontManage.setFontImpact(context,holder.tv_date_lbl);

        holder.rel_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ExamDetailActivity.class);
                i.putExtra("examID",list.get(position).getExamTypeId());
                i.putExtra("examName",list.get(position).getExamName());
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}

