package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.domain.PsychometricPostData;
import com.sseduventures.digichamps.domain.PsychometricPostRequest;
import com.sseduventures.digichamps.domain.PsychometricQuestion;
import com.sseduventures.digichamps.domain.PsychometricQuestionOption;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;
import java.util.Date;

import jp.wasabeef.richeditor.RichEditor;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PsychometricExamActivity extends
        FormActivity implements ServiceReceiver.Receiver {


    private TextView tvClock;
    private RelativeLayout rlRoot;
    private Button btFinish;
    private TextView tvQuestionNo;
    private RichEditor tvQuestion;
    private ImageView imvQ1;
    private ImageView imvQ2;
    private ImageView imvQ3;
    private ImageView imvQ4;
    private CheckBox questionReview;
    private CardView cdOpta;
    private CardView cdOptb;
    private CardView cdOptc;
    private CardView cdOptd;
    private LinearLayout llOpta;
    private LinearLayout llOptb;
    private LinearLayout llOptc;
    private LinearLayout llOptd;
    private RichEditor reOpta;
    private RichEditor reOptb;
    private RichEditor reOptc;
    private RichEditor reOptd;
    private ImageView imvOpta;
    private ImageView imvOptb;
    private ImageView imvOptc;
    private ImageView imvOptd;
    private ImageView back_arrow;
    private LinearLayout llPrev;
    private LinearLayout llNext;
    private Button btQR;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private RelativeLayout shimmer_relative;
    private LinearLayout psycholiner;
    private ShimmerFrameLayout mShimmerViewContainer;

    private ArrayList<PsychometricQuestion> questions;

    private int selectedQuestionPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.psycho_exam_activity);
        setModuleName(LogEventUtil.KEY_MentorTest_PAGE);
        logEvent(LogEventUtil.KEY_MentorTest_PAGE,
                LogEventUtil.EVENT_MentorTest_PAGE);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        init();
    }

    private void init() {
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        tvClock = findViewById(R.id.tv_clock);
        btFinish = findViewById(R.id.end_test_text);
        rlRoot = findViewById(R.id.rlRoot);

        tvQuestionNo = findViewById(R.id.tv_question_num);
        tvQuestion = findViewById(R.id.tv_question);
        imvQ1 = findViewById(R.id.qes_img1);
        imvQ2 = findViewById(R.id.qes_img2);
        imvQ3 = findViewById(R.id.qes_img3);
        imvQ4 = findViewById(R.id.qes_img4);
        back_arrow = findViewById(R.id.back_arrow);

        questionReview = findViewById(R.id.checkbox_exam);
        llOpta = findViewById(R.id.llA);
        llOptb = findViewById(R.id.llB);
        llOptc = findViewById(R.id.llC);
        llOptd = findViewById(R.id.llD);

        cdOpta = findViewById(R.id.optionA);
        cdOptb = findViewById(R.id.optionB);
        cdOptc = findViewById(R.id.optionC);
        cdOptd = findViewById(R.id.optionD);

        reOpta = findViewById(R.id.option_a);
        reOptb = findViewById(R.id.option_b);
        reOptc = findViewById(R.id.option_c);
        reOptd = findViewById(R.id.option_d);

        reOpta.setEditorHeight(0);
        reOptb.setEditorHeight(0);
        reOptc.setEditorHeight(0);
        reOptd.setEditorHeight(0);

        imvOpta = findViewById(R.id.opt_img1);
        imvOptb = findViewById(R.id.opt_img2);
        imvOptc = findViewById(R.id.opt_img3);
        imvOptd = findViewById(R.id.opt_img4);

        llPrev = findViewById(R.id.lnr_prvs);
        llNext = findViewById(R.id.linearLayout);
        btQR = findViewById(R.id.bt_question);
        psycholiner=(LinearLayout)findViewById(R.id.psycholiner);
        shimmer_relative=(RelativeLayout)findViewById(R.id.shimmer_relative);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        questions = new ArrayList<>();
        cdOpta.setClickable(false);
        cdOptb.setClickable(false);
        cdOptc.setClickable(false);
        cdOptd.setClickable(false);


        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });


        llNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int previous = selectedQuestionPosition;

                if (selectedQuestionPosition < questions.size() - 1) {
                    ++selectedQuestionPosition;
                    if (selectedQuestionPosition == 0) {
                        llPrev.setVisibility(View.INVISIBLE);

                    } else {
                        llPrev.setVisibility(View.VISIBLE);

                    }

                    if (selectedQuestionPosition == questions.size() - 1) {
                        llNext.setVisibility(View.INVISIBLE);
                    }

                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    setSkipped(previous);
                    updateQuestionUI(selectedQuestionPosition);

                }

            }
        });
        llPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int previous = selectedQuestionPosition;
                if (selectedQuestionPosition >= 1) {
                    --selectedQuestionPosition;
                    if (selectedQuestionPosition == 0) {
                        llPrev.setVisibility(View.INVISIBLE);
                    } else {
                        llPrev.setVisibility(View.VISIBLE);
                    }

                    if (questions.size() > 1) {
                        llNext.setVisibility(View.VISIBLE);

                    } else {
                        llNext.setVisibility(View.INVISIBLE);
                    }


                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    setSkipped(previous);
                    updateQuestionUI(selectedQuestionPosition);

                }

            }
        });


        btFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkSkip();

            }
        });


        btQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                qrExam();
            }
        });


        tvQuestion.setHapticFeedbackEnabled(false);
        tvQuestion.setClickable(false);
        tvQuestion.setLongClickable(false);
        tvQuestion.setFocusableInTouchMode(false);


        reOpta.setHapticFeedbackEnabled(false);
        reOpta.setClickable(false);
        reOpta.setLongClickable(false);

        reOptb.setHapticFeedbackEnabled(false);
        reOptb.setClickable(false);
        reOptb.setLongClickable(false);


        reOptc.setHapticFeedbackEnabled(false);
        reOptc.setClickable(false);
        reOptc.setLongClickable(false);


        reOptd.setHapticFeedbackEnabled(false);
        reOptd.setClickable(false);
        reOptd.setLongClickable(false);

        reOpta.setInputEnabled(false);
        reOpta.setEnabled(true);
        reOpta.loadCSS("* {" +
                "   -webkit-user-select: none;" +
                "}");


        reOptb.setInputEnabled(false);
        reOptb.setEnabled(true);
        reOptb.loadCSS("* {" +
                "   -webkit-user-select: none;" +
                "}");

        reOptc.setInputEnabled(false);
        reOptc.setEnabled(true);
        reOptc.loadCSS("* {" +
                "   -webkit-user-select: none;" +
                "}");

        reOptd.setInputEnabled(false);
        reOptd.setEnabled(true);
        reOptd.loadCSS("* {" +
                "   -webkit-user-select: none;" +
                "}");


        tvQuestion.setHapticFeedbackEnabled(false);
        tvQuestion.setClickable(false);
        tvQuestion.setLongClickable(false);
        tvQuestion.setFocusableInTouchMode(false);

        tvQuestion.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                }


                return true;

            }
        });


        llOpta.setHapticFeedbackEnabled(false);
        llOpta.setClickable(false);
        llOpta.setLongClickable(false);

        llOpta.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //Toast.makeText(ExamActivity.this,"A",Toast.LENGTH_SHORT).show();
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    setAnswered(selectedQuestionPosition, 1);

                }


                return true;

            }
        });


        llOptb.setHapticFeedbackEnabled(false);
        llOptb.setClickable(false);
        llOptb.setLongClickable(false);
        llOptb.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    setAnswered(selectedQuestionPosition, 2);

                }


                return true;

            }
        });


        llOptc.setHapticFeedbackEnabled(false);
        llOptc.setClickable(false);
        llOptc.setLongClickable(false);
        llOptc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //Toast.makeText(ExamActivity.this,"C",Toast.LENGTH_SHORT).show();
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    setAnswered(selectedQuestionPosition, 3);
                }


                return true;

            }
        });


        llOptd.setHapticFeedbackEnabled(false);
        llOptd.setClickable(false);
        llOptd.setLongClickable(false);
        llOptd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Toast.makeText(ExamActivity.this,"D",Toast.LENGTH_SHORT).show();
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    setBackgroundDefault();

                    setAnswered(selectedQuestionPosition, 4
                    );

                }


                return true;

            }
        });


        cdOpta.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    setAnswered(selectedQuestionPosition, 1);

                }


                return true;

            }
        });


        llOptb.setHapticFeedbackEnabled(false);
        llOptb.setClickable(false);
        llOptb.setLongClickable(false);
        cdOptb.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    setAnswered(selectedQuestionPosition, 2);

                }


                return true;

            }
        });


        llOptc.setHapticFeedbackEnabled(false);
        llOptc.setClickable(false);
        llOptc.setLongClickable(false);
        cdOptc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    setAnswered(selectedQuestionPosition, 3);
                }


                return true;

            }
        });


        llOptd.setHapticFeedbackEnabled(false);
        llOptd.setClickable(false);
        llOptd.setLongClickable(false);
        cdOptd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    setBackgroundDefault();

                    setAnswered(selectedQuestionPosition, 4
                    );

                }


                return true;

            }
        });

        reOpta.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    setAnswered(selectedQuestionPosition, 1);

                }


                return true;

            }
        });


        reOptb.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    setAnswered(selectedQuestionPosition, 2);

                }


                return true;

            }
        });


        reOptc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    setAnswered(selectedQuestionPosition, 3);
                }


                return true;

            }
        });


        reOptd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    setBackgroundDefault();

                    setAnswered(selectedQuestionPosition, 4
                    );

                }


                return true;

            }
        });


    }


    private void setSkipped(int position) {
        if (questions.get(position).getAnsweredId() == 0)
            questions.get(position).setSkippedId(1);
    }

    private void setAnswered(int position, int option) {
        int answer = questions.get(selectedQuestionPosition).getOptionList()
                .get((option - 1)).getOptionId();
        if (questions.get(position).getSkippedId() == 1)
            questions.get(position).setSkippedId(0);

        questions.get(position).setAnsweredId(answer);
    }

    private void setBackgroundDefault() {
        cdOpta.setBackgroundColor(Color.TRANSPARENT);
        cdOptb.setBackgroundColor(Color.TRANSPARENT);
        cdOptc.setBackgroundColor(Color.TRANSPARENT);
        cdOptd.setBackgroundColor(Color.TRANSPARENT);
        reOpta.setBackgroundColor(Color.TRANSPARENT);
        reOptb.setBackgroundColor(Color.TRANSPARENT);
        reOptc.setBackgroundColor(Color.TRANSPARENT);
        reOptd.setBackgroundColor(Color.TRANSPARENT);
    }

    private void qrExam() {

    }

    private void finishExam() {
        submitExam();
    }


    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
        if (questions.size() == 0) {
            getExamData();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        hideDialog();
        mServiceReceiver.setReceiver(null);

    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode) {

            case ResponseCodes.NO_INTERNET:
                showToast("No internet");
                break;
            case ResponseCodes.UNEXPECTED_ERROR:
                break;
            case ResponseCodes.EXCEPTION:
                break;

            case ResponseCodes.GET_PSYCHO_EXAM_DATA_SUCCESS:

                llPrev.setVisibility(View.INVISIBLE);
                ArrayList<PsychometricQuestion> list
                        = resultData.getParcelableArrayList(IntentHelper.RESULT_DATA);
                if (list != null) {
                    mShimmerViewContainer.stopShimmer();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    psycholiner.setVisibility(View.VISIBLE);
                    shimmer_relative.setVisibility(View.GONE);
                    llNext.setVisibility(View.VISIBLE);

                    questions.clear();
                    questions.addAll(list);
                    for (int i = 0; i < questions.size(); i++) {
                        int value = i + 1;
                        questions.get(i).setSno(value);
                        questions.get(i).setSkippedId(1);
                        ArrayList<PsychometricQuestionOption> options = new ArrayList<>();
                        for (int j = 0; j < questions.get(i).getOptionList().size(); j++) {
                            String optionLabel = "";
                            switch (j) {
                                case 0:
                                    optionLabel = "A";
                                    break;
                                case 1:
                                    optionLabel = "B";
                                    break;
                                case 2:
                                    optionLabel = "C";
                                    break;
                                case 3:
                                    optionLabel = "D";
                                    break;

                            }
                            questions.get(i).getOptionList().get(j).setOptionLabel(optionLabel);

                        }

                    }
                    updateQuestionUI(0);
                }
                else {
                    psycholiner.setVisibility(View.GONE);
                    shimmer_relative.setVisibility(View.VISIBLE);
                }
                break;
            case ResponseCodes.GET_PSYCHO_EXAM_DATA_FAILURE:
                break;
            case ResponseCodes.POST_PSYCHO_EXAM_DATA_FAILURE:
                break;
            case ResponseCodes.POST_PSYCHO_EXAM_DATA_SUCCESS:
                PsychometricPostData r = ((PsychometricPostData) resultData.
                        getParcelable(IntentHelper.RESULT_DATA));
                Intent intent = new Intent(this, MentorshipExamResultActivity.class);
                intent.putExtra("academic",
                        ((PsychometricPostData) resultData.
                                getParcelable(IntentHelper.RESULT_DATA))
                                .getSuccess().getAcademic());
                intent.putExtra("doubt", ((PsychometricPostData) resultData.
                        getParcelable(IntentHelper.RESULT_DATA))
                        .getSuccess().getDoubts());

                intent.putExtra("mentorship", r.getSuccess().getMentorship());


                intent.putExtra("interest", r.getSuccess().getInterest());


                intent.putExtra("time", r.getSuccess().getTime());


                intent.putExtra("self", r.getSuccess().getSelfStudy());

                intent.putExtra("school", r.getSuccess().getSchool());

                intent.putExtra("personal", r.getSuccess().getPersonal());

                startActivity(intent);
                finish();

                break;
        }
    }

    private void updateQuestionUI(int position) {

        if (position < questions.size()) {
            selectedQuestionPosition = position;
            PsychometricQuestion q = questions.get(position);

            tvQuestion.setHtml(q.getSno() + ". " + q.getQuestion());

            reOpta.setHtml(q.getOptionList().get(0).getOptionLabel() + ". "
                    + q.getOptionList().get(0).getOption());
            reOptb.setHtml(q.getOptionList().get(1).getOptionLabel() + ". "
                    + q.getOptionList().get(1).getOption());
            reOptc.setHtml(q.getOptionList().get(2).getOptionLabel() + ". "
                    + q.getOptionList().get(2).getOption());
            int optionId = 0;
            for (int i = 0; i < q.getOptionList().size(); i++) {

                if (q.getOptionList().get(i).getOptionId() == q.getAnsweredId()) {
                    String label = q.getOptionList().get(i).getOptionLabel();
                    if (label.equalsIgnoreCase("A")) {
                        optionId = 1;
                    } else if (label.equalsIgnoreCase("B")) {
                        optionId = 2;
                    } else if (label.equalsIgnoreCase("C")) {
                        optionId = 3;
                    }


                }
            }
            switch (optionId) {
                case 0:
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    break;
                case 1:
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    break;
                case 2:
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    break;
                case 3:
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    setBackgroundDefault();
                    break;
                case 4:
                    llOpta.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llOptd.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    setBackgroundDefault();
                    break;
            }
        }

    }


    private void getExamData() {
        questions.clear();

        if (false) {

            for (int i = 0; i < 20; i++) {
                int value = i + 1;
                PsychometricQuestion question = new PsychometricQuestion();
                question.setSno(value);
                question.setQuestion("question : " + value);
                question.setQuestionDesc("question desc: " + value);

                ArrayList<PsychometricQuestionOption> options = new ArrayList<>();
                for (int j = 0; j < 4; j++) {
                    int optionValue = j + 1;
                    PsychometricQuestionOption option = new PsychometricQuestionOption();
                    option.setOptionId(optionValue);
                    option.setOption("option : " + value);
                    String optionLabel = "";
                    switch (j) {
                        case 0:
                            optionLabel = "A";
                            break;
                        case 1:
                            optionLabel = "B";
                            break;
                        case 2:
                            optionLabel = "C";
                            break;
                        case 3:
                            optionLabel = "D";
                            break;

                    }
                    option.setOptionLabel(optionLabel);
                    options.add(option);

                }
                question.setOptionList(options);
                questions.add(question);

            }

            updateQuestionUI(0);

        } else {
            if (AppUtil.isInternetConnected(this)) {
                NetworkService.startActionGetPsychoExamData(this, mServiceReceiver);
            } else {
                showToast("No internet");
            }
        }
    }


    public void submitExam() {


        if (AppUtil.isInternetConnected(this)) {

            Bundle bundle = new Bundle();

            PsychometricPostRequest request
                    = new PsychometricPostRequest();
            request.setExamId(1);

            String User_Id = String.valueOf(RegPrefManager.getInstance(this).getRegId());
            request.setRegId(Integer.parseInt(User_Id));

            request.setStartTime(new Date());
            request.setStopTime(new Date());
            request.setQuestions(questions);

            bundle.putParcelable("data", request);
            NetworkService.startActionPostPsychoExamData(this,
                    bundle,
                    mServiceReceiver);
        } else {
            showToast("No internet");
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(PsychometricExamActivity.this)
                .setIcon(R.drawable.alert_warning)
                .setTitle("Exit Test")
                .setMessage("Are You Sure You Want To Exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        finish();

                    }
                })
                .setNegativeButton("No", null).show();

    }

    int index = -1;

    private void checkSkip() {
        index = -1;
        for (int i = 0; i < questions.size(); i++) {
            if (questions.get(i).getSkippedId() == 1) {
                index = i;

            }

            if (index != -1)
                break;
        }

        if (index != -1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(PsychometricExamActivity.this);
            LayoutInflater layoutInflater = (LayoutInflater) PsychometricExamActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View viewa = layoutInflater.inflate(R.layout.dialog_school_zone, null);
            builder.setView(viewa);

            final AlertDialog dialog1 = builder.create();
            TextView text = (TextView) viewa.findViewById(R.id.txt_na);
            Button ok = (Button) viewa.findViewById(R.id.okay_school);
            ImageView img_dial = (ImageView) viewa.findViewById(R.id.img_dial);
            Typeface face = Typeface.createFromAsset(PsychometricExamActivity.this.getAssets(),
                    "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");
            text.setText("Sorry! We can't give you a proper analysis \nuntil you answer all the questions");
            text.setTypeface(face);
            ok.setTypeface(face);

            img_dial.setBackground(getResources().getDrawable(R.drawable.ic_sad_dial));
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog1.dismiss();
                    if (index == 0) {
                        llPrev.setVisibility(View.INVISIBLE);
                        llNext.setVisibility(View.VISIBLE);

                    } else {

                        llPrev.setVisibility(View.VISIBLE);

                        if (index == (questions.size() - 1)) {
                            llNext.setVisibility(View.INVISIBLE);
                            llPrev.setVisibility(View.VISIBLE);
                        } else {
                            llNext.setVisibility(View.VISIBLE);
                            llPrev.setVisibility(View.VISIBLE);
                        }

                    }


                    updateQuestionUI(index);

                }
            });
            dialog1.show();
            dialog1.setCanceledOnTouchOutside(false);
            Window window = dialog1.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            window.setAttributes(wlp);


        } else {
            new AlertDialog.Builder(PsychometricExamActivity.this)
                    .setIcon(R.drawable.alert_warning)
                    .setTitle("End Test")
                    .setMessage("Do you want to End Test?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finishExam();

                        }
                    })
                    .setNegativeButton("No", null).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }
}
