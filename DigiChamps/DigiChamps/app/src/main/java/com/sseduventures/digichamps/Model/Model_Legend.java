package com.sseduventures.digichamps.Model;

/**
 * Created by ntspl22 on 12/6/2016.
 */

public class Model_Legend {
    String name, price,status;
    int id_, image, version,qstn_number;


    public Model_Legend(String name,int qstn_number) {
        this.name = name;
        this.qstn_number = qstn_number;
    }
    public String getName() {
        return name;
    }
    public int getqQtn_number() {
        return qstn_number;
    }

    public String getPrice() {
        return price;
    }


    public int getImage() {
        return image;
    }
    public String getStatus() {
        return status;
    }

    public int getId() {
        return id_;
    }
}
