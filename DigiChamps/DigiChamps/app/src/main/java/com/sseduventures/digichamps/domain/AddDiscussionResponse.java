package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/13/2018.
 */

public class AddDiscussionResponse implements Parcelable {

    private String Message;
    private int DiscussionID;
    private int ResultCount;
    private boolean status;


    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getDiscussionID() {
        return DiscussionID;
    }

    public void setDiscussionID(int discussionID) {
        DiscussionID = discussionID;
    }

    public int getResultCount() {
        return ResultCount;
    }

    public void setResultCount(int resultCount) {
        ResultCount = resultCount;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Message);
        dest.writeInt(this.DiscussionID);
        dest.writeInt(this.ResultCount);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
    }

    public AddDiscussionResponse() {
    }

    protected AddDiscussionResponse(Parcel in) {
        this.Message = in.readString();
        this.DiscussionID = in.readInt();
        this.ResultCount = in.readInt();
        this.status = in.readByte() != 0;
    }

    public static final Creator<AddDiscussionResponse> CREATOR = new Creator<AddDiscussionResponse>() {
        @Override
        public AddDiscussionResponse createFromParcel(Parcel source) {
            return new AddDiscussionResponse(source);
        }

        @Override
        public AddDiscussionResponse[] newArray(int size) {
            return new AddDiscussionResponse[size];
        }
    };
}
