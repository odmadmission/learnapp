package com.sseduventures.digichamps.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sseduventures.digichamps.Model.TimeTableList;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.adapter.CardViewCommonAdapter;
import com.sseduventures.digichamps.adapter.CardviewExamDetail;
import com.sseduventures.digichamps.adapter.CardviewTimetable;
import com.sseduventures.digichamps.domain.DailyTTList;
import com.sseduventures.digichamps.domain.DailyTTResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RecyclerItemClickListener;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.sseduventures.digichamps.utils.Constants.TIMETABLELIST;

public class DailyTTActivity extends FormActivity implements
        ServiceReceiver.Receiver{

    Context mContext;
    CardviewTimetable mCardviewTimetable;
    RecyclerView recyclerview;
    RelativeLayout layout_day;
    TextView tv_days,tv_day;
    ArrayList<String> mDaysList = new ArrayList<>();
    ArrayList<DailyTTList> mList=new ArrayList<>();


    String schoolid,sectionid;
    int classid;

    String selectedDays = "",selectDayString="";

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;

    private DailyTTResponse success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_tt);
        setModuleName(LogEventUtil.EVENT_daily_tt);
        logEvent(LogEventUtil.KEY_daily_tt,LogEventUtil.EVENT_daily_tt);
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);


        schoolid = RegPrefManager.getInstance(this).getSchoolId();
        classid = (int)RegPrefManager.getInstance(this).getclassid();
        sectionid = RegPrefManager.getInstance(this).getSectionId();
        mContext = this;
        mList = new ArrayList<DailyTTList>();
        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_day =  findViewById(R.id.tv_day);
        tv_days =  findViewById(R.id.tv_days);

        TextView tv_subject =  findViewById(R.id.tv_subject);
        TextView tv_periods =  findViewById(R.id.tv_periods);
        TextView tv_time =  findViewById(R.id.tv_time);

        FontManage.setFontMeiryo(mContext,tv_day);
        FontManage.setFontMeiryo(mContext,tv_days);

        FontManage.setFontImpact(mContext,tv_subject);
        FontManage.setFontImpact(mContext,tv_periods);
        FontManage.setFontImpact(mContext,tv_time);

        layout_day =  findViewById(R.id.layout_day);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        TextView tv_at =  findViewById(R.id.tv_at);
        FontManage.setFontHeaderBold(mContext,tv_at);

        setDaysList();
        getDailyTTNew();
        tv_days.setText(selectDayString);


        layout_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMyDialog(mDaysList,tv_days);

            }
        });

    }

    public void showMyDialog(final ArrayList<String> mList, final TextView tv_view){
        // TODO Auto-generated method stub
        final Dialog dialogEdit;
        dialogEdit = new Dialog(mContext);
        dialogEdit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogEdit.setContentView(R.layout.common_dialog);
        final RecyclerView dialog_recyclerList = (RecyclerView) dialogEdit.findViewById(R.id.dialog_recyclerList);
        TextView txtCancel = (TextView) dialogEdit.findViewById(R.id.txtCancel);
        TextView txtTitle = (TextView) dialogEdit.findViewById(R.id.txtTitle);
        txtTitle.setText("Select Day");
        ImageView imgCancel = (ImageView) dialogEdit.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogEdit.dismiss();
            }
        });
        LinearLayoutManager mManager = new LinearLayoutManager(mContext);
        mManager.setOrientation(LinearLayoutManager.VERTICAL);
        dialog_recyclerList.setLayoutManager(mManager);
        CardViewCommonAdapter mCardViewCommonAdapter = new CardViewCommonAdapter(mList,mContext,tv_view);
        dialog_recyclerList.setAdapter(mCardViewCommonAdapter);
        dialogEdit.show();
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogEdit.dismiss();

            }
        });

        dialog_recyclerList.addOnItemTouchListener(
                new RecyclerItemClickListener(mContext, dialog_recyclerList ,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override public void onItemClick(View view, int position) {
                                tv_view.setTextColor(Color.BLACK);
                                tv_view.setText(mList.get(position));
                                tv_view.setError(null);
                                dialogEdit.dismiss();
                                getDaysNumberFromList(mList.get(position));
                                checkList();
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }


                        })
        );


    }




    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mContext)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    public void checkList(){
        ArrayList<DailyTTList> mSortedList = new ArrayList<>();
        if(mList != null && mList.size()>0){
            for(int i=0;i<mList.size();i++){
                if(selectedDays.equals(mList.get(i).getDay())){
                    mSortedList.add(mList.get(i));
                }
            }
            mCardviewTimetable = new CardviewTimetable(mSortedList,mContext);
            recyclerview.setAdapter(mCardviewTimetable);

            if(mSortedList != null && mSortedList.size()==0){
                AskOptionDialog("No Time Table Found For This Day").show();
            }
        }
    }


    public void setDaysList(){
        mDaysList.add("Monday"); // 1
        mDaysList.add("Tuesday"); // 2
        mDaysList.add("Wednesday"); // 3
        mDaysList.add("Thursday"); // 4
        mDaysList.add("Friday"); // 5
        mDaysList.add("Saturday"); // 6
        mDaysList.add("Sunday"); // 7
    }

    public void getDaysNumberFromList(String name){

        if(name.equals("Monday")){
            selectedDays =  "1";
        } else if(name.equals("Tuesday")){
            selectedDays =   "2";
        } else if(name.equals("Wednesday")){
            selectedDays =   "3";
        } else if(name.equals("Thusday")){
            selectedDays =   "4";
        } else if(name.equals("Friday")){
            selectedDays =   "5";
        } else if(name.equals("Saturday")){
            selectedDays =   "6";
        } else if(name.equals("Sunday")){
            selectedDays =   "7";
        }

    }

    private void getDailyTTNew() {
        if(AppUtil.isInternetConnected(this)){


            NetworkService.startActionPostDailyTTDetails(DailyTTActivity.this,mServiceReceiver);

        }else{
            Intent in = new Intent(DailyTTActivity.this,Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        //hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(DailyTTActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(DailyTTActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(DailyTTActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:

                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if(success!=null && success.getList()!=null
                        && success.getList().size()>0){

                    int count=success.getList().size()-1;
                    while (count>=0)
                    {
                        mList.add(success.getList().get(count));
                        count--;
                    }

                    checkList();
                }

                break;

        }
    }


}
