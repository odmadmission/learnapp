package com.sseduventures.digichamps.Model;

/**
 * Created by prabhanshu.sekhar@ntspl.co.in on 9/3/2015.
 */
public class DataModel_Activity {

    private String name;
    private boolean id;
    private int Id;

    public DataModel_Activity(String name) {
        this.name = name;

    }
    public DataModel_Activity(int id) {
        this.Id = id;

    }
    public int getId() {
        return Id;
    }
    public String getName() {
        return name;
    }

    public boolean isId() {
        return id;
    }
}
