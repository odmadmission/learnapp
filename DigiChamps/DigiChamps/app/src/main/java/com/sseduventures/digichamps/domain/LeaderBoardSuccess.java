package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/5/2018.
 */

public class LeaderBoardSuccess implements Parcelable {

    private List<LeaderBoardMyResultData> My_Result;
    private List<LeaderBoardBestTenData> Top10;

    public List<LeaderBoardMyResultData> getMy_Result() {
        return My_Result;
    }

    public void setMy_Result(List<LeaderBoardMyResultData> my_Result) {
        My_Result = my_Result;
    }

    public List<LeaderBoardBestTenData> getTop10() {
        return Top10;
    }

    public void setTop10(List<LeaderBoardBestTenData> top10) {
        Top10 = top10;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.My_Result);
        dest.writeTypedList(this.Top10);
    }

    public LeaderBoardSuccess() {
    }

    protected LeaderBoardSuccess(Parcel in) {
        this.My_Result = in.createTypedArrayList(LeaderBoardMyResultData.CREATOR);
        this.Top10 = in.createTypedArrayList(LeaderBoardBestTenData.CREATOR);
    }

    public static final Creator<LeaderBoardSuccess> CREATOR = new Creator<LeaderBoardSuccess>() {
        @Override
        public LeaderBoardSuccess createFromParcel(Parcel source) {
            return new LeaderBoardSuccess(source);
        }

        @Override
        public LeaderBoardSuccess[] newArray(int size) {
            return new LeaderBoardSuccess[size];
        }
    };
}
