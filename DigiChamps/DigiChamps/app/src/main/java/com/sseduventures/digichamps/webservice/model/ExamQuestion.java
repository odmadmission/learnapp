package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ExamQuestion implements Parcelable {
    public ExamQuestion() {

    }

    protected ExamQuestion(Parcel in) {
        Question_id = in.readString();
        Answers = in.createTypedArrayList(ExamAnswers.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Question_id);
        dest.writeTypedList(Answers);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ExamQuestion> CREATOR = new Creator<ExamQuestion>() {
        @Override
        public ExamQuestion createFromParcel(Parcel in) {
            return new ExamQuestion(in);
        }

        @Override
        public ExamQuestion[] newArray(int size) {
            return new ExamQuestion[size];
        }
    };

    public String getQuestion_id() {
        return Question_id;
    }

    public void setQuestion_id(String question_id) {
        Question_id = question_id;
    }

    public ArrayList<ExamAnswers> getAnswers() {
        return Answers;
    }

    public void setAnswers(ArrayList<ExamAnswers> answers) {
        Answers = answers;
    }

    private String Question_id;
    private ArrayList<ExamAnswers> Answers;

}
