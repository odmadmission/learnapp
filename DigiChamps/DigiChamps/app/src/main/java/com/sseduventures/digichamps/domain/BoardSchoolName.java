package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class BoardSchoolName implements Parcelable {
    private String schoolId;
    private String schoolName;
    private String schoolDescription;
    private String schoolLogo;
    private String schoolVideo;
    private String schoolThumbnail;

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolDescription() {
        return schoolDescription;
    }

    public void setSchoolDescription(String schoolDescription) {
        this.schoolDescription = schoolDescription;
    }

    public String getSchoolLogo() {
        return schoolLogo;
    }

    public void setSchoolLogo(String schoolLogo) {
        this.schoolLogo = schoolLogo;
    }

    public String getSchoolVideo() {
        return schoolVideo;
    }

    public void setSchoolVideo(String schoolVideo) {
        this.schoolVideo = schoolVideo;
    }

    public String getSchoolThumbnail() {
        return schoolThumbnail;
    }

    public void setSchoolThumbnail(String schoolThumbnail) {
        this.schoolThumbnail = schoolThumbnail;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    private String modifiedDate;
    private String active;
    private String creationDate;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.schoolId);
        parcel.writeString(this.schoolName);
        parcel.writeString(this.schoolDescription);
        parcel.writeString(this.schoolLogo);
        parcel.writeString(this.schoolVideo);
        parcel.writeString(this.schoolThumbnail);
        parcel.writeString(this.modifiedDate);
        parcel.writeString(this.active);
        parcel.writeString(this.creationDate);

    }
    public BoardSchoolName(){

    }
    public BoardSchoolName(Parcel in){
        this.schoolId=in.readString();
        this.schoolName=in.readString();
        this.schoolDescription=in.readString();
        this.schoolLogo=in.readString();
        this.schoolVideo=in.readString();
        this.schoolThumbnail=in.readString();
        this.modifiedDate=in.readString();
        this.active=in.readString();
        this.creationDate=in.readString();


    }


    public static final Creator<BoardSchoolName> CREATOR = new Creator<BoardSchoolName>() {
        @Override
        public BoardSchoolName createFromParcel(Parcel source) {
            return new BoardSchoolName(source);
        }

        @Override
        public BoardSchoolName[] newArray(int size) {
            return new BoardSchoolName[size];
        }
    };
}
