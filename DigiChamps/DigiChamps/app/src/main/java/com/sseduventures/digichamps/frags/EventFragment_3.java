package com.sseduventures.digichamps.frags;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.Model.AutocompleteModel;
import com.sseduventures.digichamps.Model.Basic_info_model;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.ProfileActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.Board_listview_DialogAdapter;
import com.sseduventures.digichamps.adapter.Class_listview_DialogAdapter;
import com.sseduventures.digichamps.adapter.CustomAutoCompleteAdapter;
import com.sseduventures.digichamps.domain.EditProfileResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.model.ProfileModelClass;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class EventFragment_3 extends Fragment implements ServiceReceiver.Receiver {
    private AutoCompleteTextView input_school;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    public ProgressDialog dialog;
    private EditProfileResponse newSuccess;
    private String[] arr = null;
    ArrayList<String> school_List;
    ArrayList<AutocompleteModel> school_details;
    private ArrayList<ProfileModelClass> success;
    CustomAutoCompleteAdapter adapter;
    private RelativeLayout new_school;
    private String selecteditem="null";
    boolean flag_other = false,flagBoard=false,flag_section=false;
    private TextInputLayout school_name;
    private String nameClass="null",schoolID="null";
    private String[] boardNames = null;
    ArrayList<Basic_info_model> board_list,class_list;
    ArrayList<String> class_details;
    private Class_listview_DialogAdapter classAdapter;
    private TextInputEditText input_name, input_phone, input_email,
            input_board,
            input_class,school_name_ed,input_section;
    ArrayList<Basic_info_model> section_List_new,class_detail_list;
    private Board_listview_DialogAdapter boardAdapter;
    private TextView select_class,select_tv;

    private String board = "null", class_name = "null", nameSection = "null",boardId="",classId="",classselect="null",sectionselect="null";

    public EventFragment_3() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ((FormActivity) getActivity()).logEvent(LogEventUtil.KEY_LearnAndEarnGifts_PAGE,
                LogEventUtil.EVENT_LearnAndEarnGifts_PAGE);
        ((FormActivity) getActivity()).setModuleName(LogEventUtil.EVENT_LearnAndEarnGifts_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.event_frag_registration, container, false);
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        school_List = new ArrayList<>();
        school_details = new ArrayList<>();
        board_list = new ArrayList<>();
        class_list=new ArrayList<>();
        class_details = new ArrayList<>();
        class_detail_list = new ArrayList<>();
        new_school=(RelativeLayout)v.findViewById(R.id.new_school);
        input_school = (AutoCompleteTextView)v. findViewById(R.id.input_school);
        school_name=(TextInputLayout)v.findViewById(R.id.school_name);
        input_class = (TextInputEditText) v.findViewById(R.id.input_class);
        input_board = (TextInputEditText)v. findViewById(R.id.input_board);
        select_class = (TextView)v.findViewById(R.id.select_class);
        select_tv = (TextView)v. findViewById(R.id.select_tv);
        getBoardClass();

        input_school.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {

                // if(!input_school.getText().toString().isEmpty())

                Object item = parent.getItemAtPosition(position);


                if (item instanceof String) {
                    selecteditem = (String) item;
                    flag_other = false;

                    school_name.setVisibility(View.GONE);
                    new_school.setVisibility(View.GONE);


                    schoolId(selecteditem);


                }




            }


        });

        select_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                boarddialog();
            }
        });




        select_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(board!=null){
                    if(!input_board.getText().toString().isEmpty()){
                        createClassList();
                    }
                    if (classNames != null) {




                        dialogclass_dialog();
                    }
                }
                else {
                    AskOptionDialogNew("Please select your board").show();
                    //Toast.makeText(this, "Please select your board", Toast.LENGTH_SHORT).show();
                }
            }
        });



        return v;
    }

    private void getBoardClass() {

        if (AppUtil.isInternetConnected(getActivity())) {

            Bundle bundle = new Bundle();
            showDialog(null, "Please wait...");
            NetworkService.startActionGetBoardClass(getActivity(), mServiceReceiver, bundle);

        } else {

            Intent in = new Intent(getActivity(), Internet_Activity.class);
            startActivity(in);

        }
    }

    @Override
    public void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    public void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(), ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(), Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(), ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelableArrayList(IntentHelper.RESULT_DATA);
                //showToast(success.size()+"");
                if (success != null && success.size() > 0) {
                    createBoardNames();

                    createSchoolNames();


                }
                break;
            case ResponseCodes.IMAGESUCCESS:
//                mSuccess = resultData.getParcelable(IntentHelper.RESULT_DATA);


                break;

            case ResponseCodes.PROFILESUBMITSUCCESS:
                hideDialog();
              /*  newSuccess = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if (newSuccess.getSuccess().getMessage().equalsIgnoreCase("Profile Successfully Updated")) {
                    showToast("Successfully Uploaded");
                    RegPrefManager.getInstance(this).setUserName(input_name.getText().toString().trim());
                    RegPrefManager.getInstance(this).setPhone(input_phone.getText().toString().trim());
                    RegPrefManager.getInstance(this).setEmail(input_email.getText().toString().trim());

                    RegPrefManager.getInstance(this).setBoardName(input_board.getText().toString().trim());
                    RegPrefManager.getInstance(this).setClassName(input_class.getText().toString().trim());
                    RegPrefManager.getInstance(this).setsectionName(input_section.getText().toString().trim());

                    RegPrefManager.getInstance(this).setSchoolName(selecteditem);

                    RegPrefManager.getInstance(this).setKeyBoardId(boardId);
                    RegPrefManager.getInstance(this).setKeyClassId(Long.valueOf(classId));

                    finish();
                }
*/

                break;
        }

   /* */
    }
    public void showDialog(String title,String message)
    {
        if(dialog==null){
            dialog= ProgressDialog.show(getActivity(),title,message);
        }
    }

    public void hideDialog()
    {
        if(dialog!=null)
        {
            dialog.dismiss();
            dialog=null;
        }
    }

    private void createSchoolNames() {

        for (int j = 0; j < success.size(); j++) {
            arr = new String[success.get(j).getSchoolInfoList().size()];
            for (int i = 0; i < success.get(j).getSchoolInfoList().size(); i++) {
                arr[i] = success.get(j).getSchoolInfoList().get(i).getSchoolName().toString();
                school_List.add(success.get(j).getSchoolInfoList().get(i).getSchoolName().toString());

                AutocompleteModel autocompleteModel = new AutocompleteModel();
                autocompleteModel.setSchoolName(success.get(j).getSchoolInfoList().get(i).getSchoolName().toString());
                autocompleteModel.setSchoolId(success.get(j).getSchoolInfoList().get(i).getSchoolId().toString());
                school_details.add(autocompleteModel);
            }
        }
        if (school_List.size() > 0) {
            school_List.add(school_List.size(), "Other");
        }


        schoolList();

    }
    public void schoolList() {
        String footerText = "<font color=\"red\">School name not in the list? </font>";



        adapter = new CustomAutoCompleteAdapter(
                getActivity(), android.R.layout.simple_list_item_1, arr, footerText);
        adapter.setOnFooterClickListener(new CustomAutoCompleteAdapter.OnFooterClickListener() {
            @Override
            public void onFooterClicked() {
                flag_other = true;


                input_school.setText("School name not in the list?");

                new_school.setVisibility(View.VISIBLE);
                school_name.setVisibility(View.VISIBLE);

                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(getContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(input_school.getWindowToken(), 0);
                input_school.setSelection(input_school.length());
                input_school.dismissDropDown();
                schoolID="null";
                selecteditem="null";
            }
        });
        input_school.setThreshold(2);
        input_school.setAdapter(adapter);

    }
    public void schoolId(String selecteditem){
        for(int i=0;i<school_details.size();i++){
            String name=school_details.get(i).getSchoolName();
            if(selecteditem.equalsIgnoreCase(name)){
                String id = school_details.get(i).getSchoolId();
                schoolID=id;
            }
        }
    }


    private void createBoardNames() {
        boardNames = new String[success.size()];
        for (int i = 0; i < success.size(); i++) {
            int size=success.get(i).getBoardList().size();
            for(int j=0;j<size;j++) {
                String names=success.get(i).getBoardList().get(j).getBoardName();
                int id=success.get(i).getBoardList().get(j).getBoardId();
                Basic_info_model basic_info_model = new Basic_info_model();
                basic_info_model.setName(names);
                basic_info_model.setBoardId(String.valueOf(id));
                board_list.add(basic_info_model);

            }

        }


    }


    // private String board;
    private String[] classNames = null;



    private void createClassList() {

        for (int k = 0; k < success.size(); k++) {
            for (int i = 0; i < success.get(k).getBoardList().size(); i++) {

                if (board != null && board.equalsIgnoreCase(success.get(k).getBoardList().get(i).getBoardName())) {
                    classNames = new String[success.get(k).getBoardList().get(i).getClassList().size()];
                    for (int j = 0; j < success.get(k).getBoardList().get(i).getClassList().size(); j++) {
                        classNames[j] = success.get(k).getBoardList().get(i).getClassList().get(j).getClassName();
                        int classid=success.get(k).getBoardList().get(i).getClassList().get(j).getClassId();
                        Basic_info_model basic_info_model = new Basic_info_model();
                        basic_info_model.setName(success.get(k).getBoardList().get(i).getClassList().get(j).getClassName());
                        basic_info_model.setClassId(String.valueOf(success.get(k).getBoardList().get(i).getClassList().get(j).getClassId()));
                        class_list.add(basic_info_model);

                        class_details.add(success.get(k).getBoardList().get(i).getClassList().get(j).getClassName());
                    }
                }
            }

        }
    }

    public void dialogclass_dialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.board_editprofile_dialog);


        Set<String> primesWithoutDuplicates = new LinkedHashSet<String>(class_details);
        class_details.clear();
        class_details.addAll(primesWithoutDuplicates);
        class_list.clear();
        for (int i = 0; i < class_details.size(); i++) {


            Basic_info_model basic_info_model = new Basic_info_model();
            String name = class_details.get(i).toString();
            if (class_name.equals("null")) {

                basic_info_model.setName(name);
            } else {
                if (class_name.equals(name)) {
                    basic_info_model.setFlag(true);
                    basic_info_model.setName(name);
                } else {
                    basic_info_model.setFlag(false);
                    basic_info_model.setName(name);
                }
            }



            class_list.add(basic_info_model);

        }
        ListView listView = (ListView) dialog.findViewById(R.id.dialoglist);


        TextView select_class_tv = (TextView) dialog.findViewById(R.id.select_class_tv);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        select_class_tv.setVisibility(View.VISIBLE);
        select_tv.setVisibility(View.GONE);

        classAdapter = new Class_listview_DialogAdapter(class_list, getContext());

        class_detail_list.addAll(class_list);

        listView.setAdapter(classAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                class_list.get(position).setFlag(true);
                class_name = class_list.get(position).getName();
                for (int i = 0; i < class_list.size(); i++) {
                    if (position != i)
                        class_list.get(i).setFlag(false);
                }
                classAdapter.notifyDataSetChanged();

            }
        });

        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Basic_info_model> list = classAdapter.basic_info_modelArrayList();
                for (int i = 0; i < list.size(); i++) {
                    boolean flag = list.get(i).isFlag();
                    if (flag == true) {
                        Basic_info_model basic_info_model = list.get(i);
                        nameClass = basic_info_model.getName();
                        classId=basic_info_model.getClassId();
                        //counter=counter+1;
                        prepareClassName(nameClass);

                        classselect=nameClass;
                        if(!input_school.getText().toString().isEmpty()) {
                            String value=input_class.getText().toString();
                            /*if (value.equalsIgnoreCase("Class - IX")) {

                            } else {
                                RegPrefManager.getInstance(getActivity()).setschoolCode(null);
                            }*/
                        }



                    }
                }
                //if (section_List_new == null ) {

               // createSectionNames();
                //  AskOptionDialogNew("Please select class first").show();


                //}


                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void boarddialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.board_editprofile_dialog);

        // Set dialog title
        // dialog.setTitle("Custom Dialog");

        // set values for custom dialog components - text, image and button


        ListView listView = (ListView) dialog.findViewById(R.id.dialoglist);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);
        TextView select_class_tv = (TextView) dialog.findViewById(R.id.select_class_tv);
        select_class_tv.setVisibility(View.GONE);
        select_tv.setVisibility(View.VISIBLE);
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        boardAdapter = new Board_listview_DialogAdapter(board_list, getContext());

        listView.setAdapter(boardAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                board_list.get(position).setFlag(true);
                for (int i = 0; i < board_list.size(); i++) {
                    if (position != i)
                        board_list.get(i).setFlag(false);
                }
                boardAdapter.notifyDataSetChanged();
            }
        });
        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Basic_info_model> list = boardAdapter.basic_info_modelArrayList();
                for (int i = 0; i < list.size(); i++) {
                    boolean flag = list.get(i).isFlag();
                    if (flag == true) {
                        Basic_info_model basic_info_model = list.get(i);
                        board = basic_info_model.getName();
                        boardId=basic_info_model.getBoardId();
                        nameClass ="null";
                        createClassList();
                        prepareBoardName(board);
                        flagBoard=true;
                        if(!input_school.getText().toString().isEmpty()) {
                            String value=input_board.getText().toString();
                           /* if (value.equalsIgnoreCase("CBSE")) {

                            } else {
                                RegPrefManager.getInstance(getActivity()).setschoolCode(null);
                            }*/
                        }
                        //counter=counter+1;


                    }
                }


                dialog.dismiss();

            }
        });

        dialog.show();
    }

    private void prepareClassName(String className) {
        input_class.setText(className);
    }




    private void prepareBoardName(String boardname) {
        input_board.setText(boardname);
    }

    private android.app.AlertDialog AskOptionDialogNew(String msg) {
        android.app.AlertDialog myQuittingDialogBox = new android.app.AlertDialog.Builder(getActivity())
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })

                .create();
        return myQuittingDialogBox;
    }

}
