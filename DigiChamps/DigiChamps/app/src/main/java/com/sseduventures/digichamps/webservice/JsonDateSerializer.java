package com.sseduventures.digichamps.webservice;


import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.sseduventures.digichamps.utils.DateTimeUtil;


import java.lang.reflect.Type;
import java.util.Date;

public class JsonDateSerializer
    implements JsonSerializer<Date> {
    @Override
    public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
        return src == null ? null : new JsonPrimitive(DateTimeUtil
                .get(src.getTime()));
    }


}