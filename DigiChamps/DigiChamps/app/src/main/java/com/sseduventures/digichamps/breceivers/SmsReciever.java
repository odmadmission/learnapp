package com.sseduventures.digichamps.breceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.utils.IntentHelper;


/**
 * Created by Raju Satheesh on 9/22/2016.
 */
public class SmsReciever extends BroadcastReceiver{


    private static final String TAG="SmsReciever";

    public void onReceive(Context context, Intent intent) {


        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();
        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();
                    if(message!=null) {
                        String[] messageParts = message.split(":");
                        int smsTestCode= RegPrefManager.getInstance(context).getSmsTestSavedCode();
                        if (messageParts[1] != null&&  smsTestCode!=0) {
                            if(Integer.parseInt(messageParts[1].trim().toString())==smsTestCode) {

                                RegPrefManager.getInstance(context).
                                        setSmsTestRecievedCode(Integer.parseInt(messageParts[1].trim()));
                                LocalBroadcastManager.getInstance(context).
                                        sendBroadcast(new Intent(IntentHelper.
                                        ACTION_SMS_RECIEVED));

                            }
                        }
                    }




                } // end for loop
            } // bundle is null

        } catch (Exception e) {

        }
    }
}