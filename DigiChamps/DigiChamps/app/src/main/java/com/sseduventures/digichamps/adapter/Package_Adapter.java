package com.sseduventures.digichamps.adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.PackageActivityNew;
import com.sseduventures.digichamps.domain.PackageModel;
import com.sseduventures.digichamps.utils.IntentHelper;

import java.util.ArrayList;



public class Package_Adapter extends
        RecyclerView.Adapter<Package_Adapter.MyViewHolder> {

    public ArrayList<PackageModel> dataSet = new ArrayList<>();
    private Activity mContext;
    private int discount=0;
    private String TAG=Package_Adapter.class.getSimpleName();


    public Package_Adapter(ArrayList<PackageModel> dataSet, Activity context,int discount) {
        this.mContext = context;
        this.discount=discount;
        if(this.dataSet!=null) {

            this.dataSet.clear();
            this.dataSet.addAll(dataSet);
        }
        else
        this.dataSet = dataSet;

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_bottom_package, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView tv_val = holder.tv_val;
        TextView tv_discountPrice = holder.tv_discountPrice;
        TextView tv_Price = holder.tv_Price;
        TextView tv_discountPercent = holder.tv_discountPercent;
        holder.linear_pack.setTag(dataSet.get(listPosition));
        TextView text8 = holder.text8;


        tv_Price.setPaintFlags(tv_Price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if(discount==1){


            int price= dataSet.get(listPosition).getPrice();
            double newPrice = 0.65*price;





            tv_Price.setText(Html.fromHtml("<strike>\u20B9"+
                    dataSet.get(listPosition).getPrice()+"</strike>"));


                tv_discountPercent.setText("35"+"% discount");
            tv_discountPrice.setText("\u20B9"+(int)newPrice);


        }else{

            tv_Price.setText(Html.fromHtml("<strike>\u20B9"+dataSet.get(listPosition).getPrice()+"</strike>"));


                tv_discountPercent.setText(dataSet.get(listPosition).getDisc_Percent()+"% discount");
            tv_discountPrice.setText("\u20B9"+dataSet.get(listPosition).getDiscPrice());


        }


        if(dataSet.get(listPosition).getSubscripttion_Period() == 180){

            tv_val.setText("6 months");

        }else if(dataSet.get(listPosition).getSubscripttion_Period() == 365){
            tv_val.setText("1 year");
        }else if(dataSet.get(listPosition).getSubscripttion_Period() == 90){
            tv_val.setText("3 months");
        }else if(dataSet.get(listPosition).getSubscripttion_Period() == 30){
            tv_val.setText("1 month");
        }
        else{
            tv_val.setText(dataSet.get(listPosition).getSubscripttion_Period()+" days");
        }


       if(dataSet.get(listPosition).getInCart()>0)
       {
           holder.text8.setVisibility(View.VISIBLE);
       }else{
           holder.text8.setVisibility(View.GONE);
           holder.linear_pack.setOnClickListener(onClickListener());

       }


    }

    private View.OnClickListener onClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(mContext!=null
                        &&mContext instanceof PackageActivityNew)
                {
                    Bundle bundle=new Bundle();
                    bundle.putParcelable(IntentHelper.EXTRA_DATA1,
                            (PackageModel)v.getTag());
                    ((PackageActivityNew) mContext).addToCart(bundle);
                }
                else
                {
                    Log.v(TAG,"Context null");
                }

            }
        };
    }



    @Override
    public int getItemCount() {
        if(dataSet!=null)
        return dataSet.size();
        else
            return 0;
    }



    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView feature, val, sub, type, details;
        public RelativeLayout linear_pack;
        CardView container;
        TextView tv_val, tv_discountPrice, tv_Price,tv_discountPercent,text8;



        public MyViewHolder(View itemView) {
            super(itemView);
            linear_pack = (RelativeLayout) itemView.findViewById(R.id.linear1);
            container = (CardView) itemView.findViewById(R.id.packag_card);
            tv_val = (TextView) itemView.findViewById(R.id.text3);
            tv_discountPrice = (TextView) itemView.findViewById(R.id.text6);
            tv_Price = (TextView) itemView.findViewById(R.id.text5);
            tv_discountPercent = (TextView) itemView.findViewById(R.id.text7);
            text8 = (TextView) itemView.findViewById(R.id.text8);



        }

    }


}

