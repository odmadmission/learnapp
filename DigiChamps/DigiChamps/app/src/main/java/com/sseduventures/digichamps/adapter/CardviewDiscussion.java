package com.sseduventures.digichamps.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.sseduventures.digichamps.Model.DiscussionForumList;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.DiscussionDetailActivity;
import com.sseduventures.digichamps.domain.DiscussionList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;




public class CardviewDiscussion extends RecyclerView.Adapter<CardviewDiscussion.DataObjectHolder> implements Filterable {
    private String LOG_TAG = "MyRecyclerViewAdapter";
    Context context;
    private List<DiscussionList> list = new ArrayList<>();
    private List<DiscussionList> filterList;

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    list = filterList;
                } else {

                    ArrayList<DiscussionList> mfilteredList = new ArrayList<>();

                    for (DiscussionList discussionList : filterList) {

                        if (discussionList.getTitle().toLowerCase().contains(charString) || discussionList.getCreatedDate().toLowerCase().contains(charString)) {

                            mfilteredList.add(discussionList);
                        }
                    }

                    list = mfilteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = list;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                list = (ArrayList<DiscussionList>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        RelativeLayout rel_master;
        android.widget.RadioButton RadioButton;
        TextView tv_question, dateTv;

        public DataObjectHolder(View itemView) {
            super(itemView);
            tv_question = (TextView) itemView.findViewById(R.id.tv_question);
            dateTv = (TextView) itemView.findViewById(R.id.dateTv);
            rel_master = (RelativeLayout) itemView.findViewById(R.id.rel_master);
        }
    }

    public CardviewDiscussion(Context context) {
        this.context = context;
        filterList = list;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_discussion, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {
        holder.tv_question.setText(list.get(position).getTitle());
        String dateValue = "";
        if (list.get(position).getCreatedDate() != null) {
            dateValue = list.get(position).getCreatedDate();
        }else {
            dateValue= new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        }
        if (dateValue.contains("T")) {
            dateValue = dateValue.split("T")[0];
        }
        holder.dateTv.setText(dateValue);

        holder.rel_master.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DiscussionDetailActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("discussionID", list.get(position).getDiscussionID() + "");
                context.startActivity(i);
                Activity activity = (Activity) context;
                activity.overridePendingTransition(R.anim.from_middle, R.anim.to_middle);

            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setNotifyData(List<DiscussionList> mList) {
        list.addAll(list.size(), mList);
        notifyDataSetChanged();
    }

    public void setNewDiscussuion(DiscussionList mDiscussionForumList) {
        list.add(0, mDiscussionForumList);
        notifyDataSetChanged();
    }


}

