package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/3/2018.
 */

public class PrvYrPaperSuccess implements Parcelable {

    private List<PrvYrPaperListData> list;

    public List<PrvYrPaperListData> getList() {
        return list;
    }

    public void setList(List<PrvYrPaperListData> list) {
        this.list = list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.list);
    }

    public PrvYrPaperSuccess() {
    }

    protected PrvYrPaperSuccess(Parcel in) {
        this.list = in.createTypedArrayList(PrvYrPaperListData.CREATOR);
    }

    public static final Creator<PrvYrPaperSuccess> CREATOR = new Creator<PrvYrPaperSuccess>() {
        @Override
        public PrvYrPaperSuccess createFromParcel(Parcel source) {
            return new PrvYrPaperSuccess(source);
        }

        @Override
        public PrvYrPaperSuccess[] newArray(int size) {
            return new PrvYrPaperSuccess[size];
        }
    };
}
