package com.sseduventures.digichamps.Model;

/**
 * Created by RKB on 5/9/2017.
 */

public class DataModel_TicketAnswers {

    private String teacher_Name, answer, date, time;
    private String teacher_Image,answer_photo;


    public DataModel_TicketAnswers() {
    }

    public DataModel_TicketAnswers(String teacher_Name, String date, String time, String teacher_Image, String answer, String answer_photo) {
        this.teacher_Name = teacher_Name;
        this.answer = answer;
        this.date = date;
        this.time= time;
        this.teacher_Image = teacher_Image;
        this.answer_photo = answer_photo;
    }

    public String getTeacherName() {
        return teacher_Name;
    }

    public void setTeacher_Name(String ticketNo) {
        this.teacher_Name = teacher_Name;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String date) {
        this.time = time;
    }

    public String getTeacher_Image() {
        return teacher_Image;
    }

    public String setTeacher_Image(String teacher_Image) {
        this.teacher_Image = teacher_Image;
        return teacher_Image;
    }
    public String getAnswer_Photo() {
        return answer_photo;
    }

    public String setAnswer_photo(String answer_photo) {
        this.answer_photo = answer_photo;
        return answer_photo;
    }

}
