package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class StudyMaterialsRequest implements Parcelable{

    private String SchoolId;
    private int ClassId;
    private String SectionId;

    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public int getClassId() {
        return ClassId;
    }

    public void setClassId(int classId) {
        ClassId = classId;
    }

    public String getSectionId() {
        return SectionId;
    }

    public void setSectionId(String sectionId) {
        SectionId = sectionId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.SchoolId);
        dest.writeInt(this.ClassId);
        dest.writeString(this.SectionId);
    }

    public StudyMaterialsRequest() {
    }

    protected StudyMaterialsRequest(Parcel in) {
        this.SchoolId = in.readString();
        this.ClassId = in.readInt();
        this.SectionId = in.readString();
    }

    public static final Creator<StudyMaterialsRequest> CREATOR = new Creator<StudyMaterialsRequest>() {
        @Override
        public StudyMaterialsRequest createFromParcel(Parcel source) {
            return new StudyMaterialsRequest(source);
        }

        @Override
        public StudyMaterialsRequest[] newArray(int size) {
            return new StudyMaterialsRequest[size];
        }
    };
}
