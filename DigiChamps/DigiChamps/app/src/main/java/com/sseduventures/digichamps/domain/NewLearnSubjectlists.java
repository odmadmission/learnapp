package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/20/2018.
 */

public class NewLearnSubjectlists implements Parcelable {

    private int total_pdfs;
    private int subjectid;
    private String subject;
    private int total_chapters;
    private int total_videos;
    private int Total_Pre_req_test;
    private int Total_question_pdf;
    private int Total_question;

    public int getTotal_pdfs() {
        return total_pdfs;
    }

    public void setTotal_pdfs(int total_pdfs) {
        this.total_pdfs = total_pdfs;
    }

    public int getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(int subjectid) {
        this.subjectid = subjectid;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getTotal_chapters() {
        return total_chapters;
    }

    public void setTotal_chapters(int total_chapters) {
        this.total_chapters = total_chapters;
    }

    public int getTotal_videos() {
        return total_videos;
    }

    public void setTotal_videos(int total_videos) {
        this.total_videos = total_videos;
    }

    public int getTotal_Pre_req_test() {
        return Total_Pre_req_test;
    }

    public void setTotal_Pre_req_test(int total_Pre_req_test) {
        Total_Pre_req_test = total_Pre_req_test;
    }

    public int getTotal_question_pdf() {
        return Total_question_pdf;
    }

    public void setTotal_question_pdf(int total_question_pdf) {
        Total_question_pdf = total_question_pdf;
    }

    public int getTotal_question() {
        return Total_question;
    }

    public void setTotal_question(int total_question) {
        Total_question = total_question;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.total_pdfs);
        dest.writeInt(this.subjectid);
        dest.writeString(this.subject);
        dest.writeInt(this.total_chapters);
        dest.writeInt(this.total_videos);
        dest.writeInt(this.Total_Pre_req_test);
        dest.writeInt(this.Total_question_pdf);
        dest.writeInt(this.Total_question);
    }

    public NewLearnSubjectlists() {
    }

    protected NewLearnSubjectlists(Parcel in) {
        this.total_pdfs = in.readInt();
        this.subjectid = in.readInt();
        this.subject = in.readString();
        this.total_chapters = in.readInt();
        this.total_videos = in.readInt();
        this.Total_Pre_req_test = in.readInt();
        this.Total_question_pdf = in.readInt();
        this.Total_question = in.readInt();
    }

    public static final Creator<NewLearnSubjectlists> CREATOR = new Creator<NewLearnSubjectlists>() {
        @Override
        public NewLearnSubjectlists createFromParcel(Parcel source) {
            return new NewLearnSubjectlists(source);
        }

        @Override
        public NewLearnSubjectlists[] newArray(int size) {
            return new NewLearnSubjectlists[size];
        }
    };
}
