package com.sseduventures.digichamps.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatConfig;
import com.freshchat.consumer.sdk.FreshchatUser;
import com.freshchat.consumer.sdk.activity.ChannelListActivity;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.Order;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.webservice.ResponseCodes;

public class SettingsActivity extends FormActivity implements ServiceReceiver.Receiver,ActivityCompat.OnRequestPermissionsResultCallback {


    private android.support.v7.widget.Toolbar toolbar;
    TextView txt_orders, txt_chat, txt_pass, txt_rate, txt_contact,
            txt_youtube, txt_insta, txt_fb;
    private Button btn_logout;

    private ServiceReceiver mServiceReceiver;
    private Handler mHandler;
    private static final String APP_ID = "515d67d7-5e28-40ce-a6b0-7783baf1ebbb";
    private static final String APP_KEY = "754a44c6-6202-44e9-93cb-f5f58b7a73a4";
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        logEvent(LogEventUtil.KEY_Settings_PAGE,
                LogEventUtil.EVENT_Settings_PAGE);
        setModuleName(LogEventUtil.EVENT_Settings_PAGE);

        FreshchatConfig freshchatConfig = new
                FreshchatConfig(APP_ID, APP_KEY);
        Freshchat.getInstance(getApplicationContext()).init(freshchatConfig);


        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_setting);
        txt_orders = (TextView) findViewById(R.id.txt_orders);
        txt_chat = (TextView) findViewById(R.id.txt_chat);
        txt_pass = (TextView) findViewById(R.id.txt_pass);
        txt_rate = (TextView) findViewById(R.id.txt_rate);
        txt_contact = (TextView) findViewById(R.id.txt_contact);
        txt_youtube = (TextView) findViewById(R.id.txt_youtube);
        txt_insta = (TextView) findViewById(R.id.txt_insta);
        txt_fb = (TextView) findViewById(R.id.txt_fb);
        btn_logout = (Button) findViewById(R.id.btn_logout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(SettingsActivity.this, NewDashboardActivity.class);
                startActivity(in);
                finish();
            }
        });

        getSupportActionBar().setTitle("Settings");
        toolbar.
                setTitleTextColor(getResources().getColor(R.color.white));

        txt_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SettingsActivity.this,
                        Order.class).putExtra("activity",1));
            }
        });

        txt_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatWebview();
            }
        });

        txt_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SettingsActivity.this,
                        PasswordChangeActivity.class)
                        .putExtra("phNum", "9776201945"));
            }
        });

        txt_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://play.google.com/store/apps/details?id=com.sseduventures.digichamps";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        txt_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermission(SettingsActivity.this);
            }
        });


        txt_youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://www.youtube.com/channel/UCzAIAxzYmwubv1CcPJej8pw";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        txt_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://www.facebook.com/thedigichamps/";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        txt_insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://www.instagram.com/the_digichamps/";

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });


        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             showLogoutDialog();
            }
        });
    }

    private void showLogoutDialog()
    {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage("Do you want to logout?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Logout",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        logout();
                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    public void chatWebview() {
        logEvent(LogEventUtil.KEY_ChatWithUs_PAGE,
                LogEventUtil.EVENT_ChatWithUs_PAGE);
        FreshchatUser freshUser = Freshchat.getInstance(getApplicationContext()).getUser();



        freshUser.setFirstName(RegPrefManager.getInstance(this).getUserName());
        freshUser.setPhone("+91", RegPrefManager.getInstance(this).getPhone());
        freshUser.setEmail(RegPrefManager.getInstance(this).getEmail());

        //Call setUser so that the user information is synced with Freshchat's servers
        Freshchat.getInstance(getApplicationContext()).setUser(freshUser);
        Intent intent = new Intent(this, ChannelListActivity.class);


        intent.putExtra("myParam", "Digichamps");
        startActivity(intent);

    }


    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
    }

    private void logout() {
        if (AppUtil.isInternetConnected(this)) {
            showDialog(null, "please wait while logout");
            NetworkService.startActionLogout(this, mServiceReceiver);
        } else {
            showToast("No Internet connection");
        }

    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode) {
            case 200:
                startActivity(new Intent(this, OnBoardActivity.class));
                finishAffinity();
                break;
            case ResponseCodes.ERROR:
                showToast("ERROR");
                break;
            case ResponseCodes.EXCEPTION:
                showToast("EXCEPTION");
                break;
            case ResponseCodes.FAILURE:
                showToast("FAILURE");
                break;
        }
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public  boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.
                    CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)
                        context, Manifest.permission.CALL_PHONE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Phone Calls permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                callSetting();
                return true;
            }
        } else {
            callSetting();
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){

            //resume tasks needing this permission
            callSetting();
        }


    }

    @SuppressLint("MissingPermission")
    public void callSetting(){
      /*  Intent sIntent = new Intent(Intent.ACTION_CALL, Uri


                .parse("tel:1800 212 4322"));


        sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        if (ActivityCompat.
                checkSelfPermission(SettingsActivity.this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(SettingsActivity.this,
                    new String[]{Manifest.permission.CALL_PHONE}, 100);
            return;
        }

        startActivity(sIntent);*/
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "1800 212 4322", null));
        startActivity(intent);
    }

}
