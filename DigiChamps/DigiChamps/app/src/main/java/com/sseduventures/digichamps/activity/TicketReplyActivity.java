package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewNotificationActivity;
import com.sseduventures.digichamps.domain.DoubtReplyResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.PickerBuilder;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SessionManager;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.BitmapUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.utils.Profile_utility;
import com.sseduventures.digichamps.utils.Utility;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;

import jp.wasabeef.richeditor.RichEditor;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by RKB on 5/10/2017.
 */

public class TicketReplyActivity extends FormActivity
        implements ServiceReceiver.Receiver {

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    private static final String TAG = "Profile Activity";
    private Toolbar toolbar;
    private Button reply_SubmitBtn;
    Uri file;
    SpotsDialog dialog;
    private ImageView back_arrow;
    private TextView err_text;
    private String userChoosenTask, text_editor;
    private ImageView showImg;
    private ImageButton notification_btn, cart_button, camera_icon;
    private File outPutFile = null;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    private String resp, error, reg_uid;
    private String TAG_profile_edit = "Profile_Edit";
    private SessionManager session;
    String ticketid, ansid, filepath;
    Spanned spanned_text;
    String new_text;

    private RichEditor mEditor;
    File photofile;
    File compressedImageFile = null;
    int height;
    int width;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;

    private DoubtReplyResponse success;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_ticketreply);
        setModuleName(LogEventUtil.EVENT_Doubt_reply);

        logEvent(LogEventUtil.KEY_Doubt_reply,LogEventUtil.EVENT_Doubt_reply);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        Intent intent = getIntent();
        ticketid = intent.getStringExtra("ticket_id");
        ansid = intent.getStringExtra("answer_id");
        init();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        TicketReplyActivity.this.getWindowManager()
                .getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        reg_uid = RegPrefManager.getInstance(this).getRegId() + "";


        dialog = new SpotsDialog(this, "FUN +EDUCATION", R.style.Custom);
        session = new SessionManager(this);
        try {
            Bundle extras = getIntent().getExtras();
            Uri uri = (Uri) extras.getParcelable("imagebitmap");
            Bitmap bitmap = Utility.handleSamplingAndRotationBitmap(TicketReplyActivity.this, uri);
            showImg.setImageBitmap(bitmap);

        } catch (Exception e) {

        }

        reply_SubmitBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (validation()) {
                    if (AppUtil.isInternetConnected(TicketReplyActivity.this)) {
                        replyDoubt(ticketid, ansid, text_editor, reg_uid);
                    } else {
                        Intent intent = new Intent(TicketReplyActivity.this, Internet_Activity.class);
                        startActivity(intent);

                        finish();
                    }
                }

            }
        });

        showImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();

            }
        });

        camera_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();

            }
        });

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReturnHome(v);
            }
        });

        notification_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (AppUtil.isInternetConnected(TicketReplyActivity.this)) {

                    startActivity(new Intent(TicketReplyActivity.this,
                            NewNotificationActivity.class));


                } else {
                    startActivity(new Intent(TicketReplyActivity.this, Internet_Activity.class));

                    finish();

                }
            }

        });

        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.menu_icon);
        toolbar.setOverflowIcon(drawable);


        mEditor = (RichEditor) findViewById(R.id.editor);
        mEditor.setEditorHeight(200);
        mEditor.setEditorFontSize(22);
        mEditor.setPadding(10, 10, 10, 10);
        mEditor.setPlaceholder("Insert text here...");
        mEditor.setOnTextChangeListener(new RichEditor.OnTextChangeListener() {
            @Override
            public void onTextChange(String text) {
                text_editor = text;
                Log.d("textEditor", text);

            }
        });

        findViewById(R.id.action_undo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.undo();
            }
        });

        findViewById(R.id.action_redo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.redo();
            }
        });

        findViewById(R.id.action_bold).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setBold();
            }
        });

        findViewById(R.id.action_italic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setItalic();
            }
        });

        findViewById(R.id.action_subscript).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setSubscript();
            }
        });

        findViewById(R.id.action_superscript).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setSuperscript();
            }
        });

        findViewById(R.id.action_strikethrough).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setStrikeThrough();
            }
        });

        findViewById(R.id.action_underline).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setUnderline();
            }
        });

        findViewById(R.id.action_heading1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(1);
            }
        });

        findViewById(R.id.action_heading2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(2);
            }
        });

        findViewById(R.id.action_heading3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(3);
            }
        });

        findViewById(R.id.action_heading4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(4);
            }
        });

        findViewById(R.id.action_heading5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(5);
            }
        });

        findViewById(R.id.action_heading6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(6);
            }
        });

        findViewById(R.id.action_txt_color).setOnClickListener(new View.OnClickListener() {
            private boolean isChanged;

            @Override
            public void onClick(View v) {
                mEditor.setTextColor(isChanged ? Color.BLACK : Color.RED);
                isChanged = !isChanged;
            }
        });

        findViewById(R.id.action_bg_color).setOnClickListener(new View.OnClickListener() {
            private boolean isChanged;

            @Override
            public void onClick(View v) {
                mEditor.setTextBackgroundColor(isChanged ? Color.TRANSPARENT : Color.YELLOW);
                isChanged = !isChanged;
            }
        });

        findViewById(R.id.action_indent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setIndent();
            }
        });

        findViewById(R.id.action_outdent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setOutdent();
            }
        });

        findViewById(R.id.action_align_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setAlignLeft();
            }
        });

        findViewById(R.id.action_align_center).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setAlignCenter();
            }
        });

        findViewById(R.id.action_align_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setAlignRight();
            }
        });

        findViewById(R.id.action_blockquote).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setBlockquote();
            }
        });

        findViewById(R.id.action_insert_bullets).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setBullets();
            }
        });

        findViewById(R.id.action_insert_numbers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setNumbers();
            }
        });

        findViewById(R.id.action_insert_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.insertImage("http://www.1honeywan.com/dachshund/image/7.21/7.21_3_thumb.JPG",
                        "dachshund");
            }
        });

        findViewById(R.id.action_insert_link).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEditor.insertLink("https://github.com/wasabeef", "wasabeef");
                    }
                });
        findViewById(R.id.action_insert_checkbox).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.insertTodo();
            }
        });

    }

    // Inject into Context for font change
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    //back navigation
    public void ReturnHome(View view) {


        finish();
    }


    //getting image uri from image bitmap
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", "not found");

        return Uri.parse(path);
    }


    // Get Path of selected image
    private String getPath(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    // Initializing components inside init()method
    private void init() {
        reply_SubmitBtn = (Button) findViewById(R.id.chat_submitBtn);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        showImg = (ImageView) findViewById(R.id.query_image);
        outPutFile = new File(Environment.getExternalStorageDirectory(), "tem.jpg");
        notification_btn = (ImageButton) findViewById(R.id.notif);
        back_arrow = (ImageView) findViewById(R.id.ticket_avtivity_backArrow);
        camera_icon = (ImageButton) findViewById(R.id.camera_icon_image);
        notification_btn = (ImageButton) findViewById(R.id.notif_btn);
        cart_button = (ImageButton) findViewById(R.id.cart_btn);

        err_text = (TextView) findViewById(R.id.reply_texterror);

    }

    private boolean validation() {
        boolean validate;


        if (text_editor.equalsIgnoreCase("")) {
            err_text.setText(getResources().getString(R.string.query));
            validate = false;
        } else if (text_editor.length() > 200) {
            err_text.setText(getResources().getString(R.string.query_maximum));
            validate = false;
        } else {
            validate = true;
        }
        return validate;
    }

    // Cropping the image
    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;

        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));

        uCrop.start(TicketReplyActivity.this);
    }

    // Handling the crop image
    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            ResultActivity.startWithUri(TicketReplyActivity.this, resultUri, "TicketReplyActivity");
        } else {
            Toast.makeText(TicketReplyActivity.this, "Cannot_retrieve_cropped_image", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e(TAG, "handleCropError: ", cropError);
        } else {
            Toast.makeText(TicketReplyActivity.this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    // image select method
    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library"};
        // An alert dialog will be created
        AlertDialog.Builder builder = new AlertDialog.Builder(TicketReplyActivity.this);
        // builder.setTitle("Add Photo!");
        builder.setTitle(Html.fromHtml("<font color='#05ab9a'>Add Photo</font>"));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Profile_utility.checkPermission(TicketReplyActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        new PickerBuilder(TicketReplyActivity.this, PickerBuilder.SELECT_FROM_CAMERA)
                                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                                    @Override
                                    public void onImageReceived(Uri imageUri) {
                                        try {
                                            Bitmap bitmap = Utility.handleSamplingAndRotationBitmap(
                                                    TicketReplyActivity.this, imageUri);
                                            showImg.setBackgroundResource(0);
                                            showImg.setImageBitmap(bitmap);
                                        } catch (Exception e) {
                                            Toast.makeText(TicketReplyActivity.this, "Please insert photo again", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                })
                                .withTimeStamp(false)
                                .setCropScreenColor(Color.GREEN)
                                .start();  // method to call camera intent

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        new PickerBuilder(TicketReplyActivity.this, PickerBuilder.SELECT_FROM_GALLERY)
                                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                                    @Override
                                    public void onImageReceived(Uri imageUri) {
                                        try {
                                            Bitmap bitmap = Utility.handleSamplingAndRotationBitmap(TicketReplyActivity.this, imageUri);
                                            showImg.setBackgroundResource(0);
                                            showImg.setImageBitmap(bitmap);
                                        } catch (Exception e) {
                                            Toast.makeText(TicketReplyActivity.this, "Please insert photo again", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setCropScreenColor(Color.GREEN)
                                .setOnPermissionRefusedListener(new PickerBuilder.onPermissionRefusedListener() {
                                    @Override
                                    public void onPermissionRefused() {
                                        Toast.makeText(TicketReplyActivity.this, "This permision is necessary", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .start(); // method to call gallery intent

                } else {
                    dialog.dismiss();  // dialog will close
                }
            }
        });

        builder.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UCrop.REQUEST_CROP) {
            handleCropResult(data);
        }

        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }

        // calling gallery method on passing data
        if (requestCode == SELECT_FILE) {
            onSelectFromGalleryResult(data);
        }

        // calling camera method on passing data
        else if (requestCode == REQUEST_CAMERA) {
            if (data != null) {
                onCaptureImageResult(data.getData());
            } else {
                onCaptureImageResult(file);
            }

        }

    }

    private void onCaptureImageResult(Uri data) {
        if (data != null) {
            startCropActivity(data);
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            startCropActivity(data.getData());

        }
        showImg.setImageBitmap(bm);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }


    private void replyDoubt(final String ticket_id, final String answer_id,
                            final String msg_body, final String Regd_ID) {
        if (AppUtil.isInternetConnected(this)) {

            if (showImg.getDrawable() != null) {
                filepath = getPath(getImageUri(TicketReplyActivity.this,
                        ((BitmapDrawable) showImg.getDrawable()).getBitmap()));
                photofile = new File(filepath);


                compressedImageFile = null;
                compressedImageFile = BitmapUtil.compressImage(
                        TicketReplyActivity.this,
                        photofile,
                        height,
                        width);

            }

            Bundle bun = new Bundle();
            bun.putString("ticketID", ticket_id);
            bun.putString("answerId", answer_id);
            bun.putString("msgDetails", msg_body);


            if (compressedImageFile != null) {
                bun.putString("Image", compressedImageFile.toString());
            }

            showDialog(null, "Please wait...");
            NetworkService.startActionReplyDoubt(this, mServiceReceiver, bun);

        } else {
            Intent in = new Intent(TicketReplyActivity.this, Internet_Activity.class);
            startActivity(in);
        }

    }

    @Override
    protected void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                hideDialog();
                Intent in = new Intent(TicketReplyActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                hideDialog();
                Intent intent = new Intent(TicketReplyActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                hideDialog();
                Intent inin = new Intent(TicketReplyActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if (success != null) {
                    startActivity(new Intent(TicketReplyActivity.this,
                            TicketDetailsActivity.class)
                            .putExtra("activity", "Ticket_Reply")
                            .putExtra("ticket_id", ticketid));// intent.putExtra("activity","Ticket_list");

                    finish();
                }
                break;

        }
    }
}
