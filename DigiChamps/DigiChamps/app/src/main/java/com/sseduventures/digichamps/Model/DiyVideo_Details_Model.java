package com.sseduventures.digichamps.Model;

/**
 * Created by CHANDRA BHANU on 20-Jan-18.
 */

public class DiyVideo_Details_Model {

    String videoId,videoName,videoKey,videoThumbnail,videoDescription;


    public DiyVideo_Details_Model(String videoId, String videoName, String videoKey, String videoThumbnail, String videoDescription) {
        this.videoId = videoId;
        this.videoName = videoName;
        this.videoKey = videoKey;
        this.videoThumbnail = videoThumbnail;
        this.videoDescription = videoDescription;


    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoKey() {
        return videoKey;
    }

    public void setVideoKey(String videoKey) {
        this.videoKey = videoKey;
    }

    public String getVideoThumbnail() {
        return videoThumbnail;
    }

    public void setVideoThumbnail(String videoThumbnail) {
        this.videoThumbnail = videoThumbnail;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

}
