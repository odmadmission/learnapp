package com.sseduventures.digichamps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.DataModel_TicketAnswers;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.activities.NewNotificationActivity;
import com.sseduventures.digichamps.adapter.TicketAnswer_Adapter;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.DateTimeUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.model.DoubtDetailsModel;

/**
 * Created by RKB on 5/8/2017.
 */

public class TicketDetailsActivity extends FormActivity implements ServiceReceiver.Receiver {
    public TextView status_textView, date_textView, time_textView, ticket_numberText, subject_textview, subjectName_textView, description_textView, description_DetailsTextView;
    public ImageView ques_image;
    Toolbar toolbar;
    FloatingActionButton fab;
    private List<DataModel_TicketAnswers> ticketAnswerList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TicketAnswer_Adapter mAdapter;
    String  ans_imageview;
    private String user_id;
    private ArrayList<DataModel_TicketAnswers> list;
    private SpotsDialog dialog;
    String ticketid, ansid, activity;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    String question_imageview;
    int notify=0;
    CardView card;

    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // preventing screen capture

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_ticket_details);
        setModuleName(LogEventUtil.EVENT_Doubt_details);
        logEvent(LogEventUtil.KEY_Doubt_details,LogEventUtil.EVENT_Doubt_details);

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        //get extra for received ticket_id
        Intent intent = getIntent();
        activity = intent.getStringExtra("activity");
        notify = intent.getIntExtra("notify",0);


        if (activity.equalsIgnoreCase("Ticket_Reply")) {
            ticketid = intent.getStringExtra("ticket_id");
        } else {
            ticketid = intent.getStringExtra("ticket_id");

        }

        // Method to initialize views
        init();

        dialog = new SpotsDialog(this, "FUN + EDUCATION", R.style.Custom);
        list = new ArrayList<DataModel_TicketAnswers>();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent id = new Intent(TicketDetailsActivity.this, TicketReplyActivity.class);
                id.putExtra("ticket_id", ticketid);
                id.putExtra("answer_id", ansid);
                startActivity(id);

            }
        });


        if (AppUtil.isInternetConnected(this)) {

            //showDialog(null,"Please wait...");
            Bundle bundle=new Bundle();
            bundle.putString("ticket_id",ticketid);

            NetworkService.startDoubtDetails(TicketDetailsActivity.this,
                    mServiceReceiver, bundle);

        } else {

            startActivity(new Intent(getApplicationContext(), Internet_Activity.class));

            finish();

        }

    }

    //back navigation
    public void ReturnHome(View view) {

        finish();
    }

    // Initialization of views inside init method
    protected void init() {


        user_id = RegPrefManager.getInstance(this).getRegId()+"";

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        card = findViewById(R.id.card);
        card.setVisibility(View.INVISIBLE);
        toolbar = (Toolbar) findViewById(R.id.toolbar_ticket_details);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        status_textView = (TextView) findViewById(R.id.status_text);
        date_textView = (TextView) findViewById(R.id.text_date);
        time_textView = (TextView) findViewById(R.id.text_time);
        ticket_numberText = (TextView) findViewById(R.id.ticket_number);
        subject_textview = (TextView) findViewById(R.id.text_subject);
        subjectName_textView = (TextView) findViewById(R.id.text_subject_name);
        description_textView = (TextView) findViewById(R.id.text_description);
        description_DetailsTextView = (TextView) findViewById(R.id.text_subject_details);
        ques_image = (ImageView) findViewById(R.id.answer_image_show);
        recyclerView = (RecyclerView) findViewById(R.id.ticket_details_recyclerView);
        fab = (FloatingActionButton) findViewById(R.id.fab_reply);
        fab.setVisibility(View.GONE);

    }


    @Override
    public void onBackPressed() {

        if(notify==1)
            startActivity(
                    new Intent(TicketDetailsActivity.this,
                            NewDashboardActivity.class));
        else

        if(activity!=null&&activity.equals("notification"))
            startActivity(
                    new Intent(TicketDetailsActivity.this,
                            NewNotificationActivity.class));

        finish();

    }


    @Override
    public void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
        hideDialog();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }


    public String splitter(String data) {
        String val = data.substring(0, 8);
        return val;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();

       new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            card.setVisibility(View.VISIBLE);
                        }
                    },1000);


        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(TicketDetailsActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(TicketDetailsActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(TicketDetailsActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:


                DoubtDetailsModel doubtDetailsModel = resultData.getParcelable(
                        IntentHelper.RESULT_DATA
                );



                if(doubtDetailsModel!=null){



                String Ticket_id = String.valueOf(doubtDetailsModel.getSuccess().getDoubts().getQuestiondetails().getTicket_id());
                String Ticket_No = doubtDetailsModel.getSuccess().getDoubts().getQuestiondetails().getTicket_No();
                String Status = doubtDetailsModel.getSuccess().getDoubts().getQuestiondetails().getStatus();
                String Question_date = doubtDetailsModel.getSuccess().getDoubts().getQuestiondetails().getQuestion_date();
                String Qsubject = doubtDetailsModel.getSuccess().getDoubts().getQuestiondetails().getQsubject();
                String Question = doubtDetailsModel.getSuccess().getDoubts().getQuestiondetails().getQuestion();
                String Question_image = doubtDetailsModel.getSuccess().getDoubts().getQuestiondetails().getQuestion_image();
                String Asked_by = doubtDetailsModel.getSuccess().getDoubts().getQuestiondetails().getAsked_by();
                String Is_Answred = String.valueOf(doubtDetailsModel.getSuccess().getDoubts().getQuestiondetails().getIs_Answred());
                String Asked_Profile_image = doubtDetailsModel.getSuccess().getDoubts().getQuestiondetails().getAsked_Profile_image();
                String student_imageview = AppConfig.SRVR_URL + Asked_Profile_image;

                //ticket number
                ticket_numberText.setText(Ticket_No);

                //status
                if (Status.equalsIgnoreCase("O")) {
                    if (Is_Answred.equalsIgnoreCase("true")) {
                        Status = "Answered";
                        status_textView.setTextColor(getResources().getColor(R.color.open));
                    } else {
                        Status = "Unanswered";
                        status_textView.setTextColor(getResources().getColor(R.color.open));

                    }

                } else if (Status.equalsIgnoreCase("C")) {
                    Status = "Closed";
                    status_textView.setTextColor(getResources().getColor(R.color.close));
                } else if (Status.equalsIgnoreCase("D")) {
                    Status = "Overdue";
                    status_textView.setTextColor(getResources().getColor(R.color.overdue));
                } else if (Status.equalsIgnoreCase("R")) {
                    Status = "Rejected";
                    status_textView.setTextColor(getResources().getColor(R.color.reject));
                }


                    if (Question_image != null) {
                        question_imageview = AppConfig.SRVR_URL + Question_image;
                        Picasso.with(TicketDetailsActivity.this).load(question_imageview).
                                resize(50,50).
                                into(ques_image);
                    }else{

                    }


                if (Question_image.equalsIgnoreCase("")) {
                    ques_image.setVisibility(View.GONE);
                } else {
                    ques_image.setVisibility(View.VISIBLE);
                }



                    ques_image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent in = new Intent(TicketDetailsActivity.this,FullImageActivity.class);
                            in.putExtra("activity","FromDoubtDescrtiption");
                            in.putExtra("url",question_imageview);
                            startActivity(in);
                        }
                    });






                status_textView.setText(Status);

                //ExamQuestion date& time
                StringTokenizer que_date_time = new StringTokenizer(Question_date, "T");
                String Date_ques = que_date_time.nextToken();
                String Time_ques = que_date_time.nextToken();

                date_textView.setText(Date_ques);


                String ques_time = DateTimeUtil.convertMillisAsDateTimeString(
                        DateTimeUtil.StringDoubtDateStrToDate(Question_date).getTime());
                //splitter(Time_ques);
                time_textView.setText(ques_time);

                //subject
                subjectName_textView.setText(Qsubject);
                //question
                description_DetailsTextView.setText(Question);


                if (doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().size() == 0
                        && Status.equalsIgnoreCase("Closed")) {
                    // hide
                    fab.setVisibility(View.GONE);
                } else if (doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().size() == 0
                        && Status.equalsIgnoreCase("Rejected")) {
                    fab.setVisibility(View.GONE);
                } else if (doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().size() == 0
                        && Status.equalsIgnoreCase("Unanswered")) {
                    fab.setVisibility(View.GONE);
                }

                else if (doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().size() >= 1
                        && Status.equalsIgnoreCase("Closed")) {
                    fab.setVisibility(View.GONE);

                } else if (doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().size() >= 1
                        && Status.equalsIgnoreCase("Rejected")) {
                    fab.setVisibility(View.GONE);
                } else if (doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().size() >= 1
                        && Status.equalsIgnoreCase("Unanswered")) {
                    fab.setVisibility(View.GONE);
                }

                else {
                    // Bind the Array to ArrayList for Answer List
                    // Show FAB Button
                    fab.setVisibility(View.VISIBLE);

                    for (int i = 0; i < doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().size(); i++) {

                        String is_teacher = String.valueOf(doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().get(i).getIs_teacher());

                        ansid = String.valueOf(doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().get(i).getAnswer_id());

                        String Answered_by = doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().get(i).getAnswered_by();
                        String student = doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().get(i).getAsked_by();
                        String Answer_date = doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().get(i).getAnswer_date();

                        String Answer_by_profile = doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().get(i).getAnswer_by_profile();
                        String teacher_imageview = AppConfig.SRVR_URL + Answer_by_profile;

                        String Student_profile = doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().get(i).getAsked_Profile_image();
                        String student_image = AppConfig.SRVR_URL + Student_profile;

                        String Answer = doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().get(i).getAnswer();

                        String Answer_image = doubtDetailsModel.getSuccess().getDoubts().getAnswerdetails().get(i).getAnswer_image();
                        if (!Answer_image.equalsIgnoreCase("")) {
                            ans_imageview = AppConfig.SRVR_URL + Answer_image;
                        } else {
                            ans_imageview = "";
                        }


                        //Answer date and time
                        StringTokenizer ans_date_time = new StringTokenizer(Answer_date, "T");
                        String Date = ans_date_time.nextToken();
                        String Time = ans_date_time.nextToken();
                        String ans_time = splitter(Time);

                        //check the data comes teacher or student_reply
                        if (is_teacher.equalsIgnoreCase("true")) {
                            list.add(new DataModel_TicketAnswers(Answered_by, Date, ans_time, teacher_imageview, Answer,
                                    ans_imageview));

                        } else {
                            student = "Me";
                            list.add(new DataModel_TicketAnswers(student, Date, ans_time, student_image, Answer,
                                    ans_imageview));
                        }


                    }

                    //adapter to bind the data
                    mAdapter = new TicketAnswer_Adapter(list, TicketDetailsActivity.this);
                    recyclerView.setAdapter(mAdapter);
                }

                }else{

                    Toast.makeText(this, "Something went wrong!Please try again..", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }
}
