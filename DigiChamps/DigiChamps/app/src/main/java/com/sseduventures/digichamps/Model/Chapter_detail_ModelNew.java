package com.sseduventures.digichamps.Model;

/**
 * Created by ntspl24 on 6/9/2017.
 */

public class Chapter_detail_ModelNew {
    private String Module_Id,Module_Title,Description,Module_Image,
            Image_Key,Is_Avail,pdf_file,Is_Free,Validity, isExpire,
            Question_Pdf,No_Of_Question,Is_Free_Test,file_resource,video_key;


    public Chapter_detail_ModelNew(String Module_Id, String Module_Title,
                                   String Description, String Module_Image,
                                   String Image_Key, String Is_Avail, String pdf_file,
                                   String Is_Free, String Validity, String isExpire,
                                   String Question_Pdf, String No_Of_Question,
                                   String Is_Free_Test, String file_resource) {
        this.Module_Id = Module_Id;
        this.Module_Title = Module_Title;
        this.Description = Description;
        this.Module_Image = Module_Image;
        this.Image_Key = Image_Key;
        this.Is_Avail = Is_Avail;
        this.pdf_file = pdf_file;
        this.Is_Free = Is_Free;
        this.Validity = Validity;
        this.Question_Pdf = Question_Pdf;
        this.No_Of_Question = No_Of_Question;
        this.Is_Free_Test = Is_Free_Test;
        this.file_resource = file_resource;
        this.isExpire = isExpire;
    }


    public String getModule_Id() {
        return Module_Id;
    }

    public String getModule_Title() {
        return Module_Title;
    }

    public String getDescription() {
        return Description;
    }

    public String getModule_Image() {
        return Module_Image;
    }

    public String getImage_Key() {
        return Image_Key;
    }

    public String getIs_Avail() {
        return Is_Avail;
    }

    public String getpdf_file() {
        return pdf_file;
    }

    public String getIs_Free() {
        return Is_Free;
    }

    public String getValidity() {
        return Validity;
    }

    public String getQuestion_Pdf() {
        return Question_Pdf;
    }

    public String getNo_Of_Question() {
        return No_Of_Question;
    }

    public String get_file_resource() {
        return file_resource;
    }

    public String getVideoExpiry() {
        return isExpire;
    }


}





