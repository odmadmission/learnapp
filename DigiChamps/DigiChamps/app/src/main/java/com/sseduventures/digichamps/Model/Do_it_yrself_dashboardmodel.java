package com.sseduventures.digichamps.Model;

/**
 * Created by Tech_1 on 1/16/2018.
 */

public class Do_it_yrself_dashboardmodel {
    private String DIYVideo_ID,DIYVideo_Name, DIYVideo_Upload, DIYVideo_Description,DIYImages;

    public Do_it_yrself_dashboardmodel(String DIYVideo_ID, String DIYVideo_Name,String DIYVideo_Upload, String DIYVideo_Description,
                                String DIYImages) {

        this.DIYVideo_ID = DIYVideo_ID;
        this.DIYVideo_Name = DIYVideo_Name;
        this.DIYVideo_Upload = DIYVideo_Upload;
        this.DIYVideo_Description = DIYVideo_Description;
        this.DIYImages = DIYImages;

    }

    public String getDIYVideo_ID() {
        return DIYVideo_ID;
    }

    public String getDIYVideo_Name() {
        return DIYVideo_Name;
    }

    public String getDIYVideo_Upload() {
        return DIYVideo_Upload;
    }

    public String getDIYVideo_Description() {
        return DIYVideo_Description;
    }

    public String getDIYImages() {
        return DIYImages;
    }



}
