package com.sseduventures.digichamps.frags;

import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.PersonalMentorActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.PMOverDueAdapter;
import com.sseduventures.digichamps.domain.PMOverduetaskDataList;
import com.sseduventures.digichamps.domain.PMResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;
import java.util.List;

public class PersonalMentorOverDueTask extends Fragment implements ServiceReceiver.Receiver {

    private RecyclerView rv_over_due;
    private TextView no_overduetask;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private FormActivity mContext;
    private PMResponse success;
    private List<PMOverduetaskDataList> pmOverduetaskDataLists;
    private PMOverDueAdapter pmOverDueAdapter;
    private ShimmerFrameLayout mShimmerViewContainer;

    PersonalMentorActivity context;

    public PersonalMentorOverDueTask() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overdue_task, container, false);
        ((FormActivity)getActivity()).logEvent(LogEventUtil.
                        KEY_pm_overdue_task,
                LogEventUtil.EVENT_pm_overdue_task);

        ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_pm_overdue_task);

        init(view);

        return view;
    }

    private void init(View view) {

        rv_over_due = view.findViewById(R.id.rv_over);
        no_overduetask = view.findViewById(R.id.no_overduetask);
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mContext = (PersonalMentorActivity) getActivity();
        pmOverduetaskDataLists = new ArrayList<>();

        context = (PersonalMentorActivity) getActivity();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv_over_due.setLayoutManager(mLayoutManager);

        pmOverDueAdapter = new PMOverDueAdapter(pmOverduetaskDataLists, context);
        rv_over_due.setAdapter(pmOverDueAdapter);

        getPmDetails();

    }

    private void getPmDetails() {

        if (AppUtil.isInternetConnected(getActivity())) {
            NetworkService.startActionGetPmDetails(getActivity(), mServiceReceiver);
        } else {
            mContext.showToast("No Internet Connection");
        }
    }

    @Override
    public void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    public void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);
            }
        },1000);
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                mContext.hideDialog();
                Intent in = new Intent(getActivity(), ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                mContext.hideDialog();
                Intent intent = new Intent(getActivity(), Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                mContext.hideDialog();
                Intent inin = new Intent(getActivity(), ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:
                mContext.hideDialog();
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);


                setOverDueTask();


                break;


        }

    }

    private void setOverDueTask() {
        if (success != null
                && success.getSuccessresultTask() != null
                && success.getSuccessresultTask().getOverDueTaskList() != null
                && success.getSuccessresultTask().getOverDueTaskList()
                .getTaskDataList() != null
                && success.getSuccessresultTask().getOverDueTaskList()
                .getTaskDataList().size() > 0) {

            rv_over_due.setVisibility(View.VISIBLE);
            no_overduetask.setVisibility(View.GONE);
            pmOverduetaskDataLists.clear();
            pmOverduetaskDataLists.addAll(success.getSuccessresultTask().getOverDueTaskList()
                    .getTaskDataList());
            pmOverDueAdapter.notifyDataSetChanged();

        } else {

            rv_over_due.setVisibility(View.GONE);
            no_overduetask.setVisibility(View.VISIBLE);
        }
    }

}
