package com.sseduventures.digichamps.activities;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.adapter.NewNotificationAdapter;
import com.sseduventures.digichamps.database.DbContract;
import com.sseduventures.digichamps.firebase.LogEventUtil;

public class NewNotificationActivity extends FormActivity
        implements LoaderManager.LoaderCallbacks<Cursor>{



    private ListView mRecyclerView;
    private NewNotificationAdapter mNewNotificationAdapter;
    private ShimmerFrameLayout mShimmerViewContainer;


    private LinearLayout mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_notification);
        logEvent(LogEventUtil.KEY_Notifications_PAGE,
                        LogEventUtil.EVENT_Notifications_PAGE);
        setModuleName(LogEventUtil.EVENT_Notifications_PAGE);

        init();
        new Handler().postDelayed(new Runnable() {
                                      @Override
                                      public void run() {
                                          getLoaderManager().initLoader(0, null,
                                                  NewNotificationActivity.this);

                                      }
                                  },1000);

    }

    private void init()
    {

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        toolbar.setNavigationIcon(R.drawable.ic_back123);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        mShimmerViewContainer.startShimmer();
        mShimmerViewContainer.setVisibility(View.VISIBLE);


        mTextView = (LinearLayout) findViewById(R.id.tvDummy);
        mRecyclerView = (ListView) findViewById(R.id.my_recycler_view);
        mNewNotificationAdapter=new NewNotificationAdapter(this,null);
        mRecyclerView.setAdapter(mNewNotificationAdapter);

        mTextView.setVisibility(View.GONE);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this, DbContract.NotificationTable.CONTENT_URI,
                DbContract.NotificationTable.MEMBER_PROJECTION,null,
                null,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, final Cursor cursor) {

        if(cursor!=null)
        {
            if(cursor.getCount()==0)
            {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mShimmerViewContainer.stopShimmer();
                        mShimmerViewContainer.setVisibility(View.GONE);
                        mTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);

                    }
                },1000);

                //showToast("No data found");


            }
            else
            {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mShimmerViewContainer.stopShimmer();
                        mShimmerViewContainer.setVisibility(View.GONE);
                        mTextView.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mNewNotificationAdapter.swapCursor(cursor);
                        mNewNotificationAdapter.notifyDataSetChanged();

                    }
                },1000);



            }
        }
        else
        {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mShimmerViewContainer.stopShimmer();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    mTextView.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);

                }
            },1000);



            //showToast("No data found");
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mNewNotificationAdapter.swapCursor(null);
        mNewNotificationAdapter.notifyDataSetChanged();
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {
        finish();
    }
}
