package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/2/2018.
 */

public class ChangePasswordResponse implements Parcelable
{
    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    private String Message;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Message);
    }

    public ChangePasswordResponse() {
    }

    protected ChangePasswordResponse(Parcel in) {
        this.Message = in.readString();
    }

    public static final Creator<ChangePasswordResponse> CREATOR = new Creator<ChangePasswordResponse>() {
        @Override
        public ChangePasswordResponse createFromParcel(Parcel source) {
            return new ChangePasswordResponse(source);
        }

        @Override
        public ChangePasswordResponse[] newArray(int size) {
            return new ChangePasswordResponse[size];
        }
    };
}
