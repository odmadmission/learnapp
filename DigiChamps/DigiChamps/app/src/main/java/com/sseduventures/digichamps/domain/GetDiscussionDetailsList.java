package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/13/2018.
 */

public class GetDiscussionDetailsList implements Parcelable{


    private int DiscussionDetailID;
    private int DiscussionID;
    private String DetailText;
    private String CreatedDate;
    private int IsActive;
    private int CreatedBy;
    private int RoleID;
    private String CreatedName;
    private String photoURL;
    private Integer colorCode;
    private String hashColorCode;


    public int getDiscussionDetailID() {
        return DiscussionDetailID;
    }

    public void setDiscussionDetailID(int discussionDetailID) {
        DiscussionDetailID = discussionDetailID;
    }

    public int getDiscussionID() {
        return DiscussionID;
    }

    public void setDiscussionID(int discussionID) {
        DiscussionID = discussionID;
    }

    public String getDetailText() {
        return DetailText;
    }

    public void setDetailText(String detailText) {
        DetailText = detailText;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int createdBy) {
        CreatedBy = createdBy;
    }

    public int getRoleID() {
        return RoleID;
    }

    public void setRoleID(int roleID) {
        RoleID = roleID;
    }

    public String getCreatedName() {
        return CreatedName;
    }

    public void setCreatedName(String createdName) {
        CreatedName = createdName;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public Integer getColorCode() {
        return colorCode;
    }

    public void setColorCode(Integer colorCode) {
        this.colorCode = colorCode;
    }

    public String getHashColorCode() {
        return hashColorCode;
    }

    public void setHashColorCode(String hashColorCode) {
        this.hashColorCode = hashColorCode;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.DiscussionDetailID);
        dest.writeInt(this.DiscussionID);
        dest.writeString(this.DetailText);
        dest.writeString(this.CreatedDate);
        dest.writeInt(this.IsActive);
        dest.writeInt(this.CreatedBy);
        dest.writeInt(this.RoleID);
        dest.writeString(this.CreatedName);
        dest.writeString(this.photoURL);
        dest.writeValue(this.colorCode);
        dest.writeString(this.hashColorCode);
    }

    public GetDiscussionDetailsList() {
    }

    protected GetDiscussionDetailsList(Parcel in) {
        this.DiscussionDetailID = in.readInt();
        this.DiscussionID = in.readInt();
        this.DetailText = in.readString();
        this.CreatedDate = in.readString();
        this.IsActive = in.readInt();
        this.CreatedBy = in.readInt();
        this.RoleID = in.readInt();
        this.CreatedName = in.readString();
        this.photoURL = in.readString();
        this.colorCode = (Integer) in.readValue(Integer.class.getClassLoader());
        this.hashColorCode = in.readString();
    }

    public static final Creator<GetDiscussionDetailsList> CREATOR = new Creator<GetDiscussionDetailsList>() {
        @Override
        public GetDiscussionDetailsList createFromParcel(Parcel source) {
            return new GetDiscussionDetailsList(source);
        }

        @Override
        public GetDiscussionDetailsList[] newArray(int size) {
            return new GetDiscussionDetailsList[size];
        }
    };
}
