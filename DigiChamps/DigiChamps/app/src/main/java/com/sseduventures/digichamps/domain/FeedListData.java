package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/6/2018.
 */

public class FeedListData implements Parcelable {

    private int Feed_ID;
    private String Feed_Name;
    private String Feed_Images;
    private String Feed_Path;
    private String Feed_Description;
    private String FeedOnline;
    private String FeedBeta;

    public int getFeed_ID() {
        return Feed_ID;
    }

    public void setFeed_ID(int feed_ID) {
        Feed_ID = feed_ID;
    }

    public String getFeed_Name() {
        return Feed_Name;
    }

    public void setFeed_Name(String feed_Name) {
        Feed_Name = feed_Name;
    }

    public String getFeed_Images() {
        return Feed_Images;
    }

    public void setFeed_Images(String feed_Images) {
        Feed_Images = feed_Images;
    }

    public String getFeed_Path() {
        return Feed_Path;
    }

    public void setFeed_Path(String feed_Path) {
        Feed_Path = feed_Path;
    }

    public String getFeed_Description() {
        return Feed_Description;
    }

    public void setFeed_Description(String feed_Description) {
        Feed_Description = feed_Description;
    }

    public String getFeedOnline() {
        return FeedOnline;
    }

    public void setFeedOnline(String feedOnline) {
        FeedOnline = feedOnline;
    }

    public String getFeedBeta() {
        return FeedBeta;
    }

    public void setFeedBeta(String feedBeta) {
        FeedBeta = feedBeta;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Feed_ID);
        dest.writeString(this.Feed_Name);
        dest.writeString(this.Feed_Images);
        dest.writeString(this.Feed_Path);
        dest.writeString(this.Feed_Description);
        dest.writeString(this.FeedOnline);
        dest.writeString(this.FeedBeta);
    }

    public FeedListData() {
    }

    protected FeedListData(Parcel in) {
        this.Feed_ID = in.readInt();
        this.Feed_Name = in.readString();
        this.Feed_Images = in.readString();
        this.Feed_Path = in.readString();
        this.Feed_Description = in.readString();
        this.FeedOnline = in.readString();
        this.FeedBeta = in.readString();
    }

    public static final Creator<FeedListData> CREATOR = new Creator<FeedListData>() {
        @Override
        public FeedListData createFromParcel(Parcel source) {
            return new FeedListData(source);
        }

        @Override
        public FeedListData[] newArray(int size) {
            return new FeedListData[size];
        }
    };
}
