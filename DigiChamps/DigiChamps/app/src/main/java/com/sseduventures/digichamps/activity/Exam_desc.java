package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.activities.NewExamActivity;
import com.sseduventures.digichamps.adapter.PagerAdapter_ExamDesc;
import com.sseduventures.digichamps.config.AppController;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.utils.AppUtil;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class Exam_desc extends FormActivity {

    private Button btn_start;
    private String exam_id, chap_id, is_offline, sub_name, sub_id,module_name;
    private ImageView backBtn_examDesc;
    private ImageButton nextButton;
    private ViewPager viewPager;
    private TextView pageNumber;
    private String  flags;
    private String type;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.exam_desc);
        logEvent(LogEventUtil.KEY_TEST_INST_PAGE,
                LogEventUtil.EVENT_TEST_INST_PAGE);
        setModuleName(LogEventUtil.EVENT_TEST_INST_PAGE);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            exam_id = bundle.getString("exam_id");
            chap_id = bundle.getString("chap_id");
            sub_name = bundle.getString("sub_name");
            module_name = bundle.getString("module_type");
            sub_id = bundle.getString("sub_id");
            type = bundle.getString("type");
            flags = bundle.getString("flags");
            is_offline = String.valueOf(bundle.getBoolean("isOffline"));


        }
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new PagerAdapter_ExamDesc(this,module_name));
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);


        btn_start = (Button) findViewById(R.id.start_exam_btn);
        backBtn_examDesc = (ImageView) findViewById(R.id.test_instruction__back_image);
        pageNumber = (TextView) findViewById(R.id.pageNumber);
        nextButton = (ImageButton) findViewById(R.id.next);
        nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(getItem(+1), true); //getItem(-1) for previous
            }
        });

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppController.getInstance().
                        trackEvent("Exam Start", "Exam Start", "Start Exam Button Clicked");

                if (AppUtil.isInternetConnected(Exam_desc.this)) {


                    if(sub_name.equalsIgnoreCase("psychometric")){

                        startActivity(new Intent(Exam_desc.this, PsychometricExamActivity.class).putExtra("exam_id", exam_id)
                                .putExtra("module_type", module_name)
                                .putExtra("chapter_id", chap_id)
                                .putExtra("subject_id", sub_id)
                                .putExtra("subName", sub_name)
                                .putExtra("isOffline", false)
                                .putExtra("flagName", flags)

                        );

                        finish();
                    }else {

                        startActivity(new Intent(Exam_desc.this,
                                NewExamActivity.class).putExtra("exam_id", exam_id)
                                .putExtra("module_type", module_name)
                                .putExtra("chapter_id", chap_id)
                                .putExtra("subject_id", sub_id)
                                .putExtra("subName", sub_name)

                                .putExtra("flagName", flags)

                        );

                        finish();
                    }


                } else {

                    Intent intent = new Intent(Exam_desc.this, Internet_Activity.class);
                    startActivity(intent);

                    finish();
                }

            }
        });

        backBtn_examDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtil.isInternetConnected(Exam_desc.this)) {
                    Class c=Chapter_Details.class;
                    if(sub_name.equals("psychometric"))
                        c=NewDashboardActivity.class;

                    startActivity(new Intent(Exam_desc.this,
                            c).
                            putExtra("chapter_id", chap_id)
                            .putExtra("subject_name", sub_name)
                            .putExtra("subject_id", sub_id)
                            .putExtra("flagNewKey",flags));


                    finish();
                } else {

                    Intent intent = new Intent(Exam_desc.this, Internet_Activity.class);
                    startActivity(intent);

                    finish();
                }

            }
        });
        }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            if(position==0){
                pageNumber.setText(1+"/"+3);
            }else if(position==1){
                pageNumber.setText(2+"/"+3);
            }else{
                pageNumber.setText(3+"/"+3);
            }

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    @Override
    public void onBackPressed() {
        Class c=Chapter_Details.class;
        if(sub_name.equals("psychometric"))
            c=NewDashboardActivity.class;

        startActivity(new Intent(Exam_desc.this,
                c).
                putExtra("chapter_id", chap_id)
      .putExtra("subject_name", sub_name)
        .putExtra("subject_id", sub_id)
        .putExtra("flagNewKey",flags));


        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;

    }

}