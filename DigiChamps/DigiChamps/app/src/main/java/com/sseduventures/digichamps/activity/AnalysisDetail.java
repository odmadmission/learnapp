package com.sseduventures.digichamps.activity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;

import com.sseduventures.digichamps.adapter.AnalysisDetail_Adapter;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.config.AppController;
import com.sseduventures.digichamps.domain.AnalyticsSubjectResponse;
import com.sseduventures.digichamps.domain.SubConceptResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AnalysisDetail extends FormActivity implements ServiceReceiver.Receiver {

    RecyclerView rvSubConceptAnalysis;
    SharedPreferences preference;
    String user_id;
    List<AnalyticsSubjectResponse.AnalyticsSubjectSuccess> subConceptList;
    List<AnalyticsSubjectResponse.AnalyticsSubjectChapterList> subChapList;
    Spinner spinner_sub;
    ArrayList<String> mSpinnerSubjectname;
    ArrayAdapter<String> adapterSub;
    private AnalysisDetail_Adapter rvChapListAdapter;
    ImageView lead_back_image;
    int classId;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private AnalyticsSubjectResponse success;
    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.analysis_detail_activity);
        setModuleName(LogEventUtil.EVENT_Analysis_details);
        //For font_roboto_regular type
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        init();


        user_id = String.valueOf(RegPrefManager.getInstance(this).getRegId());

        classId = (int) RegPrefManager.getInstance(this).getclassid();
        getSubjectAnalyticsData();

        spinner_sub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String subName = spinner_sub.getItemAtPosition(spinner_sub.getSelectedItemPosition()).toString();

                setChapterList(subName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        lead_back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });


    }

    private void init() {

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        lead_back_image = (ImageView) findViewById(R.id.lead_back_image);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        subConceptList = new ArrayList<>();
        subChapList = new ArrayList<>();
        mSpinnerSubjectname = new ArrayList<>();

        rvSubConceptAnalysis = (RecyclerView) findViewById(R.id.recycler_analysisDetail);
        spinner_sub = (Spinner) findViewById(R.id.spinner_sub);


        rvSubConceptAnalysis.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvSubConceptAnalysis.setLayoutManager(layoutManager);

        rvChapListAdapter = new AnalysisDetail_Adapter((ArrayList<AnalyticsSubjectResponse.AnalyticsSubjectChapterList>) subChapList,
                this);

        rvSubConceptAnalysis.setAdapter(rvChapListAdapter);
        preference = getSharedPreferences("user_data", getApplicationContext().MODE_PRIVATE);
        //"No name defined" is the default value.
        user_id = preference.getString("User_ID", null);


        adapterSub = new ArrayAdapter<String>(AnalysisDetail.this, android.R.layout.simple_spinner_dropdown_item, mSpinnerSubjectname);
        adapterSub.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner_sub.setAdapter(adapterSub);
    }


    //back navigation
    public void ReturnHome(View view) {
        super.onBackPressed();
        finish();
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    private void setChapterList(String subName) {


        for (int i = 0; i < subConceptList.size(); i++) {
            if (subConceptList.get(i).getSubject().equals(subName)) {
                subChapList.clear();
                subChapList.addAll(subConceptList.get(i).getChapterList());

                rvChapListAdapter.notifyDataSetChanged();
            }
        }
    }

    // Inject into Context for font change
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void getSubjectAnalyticsData() {
        if (AppUtil.isInternetConnected(this)) {


           // showDialog(null, "Please wait...");
            NetworkService.startActionGetAnalyticsSubjectData(this, mServiceReceiver);

        } else {
            Intent in = new Intent(AnalysisDetail.this, Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        //hideDialog();
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if (success != null && success.getSuccess() != null
                        && success.getSubjectList() != null
                        && success.getSubjectList().size() > 0
                        && success.getSuccess().size() > 0) {

                    mSpinnerSubjectname.clear();
                    mSpinnerSubjectname.addAll(success.getSubjectList());

                    subConceptList.clear();
                    subConceptList.addAll(success.getSuccess());


                    adapterSub.notifyDataSetChanged();
                    spinner_sub.setSelection(0);


                }


                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(AnalysisDetail.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(AnalysisDetail.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(AnalysisDetail.this, Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }

}
