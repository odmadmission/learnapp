package com.sseduventures.digichamps.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.DIYOnlineVideoPlayer;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.domain.NewLearnDiyModuleLists;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.utils.AppUtil;

import java.util.List;



public class DIYOnlinePlayerAdapter extends RecyclerView.Adapter<DIYOnlinePlayerAdapter.MyViewHolder> {
    View view;

    private String moduleId,user_id;

    private ImageView listener;
    private DIYOnlineVideoPlayer context;
    private List<NewLearnDiyModuleLists> moviesList;
    public View mView;
    int count = 0;

    public DIYOnlinePlayerAdapter(List<NewLearnDiyModuleLists> moviesList,
                                  DIYOnlineVideoPlayer context, String moduleId) {
        this.moviesList = moviesList;
        this.context = context;
        this.moduleId=moduleId;

        user_id = String.valueOf(RegPrefManager.getInstance(context).getRegId());
    }



    @Override
    public DIYOnlinePlayerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_diy_video_item, parent, false);

        return new DIYOnlinePlayerAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DIYOnlinePlayerAdapter.MyViewHolder holder, int position) {
        NewLearnDiyModuleLists chap_detail = moviesList.get(position);
        holder.title.setText(chap_detail.getDIYVideo_Name());
        if(moviesList.get(position).getDiyPosterImage_production()!=null&&
                !moviesList.get(position).getDiyPosterImage_production().equalsIgnoreCase("null")
                &&!moviesList.get(position).getDiyPosterImage_production().isEmpty())
            Picasso.with(context).load(
                    moviesList.get(position).getDiyPosterImage_production()
            ).into( holder.thumbnail_image);


        listener = holder.play;




        holder.rlRoot.setTag(chap_detail);
        if(chap_detail.isFlag())
        {
            mView=holder.rlRoot;
            holder.rlRoot.setClickable(false);
            holder.rlRoot.setBackgroundResource(R.drawable.video_item_card_bg);
        }
        else
        {
            holder.rlRoot.setClickable(true);
            holder.rlRoot.setOnClickListener(onClickListener(position,holder));
            holder.rlRoot.setBackgroundResource(R.drawable.abc_btn_selector);
        }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private View.OnClickListener onClickListener(final int position, final DIYOnlinePlayerAdapter.MyViewHolder holder) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                NewLearnDiyModuleLists chap_detail = (NewLearnDiyModuleLists)v.getTag();

                context.playVideo(chap_detail.getDIYVideo_Upload());



                if (AppUtil.isInternetConnected(context)) {




                } else {
                    Intent i = new Intent(context, Internet_Activity.class);
                    Activity activity = (Activity) context;

                    context.startActivity(i);

                }
            }

        };
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView play;
        ImageView left_drawable, right_drawable;
        CardView container;
        RelativeLayout rlRoot;
        private ImageView thumbnail_image,thumbnail_lock;

        public MyViewHolder(View view) {
            super(view);
            container = (CardView) itemView.findViewById(R.id.card_view_chapter);
            rlRoot = (RelativeLayout) itemView.findViewById(R.id.rlRoot);
            thumbnail_image =(ImageView) view.findViewById(R.id.thumbnail_imagev);
            title = (TextView) view.findViewById(R.id.chap);
            thumbnail_lock =(ImageView) view.findViewById(R.id.thumbnail_imageve);



        }
    }






}
