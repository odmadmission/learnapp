package com.sseduventures.digichamps.activities;

import android.app.Dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.Model.Basic_info_model;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.DoubtListNewAdapter;
import com.sseduventures.digichamps.adapter.Doubt_listview_DialogAdapter;
import com.sseduventures.digichamps.domain.DoubtListData;
import com.sseduventures.digichamps.domain.DoubtListResponse;
import com.sseduventures.digichamps.domain.SubjectResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;
import java.util.List;

public class DoubtListActivity extends FormActivity implements ServiceReceiver.Receiver {


    private RelativeLayout rlv_lay_one;
    private TextView txt_sub_name, learn_today, headeryourdoubt;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private FloatingActionButton fab;
    private Toolbar toolbar;
    TextView no_doubt;

    private ArrayList<SubjectResponse> success;
    private String subject;
    private DoubtListResponse mSuccess;
    private ImageView image;

    private ArrayList<Basic_info_model> subjectListNew;

    private List<DoubtListData> mList;

    private RecyclerView doubt_list;

    private DoubtListNewAdapter mAdapter;
    private String analytics = "";

    private Doubt_listview_DialogAdapter boardAdapter;
    private ShimmerFrameLayout mShimmerViewContainer;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_doubt_list);
        logEvent(LogEventUtil.KEY_Doubts_List_PAGE,
                LogEventUtil.EVENT_Doubts_List_PAGE);
        setModuleName(LogEventUtil.EVENT_Doubts_List_PAGE);

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        analytics = getIntent().getStringExtra("analytics");

        if (AppUtil.isInternetConnected(this)) {
            getSubject();
        } else {
            AskOptionDialogNew("Please Check Your Internet Connection").show();
        }

        rlv_lay_one = (RelativeLayout) findViewById(R.id.rlv_lay_one);
        txt_sub_name = (TextView) findViewById(R.id.txt_sub_name);
        doubt_list = (RecyclerView) findViewById(R.id.doubt_list);
        learn_today = (TextView) findViewById(R.id.learn_today);
        headeryourdoubt = (TextView) findViewById(R.id.textview);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);

        fab = findViewById(R.id.fab);
        fab.setVisibility(View.GONE);

        image = findViewById(R.id.image);
        no_doubt = findViewById(R.id.no_doubt);


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        doubt_list.setNestedScrollingEnabled(false);
        doubt_list.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        doubt_list.setLayoutManager(mLayoutManager);
        subjectListNew = new ArrayList<>();
        mList = new ArrayList<>();
        mAdapter = new DoubtListNewAdapter(mList, this);
        doubt_list.setAdapter(mAdapter);

        headeryourdoubt.setVisibility(View.GONE);


        rlv_lay_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AppUtil.isInternetConnected(DoubtListActivity.this)) {
                    showSubjectDialog();
                } else {
                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }
            }
        });


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setTitle("Doubt List");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));


    }


    private void getDoubtDetails() {
        if (AppUtil.isInternetConnected(this)) {

            Bundle bun = new Bundle();
            bun.putInt("subid", (int) getSubId(subject));

            if (AppUtil.isInternetConnected(this)) {
                //showDialog(null, "Please Wait..");
                mList.clear();
                mAdapter.notifyDataSetChanged();
                mShimmerViewContainer.startShimmer();
                mShimmerViewContainer.setVisibility(View.VISIBLE);
                NetworkService.startActionGetNewDoubtList(this, mServiceReceiver, bun);
            } else {
                Intent in = new Intent(this, Internet_Activity.class);
                startActivity(in);
            }
        }

    }

    private void getSubject() {

        if (AppUtil.isInternetConnected(this)) {
            showDialog(null, "Please Wait..");
            NetworkService.startActionGetSubject(this, mServiceReceiver);

        } else {
            Intent in = new Intent(this, Internet_Activity.class);
            startActivity(in);
        }

    }

    @Override
    protected void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();

    }

    @Override
    protected void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onBackPressed() {

        if (analytics != null && !analytics.equals("")) {

            finish();

        } else

        {

            Intent in = new Intent(DoubtListActivity.this, DoubtActivity.class);
            startActivity(in);
        }

    }

    private String[] subjectName = null;

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
       /* new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);
            }
        },1000);*/


        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                hideDialog();
                Intent in = new Intent(DoubtListActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                hideDialog();
                Intent intent = new Intent(DoubtListActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                hideDialog();
                Intent inin = new Intent(DoubtListActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:
                hideDialog();
                success = resultData.getParcelableArrayList(IntentHelper.RESULT_DATA);
                //showToast(success.size()+"");
                if (success != null && success.size() > 0) {
                    createSubjectName();

                }
                break;

            case ResponseCodes.GETDOUBTSUCCESS:
                hideDialog();



                mSuccess = resultData.getParcelable(IntentHelper.RESULT_DATA);
                headeryourdoubt.setVisibility(View.VISIBLE);
                if (mSuccess != null && mSuccess.getSuccess() != null && mSuccess.getSuccess().getDoubt_list() != null &&
                        mSuccess.getSuccess().getDoubt_list().size() > 0) {

                    mList.clear();
                    mList.addAll(mSuccess.getSuccess().getDoubt_list());

                    mAdapter.notifyDataSetChanged();
                    no_doubt.setVisibility(View.GONE);
                    doubt_list.setVisibility(View.VISIBLE);
                } else {
                    no_doubt.setVisibility(View.VISIBLE);
                    doubt_list.setVisibility(View.GONE);
                }
                break;


        }
    }


    private void createSubjectName() {
        subjectName = new String[success.size()];


        for (int i = 0; i < success.size(); i++) {
            subjectName[i] = success.get(i).getSubName();

            Basic_info_model basic_info_model = new Basic_info_model();
            basic_info_model.setName(success.get(i).getSubName());
            subjectListNew.add(basic_info_model);
        }

    }


    private void showSubjectDialog() {

        final Dialog dialog = new Dialog(DoubtListActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.setContentView(R.layout.board_editprofile_dialog);

        // Set dialog title

        // set values for custom dialog components - text, image and button


        ListView listView = (ListView) dialog.findViewById(R.id.dialoglist);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);
        TextView select_class_tv = (TextView) dialog.findViewById(R.id.select_class_tv);
        select_class_tv.setVisibility(View.GONE);
        select_tv.setVisibility(View.VISIBLE);
        select_tv.setText("Select Subject");
        select_tv.setTextColor(Color.parseColor("#519B9C"));
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        ok_but.setTextColor(Color.parseColor("#519B9C"));
        boardAdapter = new Doubt_listview_DialogAdapter(subjectListNew, getApplicationContext());

        listView.setAdapter(boardAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                subjectListNew.get(position).setFlag(true);
                for (int i = 0; i < subjectListNew.size(); i++) {
                    if (position != i)
                        subjectListNew.get(i).setFlag(false);
                }
                boardAdapter.notifyDataSetChanged();
            }
        });
        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Basic_info_model> list = boardAdapter.basic_info_modelArrayList();
                for (int i = 0; i < list.size(); i++) {
                    boolean flag = list.get(i).isFlag();
                    if (flag == true) {
                        Basic_info_model basic_info_model = list.get(i);

                        subject = list.get(i).getName();
                        getDoubtDetails();
                        prepareSubjectName(list.get(i).getName());

                    }
                }


                dialog.dismiss();

            }
        });

        dialog.show();


    }



    private void prepareSubjectName(String subName) {
        txt_sub_name.setVisibility(View.GONE);
        txt_sub_name.setText(subName);
        learn_today.setText(subName);
    }

    private long getSubId(String b) {
        long r = 0;
        for (int i = 0; i < success.size(); i++) {
            if (success.get(i).getSubName().equalsIgnoreCase(b)) {
                r = success.get(i).getSubId();
            }
        }

        return r;
    }

    private android.app.AlertDialog AskOptionDialogNew(String msg) {
        android.app.AlertDialog myQuittingDialogBox = new android.app.AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })

                .create();
        return myQuittingDialogBox;
    }
}
