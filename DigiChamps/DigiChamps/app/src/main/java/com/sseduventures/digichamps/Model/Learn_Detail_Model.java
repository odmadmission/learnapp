package com.sseduventures.digichamps.Model;

/**
 * Created by ntspl24 on 5/8/2017.
 */

public class Learn_Detail_Model {

    private String chapter_id, chapter_name, total_video, study_notes, ques, prt, CBT,chapImage;
    private double totalProgress;

    public double getTotalProgress() {
        return totalProgress;
    }

    public void setTotalProgress(double totalProgress) {
        this.totalProgress = totalProgress;
    }

    public Learn_Detail_Model() {
    }

    public Learn_Detail_Model(String chapter_id, String chapter_name, String total_video,
                              String study_notes, String ques, String prt, String CBT,double totalPg, String chapImage) {

        this.chapter_id = chapter_id;
        this.chapter_name = chapter_name;
        this.total_video = total_video;
        this.study_notes = study_notes;
        this.ques = ques;
        this.prt = prt;
        this.CBT = CBT;
        this.totalProgress=totalPg;
        this.chapImage = chapImage;
    }

    public String getChapter_id() {
        return chapter_id;
    }

    public String getChapter_name() {
        return chapter_name;
    }

    public String getTotal_video() {
        return total_video;
    }

    public String getStudy_notes() {
        return study_notes;
    }

    public String getQues() {
        return ques;
    }


    public String getPrt() {
        return prt;
    }

    public String getCBT() {
        return CBT;
    }

    public String getChapImage() {
        return chapImage;
    }

    public void setChapImage(String chapImage) {
        this.chapImage = chapImage;
    }
}





