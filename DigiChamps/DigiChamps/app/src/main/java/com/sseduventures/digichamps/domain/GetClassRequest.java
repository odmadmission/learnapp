package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class GetClassRequest implements Parcelable{

    private String SchoolId;

    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.SchoolId);
    }

    public GetClassRequest() {
    }

    protected GetClassRequest(Parcel in) {
        this.SchoolId = in.readString();
    }

    public static final Creator<GetClassRequest> CREATOR = new Creator<GetClassRequest>() {
        @Override
        public GetClassRequest createFromParcel(Parcel source) {
            return new GetClassRequest(source);
        }

        @Override
        public GetClassRequest[] newArray(int size) {
            return new GetClassRequest[size];
        }
    };
}
