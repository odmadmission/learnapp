
package com.sseduventures.digichamps.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeWorkList {

    @SerializedName("HomeworkId")
    @Expose
    private String homeworkId;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("homework")
    @Expose
    private String homework;
    @SerializedName("homeWorkDate")
    @Expose
    private String homeWorkDate;

    public String getHomeworkId() {
        return homeworkId;
    }

    public void setHomeworkId(String homeworkId) {
        this.homeworkId = homeworkId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getHomework() {
        return homework;
    }

    public void setHomework(String homework) {
        this.homework = homework;
    }

    public String getHomeWorkDate() {
        return homeWorkDate;
    }

    public void setHomeWorkDate(String homeWorkDate) {
        this.homeWorkDate = homeWorkDate;
    }

}
