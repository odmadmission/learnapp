package com.sseduventures.digichamps.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.helper.TouchImageView;
import com.sseduventures.digichamps.utils.IntentHelper;


import java.io.File;



public class FullImageActivity extends FormActivity {

    private static final String TAG=FullImageActivity.class.getSimpleName();
    File path = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);
        TouchImageView img = (TouchImageView) findViewById(R.id.result_image);
        Intent in = getIntent();
        String flag = getIntent().getStringExtra("activity");

        if(flag.equalsIgnoreCase("FromDoubt")){
            path =(File) in.getExtras().getSerializable("url");

            if(path!=null)
            {
                Picasso.with(this)
                        .load("file://"+path)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .into(img);
            }

        }else if(flag.equalsIgnoreCase("FromChat")){

            String pathChat = getIntent().getStringExtra("url");


            if(pathChat!=null){
                Picasso.with(this)
                        .load(pathChat)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .into(img)
                ;
            }


        }else if(flag.equalsIgnoreCase("FromDoubtDescrtiption")){

            String pathDoubtDesc = getIntent().getStringExtra("url");

            if(pathDoubtDesc!=null){
                Picasso.with(this)
                        .load(pathDoubtDesc)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .into(img)
                ;
            }

        }else if(flag.equalsIgnoreCase("FromDoubtDetails")){

            String pathDoubtDesc = getIntent().getStringExtra("url");

            if(pathDoubtDesc!=null){
                Picasso.with(this)
                        .load(pathDoubtDesc)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .into(img)
                ;
            }

        }


        else if(flag.equalsIgnoreCase("ExamActivity")){

            String pathDoubtDesc = getIntent().getStringExtra("url");

            if(pathDoubtDesc!=null){
                Picasso.with(this)
                        .load(pathDoubtDesc)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .into(img)
                ;
            }

        }


    }
    View decorView;

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
         decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {

                        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {

                        } else {

                        }
                    }
                });
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {

        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE

                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

                        | View.SYSTEM_UI_FLAG_FULLSCREEN);




    }

}
