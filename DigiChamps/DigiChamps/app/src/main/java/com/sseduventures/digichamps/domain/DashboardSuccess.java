package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/24/2018.
 */

public class DashboardSuccess implements Parcelable {

    protected DashboardSuccess(Parcel in) {
        resultCount = in.readInt();
        Is_Video = in.readByte() != 0;
        Mentorid = in.readInt();
        DoubtCount = in.readInt();
        PreviousRating = in.readInt();
        Boardid = in.readInt();
        classid = in.readInt();
        schoolid = in.readString();
        asignteacherid = in.readString();
        sectionid = in.readString();
        roleid = in.readInt();
        isSectionEnabled = in.readByte() != 0;
        Is_School = in.readByte() != 0;
        Is_Doubt = in.readByte() != 0;
        Is_Mentor = in.readByte() != 0;
        Is_Dictionary = in.readByte() != 0;
        Is_Feed = in.readByte() != 0;
        Subjectlists = in.readString();
        DiyModuleLists = in.readString();
        Recentwatchedvideos = in.readString();
        BannerList = in.readString();
        totalCoins = in.readInt();
        trialDate = in.readString();
        MentorVideoLink = in.readString();
        SchoolCode = in.readString();
        pack = in.readParcelable(PackObject.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(resultCount);
        dest.writeByte((byte) (Is_Video ? 1 : 0));
        dest.writeInt(Mentorid);
        dest.writeInt(DoubtCount);
        dest.writeInt(PreviousRating);
        dest.writeInt(Boardid);
        dest.writeInt(classid);
        dest.writeString(schoolid);
        dest.writeString(asignteacherid);
        dest.writeString(sectionid);
        dest.writeInt(roleid);
        dest.writeByte((byte) (isSectionEnabled ? 1 : 0));
        dest.writeByte((byte) (Is_School ? 1 : 0));
        dest.writeByte((byte) (Is_Doubt ? 1 : 0));
        dest.writeByte((byte) (Is_Mentor ? 1 : 0));
        dest.writeByte((byte) (Is_Dictionary ? 1 : 0));
        dest.writeByte((byte) (Is_Feed ? 1 : 0));
        dest.writeString(Subjectlists);
        dest.writeString(DiyModuleLists);
        dest.writeString(Recentwatchedvideos);
        dest.writeString(BannerList);
        dest.writeInt(totalCoins);
        dest.writeString(trialDate);
        dest.writeString(MentorVideoLink);
        dest.writeString(SchoolCode);
        dest.writeParcelable(pack, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DashboardSuccess> CREATOR = new Creator<DashboardSuccess>() {
        @Override
        public DashboardSuccess createFromParcel(Parcel in) {
            return new DashboardSuccess(in);
        }

        @Override
        public DashboardSuccess[] newArray(int size) {
            return new DashboardSuccess[size];
        }
    };

    public int getDoubtCount() {
        return DoubtCount;
    }

    public void setDoubtCount(int doubtCount) {
        DoubtCount = doubtCount;
    }

    private int resultCount;
    private boolean Is_Video;
    private int Mentorid;
    private int DoubtCount;
    private int PreviousRating;
    private int Boardid;
    private int classid;
    private String schoolid;
    private String asignteacherid;
    private String sectionid;
    private int roleid;
    private boolean isSectionEnabled;
    private boolean Is_School;
    private boolean Is_Doubt;
    private boolean Is_Mentor;
    private boolean Is_Dictionary;
    private boolean Is_Feed;
    private String Subjectlists;
    private String DiyModuleLists;
    private String Recentwatchedvideos;
    private String BannerList;
    private int totalCoins;
    private String trialDate;
    private String MentorVideoLink;
    private String SchoolCode;
    private PackObject pack;

    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    public boolean isIs_Video() {
        return Is_Video;
    }

    public void setIs_Video(boolean is_Video) {
        Is_Video = is_Video;
    }

    public int getMentorid() {
        return Mentorid;
    }

    public void setMentorid(int mentorid) {
        Mentorid = mentorid;
    }

    public int getPreviousRating() {
        return PreviousRating;
    }

    public void setPreviousRating(int previousRating) {
        PreviousRating = previousRating;
    }

    public int getBoardid() {
        return Boardid;
    }

    public void setBoardid(int boardid) {
        Boardid = boardid;
    }

    public int getClassid() {
        return classid;
    }

    public void setClassid(int classid) {
        this.classid = classid;
    }

    public String getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(String schoolid) {
        this.schoolid = schoolid;
    }

    public String getAsignteacherid() {
        return asignteacherid;
    }

    public void setAsignteacherid(String asignteacherid) {
        this.asignteacherid = asignteacherid;
    }

    public String getSectionid() {
        return sectionid;
    }

    public void setSectionid(String sectionid) {
        this.sectionid = sectionid;
    }

    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    public boolean isSectionEnabled() {
        return isSectionEnabled;
    }

    public void setSectionEnabled(boolean sectionEnabled) {
        isSectionEnabled = sectionEnabled;
    }

    public boolean isIs_School() {
        return Is_School;
    }

    public void setIs_School(boolean is_School) {
        Is_School = is_School;
    }

    public boolean isIs_Doubt() {
        return Is_Doubt;
    }

    public void setIs_Doubt(boolean is_Doubt) {
        Is_Doubt = is_Doubt;
    }

    public boolean isIs_Mentor() {
        return Is_Mentor;
    }

    public void setIs_Mentor(boolean is_Mentor) {
        Is_Mentor = is_Mentor;
    }

    public boolean isIs_Dictionary() {
        return Is_Dictionary;
    }

    public void setIs_Dictionary(boolean is_Dictionary) {
        Is_Dictionary = is_Dictionary;
    }

    public boolean isIs_Feed() {
        return Is_Feed;
    }

    public void setIs_Feed(boolean is_Feed) {
        Is_Feed = is_Feed;
    }

    public String getSubjectlists() {
        return Subjectlists;
    }

    public void setSubjectlists(String subjectlists) {
        Subjectlists = subjectlists;
    }

    public String getDiyModuleLists() {
        return DiyModuleLists;
    }

    public void setDiyModuleLists(String diyModuleLists) {
        DiyModuleLists = diyModuleLists;
    }

    public String getRecentwatchedvideos() {
        return Recentwatchedvideos;
    }

    public void setRecentwatchedvideos(String recentwatchedvideos) {
        Recentwatchedvideos = recentwatchedvideos;
    }

    public String getBannerList() {
        return BannerList;
    }

    public void setBannerList(String bannerList) {
        BannerList = bannerList;
    }

    public int getTotalCoins() {
        return totalCoins;
    }

    public void setTotalCoins(int totalCoins) {
        this.totalCoins = totalCoins;
    }

    public String getTrialDate() {
        return trialDate;
    }

    public void setTrialDate(String trialDate) {
        this.trialDate = trialDate;
    }

    public String getMentorVideoLink() {
        return MentorVideoLink;
    }

    public void setMentorVideoLink(String mentorVideoLink) {
        MentorVideoLink = mentorVideoLink;
    }

    public String getSchoolCode() {
        return SchoolCode;
    }

    public void setSchoolCode(String schoolCode) {
        SchoolCode = schoolCode;
    }

    public PackObject getPack() {
        return pack;
    }

    public void setPack(PackObject pack) {
        this.pack = pack;
    }
}
