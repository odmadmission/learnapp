package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/26/2018.
 */

public class LearnChapQuestionBank implements Parcelable{

    private Integer moduleIDss;
    private Boolean is_question_bookmarked;
    private Integer noofques;
    private String Question_Pdfs;
    private String Modulename;


    public Integer getModuleIDss() {
        return moduleIDss;
    }

    public void setModuleIDss(Integer moduleIDss) {
        this.moduleIDss = moduleIDss;
    }

    public Boolean getIs_question_bookmarked() {
        return is_question_bookmarked;
    }

    public void setIs_question_bookmarked(Boolean is_question_bookmarked) {
        this.is_question_bookmarked = is_question_bookmarked;
    }

    public Integer getNoofques() {
        return noofques;
    }

    public void setNoofques(Integer noofques) {
        this.noofques = noofques;
    }

    public String getQuestion_Pdfs() {
        return Question_Pdfs;
    }

    public void setQuestion_Pdfs(String question_Pdfs) {
        Question_Pdfs = question_Pdfs;
    }

    public String getModulename() {
        return Modulename;
    }

    public void setModulename(String modulename) {
        Modulename = modulename;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.moduleIDss);
        dest.writeValue(this.is_question_bookmarked);
        dest.writeValue(this.noofques);
        dest.writeString(this.Question_Pdfs);
        dest.writeString(this.Modulename);
    }

    public LearnChapQuestionBank() {
    }

    protected LearnChapQuestionBank(Parcel in) {
        this.moduleIDss = (Integer) in.readValue(Integer.class.getClassLoader());
        this.is_question_bookmarked = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.noofques = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Question_Pdfs = in.readString();
        this.Modulename = in.readString();
    }

    public static final Creator<LearnChapQuestionBank> CREATOR = new Creator<LearnChapQuestionBank>() {
        @Override
        public LearnChapQuestionBank createFromParcel(Parcel source) {
            return new LearnChapQuestionBank(source);
        }

        @Override
        public LearnChapQuestionBank[] newArray(int size) {
            return new LearnChapQuestionBank[size];
        }
    };
}
