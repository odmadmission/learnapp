package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class DiscountCouponSuccess implements Parcelable {

    protected DiscountCouponSuccess(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiscountCouponSuccess> CREATOR = new Creator<DiscountCouponSuccess>() {
        @Override
        public DiscountCouponSuccess createFromParcel(Parcel in) {
            return new DiscountCouponSuccess(in);
        }

        @Override
        public DiscountCouponSuccess[] newArray(int size) {
            return new DiscountCouponSuccess[size];
        }
    };

    public Success getSuccess() {
        return success;
    }

    public void setSuccess(Success success) {
        this.success = success;
    }

    private Success success;

    public class Success implements Parcelable{

        protected Success(Parcel in) {
        }

        public final Creator<Success> CREATOR = new Creator<Success>() {
            @Override
            public Success createFromParcel(Parcel in) {
                return new Success(in);
            }

            @Override
            public Success[] newArray(int size) {
                return new Success[size];
            }
        };

        public GetCheckoutdata getGetcheckoutdata() {
            return getcheckoutdata;
        }

        public void setGetcheckoutdata(GetCheckoutdata getcheckoutdata) {
            this.getcheckoutdata = getcheckoutdata;
        }

        private GetCheckoutdata getcheckoutdata;


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
        }
    }

    public class GetCheckoutdata implements Parcelable{
        private ArrayList<CartDataa> cartdata;
        private int Original_price;

        protected GetCheckoutdata(Parcel in) {
            Original_price = in.readInt();
            discountprice = in.readInt();
            discpercentage = in.readInt();
            payblamt = in.readInt();
            taxx = in.readInt();
            Customer_Name = in.readString();
            Mobile_Number = in.readString();
            Email_Id = in.readString();
            coupon_code = in.readString();
            cart_item = in.readInt();
            Order_Id = in.readInt();
            Order_No = in.readString();
            afterdicount = in.readInt();
            msg = in.readString();
        }

        public final Creator<GetCheckoutdata> CREATOR = new Creator<GetCheckoutdata>() {
            @Override
            public GetCheckoutdata createFromParcel(Parcel in) {
                return new GetCheckoutdata(in);
            }

            @Override
            public GetCheckoutdata[] newArray(int size) {
                return new GetCheckoutdata[size];
            }
        };

        public ArrayList<CartDataa> getCartdata() {
            return cartdata;
        }

        public void setCartdata(ArrayList<CartDataa> cartdata) {
            this.cartdata = cartdata;
        }

        public int getOriginal_price() {
            return Original_price;
        }

        public void setOriginal_price(int original_price) {
            Original_price = original_price;
        }

        public int getDiscountprice() {
            return discountprice;
        }

        public void setDiscountprice(int discountprice) {
            this.discountprice = discountprice;
        }

        public int getDiscpercentage() {
            return discpercentage;
        }

        public void setDiscpercentage(int discpercentage) {
            this.discpercentage = discpercentage;
        }

        public int getPayblamt() {
            return payblamt;
        }

        public void setPayblamt(int payblamt) {
            this.payblamt = payblamt;
        }

        public int getTaxx() {
            return taxx;
        }

        public void setTaxx(int taxx) {
            this.taxx = taxx;
        }

        public String getCustomer_Name() {
            return Customer_Name;
        }

        public void setCustomer_Name(String customer_Name) {
            Customer_Name = customer_Name;
        }

        public String getMobile_Number() {
            return Mobile_Number;
        }

        public void setMobile_Number(String mobile_Number) {
            Mobile_Number = mobile_Number;
        }

        public String getEmail_Id() {
            return Email_Id;
        }

        public void setEmail_Id(String email_Id) {
            Email_Id = email_Id;
        }

        public String getCoupon_code() {
            return coupon_code;
        }

        public void setCoupon_code(String coupon_code) {
            this.coupon_code = coupon_code;
        }

        public int getCart_item() {
            return cart_item;
        }

        public void setCart_item(int cart_item) {
            this.cart_item = cart_item;
        }

        public int getOrder_Id() {
            return Order_Id;
        }

        public void setOrder_Id(int order_Id) {
            Order_Id = order_Id;
        }

        public String getOrder_No() {
            return Order_No;
        }

        public void setOrder_No(String order_No) {
            Order_No = order_No;
        }

        public int getAfterdicount() {
            return afterdicount;
        }

        public void setAfterdicount(int afterdicount) {
            this.afterdicount = afterdicount;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        private int discountprice;
        private int discpercentage;
        private int payblamt;
        private int taxx;
        private String Customer_Name;
        private String Mobile_Number;
        private String Email_Id;
        private String coupon_code;
        private int cart_item;
        private int Order_Id;
        private String Order_No;
        private int afterdicount;
        private String msg;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(Original_price);
            dest.writeInt(discountprice);
            dest.writeInt(discpercentage);
            dest.writeInt(payblamt);
            dest.writeInt(taxx);
            dest.writeString(Customer_Name);
            dest.writeString(Mobile_Number);
            dest.writeString(Email_Id);
            dest.writeString(coupon_code);
            dest.writeInt(cart_item);
            dest.writeInt(Order_Id);
            dest.writeString(Order_No);
            dest.writeInt(afterdicount);
            dest.writeString(msg);
        }
    }

    public class CartDataa implements Parcelable{

        private String Package_Image;
        private String Package_Name;
        private int Validity;
        private int Subscripttion_Limit;
        private int Package_ID;
        private int Price;

        protected CartDataa(Parcel in) {
            Package_Image = in.readString();
            Package_Name = in.readString();
            Validity = in.readInt();
            Subscripttion_Limit = in.readInt();
            Package_ID = in.readInt();
            Price = in.readInt();
            Discounted_Price = in.readInt();
            Package_Price = in.readInt();
            Is_Offline = in.readByte() != 0;
            Cart_ID = in.readInt();
            Order_Id = in.readInt();
            Order_No = in.readString();
            discountpercentage = in.readInt();
        }

        public final Creator<CartDataa> CREATOR = new Creator<CartDataa>() {
            @Override
            public CartDataa createFromParcel(Parcel in) {
                return new CartDataa(in);
            }

            @Override
            public CartDataa[] newArray(int size) {
                return new CartDataa[size];
            }
        };

        public String getPackage_Image() {
            return Package_Image;
        }

        public void setPackage_Image(String package_Image) {
            Package_Image = package_Image;
        }

        public String getPackage_Name() {
            return Package_Name;
        }

        public void setPackage_Name(String package_Name) {
            Package_Name = package_Name;
        }

        public int getValidity() {
            return Validity;
        }

        public void setValidity(int validity) {
            Validity = validity;
        }

        public int getSubscripttion_Limit() {
            return Subscripttion_Limit;
        }

        public void setSubscripttion_Limit(int subscripttion_Limit) {
            Subscripttion_Limit = subscripttion_Limit;
        }

        public int getPackage_ID() {
            return Package_ID;
        }

        public void setPackage_ID(int package_ID) {
            Package_ID = package_ID;
        }

        public int getPrice() {
            return Price;
        }

        public void setPrice(int price) {
            Price = price;
        }

        public int getDiscounted_Price() {
            return Discounted_Price;
        }

        public void setDiscounted_Price(int discounted_Price) {
            Discounted_Price = discounted_Price;
        }

        public int getPackage_Price() {
            return Package_Price;
        }

        public void setPackage_Price(int package_Price) {
            Package_Price = package_Price;
        }

        public boolean isIs_Offline() {
            return Is_Offline;
        }

        public void setIs_Offline(boolean is_Offline) {
            Is_Offline = is_Offline;
        }

        public int getCart_ID() {
            return Cart_ID;
        }

        public void setCart_ID(int cart_ID) {
            Cart_ID = cart_ID;
        }

        public int getOrder_Id() {
            return Order_Id;
        }

        public void setOrder_Id(int order_Id) {
            Order_Id = order_Id;
        }

        public String getOrder_No() {
            return Order_No;
        }

        public void setOrder_No(String order_No) {
            Order_No = order_No;
        }

        public int getDiscountpercentage() {
            return discountpercentage;
        }

        public void setDiscountpercentage(int discountpercentage) {
            this.discountpercentage = discountpercentage;
        }

        private int Discounted_Price;
        private int Package_Price;
        private boolean Is_Offline;
        private int Cart_ID;
        private int Order_Id;
        private String Order_No;
        private int discountpercentage;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(Package_Image);
            dest.writeString(Package_Name);
            dest.writeInt(Validity);
            dest.writeInt(Subscripttion_Limit);
            dest.writeInt(Package_ID);
            dest.writeInt(Price);
            dest.writeInt(Discounted_Price);
            dest.writeInt(Package_Price);
            dest.writeByte((byte) (Is_Offline ? 1 : 0));
            dest.writeInt(Cart_ID);
            dest.writeInt(Order_Id);
            dest.writeString(Order_No);
            dest.writeInt(discountpercentage);
        }
    }
}
