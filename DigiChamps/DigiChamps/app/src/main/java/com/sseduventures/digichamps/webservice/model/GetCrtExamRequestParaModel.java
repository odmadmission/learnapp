package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 8/28/2018.
 */

public class GetCrtExamRequestParaModel implements Parcelable {

    String id;

    public GetCrtExamRequestParaModel() {

    }

    public static final Creator<GetCrtExamRequestParaModel> CREATOR = new Creator<GetCrtExamRequestParaModel>() {
        @Override
        public GetCrtExamRequestParaModel createFromParcel(Parcel in) {
            return new GetCrtExamRequestParaModel();
        }

        @Override
        public GetCrtExamRequestParaModel[] newArray(int size) {
            return new GetCrtExamRequestParaModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    String eid;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(eid);
    }
}
