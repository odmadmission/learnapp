package com.sseduventures.digichamps.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FontManage;


/**
 * Created by user on 22-01-2018.
 */

public class StudyMaterialDetailActivity extends FormActivity {

    TextView tv_topic_name,tv_subject,tv_notice,pdfOpener;
    RelativeLayout rel_click;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studymaterial_detail);

        setModuleName(LogEventUtil.EVENT_studymaterial_details);
        logEvent(LogEventUtil.KEY_studymaterial_details,LogEventUtil.EVENT_studymaterial_details);

        tv_topic_name = findViewById(R.id.tv_topic_name);
        tv_subject = findViewById(R.id.tv_subject);
        tv_notice = findViewById(R.id.tv_notice);
        rel_click = findViewById(R.id.rel_click);
        pdfOpener = findViewById(R.id.pdfOpener);
        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        FontManage.setFontMeiryo(this,tv_notice);

        if(getIntent() != null){

            String topic_name="<b>Topic Name:</b> ";
            String subject="<b>Subject:</b> ";
            if(!getIntent().getStringExtra("topic").equals("")){
                topic_name = topic_name+""+getIntent().getStringExtra("topic").substring(0,1).toUpperCase() + getIntent().getStringExtra("topic").substring(1);
            }

            if(!getIntent().getStringExtra("subject").equals("")){
                subject = subject+""+ getIntent().getStringExtra("subject").substring(0,1).toUpperCase() + getIntent().getStringExtra("subject").substring(1);
            }


            tv_topic_name.setText(Html.fromHtml(topic_name));
            tv_subject.setText(Html.fromHtml(subject));
            tv_notice.setText(getIntent().getStringExtra("studyMaterial"));
            final String fileURL =  getIntent().getStringExtra("fileURL");


            if(fileURL.equals("")){
                pdfOpener.setText("No PDF Available");
            }else{
                pdfOpener.setText("Click To Open File");
            }

            rel_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(fileURL.equals("")){
                        showToast("No PDF to download");
                    }else{
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(Constants.IMAGE_URL+fileURL));
                        startActivity(browserIntent);
                    }



                }
            });

        }

    }

}
