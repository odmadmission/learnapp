package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/28/2018.
 */

public class DictonaryPostRequest implements Parcelable {


    private String word;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.word);
    }

    public DictonaryPostRequest() {
    }

    protected DictonaryPostRequest(Parcel in) {
        this.word = in.readString();
    }

    public static final Creator<DictonaryPostRequest> CREATOR = new Creator<DictonaryPostRequest>() {
        @Override
        public DictonaryPostRequest createFromParcel(Parcel source) {
            return new DictonaryPostRequest(source);
        }

        @Override
        public DictonaryPostRequest[] newArray(int size) {
            return new DictonaryPostRequest[size];
        }
    };
}
