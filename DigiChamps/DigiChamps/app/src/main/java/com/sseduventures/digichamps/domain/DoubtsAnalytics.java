package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class DoubtsAnalytics implements Parcelable{
        private int DoubtsAsked;
        private int DoubtsAnswered;
        private int DoubtsRejected;
        private String SubjectId;
        private String Subject;

        public int getDoubtsAsked() {
            return DoubtsAsked;
        }

        public void setDoubtsAsked(int doubtsAsked) {
            DoubtsAsked = doubtsAsked;
        }

        public int getDoubtsAnswered() {
            return DoubtsAnswered;
        }

        public void setDoubtsAnswered(int doubtsAnswered) {
            DoubtsAnswered = doubtsAnswered;
        }

        public int getDoubtsRejected() {
            return DoubtsRejected;
        }

        public void setDoubtsRejected(int doubtsRejected) {
            DoubtsRejected = doubtsRejected;
        }

        public String getSubjectId() {
            return SubjectId;
        }

        public void setSubjectId(String subjectId) {
            SubjectId = subjectId;
        }

        public String getSubject() {
            return Subject;
        }

        public void setSubject(String subject) {
            Subject = subject;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.DoubtsAsked);
            dest.writeInt(this.DoubtsAnswered);
            dest.writeInt(this.DoubtsRejected);
            dest.writeString(this.SubjectId);
            dest.writeString(this.Subject);
        }

        public DoubtsAnalytics() {
        }

        protected DoubtsAnalytics(Parcel in) {
            this.DoubtsAsked = in.readInt();
            this.DoubtsAnswered = in.readInt();
            this.DoubtsRejected = in.readInt();
            this.SubjectId = in.readString();
            this.Subject = in.readString();
        }

        public  final Creator<DoubtsAnalytics> CREATOR = new Creator<DoubtsAnalytics>() {
            @Override
            public DoubtsAnalytics createFromParcel(Parcel source) {
                return new DoubtsAnalytics(source);
            }

            @Override
            public DoubtsAnalytics[] newArray(int size) {
                return new DoubtsAnalytics[size];
            }
        };
    }