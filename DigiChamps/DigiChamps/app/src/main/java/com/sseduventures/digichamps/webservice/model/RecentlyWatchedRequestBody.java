package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 8/15/2018.
 */

public class RecentlyWatchedRequestBody implements Parcelable {

    String redgId;

    public RecentlyWatchedRequestBody() {

    }

    public static final Creator<RecentlyWatchedRequestBody> CREATOR = new Creator<RecentlyWatchedRequestBody>() {
        @Override
        public RecentlyWatchedRequestBody createFromParcel(Parcel in) {
            return new RecentlyWatchedRequestBody();
        }

        @Override
        public RecentlyWatchedRequestBody[] newArray(int size) {
            return new RecentlyWatchedRequestBody[size];
        }
    };

    public String getRedgId() {
        return redgId;
    }

    public void setRedgId(String redgId) {
        this.redgId = redgId;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    String moduleId;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(redgId);
        parcel.writeString(moduleId);
    }
}
