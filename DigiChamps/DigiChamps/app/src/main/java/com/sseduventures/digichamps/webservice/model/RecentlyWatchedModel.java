package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 8/15/2018.
 */

public class RecentlyWatchedModel implements Parcelable {

    public RecenlySuccess getSuccess() {
        return success;
    }

    public void setSuccess(RecenlySuccess success) {
        this.success = success;
    }

    private RecenlySuccess success;

    protected RecentlyWatchedModel(Parcel in) {
    }

    public static final Creator<RecentlyWatchedModel> CREATOR = new Creator<RecentlyWatchedModel>() {
        @Override
        public RecentlyWatchedModel createFromParcel(Parcel in) {
            return new RecentlyWatchedModel(in);
        }

        @Override
        public RecentlyWatchedModel[] newArray(int size) {
            return new RecentlyWatchedModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }

    public class RecenlySuccess implements Parcelable {
        protected RecenlySuccess(Parcel in) {
            message = in.readString();
        }

        public final Creator<RecenlySuccess> CREATOR = new Creator<RecenlySuccess>() {
            @Override
            public RecenlySuccess createFromParcel(Parcel in) {
                return new RecenlySuccess(in);
            }

            @Override
            public RecenlySuccess[] newArray(int size) {
                return new RecenlySuccess[size];
            }
        };

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        private String message;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(message);
        }
    }
}
