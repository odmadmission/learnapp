package com.sseduventures.digichamps.adapter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.PackageActivityNew;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.config.AppController;
import com.sseduventures.digichamps.domain.BookmarkStudyNotesList;
import com.sseduventures.digichamps.fragment.Fragment_Bookmarks2;
import com.sseduventures.digichamps.helper.RegPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



public class Bookmarks_frag2_adapter extends RecyclerView.Adapter<Bookmarks_frag2_adapter.MyLearnViewHolder> {
    public static ArrayList<BookmarkStudyNotesList> dataSet = new ArrayList<>();

    public static int position;
    static CardView container, container1;
    String sub_name,user_id,resp,error;
    View view;
    String TAG= "Videos";
    private Fragment_Bookmarks2 fragment;
    private Context context;
    Handler handler = new Handler();
    int module_id;
    private Typeface tf;

    public Bookmarks_frag2_adapter(ArrayList<BookmarkStudyNotesList> data, String sub_name, Context context, Fragment_Bookmarks2 fragment) {
        this.context = context;
        this.dataSet = data;

        this.sub_name = sub_name;
        this.fragment = fragment;
        tf = Typeface.createFromAsset(context.getAssets(),"fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");
    }
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static boolean checkImageResource(Context ctx, ImageView imageView,int imageResource) {
        boolean result = false;

        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }

            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }

        return result;
    }


    @Override
    public MyLearnViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_bookmark_frag2, parent, false);


        MyLearnViewHolder myViewHolder = new MyLearnViewHolder(itemView);

        return myViewHolder;

    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MyLearnViewHolder holder, final int listPosition) {
        TextView chap_names = holder.chap_names;
        chap_names.setText(dataSet.get(holder.getAdapterPosition()).getModuleName());
        container1 = holder.container;

        holder.container.setOnClickListener(onClickListener(listPosition));

        SharedPreferences preference = context.getApplicationContext().getSharedPreferences("user_data", context.getApplicationContext().MODE_PRIVATE);
        String flag = preference.getString("flag", null);
        user_id = String.valueOf(RegPrefManager.getInstance(context).getRegId());

        holder.bookmark_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                module_id = dataSet.get(position).getModule_Id();

                fragment.setUnsetBookMark(String.valueOf(module_id),"3");

            }
            });
        }



    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataSet.get(position).getIs_Expire().equalsIgnoreCase("true") &&
                        !dataSet.get(position).isIs_Free()) {

                    final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialogTheme_buy);
                    LayoutInflater factory = LayoutInflater.from(context);
                    final View vieww = factory.inflate(R.layout.dialog_buy, null);
                    Button ok = (Button) vieww.findViewById(R.id.button2);
                    final AlertDialog alertDialog = builder.create();
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            Intent i = new Intent(context, PackageActivityNew.class);
                            Activity activity = (Activity) context;
                            context.startActivity(i);


                        }
                    });

                    builder.setView(vieww);
                    builder.setCancelable(true);
                    builder.show();
                } else {
                    String PDF_URL =dataSet.get(position).getStudynote();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(PDF_URL));
                    context.startActivity(browserIntent);

                }
            }
        };
    }

    public static class MyLearnViewHolder extends RecyclerView.ViewHolder {
        public TextView chap_names;
        public TextView title;
        public ImageView play,bookmark_image;
        CardView container;


        public MyLearnViewHolder(View itemView) {
            super(itemView);

            this.chap_names = (TextView) itemView.findViewById(R.id.chap);
            container = (CardView) itemView.findViewById(R.id.card_sn);
            bookmark_image = (ImageView)itemView.findViewById(R.id.bookmark) ;

        }
    }

    public  void setPlaying(boolean isPlaying){
        if(isPlaying){
        }else{
        }
    }



//


}
