package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class BoardSchoolResponse implements Parcelable {


    private List<BoardClass> boardList;
    private List<BoardSchoolName> schoolInfoList;

    public List<String> getSectionList() {
        return sectionList;
    }

    public void setSectionList(List<String> sectionList) {
        this.sectionList = sectionList;
    }

    public List<String> sectionList =null;

    protected BoardSchoolResponse(Parcel in) {
        boardList = in.createTypedArrayList(BoardClass.CREATOR);
        schoolInfoList = in.createTypedArrayList(BoardSchoolName.CREATOR);
    }

    public static final Creator<BoardSchoolResponse> CREATOR = new Creator<BoardSchoolResponse>() {
        @Override
        public BoardSchoolResponse createFromParcel(Parcel in) {
            return new BoardSchoolResponse(in);
        }

        @Override
        public BoardSchoolResponse[] newArray(int size) {
            return new BoardSchoolResponse[size];
        }
    };

    public List<BoardClass> getBoardList() {
        return boardList;
    }

    public void setBoardList(List<BoardClass> boardList) {
        this.boardList = boardList;
    }

    public List<BoardSchoolName> getSchoolInfoList() {
        return schoolInfoList;
    }

    public void setSchoolInfoList(List<BoardSchoolName> schoolInfoList) {
        this.schoolInfoList = schoolInfoList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(boardList);
        parcel.writeTypedList(schoolInfoList);
    }
}
