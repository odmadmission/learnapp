package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 8/28/2018.
 */

public class PackObject implements Parcelable{

    private int PackageID;
    private String PackageName;
    private String expire;


    public int getPackageID() {
        return PackageID;
    }

    public void setPackageID(int packageID) {
        PackageID = packageID;
    }

    public String getPackageName() {
        return PackageName;
    }

    public void setPackageName(String packageName) {
        PackageName = packageName;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.PackageID);
        dest.writeString(this.PackageName);
        dest.writeString(this.expire);
    }

    public PackObject() {
    }

    protected PackObject(Parcel in) {
        this.PackageID = in.readInt();
        this.PackageName = in.readString();
        this.expire = in.readString();
    }

    public static final Creator<PackObject> CREATOR = new Creator<PackObject>() {
        @Override
        public PackObject createFromParcel(Parcel source) {
            return new PackObject(source);
        }

        @Override
        public PackObject[] newArray(int size) {
            return new PackObject[size];
        }
    };
}
