package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sseduventures.digichamps.Model.DataModel_OrderDetails;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.activities.NewNotificationActivity;
import com.sseduventures.digichamps.adapter.OrderDetails_Adapter;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;

import java.util.ArrayList;
import java.util.StringTokenizer;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.model.OrderDetailsModel;


public class Order_details_page extends FormActivity implements ServiceReceiver.Receiver {

    private Toolbar toolbar;
    private RecyclerView recyclerView_package;
    private OrderDetails_Adapter adapter;
    private String id, user_id;
    private ImageButton notification, cart;
    private ImageView back_arrow;
    private CoordinatorLayout orderdetails_Layout;
    private ArrayList<String> chapterList;
    private ArrayList<DataModel_OrderDetails> order_details_list;
    private RecyclerView.LayoutManager mLayoutManager;
    private String resp, error, TAG = "Orders";
    private SpotsDialog dialog;
    private TextView total_tv_price, total_tv_tax, total_tv_grand, total_tv_saving, tv_order_nmbr;
    private String rs = "RS. ", activity;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_order__details);
        setModuleName(LogEventUtil.EVENT_ORDER_DETAILS);
        logEvent(LogEventUtil.Key_Order_details,LogEventUtil.EVENT_ORDER_DETAILS);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        init();

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Order_details_page.this,
                        NewNotificationActivity.class));



            }
        });

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (activity.equalsIgnoreCase("Notify")) {
                    startActivity(new Intent(Order_details_page.this, NewDashboardActivity.class));

                    finish();

                } else if (activity.equalsIgnoreCase("Order Lists")) {
                    startActivity(new Intent(Order_details_page.this, Order.class));

                    finish();
                }
            }
        });


        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Order_details_page.this, Cart_Activity.class));



            }
        });


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        if (AppUtil.isInternetConnected(this)) {


            showDialog(null,"Please wait");

            Bundle bundle = new Bundle();
            bundle.putString("id", id);

            NetworkService.startOrderDetailsData(Order_details_page.this,
                    mServiceReceiver, bundle);

        } else {
            Intent intent1 = new Intent(Order_details_page.this, Internet_Activity.class);
            startActivity(intent1);

            finish();

        }


        recyclerView_package.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(Order_details_page.this);
        recyclerView_package.setLayoutManager(mLayoutManager);
        recyclerView_package.setNestedScrollingEnabled(false);


    }


    public void ReturnHome(View view) {
        finish();
    }


    public void init() {

        Intent intent = getIntent();
        id = intent.getIntExtra("id", 0) + "";
        activity = intent.getStringExtra("activity");


        long regid = RegPrefManager.getInstance(this).getRegId();
        user_id = String.valueOf(regid);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        recyclerView_package = (RecyclerView) findViewById(R.id.recycler_package);

        total_tv_price = (TextView) findViewById(R.id.tv_num_totalprice);
        total_tv_tax = (TextView) findViewById(R.id.tv_num_tax);
        total_tv_grand = (TextView) findViewById(R.id.tv_num_grand);
        total_tv_saving = (TextView) findViewById(R.id.tv_sav_text);
        tv_order_nmbr = (TextView) findViewById(R.id.order_nmbr_text);

        back_arrow = (ImageView) findViewById(R.id.orders_back_arrow);

        cart = (ImageButton) findViewById(R.id.cart_btn);
        notification = (ImageButton) findViewById(R.id.notif_btn);

        orderdetails_Layout = (CoordinatorLayout) findViewById(R.id.orderdetails_layout);
        orderdetails_Layout.setVisibility(View.GONE);

        dialog = new SpotsDialog(this, "FUN + EDUCATION", R.style.Custom);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (activity.equalsIgnoreCase("Notify")) {
            startActivity(new Intent(Order_details_page.this, NewDashboardActivity.class));

            finish();
        } else if (activity.equalsIgnoreCase("Order Lists")) {
            startActivity(new Intent(Order_details_page.this, Order.class));

            finish();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
        hideDialog();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(Order_details_page.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(Order_details_page.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(Order_details_page.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:




                OrderDetailsModel orderDetailsModel = resultData.getParcelable(
                        IntentHelper.RESULT_DATA
                );

                orderdetails_Layout.setVisibility(View.VISIBLE);

                if (orderDetailsModel == null) {

                } else {

                    ArrayList<String> sub = new ArrayList<>();
                    chapterList = new ArrayList<>();
                    order_details_list = new ArrayList<>();

                    for (int i = 0; i < orderDetailsModel.getSuccess().getOrder().size(); i++) {


                        for (int j = 0; j < orderDetailsModel.getSuccess().getOrder().get(i).getSubjects().size(); j++) {
                            sub.add(orderDetailsModel.getSuccess().getOrder().get(i).getSubjects().get(j).getSubject_Name());
                        }


                        String Chapters = String.valueOf(orderDetailsModel.getSuccess().getOrder().get(i).getChapters());
                        String Validity = String.valueOf(orderDetailsModel.getSuccess().getOrder().get(i).getValidity());
                        String Val_Upto = String.valueOf(orderDetailsModel.getSuccess().getOrder().get(i).getExpiry_Date());

                        String Package_Name = orderDetailsModel.getSuccess().getOrder().get(i).getPackage_Name();

                        String Order_Date = orderDetailsModel.getSuccess().getOrder_date();

                        StringTokenizer tokens = new StringTokenizer(Order_Date, "T");
                        String date = tokens.nextToken();

                        StringTokenizer token = new StringTokenizer(Val_Upto, "T");
                        String date_exp = token.nextToken();

                        if (sub.size() == 1) {
                            order_details_list.add(new DataModel_OrderDetails("Date: " + date, Package_Name, sub.get(0), Chapters + " Chapters",
                                    Validity + " Days", date_exp));
                        } else {
                            order_details_list.add(new DataModel_OrderDetails("Date: " + date, Package_Name, sub.get(0) + " & " + sub.get(1), Chapters + " Chapters",
                                    Validity + " Days", date_exp));
                        }
                    }

                    String Order_No = orderDetailsModel.getSuccess().getOrder_No();
                    String Tax_Amt = dialog.convert(String.valueOf(orderDetailsModel.getSuccess().getTax_Amt()));
                    String Total = dialog.convert(String.valueOf(orderDetailsModel.getSuccess().getTotal()));
                    String Total_Saving = dialog.convert(String.valueOf(orderDetailsModel.getSuccess().getTotal_savings()));
                    String Grand_Total = dialog.convert(String.valueOf(orderDetailsModel.getSuccess().getGrand_Total()));

                    String total_p = rs + Total;
                    String total_tax = rs + Tax_Amt;
                    String total_grand = rs + Grand_Total;
                    String total_sav = rs + Total_Saving;

                    total_tv_price.setText(total_p);
                    total_tv_tax.setText(total_tax);
                    total_tv_grand.setText(total_grand);
                    total_tv_saving.setText(total_sav);
                    tv_order_nmbr.setText(Order_No);

                    adapter = new OrderDetails_Adapter(order_details_list, Order_details_page.this);
                    adapter.notifyDataSetChanged();
                    recyclerView_package.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    hideDialog();
                }

        }
    }
}