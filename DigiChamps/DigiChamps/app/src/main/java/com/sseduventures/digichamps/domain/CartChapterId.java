package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/5/2018.
 */

public class CartChapterId implements Parcelable
{
    private int chpter_id;

    public int getChpter_id() {
        return chpter_id;
    }

    public void setChpter_id(int chpter_id) {
        this.chpter_id = chpter_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.chpter_id);
    }

    public CartChapterId() {
    }

    protected CartChapterId(Parcel in) {
        this.chpter_id = in.readInt();
    }

    public static final Creator<CartChapterId> CREATOR = new Creator<CartChapterId>() {
        @Override
        public CartChapterId createFromParcel(Parcel source) {
            return new CartChapterId(source);
        }

        @Override
        public CartChapterId[] newArray(int size) {
            return new CartChapterId[size];
        }
    };
}
