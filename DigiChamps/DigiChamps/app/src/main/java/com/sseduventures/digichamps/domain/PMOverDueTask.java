package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/25/2018.
 */

public class PMOverDueTask implements Parcelable {

    private List<PMOverduetaskDataList> taskDataList;


    public List<PMOverduetaskDataList> getTaskDataList() {
        return taskDataList;
    }

    public void setTaskDataList(List<PMOverduetaskDataList> taskDataList) {
        this.taskDataList = taskDataList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.taskDataList);
    }

    public PMOverDueTask() {
    }

    protected PMOverDueTask(Parcel in) {
        this.taskDataList = in.createTypedArrayList(PMOverduetaskDataList.CREATOR);
    }

    public static final Creator<PMOverDueTask> CREATOR = new Creator<PMOverDueTask>() {
        @Override
        public PMOverDueTask createFromParcel(Parcel source) {
            return new PMOverDueTask(source);
        }

        @Override
        public PMOverDueTask[] newArray(int size) {
            return new PMOverDueTask[size];
        }
    };
}
