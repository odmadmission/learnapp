package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/28/2018.
 */

public class DictonaryResponse implements Parcelable{

    private DictonarySuccess Success;

    public DictonarySuccess getSuccess() {
        return Success;
    }

    public void setSuccess(DictonarySuccess success) {
        Success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) this.Success, flags);
    }

    public DictonaryResponse() {
    }

    protected DictonaryResponse(Parcel in) {
        this.Success = in.readParcelable(DictonarySuccess.class.getClassLoader());
    }

    public static final Creator<DictonaryResponse> CREATOR = new Creator<DictonaryResponse>() {
        @Override
        public DictonaryResponse createFromParcel(Parcel source) {
            return new DictonaryResponse(source);
        }

        @Override
        public DictonaryResponse[] newArray(int size) {
            return new DictonaryResponse[size];
        }
    };
}
