package com.sseduventures.digichamps.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.Bookmarks_frag3_adapter;
import com.sseduventures.digichamps.domain.BookmarkQuestionList;
import com.sseduventures.digichamps.domain.BookmarkQuestionResponse;
import com.sseduventures.digichamps.domain.SetBookmarkResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;

/**
 * Created by Tech_1 on 12/30/2017.
 */

public class Fragment_Bookmarks3 extends android.support.v4.app.Fragment
implements ServiceReceiver.Receiver{
    private RecyclerView recyclerView;
    ArrayList<BookmarkQuestionList> list;
    private Bookmarks_frag3_adapter mAdapter;
    SpotsDialog pDialog;

    private String sub_name;
    private ArrayList<String> pdfs_list;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String chapter_id,prtTest = "false";
    private long User_Id;
    private TextView no_studynotes;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private ShimmerFrameLayout mShimmerViewContainer;



    public Fragment_Bookmarks3() {
            // Required empty public constructor
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View view = inflater.inflate(R.layout.bookmarks_frag3, container, false);
            if(getActivity() instanceof FormActivity)
            {
                ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_bookmark_qn);
                ((FormActivity)getActivity()).logEvent(LogEventUtil.KEY_bookmark_qn,
                        LogEventUtil.EVENT_bookmark_qn);
            }
            init(view);

            //checking for internet connection
            if (AppUtil.isInternetConnected(getActivity())) {
                User_Id = RegPrefManager.getInstance(getActivity()).getRegId();
                //getQuestionBanks();
            } else {
                // redirect to No internet activity
                Intent i = new Intent(getActivity(), Internet_Activity.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
            }
            return view;

        }
    private void init(View view) {

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.quesbank_recycler_view);
        recyclerView.setHasFixedSize(true);
        no_studynotes = view.findViewById(R.id.no_studynotes);

        no_studynotes.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        //list
        list = new ArrayList<>();
        pdfs_list = new ArrayList<>();

        //Progress dialog initialisation
        pDialog = new SpotsDialog(getActivity(), "Fun+Education", R.style.Custom);
        mAdapter = new Bookmarks_frag3_adapter(list,sub_name,getActivity(),Fragment_Bookmarks3.this);
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);


    }


    public void setVisibility() {

        no_studynotes.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResume() {
        getBookmarkQuestion();
        super.onResume();
    }

    public void setUnsetBookMark(String dataid,String dataType){
        if (AppUtil.isInternetConnected(getActivity())){

            Bundle bun = new Bundle();
            bun.putString("dataType",dataType);
            bun.putString("dataId",dataid);

            NetworkService.startActionGetSetUsetBookmark(getActivity(),mServiceReceiver,bun);
        }else{
            Intent in = new Intent(getActivity(), Internet_Activity.class);
            startActivity(in);
        }
    }

    private void getBookmarkQuestion(){
        if (AppUtil.isInternetConnected(getActivity())){

            NetworkService.startActionGetBookmarkQuestion(getActivity(),mServiceReceiver);


        }else{
            Intent in = new Intent(getActivity(),Internet_Activity.class);
            startActivity(in);
        }

    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);



        switch(resultCode){

            case ResponseCodes.SUCCESS:
                no_studynotes.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                final BookmarkQuestionResponse success = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if(success!=null &&
                        success.getSuccessBookmarkedQuestion()!=null &&
                        success.getSuccessBookmarkedQuestion().getQuestionList()!=null &&
                        success.getSuccessBookmarkedQuestion().getQuestionList().size()>0){



                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            list.clear();
                            list.addAll(success.getSuccessBookmarkedQuestion().getQuestionList());
                            mAdapter.notifyDataSetChanged();

                        }
                    },1000);


                }


                break;

            case ResponseCodes.BOOKMARKED:
                SetBookmarkResponse msuccessmm = resultData.getParcelable(IntentHelper.RESULT_DATA);
                Toast.makeText(getActivity(), ""+msuccessmm.getSuccess().getMessage(), Toast.LENGTH_SHORT).show();
                break;
            case ResponseCodes.UNBOOKMARKED:
                SetBookmarkResponse mmsuccessss =
                        resultData.getParcelable(IntentHelper.RESULT_DATA);

                ArrayList<BookmarkQuestionList> l=new ArrayList<>();
                for(int i=0;i<list.size();i++)
                {
                    String id=resultData.getString("dataId");
                    //Toast.makeText(getActivity(), ""+list.get(i).getModule_Id(), Toast.LENGTH_SHORT).show();

                    if(id.equals(String.valueOf(list.get(i).getModule_Id())))
                    {

                    }
                    else {
                        l.add(list.get(i));
                    }
                }
                Toast.makeText(getActivity(), ""+mmsuccessss.getSuccess().getMessage(), Toast.LENGTH_SHORT).show();

                list.clear();
                list.addAll(l);
                mAdapter.notifyDataSetChanged();

                //Toast.makeText(getActivity(), ""+list.size(), Toast.LENGTH_SHORT).show();
                if(list.size()==0)
                {
                    no_studynotes.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
                break;

            case ResponseCodes.NOLIST:
                no_studynotes.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);

                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(),ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(),ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(),Internet_Activity.class);
                startActivity(intent);
                break;

            case ResponseCodes.SERVER_ERROR:
                break;


        }


    }

    @Override
    public void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }
}



