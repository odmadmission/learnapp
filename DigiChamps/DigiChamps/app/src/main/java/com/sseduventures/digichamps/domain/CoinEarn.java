package com.sseduventures.digichamps.domain;

import java.util.Date;

/**
 * Created by NISHIKANT on 8/12/2018.
 */

public class CoinEarn
{
        private Long coinId;

        private Long regdId;

        private Long coinType;

        private Long typeId;

        private Date insertedOn;

        private boolean active;

        private int coins;



        private int rowId;


        public int getRowId() {
            return rowId;
        }

        public void setRowId(int rowId) {
            this.rowId = rowId;
        }

    public Long getCoinId() {
        return coinId;
    }

    public void setCoinId(Long coinId) {
        this.coinId = coinId;
    }

    public Long getRegdId() {
        return regdId;
    }

    public void setRegdId(Long regdId) {
        this.regdId = regdId;
    }

    public Long getCoinType() {
        return coinType;
    }

    public void setCoinType(Long coinType) {
        this.coinType = coinType;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Date getInsertedOn() {
        return insertedOn;
    }

    public void setInsertedOn(Date insertedOn) {
        this.insertedOn = insertedOn;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }
}
