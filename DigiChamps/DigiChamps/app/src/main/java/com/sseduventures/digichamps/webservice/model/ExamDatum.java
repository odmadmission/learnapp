package com.sseduventures.digichamps.webservice.model;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ExamDatum implements Parcelable
{

private int RowID;
private int question_id;
private int Board_Id;

    protected ExamDatum(Parcel in) {
        RowID = in.readInt();
        question_id = in.readInt();
        Board_Id = in.readInt();
        Class_Id = in.readInt();
        Subject_Id = in.readInt();
        ch_id = in.readInt();
        topicId = in.readInt();
        power_id = in.readInt();
        question = in.readString();
        Qustion_Desc = in.readString();
        Options = in.createTypedArrayList(Option.CREATOR);
        Image=in.createTypedArrayList(ImageModel.CREATOR);
    }

    public static final Creator<ExamDatum> CREATOR = new Creator<ExamDatum>() {
        @Override
        public ExamDatum createFromParcel(Parcel in) {
            return new ExamDatum(in);
        }

        @Override
        public ExamDatum[] newArray(int size) {
            return new ExamDatum[size];
        }
    };

    public int getRowID() {
        return RowID;
    }

    public void setRowID(int rowID) {
        RowID = rowID;
    }

    public int getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public int getBoard_Id() {
        return Board_Id;
    }

    public void setBoard_Id(int board_Id) {
        Board_Id = board_Id;
    }

    public int getClass_Id() {
        return Class_Id;
    }

    public void setClass_Id(int class_Id) {
        Class_Id = class_Id;
    }

    public int getSubject_Id() {
        return Subject_Id;
    }

    public void setSubject_Id(int subject_Id) {
        Subject_Id = subject_Id;
    }

    public int getCh_id() {
        return ch_id;
    }

    public void setCh_id(int ch_id) {
        this.ch_id = ch_id;
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public int getPower_id() {
        return power_id;
    }

    public void setPower_id(int power_id) {
        this.power_id = power_id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQustion_Desc() {
        return Qustion_Desc;
    }

    public void setQustion_Desc(String qustion_Desc) {
        Qustion_Desc = qustion_Desc;
    }

    public List<Option> getOptions() {
        return Options;
    }

    public void setOptions(List<Option> options) {
        Options = options;
    }

    public List<ImageModel> getImage() {
        return Image;
    }

    public void setImage(List<ImageModel> image) {
        Image = image;
    }

    private int Class_Id;
private int Subject_Id;
private int ch_id;
private int topicId;
private int power_id;
private String question;
private String Qustion_Desc;
private List<Option> Options = null;
private List<ImageModel> Image = null;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(RowID);
        dest.writeInt(question_id);
        dest.writeInt(Board_Id);
        dest.writeInt(Class_Id);
        dest.writeInt(Subject_Id);
        dest.writeInt(ch_id);
        dest.writeInt(topicId);
        dest.writeInt(power_id);
        dest.writeString(question);
        dest.writeString(Qustion_Desc);
        dest.writeTypedList(Options);
        dest.writeTypedList(Image);
    }
}