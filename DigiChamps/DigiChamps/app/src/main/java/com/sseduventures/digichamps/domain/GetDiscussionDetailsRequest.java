package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/13/2018.
 */

public class GetDiscussionDetailsRequest implements Parcelable{

    private String SchoolId;
    private int ClassId;
    private int DiscussionId;
    private int PageSize;
    private int StartIndex;

    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public int getClassId() {
        return ClassId;
    }

    public void setClassId(int classId) {
        ClassId = classId;
    }

    public int getDiscussionId() {
        return DiscussionId;
    }

    public void setDiscussionId(int discussionId) {
        DiscussionId = discussionId;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public int getStartIndex() {
        return StartIndex;
    }

    public void setStartIndex(int startIndex) {
        StartIndex = startIndex;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.SchoolId);
        dest.writeInt(this.ClassId);
        dest.writeInt(this.DiscussionId);
        dest.writeInt(this.PageSize);
        dest.writeInt(this.StartIndex);
    }

    public GetDiscussionDetailsRequest() {
    }

    protected GetDiscussionDetailsRequest(Parcel in) {
        this.SchoolId = in.readString();
        this.ClassId = in.readInt();
        this.DiscussionId = in.readInt();
        this.PageSize = in.readInt();
        this.StartIndex = in.readInt();
    }

    public static final Creator<GetDiscussionDetailsRequest> CREATOR = new Creator<GetDiscussionDetailsRequest>() {
        @Override
        public GetDiscussionDetailsRequest createFromParcel(Parcel source) {
            return new GetDiscussionDetailsRequest(source);
        }

        @Override
        public GetDiscussionDetailsRequest[] newArray(int size) {
            return new GetDiscussionDetailsRequest[size];
        }
    };
}
