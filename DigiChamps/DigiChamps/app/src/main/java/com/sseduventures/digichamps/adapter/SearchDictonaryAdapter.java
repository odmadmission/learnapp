package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.sseduventures.digichamps.Model.SearchDictonaryModule;
import com.sseduventures.digichamps.R;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Locale;



public class SearchDictonaryAdapter  extends ArrayAdapter implements Filterable {
    ArrayList<SearchDictonaryModule> wordDetailsarr;
    ArrayList<SearchDictonaryModule> allWordDetails=new ArrayList<>();
    Context context;
    LayoutInflater inflater;

    public   SearchDictonaryAdapter(Context context,int resource,ArrayList<SearchDictonaryModule > wordDetailsarr) {
        super(context,resource);
        this.context=context;
        this.wordDetailsarr=wordDetailsarr;
        inflater= LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return wordDetailsarr.size();

    }

    @Override
    public Object getItem(int position) {
        return wordDetailsarr.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View view1 = inflater.inflate(R.layout.auto_fill_drop_down, null);

        TextView titleName= (TextView) view1.findViewById(R.id.wordname);

        titleName.setText(wordDetailsarr.get(position).getDictonaryName());


        return view1;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return filter;
    }
    Filter filter=new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults filterResults = new FilterResults();
            if (constraint!=null){
                wordDetailsarr.clear();

                for (SearchDictonaryModule search_word:allWordDetails){

                    if (search_word.getDictonaryName().startsWith(constraint.toString())){
                        highlight(constraint.toString(),search_word.getDictonaryName());
                        wordDetailsarr.add(search_word);

                    }

                }

                filterResults.values = wordDetailsarr;
                filterResults.count = wordDetailsarr.size();
            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults results) {

            ArrayList<SearchDictonaryModule> filteredList = (ArrayList<SearchDictonaryModule>) results.values;
            if(results != null && results.count > 0) {
                wordDetailsarr.clear();
                for (SearchDictonaryModule search_word : filteredList) {
                    add(search_word);
                }
                notifyDataSetChanged();
            }
        }
    };

    public static CharSequence highlight(String search, String originalText) {
        String normalizedText = Normalizer
                .normalize(originalText, Normalizer.Form.NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
                .toLowerCase(Locale.ENGLISH);

        int start = normalizedText.indexOf(search.toLowerCase(Locale.ENGLISH));
        if (start < 0) {
            // not found, nothing to to
            return originalText;
        } else {
            Spannable highlighted = new SpannableString(originalText);
            while (start >= 0) {
                int spanStart = Math.min(start, originalText.length());
                int spanEnd = Math.min(start + search.length(),
                        originalText.length());

                highlighted.setSpan(new ForegroundColorSpan(Color.BLUE),
                        spanStart, spanEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                start = normalizedText.indexOf(search, spanEnd);
            }

            return highlighted;
        }
    }
}
