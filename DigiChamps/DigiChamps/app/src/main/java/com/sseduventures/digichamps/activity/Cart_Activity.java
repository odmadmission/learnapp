package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.MenuInflater;
import android.view.View;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewNotificationActivity;
import com.sseduventures.digichamps.adapter.Cart_Custom_Adapter;
import com.sseduventures.digichamps.adapter.RecyclerTouchListener;
import com.sseduventures.digichamps.domain.CartPackage;
import com.sseduventures.digichamps.domain.CartPackageId;
import com.sseduventures.digichamps.domain.GetCartDataSuccess;
import com.sseduventures.digichamps.domain.RemoveCartPackageList;
import com.sseduventures.digichamps.domain.RemoveCartRequest;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.AlertDialogHelper;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;

import java.util.ArrayList;

import android.content.Intent;
import android.widget.Button;

import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;


public class Cart_Activity extends FormActivity
        implements AlertDialogHelper.AlertDialogListener, ServiceReceiver.Receiver {

    ArrayList<CartPackage> list;
    ArrayList<CartPackage> multiselect_list = new ArrayList<>();

    private RecyclerView recyclerView;
    private Cart_Custom_Adapter mAdapter;
    ImageButton notification;
    ImageView back_arrow;
    boolean isMultiSelect = false;
    ActionMode mActionMode;
    Menu context_menu;
    private String resp, error, TAG = Cart_Activity.class.getSimpleName(), type;
    AlertDialogHelper alertDialogHelper;
    private Toolbar toolbar;
    private TextView no_cartdata_text;
    private ImageView empty_cart;
    private Button cart_buy_button;
    private NestedScrollView nsv;
    RelativeLayout cart_layout;
    LinearLayout lin;
    Button checkout;


    private ServiceReceiver mServiceReceiver;
    private Handler mHandler;
    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // preventing screen capture
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);


        setContentView(R.layout.activity_cart_new);
        logEvent(LogEventUtil.KEY_Cart_Checkout_PAGE,
                LogEventUtil.EVENT_Cart_Checkout_PAGE);
        setModuleName(LogEventUtil.EVENT_Cart_Checkout_PAGE);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        init();

        notification = (ImageButton) findViewById(R.id.notif);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Cart_Activity.this,
                        NewNotificationActivity.class));


            }
        });

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Cart_Activity.this,
                        Checkout_Activity.class);
                intent.putExtra("discount",
                        RegPrefManager.getInstance(
                                Cart_Activity.this
                        ).getReedeem());
                startActivity(intent);
                finish();


            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);


        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener
                (getApplicationContext(), recyclerView,
                        new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                if (isMultiSelect)
                    multi_select(position);
                else {

                }
            }

            @Override
            public void onLongClick(View view, int position) {
                if (!isMultiSelect) {
                    multiselect_list = new ArrayList<CartPackage>();
                    isMultiSelect = true;

                    if (mActionMode == null) {
                        mActionMode = startActionMode(mActionModeCallback);
                    }
                }

                multi_select(position);
            }
        }));

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ReturnHome(v);

            }
        });
        cart_buy_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Cart_Activity.this,
                        PackageActivityNew.class);
                intent.putExtra("discount",1);

                startActivity(intent);


            }
        });



    }

    private void init() {

        mHandler=new Handler();
        mServiceReceiver=new ServiceReceiver(mHandler);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        alertDialogHelper = new AlertDialogHelper(this);

        checkout = (Button) findViewById(R.id.bt_order);

        recyclerView = (RecyclerView) findViewById(R.id.cart_recycler_view);

        list = new ArrayList<>();
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        mAdapter = new Cart_Custom_Adapter(list, this, RegPrefManager
        .getInstance(this).getReedeem());
        recyclerView.setAdapter(mAdapter);

        back_arrow = (ImageView) findViewById(R.id.cart_back_image);
        cart_layout = (RelativeLayout) findViewById(R.id.cart_Layout);
        cart_layout.setVisibility(View.GONE);

        lin = (LinearLayout) findViewById(R.id.linear);
        empty_cart = (ImageView) findViewById(R.id.empty);
        no_cartdata_text = (TextView) findViewById(R.id.text_empty);
        cart_buy_button = (Button) findViewById(R.id.buy_btn);
        nsv = (NestedScrollView) findViewById(R.id.cart_nsv);
        cart_layout.setVisibility(View.VISIBLE);

    }

    public void buy(View view) {
        Intent intent = new Intent(Cart_Activity.this, PackageActivityNew.class);
        startActivity(intent);

    }

    public void multi_select(int position) {
        if (mActionMode != null) {
            if (multiselect_list.contains(list.get(position)))
                multiselect_list.remove(list.get(position));
            else
                multiselect_list.add(list.get(position));

            if (multiselect_list.size() > 0)
                mActionMode.setTitle("" + multiselect_list.size() + " Selected");
            else
                mActionMode.setTitle("");

            refreshAdapter();

        }
    }

    public void refreshAdapter() {
        mAdapter.selected_usersList = multiselect_list;
        mAdapter.dataSet = list;
        mAdapter.notifyDataSetChanged();
    }

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_multi_select, menu);
            toolbar.setVisibility(View.GONE);
            context_menu = menu;
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            Cart_Activity.this, R.style.myDialog);
                    builder.setMessage(getResources().getString(R.string.delete_warning))
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if (multiselect_list.size() > 0) {
                                        for (int i = 0; i < multiselect_list.size(); i++)
                                            mAdapter.notifyDataSetChanged();
                                        multiDelete();
                                        if (mActionMode != null) {
                                            mActionMode.finish();
                                        }
                                    }
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.
                                    OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                            .setTitle("Delete Items!")
                            .setIcon(R.drawable.alert_warning);
                    AlertDialog alert = builder.create();
                    alert.show();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
            isMultiSelect = false;
            multiselect_list = new ArrayList<CartPackage>();
            toolbar.setVisibility(View.VISIBLE);
            refreshAdapter();
        }
    };


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public void onPositiveClick(int from) {
        if (from == 1) {

        } else if (from == 2) {
            if (mActionMode != null) {
                mActionMode.finish();
            }

            CartPackage mSample = new CartPackage();
            list.add(mSample);
            mAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public void onNegativeClick(int from) {

    }

    @Override
    public void onNeutralClick(int from) {

    }




    public void ReturnHome(View view) {
        super.onBackPressed();

        finish();
    }


    @Override
    protected void onStop() {
        super.onStop();
        hideDialog();
        mServiceReceiver.setReceiver(this);


    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
        getCartData();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        hideDialog();
        super.onSaveInstanceState(outState, outPersistentState);
    }
    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        switch (resultCode) {
            case ResponseCodes.NO_INTERNET:

                showAlertDialog(
                      "No internet",
                        "No internet"
                );
                break;

            case ResponseCodes.UNEXPECTED_ERROR:

                showAlertDialog("Something went wrong",
                        "Please try again later " +
                                resultData.getString(IntentHelper.RESULT_EXCEPTION));
                break;


            case ResponseCodes.EXCEPTION:

                showToast("Exception : " +
                        resultData.getString(IntentHelper.RESULT_EXCEPTION));
                break;

            case ResponseCodes.MULTI_CART_REMOVE_SUCCESS:

                multiselect_list.clear();
                getCartData();
                break;


            case ResponseCodes.MULTI_CART_REMOVE_FAILURE:
                showToast("Add to cart fail");
                break;

            case ResponseCodes.GET_CART_DATA_SUCCESS:
                multiselect_list.clear();
                GetCartDataSuccess response=resultData.
                        getParcelable(IntentHelper.RESULT_DATA);
                if(response!=null
                        &&response.getGetcartdata()!=null
                        &&response.getGetcartdata().getCartdata()!=null) {

                    ArrayList<CartPackage> l =

                            response.getGetcartdata().getCartdata();
                    if (l != null && l.size() > 0) {

                        list.clear();
                        list.addAll(l);
                        mAdapter.notifyDataSetChanged();

                        empty_cart.setVisibility(View.GONE);
                        no_cartdata_text.setVisibility(View.GONE);
                        cart_buy_button.setVisibility(View.GONE);
                        checkout.setVisibility(View.VISIBLE);

                    }
                }
                else {

                    empty_cart.setVisibility(View.VISIBLE);
                    no_cartdata_text.setVisibility(View.VISIBLE);
                    cart_buy_button.setVisibility(View.VISIBLE);
                    checkout.setVisibility(View.GONE);


                }

                break;


            case ResponseCodes.GET_CART_DATA_FAILURE:

                showToast("Add to cart fail");

                break;
        }
    }


    private void showAlertDialog(String title,String text)
    {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setTitle(title);
        builder1.setMessage(text);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "CLOSE",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });



        AlertDialog alert11 = builder1.create();
        alert11.show();

    }
    private void getCartData()
    {
        list.clear();

        if(AppUtil.isInternetConnected(this))
        {

            NetworkService.startActionGetCartData(this,
                    mServiceReceiver);


        }
        else
        {
            showToast("No internet");
        }
    }
    private void multiDelete()
    {
        list.clear();

        if(AppUtil.isInternetConnected(this))
        {

            Bundle bundle=new Bundle();
            ArrayList<CartPackageId> ids=new ArrayList<>();
            for (int i=0;i<multiselect_list.size();i++)
            {
                CartPackageId id=new CartPackageId();
                id.setId(multiselect_list.get(i).getCart_ID());
                ids.add(id);
            }

            RemoveCartPackageList list
                    =new RemoveCartPackageList();
            list.setRemovecart(ids);

            RemoveCartRequest r=new RemoveCartRequest();
            r.setDeleteitem(list);

            bundle.putParcelable(IntentHelper.EXTRA_DATA1,r);

            NetworkService.startActionRemoveCartData(this,
                    mServiceReceiver,bundle);


        }
        else
        {
            showToast("No internet");
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }
}
