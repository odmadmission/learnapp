package com.sseduventures.digichamps.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Tech_1 on 1/3/2018.
 */

public class Bookmarks_frag1_model implements Parcelable {

        private String Module_Id, ModuleName, Module_Title,
    Module_Image, Description,Image_Key,VideoKey,Is_Expire,Is_Free,Is_Avail;


    private boolean isFlag;

    public boolean isFlag() {
        return isFlag;
    }

    public void setFlag(boolean flag) {
        isFlag = flag;
    }


        public Bookmarks_frag1_model(String Module_Id, String ModuleName,
                                    String Module_Title, String Module_Image,
                                    String Description,String Image_Key,String VideoKey,String Is_Expire,String Is_Free,String Is_Avail) {
            this.Module_Id = Module_Id;
            this.ModuleName = ModuleName;
            this.Module_Title = Module_Title;
            this.Module_Image = Module_Image;
            this.Module_Image = Module_Image;
            this.Description = Description;
            this.Image_Key = Image_Key;
            this.VideoKey = VideoKey;
            this.Is_Expire = Is_Expire;
            this.Is_Free = Is_Free;
            this.Is_Avail = Is_Avail;
        }

    protected Bookmarks_frag1_model(Parcel in) {
        Module_Id = in.readString();
        ModuleName = in.readString();
        Module_Title = in.readString();
        Module_Image = in.readString();
        Description = in.readString();
        Image_Key = in.readString();
        VideoKey = in.readString();
        Is_Expire = in.readString();
        Is_Free = in.readString();
        Is_Avail = in.readString();
    }

    public static final Creator<Bookmarks_frag1_model> CREATOR = new Creator<Bookmarks_frag1_model>() {
        @Override
        public Bookmarks_frag1_model createFromParcel(Parcel in) {
            return new Bookmarks_frag1_model(in);
        }

        @Override
        public Bookmarks_frag1_model[] newArray(int size) {
            return new Bookmarks_frag1_model[size];
        }
    };

    public String getModule_Id() {
        return Module_Id;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public String getModule_Title() {
        return Module_Title;
    }

    public String getModule_Image() {
        return Module_Image;
    }

    public String getDescription() {
        return Description;
    }

    public String getImage_Key() {
        return Image_Key;
    }

    public String getVideoKey() {
        return VideoKey;
    }

    public String getIs_Expire() {
        return Is_Expire;
    }

    public String getIs_Free() {
        return Is_Free;
    }

    public String getIs_Avail() {
        return Is_Avail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Module_Id);
        dest.writeString(ModuleName);
        dest.writeString(Module_Title);
        dest.writeString(Module_Image);
        dest.writeString(Description);
        dest.writeString(Image_Key);
        dest.writeString(VideoKey);
        dest.writeString(Is_Expire);
        dest.writeString(Is_Free);
        dest.writeString(Is_Avail);
    }
}





