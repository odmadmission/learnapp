package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NISHIKANT on 7/9/2018.
 */

public class SubConceptResponse implements Parcelable{

    private List<SubSuccessList> success;

    private ArrayList<String> subjectList;

    public List<SubSuccessList> getSuccess() {
        return success;
    }

    public void setSuccess(List<SubSuccessList> success) {
        this.success = success;
    }

    public ArrayList<String> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(ArrayList<String> subjectList) {
        this.subjectList = subjectList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.success);
        dest.writeStringList(this.subjectList);
    }

    public SubConceptResponse() {
    }

    protected SubConceptResponse(Parcel in) {
        this.success = in.createTypedArrayList(SubSuccessList.CREATOR);
        this.subjectList = in.createStringArrayList();
    }

    public static final Creator<SubConceptResponse> CREATOR = new Creator<SubConceptResponse>() {
        @Override
        public SubConceptResponse createFromParcel(Parcel source) {
            return new SubConceptResponse(source);
        }

        @Override
        public SubConceptResponse[] newArray(int size) {
            return new SubConceptResponse[size];
        }
    };
}
