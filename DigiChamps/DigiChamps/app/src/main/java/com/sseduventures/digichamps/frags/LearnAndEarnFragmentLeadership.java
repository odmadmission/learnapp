package com.sseduventures.digichamps.frags;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.LearnAndEarnActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.CoinLeaderboardAdapter;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.domain.BoardClass;
import com.sseduventures.digichamps.domain.CoinLeaderboardResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.CircleTransform;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class LearnAndEarnFragmentLeadership extends Fragment implements ServiceReceiver.Receiver {


    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private ArrayList<CoinLeaderboardResponse> success;

    private RecyclerView learn_point;
    private CoinLeaderboardAdapter mAdapter;
    private List<CoinLeaderboardResponse> mList;
    private RelativeLayout rlv_lay,rlvLayTwo;

    private FormActivity mContext;
    private TextView nameTextMy,classTextMy,pointsTextMy,txtMyCoins;
    private ImageView userImageMy;
    private ShimmerFrameLayout mShimmerViewContainer;


    public LearnAndEarnFragmentLeadership() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ((FormActivity)getActivity()).
                logEvent(LogEventUtil.KEY_LearnAndEarnLeaderboard_PAGE,
                LogEventUtil.EVENT_LearnAndEarnLeaderboard_PAGE);
        ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_LearnAndEarnLeaderboard_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.leatn_and_earn_leadership,
                container, false);

        init(view);

        return view;
    }


    private void init(View view){

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        mContext = (LearnAndEarnActivity) getActivity();
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        learn_point = (RecyclerView) view.findViewById(R.id.learn_point);
        rlv_lay = view.findViewById(R.id.rlv_lay);
        rlvLayTwo = view.findViewById(R.id.rlvLayTwo);
        nameTextMy = view.findViewById(R.id.nameTextMy);
        classTextMy = view.findViewById(R.id.classTextMy);
        userImageMy = view.findViewById(R.id.userImageMy);
        pointsTextMy = view.findViewById(R.id.pointsTextMy);
        txtMyCoins = view.findViewById(R.id.txtMyCoins);


        txtMyCoins.setVisibility(View.GONE);
        rlvLayTwo.setVisibility(View.GONE);




        learn_point.setNestedScrollingEnabled(false);
        learn_point.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        learn_point.setLayoutManager(mLayoutManager);

        mList = new ArrayList<>();
        getCoinLeaderBoardList();
        mAdapter = new CoinLeaderboardAdapter(mList,getActivity());
        learn_point.setAdapter(mAdapter);



    }


    private void getCoinLeaderBoardList(){

        if(AppUtil.isInternetConnected(getActivity())){
            NetworkService.
                    startActionGetCoinsLeaderboard(getActivity(),mServiceReceiver);

        }else{
            Toast.makeText(getActivity(), "No Internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    public void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        switch(resultCode){

            case ResponseCodes.SUCCESS:

                success = resultData.getParcelableArrayList(IntentHelper.RESULT_DATA);
                if(success!=null && success.size()>0){

                    int j = 0;

                    CoinLeaderboardResponse obj = null;
                    for(int i=0;i<success.size();i++)
                    {
                       if(success.get(i)!=null&&
                               !success.get(i).equals("null")
                        &&success.get(i).getRegdId() ==
                               RegPrefManager.getInstance(getActivity()).getRegId()){
                           obj = success.get(i);
                           j=i;
                       }
                    }

                    if(j==10){

                        //Visible
                        rlvLayTwo.setVisibility(View.VISIBLE);
                        txtMyCoins.setVisibility(View.VISIBLE);

                        success.remove(j);
                        nameTextMy.setText(obj.getName());
                        classTextMy.setText(obj.getClassName());
                        pointsTextMy.setText(obj.getCoins());

                        String correctImage = AppConfig.SRVR_URL
                                + "/Images/Profile/" +obj.getImageUrl();
                        Picasso.with(getApplicationContext()).load(correctImage)
                                .transform(new CircleTransform()).into(userImageMy);




                    }else{
                        success.remove(success.get(success.size()-1));
                        rlvLayTwo.setVisibility(View.GONE);
                        txtMyCoins.setVisibility(View.GONE);
                    }

                    success.size();
                    mList.clear();
                    mList.addAll(success);
                    mAdapter.notifyDataSetChanged();

                }

                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(),ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(),ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(),Internet_Activity.class);
                startActivity(intent);
                break;

            case ResponseCodes.SERVER_ERROR:
                break;


        }

    }
}