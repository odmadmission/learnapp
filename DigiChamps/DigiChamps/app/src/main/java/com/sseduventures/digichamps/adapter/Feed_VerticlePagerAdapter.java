package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.DataModel_dashboardSub;
import com.sseduventures.digichamps.Model.DataModel_feed;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FeedKnowMoreActivity;
import com.sseduventures.digichamps.domain.FeedListData;

import java.util.List;



public class Feed_VerticlePagerAdapter extends PagerAdapter {


    Context mContext;
    LayoutInflater mLayoutInflater;
    private List<FeedListData> feedDataset;

    public Feed_VerticlePagerAdapter(List<FeedListData> feedlist, Context context) {
        this.feedDataset = feedlist;
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return feedDataset.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.feed_content_main, container, false);
        TextView title = (TextView) itemView.findViewById(R.id.tv_title);
        Button feed_link = (Button) itemView.findViewById(R.id.feed_link);
        TextView descp = (TextView) itemView.findViewById(R.id.tv_descp);
        ImageView upDown = (ImageView) itemView.findViewById(R.id.btn);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        TextView knowmore_tv=(TextView)itemView.findViewById(R.id.knowmore_tv);
        Typeface face = Typeface.createFromAsset(mContext.getAssets(),
                "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");
        feed_link.setTypeface(face);
        title.setTypeface(face);
        descp.setTypeface(face);
        knowmore_tv.setTypeface(face);

        title.setText(feedDataset.get(position).getFeed_Name());
        descp.setText(feedDataset.get(position).getFeed_Description());
        Picasso.with(mContext).load(feedDataset.get(position).getFeedOnline()).into(imageView);

        if(feedDataset.get(position).getFeed_Path().isEmpty()){
          //  feed_link.setVisibility(View.GONE);
            knowmore_tv.setVisibility(View.GONE);
        }else{
            knowmore_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri uri = Uri.parse(feedDataset.get(position).getFeed_Path());
                    String url=feedDataset.get(position).getFeed_Path();
                    Intent i = new Intent(mContext,FeedKnowMoreActivity.class);
                    i.putExtra("URI",url);
                    mContext.startActivity(i);


                }
            });
        }

        container.addView(itemView);


        if(position<3){

            upDown.setVisibility(View.GONE);

        }else{

            upDown.setVisibility(View.GONE);
        }

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
