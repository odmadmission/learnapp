package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ChapterDetailsSuccess implements Parcelable {

    protected ChapterDetailsSuccess(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChapterDetailsSuccess> CREATOR = new Creator<ChapterDetailsSuccess>() {
        @Override
        public ChapterDetailsSuccess createFromParcel(Parcel in) {
            return new ChapterDetailsSuccess(in);
        }

        @Override
        public ChapterDetailsSuccess[] newArray(int size) {
            return new ChapterDetailsSuccess[size];
        }
    };

    public Success getSuccess() {
        return success;
    }

    public void setSuccess(Success success) {
        this.success = success;
    }

    private Success success;

    public class Success implements Parcelable{
        public ArrayList<QuesbankClass> getQuesbank() {
            return Quesbank;
        }

        public void setQuesbank(ArrayList<QuesbankClass> quesbank) {
            Quesbank = quesbank;
        }

        public ArrayList<pdfsClass> getPdfs() {
            return pdfs;
        }

        public void setPdfs(ArrayList<pdfsClass> pdfs) {
            this.pdfs = pdfs;
        }

        public Integer getChapterid() {
            return Chapterid;
        }

        public void setChapterid(Integer chapterid) {
            Chapterid = chapterid;
        }

        public String getChapter_Name() {
            return Chapter_Name;
        }

        public void setChapter_Name(String chapter_Name) {
            Chapter_Name = chapter_Name;
        }

        public String getChapterImage() {
            return ChapterImage;
        }

        public void setChapterImage(String chapterImage) {
            ChapterImage = chapterImage;
        }

        public String getToday_date() {
            return Today_date;
        }

        public void setToday_date(String today_date) {
            Today_date = today_date;
        }

        public Boolean getIs_Offline() {
            return Is_Offline;
        }

        public void setIs_Offline(Boolean is_Offline) {
            Is_Offline = is_Offline;
        }

        public Object getDIYModules() {
            return DIYModules;
        }

        public void setDIYModules(Object DIYModules) {
            this.DIYModules = DIYModules;
        }

        private ArrayList<QuesbankClass> Quesbank;
        private ArrayList<pdfsClass> pdfs;
        private Integer Chapterid;
        private String Chapter_Name;
        private String ChapterImage;

        public void setChapterModules(ArrayList<ChapterModuleClass> chapterModules) {
            ChapterModules = chapterModules;
        }

        public Creator<Success> getCREATOR() {
            return CREATOR;
        }

        public ArrayList<ChapterModuleClass> getChapterModules() {
            return ChapterModules;
        }

        private ArrayList<ChapterModuleClass> ChapterModules;
        private String Today_date;
        private Boolean Is_Offline;
        private Object DIYModules;

        protected Success(Parcel in) {
            if (in.readByte() == 0) {
                Chapterid = null;
            } else {
                Chapterid = in.readInt();
            }
            Chapter_Name = in.readString();
            ChapterImage = in.readString();
            Today_date = in.readString();
            byte tmpIs_Offline = in.readByte();
            Is_Offline = tmpIs_Offline == 0 ? null : tmpIs_Offline == 1;
        }

        public final Creator<Success> CREATOR = new Creator<Success>() {
            @Override
            public Success createFromParcel(Parcel in) {
                return new Success(in);
            }

            @Override
            public Success[] newArray(int size) {
                return new Success[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (Chapterid == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(Chapterid);
            }
            dest.writeString(Chapter_Name);
            dest.writeString(ChapterImage);
            dest.writeString(Today_date);
            dest.writeByte((byte) (Is_Offline == null ? 0 : Is_Offline ? 1 : 2));
        }
    }

    public class QuesbankClass implements Parcelable{
        private Integer moduleIDss;
        private Boolean is_question_bookmarked;
        private Object noofques;
        private String Question_Pdfs;

        protected QuesbankClass(Parcel in) {
            if (in.readByte() == 0) {
                moduleIDss = null;
            } else {
                moduleIDss = in.readInt();
            }
            byte tmpIs_question_bookmarked = in.readByte();
            is_question_bookmarked = tmpIs_question_bookmarked == 0 ? null : tmpIs_question_bookmarked == 1;
            Question_Pdfs = in.readString();
            Modulename = in.readString();
        }

        public final Creator<QuesbankClass> CREATOR = new Creator<QuesbankClass>() {
            @Override
            public QuesbankClass createFromParcel(Parcel in) {
                return new QuesbankClass(in);
            }

            @Override
            public QuesbankClass[] newArray(int size) {
                return new QuesbankClass[size];
            }
        };

        public Integer getModuleIDss() {
            return moduleIDss;
        }

        public void setModuleIDss(Integer moduleIDss) {
            this.moduleIDss = moduleIDss;
        }

        public Boolean getIs_question_bookmarked() {
            return is_question_bookmarked;
        }

        public void setIs_question_bookmarked(Boolean is_question_bookmarked) {
            this.is_question_bookmarked = is_question_bookmarked;
        }

        public Object getNoofques() {
            return noofques;
        }

        public void setNoofques(Object noofques) {
            this.noofques = noofques;
        }

        public String getQuestion_Pdfs() {
            return Question_Pdfs;
        }

        public void setQuestion_Pdfs(String question_Pdfs) {
            Question_Pdfs = question_Pdfs;
        }

        public String getModulename() {
            return Modulename;
        }

        public void setModulename(String modulename) {
            Modulename = modulename;
        }

        private String Modulename;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (moduleIDss == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(moduleIDss);
            }
            dest.writeByte((byte) (is_question_bookmarked == null ? 0 : is_question_bookmarked ? 1 : 2));
            dest.writeString(Question_Pdfs);
            dest.writeString(Modulename);
        }
    }

    public class ChapterModuleClass implements Parcelable{

        private Integer Module_Id;
        private String Module_Title;
        private String Description;
        private String Module_Image;
        private String Image_Key;
        private Boolean Is_Avail;
        private String pdf_file;
        private String pdf_name;
        private Boolean Is_Free;
        private String Validity;
        private String Question_Pdf;
        private String Question_Pdf_Name;
        private Integer No_Of_Question;
        private Boolean Is_Free_Test;
        private String Media_Id;

        protected ChapterModuleClass(Parcel in) {
            if (in.readByte() == 0) {
                Module_Id = null;
            } else {
                Module_Id = in.readInt();
            }
            Module_Title = in.readString();
            Description = in.readString();
            Module_Image = in.readString();
            Image_Key = in.readString();
            byte tmpIs_Avail = in.readByte();
            Is_Avail = tmpIs_Avail == 0 ? null : tmpIs_Avail == 1;
            pdf_file = in.readString();
            pdf_name = in.readString();
            byte tmpIs_Free = in.readByte();
            Is_Free = tmpIs_Free == 0 ? null : tmpIs_Free == 1;
            Validity = in.readString();
            Question_Pdf = in.readString();
            Question_Pdf_Name = in.readString();
            if (in.readByte() == 0) {
                No_Of_Question = null;
            } else {
                No_Of_Question = in.readInt();
            }
            byte tmpIs_Free_Test = in.readByte();
            Is_Free_Test = tmpIs_Free_Test == 0 ? null : tmpIs_Free_Test == 1;
            Media_Id = in.readString();
            VideoKey = in.readString();
            template_id = in.readString();
            thumbnail_key = in.readString();
            byte tmpIs_Video_bookmarked = in.readByte();
            is_Video_bookmarked = tmpIs_Video_bookmarked == 0 ? null : tmpIs_Video_bookmarked == 1;
            byte tmpIs_question_bookmarked = in.readByte();
            is_question_bookmarked = tmpIs_question_bookmarked == 0 ? null : tmpIs_question_bookmarked == 1;
            byte tmpIs_studynotebookmarked = in.readByte();
            is_studynotebookmarked = tmpIs_studynotebookmarked == 0 ? null : tmpIs_studynotebookmarked == 1;
            byte tmpIs_Expire = in.readByte();
            Is_Expire = tmpIs_Expire == 0 ? null : tmpIs_Expire == 1;
        }

        public final Creator<ChapterModuleClass> CREATOR = new Creator<ChapterModuleClass>() {
            @Override
            public ChapterModuleClass createFromParcel(Parcel in) {
                return new ChapterModuleClass(in);
            }

            @Override
            public ChapterModuleClass[] newArray(int size) {
                return new ChapterModuleClass[size];
            }
        };

        public Integer getModule_Id() {
            return Module_Id;
        }

        public void setModule_Id(Integer module_Id) {
            Module_Id = module_Id;
        }

        public String getModule_Title() {
            return Module_Title;
        }

        public void setModule_Title(String module_Title) {
            Module_Title = module_Title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getModule_Image() {
            return Module_Image;
        }

        public void setModule_Image(String module_Image) {
            Module_Image = module_Image;
        }

        public String getImage_Key() {
            return Image_Key;
        }

        public void setImage_Key(String image_Key) {
            Image_Key = image_Key;
        }

        public Boolean getIs_Avail() {
            return Is_Avail;
        }

        public void setIs_Avail(Boolean is_Avail) {
            Is_Avail = is_Avail;
        }

        public String getPdf_file() {
            return pdf_file;
        }

        public void setPdf_file(String pdf_file) {
            this.pdf_file = pdf_file;
        }

        public String getPdf_name() {
            return pdf_name;
        }

        public void setPdf_name(String pdf_name) {
            this.pdf_name = pdf_name;
        }

        public Boolean getIs_Free() {
            return Is_Free;
        }

        public void setIs_Free(Boolean is_Free) {
            Is_Free = is_Free;
        }

        public String getValidity() {
            return Validity;
        }

        public void setValidity(String validity) {
            Validity = validity;
        }

        public String getQuestion_Pdf() {
            return Question_Pdf;
        }

        public void setQuestion_Pdf(String question_Pdf) {
            Question_Pdf = question_Pdf;
        }

        public String getQuestion_Pdf_Name() {
            return Question_Pdf_Name;
        }

        public void setQuestion_Pdf_Name(String question_Pdf_Name) {
            Question_Pdf_Name = question_Pdf_Name;
        }

        public Integer getNo_Of_Question() {
            return No_Of_Question;
        }

        public void setNo_Of_Question(Integer no_Of_Question) {
            No_Of_Question = no_Of_Question;
        }

        public Boolean getIs_Free_Test() {
            return Is_Free_Test;
        }

        public void setIs_Free_Test(Boolean is_Free_Test) {
            Is_Free_Test = is_Free_Test;
        }

        public String getMedia_Id() {
            return Media_Id;
        }

        public void setMedia_Id(String media_Id) {
            Media_Id = media_Id;
        }

        public String getVideoKey() {
            return VideoKey;
        }

        public void setVideoKey(String videoKey) {
            VideoKey = videoKey;
        }

        public String getTemplate_id() {
            return template_id;
        }

        public void setTemplate_id(String template_id) {
            this.template_id = template_id;
        }

        public String getThumbnail_key() {
            return thumbnail_key;
        }

        public void setThumbnail_key(String thumbnail_key) {
            this.thumbnail_key = thumbnail_key;
        }

        public Boolean getIs_Video_bookmarked() {
            return is_Video_bookmarked;
        }

        public void setIs_Video_bookmarked(Boolean is_Video_bookmarked) {
            this.is_Video_bookmarked = is_Video_bookmarked;
        }

        public Boolean getIs_question_bookmarked() {
            return is_question_bookmarked;
        }

        public void setIs_question_bookmarked(Boolean is_question_bookmarked) {
            this.is_question_bookmarked = is_question_bookmarked;
        }

        public Boolean getIs_studynotebookmarked() {
            return is_studynotebookmarked;
        }

        public void setIs_studynotebookmarked(Boolean is_studynotebookmarked) {
            this.is_studynotebookmarked = is_studynotebookmarked;
        }

        public Boolean getIs_Expire() {
            return Is_Expire;
        }

        public void setIs_Expire(Boolean is_Expire) {
            Is_Expire = is_Expire;
        }

        private String VideoKey;
        private String template_id;
        private String thumbnail_key;
        private Boolean is_Video_bookmarked;
        private Boolean is_question_bookmarked;
        private Boolean is_studynotebookmarked;
        private Boolean Is_Expire;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (Module_Id == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(Module_Id);
            }
            dest.writeString(Module_Title);
            dest.writeString(Description);
            dest.writeString(Module_Image);
            dest.writeString(Image_Key);
            dest.writeByte((byte) (Is_Avail == null ? 0 : Is_Avail ? 1 : 2));
            dest.writeString(pdf_file);
            dest.writeString(pdf_name);
            dest.writeByte((byte) (Is_Free == null ? 0 : Is_Free ? 1 : 2));
            dest.writeString(Validity);
            dest.writeString(Question_Pdf);
            dest.writeString(Question_Pdf_Name);
            if (No_Of_Question == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(No_Of_Question);
            }
            dest.writeByte((byte) (Is_Free_Test == null ? 0 : Is_Free_Test ? 1 : 2));
            dest.writeString(Media_Id);
            dest.writeString(VideoKey);
            dest.writeString(template_id);
            dest.writeString(thumbnail_key);
            dest.writeByte((byte) (is_Video_bookmarked == null ? 0 : is_Video_bookmarked ? 1 : 2));
            dest.writeByte((byte) (is_question_bookmarked == null ? 0 : is_question_bookmarked ? 1 : 2));
            dest.writeByte((byte) (is_studynotebookmarked == null ? 0 : is_studynotebookmarked ? 1 : 2));
            dest.writeByte((byte) (Is_Expire == null ? 0 : Is_Expire ? 1 : 2));
        }
    }
    public class pdfsClass implements Parcelable{

        protected pdfsClass(Parcel in) {
            byte tmpIs_studynotebookmarked = in.readByte();
            is_studynotebookmarked = tmpIs_studynotebookmarked == 0 ? null : tmpIs_studynotebookmarked == 1;
            if (in.readByte() == 0) {
                moduleID = null;
            } else {
                moduleID = in.readInt();
            }
            Url = in.readString();
            Modulename = in.readString();
        }

        public final Creator<pdfsClass> CREATOR = new Creator<pdfsClass>() {
            @Override
            public pdfsClass createFromParcel(Parcel in) {
                return new pdfsClass(in);
            }

            @Override
            public pdfsClass[] newArray(int size) {
                return new pdfsClass[size];
            }
        };

        public Boolean getIs_studynotebookmarked() {
            return is_studynotebookmarked;
        }

        public void setIs_studynotebookmarked(Boolean is_studynotebookmarked) {
            this.is_studynotebookmarked = is_studynotebookmarked;
        }

        public Integer getModuleID() {
            return moduleID;
        }

        public void setModuleID(Integer moduleID) {
            this.moduleID = moduleID;
        }

        public String getUrl() {
            return Url;
        }

        public void setUrl(String url) {
            Url = url;
        }

        public String getModulename() {
            return Modulename;
        }

        public void setModulename(String modulename) {
            Modulename = modulename;
        }

        private Boolean is_studynotebookmarked;
        private Integer moduleID;
        private String Url;
        private String Modulename;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (is_studynotebookmarked == null ? 0 : is_studynotebookmarked ? 1 : 2));
            if (moduleID == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(moduleID);
            }
            dest.writeString(Url);
            dest.writeString(Modulename);
        }
    }

}
