package com.sseduventures.digichamps.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.activity.Learn_Detail;
import com.sseduventures.digichamps.activity.NcertActivity;
import com.sseduventures.digichamps.activity.OlympiadsActivity;
import com.sseduventures.digichamps.activity.PrevYrPaperActivity;
import com.sseduventures.digichamps.adapter.LearnAdapter;
import com.sseduventures.digichamps.domain.NewLearnDiyModuleLists;
import com.sseduventures.digichamps.domain.NewLearnRecentwatchedvideos;
import com.sseduventures.digichamps.domain.NewLearnResponse;
import com.sseduventures.digichamps.domain.NewLearnSubjectlists;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;

public class LearnActivity extends FormActivity
        implements ServiceReceiver.Receiver {

    TextView your_coins,
            earn_your, textview_coin,
            txt_sc_heading, txt_math_heading, txt_sc_details, txt_math_details;
    private CollapsingToolbarLayout collapsing_toolbar;
    private Toolbar toolbar;
    AppBarLayout appBarLayout;
    private ImageView home;
    private RelativeLayout diy_page, math_relative, science_relative;
    private CardView card_view_one, card_view_two, card_view_three;
    private RecyclerView recentlywatched_rcl;
    private LearnAdapter learnAdapter;
    private ArrayList<NewLearnRecentwatchedvideos> recentVideoList;
    private TextView textview_recently_watch;
    private Handler mHandler;
    private RelativeLayout learn_relative;
    private ShimmerFrameLayout mShimmerViewContainer, shimmer_view_container1;
    private LinearLayout learn_linear;
    private ServiceReceiver mServiceReceiver;
    private NewLearnResponse success;
    private ArrayList<NewLearnDiyModuleLists> mList;
    private String math_subject_id, math_subject_name, science_subject_id, science_subject_name;


    private ArrayList<NewLearnRecentwatchedvideos> mlist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_learn);

        logEvent(LogEventUtil.KEY_LEARNPAGE,
                LogEventUtil.EVENT_LEARNPAGE);
        setModuleName(LogEventUtil.EVENT_LEARNPAGE);
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        shimmer_view_container1 = findViewById(R.id.shimmer_view_container1);
        learn_relative = findViewById(R.id.learn_relative);
        learn_linear = findViewById(R.id.learn_linear);
        learn_linear.setVisibility(View.GONE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(LearnActivity.this, NewDashboardActivity.class);
                startActivity(in);
            }
        });

        if (AppUtil.isInternetConnected(LearnActivity.this)) {

            getLearn();

        } else {
            AskOptionDialogNew("Please Check Your Internet Connection").show();
        }

        diy_page = findViewById(R.id.diy_page);
        textview_recently_watch = findViewById(R.id.textview_recently_watch);

        txt_sc_heading = findViewById(R.id.txt_sc_heading);
        txt_math_heading = findViewById(R.id.txt_math_heading);
        txt_sc_details = findViewById(R.id.txt_sc_details);
        txt_math_details = findViewById(R.id.txt_math_details);
        your_coins = findViewById(R.id.your_coins);
        //   refer_earn = findViewById(R.id.refer_earn);
        earn_your = findViewById(R.id.earn_your);
        appBarLayout = findViewById(R.id.appbar);
        collapsing_toolbar = findViewById(R.id.collapsing_toolbar);
        textview_coin = findViewById(R.id.textview_coin);
        home = findViewById(R.id.home);
        card_view_one = (CardView) findViewById(R.id.card_view_one);
        card_view_two = (CardView) findViewById(R.id.card_view_two);
        card_view_three = (CardView) findViewById(R.id.card_view_three);
        recentlywatched_rcl = (RecyclerView) findViewById(R.id.recentlywatched_rcl);

        math_relative = (RelativeLayout) findViewById(R.id.math_relative);
        science_relative = (RelativeLayout) findViewById(R.id.science_relative);

        recentVideoList = new ArrayList<>();
        mList = new ArrayList<>();

        learnAdapter =
                new LearnAdapter(recentVideoList,
                        LearnActivity.this, null, null);
        LinearLayoutManager horizontalBannerLayoutManage
                = new LinearLayoutManager(LearnActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recentlywatched_rcl.setLayoutManager(horizontalBannerLayoutManage);
        recentlywatched_rcl.setNestedScrollingEnabled(false);
        recentlywatched_rcl.setAdapter(learnAdapter);


        textview_coin.setText("Do it yourself");

        your_coins.setText("Bring out the scientist\n" + "in you with");
        earn_your.setText("25+ Experimental\n" + "Videos");

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsing_toolbar.setTitle("LEARN");
                    isShow = true;
                } else if (isShow) {
                    collapsing_toolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });

        card_view_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AppUtil.isInternetConnected(LearnActivity.this)) {
                    Intent intent = new Intent(getApplicationContext(), OlympiadsActivity.class);
                    startActivity(intent);

                } else {
                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }

            }
        });

        card_view_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AppUtil.isInternetConnected(LearnActivity.this)) {
                    Intent intent = new Intent(getApplicationContext(), PrevYrPaperActivity.class);
                    startActivity(intent);

                } else {
                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }
            }
        });

        card_view_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AppUtil.isInternetConnected(LearnActivity.this)) {
                    Intent intent = new Intent(getApplicationContext(), NcertActivity.class);
                    startActivity(intent);
                } else {
                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }
            }
        });

        diy_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AppUtil.isInternetConnected(LearnActivity.this)) {
                    if (mList.size() > 0) {
                        Intent in = new Intent(LearnActivity.this, DoItYourselfActivity.class);
                        in.putParcelableArrayListExtra("diylist", mList);
                        startActivity(in);

                    }
                } else {
                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }
            }
        });

        science_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AppUtil.isInternetConnected(LearnActivity.this)) {
                    SharedPreferences.Editor editor = getSharedPreferences("user_data", MODE_PRIVATE).edit();
                    editor.putString("flag", "#ffb74d").apply();
                    Intent intent = new Intent(LearnActivity.this,
                            Learn_Detail.class);
                    intent.putExtra("flag", "#ffb74d");
                    intent.putExtra("subject_id", science_subject_id);
                    intent.putExtra("subject_name", science_subject_name);
                    startActivity(intent);
                } else {
                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }

            }
        });
        math_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AppUtil.isInternetConnected(LearnActivity.this)) {
                    SharedPreferences.Editor editor = getSharedPreferences("user_data", MODE_PRIVATE).edit();
                    editor.putString("flag", "#3949ab").apply();
                    Intent intent = new Intent(LearnActivity.this, Learn_Detail.class);
                    intent.putExtra("flag", "#3949ab");
                    intent.putExtra("subject_id", math_subject_id);
                    intent.putExtra("subject_name", math_subject_name);
                    startActivity(intent);
                } else {
                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }

            }
        });


    }


    private void getLearn() {


        Bundle bun = new Bundle();

        String user_id = String.valueOf(RegPrefManager.getInstance(this).getRegId());
        bun.putString("regId", user_id);
        if (AppUtil.isInternetConnected(this)) {

            //showDialog(null, "Learning never exhausts the mind");
            NetworkService.startActionPostLearn(LearnActivity.this,
                    mServiceReceiver, bun);

        } else {

            Intent in = new Intent(LearnActivity.this, Internet_Activity.class);
            startActivity(in);

        }

    }


    @Override
    protected void onStart() {


        mServiceReceiver.setReceiver(this);
        getLearn();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
        shimmer_view_container1.startShimmer();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmer();
        shimmer_view_container1.stopShimmer();
        super.onPause();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                //hideDialog();
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);
                shimmer_view_container1.stopShimmer();
                shimmer_view_container1.setVisibility(View.GONE);
                learn_relative.setVisibility(View.GONE);
                learn_linear.setVisibility(View.VISIBLE);

                success = resultData.getParcelable(IntentHelper.RESULT_DATA);


                if (success != null && success.getSuccess().getSubjectlists() != null && success.getSuccess().getSubjectlists().size() > 0) {
                    setSubjectData();
                }

                if (success != null && success.getSuccess().getRecentwatchedvideos() != null
                        && success.getSuccess().getRecentwatchedvideos().size() > 0) {
                    textview_recently_watch.setVisibility(View.VISIBLE);
                    recentlywatched_rcl.setVisibility(View.VISIBLE);
                    mlist = new ArrayList<>();
                    mlist.addAll(success.getSuccess().getRecentwatchedvideos());
                    recentVideoList.clear();
                    int start = mlist.size() - 1;

                    while (start >= 0) {
                        recentVideoList.add(mlist.get(start));
                        start--;

                    }

                    learnAdapter.notifyDataSetChanged();


                } else {
                    recentlywatched_rcl.setVisibility(View.GONE);
                }


                if (success != null
                        && success.getSuccess().getDiyModuleLists() != null &&
                        success.getSuccess().getDiyModuleLists().size() > 0)

                {
                    mList.clear();
                    mList.addAll(success.getSuccess().getDiyModuleLists());

                }


                break;

            case ResponseCodes.FAILURE:
                hideDialog();

                break;

            case ResponseCodes.RESPONSE_NULL:

                hideDialog();
                break;
        }


    }

    NewLearnSubjectlists maths = null;
    NewLearnSubjectlists science = null;

    private void setSubjectData() {

        for (int i = 0; i < success.getSuccess().getSubjectlists().size(); i++) {
            if (success.getSuccess().getSubjectlists().get(i).getSubject().contains("Mathematics")) {
                maths = success.getSuccess().getSubjectlists().get(i);
                math_subject_id = String.valueOf(success.getSuccess().getSubjectlists().get(i).getSubjectid());
                math_subject_name = success.getSuccess().getSubjectlists().get(i).getSubject();

            } else if (success.getSuccess().getSubjectlists().get(i).getSubject().contains("Science")) {
                science = success.getSuccess().getSubjectlists().get(i);
                science_subject_id = String.valueOf(success.getSuccess().getSubjectlists().get(i).getSubjectid());
                science_subject_name = success.getSuccess().getSubjectlists().get(i).getSubject();

            }
        }

        if (maths != null) {

            String mathDetails = maths.getTotal_chapters() + " " + "Chapters" +
                    " " + maths.getTotal_videos() + " " + "Videos";
            txt_math_heading.setText(maths.getSubject());
            txt_math_details.setText(mathDetails);
        }

        if (science != null) {
            String scDetails = science.getTotal_chapters() + " " + "Chapters" +
                    " " + science.getTotal_videos() + " " + "Videos";
            txt_sc_heading.setText(science.getSubject());
            txt_sc_details.setText(scDetails);
        }
    }

    private android.app.AlertDialog AskOptionDialogNew(String msg) {
        android.app.AlertDialog myQuittingDialogBox = new android.app.AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })

                .create();
        return myQuittingDialogBox;
    }


}

