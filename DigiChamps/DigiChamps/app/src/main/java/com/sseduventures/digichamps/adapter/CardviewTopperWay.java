package com.sseduventures.digichamps.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.sseduventures.digichamps.Model.TopperWayList;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.domain.ToppersList;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class CardviewTopperWay extends RecyclerView.Adapter<CardviewTopperWay.DataObjectHolder> {
    private String LOG_TAG = "MyRecyclerViewAdapter";
    Context context;
    private List<ToppersList> list = new ArrayList<>();

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView tv_title,tv_class,tv_date;
        RelativeLayout rel_master;

        public DataObjectHolder(View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_class = itemView.findViewById(R.id.tv_class);
            tv_date = itemView.findViewById(R.id.tv_date);
            rel_master = itemView.findViewById(R.id.rel_master);

        }
    }

    public CardviewTopperWay(List<ToppersList> getlist, Context context) {
        this.list = getlist;
        this.context = context;
    }

    @Override
    public CardviewTopperWay.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_topper, parent, false);

        CardviewTopperWay.DataObjectHolder dataObjectHolder = new CardviewTopperWay.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final CardviewTopperWay.DataObjectHolder holder, final int position) {
        holder.tv_title.setText(list.get(position).getTitle());
        holder.tv_class.setText(list.get(position).getClassName());
        holder.tv_date.setText(Utils.parseDateChange(list.get(position).getCreatedDate()));
        holder.rel_master.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!list.get(position).getFileURL().equals("")){
                    Log.v("FileURL",list.get(position).getFileURL());
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(Constants.IMAGE_URL+list.get(position).getFileURL()));
                    context.startActivity(browserIntent);
                }else{
                    AskOptionDialog("No Attachment Available").show();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(context)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

}

