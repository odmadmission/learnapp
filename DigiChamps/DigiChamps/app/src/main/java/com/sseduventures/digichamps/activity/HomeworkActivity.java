package com.sseduventures.digichamps.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.datetimepicker.date.DatePickerDialog;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sseduventures.digichamps.Model.HomeWorkList;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.adapter.CardviewHomework;
import com.sseduventures.digichamps.domain.HomeworkList;
import com.sseduventures.digichamps.domain.HomeworkResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.utils.Utils;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.model.FeedBackFormModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.sseduventures.digichamps.utils.Constants.HOMEWORKLIST;




public class HomeworkActivity extends FormActivity implements
        DatePickerDialog.OnDateSetListener,ServiceReceiver.Receiver {

    Context mContext;
    CardviewHomework mCardviewHomework;
    RecyclerView recyclerview;
    RelativeLayout layout_product1;
    DatePickerDialog mDatePicker;
    private Calendar calStartDate;
    TextView tv_date;
    ArrayList<HomeworkList> mList;
    String schoolid,sectionid;
    int classid;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private HomeworkResponse success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework);

        setModuleName(LogEventUtil.EVENT_homework_activity);
        logEvent(LogEventUtil.KEY_homework_activity,LogEventUtil.EVENT_homework_activity);

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        schoolid = RegPrefManager.getInstance(this).getSchoolId();
        classid = (int)RegPrefManager.getInstance(this).getclassid();
        sectionid = RegPrefManager.getInstance(this).getSectionId();

        mContext = this;
        mList = new ArrayList<>();
        mCardviewHomework = new CardviewHomework(mList,mContext);
        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        layout_product1 =  findViewById(R.id.layout_product1);
        tv_date =  findViewById(R.id.tv_date);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        TextView tv_at =  findViewById(R.id.tv_at);
        TextView tv_day =  findViewById(R.id.tv_day);
        TextView tv_date =  findViewById(R.id.tv_date);
        TextView tv_homework =  findViewById(R.id.tv_homework);
        TextView tv_subject =  findViewById(R.id.tv_subject);
        FontManage.setFontHeaderBold(mContext,tv_at);
        FontManage.setFontMeiryo(mContext,tv_day);
        FontManage.setFontMeiryo(mContext,tv_date);
        FontManage.setFontImpact(mContext,tv_homework);
        FontManage.setFontImpact(mContext,tv_subject);

        mCardviewHomework = new CardviewHomework(mList,this);
        layout_product1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDatePicker.show(getFragmentManager(), "dateStartPicker");
            }
        });

        calStartDate = Calendar.getInstance();

        mDatePicker = com.android.datetimepicker.date.DatePickerDialog.newInstance(HomeworkActivity.this,
                calStartDate.get(Calendar.YEAR), calStartDate.get(Calendar.MONTH),
                calStartDate.get(Calendar.DAY_OF_MONTH));


        Calendar now = Calendar.getInstance();
        mDatePicker.setYearRange(now.get(Calendar.YEAR),now.get(Calendar.YEAR)+1);

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = df.format(c);

        tv_date.setText(Utils.parseDateChange(currentDate));

        getHomeWorkListNew(currentDate);
    }




    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        if(datePickerDialog.getTag().equals("dateStartPicker")){
            calStartDate.set(year, monthOfYear, dayOfMonth);

            update(tv_date,calStartDate);

        }
    }

    private void update(TextView tv, Calendar cal) {
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd/MM/yyyy");
        tv.setText(fmtOut.format(cal.getTime()));
        tv.setError(null);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        getHomeWorkListNew(df.format(cal.getTime()));
    }

    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mContext)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    private void getHomeWorkListNew(String date) {
        if(AppUtil.isInternetConnected(this)){

            Bundle bundle=new Bundle();
            bundle.putString("homeworkdate",date);

            NetworkService.startActionPostHomeworkDetails(HomeworkActivity.this
                    ,mServiceReceiver,bundle);

        }else{
            Intent in = new Intent(HomeworkActivity.this,
                    Internet_Activity.class);
            startActivity(in);
        }
    }

    @Override
    protected void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(HomeworkActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(HomeworkActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(HomeworkActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:

                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if(success!=null && success.isStatus()){

                    if(success.getList()!=null && success.getList().size()>0){
                        mList.addAll(success.getList());
                        mCardviewHomework = new CardviewHomework(mList,mContext);
                        recyclerview.setAdapter(mCardviewHomework);
                        mCardviewHomework.notifyDataSetChanged();
                    }else{
                        mList = new ArrayList<>();
                        mCardviewHomework = new CardviewHomework(mList,mContext);
                        recyclerview.setAdapter(mCardviewHomework);
                        mCardviewHomework.notifyDataSetChanged();
                        AskOptionDialog("No Homework for this date").show();
                    }

                }else{
                    mList = new ArrayList<>();
                    mCardviewHomework = new CardviewHomework(mList,mContext);
                    recyclerview.setAdapter(mCardviewHomework);
                    mCardviewHomework.notifyDataSetChanged();
                    AskOptionDialog("No Homework for this date").show();
                }


                break;

        }
    }


    public void AddressDialogTwo(String details) {
        final Dialog dialog = new Dialog(HomeworkActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.homeworkdetails_dialog);
        dialog.setCanceledOnTouchOutside(false);

        //listView= (ListView) dialog.findViewById(R.id.dialoglist);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);


        select_tv.setVisibility(View.VISIBLE);
        select_tv.setText("HOMEWORK DETAILS");
        //select_tv.setText("Encash your coins");
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        ok_but.setText("OK");
        TextView back_but = (TextView) dialog.findViewById(R.id.back_but);
        back_but.setVisibility(View.GONE);
        TextView phoneed = (TextView) dialog.findViewById(R.id.phoneed);

        phoneed.setText(details);

        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }

        });

        back_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}