package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/17/2018.
 */

public class RegisDetails implements Parcelable {

    private int boardId;
    private int classId;


    public int getBoardId() {
        return boardId;
    }

    public void setBoardId(int boardId) {
        this.boardId = boardId;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.boardId);
        dest.writeInt(this.classId);
    }

    public RegisDetails() {
    }

    protected RegisDetails(Parcel in) {
        this.boardId = in.readInt();
        this.classId = in.readInt();
    }

    public static final Creator<RegisDetails> CREATOR = new Creator<RegisDetails>() {
        @Override
        public RegisDetails createFromParcel(Parcel source) {
            return new RegisDetails(source);
        }

        @Override
        public RegisDetails[] newArray(int size) {
            return new RegisDetails[size];
        }
    };
}
