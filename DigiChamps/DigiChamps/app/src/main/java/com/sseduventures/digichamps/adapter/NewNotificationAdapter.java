package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.activities.NewNotificationActivity;
import com.sseduventures.digichamps.activities.PersonalMentorActivity;
import com.sseduventures.digichamps.activity.Order;
import com.sseduventures.digichamps.activity.TestHighlight_Activity;
import com.sseduventures.digichamps.activity.TicketDetailsActivity;
import com.sseduventures.digichamps.database.DbContract;
import com.sseduventures.digichamps.firebase.NotificationType;
import com.sseduventures.digichamps.utils.DateTimeUtil;


public class NewNotificationAdapter extends SectionCursorAdapter {


    private LayoutInflater inflater;
    private NewNotificationActivity mContext;

    public NewNotificationAdapter(NewNotificationActivity context, Cursor cursor) {
        super(context, cursor, 0);

        mContext = context;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    protected Object getSectionFromCursor(Cursor cursor) {
        return DateTimeUtil.getDateAsTodayEtc(cursor.getLong(
                cursor.
                        getColumnIndex
                                (DbContract.NotificationTable.INSERTED_DATE)));


    }

    @Override
    protected View newSectionView(Context context, Object item, ViewGroup parent) {
        View view = getLayoutInflater().inflate(R.layout.item_section, parent,
                false);
        TextView tvDate = (TextView) view.findViewById(R.id.tvSection);

        return tvDate;
    }

    @Override
    protected void bindSectionView(View convertView, Context context, int position,
                                   Object item) {
        ((TextView) convertView).setText((String) item);
    }

    @Override
    protected View newItemView(Context context, Cursor cursor, ViewGroup parent) {
        View convertView = getLayoutInflater().
                inflate(R.layout.notification_layout, parent, false);
        ViewHolder holder = new ViewHolder(convertView);
        convertView.setTag(holder);

        return convertView;
    }

    @Override
    protected void bindItemView(View convertView, final Context context,final Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.message.setText(
                cursor.getString(cursor.getColumnIndex(DbContract.
                        NotificationTable.MESSAGE)));
        viewHolder.message.setTag(cursor.getInt(cursor.getColumnIndex(DbContract.
                NotificationTable.TYPE))+"@"+
                cursor.getString(cursor.getColumnIndex(DbContract.
                        NotificationTable.DATA)));


       String value=viewHolder.message.getTag().toString();
       int tag=Integer.parseInt(value.split("@")[0]);
        switch (tag){
            case NotificationType.NEW_FEED:
                viewHolder.message.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_bg_white_new, 0, R.drawable.ic_right_arrow, 0);
                break;
            case NotificationType.NEW_MENTOR_TASK:
                viewHolder.message.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_list, 0, R.drawable.ic_right_arrow, 0);
                break;
            case NotificationType.DOUBT_ANSWER:
                viewHolder.message.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_with_doubts, 0, R.drawable.ic_right_arrow, 0);
                break;
            case NotificationType.ORDER_CONFIRM:
                viewHolder.message.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_shopping_cart, 0, R.drawable.ic_right_arrow, 0);
                break;
            case NotificationType.TEST_RESULT:
                viewHolder.message.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_exam_plus, 0, R.drawable.ic_right_arrow, 0);
                break;
            case NotificationType.BACKEND_MESSAGE:
                viewHolder.message.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_backend, 0, R.drawable.ic_right_arrow, 0);
                break;
            case NotificationType.UPDATE_MENTOR_TASK:
                viewHolder.message.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_noti_mentor, 0, R.drawable.ic_right_arrow, 0);
                break;
            case NotificationType.OVERDUE_MENTOR_TASK:
                viewHolder.message.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_noti_mentor, 0, R.drawable.ic_right_arrow, 0);
                break;
            case NotificationType.MENTOR_ASSIGN:
                viewHolder.message.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_noti_mentor, 0, R.drawable.ic_right_arrow, 0);
                break;
        }


        viewHolder.message.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = (String) v.getTag();
                int tag=Integer.parseInt(data.split("@")[0]);



                switch (tag) {
                    case NotificationType.NEW_FEED:
                        Intent intent8 = new Intent(context,
                                NewDashboardActivity.class);
                        intent8.putExtra("TabId", "feed");
                        context.startActivity(intent8);
                        mContext.finish();
                        break;
                    case NotificationType.NEW_MENTOR_TASK:
                        Intent intent4 = new Intent(context,
                                PersonalMentorActivity.class);
                        context.startActivity(intent4);
                        mContext.finish();
                        break;
                    case NotificationType.DOUBT_ANSWER:

                        Intent intent = new Intent(context, TicketDetailsActivity.class);
                        intent.putExtra("activity","notification");
                        intent.putExtra("ticket_id",data.split("@")[1]);
                        context.startActivity(intent);
                        mContext.finish();
                        break;

                    case NotificationType.ORDER_CONFIRM:
                        Intent intent1 = new Intent(context, Order.class);
                        context.startActivity(intent1);
                        mContext.finish();
                        break;
                    case NotificationType.TEST_RESULT:
                        Intent intent2 = new Intent(context, TestHighlight_Activity.class);
                        intent2.putExtra("Result_ID",
                        data.split("@")[1]);
                        intent2.putExtra("navigation","notification");
                        intent2.putExtra("activity","notification");
                        context.startActivity(intent2);
                        mContext.finish();
                        break;

                    case NotificationType.REG_REFERRAL:



                        break;
                    case NotificationType.BACKEND_MESSAGE:


                        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                        if (browserIntent.resolveActivity(
                                context.getPackageManager()) != null) {
                            browserIntent.setData(Uri.parse(data.split("@")[1]));
                            context.startActivity(browserIntent);
                        }

                        break;
                    case NotificationType.UPDATE_MENTOR_TASK:
                        Intent intent3 = new Intent(context, PersonalMentorActivity.class);
                        context.startActivity(intent3);
                        mContext.finish();
                        break;

                    case NotificationType.OVERDUE_MENTOR_TASK:
                        Intent intent6 = new Intent(context, PersonalMentorActivity.class);
                        context.startActivity(intent6);
                        mContext.finish();
                        break;
                    case NotificationType.MENTOR_ASSIGN:
                        Intent intent5 = new Intent(context, PersonalMentorActivity.class);
                        context.startActivity(intent5);
                        mContext.finish();
                        break;

                }
            }
        });

    }


    private static class ViewHolder {
        TextView message;
        TextView date;


        private ViewHolder(View convertView) {
            message = (TextView) convertView.findViewById(R.id.title);

        }
    }

    @Override
    protected int getMaxIndexerLength() {
        return 1;
    }
}