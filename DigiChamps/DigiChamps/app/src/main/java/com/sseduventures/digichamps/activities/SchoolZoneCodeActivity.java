package com.sseduventures.digichamps.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sseduventures.digichamps.R;

public class SchoolZoneCodeActivity extends FormActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_zone_code);
    }
}
