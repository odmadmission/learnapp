package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/5/2018.
 */

public class CartPackageId implements Parcelable
{
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
    }

    public CartPackageId() {
    }

    protected CartPackageId(Parcel in) {
        this.id = in.readInt();
    }

    public static final Creator<CartPackageId> CREATOR = new Creator<CartPackageId>() {
        @Override
        public CartPackageId createFromParcel(Parcel source) {
            return new CartPackageId(source);
        }

        @Override
        public CartPackageId[] newArray(int size) {
            return new CartPackageId[size];
        }
    };
}
