package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.database.Cursor;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SectionIndexer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.SortedMap;
import java.util.TreeMap;

public abstract class SectionCursorAdapter extends CursorAdapter implements SectionIndexer {

    public static final int NO_CURSOR_POSITION = -99; // used when mapping section list position to cursor position

    protected static final int VIEW_TYPE_SECTION = 0;
    protected static final int VIEW_TYPE_ITEM = 1;

    protected SortedMap<Integer, Object> mSections = new TreeMap<Integer, Object>(); // should not be null
    ArrayList<Integer> mSectionList = new ArrayList<Integer>();
    private Object[] mFastScrollObjects;

    private LayoutInflater mLayoutInflater;

    public SectionCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);
        init(context, null);
    }

    protected SectionCursorAdapter(Context context, Cursor c, boolean autoRequery, SortedMap<Integer, Object> sections) {
        super(context, c, autoRequery);
        init(context, sections);
    }

    @Deprecated
    public SectionCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor);
        init(context, null);
    }

    private void init(Context context, SortedMap<Integer, Object> sections) {
        mLayoutInflater = LayoutInflater.from(context);
        if (sections != null) {
            mSections = sections;
        } else {
            buildSections();
        }
    }


    protected LayoutInflater getLayoutInflater() {
        return mLayoutInflater;
    }


    private void buildSections() {
        if (hasOpenCursor()) {
            Cursor cursor = getCursor();
            cursor.moveToPosition(-1);
            mSections = buildSections(cursor);
            if (mSections == null) {
                mSections = new TreeMap<Integer, Object>();
            }
        }
    }


    protected SortedMap<Integer, Object> buildSections(Cursor cursor) {
        TreeMap<Integer, Object> sections = new TreeMap<Integer, Object>();
        int cursorPosition = 0;
        while (hasOpenCursor() && cursor.moveToNext()) {
            Object section = getSectionFromCursor(cursor);
            if (cursor.getPosition() != cursorPosition)
                throw new IllegalStateException("Do no move the cursor's position in getSectionFromCursor.");
            if (!sections.containsValue(section))
                sections.put(cursorPosition + sections.size(), section);
            cursorPosition++;
        }
        return sections;
    }


    protected abstract Object getSectionFromCursor(Cursor cursor);

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        boolean isSection = isSection(position);
        Context context = parent.getContext();
        Cursor cursor = getCursor();
        View view;

        if (!isSection) {
            int newPosition = getCursorPositionWithoutSections(position);
            if (!hasOpenCursor()) {
                return new View(parent.getContext());
            } else if (!cursor.moveToPosition(newPosition)) {
                throw new IllegalStateException("couldn't move cursor to position " + newPosition);
            }
        }

        if (convertView == null) {
            view = isSection ? newSectionView(context, getItem(position), parent)
                    : newItemView(context, cursor, parent);
        } else {
            view = convertView;
        }

        if (isSection) {
            bindSectionView(view, context, position, getItem(position));
        } else {
            bindItemView(view, context, cursor);
        }

        return view;
    }

    @Override
    @Deprecated

    public final View newView(Context context, Cursor cursor, ViewGroup parent) {
        throw new IllegalStateException("This method is not used by " + SectionCursorAdapter.class.getSimpleName());
    }

    @Override
    @Deprecated

    public final void bindView(View view, Context context, Cursor cursor) {
        throw new IllegalStateException("This method is not used by " + SectionCursorAdapter.class.getSimpleName());
    }


    protected abstract View newSectionView(Context context, Object item, ViewGroup parent);


    protected abstract void bindSectionView(View convertView, Context context, int position, Object item);

    protected abstract View newItemView(Context context, Cursor cursor, ViewGroup parent);


    protected abstract void bindItemView(View convertView, Context context, Cursor cursor);


    public boolean isSection(int listPosition) {
        return mSections.containsKey(listPosition);
    }


    public int getCursorPositionWithoutSections(int listPosition) {
        if (mSections.size() == 0) {
            return listPosition;
        } else if (!isSection(listPosition)) {
            int sectionIndex = getIndexWithinSections(listPosition);
            if (isListPositionBeforeFirstSection(listPosition, sectionIndex)) {
                return listPosition;
            } else {
                return listPosition - (sectionIndex + 1);
            }
        } else {
            return NO_CURSOR_POSITION;
        }
    }


    public int getIndexWithinSections(int listPosition) {
        boolean isSection = false;
        int numPrecedingSections = 0;
        for (Integer sectionPosition : mSections.keySet()) {
            if (listPosition > sectionPosition)
                numPrecedingSections++;
            else if (listPosition == sectionPosition)
                isSection = true;
            else
                break;
        }
        return isSection ? numPrecedingSections : Math.max(numPrecedingSections - 1, 0);
    }

    private boolean isListPositionBeforeFirstSection(int listPosition, int sectionIndex) {
        boolean hasSections = mSections != null && mSections.size() > 0;
        return sectionIndex == 0 && hasSections && listPosition < mSections.firstKey();
    }

    @Override
    public void notifyDataSetChanged() {
        if (hasOpenCursor()) {
            buildSections();
            mFastScrollObjects = null;
            mSectionList.clear();
        }
        super.notifyDataSetChanged();
    }


    @Override
    public void notifyDataSetInvalidated() {
        if (hasOpenCursor()) {
            buildSections();
            mFastScrollObjects = null;
            mSectionList.clear();
        }
        super.notifyDataSetInvalidated();
    }


    @Override
    public Object getItem(int listPosition) {
        if (isSection(listPosition))
            return mSections.get(listPosition);
        else
            return super.getItem(getCursorPositionWithoutSections(listPosition));
    }


    @Override
    public long getItemId(int listPosition) {
        if (isSection(listPosition))
            return listPosition;
        else {
            int cursorPosition = getCursorPositionWithoutSections(listPosition);
            Cursor cursor = getCursor();
            if (hasOpenCursor() && cursor.moveToPosition(cursorPosition)) {
                return cursor.getLong(cursor.getColumnIndex("_id"));
            }
            return NO_CURSOR_POSITION;
        }
    }


    @Override
    public int getCount() {
        return super.getCount() + mSections.size();
    }


    @Override
    public int getItemViewType(int listPosition) {
        return isSection(listPosition) ? VIEW_TYPE_SECTION : VIEW_TYPE_ITEM;
    }


    @Override
    public int getViewTypeCount() {
        return 2;
    }

    protected boolean hasOpenCursor() {
        Cursor cursor = getCursor();
        if (cursor == null || cursor.isClosed()) {
            swapCursor(null);
            return false;
        }
        return true;
    }


    @Override
    public int getPositionForSection(int sectionIndex) {
        if (mSectionList.size() == 0) {
            for (Integer key : mSections.keySet()) {
                mSectionList.add(key);
            }
        }
        return sectionIndex < mSectionList.size() ? mSectionList.get(sectionIndex) : getCount();
    }


    @Override
    public int getSectionForPosition(int position) {
        Object[] objects = getSections(); // the fast scroll section objects
        int sectionIndex = getIndexWithinSections(position);

        return sectionIndex < objects.length ? sectionIndex : 0;
    }

    @Override
    public Object[] getSections() {
        if (mFastScrollObjects == null) {
            mFastScrollObjects = getFastScrollDialogLabels();
        }
        return mFastScrollObjects;
    }


    protected int getMaxIndexerLength() {
        return 3;
    }


    private Object[] getFastScrollDialogLabels() {
        Collection<Object> sectionsCollection = mSections.values();
        Object[] objects = sectionsCollection.toArray(new Object[sectionsCollection.size()]);
        if (VERSION.SDK_INT < VERSION_CODES.KITKAT) {
            int max = getMaxIndexerLength();
            for (int i = 0; i < objects.length; i++) {
                if (objects[i].toString().length() >= max) {
                    objects[i] = objects[i].toString().substring(0, max);
                }
            }
        }
        return objects;
    }
}