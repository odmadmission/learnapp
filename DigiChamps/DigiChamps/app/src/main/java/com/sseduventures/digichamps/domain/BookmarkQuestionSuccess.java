package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 9/5/2018.
 */

public class BookmarkQuestionSuccess implements Parcelable{

    private List<BookmarkQuestionList> QuestionList;

    protected BookmarkQuestionSuccess(Parcel in) {
        QuestionList = in.createTypedArrayList(BookmarkQuestionList.CREATOR);
    }

    public static final Creator<BookmarkQuestionSuccess> CREATOR = new Creator<BookmarkQuestionSuccess>() {
        @Override
        public BookmarkQuestionSuccess createFromParcel(Parcel in) {
            return new BookmarkQuestionSuccess(in);
        }

        @Override
        public BookmarkQuestionSuccess[] newArray(int size) {
            return new BookmarkQuestionSuccess[size];
        }
    };

    public List<BookmarkQuestionList> getQuestionList() {
        return QuestionList;
    }

    public void setQuestionList(List<BookmarkQuestionList> questionList) {
        QuestionList = questionList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(QuestionList);
    }
}
