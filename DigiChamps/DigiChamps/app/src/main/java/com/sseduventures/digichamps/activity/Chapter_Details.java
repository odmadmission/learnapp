package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.loopj.android.http.AsyncHttpClient;
import com.sseduventures.digichamps.Model.Chapter_detail_Model;
import com.sseduventures.digichamps.Model.QuestionBank_Model;
import com.sseduventures.digichamps.Model.StudyNotes_Model;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.DoubtActivity;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.PersonalMentorActivity;
import com.sseduventures.digichamps.adapter.Chapter_Details_Adapter;
import com.sseduventures.digichamps.adapter.QNChap_Details_Adapter;
import com.sseduventures.digichamps.adapter.SNChap_Details_Adapter;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.config.AppController;
import com.sseduventures.digichamps.domain.ChapterDetailsSuccess;
import com.sseduventures.digichamps.domain.LearnChapSuccess;
import com.sseduventures.digichamps.domain.SetBookmarkResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.fragment.Fragment_PDF;
import com.sseduventures.digichamps.fragment.Fragment_QuesBank;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SessionManager;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.MyIntentService;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.model.GetCbtResponseModel;
import com.sseduventures.digichamps.webservice.model.GetPrtExamResponse;
import com.vdocipher.aegis.player.VdoPlayer;
import com.vdocipher.aegis.player.VdoPlayerFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class Chapter_Details extends FormActivity
        implements ServiceReceiver.Receiver {


    private CollapsingToolbarLayout collapsing_toolbar;

    private AppBarLayout appbar;

    RecyclerView recyclerView, snRecyclerView, qbRecyclerView;
    TextView chap_name;
    ArrayList<Chapter_detail_Model> list;
    List<QuestionBank_Model> snlist;
    List<StudyNotes_Model> qnlist;
    String file_resource;
    private VdoPlayer player;
    private VdoPlayerFragment playerFragment;
    private ImageButton playPauseButton, replayButton, errorButton;
    private TextView currTime, duration;
    private SeekBar seekBar;
    private ProgressBar bufferingIcon;
    private AsyncHttpClient client = new AsyncHttpClient();
    private String otp;
    private boolean isPlaying = false;
    private boolean controlsShowing = false;
    private boolean isLandscape = false;
    private int mLastSystemUiVis;
    private ShimmerFrameLayout mShimmerViewContainer, shimmer_view_container1,shimmer_view_container2;
    private RelativeLayout toolbar;
    CoordinatorLayout mCoordinatorLayout;
    private List<Chapter_detail_Model> movieList = new ArrayList<>();
    private Chapter_Details_Adapter adapter;
    private SNChap_Details_Adapter snAdapter;
    private QNChap_Details_Adapter qnAdapter;
    private ArrayList<String> pdfs_list, question_bank_list, pdfs_namelist, Quespdfname;
    private String TAG = "CHAPTER DETAILS";


    private String chap_id, sub_name, sub_id, flagsNewKey;

    private Button takeTest, prtTest_btn, descrption_doubt;

    private ImageView back_arrow;
    private CollapsingToolbarLayout collapsing_learn;
    private ImageView imageVideo, study_icon, study_icon_qb;
    private TextView video_txt, sn_txt, qb_txt,toolbar_text;
    private android.support.v7.widget.Toolbar toolbar_chap;
    private RelativeLayout rltv_pm, relMaster1123;


    private View.OnClickListener playerTapListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showControls(!controlsShowing);
            if (isLandscape) {

                showSystemUi(controlsShowing);
            }
        }
    };

    private SeekBar.OnSeekBarChangeListener seekbarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean fromUser) {

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            player.seekTo(seekBar.getProgress());
        }
    };
    private View.OnClickListener fullscreenToggleListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showFullScreen(!isLandscape);
            isLandscape = !isLandscape;
            int fsButtonResId = isLandscape ? R.drawable.ic_action_return_from_full_screen : R.drawable.ic_action_full_screen;
            ((ImageButton) (findViewById(R.id.fullscreen_toggle_button))).setImageResource(fsButtonResId);
        }
    };
    private View.OnSystemUiVisibilityChangeListener uiVisibilityListener = new View.OnSystemUiVisibilityChangeListener() {
        @Override
        public void onSystemUiVisibilityChange(int visibility) {


            int diff = mLastSystemUiVis ^ visibility;
            mLastSystemUiVis = visibility;
            if ((diff & View.SYSTEM_UI_FLAG_HIDE_NAVIGATION) != 0
                    && (visibility & View.SYSTEM_UI_FLAG_HIDE_NAVIGATION) == 0) {

                showControls(true);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_chapterdetails);

        logEvent(LogEventUtil.KEY_CHAPTER_DETAILS,
                LogEventUtil.EVENT_CHAPTER_PAGE);
        setModuleName(LogEventUtil.EVENT_CHAPTER_PAGE);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        shimmer_view_container1 = findViewById(R.id.shimmer_view_container1);
        shimmer_view_container2=findViewById(R.id.shimmer_view_container2);
        setUpReciever();
        int mentorIdNew = RegPrefManager.getInstance(this).getMentorid();
        String mentorId = String.valueOf(mentorIdNew);



        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(uiVisibilityListener);

        init();

        if (mentorId.equals(null) || mentorId.equalsIgnoreCase("null")) {
            relMaster1123.setVisibility(View.VISIBLE);
        } else {
            relMaster1123.setVisibility(View.GONE);
        }

        rltv_pm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Chapter_Details.this, PersonalMentorActivity.class);
                startActivity(in);

            }
        });


        final Intent intent = getIntent();
        chap_id = intent.getStringExtra("chapter_id");
        sub_name = intent.getStringExtra("subject_name");
        sub_id = intent.getStringExtra("subject_id");
        flagsNewKey = intent.getStringExtra("flagNewKey");
        if (flagsNewKey != null && !flagsNewKey.equalsIgnoreCase("null")) {


            if (flagsNewKey.equalsIgnoreCase("#3949ab")) {
                int colorCodeDarkBlue = Color.parseColor("#1976D2");
                collapsing_learn.setBackgroundColor(colorCodeDarkBlue);
                prtTest_btn.setBackgroundResource(R.drawable.prtbuttonblue);
                prtTest_btn.setBackground(getResources().getDrawable(R.drawable.abc_btn_selector_chap_details));
                prtTest_btn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.prticon_blue, 0, 0, 0);
                takeTest.setBackgroundResource(R.drawable.prtbuttonblue);
                takeTest.setBackground(getResources().getDrawable(R.drawable.abc_btn_selector_chap_details));
                takeTest.setCompoundDrawablesWithIntrinsicBounds(R.drawable.prticon_blue, 0, 0, 0);
                descrption_doubt.setBackgroundResource(R.drawable.prtbuttonblue);
                descrption_doubt.setBackground(getResources().getDrawable(R.drawable.abc_btn_selector_chap_details));
                descrption_doubt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.doubticon_blue, 0, 0, 0);
                imageVideo.setBackgroundResource(R.drawable.videoicon_blue);
                study_icon.setBackgroundResource(R.drawable.book_blue);
                study_icon_qb.setBackgroundResource(R.drawable.book_blue);
                video_txt.setTextColor(colorCodeDarkBlue);
                sn_txt.setTextColor(colorCodeDarkBlue);
                qb_txt.setTextColor(colorCodeDarkBlue);
                toolbar_chap.setBackgroundColor(colorCodeDarkBlue);

            } else if (flagsNewKey.equalsIgnoreCase("#ffb74d")) {
                int colorCodeDark = Color.parseColor("#f89b31");
                updateStatusBarColor("#f89b31");
                collapsing_learn.setBackgroundColor(colorCodeDark);
                prtTest_btn.setBackgroundResource(R.drawable.prtbuttonyellow);
                prtTest_btn.setBackground(getResources().getDrawable(R.drawable.abc_btn_selector_yellow_chap));
                prtTest_btn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.prticon_yellow, 0, 0, 0);
                takeTest.setBackgroundResource(R.drawable.prtbuttonyellow);
                takeTest.setBackground(getResources().getDrawable(R.drawable.abc_btn_selector_yellow_chap));
                takeTest.setCompoundDrawablesWithIntrinsicBounds(R.drawable.prticon_yellow, 0, 0, 0);
                descrption_doubt.setBackgroundResource(R.drawable.prtbuttonyellow);
                descrption_doubt.setBackground(getResources().getDrawable(R.drawable.abc_btn_selector_yellow_chap));
                descrption_doubt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.doubticon_yellow, 0, 0, 0);
                video_txt.setTextColor(colorCodeDark);
                sn_txt.setTextColor(colorCodeDark);
                qb_txt.setTextColor(colorCodeDark);
                imageVideo.setBackgroundResource(R.drawable.videoicon_yellow);
                study_icon.setBackgroundResource(R.drawable.booky_yellow);
                study_icon_qb.setBackgroundResource(R.drawable.booky_yellow);
                toolbar_chap.setBackgroundColor(colorCodeDark);
            }

        } else {

        }

        chapter_details();


        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Chapter_Details.this, Learn_Detail.class)
                        .putExtra("subject_name", sub_name).
                                putExtra("subject_id", sub_id).putExtra("flag", flagsNewKey));

                finish();


            }

        });



        takeTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getOnlineExam(chap_id);
            }
        });

        prtTest_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                getOflineExam(chap_id);


            }
        });

        descrption_doubt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String Is_Doubt = String.valueOf(RegPrefManager.getInstance(Chapter_Details.this).getIsDoubt());


               // if (Is_Doubt != null && !Is_Doubt.equals("null") && Is_Doubt.equalsIgnoreCase("true")) {
                    intent.putExtra("chapterdetails", "chapterdetails");
                    intent.putExtra("subjectid", sub_id);
                    startActivity(new Intent(Chapter_Details.this, DoubtActivity.class)
                            .putExtra("subjectid", sub_id)
                            .putExtra("chapId", chap_id));
               // }
                /*else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(Chapter_Details.this);
                    LayoutInflater layoutInflater = (LayoutInflater) Chapter_Details.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = layoutInflater.inflate(R.layout.dialog_school_zone, null);
                    builder.setView(view);

                    final AlertDialog dialog1 = builder.create();
                    TextView text = (TextView) view.findViewById(R.id.txt_na);
                    Button ok = (Button) view.findViewById(R.id.okay_school);
                    Typeface face = Typeface.createFromAsset(Chapter_Details.this.getAssets(),
                            "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");
                    text.setText("Please buy package to access \nthis service.Call us now at\n1800 212 4322");
                    text.setTypeface(face);
                    ok.setTypeface(face);
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog1.dismiss();

                        }
                    });
                    dialog1.show();
                    dialog1.setCanceledOnTouchOutside(false);

                    Window window = dialog1.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();
                    window.setAttributes(wlp);

                }*/
            }
        });

        adapter = new Chapter_Details_Adapter(list, Chapter_Details.this, chap_id, sub_name, sub_id);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        snAdapter = new SNChap_Details_Adapter(qnlist, Chapter_Details.this, chap_id, sub_id, Chapter_Details.this);
        snAdapter.notifyDataSetChanged();
        snRecyclerView.setAdapter(snAdapter);

        qnAdapter = new QNChap_Details_Adapter(snlist, Chapter_Details.this, chap_id, sub_id,
                Chapter_Details.this);
        qnAdapter.notifyDataSetChanged();
        qbRecyclerView.setAdapter(qnAdapter);


        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.subject_frame, new Fragment_PDF());
        ft.commitAllowingStateLoss();
        fm.executePendingTransactions();


        FragmentManager fm1 = getSupportFragmentManager();
        FragmentTransaction ft1 = fm1.beginTransaction();
        ft1.add(R.id.subject_frame, new Fragment_QuesBank());
        ft1.commitAllowingStateLoss();
        fm1.executePendingTransactions();

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        seekBar = (SeekBar) findViewById(R.id.seekbar);
        seekBar.setEnabled(false);
        currTime = (TextView) findViewById(R.id.current_time);
        duration = (TextView) findViewById(R.id.duration);
        playerFragment = (VdoPlayerFragment) getFragmentManager().findFragmentById(R.id.online_vdo_player_fragment);
        playPauseButton = (ImageButton) findViewById(R.id.play_pause_button);
        replayButton = (ImageButton) findViewById(R.id.replay_button);
        replayButton.setVisibility(View.INVISIBLE);
        errorButton = (ImageButton) findViewById(R.id.error_icon);
        errorButton.setEnabled(false);
        errorButton.setVisibility(View.INVISIBLE);
        bufferingIcon = (ProgressBar) findViewById(R.id.loading_icon);
        showLoadingIcon(false);
        showControls(true);


        if (savedInstanceState != null) {
            otp = savedInstanceState.getString("otp", null);

        }
    }

    private void chapter_details() {

        if (AppUtil.isInternetConnected(this)) {


            Bundle bun = new Bundle();
            bun.putString("chap_id", chap_id);
            NetworkService.startGetChapterDetails(this, mServiceReceiver, bun);
        } else {
            Intent i = new Intent(Chapter_Details.this, Internet_Activity.class);
            startActivity(i);
            overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
        }
    }

    public void updateStatusBarColor(String color) {// Color must be in hexadecimal fromat
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.
                    FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    protected void onStop() {

        super.onStop();
        mServiceReceiver.setReceiver(null);
        disablePlayerUI();
        if (player != null) {
            player.stop();
        }
        player = null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        hideDialog();
        super.onSaveInstanceState(outState);
        if (otp != null) {
            outState.putString("otp", otp);
        }
    }


    private void showControls(boolean show) {

        int visibility = show ? View.VISIBLE : View.INVISIBLE;
        playPauseButton.setVisibility(visibility);
        (findViewById(R.id.bottom_panel)).setVisibility(visibility);
        controlsShowing = show;
    }

    private void disablePlayerUI() {
        showControls(false);
        showLoadingIcon(false);
        playPauseButton.setEnabled(false);
        currTime.setEnabled(false);
        seekBar.setEnabled(false);
        replayButton.setEnabled(false);
        replayButton.setVisibility(View.INVISIBLE);
        (findViewById(R.id.player_region)).setOnClickListener(null);
    }


    private void showLoadingIcon(final boolean showIcon) {
        if (showIcon) {
            bufferingIcon.setVisibility(View.VISIBLE);
            bufferingIcon.bringToFront();
        } else {
            bufferingIcon.setVisibility(View.INVISIBLE);
            bufferingIcon.requestLayout();
        }
    }

    private void showFullScreen(boolean show) {


        if (show) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
            (findViewById(R.id.online_vdo_player_fragment)).setLayoutParams(new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
            findViewById(R.id.player_region).setFitsSystemWindows(true);
            toolbar.setVisibility(View.GONE);
            showSystemUi(false);
            showControls(true);

        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            (findViewById(R.id.online_vdo_player_fragment)).setLayoutParams(new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            findViewById(R.id.player_region).setFitsSystemWindows(false);
            findViewById(R.id.player_region).setPadding(0, 0, 0, 0);
            showSystemUi(true);
            toolbar.setVisibility(View.VISIBLE);
        }

    }

    private void showSystemUi(boolean show) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (!show) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            } else {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            }
        }
    }

    @Override
    protected void onResume() {
        mShimmerViewContainer.startShimmer();
        shimmer_view_container1.startShimmer();
        shimmer_view_container2.startShimmer();
        super.onResume();

    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmer();
        shimmer_view_container1.stopShimmer();
        shimmer_view_container2.stopShimmer();
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            showFullScreen(true);
            int fsButtonResId = R.drawable.ic_action_return_from_full_screen;
            ((ImageButton) (findViewById(R.id.fullscreen_toggle_button))).setImageResource(fsButtonResId);

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            showFullScreen(false);
            int fsButtonResId = R.drawable.ic_action_full_screen;
            ((ImageButton) (findViewById(R.id.fullscreen_toggle_button))).setImageResource(fsButtonResId);

        }
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        otp = savedInstanceState.getString("otp");
    }

    public void init() {

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        back_arrow = (ImageView) findViewById(R.id.learn_arrow);
        toolbar_text=(TextView)findViewById(R.id.toolbar_text);
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        collapsing_learn = (CollapsingToolbarLayout) findViewById(R.id.collapsing_learn);

        relMaster1123 = (RelativeLayout) findViewById(R.id.relMaster1123);
        rltv_pm = (RelativeLayout) findViewById(R.id.rltv_pm);

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.chapter_details);
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        snRecyclerView = (RecyclerView) findViewById(R.id.recycler_sn);
        snRecyclerView.setHasFixedSize(true);

        qbRecyclerView = (RecyclerView) findViewById(R.id.recycler_qb);
        qbRecyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this);
        qbRecyclerView.setLayoutManager(layoutManager1);

        LinearLayoutManager layoutManager2 = new LinearLayoutManager(this);
        snRecyclerView.setLayoutManager(layoutManager2);

        list = new ArrayList<>();
        qnlist = new ArrayList<>();
        snlist = new ArrayList<>();


        adapter = new Chapter_Details_Adapter(list, Chapter_Details.this, chap_id, sub_name, sub_id);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        snAdapter = new SNChap_Details_Adapter(qnlist, Chapter_Details.this, chap_id, sub_id, Chapter_Details.this);
        snAdapter.notifyDataSetChanged();
        snRecyclerView.setAdapter(snAdapter);

        qnAdapter = new QNChap_Details_Adapter(snlist, Chapter_Details.this, chap_id, sub_id,
                Chapter_Details.this);
        qnAdapter.notifyDataSetChanged();
        qbRecyclerView.setAdapter(qnAdapter);


        chap_name = (TextView) findViewById(R.id.txt_heading_name);
        takeTest = (Button) findViewById(R.id.descrption);
        prtTest_btn = (Button) findViewById(R.id.descrption_prt);

        descrption_doubt = (Button) findViewById(R.id.descrption_doubt);
        imageVideo = (ImageView) findViewById(R.id.imageVideo);
        video_txt = (TextView) findViewById(R.id.video_txt);
        study_icon = (ImageView) findViewById(R.id.study_icon);
        sn_txt = (TextView) findViewById(R.id.sn_txt);
        study_icon_qb = (ImageView) findViewById(R.id.study_icon_qb);
        qb_txt = (TextView) findViewById(R.id.qb_txt);
        toolbar_chap = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar_chap);

        collapsing_toolbar = findViewById(R.id.collapsing_learn);
        appbar=(AppBarLayout) findViewById(R.id.appBarLayout_learn);
        pdfs_list = new ArrayList<>();
        pdfs_namelist = new ArrayList<>();
        question_bank_list = new ArrayList<>();
        Quespdfname = new ArrayList<>();


    }


    @Override
    public void onBackPressed() {
        final Intent localIntent = new Intent(this, MyIntentService.class);
        localIntent.setAction(Constants.ACTION_SET_ALARM);
        startService(localIntent);
        startActivity(new Intent(Chapter_Details.this,
                Learn_Detail.class)
                .putExtra("subject_name",
                        sub_name).putExtra("subject_id", sub_id).
                        putExtra("flag", flagsNewKey));

        finish();


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public ArrayList<String> getPdfs_list() {
        return pdfs_list;
    }

    public ArrayList<String> get_pdfname() {
        return pdfs_namelist;
    }

    public ArrayList<String> getQuestion_bank_list() {
        return question_bank_list;
    }

    public ArrayList<String> get_Quespdfname() {
        return Quespdfname;
    }

    private boolean limit_reached = false;
    private String exam_ID,
            exam_name, chap_ids, exam_ID_OFF,
            exam_name_OFF, chap_ids_OFF, error_message;
    private int attempts_nos,
            student_attempts,
            attempts_nos_OFF, student_attempts_OFF;


    public void setUnsetBookMark(String dataid, String dataType) {
        if (AppUtil.isInternetConnected(this)) {

            Bundle bun = new Bundle();
            bun.putString("dataType", dataType);
            bun.putString("dataId", dataid);

            NetworkService.startActionGetSetUsetBookmark(this, mServiceReceiver, bun);
        } else {
            Intent in = new Intent(this, Internet_Activity.class);
            startActivity(in);
        }
    }

    private void getOflineExam(String chapId) {
        showDialog(null, "Please wait");

        Bundle bundle = new Bundle();
        bundle.putString("Chapter_ID", chapId);

        if (AppUtil.isInternetConnected(this)) {

            showDialog(null, "please wait");
            NetworkService.startGetPrtExam(this,
                    mServiceReceiver, bundle);

        } else {
            AskOptionDialogNew("Please Check Your Internet Connection").show();
        }
    }


    private void getOnlineExam(String chapID) {

        showDialog(null, "Please wait");

        Bundle bundle = new Bundle();
        bundle.putString("Chapter_ID", chapID);

        if (AppUtil.isInternetConnected(this)) {

            showDialog(null, "please wait");
            NetworkService.startGetCbtExam(this,
                    mServiceReceiver, bundle);

        } else {
            AskOptionDialogNew("Please Check Your Internet Connection").show();
        }


    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        switch (resultCode) {
            case ResponseCodes.NO_INTERNET:


                break;

            case ResponseCodes.UNEXPECTED_ERROR:


                break;


            case ResponseCodes.EXCEPTION:

                break;

            case ResponseCodes.SUCCESS:

                LearnChapSuccess success = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if (success != null) {

                    chap_name.setText(success.getChapter_Name());
                    Toast.makeText(this, "Success" + success, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Not Success", Toast.LENGTH_SHORT).show();
                }

                break;

            case ResponseCodes.SUCCESSPRT:
                ArrayList<GetPrtExamResponse> getPrtExamResponses = resultData.getParcelableArrayList(IntentHelper.RESULT_DATA);
                hideDialog();


                for (int k = 0; k < getPrtExamResponses.size(); k++) {
                    exam_ID_OFF = String.valueOf(getPrtExamResponses.get(k).getExam_ID());
                    exam_name_OFF = getPrtExamResponses.get(k).getExam_Name();
                    chap_ids_OFF = String.valueOf(getPrtExamResponses.get(k).getChapter_Id());
                    attempts_nos_OFF = getPrtExamResponses.get(k).getAttempt_nos();

                    student_attempts_OFF = getPrtExamResponses.get(k).getStudent_Attempt();
                }

                Intent intent = new Intent(Chapter_Details.this,
                        Exam_desc.class);
                intent.putExtra("exam_id", exam_ID_OFF);
                intent.putExtra("sub_name", sub_name);
                intent.putExtra("sub_id", sub_id);

                intent.putExtra("module_type", Constants.PROGRESS_PRT);
                intent.putExtra("chap_id", chap_ids_OFF);
                intent.putExtra("isOffline", true);
                intent.putExtra("type", "PRT");
                intent.putExtra("flags", flagsNewKey);

                startActivity(intent);

                finish();
                break;
            /*}*/


            case ResponseCodes.STATUS:
                hideDialog();

                ArrayList<GetCbtResponseModel> getCbtResponseModel = resultData.getParcelableArrayList(IntentHelper.RESULT_DATA);
                hideDialog();

                for (int i = 0; i < getCbtResponseModel.size(); i++) {
                    exam_ID = String.valueOf(getCbtResponseModel.get(i).getExam_ID());
                    exam_name = getCbtResponseModel.get(i).getExam_Name();
                    chap_ids = String.valueOf(getCbtResponseModel.get(i).getChapter_Id());
                    attempts_nos = getCbtResponseModel.get(i).getAttempt_nos();
                    student_attempts = getCbtResponseModel.get(i).getStudent_Attempt();
                }


                limit_reached = false;
                Intent in = new Intent(Chapter_Details.this,
                        Exam_desc.class);
                in.putExtra("exam_id", exam_ID);
                in.putExtra("sub_name", sub_name);
                in.putExtra("sub_id", sub_id);

                in.putExtra("module_type", Constants.PROGRESS_CBT);
                in.putExtra("chap_id", chap_id);
                in.putExtra("isOffline", false);
                in.putExtra("type", "CBT");
                in.putExtra("flags", flagsNewKey);
                startActivity(in);

                finish();

                break;

            case ResponseCodes.BOOKMARKED:
                SetBookmarkResponse msuccess = resultData.getParcelable(IntentHelper.RESULT_DATA);


                if (resultData.getString("dataType").equalsIgnoreCase("3")) {

                    ArrayList<StudyNotes_Model> li = new ArrayList<>();
                    for (int i = 0; i < qnlist.size(); i++) {
                        String id = resultData.getString("dataId");

                        if (id.equals(qnlist.get(i).getModuleId())) {
                            qnlist.get(i).setIsSNBookmarked("true");

                        }

                        li.add(qnlist.get(i));

                    }

                    Toast.makeText(this, "" + msuccess.getSuccess().getMessage(), Toast.LENGTH_SHORT).show();


                    qnlist.clear();
                    qnlist.addAll(li);
                    snAdapter.notifyDataSetChanged();

                } else if (resultData.getString("dataType").equalsIgnoreCase("1")) {

                    ArrayList<QuestionBank_Model> li = new ArrayList<>();
                    for (int i = 0; i < snlist.size(); i++) {
                        String id = resultData.getString("dataId");

                        if (id.equals(snlist.get(i).getQbmoduleId())) {
                            snlist.get(i).setIsQBBookMarked("true");

                        }

                        li.add(snlist.get(i));

                    }

                    Toast.makeText(this, "" + msuccess.getSuccess().getMessage(), Toast.LENGTH_SHORT).show();


                    snlist.clear();
                    snlist.addAll(li);
                    qnAdapter.notifyDataSetChanged();

                }


                break;
            case ResponseCodes.UNBOOKMARKED:
                SetBookmarkResponse mmsuccess =
                        resultData.getParcelable(IntentHelper.RESULT_DATA);



                if (resultData.getString("dataType").equalsIgnoreCase("3")) {

                    ArrayList<StudyNotes_Model> l = new ArrayList<>();
                    for (int i = 0; i < qnlist.size(); i++) {
                        String id = resultData.getString("dataId");

                        if (id.equals(qnlist.get(i).getModuleId())) {
                            qnlist.get(i).setIsSNBookmarked("false");

                        }

                        l.add(qnlist.get(i));

                    }
                    Toast.makeText(this, "" + mmsuccess.getSuccess().getMessage(), Toast.LENGTH_SHORT).show();

                    qnlist.clear();
                    qnlist.addAll(l);
                    snAdapter.notifyDataSetChanged();

                } else if (resultData.getString("dataType").equalsIgnoreCase("1")) {


                    ArrayList<QuestionBank_Model> l = new ArrayList<>();
                    for (int i = 0; i < snlist.size(); i++) {
                        String id = resultData.getString("dataId");

                        if (id.equals(snlist.get(i).getQbmoduleId())) {
                            snlist.get(i).setIsQBBookMarked("false");

                        }

                        l.add(snlist.get(i));

                    }
                    Toast.makeText(this, "" + mmsuccess.getSuccess().getMessage(), Toast.LENGTH_SHORT).show();

                    snlist.clear();
                    snlist.addAll(l);
                    qnAdapter.notifyDataSetChanged();

                }


                break;


            case ResponseCodes.FAILURE:

                break;

            case ResponseCodes.CHAPTERDETAILS:

                ChapterDetailsSuccess mmmsuccess =
                        resultData.getParcelable(IntentHelper.RESULT_DATA);


                final String chapter_name = mmmsuccess.getSuccess().getChapter_Name();


                for (int j = 0; j < mmmsuccess.getSuccess().getQuesbank().size(); j++) {
                    question_bank_list.add(mmmsuccess.getSuccess().getQuesbank().get(j).getQuestion_Pdfs());
                    Quespdfname.add(mmmsuccess.getSuccess().getQuesbank().get(j).getModulename());


                    String snPDF = mmmsuccess.getSuccess().getQuesbank().get(j).getQuestion_Pdfs();
                    String moduleName = mmmsuccess.getSuccess().getQuesbank().get(j).getModulename();
                    String isSNBookmarked = String.valueOf(mmmsuccess.getSuccess().getQuesbank().get(j).getIs_question_bookmarked());
                    String moduleId = String.valueOf(mmmsuccess.getSuccess().getQuesbank().get(j).getModuleIDss());

                    snlist.add(new QuestionBank_Model(moduleName, snPDF, moduleId, isSNBookmarked));
                }
                shimmer_view_container1.setVisibility(View.GONE);
                shimmer_view_container2.setVisibility(View.GONE);
                qnAdapter = new QNChap_Details_Adapter(snlist, Chapter_Details.this, chap_id, sub_id,
                        Chapter_Details.this);
                qnAdapter.notifyDataSetChanged();
                qbRecyclerView.setAdapter(qnAdapter);
                //pdfs

                for (int k = 0; k < mmmsuccess.getSuccess().getPdfs().size(); k++) {
                    pdfs_list.add(mmmsuccess.getSuccess().getPdfs().get(k).getUrl());
                    pdfs_namelist.add(mmmsuccess.getSuccess().getPdfs().get(k).getModulename());

                    String pdfName = mmmsuccess.getSuccess().getPdfs().get(k).getModulename();
                    String qbPDF = mmmsuccess.getSuccess().getPdfs().get(k).getUrl();
                    String isQBBookmarked = String.valueOf(mmmsuccess.getSuccess().getPdfs().get(k).getIs_studynotebookmarked());
                    String moduleID = String.valueOf(mmmsuccess.getSuccess().getPdfs().get(k).getModuleID());
                    qnlist.add(new StudyNotes_Model(pdfName, qbPDF, moduleID, isQBBookmarked));

                }

                shimmer_view_container1.setVisibility(View.GONE);
                shimmer_view_container2.setVisibility(View.GONE);
                snAdapter = new SNChap_Details_Adapter(qnlist, Chapter_Details.this, chap_id, sub_id,
                        Chapter_Details.this);
                snAdapter.notifyDataSetChanged();
                snRecyclerView.setAdapter(snAdapter);


                for (int i = 0; i < mmmsuccess.getSuccess().getChapterModules().size(); i++) {

                    String Module_Id = String.valueOf(mmmsuccess.getSuccess().getChapterModules().get(i).getModule_Id());
                    String Module_Title = mmmsuccess.getSuccess().getChapterModules().get(i).getModule_Title();
                    String Description = mmmsuccess.getSuccess().getChapterModules().get(i).getDescription();
                    String Module_Image = mmmsuccess.getSuccess().getChapterModules().get(i).getModule_Image();
                    String Image_Key = mmmsuccess.getSuccess().getChapterModules().get(i).getImage_Key();
                    String Is_Avail = String.valueOf(mmmsuccess.getSuccess().getChapterModules().get(i).getIs_Avail());
                    String pdf_file = mmmsuccess.getSuccess().getChapterModules().get(i).getPdf_file();
                    String Is_Free = String.valueOf(mmmsuccess.getSuccess().getChapterModules().get(i).getIs_Free());
                    String Validity = mmmsuccess.getSuccess().getChapterModules().get(i).getValidity();
                    String Question_Pdf = mmmsuccess.getSuccess().getChapterModules().get(i).getQuestion_Pdf();
                    String No_Of_Question = String.valueOf(mmmsuccess.getSuccess().getChapterModules().get(i).getNo_Of_Question());
                    String Is_Free_Test = String.valueOf(mmmsuccess.getSuccess().getChapterModules().get(i).getIs_Free());
                    String media_id = mmmsuccess.getSuccess().getChapterModules().get(i).getMedia_Id();
                    String temp_id = mmmsuccess.getSuccess().getChapterModules().get(i).getTemplate_id();
                    String isExpire = String.valueOf(mmmsuccess.getSuccess().getChapterModules().get(i).getIs_Expire());
                    String isBookmarked = String.valueOf(mmmsuccess.getSuccess().getChapterModules().get(i).getIs_Video_bookmarked());

                    file_resource = media_id + temp_id;

                    list.add(new Chapter_detail_Model(Module_Id, Module_Title, Description,
                            Module_Image, Image_Key, Is_Avail, pdf_file, Is_Free, Validity, isExpire,
                            Question_Pdf, No_Of_Question,
                            Is_Free_Test, media_id, isBookmarked));

                }
                if (list.size() != 0) {
                } else {


                }

                chap_name.setText(chapter_name);
                collapsing_toolbar.setTitleEnabled(false);
                collapsing_toolbar.setTitle(chapter_name);
                adapter = new Chapter_Details_Adapter(list, Chapter_Details.this, chap_id, sub_name, sub_id);
                recyclerView.setAdapter(adapter);

                appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                    boolean isShow = true;
                    int scrollRange = -1;

                    @Override
                    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                        if (scrollRange == -1) {
                            scrollRange = appBarLayout.getTotalScrollRange();
                        }
                        if (scrollRange + verticalOffset == 0) {
                            toolbar_text.setText(chapter_name);
                            isShow = true;
                        } else if (isShow) {
                            toolbar_text.setText(" ");
                            isShow = false;
                        }
                    }
                });

                break;
        }
    }


    private android.app.AlertDialog AskOptionDialogNew(String msg) {
        android.app.AlertDialog myQuittingDialogBox = new android.app.AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })

                .create();
        return myQuittingDialogBox;
    }

}
