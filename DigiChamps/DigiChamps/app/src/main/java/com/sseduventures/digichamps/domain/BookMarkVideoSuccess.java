package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 9/1/2018.
 */

public class BookMarkVideoSuccess implements Parcelable{

    private List<BookMarkVideoList> VideoList;

    protected BookMarkVideoSuccess(Parcel in) {
    }

    public static final Creator<BookMarkVideoSuccess> CREATOR = new Creator<BookMarkVideoSuccess>() {
        @Override
        public BookMarkVideoSuccess createFromParcel(Parcel in) {
            return new BookMarkVideoSuccess(in);
        }

        @Override
        public BookMarkVideoSuccess[] newArray(int size) {
            return new BookMarkVideoSuccess[size];
        }
    };

    public List<BookMarkVideoList> getVideoList() {
        return VideoList;
    }

    public void setVideoList(List<BookMarkVideoList> videoList) {
        VideoList = videoList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
