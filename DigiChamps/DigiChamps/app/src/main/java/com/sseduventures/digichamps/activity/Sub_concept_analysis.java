package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.PersonalMentorActivity;
import com.sseduventures.digichamps.adapter.AdapterForSubConceptAnalyticsList;
import com.sseduventures.digichamps.domain.SubChapterList;
import com.sseduventures.digichamps.domain.SubConceptResponse;
import com.sseduventures.digichamps.domain.SubSuccessList;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import java.util.ArrayList;
import java.util.List;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Sub_concept_analysis extends FormActivity implements ServiceReceiver.Receiver {
    RecyclerView rvSubConceptAnalysis;
    String user_id;
    List<SubSuccessList> subConceptList;
    List<SubChapterList> subChapList;
    Spinner spinner_sub;
    ArrayList<String> spinnerSubjectName;
    ArrayAdapter<String> adapterSub;
    private AdapterForSubConceptAnalyticsList rvChapListAdapter;
    int classId;
    ImageView lead_back_image;
    TextView weak_tv;
    CardView weak_msg;
    ArrayList<String> sub;
    private SpotsDialog dialog;
    private RelativeLayout rltv_pm, relMaster1123;
    private String mentorId;
    private TextView pm;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private SubConceptResponse success;
    private ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_sub_concept_analysis);
        setModuleName(LogEventUtil.EVENT_sub_concept_analysis);
        logEvent(LogEventUtil.KEY_sub_concept_analysis,LogEventUtil.EVENT_sub_concept_analysis);

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        setTheme(R.style.AppTheme);

        dialog = new SpotsDialog(this, "Fun+Education", R.style.Custom);

        subConceptList = new ArrayList<>();
        subChapList = new ArrayList<>();
        spinnerSubjectName = new ArrayList<>();
        sub = new ArrayList<>();
        pm = (TextView) findViewById(R.id.pm);
        rvSubConceptAnalysis = (RecyclerView) findViewById(R.id.recycler_sub_concept);
        spinner_sub = (Spinner) findViewById(R.id.spinner_sub);
        lead_back_image = (ImageView) findViewById(R.id.lead_back_image);
        weak_tv = (TextView) findViewById(R.id.weak_tv);
        rvSubConceptAnalysis.setHasFixedSize(true);
        weak_msg = (CardView) findViewById(R.id.weak_msg);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        relMaster1123 = (RelativeLayout) findViewById(R.id.relMaster1123);
        rltv_pm = (RelativeLayout) findViewById(R.id.rltv_pm);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvSubConceptAnalysis.setLayoutManager(layoutManager);

        rvChapListAdapter = new AdapterForSubConceptAnalyticsList(subChapList, Sub_concept_analysis.this);
        rvSubConceptAnalysis.setAdapter(rvChapListAdapter);
        user_id = String.valueOf(RegPrefManager.getInstance(this).getRegId());

        classId = (int) RegPrefManager.getInstance(this).getclassid();
        mentorId = String.valueOf(RegPrefManager.getInstance(this).getMentorid());
        getSubConceptData();


        adapterSub = new ArrayAdapter<String>(Sub_concept_analysis.this,
                android.R.layout.simple_spinner_dropdown_item, sub);
        adapterSub.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        spinner_sub.setAdapter(adapterSub);


        if (mentorId == null || mentorId.equalsIgnoreCase("null")) {
            relMaster1123.setVisibility(View.VISIBLE);
            pm.setVisibility(View.VISIBLE);
        } else {
            relMaster1123.setVisibility(View.GONE);
            pm.setVisibility(View.GONE);
        }

        rltv_pm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Sub_concept_analysis.this, PersonalMentorActivity.class);
                startActivity(in);

            }
        });


        lead_back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sub_concept_analysis.super.onBackPressed();
                finish();
            }
        });


        spinner_sub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String subName = spinner_sub.getItemAtPosition(spinner_sub.getSelectedItemPosition()).toString();

                setChapterList(subName);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void setChapterList(String subName) {

        subChapList.clear();

        for (int i = 0; i < subConceptList.size(); i++) {
            if (subConceptList.get(i).getSubject().equals(subName)) {

                subChapList.addAll(subConceptList.get(i).getChapterList());

            }
        }

        rvChapListAdapter.notifyDataSetChanged();
        if (subChapList.size() == 0) {
            weak_msg.setVisibility(View.VISIBLE);
            rvSubConceptAnalysis.setVisibility(View.GONE);
        } else {
            weak_msg.setVisibility(View.GONE);
            rvSubConceptAnalysis.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void getSubConceptData() {
        if (AppUtil.isInternetConnected(this)) {

            NetworkService.startActionGetSubConceptData(this, mServiceReceiver);

        } else {
            Intent in = new Intent(Sub_concept_analysis.this, Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        hideDialog();
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);


                if (success != null && success.getSuccess() != null
                        && success.getSuccess().size() > 0) {
                    subConceptList.clear();
                    subConceptList.addAll(success.getSuccess());
                    sub.clear();
                    sub.addAll(success.getSubjectList());

                    adapterSub.notifyDataSetChanged();
                    spinner_sub.setSelection(0);

                }
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(Sub_concept_analysis.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(Sub_concept_analysis.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(Sub_concept_analysis.this, Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }

}
