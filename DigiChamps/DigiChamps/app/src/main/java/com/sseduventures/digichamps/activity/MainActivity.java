package com.sseduventures.digichamps.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.fragment.AssignmentFragment;
import com.sseduventures.digichamps.fragment.ForumFragment;
import com.sseduventures.digichamps.fragment.HomeFragment;
import com.sseduventures.digichamps.fragment.InformationFragment;
import com.sseduventures.digichamps.fragment.ScheduleFragment;
import com.sseduventures.digichamps.fragment.TopperFragment;


public class MainActivity extends FormActivity {

    RelativeLayout reSchedules,relInfo,relHome,relAssignment,relForum,relTooper,relHome0;
    ImageView imgNavMenu,imvUser0;
    TextView tv_school,txtInfo,txtSchedules,txtAssignment,txtForum,txtTooper,tv_exit;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    Boolean isDrawerEnable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logEvent(LogEventUtil.KEY_SchoolZoneMain_PAGE,
                LogEventUtil.EVENT_SchoolZoneMain_PAGE);
        if(savedInstanceState == null){
            Fragment mFragment = null;
            mFragment = new HomeFragment();
            android.support.v4.app.FragmentTransaction fragmentTransactionHome = getSupportFragmentManager().beginTransaction();
            //fragmentTransactionHome.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right);
            fragmentTransactionHome.replace(R.id.frame_container,mFragment,HomeFragment.class.getName());
            fragmentTransactionHome.addToBackStack(HomeFragment.class.getName());
            fragmentTransactionHome.commit();
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        imgNavMenu = (ImageView) findViewById(R.id.imgNavMenu);
        txtSchedules = (TextView) findViewById(R.id.txtSchedules);
        txtForum = (TextView) findViewById(R.id.txtForum);
        tv_school = (TextView) findViewById(R.id.tv_school);
        txtInfo = (TextView) findViewById(R.id.txtInfo);
        txtTooper = (TextView) findViewById(R.id.txtTooper);
        relHome = (RelativeLayout) findViewById(R.id.relHome);
        txtAssignment = (TextView) findViewById(R.id.txtAssignment);
        relForum = (RelativeLayout) findViewById(R.id.relForum);
        relTooper = (RelativeLayout) findViewById(R.id.relTooper);
        relHome0=(RelativeLayout) findViewById(R.id.relHome0);
        tv_exit = (TextView) findViewById(R.id.tv_exit);
        relInfo=(RelativeLayout)findViewById(R.id.relInfo);
        reSchedules=(RelativeLayout)findViewById(R.id.reSchedules);
        relAssignment=(RelativeLayout)findViewById(R.id.relAssignment);

        relHome0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), NewDashboardActivity.class);
                startActivity(intent);
            }


        });


        tv_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), NewDashboardActivity.class);
                startActivity(intent);
                finish();
            }


        });

        relHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new HomeFragment();
                changFragment(mFragment);
            }
        });

        tv_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new HomeFragment();
                changFragment(mFragment);
            }
        });


        txtInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new InformationFragment();
                changFragment(mFragment);
            }
        });

        txtSchedules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new ScheduleFragment();
                changFragment(mFragment);
            }
        });

        txtAssignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new AssignmentFragment();
                changFragment(mFragment);
            }
        });

        txtForum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new ForumFragment();
                changFragment(mFragment);
            }
        });

        txtTooper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new TopperFragment();
                changFragment(mFragment);
            }
        });

        relInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new InformationFragment();
                changFragment(mFragment);
            }
        });

        reSchedules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new ScheduleFragment();
                changFragment(mFragment);
            }
        });

        relAssignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new AssignmentFragment();
                changFragment(mFragment);
            }
        });

        relForum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new ForumFragment();
                changFragment(mFragment);
            }
        });

        relTooper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mFragment = new TopperFragment();
                changFragment(mFragment);
            }
        });


        imgNavMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                    setDrawerEnable(false);
                } else {
                    setDrawerEnable(true);
                    mDrawerLayout.openDrawer(Gravity.LEFT);

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(MainActivity.this, NewDashboardActivity.class);
        startActivity(i);
        finish();
    }


    public void changFragment(Fragment mFragment){

        android.support.v4.app.FragmentTransaction fragmentTransactionHome = getSupportFragmentManager().beginTransaction();
        fragmentTransactionHome.replace(R.id.frame_container,mFragment,InformationFragment.class.getName());
        fragmentTransactionHome.addToBackStack(InformationFragment.class.getName());
        fragmentTransactionHome.commit();
        setDrawerEnable(false);
    }

    public void setDrawerEnable(Boolean enabled){
        isDrawerEnable=enabled;
        if(isDrawerEnable){
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }else{
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }
}
