package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 9/5/2018.
 */

public class SetBookmarkResponse implements Parcelable{

    private SetBookmarkSuccess Success;

    protected SetBookmarkResponse(Parcel in) {
        Success = in.readParcelable(SetBookmarkSuccess.class.getClassLoader());
    }

    public static final Creator<SetBookmarkResponse> CREATOR = new Creator<SetBookmarkResponse>() {
        @Override
        public SetBookmarkResponse createFromParcel(Parcel in) {
            return new SetBookmarkResponse(in);
        }

        @Override
        public SetBookmarkResponse[] newArray(int size) {
            return new SetBookmarkResponse[size];
        }
    };

    public SetBookmarkSuccess getSuccess() {
        return Success;
    }

    public void setSuccess(SetBookmarkSuccess success) {
        Success = success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(Success, i);
    }
}
