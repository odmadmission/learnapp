package com.sseduventures.digichamps.activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatConfig;
import com.freshchat.consumer.sdk.FreshchatUser;
import com.freshchat.consumer.sdk.activity.ChannelListActivity;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;
import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.Exam_desc;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.activity.MainActivity;

import com.sseduventures.digichamps.activity.Order;
import com.sseduventures.digichamps.activity.PackageActivityNew;
import com.sseduventures.digichamps.activity.ScholarShipActivity;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.domain.PackObject;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.frags.AnalyticsFragment;
import com.sseduventures.digichamps.frags.BookmarkFragment;
import com.sseduventures.digichamps.frags.DashboardFragment;
import com.sseduventures.digichamps.frags.DictonaryFragment;
import com.sseduventures.digichamps.frags.FeedFragment;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.intentpicker.IntentPickerSheetView;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AlarmManagerUtils;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.CircleTransform;
import com.sseduventures.digichamps.utils.IntentHelper;

import java.util.Collections;
import java.util.Comparator;
import java.util.StringTokenizer;

public class NewDashboardActivity extends FormActivity implements ServiceReceiver.Receiver{

    private static final String TAG=NewDashboardActivity.class.getSimpleName();
    private TextView news_feed,
            dictonary,
            mentortest,
            scholarship,
            notification,
            settings,
            getName,
            chat_with_us,freeTrial,packages,trial_header;
    private RelativeLayout freetrail,rlv_nav;

    DrawerLayout mDrawerLayout;
    private ImageView nav_img;
    private int fragmentId;
    private Fragment fragment;
    private Toolbar toolbar;
    private TextView coinsCount;
    protected BottomSheetLayout bottomSheetLayout;


    private String trialDate;
    private ActionBarDrawerToggle actionBarDrawerToggle;

    private IntentFilter filter;

    private static final String APP_ID = "515d67d7-5e28-40ce-a6b0-7783baf1ebbb";
    private static final String APP_KEY = "754a44c6-6202-44e9-93cb-f5f58b7a73a4";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_dashboard);
        setModuleName(LogEventUtil.EVENT_DASHBOARD);
        AlarmManagerUtils.setSyncAlarmMDM(this,false);
        logEvent(LogEventUtil.KEY_DASHBOARD,
                LogEventUtil.EVENT_DASHBOARD);
        setModuleName(LogEventUtil.EVENT_NEW_DASHBOARD_PAGE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        FreshchatConfig freshchatConfig = new
                FreshchatConfig(APP_ID, APP_KEY);
        Freshchat.getInstance(getApplicationContext()).init(freshchatConfig);
        setUpNavigationView();


        setUpReciever();

        String imageString = RegPrefManager.getInstance(this).getImageString();
        String userName = RegPrefManager.getInstance(this).getUserName();


        news_feed = (TextView) findViewById(R.id.news_feed);
        dictonary = (TextView) findViewById(R.id.dictonary);
        mentortest = (TextView) findViewById(R.id.mentortest);
        scholarship = (TextView) findViewById(R.id.scholarship);
        notification = (TextView) findViewById(R.id.notification);
        chat_with_us = findViewById(R.id.chat_with_us);
        settings = (TextView) findViewById(R.id.settings);
        freeTrial = (TextView) findViewById(R.id.freeTrial);
        trial_header = findViewById(R.id.trial_header);
        packages =  (TextView) findViewById(R.id.packages);
        freetrail = (RelativeLayout) findViewById(R.id.freetrail);
        getName = (TextView) findViewById(R.id.getName);
        nav_img = (ImageView) findViewById(R.id.nav_img);
        rlv_nav = (RelativeLayout) findViewById(R.id.rlv_nav);
        bottomSheetLayout = (BottomSheetLayout) findViewById(R.id.bottomsheet);
        coinsCount=findViewById(R.id.coinsCount);



        if (userName!=null){
            getName.setText(userName);
        }





        if(imageString!=null){


            String correctImage = AppConfig.SRVR_URL + "/Images/Profile/" +imageString;
            Picasso.with(getApplicationContext()).load(correctImage)
                    .transform(new CircleTransform()).into(nav_img);
        }

        else{

            Picasso.with(getApplicationContext()).load(imageString).
                    placeholder(R.drawable.student).
                    into(nav_img);
        }

        news_feed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                   logEvent(LogEventUtil.KEY_SchoolZoneCode_PAGE,
                           LogEventUtil.EVENT_SchoolZoneCode_PAGE);

                //status

                if(AppUtil.isInternetConnected(NewDashboardActivity.this)){
                    boolean isSectionEnabled =
                            RegPrefManager.getInstance(NewDashboardActivity.this).getIsSectionEnabled();
                    boolean Is_School = RegPrefManager.getInstance(NewDashboardActivity.this).getIsSchool();
                    String schooldId = RegPrefManager.getInstance(NewDashboardActivity.this).
                            getSchoolId();
                    String sectionId = RegPrefManager.getInstance(NewDashboardActivity.this).
                            getSectionId();


                    if (isSectionEnabled) {

                        if (schooldId != null && sectionId != null
                                && !schooldId.equals("null") && !sectionId.equals("null")&&
                                !sectionId.equals("00000000-0000-0000-0000-000000000000")) {


                            if(RegPrefManager.
                                    getInstance(NewDashboardActivity.this).getschoolCode()!=null){
                                startActivity(new Intent(NewDashboardActivity.this,
                                        MainActivity.class));
                            }
                            else
                                accessSchoolZone();
                        } else {
                            dialogSchoolZone("Select school or section to get access to school zone");

                        }


                    } else {
                        dialogSchoolZone("School zone not activated for your section");
                        // accessSchoolZone();
                        //
                    }

                }else{

                    startActivity(new Intent(NewDashboardActivity.this, Internet_Activity.class));
                }






            }
        });

        coinsCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(NewDashboardActivity.this, LearnAndEarnActivity.class);
                startActivity(intent);
            }
        });

        packages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(NewDashboardActivity.this, PackageActivityNew.class);
                startActivity(in);
            }
        });


        mentortest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(AppUtil.isInternetConnected(NewDashboardActivity.this)){

                    int resultcount = RegPrefManager.getInstance(NewDashboardActivity.this).getResultCount();

                    resultcount = 0;
                    if(resultcount == 3){
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                NewDashboardActivity.this);
                        LayoutInflater layoutInflater = (LayoutInflater) NewDashboardActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View viewa = layoutInflater.inflate(R.layout.dialog_school_zone, null);
                        builder.setView(viewa);


                        final AlertDialog dialog1 = builder.create();

                        TextView text = (TextView) viewa.findViewById(R.id.txt_na);
                        Button ok = (Button) viewa.findViewById(R.id.okay_school);
                        ImageView img_dial=(ImageView) viewa.findViewById(R.id.img_dial);
                        Typeface face = Typeface.createFromAsset(NewDashboardActivity.this.getAssets(),
                                "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");
                        text.setText("Sorry! You have attempted \nthe test maximum number of times" );
                        //  headerw.setTypeface(face);
                        text.setTypeface(face);
                        ok.setTypeface(face);
                        img_dial.setBackground(getResources().getDrawable(R.drawable.ic_sad_dial));
                        ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog1.dismiss();

                            }
                        });
                        dialog1.show();
                        dialog1.setCanceledOnTouchOutside(false);


                        Window window = dialog1.getWindow();
                        WindowManager.LayoutParams wlp = window.getAttributes();

                        window.setAttributes(wlp);
                    }else{
                        Intent intent = new Intent(NewDashboardActivity.this, Exam_desc.class);
                        intent.putExtra("exam_id","1");
                        intent.putExtra("sub_name", "psychometric");
                        intent.putExtra("sub_id", "0");

                        intent.putExtra("module_type", "psychometric");
                        intent.putExtra("chap_id", "0");
                        intent.putExtra("isOffline", "0");
                        startActivity(intent);
                    }



                }else{


                    startActivity(new Intent(NewDashboardActivity.this, Internet_Activity.class));

                }



            }
        });


        scholarship.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(AppUtil.isInternetConnected(NewDashboardActivity.this)){

                    Intent in = new Intent(NewDashboardActivity.this,ScholarShipActivity.class);
                    startActivity(in);

                }else{

                    startActivity(new Intent(NewDashboardActivity.this, Internet_Activity.class));

                }




            }
        });


        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(AppUtil.isInternetConnected(NewDashboardActivity.this)){
                    Intent in = new Intent(NewDashboardActivity.this,
                            NewNotificationActivity.class);
                    startActivity(in);
                }else{
                    startActivity(new Intent(NewDashboardActivity.this, Internet_Activity.class));
                }




            }
        });

        freetrail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(AppUtil.isInternetConnected(NewDashboardActivity.this)){
                    Intent in = new Intent(NewDashboardActivity.this,
                            Order.class);
                    in.putExtra("activity",0);

                    startActivity(in);
                }else{
                    startActivity(new Intent(NewDashboardActivity.this, Internet_Activity.class));
                }




            }
        });

        nav_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(AppUtil.isInternetConnected(NewDashboardActivity.this)){
                    startActivity(new Intent(NewDashboardActivity.this, ProfileActivity.class));
                }else{
                    startActivity(new Intent(NewDashboardActivity.this, Internet_Activity.class));
                }




            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(AppUtil.isInternetConnected(NewDashboardActivity.this)){
                    startActivity(new Intent(NewDashboardActivity.this, SettingsActivity.class));
                }else{
                    startActivity(new Intent(NewDashboardActivity.this, Internet_Activity.class));
                }

            }
        });


        rlv_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                if(AppUtil.isInternetConnected(NewDashboardActivity.this)){
                    Intent in = new Intent(NewDashboardActivity.this,
                            ProfileActivity.class);
                    startActivity(in);
                }else{
                    startActivity(new Intent(NewDashboardActivity.this, Internet_Activity.class));
                }

            }
        });

        chat_with_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(AppUtil.isInternetConnected(NewDashboardActivity.this)){
                    chatWebview();
                }else{
                    startActivity(new Intent(NewDashboardActivity.this, Internet_Activity.class));
                }


            }
        });


        if(getIntent().getStringExtra("feed")!=null)
            changeFragment(R.id.tab_school);
        else
        changeFragment(R.id.tab_home_new);


    }



    public void savePackageData(PackObject packObject){




          if(packObject == null){
              setTrialDate();
          }else{
              String packageName = packObject.getPackageName();
              String packageDate = packObject.getExpire();

              StringTokenizer que_date_time = new StringTokenizer(packageDate, "T");
              String Date_ques = que_date_time.nextToken();
              trial_header.setText(packageName);
              freeTrial.setText("Your package expires on "+Date_ques);
          }


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
    }

    public  void setTrialDate()
    {
        trialDate = RegPrefManager.getInstance(this).getExpireDate();
        if(trialDate==null
                ||
                trialDate.equalsIgnoreCase("null")
                ){

            freeTrial.setText("Your free trial has expired");
        }else{
            freeTrial.setText("Your free trial expires on" +" "+trialDate);
        }
    }

    private BottomBar bottomBar;
    private void accessSchoolZone()
    {
        mDrawerLayout.closeDrawer(Gravity.LEFT);
        final View view = getLayoutInflater()
                .inflate(R.layout.dialog_school_zone_code, null);
        final EditText mEditText=view.findViewById(R.id.etSchoolCode);
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "PROCEED",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        callWebApi(mEditText.getText().toString());
                    }
                });


        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });


        alertDialog.setView(view);
        alertDialog.show();


    }

    private void callWebApi(String code)
    {



        if(AppUtil.isInternetConnected(this))
        {
            Bundle bundle=new Bundle();
            bundle.putLong("regId",RegPrefManager.getInstance(this).getRegId());
            bundle.putString("schoolId",RegPrefManager.getInstance(this).getSchoolId());
            bundle.putString("scolCode",code);
            showDialog(null,"Please Wait...");
            NetworkService.
                    startActionAssignSchoolZone(this,mServiceReceiver,bundle);
            // showToast("No Internet connection");
        }
        else
        {
            showToast("No Internet connection");
        }



    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }
    public void setUpNavigationView() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return false;
            }
        });

        bottomBar = (BottomBar) findViewById(R.id.bottom_bar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {



                changeFragment(tabId);
            }
        });


    }

    public void selectAnalytics(){
        bottomBar.selectTabWithId(R.id.tab_analytics);


    }


    public void changeFragment(int id)
    {
        fragmentId=id;
        FragmentManager fragmentManager=getSupportFragmentManager();

        switch (id)
        {


            case R.id.tab_bookmark:

                bottomBar.getTabWithId(R.id.tab_bookmark).
                        setBackgroundColor(getResources().getColor(
                                R.color.white));

                bottomBar.getTabWithId(R.id.tab_home_new).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));
                bottomBar.getTabWithId(R.id.tab_chat).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));
                bottomBar.getTabWithId(R.id.tab_analytics).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));

                bottomBar.getTabWithId(R.id.tab_school).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));

                fragment= BookmarkFragment.getInstance();
                break;
            case R.id.tab_home_new:
                bottomBar.getTabWithId(R.id.tab_bookmark).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));

                bottomBar.getTabWithId(R.id.tab_home_new).
                        setBackgroundColor(getResources().getColor(
                                R.color.white));
                bottomBar.getTabWithId(R.id.tab_chat).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));
                bottomBar.getTabWithId(R.id.tab_analytics).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));

                bottomBar.getTabWithId(R.id.tab_school).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));
                fragment= DashboardFragment.getInstance();
                break;
            case R.id.tab_analytics:

                bottomBar.getTabWithId(R.id.tab_bookmark).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));

                bottomBar.getTabWithId(R.id.tab_home_new).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));
                bottomBar.getTabWithId(R.id.tab_chat).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));
                bottomBar.getTabWithId(R.id.tab_analytics).
                        setBackgroundColor(getResources().getColor(
                                R.color.white));

                bottomBar.getTabWithId(R.id.tab_school).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));
                fragment= AnalyticsFragment.getInstance();
                break;
            case R.id.tab_chat:
                bottomBar.getTabWithId(R.id.tab_bookmark).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));

                bottomBar.getTabWithId(R.id.tab_home_new).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));
                bottomBar.getTabWithId(R.id.tab_chat).
                        setBackgroundColor(getResources().getColor(
                                R.color.white));
                bottomBar.getTabWithId(R.id.tab_analytics).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));

                bottomBar.getTabWithId(R.id.tab_school).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));
                fragment = DictonaryFragment.getInstance();
                break;
            case R.id.tab_school:

                // setTheme(R.style.AppTheme_ScienceTheme);

                bottomBar.getTabWithId(R.id.tab_bookmark).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));

                bottomBar.getTabWithId(R.id.tab_home_new).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));
                bottomBar.getTabWithId(R.id.tab_chat).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));
                bottomBar.getTabWithId(R.id.tab_analytics).
                        setBackgroundColor(getResources().getColor(
                                R.color.grey_light_dashboard));

                bottomBar.getTabWithId(R.id.tab_school).
                        setBackgroundColor(getResources().getColor(
                                R.color.white));
                fragment = FeedFragment.getInstance();
                break;


        }
        if(fragment!=null) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.container,fragment);
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).commit();
            fragmentManager.executePendingTransactions();
            toolbar.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));

        }



    }



    private void setTabBackgroundColor()
    {


    }


    @Override
    public void onBackPressed() {

        dialog();

    }


    public void chatWebview(){
        logEvent(LogEventUtil.KEY_ChatWithUs_PAGE,
                LogEventUtil.EVENT_ChatWithUs_PAGE);
        FreshchatUser freshUser= Freshchat.getInstance(getApplicationContext()).getUser();



        freshUser.setFirstName(RegPrefManager.getInstance(this).getUserName());
        freshUser.setPhone("+91", RegPrefManager.getInstance(this).getPhone());
        freshUser.setEmail(RegPrefManager.getInstance(this).getEmail());


        //Call setUser so that the user information is synced with Freshchat's servers
        Freshchat.getInstance(getApplicationContext()).setUser(freshUser);
        Intent intent = new Intent(this, ChannelListActivity.class);

        intent.putExtra("myParam", "Digichamps");
        startActivity(intent);

    }


    public void dialogSchoolZone(String textNew) {
        AlertDialog.Builder builder = new AlertDialog.Builder(NewDashboardActivity.this);
        LayoutInflater layoutInflater = (LayoutInflater) NewDashboardActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dialog_school_zone, null);
        builder.setView(view);


        final AlertDialog dialog1 = builder.create();

        TextView text = (TextView) view.findViewById(R.id.txt_na);
        text.setText(textNew);
        Button ok = (Button) view.findViewById(R.id.okay_school);
        Typeface face = Typeface.createFromAsset(NewDashboardActivity.this.getAssets(),
                "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");


        text.setTypeface(face);
        ok.setTypeface(face);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        dialog1.show();



        Window window = dialog1.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        window.setAttributes(wlp);

    }


    private void dialog() {

        final Dialog dialog = new Dialog(NewDashboardActivity.this);
        dialog.setContentView(R.layout.custom_dialog_exit);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        Button btn1 = (Button) dialog.findViewById(R.id.btn1);
        Button btn2 = (Button) dialog.findViewById(R.id.btn2);


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                finishAffinity();
            }
        });



        dialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        mServiceReceiver.setReceiver(null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        filter=new IntentFilter();
        filter.addAction(IntentHelper.ACTION_COIN_RELOAD);
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver,filter);
        mServiceReceiver.setReceiver(this);
    }
    private BroadcastReceiver mBroadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent!=null&&intent.getAction().equals(IntentHelper.ACTION_COIN_RELOAD))
            {

                if(fragment!=null&&fragment instanceof DashboardFragment)
                {
                    ((DashboardFragment) fragment).setCount();
                }
            }
        }
    };

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode)
        {
            case 200:
                if(RegPrefManager.getInstance(NewDashboardActivity.this).getschoolCode()!=null){
                    startActivity(new Intent(NewDashboardActivity.this,
                            MainActivity.class));
                }
                break;
            case 501:
                showToast("School Code wrong");
                break;
            case 204:
                showToast("Something went wrong");
                break;
            case 409:
                showToast("You already used the school code. Please contact our support team");
                break;
        }
    }


    public void openBottomSheet(){

        String referUrl = RegPrefManager.getInstance(this).getFamilyInviteLink();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);


        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Hey! Download this really cool App. You can learn videos and test completely free. They have launched India's 1st Learn and Earn Program. Check it out. "+ "\n" + referUrl);
        shareIntent.setType("text/plain");
        IntentPickerSheetView intentPickerSheet = new IntentPickerSheetView(
                this, shareIntent, "Share with...", new IntentPickerSheetView.OnIntentPickedListener() {
            @Override
            public void onIntentPicked(IntentPickerSheetView.ActivityInfo activityInfo) {
                bottomSheetLayout.dismissSheet();
                startActivity(activityInfo.getConcreteIntent(shareIntent));
            }
        });
        // Filter out built in sharing options such as bluetooth and beam.
        intentPickerSheet.setFilter(new IntentPickerSheetView.Filter() {
            @Override
            public boolean include(IntentPickerSheetView.ActivityInfo info) {
                return !info.componentName.getPackageName().startsWith("com.android");
            }
        });
        // Sort activities in reverse order for no good reason
        intentPickerSheet.setSortMethod(new Comparator<IntentPickerSheetView.ActivityInfo>() {
            @Override
            public int compare(IntentPickerSheetView.ActivityInfo lhs, IntentPickerSheetView.ActivityInfo rhs) {
                return rhs.label.compareTo(lhs.label);
            }
        });

        // Add custom mixin example
        Drawable customDrawable = ResourcesCompat.getDrawable(getResources(),
                R.mipmap.ic_launcher, null);
        IntentPickerSheetView.ActivityInfo customInfo =
                new IntentPickerSheetView.ActivityInfo(customDrawable, "Custom mix-in",
                        this, NewDashboardActivity.class);
        intentPickerSheet.setMixins(Collections.singletonList(customInfo));

        bottomSheetLayout.showWithSheetView(intentPickerSheet);
    }

    public void updateStatusBarColor(String color){// Color must be in hexadecimal fromat
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.
                    FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
    }
}
