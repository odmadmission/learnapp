package com.sseduventures.digichamps.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.sseduventures.digichamps.Model.MentorStudentChat;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.ChatAdapter;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FileUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.utils.Profile_utility;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

public class PersonalMentorChatActivity extends FormActivity implements ServiceReceiver.Receiver, ActivityCompat.OnRequestPermissionsResultCallback {


    private static final int REQUEST_IMAGE_GALLARY = 100;
    private static final int REQUEST_IMAGE_CAPTURE = 101;

    Context mContext;

    ListView recyclerview;

    RelativeLayout btnSend;
    EditText enter_message;
    RelativeLayout progressbarrel;


    String schoolid, regId, roleid;
    int classid;

    int height;
    int width;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;


    SharedPreferences sp;

    private ServiceReceiver mResultReceiver;
    private File photoFile;
    private Uri imageUri;
    private Uri tempUri;

    private String assignteacherid;
    private ArrayList<MentorStudentChat> mList;
    private ChatAdapter mChatAdapter;
    private ImageView imvSend;
    private Toolbar toolbar;
    private Handler mHandler;
    private int type = 0;
    private IntentFilter intentFilter;
    private RegPrefManager regPrefManager;


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equalsIgnoreCase(IntentHelper.
                    ACTION_NEW_MESSAGE)) {

                NotificationManager manager = (NotificationManager)
                        PersonalMentorChatActivity.this.
                                getSystemService(
                                        Context.NOTIFICATION_SERVICE);
                manager.cancel(129);

                MentorStudentChat msc = intent.getParcelableExtra("msc");
                msc.setInsertedDate(new Date(msc.getInsertedDate().getTime() - (19800000)));
                mList.add(mList.size(), msc);
                recyclerview.setSelection(mList.size());
                recyclerview.post(new Runnable() {
                    @Override
                    public void run() {
                        recyclerview.setSelection(mList.size());
                    }
                });
                mChatAdapter.notifyDataSetChanged();

            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_mentor_chat);
        logEvent(LogEventUtil.KEY_ChatwithMentor_PAGE,
                LogEventUtil.EVENT_ChatwithMentor_PAGE);
        setModuleName(LogEventUtil.EVENT_ChatwithMentor_PAGE);


        init();

        regPrefManager = RegPrefManager.getInstance(this);
        sp = getSharedPreferences("paper_zone",
                Context.MODE_PRIVATE);


        regId = String.valueOf(regPrefManager.getRegId());
        schoolid = regPrefManager.getschoolId();
        classid = (int) regPrefManager.getclassid();
        roleid = String.valueOf(regPrefManager.getroleid());
        assignteacherid = String.valueOf(regPrefManager.getMentorid());

        if (AppUtil.isInternetConnected(PersonalMentorChatActivity.this)) {
            getList();
        } else {
            AskOptionDialogNew("Please Check Your Internet Connection").show();
        }
    }


    private void init() {
        final LayoutInflater inflater = (LayoutInflater) PersonalMentorChatActivity.this.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = PersonalMentorChatActivity.this;

        mList = new ArrayList<>();
        mHandler = new Handler();
        mResultReceiver = new ServiceReceiver(mHandler);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        PersonalMentorChatActivity.this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        progressbarrel = findViewById(R.id.progressbarrel);
        btnSend = findViewById(R.id.btnSend);
        imvSend = findViewById(R.id.imvSend);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back123));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Chat With Mentor");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        toolbar.setTitle("Chat With Mentor");


        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AppUtil.isInternetConnected(PersonalMentorChatActivity.this)) {
                    if (type == 0) {

                        checkPermission(PersonalMentorChatActivity.this);
                    } else
                        sendMessage();
                } else {

                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }
            }
        });


        enter_message = findViewById(R.id.enter_message);

        enter_message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s,
                                      int start, int before, int count) {

                if (s != null && s.length() > 0) {
                    type = 1;
                    imvSend.setImageDrawable(
                            getResources().getDrawable(R.drawable.ic_send));
                } else {
                    type = 0;
                    imvSend.setImageDrawable(
                            getResources().getDrawable(R.mipmap.attach)
                    );
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);


        recyclerview = (ListView) findViewById(R.id.recyclerview);

        mChatAdapter = new ChatAdapter(
                PersonalMentorChatActivity.this,
                mList,
                width, height

        );
        recyclerview.setAdapter(mChatAdapter);

    }


    public void hideKeyboard() {
        View view = PersonalMentorChatActivity.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) PersonalMentorChatActivity.this.
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public String getNextColor(String mycolor) {
        if (mycolor.equals("#00ADCA")) {
            return "#F44337";
        } else if (mycolor.equals("#F44337")) {
            return "#4051B5";
        } else if (mycolor.equals("#4051B5")) {
            return "#009933";
        } else if (mycolor.equals("#009933")) {
            return "#00ADCA";
        }
        return "#00ADCA";
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(PersonalMentorChatActivity.this, PersonalMentorActivity.class));
        finish();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode) {
            case ResponseCodes.NO_INTERNET:
                hideDialog();
                Intent intent = new Intent(PersonalMentorChatActivity.this,
                        Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.EXCEPTION:
                hideDialog();
                Intent in = new Intent(PersonalMentorChatActivity.this,
                        ErrorActivity.class);
                startActivity(in);
                break;

            case ResponseCodes.FAILURE:
                hideDialog();
                Intent inin = new Intent(PersonalMentorChatActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.UNEXPECTED_ERROR:

                break;
            case 200:
                ArrayList<MentorStudentChat> list = resultData.
                        getParcelableArrayList("data");
                mList.addAll(mList.size(), list);
                recyclerview.setSelection(mList.size());
                recyclerview.post(new Runnable() {
                    @Override
                    public void run() {
                        recyclerview.setSelection(mList.size());
                    }
                });
                mChatAdapter.notifyDataSetChanged();
                break;
            case 201:

                MentorStudentChat msc = resultData.getParcelable("msc");
                mList.add(mList.size(), msc);
                recyclerview.setSelection(mList.size());
                recyclerview.post(new Runnable() {
                    @Override
                    public void run() {
                        recyclerview.setSelection(mList.size());
                    }
                });
                mChatAdapter.notifyDataSetChanged();
                break;

        }


    }


    private void sendMessage() {
        if (AppUtil.isInternetConnected(PersonalMentorChatActivity.this)) {
            if (validate()) {
                showDialog(null, "please wait");

                MentorStudentChat msc = new MentorStudentChat();
                msc.setSender(Constants.SENDER_STUDENT);
                msc.setMime(Constants.CHAT_MIME_TEXT);
                msc.setMessage(enter_message.getText().toString());
                msc.setRegId(
                        Long.parseLong(regId));
                msc.setChatId(0);
                msc.setTeacherId(Long.parseLong(
                        assignteacherid));
                msc.setSyncDate(new Date());
                msc.setInsertedDate(new Date());
                msc.setRowId(0);


                Bundle bundle = new Bundle();
                bundle.putParcelable("msc", msc);

                NetworkService.startActionSendMessage(PersonalMentorChatActivity.this,
                        mResultReceiver, bundle);
                enter_message.setText("");
            }
        } else
            Toast.makeText(PersonalMentorChatActivity.this,
                    "No Internet", Toast.LENGTH_SHORT).show();

    }

    private boolean validate() {

        if (enter_message.getText().toString().isEmpty()) {
            Toast.makeText(PersonalMentorChatActivity.this,
                    "Message cannot " +
                            " be empty", Toast.LENGTH_SHORT)
                    .show();
            return false;
        }

        return true;
    }

    protected void startCamera() {

        boolean result = Profile_utility.checkPermission(PersonalMentorChatActivity.this);

        if (result) {
            Intent intent = getCameraIntent();
            if (intent != null) {
                photoFile = FileUtil.getOutputMediaFile(FileUtil.MEDIA_TYPE_IMAGE,
                        FileUtil.IMAGE_SENT_DIR);

                // Continue only if the File was successfully created
                if (photoFile != null) {
                    if (Build.VERSION.SDK_INT >= 24) {
                        tempUri = FileProvider.getUriForFile(PersonalMentorChatActivity.this,
                                getResources().getString(R.string.freshchat_file_provider_authority),
                                photoFile);
                    } else {
                        tempUri = Uri.fromFile(photoFile);
                    }//

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                } else
                    this.showToast("Unable to proceed");
            }
        }

    }

    protected void openGallery() {

        boolean result = Profile_utility.checkPermission(
                PersonalMentorChatActivity.this);

        if (result) {
            if (FileUtil.isExternalStorageWritable()) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                if (intent.resolveActivity(PersonalMentorChatActivity.this.getPackageManager()) != null) {
                    startActivityForResult(intent, REQUEST_IMAGE_GALLARY);
                } else {
                    this.showToast("No Camera app found");
                }
            } else {
                this.showToast("No External Storage");
            }
        }
    }

    public Intent getCameraIntent() {
        if (AppUtil.checkCameraHardware(PersonalMentorChatActivity.this)) {
            if (FileUtil.isExternalStorageWritable()) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (intent.resolveActivity(PersonalMentorChatActivity.this.getPackageManager()) != null) {
                    return intent;
                } else
                    this.showToast("No camera app not found");
            } else
                this.showToast("No External Storage Found");
        } else
            this.showToast("No camera hardware found");


        return null;
    }

    private void showFileOptions() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PersonalMentorChatActivity.this);
        builder.setTitle("Select option");

// add a list
        String[] animals = {"Camera", "Gallery"};
        builder.setItems(animals, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:

                        startCamera();
                        break;
                    case 1:
                        openGallery();
                        break;

                }
            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();


    }

    public void showToast(String text) {
        Toast.makeText(PersonalMentorChatActivity.this, text + "", Toast.LENGTH_SHORT).show();
    }

    private void getList() {
        // Log.v(TAG, "getList()");
        if (AppUtil.isInternetConnected(this)) {


            Bundle bundle = new Bundle();
            bundle.putString("regId", regId);

            NetworkService.startActionGetMessages(PersonalMentorChatActivity.this,
                    mResultReceiver,
                    bundle
            );
        } else {
            Toast.makeText(PersonalMentorChatActivity.this, "No internet", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onStart() {
        NotificationManager manager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(129);


        sp.edit().putInt("chat_opened", 1).commit();

        intentFilter = new IntentFilter();
        // intentFilter.addAction(Constants.ACTION_SYNC_DATA);
        intentFilter.addAction(IntentHelper.ACTION_NEW_MESSAGE);
        LocalBroadcastManager.getInstance(this).
                registerReceiver(receiver, intentFilter);
        mResultReceiver.setReceiver(PersonalMentorChatActivity.this);
        mResultReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mResultReceiver.setReceiver(null);
        super.onStop();
        sp.edit().putInt("chat_opened", 0).commit();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            Bundle bundle = null;
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                imageUri = tempUri;
                bundle = new Bundle();
                bundle.putInt("type", 1);
                bundle.putInt("width", width);
                bundle.putInt("height", height);
                bundle.putSerializable("file", photoFile);
                bundle.putParcelable("imageuri", imageUri);
                bundle.putParcelable("tempuri", tempUri);
                bundle.putLong(IntentHelper.BUNDLE_DATA1,
                        RegPrefManager.getInstance(this)
                                .getRegId()
                );


            } else if (requestCode == REQUEST_IMAGE_GALLARY) {
                tempUri = data.getData();
                imageUri = tempUri;
                bundle = new Bundle();
                bundle.putInt("type", 2);
                bundle.putInt("width", width);
                bundle.putInt("height", height);
                bundle.putSerializable("file", photoFile);
                bundle.putParcelable("imageuri", imageUri);
                bundle.putParcelable("tempuri", tempUri);
                bundle.putLong(IntentHelper.BUNDLE_DATA1,
                        RegPrefManager.getInstance(this)
                                .getRegId());

            }

            if (AppUtil.isInternetConnected(PersonalMentorChatActivity.this)) {

                showDialog(null, "Please wait...");

                MentorStudentChat msc = new MentorStudentChat();
                msc.setSender(Constants.SENDER_STUDENT);
                msc.setMime(Constants.CHAT_MIME_MEDIA);
                msc.setRegId(
                        Long.parseLong(regId));
                msc.setChatId(0);
                msc.setTeacherId(Long.parseLong(
                        assignteacherid));
                msc.setSyncDate(new Date());
                msc.setInsertedDate(new Date());
                msc.setRowId(0);


                bundle.putParcelable("msc", msc);

                NetworkService.startActionSendImage(PersonalMentorChatActivity.this,
                        mResultReceiver, bundle);
                enter_message.setText("");

            } else
                Toast.makeText(PersonalMentorChatActivity.this,
                        "No Internet", Toast.LENGTH_SHORT).show();


        }

    }


    private AlertDialog AskOptionDialogNew(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })

                .create();
        return myQuittingDialogBox;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.
                    WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)
                        context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    android.support.v7.app.AlertDialog.Builder alertBuilder = new android.support.v7.app.AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    android.support.v7.app.AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                showFileOptions();
                return true;
            }
        } else {
            showFileOptions();
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Log.v("","Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission
            showFileOptions();
        }


    }
}
