package com.sseduventures.digichamps.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.adapter.CardViewCommonAdapter;
import com.sseduventures.digichamps.adapter.CardviewTopperWay;
import com.sseduventures.digichamps.domain.BoardListData;
import com.sseduventures.digichamps.domain.BoardResponse;
import com.sseduventures.digichamps.domain.GetClassListData;
import com.sseduventures.digichamps.domain.GetClassResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RecyclerItemClickListener;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.sseduventures.digichamps.utils.Constants.BOARDLIST;
import static com.sseduventures.digichamps.utils.Constants.CLASSLIST;


public class ClassListActivity extends FormActivity implements
        ServiceReceiver.Receiver{

    Context mContext;
    ArrayList<GetClassListData> mList;
    ArrayList<BoardListData> mBoardList;
    ArrayList<String> mListString,mListClassString;
    TextView tv_at,tv_board,tv_selected_board,lbl_class,tv_class;
    RelativeLayout layout_board,layout_class,btnGo;
    String classId = "",boardId="",refboardId,refclassId;
    String schoolid;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private BoardResponse success;
    private GetClassResponse mSuccess;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_class);

        setModuleName(LogEventUtil.EVENT_Class_list_activity);
        logEvent(LogEventUtil.KEY_Class_list_activity,LogEventUtil.EVENT_bookmark);

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);


        schoolid = RegPrefManager.getInstance(this).getSchoolId();
        refboardId=RegPrefManager.getInstance(this).getKeyBoardId();
        refclassId=String.valueOf(RegPrefManager.getInstance(this).getKeyClassId());



        mContext = this;
        mList = new ArrayList<>();
        mBoardList = new ArrayList<>();
        mListString = new ArrayList<>();
        mListClassString = new ArrayList<>();
        tv_board = findViewById(R.id.tv_board);
        tv_selected_board = findViewById(R.id.tv_selected_board);
        tv_class = findViewById(R.id.tv_class);
        lbl_class = findViewById(R.id.lbl_class);
        btnGo = findViewById(R.id.btnGo);

        layout_class = findViewById(R.id.layout_class);
        layout_board = findViewById(R.id.layout_board);
        tv_at = findViewById(R.id.tv_at);

        FontManage.setFontHeaderBold(mContext,tv_at);
        FontManage.setFontMeiryoBold(mContext,tv_board);
        FontManage.setFontMeiryo(mContext,tv_selected_board);
        FontManage.setFontMeiryo(mContext,tv_class);
        FontManage.setFontMeiryoBold(mContext,lbl_class);


        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getBoard();
        getClassDetails();

        layout_board.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMyDialog(mListString,tv_selected_board,"Select Board");
            }
        });

        layout_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mListClassString != null && mListClassString.size()>0){
                    showMyDialog(mListClassString,tv_class,"Select Class");
                }else{
                    Toast.makeText(mContext, "Please select board first", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(classId != ""){
                    Intent i = new Intent(mContext,LeaderBoardActivity.class);
                    i.putExtra("classID",classId+"");
                    mContext.startActivity(i);
                    Activity activity = (Activity) mContext;
                    activity.overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
                }else{
                    Toast.makeText(mContext, "Please select board and class", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public void showMyDialog(final ArrayList<String> mList, final TextView tv_view,String title){
        // TODO Auto-generated method stub
        final Dialog dialogEdit;
        dialogEdit = new Dialog(mContext);
        dialogEdit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogEdit.setContentView(R.layout.common_dialog);
        final RecyclerView dialog_recyclerList = (RecyclerView) dialogEdit.findViewById(R.id.dialog_recyclerList);
        TextView txtCancel = (TextView) dialogEdit.findViewById(R.id.txtCancel);
        TextView txtTitle = (TextView) dialogEdit.findViewById(R.id.txtTitle);
        txtTitle.setText(title);
        ImageView imgCancel = (ImageView) dialogEdit.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogEdit.dismiss();
            }
        });
        LinearLayoutManager mManager = new LinearLayoutManager(mContext);
        mManager.setOrientation(LinearLayoutManager.VERTICAL);
        dialog_recyclerList.setLayoutManager(mManager);
        CardViewCommonAdapter mCardViewCommonAdapter = new CardViewCommonAdapter(mList,mContext,tv_view);
        dialog_recyclerList.setAdapter(mCardViewCommonAdapter);
        dialogEdit.show();
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogEdit.dismiss();

            }
        });

        dialog_recyclerList.addOnItemTouchListener(
                new RecyclerItemClickListener(mContext, dialog_recyclerList ,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                tv_view.setTextColor(Color.BLACK);
                                tv_view.setText(mList.get(position));
                                tv_view.setError(null);
                                dialogEdit.dismiss();
                                if (tv_view == tv_selected_board) {
                                    getCLassBoard(mList.get(position));
                                } else {
                                    getClassId(mList.get(position));
                                }
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }
                        }));

    }

    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mContext)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    public void getCLassBoard(String id){
        tv_class.setText("");
        classId = "";
        boardId="";
        mListClassString = new ArrayList<>();
        if(mList != null && mList.size()>0){
            for(int i=0;i<mList.size();i++){
                if(mList.get(i).getBoardName().equals(id)){
                    mListClassString.add(mList.get(i).getClassName());
                }
            }
        }

        if(mBoardList != null && mBoardList.size()>0){
            for(int i=0;i<mBoardList.size();i++){
                if(mBoardList.get(i).getBoardName().equals(id)){
                    boardId = mBoardList.get(i).getBoardID()+"";
                }
            }
        }
    }

    public void getClassId(String cid){
        if(mList != null && mList.size()>0){
            for(int i=0;i<mList.size();i++){
                if(mList.get(i).getClassName().equals(cid) && mList.get(i).getBoardID()== Integer.parseInt(boardId)){
                   classId = mList.get(i).getClassID()+"";
                }
            }
        }
    }

    private void getBoard() {
        if(AppUtil.isInternetConnected(this)){


            NetworkService.startActionPostBoardDetails(ClassListActivity.this
                    ,mServiceReceiver);

        }else{
            Intent in = new Intent(ClassListActivity.this,
                    Internet_Activity.class);
            startActivity(in);
        }
    }

    private void getClassDetails() {
        if(AppUtil.isInternetConnected(this)){


            NetworkService.startActionPostClassDetails(ClassListActivity.this
                    ,mServiceReceiver);

        }else{
            Intent in = new Intent(ClassListActivity.this,
                    Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        //hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(ClassListActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(ClassListActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(ClassListActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:

                success = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if(success.isStatus()){
                    if(success.getList()!=null &&
                            success.getList().size()>0){
                        mBoardList.addAll(success.getList());
                        if(mBoardList != null && mBoardList.size()>0){
                            mListString = new ArrayList<>();
                            for(int i=0;i<mBoardList.size();i++){
                                mListString.add(mBoardList.get(i).getBoardName());
                            }
                        }
                    }
                }else{
                    AskOptionDialog("No Class Available").show();
                }
                break;

            case ResponseCodes.CLASSSUCCESS:
                mSuccess = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if(mSuccess.isStatus()){

                    if(mSuccess.getList()!=null
                            &&mSuccess.getList().size()>0){
                        mList.addAll(mSuccess.getList());
                    }
                }else{
                    AskOptionDialog("No Class Available").show();
                }
                    break;

        }
    }

}
