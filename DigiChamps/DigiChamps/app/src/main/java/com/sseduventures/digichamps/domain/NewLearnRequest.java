package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/20/2018.
 */

public class NewLearnRequest implements Parcelable {

    private String RegID;

    public String getRegID() {
        return RegID;
    }

    public void setRegID(String regID) {
        RegID = regID;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.RegID);
    }

    public NewLearnRequest() {
    }

    protected NewLearnRequest(Parcel in) {
        this.RegID = in.readString();
    }

    public static final Creator<NewLearnRequest> CREATOR = new Creator<NewLearnRequest>() {
        @Override
        public NewLearnRequest createFromParcel(Parcel source) {
            return new NewLearnRequest(source);
        }

        @Override
        public NewLearnRequest[] newArray(int size) {
            return new NewLearnRequest[size];
        }
    };
}
