package com.sseduventures.digichamps.activities;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.Model.Chapter_detail_Model;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.adapter.Bookmark_VideoView_Adapter;
import com.sseduventures.digichamps.adapter.RWOnlinePlayerAdapter;
import com.sseduventures.digichamps.adapter.VideoListAdapter;
import com.sseduventures.digichamps.domain.NewLearnRecentwatchedvideos;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.vdocipher.aegis.media.ErrorDescription;
import com.vdocipher.aegis.media.Track;
import com.vdocipher.aegis.player.VdoPlayer;
import com.vdocipher.aegis.player.VdoPlayerFragment;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by NISHIKANT on 7/28/2018.
 */

public class RWOnlineVideoPlayingActivity extends
        FormActivity implements VdoPlayer.InitializationListener {

    private final String TAG = "OnlinePlayerActivity";

    private VdoPlayer player;
    private VdoPlayerFragment playerFragment;
    private VdoPlayerControlView playerControlView;
    //  private TextView eventLog;
    private String eventLogString = "";

    private boolean playWhenReady = false;
    private int currentOrientation;

    private volatile String mOtp;
    private volatile String mPlaybackInfo;
    private volatile String mVideoId;
    private RecyclerView mRecyclerView;
    private RWOnlinePlayerAdapter mVideoListAdapter;
    private ArrayList<NewLearnRecentwatchedvideos> mList;
    private ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Log.v(TAG, "onCreate called");
        setContentView(R.layout.activity_online_player);
        logEvent(LogEventUtil.KEY_RECNT_WATCHED_PAGE,
                LogEventUtil.EVENT_RECNT_WATCHED_PAGE);
        setModuleName(LogEventUtil.EVENT_RECNT_WATCHED_PAGE);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(uiVisibilityListener);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        if (savedInstanceState != null) {
            mOtp = savedInstanceState.getString("otp");
            mPlaybackInfo = savedInstanceState.getString("playbackInfo");
            mVideoId = savedInstanceState.getString("videoId");
            mList = savedInstanceState.getParcelableArrayList("list");
        }
        else
        {
            mVideoId = getIntent().getStringExtra("videoid");
            mList = getIntent().getParcelableArrayListExtra("list");
        }
        for (int i=0;i<mList.size();i++)
        {
            if(mVideoId.equals(mList.get(i).getModule_video()))
                mList.get(i).setFlag(true);

            else
                mList.get(i).setFlag(false);
        }
        mRecyclerView=findViewById(R.id.videoList);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);

                mVideoListAdapter=new RWOnlinePlayerAdapter(mList,RWOnlineVideoPlayingActivity.this,mVideoId);

                mRecyclerView.setAdapter(mVideoListAdapter);

            }
        },1000);



        playerFragment = (VdoPlayerFragment)getFragmentManager().
                findFragmentById(R.id.online_vdo_player_fragment);
        playerControlView = (VdoPlayerControlView)findViewById(R.id.player_control_view);

        showControls(true);
        playerControlView.hideAfterTimeout();

        currentOrientation = getResources().getConfiguration().orientation;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        initializePlayer();
    }

    @Override
    protected void onStart() {

        super.onStart();
    }

    @Override
    protected void onStop() {

        disablePlayerUI();
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        if (mOtp != null && mPlaybackInfo != null&&mVideoId!=null) {
            outState.putString("otp", mOtp);
            outState.putString("playbackInfo", mPlaybackInfo);
            outState.putString("videoId", mVideoId);
        }
    }

    private void initializePlayer() {
        if (mOtp != null ) {

            playerFragment.initialize(RWOnlineVideoPlayingActivity.this);
            log("initializing player fragment");
        } else {
            // lets get otp and playbackInfo before creating the player
            if(mVideoId!=null)
                obtainOtpAndPlaybackInfo();
            else
                showToast("No VideoId Found");
        }
    }

    /**
     * Fetch (otp + playbackInfo) and initialize VdoPlayer
     * here we're fetching a sample (otp + playbackInfo)
     * TODO you need to generate/fetch (otp + playbackInfo) OR (signature + playbackInfo) for the
     * video you wish to play
     */
    private void obtainOtpAndPlaybackInfo() {
        // todo use asynctask
       // log("fetching params...");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Pair<String, String> pair =
                            Utils.postSampleOtpAndPlaybackInfo(
                                    mVideoId
                            );
                    mOtp = pair.first;
                    mPlaybackInfo = pair.second;
                   // Log.i(TAG, "obtained new otp and playbackInfo");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initializePlayer();
                        }
                    });
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showToast("Error fetching otp and playbackInfo: "
                                    + e.getClass().getSimpleName());
                            log("error fetching otp and playbackInfo");
                        }
                    });
                }
            }
        }).start();
    }



    private void log(String msg) {
        eventLogString += (msg + "\n");
        //eventLog.setText(eventLogString);
    }

    private void showControls(boolean show) {
       // Log.v(TAG, (show ? "show" : "hide") + " controls");
        if (show) {
            playerControlView.show();
        } else {
            playerControlView.hide();
        }
    }

    private void disablePlayerUI() {
        showControls(false);
    }

    public void playVideo(String videoId)
    {
        this.mVideoId=videoId;
        mOtp=null;
        mPlaybackInfo=null;
        playerControlView.stop();
        player=null;
        firstTime=false;
        showControls(true);
        playerControlView.hideAfterTimeout();
        for (int i=0;i<mList.size();i++)
        {
            if(mVideoId.equals(mList.get(i).getModule_video()))
                mList.get(i).setFlag(true);

            else
                mList.get(i).setFlag(false);
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);

                mVideoListAdapter.notifyDataSetChanged();

            }
        },1000);


        initializePlayer();
    }



    @Override
    public void onInitializationSuccess(VdoPlayer.PlayerHost playerHost, VdoPlayer player, boolean wasRestored) {

        this.player = player;
        player.addPlaybackEventListener(playbackListener);
        playerControlView.setPlayer(player);
        // showControls(true);

        playerControlView.setFullscreenActionListener(fullscreenToggleListener);
        playerControlView.setControllerVisibilityListener(visibilityListener);


        // load a media to the player
        VdoPlayer.VdoInitParams vdoParams = new VdoPlayer.VdoInitParams.Builder()
                .setOtp(mOtp)
                .setPlaybackInfo(mPlaybackInfo)
                .setPreferredCaptionsLanguage("en")
                .build();
        player.load(vdoParams);

        //log("loaded init params to player");
    }

    @Override
    public void onInitializationFailure(VdoPlayer.PlayerHost playerHost,
                                        ErrorDescription errorDescription) {
        String msg = "onInitializationFailure: errorCode = " +
                errorDescription.errorCode + ": " + errorDescription.errorMsg;
        log(msg);
      //  Log.e(TAG, msg);
        Toast.makeText(RWOnlineVideoPlayingActivity.this,
                "initialization failure: " + errorDescription.errorMsg,
                Toast.LENGTH_LONG).show();
    }

    private boolean firstTime=false;
    private VdoPlayer.PlaybackEventListener playbackListener = new VdoPlayer.PlaybackEventListener() {
        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            log(Utils.playbackStateString(playWhenReady, playbackState));
            RWOnlineVideoPlayingActivity.this.playWhenReady = playWhenReady;
            if(!firstTime)
            {
                firstTime=true;
                playerControlView.play();
            }
        }

        @Override
        public void onTracksChanged(Track[] tracks, Track[] tracks1) {

        }

        @Override
        public void onBufferUpdate(long bufferTime) {

        }

        @Override
        public void onSeekTo(long millis) {
            Log.i(TAG, "onSeekTo: " + String.valueOf(millis));
        }

        @Override
        public void onProgress(long millis) {}

        @Override
        public void onPlaybackSpeedChanged(float speed) {

        }

        @Override
        public void onLoading(VdoPlayer.VdoInitParams vdoInitParams) {

        }

        @Override
        public void onLoadError(VdoPlayer.VdoInitParams vdoInitParams,
                                ErrorDescription errorDescription) {
            String err = "onLoadError code: " + errorDescription.errorCode;

        }

        @Override
        public void onLoaded(VdoPlayer.VdoInitParams vdoInitParams) {

        }

        @Override
        public void onError(VdoPlayer.VdoInitParams vdoParams, ErrorDescription errorDescription) {
            String err = "onError code " + errorDescription.errorCode + ": " + errorDescription.errorMsg;

        }

        @Override
        public void onMediaEnded(VdoPlayer.VdoInitParams vdoInitParams) {

        }
    };

    private VdoPlayerControlView.FullscreenActionListener fullscreenToggleListener = new VdoPlayerControlView.FullscreenActionListener() {
        @Override
        public boolean onFullscreenAction(boolean enterFullscreen) {
            showFullScreen(enterFullscreen);
            return true;
        }
    };

    private VdoPlayerControlView.ControllerVisibilityListener visibilityListener = new VdoPlayerControlView.ControllerVisibilityListener() {
        @Override
        public void onControllerVisibilityChange(int visibility) {
          //  Log.i(TAG, "controller visibility " + visibility);
            if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                if (visibility != View.VISIBLE) {
                    showSystemUi(false);
                }
            }
        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        final int newOrientation = newConfig.orientation;
        final int oldOrientation = currentOrientation;
        currentOrientation = newOrientation;

        super.onConfigurationChanged(newConfig);
        if (newOrientation == oldOrientation) {
           // Log.i(TAG, "orientation unchanged");
        } else if (newOrientation == Configuration.ORIENTATION_LANDSCAPE) {

            (findViewById(R.id.online_vdo_player_fragment)).setLayoutParams(new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
            playerControlView.setFitsSystemWindows(true);
            // hide system windows
            showSystemUi(false);
            showControls(false);
        } else {
            // show other views

            (findViewById(R.id.online_vdo_player_fragment)).setLayoutParams(new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            playerControlView.setFitsSystemWindows(false);
            playerControlView.setPadding(0,0,0,0);
            // show system windows
            showSystemUi(true);
        }
    }

    @Override
    public void onBackPressed() {
        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            showFullScreen(false);
            playerControlView.setFullscreenState(false);
        } else {
            super.onBackPressed();
        }
    }

    private void showFullScreen(boolean show) {
      //  Log.v(TAG, (show ? "enter" : "exit") + " fullscreen");
        if (show) {
            // go to landscape orientation for fullscreen mode
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else {
            // go to portrait orientation
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }

    private void showSystemUi(boolean show) {
       // Log.v(TAG, (show ? "show" : "hide") + " system ui");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (!show) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
            } else {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            }
        }
    }

    private View.OnSystemUiVisibilityChangeListener uiVisibilityListener = new View.OnSystemUiVisibilityChangeListener() {
        @Override
        public void onSystemUiVisibilityChange(int visibility) {

            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
               // Log.v(TAG, "system ui visible, making controls visible");
                showControls(true);
            }
        }
    };
}
