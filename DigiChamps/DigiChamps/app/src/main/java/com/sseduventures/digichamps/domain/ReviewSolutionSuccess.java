package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NISHIKANT on 7/5/2018.
 */

public class ReviewSolutionSuccess implements Parcelable{

    private List<ReviewSolutionQuestionList> QuestionReviewList;

    public List<ReviewSolutionQuestionList> getQuestionReviewList() {
        return QuestionReviewList;
    }

    public void setQuestionReviewList(List<ReviewSolutionQuestionList> questionReviewList) {
        QuestionReviewList = questionReviewList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.QuestionReviewList);
    }

    public ReviewSolutionSuccess() {
    }

    protected ReviewSolutionSuccess(Parcel in) {
        this.QuestionReviewList = new ArrayList<ReviewSolutionQuestionList>();
        in.readList(this.QuestionReviewList, ReviewSolutionQuestionList.class.getClassLoader());
    }

    public static final Creator<ReviewSolutionSuccess> CREATOR = new Creator<ReviewSolutionSuccess>() {
        @Override
        public ReviewSolutionSuccess createFromParcel(Parcel source) {
            return new ReviewSolutionSuccess(source);
        }

        @Override
        public ReviewSolutionSuccess[] newArray(int size) {
            return new ReviewSolutionSuccess[size];
        }
    };
}
