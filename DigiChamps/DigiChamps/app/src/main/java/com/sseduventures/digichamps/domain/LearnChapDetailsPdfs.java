package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/26/2018.
 */

public class LearnChapDetailsPdfs implements Parcelable{

    private Boolean is_studynotebookmarked;
    private Integer moduleID;
    private String Url;
    private String Modulename;

    public Boolean getIs_studynotebookmarked() {
        return is_studynotebookmarked;
    }

    public void setIs_studynotebookmarked(Boolean is_studynotebookmarked) {
        this.is_studynotebookmarked = is_studynotebookmarked;
    }

    public Integer getModuleID() {
        return moduleID;
    }

    public void setModuleID(Integer moduleID) {
        this.moduleID = moduleID;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getModulename() {
        return Modulename;
    }

    public void setModulename(String modulename) {
        Modulename = modulename;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.is_studynotebookmarked);
        dest.writeValue(this.moduleID);
        dest.writeString(this.Url);
        dest.writeString(this.Modulename);
    }

    public LearnChapDetailsPdfs() {
    }

    protected LearnChapDetailsPdfs(Parcel in) {
        this.is_studynotebookmarked = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.moduleID = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Url = in.readString();
        this.Modulename = in.readString();
    }

    public static final Creator<LearnChapDetailsPdfs> CREATOR = new Creator<LearnChapDetailsPdfs>() {
        @Override
        public LearnChapDetailsPdfs createFromParcel(Parcel source) {
            return new LearnChapDetailsPdfs(source);
        }

        @Override
        public LearnChapDetailsPdfs[] newArray(int size) {
            return new LearnChapDetailsPdfs[size];
        }
    };
}
