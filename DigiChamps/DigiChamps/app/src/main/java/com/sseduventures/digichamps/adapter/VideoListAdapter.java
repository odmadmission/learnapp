package com.sseduventures.digichamps.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.Chapter_detail_Model;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.OnlinePlayerActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.activity.PackageActivityNew;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.utils.AppUtil;

import java.util.List;



public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.MyViewHolder> {
    View view;
    private String moduleId,user_id;
    private SpotsDialog dialog;
    private ImageView listener;
    public OnlinePlayerActivity context;
    private List<Chapter_detail_Model> moviesList;
    public View mView;

    public VideoListAdapter(List<Chapter_detail_Model> moviesList,
                            OnlinePlayerActivity context, String moduleId) {
        this.moviesList = moviesList;
        this.context = context;
        this.moduleId = moduleId;

        user_id = String.valueOf(RegPrefManager.getInstance(context).getRegId());
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static boolean checkImageResource(Context ctx, ImageView imageView,
                                             int imageResource) {
        boolean result = false;

        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }

            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }

        return result;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_chapter_detailnew, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Chapter_detail_Model chap_detail = moviesList.get(position);
        holder.title.setText(chap_detail.getModule_Title());
        if (moviesList.get(position).getModule_Image() != null &&
                !moviesList.get(position).getModule_Image().equalsIgnoreCase("null")
                && !moviesList.get(position).getModule_Image().isEmpty())
            Picasso.with(context).load(
                    moviesList.get(position).getModule_Image()
            ).into(holder.thumbnail_image);



        if (moviesList.get(position).getVideoExpiry().equalsIgnoreCase("true") && moviesList.get(position).getIs_Free().equalsIgnoreCase("false")) {
            holder.thumbnail_lock.setVisibility(View.VISIBLE);
            holder.play.setVisibility(View.GONE);

        } else {
            holder.thumbnail_lock.setVisibility(View.GONE);
        }

        if (moviesList.get(position).getIs_Free().equalsIgnoreCase("true")) {

            if (moviesList.get(position).getIsBookmarked().equalsIgnoreCase("true")) {
                holder.play.setImageResource(R.drawable.bookmarks);
            } else if (moviesList.get(position).getIsBookmarked().equalsIgnoreCase("false")) {
                holder.play.setImageResource(R.drawable.bookmarksilver);
            }
        } else if (moviesList.get(position).getVideoExpiry().equalsIgnoreCase("false")) {
            if (moviesList.get(position).getIsBookmarked().equalsIgnoreCase("true")) {
                holder.play.setImageResource(R.drawable.bookmarks);
            } else if (moviesList.get(position).getIsBookmarked().equalsIgnoreCase("false")) {
                holder.play.setImageResource(R.drawable.bookmarksilver);
            }
        } else {
            holder.play.setImageResource(R.drawable.lock_chapter);
        }



        holder.rlRoot.setTag(chap_detail);



        if (chap_detail.isFlag()) {
            mView = holder.rlRoot;
            holder.rlRoot.setClickable(false);
            holder.rlRoot.setBackgroundResource(R.drawable.video_item_card_bg);
        } else {
            holder.rlRoot.setClickable(true);
            holder.rlRoot.setOnClickListener(onClickListener(position, holder));
            holder.rlRoot.setBackgroundResource(R.drawable.abc_btn_selector);
        }

        holder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (moviesList.get(position).getVideoExpiry().equalsIgnoreCase("true")) {
                    if (moviesList.get(position).getIs_Free().equalsIgnoreCase("false")) {

                        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialogTheme_buy);
                        LayoutInflater factory = LayoutInflater.from(context);
                        final View vieww = factory.inflate(R.layout.dialog_buy, null);
                        Button ok = (Button) vieww.findViewById(R.id.button2);
                        final AlertDialog alertDialog = builder.create();
                        ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                                Intent i = new Intent(context, PackageActivityNew.class);
                                Activity activity = (Activity) context;
                                context.startActivity(i);

                                activity.finish();
                            }
                        });

                        builder.setView(vieww);
                        builder.setCancelable(true);
                        builder.setNeutralButton("Here!", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dlg, int sumthin) {

                            }
                        });

                        builder.show();
                    } else {
                        moduleId = moviesList.get(position).getModule_Id();
                        context.setUnsetBookMark(moduleId,"2");


                    }
                } else {
                    moduleId = moviesList.get(position).getModule_Id();

                    context.setUnsetBookMark(moduleId,"2");

                }


            }
        });


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private View.OnClickListener onClickListener(final int position, final MyViewHolder holder) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Chapter_detail_Model chap_detail = (Chapter_detail_Model) v.getTag();

                context.playVideo(chap_detail.get_file_resource());



                if (AppUtil.isInternetConnected(context)) {
                    if (checkImageResource(context, holder.play, R.drawable.lock_chapter)) {

                        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialogTheme_buy);
                        LayoutInflater factory = LayoutInflater.from(context);
                        final View view = factory.inflate(R.layout.dialog_buy, null);
                        Button ok = (Button) view.findViewById(R.id.button2);
                        final AlertDialog alertDialog = builder.create();
                        ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                                Intent i = new Intent(context, PackageActivityNew.class);
                                Activity activity = (Activity) context;
                                context.startActivity(i);


                            }
                        });

                        builder.setView(view);
                        builder.setCancelable(true);
                        builder.show();
                    } else {

                        final String moduleIdNew = moviesList.get(position).getModule_Id();
                        context.save(moduleIdNew);


                    }


                } else {
                    Intent i = new Intent(context, Internet_Activity.class);
                    Activity activity = (Activity) context;

                    context.startActivity(i);

                }
            }

        };
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView play;
        ImageView left_drawable, right_drawable;
        CardView container;
        RelativeLayout rlRoot;
        private ImageView thumbnail_image, thumbnail_lock;

        public MyViewHolder(View view) {
            super(view);
            container = (CardView) itemView.findViewById(R.id.card_view_chapter);
            rlRoot = (RelativeLayout) itemView.findViewById(R.id.rlRoot);
            thumbnail_image = (ImageView) view.findViewById(R.id.thumbnail_imagev);
            title = (TextView) view.findViewById(R.id.chap);
            play = (ImageView) view.findViewById(R.id.play);
            thumbnail_lock = (ImageView) view.findViewById(R.id.thumbnail_imageve);


        }
    }


}
