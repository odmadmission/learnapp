package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NISHIKANT on 7/4/2018.
 */

public class TestHighlightsSuccess implements Parcelable{

    private String ChapterName;
    private String TestName;
    private String TestType;
    private List<TestHighlightsSubconcept> subConcept;
    private List<TestHighLightsDifficulty> difficulty;
    private int Question_Nos;
    private int Total_Correct_Ans;
    private int Totaltime;
    private int wrong;
    private int Accuracy;
    private int Rank;
    private int Performance;
    private List<TestHighlightsDifficultys> Difficultys;
    private List<TestHighlightsTopic_details> Topic_details;
    private int Exam_ID;
    private int Total_Incorrec_Ans;
    private int Question_Attempted;
    private int Unanswered;


    public String getChapterName() {
        return ChapterName;
    }

    public void setChapterName(String chapterName) {
        ChapterName = chapterName;
    }

    public String getTestName() {
        return TestName;
    }

    public void setTestName(String testName) {
        TestName = testName;
    }

    public String getTestType() {
        return TestType;
    }

    public void setTestType(String testType) {
        TestType = testType;
    }

    public List<TestHighlightsSubconcept> getSubConcept() {
        return subConcept;
    }

    public void setSubConcept(List<TestHighlightsSubconcept> subConcept) {
        this.subConcept = subConcept;
    }

    public List<TestHighLightsDifficulty> getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(List<TestHighLightsDifficulty> difficulty) {
        this.difficulty = difficulty;
    }

    public int getQuestion_Nos() {
        return Question_Nos;
    }

    public void setQuestion_Nos(int question_Nos) {
        Question_Nos = question_Nos;
    }

    public int getTotal_Correct_Ans() {
        return Total_Correct_Ans;
    }

    public void setTotal_Correct_Ans(int total_Correct_Ans) {
        Total_Correct_Ans = total_Correct_Ans;
    }

    public int getTotaltime() {
        return Totaltime;
    }

    public void setTotaltime(int totaltime) {
        Totaltime = totaltime;
    }

    public int getWrong() {
        return wrong;
    }

    public void setWrong(int wrong) {
        this.wrong = wrong;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.Accuracy = accuracy;
    }

    public int getRank() {
        return Rank;
    }

    public void setRank(int rank) {
        Rank = rank;
    }

    public int getPerformance() {
        return Performance;
    }

    public void setPerformance(int performance) {
        Performance = performance;
    }

    public List<TestHighlightsDifficultys> getDifficultys() {
        return Difficultys;
    }

    public void setDifficultys(List<TestHighlightsDifficultys> difficultys) {
        Difficultys = difficultys;
    }

    public List<TestHighlightsTopic_details> getTopic_details() {
        return Topic_details;
    }

    public void setTopic_details(List<TestHighlightsTopic_details> topic_details) {
        Topic_details = topic_details;
    }

    public int getExam_ID() {
        return Exam_ID;
    }

    public void setExam_ID(int exam_ID) {
        Exam_ID = exam_ID;
    }

    public int getTotal_Incorrec_Ans() {
        return Total_Incorrec_Ans;
    }

    public void setTotal_Incorrec_Ans(int total_Incorrec_Ans) {
        Total_Incorrec_Ans = total_Incorrec_Ans;
    }

    public int getQuestion_Attempted() {
        return Question_Attempted;
    }

    public void setQuestion_Attempted(int question_Attempted) {
        Question_Attempted = question_Attempted;
    }

    public int getUnanswered() {
        return Unanswered;
    }

    public void setUnanswered(int unanswered) {
        Unanswered = unanswered;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ChapterName);
        dest.writeString(this.TestName);
        dest.writeString(this.TestType);
        dest.writeList(this.subConcept);
        dest.writeList(this.difficulty);
        dest.writeInt(this.Question_Nos);
        dest.writeInt(this.Total_Correct_Ans);
        dest.writeInt(this.Totaltime);
        dest.writeInt(this.wrong);
        dest.writeInt(this.Accuracy);
        dest.writeInt(this.Rank);
        dest.writeInt(this.Performance);
        dest.writeList(this.Difficultys);
        dest.writeTypedList(this.Topic_details);
        dest.writeInt(this.Exam_ID);
        dest.writeInt(this.Total_Incorrec_Ans);
        dest.writeInt(this.Question_Attempted);
        dest.writeInt(this.Unanswered);
    }

    public TestHighlightsSuccess() {
    }

    protected TestHighlightsSuccess(Parcel in) {
        this.ChapterName = in.readString();
        this.TestName = in.readString();
        this.TestType = in.readString();
        this.subConcept = new ArrayList<TestHighlightsSubconcept>();
        in.readList(this.subConcept, TestHighlightsSubconcept.class.getClassLoader());
        this.difficulty = new ArrayList<TestHighLightsDifficulty>();
        in.readList(this.difficulty, TestHighLightsDifficulty.class.getClassLoader());
        this.Question_Nos = in.readInt();
        this.Total_Correct_Ans = in.readInt();
        this.Totaltime = in.readInt();
        this.wrong = in.readInt();
        this.Accuracy = in.readInt();
        this.Rank = in.readInt();
        this.Performance = in.readInt();
        this.Difficultys = new ArrayList<TestHighlightsDifficultys>();
        in.readList(this.Difficultys, TestHighlightsDifficultys.class.getClassLoader());
        this.Topic_details = in.createTypedArrayList(TestHighlightsTopic_details.CREATOR);
        this.Exam_ID = in.readInt();
        this.Total_Incorrec_Ans = in.readInt();
        this.Question_Attempted = in.readInt();
        this.Unanswered = in.readInt();
    }

    public static final Creator<TestHighlightsSuccess> CREATOR = new Creator<TestHighlightsSuccess>() {
        @Override
        public TestHighlightsSuccess createFromParcel(Parcel source) {
            return new TestHighlightsSuccess(source);
        }

        @Override
        public TestHighlightsSuccess[] newArray(int size) {
            return new TestHighlightsSuccess[size];
        }
    };
}
