package com.sseduventures.digichamps.Model;

/**
 * Created by Tech_1 on 1/9/2018.
 */

public class Bookmarks_frag3_model {
    private String Module_Id, ModuleName,Module_Title,Is_Expire,Is_Free,Is_Avail,Question ;

        public Bookmarks_frag3_model( String Module_Id,String ModuleName, String Module_Title,String Is_Expire,String Is_Free,String Is_Avail,String Question
        ) {

            this.Module_Id = Module_Id;
            this.ModuleName = ModuleName;
            this.Module_Title = Module_Title;
            this.Is_Expire = Is_Expire;
            this.Is_Free = Is_Free;
            this.Is_Avail = Is_Avail;
            this.Question = Question;
        }

    public String getModule_Id() {
        return Module_Id;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public String getModule_Title() {
        return Module_Title;
    }

    public String getIs_Expire() {
        return Is_Expire;
    }

    public String getIs_Free() {
        return Is_Free;
    }

    public String getIs_Avail() {
        return Is_Avail;
    }

    public String getQuestion() {
        return Question;
    }
}



