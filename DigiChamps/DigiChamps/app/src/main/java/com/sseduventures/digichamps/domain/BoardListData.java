package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class BoardListData implements Parcelable{

    private int boardID;
    private String boardName;


    public int getBoardID() {
        return boardID;
    }

    public void setBoardID(int boardID) {
        this.boardID = boardID;
    }

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.boardID);
        dest.writeString(this.boardName);
    }

    public BoardListData() {
    }

    protected BoardListData(Parcel in) {
        this.boardID = in.readInt();
        this.boardName = in.readString();
    }

    public static final Creator<BoardListData> CREATOR = new Creator<BoardListData>() {
        @Override
        public BoardListData createFromParcel(Parcel source) {
            return new BoardListData(source);
        }

        @Override
        public BoardListData[] newArray(int size) {
            return new BoardListData[size];
        }
    };
}
