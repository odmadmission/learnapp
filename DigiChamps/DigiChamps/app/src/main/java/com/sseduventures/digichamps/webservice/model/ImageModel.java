package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageModel implements Parcelable {
    protected ImageModel(Parcel in) {
        Question_desc_Image = in.readString();
    }

    public static final Creator<ImageModel> CREATOR = new Creator<ImageModel>() {
        @Override
        public ImageModel createFromParcel(Parcel in) {
            return new ImageModel(in);
        }

        @Override
        public ImageModel[] newArray(int size) {
            return new ImageModel[size];
        }
    };

    public String getQuestion_desc_Image() {
        return Question_desc_Image;
    }

    public void setQuestion_desc_Image(String question_desc_Image) {
        Question_desc_Image = question_desc_Image;
    }

    private String Question_desc_Image = "";

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Question_desc_Image);
    }
}
