package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.DataModel_ReviewAnswers;
import com.sseduventures.digichamps.Model.LeaderBoardList;
import com.sseduventures.digichamps.R;

import com.sseduventures.digichamps.domain.ReviewSolutionQuestionList;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.richeditor.RichEditor;



public class ReviewAnswer_Adapter extends RecyclerView.Adapter<ReviewAnswer_Adapter.MyViewHolder> {
    private List<ReviewSolutionQuestionList> moviesList;

    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RichEditor question,option1,option2,option3,option4;
        RichEditor description_details;
        CardView llA,llB,llC,llD;
        ImageView up_iv,down_iv;
        TextView description_title;
        ScrollView scrollView;

        public MyViewHolder(View view) {
            super(view);
            question = (RichEditor) view.findViewById(R.id.tv_question_num);
            option1 = (RichEditor) view.findViewById(R.id.option_a);
            option2 = (RichEditor) view.findViewById(R.id.option_b);
            option3 = (RichEditor) view.findViewById(R.id.option_c);
            option4 = (RichEditor) view.findViewById(R.id.option_d);
            llA = (CardView) view.findViewById(R.id.optionA);
            llB = (CardView) view.findViewById(R.id.optionB);
            llC = (CardView) view.findViewById(R.id.optionC);
            llD = (CardView) view.findViewById(R.id.optionD);

            description_details = (RichEditor) view.findViewById(R.id.description_details);
            description_details.setClickable(false);
            description_details.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
            description_title = (TextView) view.findViewById(R.id.descption_title);
            up_iv = (ImageView) view.findViewById(R.id.up_iv);
            down_iv = (ImageView) view.findViewById(R.id.down_iv);
            scrollView = (ScrollView) view.findViewById(R.id.scrollView);






        }
    }

    public ReviewAnswer_Adapter(List<ReviewSolutionQuestionList> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_review_ans_new, parent, false);




        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        ReviewSolutionQuestionList list = moviesList.get(position);
        int pos = position+1;
        holder.description_details.setInputEnabled(false);
        holder.description_details.setEnabled(true);
        holder.description_details.loadCSS("* {" +
                "   -webkit-user-select: none;" +
                "}");

        if(list.getAnswers() != null) {
            holder.description_details.setHtml
                    (list.getAnswers().getAnswer_desc());
        }else{
            holder.description_details.setHtml("Keep exploring, till we are back with explanation");
        }
        holder.description_details.setVisibility(View.GONE);
        holder.description_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(holder.description_details.getVisibility() == View.GONE) {
                    holder.description_details.setVisibility(View.VISIBLE);
                    holder.down_iv.setVisibility(View.GONE);
                    holder.up_iv.setVisibility(View.VISIBLE);

                    holder.scrollView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            holder.scrollView.smoothScrollTo(0,holder.scrollView.getHeight());
                        }
                    },100);

                }else if(holder.description_details.getVisibility() == View.VISIBLE){
                    holder.description_details.setVisibility(View.GONE);
                    holder.up_iv.setVisibility(View.GONE);
                    holder.down_iv.setVisibility(View.VISIBLE);



                }

            }
        });
        holder.up_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(holder.description_details.getVisibility() == View.GONE) {
                    holder.description_details.setVisibility(View.VISIBLE);
                    holder.up_iv.setVisibility(View.GONE);
                    holder.down_iv.setVisibility(View.VISIBLE);

                    holder.scrollView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            holder.scrollView.smoothScrollTo(0,holder.scrollView.getHeight());
                        }
                    },100);

                }else if(holder.description_details.getVisibility() == View.VISIBLE){
                    holder.description_details.setVisibility(View.GONE);
                    holder.up_iv.setVisibility(View.VISIBLE);
                    holder.down_iv.setVisibility(View.GONE);



                }

            }
        });
        holder.question.setHtml(+pos + ". " +list.getQuestion());

        if(list.getOptions()!=null) {
            holder.option1.setHtml("A. "+ list.getOptions().get(0).getOption());
            holder.option2.setHtml("B. "+ list.getOptions().get(1).getOption());
            holder.option3.setHtml("C. "+ list.getOptions().get(2).getOption());
            holder.option4.setHtml("D. "+ list.getOptions().get(3).getOption());
        }


       holder.question.setHapticFeedbackEnabled(false);
        holder.question.setClickable(false);
        holder.question.setLongClickable(false);
        holder.question.setFocusableInTouchMode(false);


        holder.question.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        holder.option1.setHapticFeedbackEnabled(false);
        holder.option1.setClickable(false);
        holder.option1.setLongClickable(false);
        holder.option1.setFocusableInTouchMode(false);

        holder.option1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {


                }


                return true;

            }
        });


        holder.option2.setHapticFeedbackEnabled(false);
        holder.option2.setClickable(false);
        holder.option2.setLongClickable(false);
        holder.option2.setFocusableInTouchMode(false);

        holder.option2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {


                }


                return true;

            }
        });


        holder.option3.setHapticFeedbackEnabled(false);
        holder.option3.setClickable(false);
        holder.option3.setLongClickable(false);
        holder.option3.setFocusableInTouchMode(false);
        holder.option3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {


                }


                return true;

            }
        });


        holder.option4.setHapticFeedbackEnabled(false);
        holder.option4.setClickable(false);
        holder.option4.setLongClickable(false);
        holder.option4.setFocusableInTouchMode(false);

        holder.option4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {


                }


                return true;

            }
        });



        int correctAnswer=-1;
        int userAnswer=-1;
        for (int i=0;i<list.getOptions().size();i++)
        {
            if((Integer)list.getAnswers().getCorrectAnswerId()!=null) {
                if (list.getOptions().get(i).getOptionID() == list.getAnswers().getCorrectAnswerId()) {
                    correctAnswer = i;

                }
            }

            if((Integer)list.getAnswers().getUserAnswerId()!=null) {
                if (list.getOptions().get(i).getOptionID() == list.getAnswers().getUserAnswerId()) {
                    userAnswer = i;

                }
            }
           else if((Integer)list.getAnswers().getUserAnswerId() ==null) {
                if (list.getOptions().get(i).getOptionID() == list.getAnswers().getCorrectAnswerId()) {
                    correctAnswer = i;

                }
            }

        }

        switch (correctAnswer)
        {
            case 0 :
                holder.option1.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.option2.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.option3.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.option4.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.llA.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_green));
                holder.llB.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                holder.llC.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                holder.llD.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                holder.option1.setTag(correctAnswer);
                break;
            case 1 :
                holder.option1.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.option2.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.option3.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.option4.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.llA.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                holder.llB.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_green));
                holder.llC.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                holder.llD.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                holder.option2.setTag(correctAnswer);
                break;
            case 2 :

                holder.option1.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.option2.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.option3.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.option4.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.llA.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                holder.llB.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                holder.llC.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_green));
                holder.llD.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                holder.option3.setTag(correctAnswer);
                break;
            case 3 :
                holder.option1.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.option2.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.option3.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.option4.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                holder.llA.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                holder.llB.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                holder.llC.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                holder.llD.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_green));
                holder.option4.setTag(correctAnswer);
                break;

        }

        Log.v(ReviewAnswer_Adapter.class.getSimpleName(),
                correctAnswer+":"+userAnswer);
        if(correctAnswer!=userAnswer)
        {
            // set red background

            switch (userAnswer)
            {
                case 0 :


                    holder.llA.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_red));
                    if(holder.option2.getTag()==null)
                    holder.llB.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                    if(holder.option3.getTag()==null)
                    holder.llC.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                    if(holder.option4.getTag()==null)
                    holder.llD.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                    break;
                case 1 :

                    holder.llB.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_red));
                    if(holder.option1.getTag()==null)
                    holder.llA.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                    if(holder.option3.getTag()==null)
                    holder.llC.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                    if(holder.option4.getTag()==null)

                    holder.llD.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                    break;
                case 2 :


                    holder.llC.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_red));
                    if(holder.option2.getTag()==null)
                    holder.llB.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                    if(holder.option1.getTag()==null)
                    holder.llA.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));

                    if(holder.option4.getTag()==null)
                    holder.llD.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                    break;
                case 3 :
                    if(holder.option2.getTag()==null)
                    holder.llB.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));

                    if(holder.option3.getTag()==null)
                    holder.llC.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                    if(holder.option1.getTag()==null)
                    holder.llA.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_white));
                    holder.llD.setBackground(context.getResources().getDrawable(R.drawable.rev_bg_red));
                    break;

            }
        }



    }


    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent,
                                    final View child, final Point accumulatedOffset) {
        ViewGroup parentGroup = (ViewGroup) parent;
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) {
            return;
        }
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }

}
