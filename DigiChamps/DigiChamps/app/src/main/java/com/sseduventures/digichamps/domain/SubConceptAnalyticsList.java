package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class SubConceptAnalyticsList implements Parcelable{
        private int SubConceptsAppeared;
        private int TotalWeakSubConcepts;
        private int SubjectId;
        private String Subject;

        public int getSubConceptsAppeared() {
            return SubConceptsAppeared;
        }

        public void setSubConceptsAppeared(int subConceptsAppeared) {
            SubConceptsAppeared = subConceptsAppeared;
        }

        public int getTotalWeakSubConcepts() {
            return TotalWeakSubConcepts;
        }

        public void setTotalWeakSubConcepts(int totalWeakSubConcepts) {
            TotalWeakSubConcepts = totalWeakSubConcepts;
        }

        public int getSubjectId() {
            return SubjectId;
        }

        public void setSubjectId(int subjectId) {
            SubjectId = subjectId;
        }

        public String getSubject() {
            return Subject;
        }

        public void setSubject(String subject) {
            Subject = subject;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.SubConceptsAppeared);
            dest.writeInt(this.TotalWeakSubConcepts);
            dest.writeInt(this.SubjectId);
            dest.writeString(this.Subject);
        }

        public SubConceptAnalyticsList() {
        }

        protected SubConceptAnalyticsList(Parcel in) {
            this.SubConceptsAppeared = in.readInt();
            this.TotalWeakSubConcepts = in.readInt();
            this.SubjectId = in.readInt();
            this.Subject = in.readString();
        }

        public  final Creator<SubConceptAnalyticsList> CREATOR = new Creator<SubConceptAnalyticsList>() {
            @Override
            public SubConceptAnalyticsList createFromParcel(Parcel source) {
                return new SubConceptAnalyticsList(source);
            }

            @Override
            public SubConceptAnalyticsList[] newArray(int size) {
                return new SubConceptAnalyticsList[size];
            }
        };
    }