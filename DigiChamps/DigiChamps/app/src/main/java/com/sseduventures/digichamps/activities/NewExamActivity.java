package com.sseduventures.digichamps.activities;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.ExamDetailsSqlModel;
import com.sseduventures.digichamps.Model.Model_Exam;
import com.sseduventures.digichamps.Model.Model_option;
import com.sseduventures.digichamps.Model.ProgressModule;
import com.sseduventures.digichamps.Model.RealmExamDetails;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.FullImageActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.activity.Questions_Review_Activity;
import com.sseduventures.digichamps.activity.ScoreActivity;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.config.Prefs;
import com.sseduventures.digichamps.database.DbContract;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.model.ExamAnswers;
import com.sseduventures.digichamps.webservice.model.ExamQuestion;
import com.sseduventures.digichamps.webservice.model.GetExamResponse;
import com.sseduventures.digichamps.webservice.model.GetPostExamData;
import com.sseduventures.digichamps.webservice.model.GetQuestion;
import com.sseduventures.digichamps.webservice.model.PostQuestionRequestModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jp.wasabeef.richeditor.RichEditor;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NewExamActivity extends FormActivity implements ServiceReceiver.Receiver {
    private static CountDownTimer countDownTimer;
    RichEditor optA, optB, optC, optD, tv_qes;
    ImageView qes_fig1, qes_fig2, qes_fig3, qes_fig4, optA_fig, optB_fig, optC_fig, optD_fig;
    ImageView next, prev;
    String user_id;
    LinearLayout linear_lyout,lnrNextPrvsRev;
    ScrollView scrollView;
    RelativeLayout relativeTimeFinish;
    Button view_ques, test_end_btn;
    public View.OnClickListener mOnClickListener;
    TextView tv_qes_num, prvs_text, next_text;
    private boolean userIsOut;
    LinearLayout lla, llb, llc, lld;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private ExamDetailsSqlModel examDetailsSqlModel;

    private static TextView tv_timer;
    private TextView tv_ques_rem;
    private int i = 0;
    private ArrayList<Model_Exam> data;
    private ArrayList<Model_option> Alloption;
    private CardView optionA, optionB, optionC, optionD;

    private String question_desc;

    //variables for getting intent data
    private Bundle extras;
    int newString;

    private CheckBox skip;
    private boolean isInternetPresent = false;
    private String option_img, start_time, end_time,
            time, start_date, end_date, activity, exam_id;

    private int result_id;
    private SpotsDialog dialog;
    private boolean flag_submit = false;
    private LinearLayout lnr_prvs, linearLayout;



    private boolean submited = true;
    private String User_Id;
    ArrayList<RealmExamDetails> results;
    ;
    private boolean isFullScreenTrue = false;
    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        // preventing screen capture
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setModuleName(LogEventUtil.EVENT_NEW_EXAM_ACTIVITY);
        logEvent(LogEventUtil.EVENT_NEW_EXAM_ACTIVITY,
                LogEventUtil.KEY_NEW_EXAM_ACTIVITY);
        setContentView(R.layout.activity_exam_new);
        //For font_roboto_regular type
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);


        // database = new DatabaseHelper(NewExamActivity.this);

        //get realm instance
        // this.realm = RealmController.with(this).getRealm();


        //retrieve all results
        //results = realm.where(RealmExamDetails.class).findAll();
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        init();

        linear_lyout = (LinearLayout) findViewById(R.id.linear_lyout);
        lnrNextPrvsRev = findViewById(R.id.lnrNextPrvsRev);
        scrollView = findViewById(R.id.scrollView);
        relativeTimeFinish = findViewById(R.id.relativeTimeFinish);
        linear_lyout.setVisibility(View.GONE);
        scrollView.setVisibility(View.GONE);
        relativeTimeFinish.setVisibility(View.GONE);
        lnrNextPrvsRev.setVisibility(View.GONE);

        qes_fig1.setOnClickListener(onClickListener);
        qes_fig2.setOnClickListener(onClickListener);
        qes_fig3.setOnClickListener(onClickListener);
        qes_fig4.setOnClickListener(onClickListener);

        optA_fig.setOnClickListener(onClickListener);
        optB_fig.setOnClickListener(onClickListener);
        optC_fig.setOnClickListener(onClickListener);
        optD_fig.setOnClickListener(onClickListener);


        tv_ques_rem.setText(" ");

        optionB.setBackgroundColor(Color.TRANSPARENT);
        optionC.setBackgroundColor(Color.TRANSPARENT);
        optionD.setBackgroundColor(Color.TRANSPARENT);
        optionA.setBackgroundColor(Color.TRANSPARENT);
        optA.setBackgroundColor(Color.TRANSPARENT);
        optB.setBackgroundColor(Color.TRANSPARENT);
        optC.setBackgroundColor(Color.TRANSPARENT);
        optD.setBackgroundColor(Color.TRANSPARENT);


        //Convert minutes into milliseconds

        //start countdown

        //getting intent data from ExamQuestion review activity
        extras = getIntent().getExtras();
        /* fetching the int passed with intent using ‘extras’*/
        if (extras != null) {
            newString = extras.getInt("position");
            activity = extras.getString("activity");//chap_id
            exam_id = extras.getString("exam_id");
            result_id = extras.getInt("result_id");

            if (activity == null) {
                try {
                    // realm.beginTransaction();
                    //your operations here
                    //deleting all results
                    //  realm.clear(RealmExamDetails.class);
                    // realm.commitTransaction();
                    if (AppUtil.isInternetConnected(NewExamActivity.this)) {


                        user_id = RegPrefManager.getInstance(this)
                                .getRegId() + "";
                        //getLoginStatus();
                        // getExamData(user_id, exam_id);
                        data = new ArrayList<Model_Exam>();
                        Bundle b = new Bundle();
                        b.putString("Exam_ID", exam_id);


                        mShimmerViewContainer.startShimmer();
                        mShimmerViewContainer.setVisibility(View.VISIBLE);
                        //showDialog(null, "We're preparing right questions for you");
                        NetworkService.startActionGetExamData(NewExamActivity.this,
                                mServiceReceiver, b);

                    } else {
                        startActivity(new Intent(getApplicationContext(), Internet_Activity.class));
                        finish();
                    }

                } catch (Exception e) {
                    // realm.cancelTransaction();
                }
            } else {
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);
                linear_lyout.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.VISIBLE);
                relativeTimeFinish.setVisibility(View.VISIBLE);
                lnrNextPrvsRev.setVisibility(View.VISIBLE);

                data = this.getIntent().getExtras().getParcelableArrayList("Birds");
                results = this.getIntent().getExtras().getParcelableArrayList("Results");
                newString = getIntent().getIntExtra("position", 0);
                result_id = getIntent().getIntExtra("result_id", 0);
                if (newString < results.size()) {
                    // data = (ArrayList<Model_Exam>)getIntent().getSerializableExtra("FILES_TO_SEND");
//                    prev.setVisibility(View.INVISIBLE);
//                    prvs_text.setVisibility(View.INVISIBLE);
//                    if (data.size() > 1) {
//                        next.setVisibility(View.VISIBLE);
//                        next_text.setVisibility(View.VISIBLE);
//                    } else {
//                        next.setVisibility(View.INVISIBLE);
//                        next_text.setVisibility(View.INVISIBLE);
//                    }

                    i = newString;
                    //showToast(result_id+"");
                    fetchdata(i);

                    setAnswer();
                    //method call for setting data in to questions
                }
            }

        }

        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkSnackbar();

            }
        };
        view_ques.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag_submit = true;
                //redirect to view question activity
                Intent intent = new Intent(
                        NewExamActivity.this,
                        Questions_Review_Activity.class);

                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("Birds", data);
                bundle.putParcelableArrayList("Results", results);
                intent.putExtras(bundle);
                intent.putExtra("exam_id", exam_id);
                intent.putExtra("timer_value",
                        tv_timer.getText().toString());
                //showToast(result_id+"");
                intent.putExtra("result_id", result_id);
                intent.putExtra("module_type", getIntent()
                        .getStringExtra("module_type"));
                intent.putExtra("chapter_id", getIntent()
                        .getStringExtra("chapter_id"));
                intent.putExtra("subject_id", getIntent()
                        .getStringExtra("subject_id"));
                intent.putExtra("subNameNewNew", getIntent().getStringExtra("subName"));
                intent.putExtra("flagNameNewNew", getIntent().getStringExtra("flagName"));


                startActivity(intent);

            }
        });

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int k = i;

                if (i < data.size() - 1) {
                    // getAllUsers();
                    ++i;
                    if (i == 0) {
                        prev.setVisibility(View.INVISIBLE);
                        prvs_text.setVisibility(View.INVISIBLE);
                        // previous.setVisibility(View.INVISIBLE);
                    } else {
                        prev.setVisibility(View.VISIBLE);
                        prvs_text.setVisibility(View.VISIBLE);
                    }

                    if (i == data.size() - 1) {
                        next.setVisibility(View.INVISIBLE);
                        next_text.setVisibility(View.INVISIBLE);
                    }

                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);
                    skip.setChecked(false);
                    try {
                        if (results.get(i - 1).getoption() == 0)
                            results.get(i - 1).setSkip(1);

                        setAnswer();
                        fetchdata(i);

                    } catch (Exception e) {

                    }


                }

            }
        });
        lnr_prvs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (i >= 1) {
                    --i;
                    if (i == 0) {
                        prev.setVisibility(View.INVISIBLE);
                        prvs_text.setVisibility(View.INVISIBLE);
                    } else {
                        prev.setVisibility(View.VISIBLE);
                        prvs_text.setVisibility(View.VISIBLE);
                    }

                    if (data.size() > 1) {
                        next.setVisibility(View.VISIBLE);
                        next_text.setVisibility(View.VISIBLE);

                    } else {
                        next.setVisibility(View.INVISIBLE);
                        next_text.setVisibility(View.INVISIBLE);
                    }


                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);
                    skip.setChecked(false);
                    try {
                        setAnswer();


                        fetchdata(i);

                    } catch (Exception e) {

                    }

                }

            }
        });
        //End test
        test_end_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(NewExamActivity.this)
                        .setIcon(R.drawable.alert_warning)
                        .setTitle("End Test")
                        .setMessage("Do you want to End Test?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                DbTask dbTask = new DbTask();
                                dbTask.execute(getIntent()
                                                .getStringExtra("module_type"),
                                        getIntent()
                                                .getStringExtra("chapter_id"),
                                        getIntent()
                                                .getStringExtra("subject_id"));

                                if (AppUtil.isInternetConnected(NewExamActivity.this)) {

                                    postExamData();
                                    countDownTimer.cancel();
                                } else {
                                    new AlertDialog.Builder(NewExamActivity.this)
                                            .setIcon(R.drawable.alert_warning)
                                            .setTitle("No Internet!")
                                            .setMessage("Please Connect to Internet.")
                                            .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    checkSnackbar();
                                                }
                                            })
                                            .show();
                                }

                            }
                        })
                        .setNegativeButton("No", null).show();

            }
        });

        // RichEditor optA, optB, optC, optD, tv_qes;
        tv_qes.setHapticFeedbackEnabled(false);
        tv_qes.setClickable(false);
        tv_qes.setLongClickable(false);
        //tv_qes.setOnTouchListener(null);
        tv_qes.setFocusableInTouchMode(false);

        tv_qes.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //Toast.makeText(NewNewExamActivity.this,"A",Toast.LENGTH_SHORT).show();
                    setTouched(v.getId());

                }


                return true;

            }
        });


        optA.setHapticFeedbackEnabled(false);
        optA.setClickable(false);
        optA.setLongClickable(false);

        optA.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //Toast.makeText(NewNewExamActivity.this,"A",Toast.LENGTH_SHORT).show();
                    setTouched(v.getId());

                }


                return true;

            }
        });


        optB.setHapticFeedbackEnabled(false);
        optB.setClickable(false);
        optB.setLongClickable(false);
        // optB.setOnTouchListener(null);
        optB.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    //Toast.makeText(NewNewExamActivity.this,"B",Toast.LENGTH_SHORT).show();

                    // Toast.makeText(NewNewExamActivity.this,"B",Toast.LENGTH_SHORT).show();

                    setTouched(v.getId());

                }


                return true;

            }
        });


        optC.setHapticFeedbackEnabled(false);
        optC.setClickable(false);
        optC.setLongClickable(false);
        // optC.setOnTouchListener(null);
        optC.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //Toast.makeText(NewNewExamActivity.this,"C",Toast.LENGTH_SHORT).show();
                    setTouched(v.getId());

                }


                return true;

            }
        });


        optD.setHapticFeedbackEnabled(false);
        optD.setClickable(false);
        optD.setLongClickable(false);
        // optD.setOnTouchListener(null);
        optD.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Toast.makeText(NewNewExamActivity.this,"D",Toast.LENGTH_SHORT).show();
                    setTouched(v.getId());

                }


                return true;

            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }


    @Override
    protected void onPause() {

        if (isFullScreenTrue) {

            isFullScreenTrue = false;
        } else {
            if (flag_submit == false) {

                if (countDownTimer != null) {
                    //stop time
                    countDownTimer.cancel();
                    //send exam data to server
                    if (submited) {
                        postExamData();
                        Intent intent = new Intent(NewExamActivity.this, LearnActivity.class);
                        startActivity(intent);
                    }
                }
            } else {

            }
        }
        //Log("exam_submit_onpause", "#onPause");
        super.onPause();
    }


    // Inject into Context for font change
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    //method for onclick of different image views
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.opt_img1:
                    //calling method show image for showing respective image
                    show_images(optA_fig.getDrawable());
                    break;
                case R.id.opt_img2:
                    //calling method show image for showing respective image
                    show_images(optB_fig.getDrawable());
                    break;
                case R.id.opt_img3:
                    //calling method show image for showing respective image
                    show_images(optC_fig.getDrawable());
                    break;
                case R.id.opt_img4:
                    //calling method show image for showing respective image
                    show_images(optD_fig.getDrawable());
                    break;
                /*case R.id.qes_img1:
                    //DO something
                    //show_images(qes_fig1.getDrawable());
                    break;*/
                case R.id.qes_img2:
                    //calling method show image for showing respective image
                    show_images(qes_fig2.getDrawable());
                    break;
                case R.id.qes_img3:
                    //calling method show image for showing respective image
                    show_images(qes_fig3.getDrawable());
                    break;
                case R.id.qes_img4:
                    //calling method show image for showing respective image
                    show_images(qes_fig4.getDrawable());
                    break;

            }

        }
    };

    // calculate time difference
    public int calculateTime() {
        long difference;
        try {
            String format = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Date fromDate = sdf.parse(start_date + " " + start_time);
            Date toDate = sdf.parse(end_date + " " + end_time);

            difference = toDate.getTime() - fromDate.getTime();

        } catch (Exception e) {
            difference = 0;
        }

        return (int) difference;
    }

    //initialisation
    public void init() {

        //ImageButton

        examDetailsSqlModel = new ExamDetailsSqlModel();
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        lnr_prvs = (LinearLayout) findViewById(R.id.lnr_prvs);

        lla = (LinearLayout) findViewById(R.id.llA);
        llb = (LinearLayout) findViewById(R.id.llB);
        llc = (LinearLayout) findViewById(R.id.llC);
        lld = (LinearLayout) findViewById(R.id.llD);


        lla.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //Toast.makeText(NewNewExamActivity.this,"AL",Toast.LENGTH_SHORT).show();

                    setTouched(v.getId());
                }


                return true;

            }
        });

        lld.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //Toast.makeText(NewNewExamActivity.this,"DL",Toast.LENGTH_SHORT).show();
                    setTouched(v.getId());
                }


                return true;

            }
        });
        llc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //Toast.makeText(NewNewExamActivity.this,"CL",Toast.LENGTH_SHORT).show();
                    setTouched(v.getId());
                }


                return true;

            }
        });
        llb.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    // Toast.makeText(NewNewExamActivity.this,"BL",Toast.LENGTH_SHORT).show();
                    setTouched(v.getId());
                }


                return true;

            }
        });


        next = (ImageView) findViewById(R.id.bt_next);
        prev = (ImageView) findViewById(R.id.bt_prev);
        prvs_text = (TextView) findViewById(R.id.prvs_text);
        next_text = (TextView) findViewById(R.id.next_text);
        //CheckBox
        skip = (CheckBox) findViewById(R.id.checkbox_exam);
        //Button
        view_ques = (Button) findViewById(R.id.bt_question);
        test_end_btn = (Button) findViewById(R.id.end_test_text);
        //TextView
        optA = (RichEditor) findViewById(R.id.option_a);
        optB = (RichEditor) findViewById(R.id.option_b);
        optC = (RichEditor) findViewById(R.id.option_c);
        optD = (RichEditor) findViewById(R.id.option_d);

        optA.setInputEnabled(false);
        optA.setEnabled(true);
        optA.loadCSS("* {" +
                "   -webkit-user-select: none;" +
                "}");


        optB.setInputEnabled(false);
        optB.setEnabled(true);
        optB.loadCSS("* {" +
                "   -webkit-user-select: none;" +
                "}");

        optC.setInputEnabled(false);
        optC.setEnabled(true);
        optC.loadCSS("* {" +
                "   -webkit-user-select: none;" +
                "}");

        optD.setInputEnabled(false);
        optD.setEnabled(true);
        optD.loadCSS("* {" +
                "   -webkit-user-select: none;" +
                "}");


        tv_qes = (RichEditor) findViewById(R.id.tv_question);
        tv_qes_num = (TextView) findViewById(R.id.tv_question_num);
        tv_timer = (TextView) findViewById(R.id.tv_clock);
        tv_ques_rem = (TextView) findViewById(R.id.tv_question_number);
        //CardView
        optionA = (CardView) findViewById(R.id.optionA);
        optionA.setVisibility(View.GONE);
        optionB = (CardView) findViewById(R.id.optionB);
        optionB.setVisibility(View.GONE);
        optionC = (CardView) findViewById(R.id.optionC);
        optionC.setVisibility(View.GONE);
        optionD = (CardView) findViewById(R.id.optionD);
        optionD.setVisibility(View.GONE);
        //ImageView
        optA_fig = (ImageView) findViewById(R.id.opt_img1);
        optB_fig = (ImageView) findViewById(R.id.opt_img2);
        optC_fig = (ImageView) findViewById(R.id.opt_img3);
        optD_fig = (ImageView) findViewById(R.id.opt_img4);
        qes_fig1 = (ImageView) findViewById(R.id.qes_img1);
        qes_fig2 = (ImageView) findViewById(R.id.qes_img2);
        qes_fig3 = (ImageView) findViewById(R.id.qes_img3);
        qes_fig4 = (ImageView) findViewById(R.id.qes_img4);

        // initialisation of connection helper

        //Progress dialog initialisation
        dialog = new SpotsDialog(this, "FUN + EDUCATION", R.style.Custom);
        //text view for amount
    }

    // Skip the question and storing data into realm
    public void skip(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();
        try {
            // Check which checkbox was clicked
            switch (view.getId()) {
                case R.id.checkbox_exam:
                    if (checked) {


                        results.get(i).setReviewed(1);

                    } else {

                        results.get(i).setReviewed(0);
                    }
                    break;

            }
        } catch (Exception e) {

        }


    }


    private void setSqlLiteDatabase() {


    }

    //method for adding data to realm database
    private void setRealmData() {

        results = new ArrayList<>();

        //adding data to array list
        for (int i = 0; i < data.size(); i++) {
            RealmExamDetails book = new RealmExamDetails();
            book.setQuestion(data.get(i).getQuestion());
            book.setques_img1(R.drawable.area_triangle);
            book.setques_img2(0);
            book.setSkip(0);
            results.add(book);
        }


        for (RealmExamDetails b : results) {
            //data saving in sqlite
        }

        Prefs.with(this).setPreLoad(true);


    }

    public int getOption() {
        RealmExamDetails book = results.get(i);
        return book.getoption();
    }

    public int getSkipped() {

        RealmExamDetails book = results.get(i);
        return book.getSkip();
    }

    // method for keeping selected answer stored in to realm data base.
    public void setAnswer() {
        int j = results.get(i).getoption();
//        showToast(i+":"+j+"");
//        if (j== 0) {
//            results.get(i).setSkip(1);
//        }

        if (results.get(i).getReviewed() == 1)
            skip.setChecked(true);
        else
            skip.setChecked(false);

        switch (j) {
            case 1:
                //highlight selected option


                lla.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                optionB.setBackgroundColor(Color.TRANSPARENT);
                optionC.setBackgroundColor(Color.TRANSPARENT);
                optionD.setBackgroundColor(Color.TRANSPARENT);
                optionA.setBackgroundColor(Color.TRANSPARENT);
                optA.setBackgroundColor(Color.TRANSPARENT);
                optB.setBackgroundColor(Color.TRANSPARENT);
                optC.setBackgroundColor(Color.TRANSPARENT);
                optD.setBackgroundColor(Color.TRANSPARENT);
                break;
            case 2:
                //highlight selected option

                lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                llb.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                optionB.setBackgroundColor(Color.TRANSPARENT);
                optionC.setBackgroundColor(Color.TRANSPARENT);
                optionD.setBackgroundColor(Color.TRANSPARENT);
                optionA.setBackgroundColor(Color.TRANSPARENT);
                optA.setBackgroundColor(Color.TRANSPARENT);
                optB.setBackgroundColor(Color.TRANSPARENT);
                optC.setBackgroundColor(Color.TRANSPARENT);
                optD.setBackgroundColor(Color.TRANSPARENT);
                break;
            case 3:
                //highlight selected option

                lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                llc.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                optionB.setBackgroundColor(Color.TRANSPARENT);
                optionC.setBackgroundColor(Color.TRANSPARENT);
                optionD.setBackgroundColor(Color.TRANSPARENT);
                optionA.setBackgroundColor(Color.TRANSPARENT);
                optA.setBackgroundColor(Color.TRANSPARENT);
                optB.setBackgroundColor(Color.TRANSPARENT);
                optC.setBackgroundColor(Color.TRANSPARENT);
                optD.setBackgroundColor(Color.TRANSPARENT);

                break;
            case 4:
                //highlight selected option

                lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                lld.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                optionB.setBackgroundColor(Color.TRANSPARENT);
                optionC.setBackgroundColor(Color.TRANSPARENT);
                optionD.setBackgroundColor(Color.TRANSPARENT);
                optionA.setBackgroundColor(Color.TRANSPARENT);
                optA.setBackgroundColor(Color.TRANSPARENT);
                optB.setBackgroundColor(Color.TRANSPARENT);
                optC.setBackgroundColor(Color.TRANSPARENT);
                optD.setBackgroundColor(Color.TRANSPARENT);
                break;

        }
    }

    //Highlighting selected options
    public void setSelected(View view) {
//        if (skip.isChecked()) {
//            skip.setChecked(false);
//           // realm.beginTransaction();
//            results.get(i).setSkip(0);
//           // realm.commitTransaction();
//        }

        try {
            switch (view.getId()) {

                case R.id.option_a:
                    //Toast.makeText(NewExamActivity.this, "lla", Toast.LENGTH_LONG).show();

                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);

                    results.get(i).setoption(1);
                    results.get(i).setAnswerId(data.get(i).getoptions().get(0).getAnswer_ID());
                    break;
                case R.id.optionB:
                    //Toast.makeText(NewExamActivity.this, "B", Toast.LENGTH_LONG).show();

                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);

                    results.get(i).setoption(2);
                    results.get(i).setAnswerId(data.get(i).getoptions().get(1).getAnswer_ID());
                    break;
                case R.id.optionC:
                    //Toast.makeText(NewExamActivity.this, "C", Toast.LENGTH_LONG).show();

                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);
                    results.get(i).setoption(3);
                    results.get(i).setAnswerId(data.get(i).getoptions().get(2).getAnswer_ID());
                    break;
                case R.id.optionD:
                    //Toast.makeText(NewExamActivity.this, "D", Toast.LENGTH_LONG).show();

                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);
                    results.get(i).setoption(4);
                    results.get(i).setAnswerId(data.get(i).getoptions().get(3).getAnswer_ID());
                    break;
            }
        } catch (Exception e) {

        }


    }

    private void setTouched(int id) {
//        if (skip.isChecked()) {
//            skip.setChecked(false);
//            results.get(i).setSkip(0);
//        }
        try {
            switch (id) {

                case R.id.option_a:
                    //Toast.makeText(NewNewExamActivity.this,"lla", Toast.LENGTH_LONG).show();

                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);

                    results.get(i).setoption(1);
                    results.get(i).setAnswerId(data.get(i).getoptions().get(0).getAnswer_ID());

                    break;
                case R.id.llA:
                    // Toast.makeText(NewNewExamActivity.this,"lla", Toast.LENGTH_LONG).show();

                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);
                    results.get(i).setoption(1);
                    results.get(i).setAnswerId(data.get(i).getoptions().get(0).getAnswer_ID());
                    break;
                case R.id.option_b:
                    // Toast.makeText(NewNewExamActivity.this,"B", Toast.LENGTH_LONG).show();

                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);
                    results.get(i).setoption(2);
                    results.get(i).setAnswerId(data.get(i).getoptions().get(1).getAnswer_ID());
                    break;
                case R.id.llB:
                    //Toast.makeText(NewNewExamActivity.this,"B", Toast.LENGTH_LONG).show();
                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);
                    results.get(i).setoption(2);
                    results.get(i).setAnswerId(data.get(i).getoptions().get(1).getAnswer_ID());
                    break;
                case R.id.option_c:
                    //Toast.makeText(NewNewExamActivity.this,"C", Toast.LENGTH_LONG).show();

                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);
                    results.get(i).setoption(3);
                    results.get(i).setAnswerId(data.get(i).getoptions().get(2).getAnswer_ID());
                    break;
                case R.id.llC:
                    //Toast.makeText(NewNewExamActivity.this,"C", Toast.LENGTH_LONG).show();

                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);
                    results.get(i).setoption(3);
                    results.get(i).setAnswerId(data.get(i).getoptions().get(2).getAnswer_ID());
                    break;
                case R.id.option_d:
                    //Toast.makeText(NewNewExamActivity.this,"D", Toast.LENGTH_LONG).show();

                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);
                    results.get(i).setoption(4);
                    results.get(i).setAnswerId(data.get(i).getoptions().get(3).getAnswer_ID());
                    break;
                case R.id.llD:
                    //Toast.makeText(NewNewExamActivity.this,"D", Toast.LENGTH_LONG).show();

                    lla.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llb.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    llc.setBackground(getResources().getDrawable(R.drawable.rect_card_exam));
                    lld.setBackground(getResources().getDrawable(R.drawable.rect_card_yellow));
                    optionB.setBackgroundColor(Color.TRANSPARENT);
                    optionC.setBackgroundColor(Color.TRANSPARENT);
                    optionD.setBackgroundColor(Color.TRANSPARENT);
                    optionA.setBackgroundColor(Color.TRANSPARENT);
                    optA.setBackgroundColor(Color.TRANSPARENT);
                    optB.setBackgroundColor(Color.TRANSPARENT);
                    optC.setBackgroundColor(Color.TRANSPARENT);
                    optD.setBackgroundColor(Color.TRANSPARENT);
                    results.get(i).setoption(4);
                    results.get(i).setAnswerId(data.get(i).getoptions().get(3).getAnswer_ID());
                    break;
            }
        } catch (Exception e) {

        }

    }


    //method for fetching data from model and setting to questions and options
    public void fetchdata(int j) {

        try {
            if (j < data.size()) {
                //tv_qes_num.setText(data.get(j).getQuestion_number());
                tv_qes.setHtml(data.get(j).getQuestion_number() + " " + data.get(j).getQuestion());

                //checking conditions for image is present or not

                int count = data.get(j).getImages().size();
                if (data.get(j).getImages().size() == 1) {
                    qes_fig1.setVisibility(View.VISIBLE);
                    Picasso.with(this).load(data.get(j).getImages().get(0)).
                            placeholder(R.drawable.loading).into(qes_fig1);
                    final int finalJ = j;
                    qes_fig1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isFullScreenTrue = true;
                            Intent in = new Intent(NewExamActivity.this, FullImageActivity.class);
                            in.putExtra("activity", "ExamActivity");
                            in.putExtra("url", data.get(finalJ).getImages().get(0));
                            startActivity(in);
                        }
                    });
                } else if (data.get(j).getImages().size() == 2) {
                    Picasso.with(this).load(data.get(j).getImages().get(0)).
                            placeholder(R.drawable.loading).into(qes_fig1);
                    Picasso.with(this).load(data.get(j).getImages().get(1)).
                            placeholder(R.drawable.loading).into(qes_fig2);

                    final int finalJ = j;
                    qes_fig1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isFullScreenTrue = true;
                            Intent in = new Intent(NewExamActivity.this, FullImageActivity.class);
                            in.putExtra("activity", "ExamActivity");
                            in.putExtra("url", data.get(finalJ).getImages().get(0));
                            startActivity(in);
                        }
                    });

                    qes_fig2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isFullScreenTrue = true;
                            Intent in = new Intent(NewExamActivity.this, FullImageActivity.class);
                            in.putExtra("activity", "ExamActivity");
                            in.putExtra("url", data.get(finalJ).getImages().get(1));
                            startActivity(in);
                        }
                    });

                    qes_fig1.setVisibility(View.VISIBLE);
                    qes_fig2.setVisibility(View.VISIBLE);
                } else if (data.get(j).getImages().size() == 3) {
                    qes_fig1.setVisibility(View.VISIBLE);
                    qes_fig2.setVisibility(View.VISIBLE);
                    qes_fig3.setVisibility(View.VISIBLE);
                    Picasso.with(this).load(data.get(j).getImages().get(0)).placeholder(R.drawable.loading).into(qes_fig1);
                    Picasso.with(this).load(data.get(j).getImages().get(1)).placeholder(R.drawable.loading).into(qes_fig2);
                    Picasso.with(this).load(data.get(j).getImages().get(2)).placeholder(R.drawable.loading).into(qes_fig3);

                    final int finalJ = j;
                    qes_fig1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isFullScreenTrue = true;
                            Intent in = new Intent(NewExamActivity.this, FullImageActivity.class);
                            in.putExtra("activity", "ExamActivity");
                            in.putExtra("url", data.get(finalJ).getImages().get(0));
                            startActivity(in);
                        }
                    });

                    qes_fig2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isFullScreenTrue = true;
                            Intent in = new Intent(NewExamActivity.this, FullImageActivity.class);
                            in.putExtra("activity", "ExamActivity");
                            in.putExtra("url", data.get(finalJ).getImages().get(1));
                            startActivity(in);
                        }
                    });


                    qes_fig3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isFullScreenTrue = true;
                            Intent in = new Intent(NewExamActivity.this, FullImageActivity.class);
                            in.putExtra("activity", "ExamActivity");
                            in.putExtra("url", data.get(finalJ).getImages().get(2));
                            startActivity(in);
                        }
                    });
                } else if (data.get(j).getImages().size() == 4) {
                    qes_fig1.setVisibility(View.VISIBLE);
                    qes_fig2.setVisibility(View.VISIBLE);
                    qes_fig3.setVisibility(View.VISIBLE);
                    qes_fig4.setVisibility(View.VISIBLE);
                    Picasso.with(this).load(data.get(j).getImages().get(0)).
                            placeholder(R.drawable.loading).into(qes_fig1);
                    Picasso.with(this).load(data.get(j).getImages().get(1)).
                            placeholder(R.drawable.loading).into(qes_fig2);
                    Picasso.with(this).load(data.get(j).getImages().get(2)).
                            placeholder(R.drawable.loading).into(qes_fig3);
                    Picasso.with(this).load(data.get(j).getImages().get(3)).
                            placeholder(R.drawable.loading).into(qes_fig4);

                    final int finalJ = j;
                    qes_fig1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isFullScreenTrue = true;
                            Intent in = new Intent(NewExamActivity.this, FullImageActivity.class);
                            in.putExtra("activity", "ExamActivity");
                            in.putExtra("url", data.get(finalJ).getImages().get(0));
                            startActivity(in);
                        }
                    });

                    qes_fig2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isFullScreenTrue = true;
                            Intent in = new Intent(NewExamActivity.this,
                                    FullImageActivity.class);
                            in.putExtra("activity", "ExamActivity");
                            in.putExtra("url", data.get(finalJ).getImages().get(1));
                            startActivity(in);
                        }
                    });

                    qes_fig3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isFullScreenTrue = true;
                            Intent in = new Intent(NewExamActivity.this,
                                    FullImageActivity.class);
                            in.putExtra("activity", "ExamActivity");
                            in.putExtra("url", data.get(finalJ).getImages().get(2));
                            startActivity(in);
                        }
                    });

                    qes_fig4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isFullScreenTrue = true;
                            Intent in = new Intent(NewExamActivity.this,
                                    FullImageActivity.class);
                            in.putExtra("activity", "ExamActivity");
                            in.putExtra("url", data.get(finalJ).getImages().get(3));
                            startActivity(in);
                        }
                    });

                } else {
                    qes_fig1.setVisibility(View.GONE);
                    qes_fig2.setVisibility(View.GONE);
                    qes_fig3.setVisibility(View.GONE);
                    qes_fig4.setVisibility(View.GONE);
                }

                if (data.get(j).getoptions().size() == 1) {
                    optionC.setVisibility(View.GONE);
                    optionB.setVisibility(View.GONE);
                    optionA.setVisibility(View.VISIBLE);
                    optionD.setVisibility(View.GONE);

                    optA.setHtml(data.get(j).getoptions().get(0).getOption_Desc());
                    if (data.get(j).getoptions().get(0).getOption_Image().
                            equalsIgnoreCase("")) {
                        optA_fig.setVisibility(View.GONE);
                    } else {
                        optA_fig.setVisibility(View.VISIBLE);

                        Picasso.with(this).load(data.get(j).getoptions().get(0).
                                getOption_Image()).placeholder(R.drawable.loading).into(optA_fig);
                    }
                } else if (data.get(j).getoptions().size() == 2) {
                    optionC.setVisibility(View.GONE);
                    optionB.setVisibility(View.VISIBLE);
                    optionA.setVisibility(View.VISIBLE);
                    optionD.setVisibility(View.GONE);
                    optA.setHtml(data.get(j).getoptions().get(0).getOption_Desc());
                    if (data.get(j).getoptions().get(0).getOption_Image().
                            equalsIgnoreCase("")) {
                        optA_fig.setVisibility(View.GONE);
                    } else {
                        optA_fig.setVisibility(View.VISIBLE);

                        Picasso.with(this).load(data.get(j).getoptions().get(0).
                                getOption_Image()).placeholder(R.drawable.loading).into(optA_fig);
                    }
                    optB.setHtml(data.get(j).getoptions().get(1).getOption_Desc());
                    if (data.get(j).getoptions().get(1).getOption_Image().
                            equalsIgnoreCase("")) {
                        optB_fig.setVisibility(View.GONE);
                    } else {
                        optB_fig.setVisibility(View.VISIBLE);
                        Picasso.with(this).load(data.get(j).getoptions().get(1).
                                getOption_Image()).placeholder(R.drawable.loading).into(optB_fig);

                    }

                } else if (data.get(j).getoptions().size() == 3) {
                    optionC.setVisibility(View.VISIBLE);
                    optionB.setVisibility(View.VISIBLE);
                    optionA.setVisibility(View.VISIBLE);
                    optionD.setVisibility(View.GONE);
                    optA.setHtml(data.get(j).getoptions().get(0).getOption_Desc());
                    if (data.get(j).getoptions().get(0).getOption_Image().
                            equalsIgnoreCase("")) {
                        optA_fig.setVisibility(View.GONE);
                    } else {
                        optA_fig.setVisibility(View.VISIBLE);

                        Picasso.with(this).load(data.get(j).getoptions().get(0).
                                getOption_Image()).placeholder(R.drawable.loading).into(optA_fig);
                    }
                    optB.setHtml(data.get(j).getoptions().get(1).getOption_Desc());
                    if (data.get(j).getoptions().get(1).getOption_Image().
                            equalsIgnoreCase("")) {
                        optB_fig.setVisibility(View.GONE);
                    } else {
                        optB_fig.setVisibility(View.VISIBLE);
                        Picasso.with(this).load(data.get(j).getoptions().get(1).getOption_Image()).placeholder(R.drawable.loading).into(optB_fig);

                    }
                    optC.setHtml(data.get(j).getoptions().get(2).getOption_Desc());
                    if (data.get(j).getoptions().get(2).getOption_Image().equalsIgnoreCase("")) {
                        optC_fig.setVisibility(View.GONE);
                    } else {
                        optC_fig.setVisibility(View.VISIBLE);
                        Picasso.with(this).load(data.get(j).getoptions().get(2).getOption_Image()).placeholder(R.drawable.loading).into(optC_fig);

                    }

                } else if (data.get(j).getoptions().size() == 4) {
                    optionC.setVisibility(View.VISIBLE);
                    optionB.setVisibility(View.VISIBLE);
                    optionA.setVisibility(View.VISIBLE);
                    optionD.setVisibility(View.VISIBLE);
                    optA.setHtml(data.get(j).getoptions().get(0).getOption_Desc());
                    if (data.get(j).getoptions().get(0).getOption_Image().equalsIgnoreCase("")) {
                        optA_fig.setVisibility(View.GONE);
                    } else {
                        optA_fig.setVisibility(View.VISIBLE);

                        Picasso.with(this).load(data.get(j).getoptions().get(0).getOption_Image()).placeholder(R.drawable.loading).into(optA_fig);

                    }
                    optB.setHtml(data.get(j).getoptions().get(1).getOption_Desc());
                    if (data.get(j).getoptions().get(1).getOption_Image().equalsIgnoreCase("")) {
                        optB_fig.setVisibility(View.GONE);
                    } else {
                        optB_fig.setVisibility(View.VISIBLE);
                        Picasso.with(this).load(data.get(j).getoptions().get(1).getOption_Image()).placeholder(R.drawable.loading).into(optB_fig);

                    }
                    optC.setHtml(data.get(j).getoptions().get(2).getOption_Desc());
                    if (data.get(j).getoptions().get(2).getOption_Image().equalsIgnoreCase("")) {
                        optC_fig.setVisibility(View.GONE);
                    } else {
                        optC_fig.setVisibility(View.VISIBLE);
                        Picasso.with(this).load(data.get(j).getoptions().get(2).getOption_Image()).placeholder(R.drawable.loading).into(optC_fig);

                    }
                    optD.setHtml(data.get(j).getoptions().get(3).getOption_Desc());
                    if (data.get(j).getoptions().get(3).getOption_Image().equalsIgnoreCase("")) {
                        optD_fig.setVisibility(View.GONE);
                    } else {
                        optD_fig.setVisibility(View.VISIBLE);
                        Picasso.with(this).load(data.get(j).getoptions().get(3).getOption_Image()).placeholder(R.drawable.loading).into(optD_fig);

                    }
                }

            }
            //else
            // showToast("size : "+data.size());
            j = j++;
            if (j++ == data.size()) {
                next.setVisibility(View.INVISIBLE);
            }

        } catch (Exception e) {

            showToast("Exception" + e.getMessage());
        }


    }

    @Override
    protected void onDestroy() {
        userIsOut = true;
        super.onDestroy();
    }

    //Start Count down method
    private void startTimer(int noOfMinutes) {
        countDownTimer = new CountDownTimer(noOfMinutes, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds
                String hms = String.format("%02d:%02d:%02d",
                        TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                tv_timer.setText(hms);//set text

            }

            public void onFinish() {
                tv_timer.setText("Time out!");
                // display alert dialog

                if (!userIsOut) {
                    new AlertDialog.Builder(NewExamActivity.this)
                            .setIcon(R.drawable.time_out)
                            .setTitle("Time Out!")
                            .setMessage("Time is up!")
                            .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    if (AppUtil.isInternetConnected(NewExamActivity.this)) {

                                        postExamData();
                                    } else {
                                        new AlertDialog.Builder(NewExamActivity.this)
                                                .setIcon(R.drawable.alert_warning)
                                                .setTitle("No Internet!")
                                                .setMessage("Please Connect to Internet.")
                                                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        Internet_Activity internet = new Internet_Activity();
                                                        internet.checkInternet();
                                                    }
                                                })
                                                .show();
                                    }
                                }
                            })
                            .show();
                }


            }
        }
                .start();

    }

    @Override
    public void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
        hideDialog();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        hideDialog();
    }


    //handling back press button
    @Override
    public void onBackPressed() {

        // display alert dialog
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.alert_warning)
                .setTitle("Submit")
                .setMessage("Do you want to submit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (AppUtil.isInternetConnected(NewExamActivity.this)) {
                            if (data.isEmpty()) {

                                new AlertDialog.Builder(NewExamActivity.this)
                                        .setIcon(R.drawable.alert_warning)
                                        .setTitle("No Data!")
                                        .setMessage("Sorry! No Exam data Found.")
                                        .setPositiveButton("Back",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        startActivity(new Intent(
                                                                NewExamActivity.this,
                                                                NewDashboardActivity.class));

                                                        finish();
                                                    }
                                                })
                                        .show();

                            } else {
                                //stop timer
                                countDownTimer.cancel();
                                //send exam data to server
                                postExamData();

                            }
                        } else {
                            new AlertDialog.Builder(NewExamActivity.this)
                                    .setIcon(R.drawable.alert_warning)
                                    .setTitle("No Internet!")
                                    .setMessage("Please Connect to Internet.")
                                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Internet_Activity internet = new Internet_Activity();
                                            internet.checkInternet();
                                        }
                                    })
                                    .show();
                        }
                    }
                })
                .setNegativeButton("No", null).show();


    }

    ///method for showing clicked image in Dialog having zoom in and zoom out feature
    public void show_images(final Drawable images) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                NewExamActivity.this,
                R.style.MyDialogTheme);
        //setting alert dialog layout
        builder.setView(R.layout.alert_showimage);
        final AlertDialog dialog = builder.create();
        //setting alert dialog animation
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //button for hiding alert dialog
        ImageButton next = (ImageButton) dialog.findViewById(R.id.bt_ok);
        if (next != null) {
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        } else {
            dialog.dismiss();
        }


        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.gravity = Gravity.CENTER_VERTICAL;
        window.setAttributes(wlp);
    }

    //getting option value


    //method for sending exam data to server
    public void postExamData() {

        submited = false;
        PostQuestionRequestModel m = new PostQuestionRequestModel();

        //  //Log(TAG, SendAllData.toString());

        GetQuestion q = new GetQuestion();
        q.setResult_id(result_id);
        q.setExam_id(String.valueOf(exam_id));
        q.setStudent_id(user_id);


        ArrayList<ExamQuestion> ql = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {


            ExamQuestion eq = new ExamQuestion();
            eq.setQuestion_id(data.get(i).getQuestion_Id());
            ArrayList<ExamAnswers> answers = new ArrayList<>();
            ExamAnswers ea = null;
            if (results.get(i).getAnswerId() != null &&
                    !results.get(i).getAnswerId().equals("null") &&
                    results.get(i).getoption() > 0) {
                ea = new ExamAnswers();
                ea.setAnswer_id(results.get(i).getAnswerId());


                answers.add(ea);
            }
            eq.setAnswers(answers);


            ql.add(eq);
        }
        q.setQuestion(ql);
        m.setGet_question(q);

        showDialog(null, "please wait..");
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", m);
        NetworkService.startActionGetPostExamData(this, mServiceReceiver, bundle);


    }


    public String splitter(String data, String split) {
        String val = data.substring(0, 8);
        return val;
    }

    public static String time() {
        return tv_timer.getText().toString();
    }


    public void checkSnackbar() {


        // TODO Auto-generated method stub
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfos = connectivityManager.getActiveNetworkInfo();
            //checking for internet
            if (netInfos != null) {
                if (netInfos.isConnected()) {
                    isInternetPresent = true;

                    if (AppUtil.isInternetConnected(NewExamActivity.this)) {
                        postExamData();

                    }
                }

            } else {
                RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.rlRoot);
                Snackbar snackbar = Snackbar
                        .make(relativeLayout, "No internet connection", Snackbar.LENGTH_LONG)
                        .setAction("Retry", mOnClickListener);
                snackbar.setActionTextColor(Color.WHITE);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(Color.DKGRAY);
                TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
            }
        }
    }

    public void submit() {

    }


    private class DbTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings) {
            try {


                ProgressModule pm = new ProgressModule();
                pm.setModule_ID(Integer.parseInt(exam_id));
                pm.setModule_Type(strings[0]);
                pm.setSection_ID(RegPrefManager
                        .getInstance(NewExamActivity.this).getSectionId());

                pm.setClass_ID(
                        (int) RegPrefManager
                                .getInstance(NewExamActivity.this).
                                        getKeyClassId());
                pm.setChapter_ID(Integer.parseInt(strings[1]));
                pm.setSubject_ID(Integer.parseInt(strings[2]));


                ContentValues values = new ContentValues();


                values.put(
                        DbContract.ProgressTable.KEY_CLASS_ID
                        , pm.getClass_ID());
                values.put(
                        DbContract.ProgressTable.KEY_SECTION_ID,
                        pm.getSection_ID());
                values.put(DbContract.ProgressTable.KEY_CHAPTER_ID,
                        pm.getChapter_ID());
                values.put(DbContract.ProgressTable.KEY_SUBJECT_ID
                        , pm.getSubject_ID());
                values.put(DbContract.ProgressTable.KEY_MODULE_ID,
                        pm.getModule_ID());
                values.put(DbContract.ProgressTable.KEY_MODULE_TYPE,
                        pm.getModule_Type());

                values.put(DbContract.ProgressTable
                        .KEY_INSERT_DATE_TIME, pm.getInserted_On().getTime());
                values.put(DbContract.ProgressTable.KEY_REG_ID,
                        pm.getRegd_ID());

                Uri uri2 = getContentResolver().insert(
                        DbContract.ProgressTable.CONTENT_URI,
                        values);

                if (uri2 != null) {
                    //Log(TAG,"PG: "+uri2.getLastPathSegment()+"");
                }

                //Log(TAG, "Inserted rowId : " + rowId);


            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }


    //created by Abhishek for API call

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        switch (resultCode) {



            case ResponseCodes.EXCEPTION:

                Intent in = new Intent(this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:

                Intent intent = new Intent(this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:

                Intent inin = new Intent(this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:





                GetExamResponse getExamResponse = resultData.getParcelable(
                        IntentHelper.RESULT_DATA
                );

                if (getExamResponse != null && getExamResponse.
                        getSuccess() != null &&
                        getExamResponse.getSuccess().getExamData() != null) {

                    /*new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                        }
                    },2000);*/

                    linear_lyout.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.VISIBLE);
                    relativeTimeFinish.setVisibility(View.VISIBLE);
                    lnrNextPrvsRev.setVisibility(View.VISIBLE);
                    String data1 = getExamResponse.getSuccess().getStart_Time();
                    String[] datas = data1.split("T");
                    String time1 = datas[1];
                    start_date = datas[0];
                    time = getExamResponse.getSuccess().getStart_Time();
                    start_time = splitter(time1, "");
                    String data2 = getExamResponse.getSuccess().getEnd_Time();

                    String[] time_date2 = data2.split("T");
                    String time2 = time_date2[1];
                    end_date = time_date2[0];
                    end_time = splitter(time2, "");

                    result_id = getExamResponse.getSuccess().getResult_ID();
                    int Minutes = calculateTime();
                    if (activity != null) {
                        data.clear();
                    } else {
                        startTimer(Minutes);
                    }

                    for (int i = 0; i < getExamResponse.getSuccess().getExamData().size(); i++) {
                        ArrayList<String> Image = new ArrayList<>();
                        Alloption = new ArrayList<Model_option>();
                        String question_no = String.valueOf(getExamResponse.getSuccess().getExamData().get(i).getRowID());
                        String question_id = String.valueOf(getExamResponse.getSuccess().getExamData().get(i).getQuestion_id());
                        String question = String.valueOf(getExamResponse.getSuccess().getExamData().get(i).getQuestion());


                        if (String.valueOf(getExamResponse.getSuccess().getExamData().get(i).getQustion_Desc()) != null) {
                            question_desc = String.valueOf(getExamResponse.getSuccess().getExamData().get(i).getQustion_Desc());
                        } else {
                            question_desc = "";
                        }

                        if (getExamResponse.getSuccess().getExamData().get(i).getImage().size() != 0) {
                            for (int j = 0; j < getExamResponse.getSuccess().getExamData().get(i).getImage().size(); j++) {
                                if (!getExamResponse.getSuccess().getExamData().get(i).getImage().get(j).getQuestion_desc_Image().equalsIgnoreCase("")) {
                                    Image.add(AppConfig.SRVR_URL + getExamResponse.getSuccess().getExamData().get(i).getImage().get(j).getQuestion_desc_Image().replaceAll(" ", "%20"));
                                }

                            }
                        }

                        for (int k = 0; k < getExamResponse.getSuccess().getExamData().get(i).getOptions().size(); k++) {
                            String option_desc;
                            String answer_id = String.valueOf(getExamResponse.getSuccess().getExamData().get(i).getOptions().get(k).getAnswer_ID());
                            if (!getExamResponse.getSuccess().getExamData().get(i).getOptions().get(k).getOption_Image().equalsIgnoreCase("")) {
                                option_img = AppConfig.SRVR_URL + getExamResponse.getSuccess().getExamData().get(i).getOptions().get(k).getOption_Image().replaceAll(" ", "%20");

                            } else {
                                option_img = "";
                            }
                            if (k == 0) {
                                option_desc = "A. " + getExamResponse.getSuccess().getExamData().get(i).getOptions().get(k).getOption_Desc();
                            } else if (k == 1) {
                                option_desc = "B. " + getExamResponse.getSuccess().getExamData().get(i).getOptions().get(k).getOption_Desc();
                            } else if (k == 2) {
                                option_desc = "C. " + getExamResponse.getSuccess().getExamData().get(i).getOptions().get(k).getOption_Desc();
                            } else {
                                option_desc = "D. " + getExamResponse.getSuccess().getExamData().get(i).getOptions().get(k).getOption_Desc();
                            }

                            Alloption.add(new Model_option(answer_id, option_desc, option_img));
                        }

                        data.add(new Model_Exam(question_no + ".", question_id, question, question_desc, Image, Alloption));


                    }

                    prev.setVisibility(View.INVISIBLE);
                    prvs_text.setVisibility(View.INVISIBLE);
                    if (data.size() > 1) {
                        next.setVisibility(View.VISIBLE);
                        next_text.setVisibility(View.VISIBLE);
                    } else {
                        next.setVisibility(View.INVISIBLE);
                        next_text.setVisibility(View.INVISIBLE);
                    }


                    setRealmData();
                    setAnswer();
                    if (data.size() != 0) {
                        fetchdata(i);
                    }

                } else {
                    showToast("No Questions Found");
                    finish();
                }
                break;

            case ResponseCodes.STATUS:

                GetPostExamData getPostExamData = resultData.getParcelable(
                        IntentHelper.RESULT_DATA
                );

                if (getPostExamData == null || getPostExamData.getSuccess() == null) {
                    Toast.makeText(this,
                            "Something went wrong",
                            Toast.LENGTH_SHORT).show();
                } else {


                    String success_message = getPostExamData.getSuccess().getMessage();
                    String Result_id = String.valueOf(getPostExamData.getSuccess().getResult_id());
                    String moduleType = getIntent().getStringExtra("module_type");
                    String chapIdNew = getIntent().getStringExtra("chapter_id");
                    String subIdNew = getIntent().getStringExtra("subject_id");
                    String subNameNew = getIntent().getStringExtra("subName");
                    String flagNameNew = getIntent().getStringExtra("flagName");


                    Intent i = new Intent(this,
                            ScoreActivity.class);
                    i.putExtra("Result_ID", Result_id);
                    i.putExtra("exam_id", exam_id);
                    i.putExtra("chapter_id", chapIdNew);
                    i.putExtra("module_type", moduleType);
                    i.putExtra("subject_id", subIdNew);
                    i.putExtra("subName", subNameNew);
                    i.putExtra("flagName", flagNameNew);
                    i.putExtra("activity", "NewNewExamActivity");


                    startActivity(i);
                    overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
                    finish();
                    Toast.makeText(this, success_message, Toast.LENGTH_LONG).show();

                    break;
                }
        }

    }


    private class ExamTask extends AsyncTask<Void, Void, List<RealmExamDetails>> {


        @Override
        protected List<RealmExamDetails> doInBackground(Void... voids) {
            try {
                Cursor cursor = getContentResolver().query(DbContract.ExamTable.CONTENT_URI,
                        DbContract.ExamTable.MEMBER_PROJECTION,
                        null, null, null);


                if (cursor != null && cursor.getCount() > 0) {
                    cursor.moveToFirst();

                    do {

                        RealmExamDetails red = new RealmExamDetails();
                        red.setAnswerId(

                                cursor.getString(cursor.getColumnIndex(DbContract.ExamTable.answerId)));
                        red.setoption(
                                cursor.getInt(cursor.getColumnIndex(DbContract.ExamTable.option)));
                        red.setques_img1(
                                cursor.getInt(cursor.getColumnIndex(DbContract.ExamTable.ques_img1)));
                        red.setques_img2(
                                cursor.getInt(cursor.getColumnIndex(DbContract.ExamTable.ques_img2)));

                        red.setQuestion(
                                cursor.getString(cursor.getColumnIndex(DbContract.ExamTable.Question)));

                        red.setSkip(
                                cursor.getInt(cursor.getColumnIndex(DbContract.ExamTable.skip)));


                        results.add(red);

                    }
                    while (cursor.moveToNext());
                }
            } catch (Exception e) {
                //Log(TAG,e.getMessage()+"");
            }


            return null;
        }

        @Override
        protected void onPostExecute(List<RealmExamDetails> realmExamDetails) {
            super.onPostExecute(realmExamDetails);


        }
    }


}
