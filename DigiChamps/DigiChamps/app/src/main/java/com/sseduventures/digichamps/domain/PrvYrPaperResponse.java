package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/3/2018.
 */

public class PrvYrPaperResponse implements Parcelable{

    private PrvYrPaperSuccess Success;

    public PrvYrPaperSuccess getSuccess() {
        return Success;
    }

    public void setSuccess(PrvYrPaperSuccess success) {
        Success = success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.Success, flags);
    }

    public PrvYrPaperResponse() {
    }

    protected PrvYrPaperResponse(Parcel in) {
        this.Success = in.readParcelable(PrvYrPaperSuccess.class.getClassLoader());
    }

    public static final Creator<PrvYrPaperResponse> CREATOR = new Creator<PrvYrPaperResponse>() {
        @Override
        public PrvYrPaperResponse createFromParcel(Parcel source) {
            return new PrvYrPaperResponse(source);
        }

        @Override
        public PrvYrPaperResponse[] newArray(int size) {
            return new PrvYrPaperResponse[size];
        }
    };
}
