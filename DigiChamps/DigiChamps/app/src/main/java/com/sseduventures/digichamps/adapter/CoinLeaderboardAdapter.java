package com.sseduventures.digichamps.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.DataModel_TestList;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.Exam_desc;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.domain.CoinLeaderboardResponse;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.utils.CircleTransform;

import java.util.List;



public class CoinLeaderboardAdapter extends RecyclerView.Adapter<CoinLeaderboardAdapter.MyTestView> {

    private List<CoinLeaderboardResponse> testLists;
    RelativeLayout container, container1;
    Context context;
    public static int position;


    public static class MyTestView extends RecyclerView.ViewHolder {

        TextView name,userClass,coins;
        ImageView userImage;
        RelativeLayout container;

        public MyTestView(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.nameText);
            userClass = (TextView) view.findViewById(R.id.classText);
            coins = (TextView) view.findViewById(R.id.pointsText);
            userImage = (ImageView) view.findViewById(R.id.userImage);
            container = (RelativeLayout) view.findViewById(R.id.rlv_lay);

        }
    }

    public CoinLeaderboardAdapter(List<CoinLeaderboardResponse> testLists,
                            Context context) {
        this.testLists = testLists;
        this.context = context;

    }

    @Override
    public CoinLeaderboardAdapter.MyTestView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.coin_leaderboard_item, parent, false);

        CoinLeaderboardAdapter.MyTestView myTestView = new CoinLeaderboardAdapter.MyTestView(view);
        return myTestView;
    }

    @Override
    public void onBindViewHolder(final CoinLeaderboardAdapter.MyTestView holder,
                                 final int listPosition) {
        position = listPosition;
        TextView name = holder.name;
        TextView userClass = holder.userClass;
        TextView coins = holder.coins;
        ImageView userImage = holder.userImage;

        name.setText(testLists.get(position).getName());
        userClass.setText(testLists.get(position).getClassName());
        coins.setText(testLists.get(position).getCoins());
        String correctImage = AppConfig.SRVR_URL
                + "/Images/Profile/" +testLists.get(position).getImageUrl();
        Picasso.with(context).load(correctImage)
                .resize(60,60)
                .transform(new CircleTransform()).into(userImage);

        this.container1 = container;
        holder.container.setOnClickListener(onClickListener(position));
        Log.v(
                CoinLeaderboardAdapter.class
                        .getSimpleName(),testLists.get(listPosition).
                        getRegdId()+":"+RegPrefManager.getInstance(
                        context
                ).getRegId());
        if(testLists.get(listPosition).getRegdId()== RegPrefManager.getInstance(
                context
        ).getRegId())
        {


            holder.container.setBackgroundColor(context.getResources()
            .getColor(R.color.sky_blue_light));
        }
        else
            holder.container.setBackgroundColor(Color.TRANSPARENT);

    }
    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        };
    }
    @Override
    public int getItemCount() {
        return testLists.size();
    }

}
