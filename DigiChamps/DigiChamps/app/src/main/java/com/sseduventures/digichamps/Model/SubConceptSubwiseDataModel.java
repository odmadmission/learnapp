package com.sseduventures.digichamps.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tech_2 on 5/7/2018.
 */

public class SubConceptSubwiseDataModel {

    String subjectId,subjectName;
    List<SubConceptChapListDataModel> chapterList;




    ArrayList<AnalysisDetaillistModel> analysisDetaillistModels;

    public ArrayList<AnalysisDetaillistModel> getAnalysisDetaillistModels() {
        return analysisDetaillistModels;
    }

    public void setAnalysisDetaillistModels(ArrayList<AnalysisDetaillistModel> analysisDetaillistModels) {
        this.analysisDetaillistModels = analysisDetaillistModels;
    }

    public SubConceptSubwiseDataModel(String subjectId, String subjectName,
                                      List<SubConceptChapListDataModel> chapterList, ArrayList<AnalysisDetaillistModel> analysisDetaillistModels) {
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.chapterList = chapterList;
        this.analysisDetaillistModels=analysisDetaillistModels;
    }



    public SubConceptSubwiseDataModel(String subjectId, String subjectName, List<SubConceptChapListDataModel> chapterList) {
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.chapterList = chapterList;
    }


    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public List<SubConceptChapListDataModel> getChapterList() {
        return chapterList;
    }

    public void setChapterList(ArrayList<SubConceptChapListDataModel> chapterList) {
        this.chapterList = chapterList;
    }
}
