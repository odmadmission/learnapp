package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class LoginDto implements Parcelable
{

    private String mobile;
    private String password;
    private String deviceId;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mobile);
        dest.writeString(this.password);
        dest.writeString(this.deviceId);
    }

    public LoginDto() {
    }

    protected LoginDto(Parcel in) {
        this.mobile = in.readString();
        this.password = in.readString();
        this.deviceId = in.readString();
    }

    public static final Creator<LoginDto> CREATOR = new Creator<LoginDto>() {
        @Override
        public LoginDto createFromParcel(Parcel source) {
            return new LoginDto(source);
        }

        @Override
        public LoginDto[] newArray(int size) {
            return new LoginDto[size];
        }
    };
}
