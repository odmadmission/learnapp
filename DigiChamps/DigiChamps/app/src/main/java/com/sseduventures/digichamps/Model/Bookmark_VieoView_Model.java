package com.sseduventures.digichamps.Model;

/**
 * Created by khushbu on 1/22/2018.
 */

public class Bookmark_VieoView_Model {
    private String Module_Id,Module_Title,Description,Module_Image,
            Image_Key,Is_Avail,video_key,pdf_file,Is_Free,Validity, isExpire,
            Question_Pdf,No_Of_Question,Is_Free_Test,file_resource;


    public Bookmark_VieoView_Model(String Module_Id, String Module_Title,
                                 String Description, String Module_Image,
                                 String Image_Key,String video_key) {
        this.Module_Id = Module_Id;
        this.Module_Title = Module_Title;
        this.Description = Description;
        this.Module_Image = Module_Image;
        this.Image_Key = Image_Key;
        this.video_key = video_key;
        this.Is_Free = Is_Free;
        this.Validity = Validity;
        this.file_resource = file_resource;
        this.isExpire = isExpire;
    }

    public String getModule_Id() {
        return Module_Id;
    }

    public String getModule_Title() {
        return Module_Title;
    }

    public String getDescription() {
        return Description;
    }

    public String getModule_Image() {
        return Module_Image;
    }

    public String getImage_Key() {
        return Image_Key;
    }

    public String getIs_Avail() {
        return Is_Avail;
    }

    public String getpdf_file() {
        return pdf_file;
    }

    public String getIs_Free() {
        return Is_Free;
    }

    public String getValidity() {
        return Validity;
    }

    public String getQuestion_Pdf() {
        return Question_Pdf;
    }

    public String getNo_Of_Question() {
        return No_Of_Question;
    }

    public String getIs_Free_Test() {
        return Is_Free_Test;
    }

    public String getVideo_key() {
        return video_key;
    }

    public String getVideoExpiry() {
        return isExpire;
    }


}


