package com.sseduventures.digichamps.database;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.Nullable;

import java.util.ArrayList;


/**
 * Created by Raju Satheesh on 1/17/2017.
 */

public class DbProvider extends ContentProvider {
    private static  final  String DEBUG_TAG=DbProvider.class.getSimpleName();
    private static UriMatcher sUriMatcher;
    private DbOpenHelper dbOpenHelper;
    private SQLiteDatabase sqlDatabase;
    public static final int USER_QUERY = 1;

    public static final int COIN_QUERY = 2;

    public static final int NOTIFICATION_QUERY = 3;


    public static final int EXAM_QUERY = 4;

    public static final int VIDEOLOG_QUERY = 5;
    public static final int ACTIVITY_LOG = 6;

    public static final int PROGRESS_LOG = 7;

    public static final int FEED_QUERY = 8;
    public static final int BOOKMARKS_QUERY = 9;
    public static final int DICTIONARY_QUERY = 10;


    static {


        sUriMatcher = new UriMatcher(0);

        sUriMatcher.addURI(
                DbContract.Schema.CONTENT_AUTHORITY,
                DbContract.DictionaryTable.TABLE_NAME,
                DICTIONARY_QUERY);

        sUriMatcher.addURI(
                DbContract.Schema.CONTENT_AUTHORITY,
                DbContract.BookMarksTable.TABLE_NAME,
                BOOKMARKS_QUERY);

        sUriMatcher.addURI(
                DbContract.Schema.CONTENT_AUTHORITY,
                DbContract.UserTable.TABLE_NAME,
                USER_QUERY);

        sUriMatcher.addURI(
                DbContract.Schema.CONTENT_AUTHORITY,
                DbContract.FeedTable.TABLE_NAME,
                FEED_QUERY);

        sUriMatcher.addURI(
                DbContract.Schema.CONTENT_AUTHORITY,
                DbContract.ProgressTable.TABLE_NAME,
                PROGRESS_LOG);

        sUriMatcher.addURI(
                DbContract.Schema.CONTENT_AUTHORITY,
                DbContract.ActivityTimeTable.TABLE_NAME,
                ACTIVITY_LOG);

        sUriMatcher.addURI(
                DbContract.Schema.CONTENT_AUTHORITY,
                DbContract.VideoLogTable.TABLE_NAME,
                VIDEOLOG_QUERY);


        sUriMatcher.addURI(
                DbContract.Schema.CONTENT_AUTHORITY,
                DbContract.CoinTable.TABLE_NAME,
                COIN_QUERY);

        sUriMatcher.addURI(
                DbContract.Schema.CONTENT_AUTHORITY,

                DbContract.NotificationTable.TABLE_NAME,
                NOTIFICATION_QUERY);

        sUriMatcher.addURI(
                DbContract.Schema.CONTENT_AUTHORITY,

                DbContract.ExamTable.TABLE_NAME,
                EXAM_QUERY);


    }

    @Override
    public boolean onCreate() {
        Context context = getContext();

        dbOpenHelper=new DbOpenHelper(context);

        sqlDatabase = dbOpenHelper.getWritableDatabase();
        return (sqlDatabase == null)? false:true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection,
                        String selection, String[] selectionArgs,
                        String sortOrder) {
        Cursor returnCursor=null;
        switch (sUriMatcher.match(uri))
        {

            case BOOKMARKS_QUERY:
                returnCursor = sqlDatabase.query(
                        DbContract.BookMarksTable.TABLE_NAME,
                        projection,
                        selection, selectionArgs, sortOrder, null, null);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;
            case DICTIONARY_QUERY:
                returnCursor = sqlDatabase.query(
                        DbContract.DictionaryTable.TABLE_NAME,
                        projection,
                        selection, selectionArgs, sortOrder, null, null);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;
            case USER_QUERY:
                returnCursor = sqlDatabase.query(
                        DbContract.UserTable.TABLE_NAME,
                        projection,
                        selection, selectionArgs, sortOrder, null, null);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;

            case FEED_QUERY:
                returnCursor = sqlDatabase.query(
                        DbContract.FeedTable.TABLE_NAME,
                        projection,
                        selection, selectionArgs, sortOrder, null, null);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;


            case PROGRESS_LOG:
                returnCursor = sqlDatabase.query(
                        DbContract.ProgressTable.TABLE_NAME,
                        projection,
                        selection, selectionArgs, sortOrder, null, null);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;


            case ACTIVITY_LOG:
                returnCursor = sqlDatabase.query(
                        DbContract.ActivityTimeTable.TABLE_NAME,
                        projection,
                        selection, selectionArgs, sortOrder, null, null);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;


            case COIN_QUERY:
                returnCursor = sqlDatabase.query(
                        DbContract.CoinTable.TABLE_NAME,
                        projection,
                        selection, selectionArgs, sortOrder, null, null);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;


            case NOTIFICATION_QUERY:
                returnCursor = sqlDatabase.query(
                        DbContract.NotificationTable.TABLE_NAME,

                        projection,
                        selection, selectionArgs, sortOrder, null, null);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;
            case EXAM_QUERY:
                returnCursor = sqlDatabase.query(
                        DbContract.ExamTable.TABLE_NAME,

                        projection,
                        selection, selectionArgs, sortOrder, null, null);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;


            case VIDEOLOG_QUERY:
                returnCursor = sqlDatabase.query(
                        DbContract.VideoLogTable.TABLE_NAME,

                        projection,
                        selection, selectionArgs, sortOrder, null, null);
                returnCursor.setNotificationUri(getContext().getContentResolver(), uri);
                return returnCursor;



        }


        return null;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        long rowID;
        Uri returnUri=null;
        switch (sUriMatcher.match(uri))
        {

            case BOOKMARKS_QUERY:
                rowID = sqlDatabase.insert(DbContract.BookMarksTable
                        .TABLE_NAME, "", contentValues);
                if (rowID > 0)
                {
                    returnUri = ContentUris.withAppendedId(uri, rowID);
                    getContext().getContentResolver().
                            notifyChange(returnUri, null);

                }
                break;
            case DICTIONARY_QUERY:
                rowID = sqlDatabase.insert(DbContract.DictionaryTable
                        .TABLE_NAME, "", contentValues);
                if (rowID > 0)
                {
                    returnUri = ContentUris.withAppendedId(uri, rowID);
                    getContext().getContentResolver().
                            notifyChange(returnUri, null);

                }
                break;


            case USER_QUERY:
                rowID = sqlDatabase.insert(DbContract.UserTable.TABLE_NAME, "", contentValues);
                if (rowID > 0)
                {
                    returnUri = ContentUris.withAppendedId(uri, rowID);
                    getContext().getContentResolver().notifyChange(returnUri, null);

                }
                break;

            case FEED_QUERY:
                rowID = sqlDatabase.insert(DbContract.
                        FeedTable.TABLE_NAME, "", contentValues);
                if (rowID > 0)
                {
                    returnUri = ContentUris.withAppendedId(uri, rowID);
                    getContext().getContentResolver().notifyChange(returnUri, null);

                }
                break;

            case PROGRESS_LOG:
                rowID = sqlDatabase.insert(DbContract.
                        ProgressTable.TABLE_NAME, "", contentValues);
                if (rowID > 0)
                {
                    returnUri = ContentUris.withAppendedId(uri, rowID);
                    getContext().getContentResolver().notifyChange(returnUri, null);

                }
                break;

            case ACTIVITY_LOG:
                rowID = sqlDatabase.insert(DbContract.
                        ActivityTimeTable.TABLE_NAME, "", contentValues);
                if (rowID > 0)
                {
                    returnUri = ContentUris.withAppendedId(uri, rowID);
                    getContext().getContentResolver().notifyChange(returnUri, null);

                }
                break;


            case COIN_QUERY:
                rowID = sqlDatabase.
                        insert(DbContract.CoinTable.TABLE_NAME, "", contentValues);
                if (rowID > 0)
                {
                    returnUri = ContentUris.withAppendedId(uri, rowID);
                    getContext().getContentResolver().notifyChange(returnUri, null);

                }
                break;
            case NOTIFICATION_QUERY:
                rowID = sqlDatabase.
                        insert(DbContract.NotificationTable.TABLE_NAME,
                                "", contentValues);
                if (rowID > 0)
                {
                    returnUri = ContentUris.withAppendedId(uri, rowID);
                    getContext().getContentResolver().notifyChange(returnUri, null);

                }
                break;

            case EXAM_QUERY:
                rowID = sqlDatabase.
                        insert(DbContract.ExamTable.TABLE_NAME, "", contentValues);
                if (rowID > 0)
                {
                    returnUri = ContentUris.withAppendedId(uri, rowID);
                    getContext().getContentResolver().notifyChange(returnUri, null);

                }
                break;

            case VIDEOLOG_QUERY:
                rowID = sqlDatabase.
                        insert(DbContract.VideoLogTable
                                .TABLE_NAME, "", contentValues);
                if (rowID > 0)
                {
                    returnUri = ContentUris.withAppendedId(uri, rowID);
                    getContext().getContentResolver().notifyChange(returnUri, null);

                }
                break;


        }
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {int count = 0;

        switch (sUriMatcher.match(uri)){


            case BOOKMARKS_QUERY:
                count = sqlDatabase.delete(DbContract.
                        BookMarksTable.
                        TABLE_NAME, selection, selectionArgs);
                break;

            case DICTIONARY_QUERY:
                count = sqlDatabase.delete(DbContract.
                        DictionaryTable.
                        TABLE_NAME, selection, selectionArgs);
                break;

            case USER_QUERY:
                count = sqlDatabase.delete(DbContract.UserTable.
                        TABLE_NAME, selection, selectionArgs);
                break;


            case PROGRESS_LOG:
                count = sqlDatabase.
                        delete(DbContract.ProgressTable.TABLE_NAME,
                                selection, selectionArgs);
                break;

            case FEED_QUERY:
                count = sqlDatabase.
                        delete(DbContract.FeedTable.TABLE_NAME,
                                selection, selectionArgs);
                break;


            case ACTIVITY_LOG:
                count = sqlDatabase.delete(DbContract
                        .ActivityTimeTable.TABLE_NAME, selection, selectionArgs);
                break;
            case COIN_QUERY:
                count = sqlDatabase.delete(DbContract.CoinTable.TABLE_NAME, selection, selectionArgs);
                break;

            case NOTIFICATION_QUERY:
                count = sqlDatabase.delete(DbContract.NotificationTable
                        .TABLE_NAME, selection, selectionArgs);

                break;



            case EXAM_QUERY:
                count = sqlDatabase.delete(DbContract.ExamTable

                        .TABLE_NAME, selection, selectionArgs);



                break;
            case VIDEOLOG_QUERY:
                count = sqlDatabase.delete(DbContract.VideoLogTable

                        .TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri,null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        int count = 0;

        switch (sUriMatcher.match(uri)){

            case BOOKMARKS_QUERY:
                count = sqlDatabase.update(DbContract.
                                BookMarksTable
                                .TABLE_NAME,
                        contentValues,selection, selectionArgs);
                break;

            case DICTIONARY_QUERY:
                count = sqlDatabase.update(DbContract.
                                DictionaryTable
                                .TABLE_NAME,
                        contentValues,selection, selectionArgs);
                break;

            case USER_QUERY:
                count = sqlDatabase.update(DbContract.UserTable.TABLE_NAME,
                        contentValues,selection, selectionArgs);
                break;

            case PROGRESS_LOG:
                count = sqlDatabase.update(DbContract.
                                ProgressTable.TABLE_NAME,
                        contentValues,selection, selectionArgs);
                break;


            case FEED_QUERY:
                count = sqlDatabase.update(DbContract.
                                FeedTable.TABLE_NAME,
                        contentValues,selection, selectionArgs);
                break;

            case ACTIVITY_LOG:
                count = sqlDatabase.update(DbContract.ActivityTimeTable.TABLE_NAME,
                        contentValues,selection, selectionArgs);
                break;

            case COIN_QUERY:
                count = sqlDatabase.update(DbContract.CoinTable.TABLE_NAME,
                        contentValues,selection, selectionArgs);
                break;
            case NOTIFICATION_QUERY:
                count = sqlDatabase.update(DbContract.NotificationTable
                                .TABLE_NAME,
                        contentValues,selection, selectionArgs);
                break;

            case EXAM_QUERY:
                count = sqlDatabase.update(DbContract.ExamTable.TABLE_NAME,
                        contentValues,selection, selectionArgs);
                break;
            case VIDEOLOG_QUERY:
                count = sqlDatabase.update(DbContract.VideoLogTable.TABLE_NAME,
                        contentValues,selection, selectionArgs);
                break;


            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri,null);
        return count;
    }
    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {

        return super.bulkInsert(uri, values);
    }
    private final ThreadLocal<Boolean> mIsInBatchMode = new ThreadLocal<Boolean>();
    @Override
    public ContentProviderResult[] applyBatch(
            ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        mIsInBatchMode.set(true);
        // the next line works because SQLiteDatabase
        // uses a thread local SQLiteSession object for
        // all manipulations
        sqlDatabase.beginTransaction();
        try {
            final ContentProviderResult[] retResult = super.applyBatch(operations);
            sqlDatabase.setTransactionSuccessful();
            getContext().getContentResolver().
                    notifyChange(DbContract.BookMarksTable
                            .CONTENT_URI, null);
            getContext().getContentResolver().notifyChange(DbContract.
                    FeedTable.CONTENT_URI, null);
            getContext().getContentResolver().
                    notifyChange(DbContract.BookMarksTable
                            .CONTENT_URI, null);

            getContext().getContentResolver().
                    notifyChange(DbContract.DictionaryTable
                            .CONTENT_URI, null);

            return retResult;
        }
        finally {
            mIsInBatchMode.remove();
            sqlDatabase.endTransaction();
        }
    }

    private boolean isInBatchMode() {
        return mIsInBatchMode.get() != null
                && mIsInBatchMode.get();
    }
    public static class DbOpenHelper extends SQLiteOpenHelper{


        public DbOpenHelper(Context context){

            super(context, DbContract.Schema.DATABASE_NAME, null,
                    DbContract.Schema.DATABASE_VERSION);

        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
          createTables(sqLiteDatabase);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

              dropTables(sqLiteDatabase);
            createTables(sqLiteDatabase);

        }

        private void createTables(SQLiteDatabase sqLiteDatabase)
        {
            sqLiteDatabase.execSQL(DbContract.UserTable.SQL_CREATE_STATEMENT);
            sqLiteDatabase.execSQL(DbContract.CoinTable.SQL_CREATE_STATEMENT);
            sqLiteDatabase.execSQL(DbContract.NotificationTable.SQL_CREATE_STATEMENT);
            sqLiteDatabase.execSQL(DbContract.ExamTable.SQL_CREATE_STATEMENT);
            sqLiteDatabase.execSQL(DbContract.VideoLogTable.SQL_CREATE_STATEMENT);

            sqLiteDatabase.execSQL(DbContract.ActivityTimeTable.SQL_CREATE_STATEMENT);
            sqLiteDatabase.execSQL(DbContract.ProgressTable.CREATE_PROGRESS_TABLE);
            sqLiteDatabase.execSQL(DbContract.FeedTable.SQL_CREATE_STATEMENT);
            sqLiteDatabase.execSQL(DbContract.DictionaryTable.SQL_CREATE_STATEMENT);

        }
        private void dropTables(SQLiteDatabase sqLiteDatabase)
        {

            sqLiteDatabase.execSQL(DbContract.UserTable.SQL_DELETE_STATEMENT);
            sqLiteDatabase.execSQL(DbContract.CoinTable.SQL_DELETE_STATEMENT);
            sqLiteDatabase.execSQL(DbContract.NotificationTable.SQL_DELETE_STATEMENT);
            sqLiteDatabase.execSQL(DbContract.ExamTable.SQL_DELETE_STATEMENT);

            sqLiteDatabase.execSQL(DbContract.VideoLogTable.SQL_DELETE_STATEMENT);

            sqLiteDatabase.execSQL(DbContract.ActivityTimeTable.SQL_DELETE_STATEMENT);
            sqLiteDatabase.execSQL(DbContract.ProgressTable.SQL_DELETE_STATEMENT);
            sqLiteDatabase.execSQL(DbContract.FeedTable.SQL_DELETE_STATEMENT);
            sqLiteDatabase.execSQL(DbContract.DictionaryTable.SQL_DELETE_STATEMENT);

        }

    }
}
