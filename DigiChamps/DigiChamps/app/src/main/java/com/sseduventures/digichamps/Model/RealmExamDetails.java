package com.sseduventures.digichamps.Model;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ntspl22 on 5/5/2017.
 */

public class RealmExamDetails implements Parcelable
{

    private String answerId;
    private String Question;

    private int ques_img1;
    private int ques_img2;
    private int skip;

    private int option;
    private int reviewed;

    public int getReviewed() {
        return reviewed;
    }

    public void setReviewed(int reviewed) {
        this.reviewed = reviewed;
    }

    public String getQuestion() {
        return Question;
    }
    public int getoption() {
        return option;
    }
    public int getSkip() {
        return skip;
    }

    public int getques_img1() {
        return ques_img1;
    }
    public int getques_img2() {
        return ques_img2;
    }
    public String getAnswerId() {
        return answerId;
    }



    public void setQuestion(String Question) {
        this.Question = Question;
    }
    public void setoption(int option) {
        this.option = option;
    }
    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }
    public void setSkip(int skip) {
        this.skip = skip;
    }

//setAnswerId
    public void setques_img1(int ques_img1) {
        this.ques_img1 = ques_img1;
    }
    public void setques_img2(int ques_img2) {
        this.ques_img2 = ques_img2;
    }

    public RealmExamDetails() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.answerId);
        dest.writeString(this.Question);
        dest.writeInt(this.ques_img1);
        dest.writeInt(this.ques_img2);
        dest.writeInt(this.skip);
        dest.writeInt(this.option);
        dest.writeInt(this.reviewed);
    }

    protected RealmExamDetails(Parcel in) {
        this.answerId = in.readString();
        this.Question = in.readString();
        this.ques_img1 = in.readInt();
        this.ques_img2 = in.readInt();
        this.skip = in.readInt();
        this.option = in.readInt();
        this.reviewed = in.readInt();
    }

    public static final Creator<RealmExamDetails> CREATOR = new Creator<RealmExamDetails>() {
        @Override
        public RealmExamDetails createFromParcel(Parcel source) {
            return new RealmExamDetails(source);
        }

        @Override
        public RealmExamDetails[] newArray(int size) {
            return new RealmExamDetails[size];
        }
    };
}
