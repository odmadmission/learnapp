package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.adapter.CardviewDiscussionDetail;
import com.sseduventures.digichamps.domain.DiscussionList;
import com.sseduventures.digichamps.domain.GetDiscusiionDetailsResponse;
import com.sseduventures.digichamps.domain.GetDiscussionDetailsList;
import com.sseduventures.digichamps.domain.PostDiscussionDetailsResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.IntentHelper;

import com.sseduventures.digichamps.webservice.ResponseCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.sseduventures.digichamps.utils.Constants.ADD_DISCUSSION_DETAIL;
import static com.sseduventures.digichamps.utils.Constants.DISCUSSION_DETAIL;


/**
 * Created by user on 23-01-2018.
 */

public class DiscussionDetailActivity extends FormActivity implements
        ServiceReceiver.Receiver {

    Context mContext;
    CardviewDiscussionDetail mCardviewDiscussionDetail;
    RecyclerView recyclerview;
    String discussionID = "", examName = "";
    ArrayList<GetDiscussionDetailsList> mList;
    RelativeLayout btnSend;
    EditText enter_message;

    RelativeLayout progressbarrel;
    int pageIndex = 0;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean loading = true;
    String schoolid, regId, roleid;
    int classid;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private GetDiscusiionDetailsResponse success;
    private PostDiscussionDetailsResponse mSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discussion_detail);

        setModuleName(LogEventUtil.EVENT_discussion_details_activity);
        logEvent(LogEventUtil.KEY_discussion_details_activity,LogEventUtil.EVENT_discussion_details_activity);
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        regId = String.valueOf(RegPrefManager.getInstance(this).getRegId());
        classid = (int) RegPrefManager.getInstance(this).getclassid();
        schoolid = RegPrefManager.getInstance(this).getSchoolId();
        roleid = String.valueOf(RegPrefManager.getInstance(this).getroleid());
        mContext = this;
        mList = new ArrayList<>();
        progressbarrel = findViewById(R.id.progressbarrel);
        btnSend = findViewById(R.id.btnSend);
        enter_message = findViewById(R.id.enter_message);
        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView tv_at = findViewById(R.id.tv_at);
        FontManage.setFontHeaderBold(mContext, tv_at);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (enter_message.getText().toString().trim().equals("")) {
                    Toast.makeText(mContext, "Please enter message", Toast.LENGTH_SHORT).show();
                } else {
                    addForumNew(enter_message.getText().toString().trim());
                }
            }
        });

        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager.setReverseLayout(true);

        recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {

                        }
                    }
                }
            }
        });


        if (getIntent() != null) {
            discussionID = getIntent().getStringExtra("discussionID");
            getForumListNew(pageIndex);
        }
    }


    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public String getNextColor(String mycolor) {
        if (mycolor.equals("#00ADCA")) {
            return "#F44337";
        } else if (mycolor.equals("#F44337")) {
            return "#4051B5";
        } else if (mycolor.equals("#4051B5")) {
            return "#009933";
        } else if (mycolor.equals("#009933")) {
            return "#00ADCA";
        }
        return "#00ADCA";
    }


    private void addForumNew(String title) {
        if (AppUtil.isInternetConnected(this)) {

            int Disccussion = 0;
            if (discussionID != null) {

                Disccussion = Integer.parseInt(discussionID);
            }

            Bundle bun = new Bundle();
            bun.putInt("discussionid", Disccussion);
            bun.putString("titleA", title);
            NetworkService.startActionPostAddForumList(DiscussionDetailActivity.this
                    , mServiceReceiver, bun);

        } else {
            Intent in = new Intent(DiscussionDetailActivity.this,
                    Internet_Activity.class);
            startActivity(in);
        }
    }

    private void getForumListNew(int StartIndex) {
        if (AppUtil.isInternetConnected(this)) {

            int Disccussion = 0;
            if (discussionID != null) {

                Disccussion = Integer.parseInt(discussionID);
            }

            Bundle bun = new Bundle();
            bun.putInt("discussionid", Disccussion);
            bun.putInt("index", StartIndex);
            NetworkService.startActionPostForumDetails(DiscussionDetailActivity.this
                    , mServiceReceiver, bun);

        } else {
            Intent in = new Intent(DiscussionDetailActivity.this,
                    Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        //hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(DiscussionDetailActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(DiscussionDetailActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(DiscussionDetailActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if (success.getResultCount() == 0) {
                    progressbarrel.setVisibility(View.GONE);

                } else {

                    if (success.getDiscussionDetail() != null &&
                            success.getDiscussionDetail().size() > 0) {
                        mList.addAll(success.getDiscussionDetail());
                        if (mList != null && mList.size() > 0) {
                            int colorcode = 0;
                            for (int i = 0; i < mList.size(); i++) {
                                if (colorcode == 0) {
                                    colorcode = colorcode + 1;
                                    mList.get(i).setColorCode(Color.parseColor("#00ADCA"));
                                    mList.get(i).setHashColorCode("#00ADCA");
                                } else if (colorcode == 1) {
                                    colorcode = colorcode + 1;
                                    mList.get(i).setColorCode(Color.parseColor("#F44337"));
                                    mList.get(i).setHashColorCode("#F44337");
                                } else if (colorcode == 2) {
                                    colorcode = colorcode + 1;
                                    mList.get(i).setColorCode(Color.parseColor("#4051B5"));
                                    mList.get(i).setHashColorCode("#4051B5");
                                } else if (colorcode == 3) {
                                    colorcode = 0;
                                    mList.get(i).setColorCode(Color.parseColor("#009933"));
                                    mList.get(i).setHashColorCode("#009933");
                                }
                            }
                            recyclerview.setVisibility(View.VISIBLE);
                            if (pageIndex == 0) {
                                mCardviewDiscussionDetail = new CardviewDiscussionDetail(mList, mContext);
                                recyclerview.setAdapter(mCardviewDiscussionDetail);
                            } else {
                                recyclerview.post(new Runnable() {
                                    public void run() {
                                        mCardviewDiscussionDetail.setData(mList);
                                    }
                                });
                            }

                            pageIndex = Constants.PAGE_SIZE + 1;
                            loading = true;
                            progressbarrel.setVisibility(View.GONE);
                        } else {
                            recyclerview.setVisibility(View.GONE);
                        }
                    } else {
                        progressbarrel.setVisibility(View.GONE);
                        if (pageIndex == 0) {
                            recyclerview.setVisibility(View.GONE);
                            if (mList != null) {
                                mList.clear();
                                mCardviewDiscussionDetail = new CardviewDiscussionDetail(mList, mContext);
                                recyclerview.setAdapter(mCardviewDiscussionDetail);
                            }
                        }
                    }
                }
                break;

            case ResponseCodes.ADDFORUMSUCCESS:

                mSuccess = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if (mSuccess.getMessage().equalsIgnoreCase("Successfull")) {


                    GetDiscussionDetailsList mModel = new GetDiscussionDetailsList();


                    mModel.setDiscussionID(mSuccess.getMessageDetail().getDiscussionDetailID());
                    mModel.setCreatedDate(mSuccess.getMessageDetail().getCreatedDate());
                    mModel.setCreatedName(mSuccess.getMessageDetail().getCreatedName());
                    mModel.setCreatedBy(mSuccess.getMessageDetail().getCreatedBy());
                    mModel.setPhotoURL(mSuccess.getMessageDetail().getPhotoURL());
                    mModel.setDetailText(mSuccess.getMessageDetail().getDetailText());

                    if (mList != null && mList.size() > 0) {
                        String nextColorCode = getNextColor(mList.get(0).getHashColorCode());
                        mModel.setColorCode(Color.parseColor(nextColorCode));
                        mModel.setHashColorCode(nextColorCode);
                    } else {
                        mModel.setColorCode(Color.parseColor("#00ADCA"));
                        mModel.setHashColorCode("#00ADCA");
                    }


                    enter_message.setText("");
                    enter_message.setHint("Enter Message..");
                    hideKeyboard();

                    if (mCardviewDiscussionDetail != null) {
                        mCardviewDiscussionDetail.setData(mModel);
                    } else {
                        mList = new ArrayList<>();
                        mList.add(mModel);
                        mCardviewDiscussionDetail = new CardviewDiscussionDetail(mList, mContext);
                        recyclerview.setAdapter(mCardviewDiscussionDetail);
                    }
                }

                break;

        }
    }
}