package com.sseduventures.digichamps.activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


import com.sseduventures.digichamps.Model.AutocompleteModel;
import com.sseduventures.digichamps.Model.Basic_info_model;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.Board_listview_DialogAdapter;
import com.sseduventures.digichamps.adapter.Class_listview_DialogAdapter;
import com.sseduventures.digichamps.adapter.CustomAutoCompleteAdapter;
import com.sseduventures.digichamps.adapter.Email_List_DialogAdapter;
import com.sseduventures.digichamps.adapter.School_listview_DialogAdapter;
import com.sseduventures.digichamps.domain.BoardClass;
import com.sseduventures.digichamps.domain.Registration;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.utils.Utils;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.model.ProfileModelClass;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


@SuppressWarnings("All")
public class BasicInfomationActivity extends FormActivity implements
        View.OnClickListener, ServiceReceiver.Receiver {

    private TextInputLayout name, email, password, confirm_password, school_name;
    private TextInputEditText name_ed, password_ed, comfirm_password_ed, school_name_ed;
    private EditText email_ed;
    private TextView class_select_tv, class_tv, select_tv_board, board_tv, section_tv, section_select_tv, select_tv;
    private Button submit_but;
    private ArrayList<String> basic_list;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;

    private ArrayList<BoardClass> success;

    private ArrayList<ProfileModelClass> mmSuccess;

    private Registration mSuccess;

    private String mobileNum;
    private String nameClass = "null", nameSection = "null";
    private Board_listview_DialogAdapter boardAdapter;
    ArrayList<Basic_info_model> board_list, class_list, class_detail_list, school_list;
    ArrayList<String> class_details, email_list;
    private Class_listview_DialogAdapter classAdapter;
    private Toolbar toolbar;


    private School_listview_DialogAdapter schoolAdapter;

    private View view_line;
    AutoCompleteTextView autocomplete;
    private Email_List_DialogAdapter emailAdapter;
    private Boolean flag = true;
    public static final int RequestPermissionCode = 1;
    private String[] arr = null;
    ArrayList<ProfileModelClass> newList;
    List<String> newList_section;
    ArrayList<String> school_List;
    ArrayList<Basic_info_model> section_List;
    ArrayList<Basic_info_model> section_List_new;
    ArrayList<Basic_info_model> section_List_new_;
    ArrayList<AutocompleteModel> school_details;
    private String selecteditem, school_id;
    private String board = "null", class_name = "null", section_name = "null";
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    int counter = 0;
    CustomAutoCompleteAdapter adapter;
    boolean flag_other = false;

    Registration mRegistration;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basic_info);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        // EnableRuntimePermission();
        logEvent(LogEventUtil.KEY_BASIC_INFO,
                LogEventUtil.EVENT_BASIC_INFO);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(BasicInfomationActivity.this, OnBoardActivity.class);
                startActivity(in);
            }
        });

        mRegistration=getIntent().getParcelableExtra("data");
        init();

        board_list = new ArrayList<>();
        class_list = new ArrayList<>();
        class_detail_list = new ArrayList<>();
        school_list = new ArrayList<>();
        class_details = new ArrayList<>();
        school_details = new ArrayList<>();
        // newList=new ArrayList<>();
        email_list = new ArrayList<>();
        school_List = new ArrayList<>();
        section_List = new ArrayList<>();
        section_List_new = new ArrayList<>();




        if (AppUtil.isInternetConnected(this)) {
            showDialog(null, "Please wait");
            getBoardClass();

        } else {

           /* Intent in = new Intent(BasicInfomationActivity.this,Internet_Activity.class);
            startActivity(in);*/
            AskOptionDialogNew("Please Check Your Internet Connection").show();
        }


    }


    private void init() {



        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);


        Intent in = getIntent();
        if (in != null) {
            mobileNum = in.getStringExtra("mobileNum");
        }

        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);



        name_ed = findViewById(R.id.name_ed);
        email_ed = findViewById(R.id.email_ed);
        password_ed = findViewById(R.id.password_ed);
        school_name_ed = findViewById(R.id.school_name_ed);


        class_tv = findViewById(R.id.class_tv);
        class_select_tv = findViewById(R.id.class_select_tv);
        board_tv = findViewById(R.id.board_tv);
        select_tv_board = findViewById(R.id.select_tv);

        section_select_tv = findViewById(R.id.section_select_tv);
        select_tv = findViewById(R.id.select_tv);
        section_tv = findViewById(R.id.section_tv);

        school_name = findViewById(R.id.school_name);
        view_line = findViewById(R.id.view_line);


        autocomplete = (AutoCompleteTextView)
                findViewById(R.id.autoCompleteTextView1);





        autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {


                Object item = parent.getItemAtPosition(position);
                if (item instanceof String) {
                    selecteditem = (String) item;
                    flag_other = false;

                    schoolId(selecteditem);
                    school_name.setVisibility(View.GONE);
                    view_line.setVisibility(View.GONE);

                }


            }


        });


        submit_but = findViewById(R.id.submit_but);
        basic_list = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            String value = "X";
            basic_list.add(value);
        }

        class_select_tv.setOnClickListener(this);
        submit_but.setOnClickListener(this);
        select_tv_board.setOnClickListener(this);
        section_select_tv.setOnClickListener(this);
        select_tv.setOnClickListener(this);


        name_ed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!name_ed.getText().toString().isEmpty())
                    name.setError(null);
                name.setErrorEnabled(false);

            }
        });

        email_ed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!email_ed.getText().toString().isEmpty())
                    email.setError(null);
                email.setErrorEnabled(false);

            }
        });
        password_ed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!password_ed.getText().toString().isEmpty())
                    password.setError(null);
                password.setErrorEnabled(false);


            }
        });



        if(mRegistration!=null)
        {
            name_ed.setText(mRegistration.getCustomerName());
            email_ed.setText(mRegistration.getEmail());
            password_ed.setText(mRegistration.getPassword());
            mobileNum=mRegistration.getMobile();


        }

    }


    private Boolean validation() {

        String emailStr, nameStr, passwordStr, confirm_passwordStr;
        emailStr = email_ed.getText().toString().trim();
        nameStr = name_ed.getText().toString().trim();
        passwordStr = password_ed.getText().toString().trim();
        confirm_passwordStr = comfirm_password_ed.getText().toString().trim();
        Boolean valid_email = Utils.isValidMail(emailStr);

        if (nameStr.isEmpty()) {
            AskOptionDialogNew("Please Enter Your Name");

            return false;
        }


        if (emailStr.isEmpty()) {
            AskOptionDialogNew("Please Enter Your Email");

            return false;
        }

        if (!valid_email) {
            AskOptionDialogNew("Please Enter Valid Email");

            return false;
        }

        if (passwordStr.isEmpty()) {
            AskOptionDialogNew("Please Enter Password");

            return false;
        }


        return true;
    }


    private Boolean validationNew() {
        Boolean valid_email = Utils.isValidMail(email_ed.getText().toString().trim());
        if (name_ed.getText().toString().trim().isEmpty()) {

            name.setError("Please Enter Your Name");

            return false;
        }


        if (email_ed.getText().toString().trim().isEmpty()) {

            email.setError("Please Enter Your Email");

            return false;
        }

        if (!valid_email) {

            email.setError("Please Enter Valid Email");

            return false;
        }

        if (password_ed.getText().toString().trim().isEmpty()) {

            password.setError("Please Enter Password");

            return false;
        }

        if (autocomplete.getText().toString().trim().isEmpty()) {
            showToast("Please Enter School Name");

            return false;
        }
        if (board.equalsIgnoreCase("null")) {
            showToast("Please Your Board Name");

            return false;
        }
        if (nameClass.equalsIgnoreCase("null")) {
            showToast("Please Your Class Name");

            return false;
        }
        if (nameSection.equalsIgnoreCase("null")) {
            showToast("Please Your Section Name");

            return false;
        }




        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.class_select_tv:
                if (classNames != null) {

                    dialogclass_dialog();
                } else {
                    AskOptionDialogNew("Please select your board");

                }


                break;

            case R.id.submit_but:

                if (validationNew()) {


                    if (AppUtil.isInternetConnected(this)) {
                        submitRegistrationDetails();
                    } else {
                        Intent in = new Intent(BasicInfomationActivity.this, Internet_Activity.class);
                        startActivity(in);
                    }


                }

                break;
            case R.id.select_tv:


                boarddialog();
                break;
            case R.id.section_select_tv:
                sectiondialog();
                break;


        }
    }

    private void class_dialog() {
        final Dialog dialog = new Dialog(this);

    }


    private void prepareClassName(String className) {
        class_select_tv.setText("Change");
        class_tv.setText(className);
    }

    private void prepareSectionName(String sectionName) {
        section_select_tv.setText("Change");
        section_tv.setText(sectionName);
    }

    private void prepareBoardName(String boardname) {
        select_tv.setText("Change");
        board_tv.setText(boardname);
    }

    private void showClassDialog() {

        AlertDialog.Builder myDialog =
                new AlertDialog.Builder(this);
        myDialog.setTitle("Select Class");
        myDialog.setItems(classNames, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {


                //board = boardNames[which];
                nameClass = classNames[which];
                prepareClassName(nameClass);

            }


        });

        myDialog.show();

    }

    private void checkBoard() {


    }

    private void showBoardDialog() {
        AlertDialog.Builder myDialog =
                new AlertDialog.Builder(this);
        myDialog.setTitle("Select Board");
        myDialog.setItems(boardNames, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {


                board = boardNames[which];
                createClassList();
                prepareBoardName(board);

            }


        });


        myDialog.show();

    }

    private void submitRegistrationDetails() {

        if (flag_other == true) {
            selecteditem = school_name_ed.getText().toString();
            school_id = null;
        } else {

        }

        String userName = name_ed.getText().toString().trim();
        String userEmail = email_ed.getText().toString().trim();
        String userPass = password_ed.getText().toString().trim();

        Bundle bun = new Bundle();
        bun.putString("customerName", userName);
        bun.putString("mobile", mobileNum);
        bun.putString("email", userEmail);
        bun.putString("password", userPass);
        bun.putLong("boardId", getBoardId(board));
        bun.putLong("classId", getClassId(board, nameClass));
        bun.putString("schoolId", school_id);
        bun.putString("schoolName", selecteditem);
        bun.putString("sectionName", nameSection);
        if(mRegistration!=null)
        bun.putLong("regId", mRegistration.getRegdId());
        else
            bun.putLong("regId",0);


        if (AppUtil.isInternetConnected(this)) {

            showDialog(null, "creating profile....");
            NetworkService.startActionSignupDetails(this, mServiceReceiver, bun);

        } else {

            Intent in = new Intent(BasicInfomationActivity.this,
                    Internet_Activity.class);
            startActivity(in);

        }
    }


    private String getSchoolId(String schoolName) {
        String schoolIdNew = "";

        if (mmSuccess != null) {
            for (int i = 0; i < mmSuccess.size(); i++) {

                if (mmSuccess.get(i).getSchoolInfoList().get(i).getSchoolName().equalsIgnoreCase(schoolName)) {

                    schoolIdNew = mmSuccess.get(i).getSchoolInfoList().get(i).getSchoolId();


                }
            }
        }

        return schoolIdNew;
    }

    public void schoolId(String selecteditem){
        for(int i=0;i<school_details.size();i++){
            String name=school_details.get(i).getSchoolName();
            if(selecteditem.equalsIgnoreCase(name)){
                String id = school_details.get(i).getSchoolId();
                school_id=id;
            }
        }
    }

    private long getBoardId(String b) {
        long r = 0;

        if (mmSuccess != null) {
            for (int i = 0; i < mmSuccess.size(); i++) {

               /* ArrayList<String> boardl=new ArrayList<String >();
                boardl.add(String.valueOf(mmSuccess.get(i).getBoardList().get(i)));
                ////Log.vvd("Tag","tag");*/

                int size = mmSuccess.get(i).getBoardList().size();
                for (int j = 0; j < size; j++) {
                    String name = mmSuccess.get(i).getBoardList().get(j).getBoardName();
                    if (name.equalsIgnoreCase(b)) {
                        r = mmSuccess.get(i).getBoardList().get(j).getBoardId();
                    }
                }
            }
        }

        return r;
    }

    private long getClassId(String b, String c) {
        long r = 0;

        if (mmSuccess != null) {

            for (int i = 0; i < mmSuccess.size(); i++) {

                int size = mmSuccess.get(i).getBoardList().size();
                for (int k = 0; k < size; k++) {
                    String name = mmSuccess.get(i).getBoardList().get(k).getBoardName();
                    if (name.equalsIgnoreCase(b)) {
                        int class_size = mmSuccess.get(i).getBoardList().get(k).getClassList().size();
                        for (int j = 0; j < class_size; j++) {
                            String class_name = mmSuccess.get(i).getBoardList().get(k).getClassList().get(j)
                                    .getClassName();
                            if (class_name.equalsIgnoreCase(c)) {
                                r = mmSuccess.get(i).getBoardList().get(k).getClassList().get(j).getClassId();
                            }
                        }
                    }
                }


            }
        }

        return r;
    }

    private void getBoardClass() {

        if (AppUtil.isInternetConnected(this)) {
            Bundle bundle = new Bundle();

            NetworkService.startActionGetBoardClass(this, mServiceReceiver, bundle);

        } else {

            Intent in = new Intent(BasicInfomationActivity.this, Internet_Activity.class);
            startActivity(in);

        }
    }

    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }


    private String[] boardNames = null;

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(BasicInfomationActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(BasicInfomationActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(BasicInfomationActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:


                mmSuccess = resultData.getParcelableArrayList(IntentHelper.RESULT_DATA);
                //showToast(success.size()+"");
                for (int i = 0; i < mmSuccess.size(); i++) {
                    if (mmSuccess != null && mmSuccess.get(i).getBoardList().size() > 0) {
                        createBoardNames();
                    }

                }

                for (int i = 0; i < mmSuccess.size(); i++) {
                    if (mmSuccess != null && mmSuccess.get(i).getSchoolInfoList().size() > 0) {


                        createSchoolNames();
                    }
                }

                for (int i = 0; i < mmSuccess.size(); i++) {

                    if (mmSuccess != null && mmSuccess.get(i).getSectionList().size() > 0) {

                        createSectionNames();
                    }
                }


                break;
            case ResponseCodes.SUCCESSFULLYSIGNEDUP:
                mSuccess = resultData.getParcelable(IntentHelper.RESULT_DATA);

                String referUrl = mSuccess.getAddress();


                Intent intentin = new Intent(BasicInfomationActivity.this,
                        NewDashboardActivity.class);
                startActivity(intentin);
                finish();
                break;
            case ResponseCodes.USER_ALREADY_DONE:
                AskOptionDialog("You have already registered");
                break;

        }
    }

    private void createBoardNames() {

        for (int j = 0; j < mmSuccess.size(); j++) {
            boardNames = new String[mmSuccess.get(j).getBoardList().size()];
            for (int i = 0; i < mmSuccess.get(j).getBoardList().size(); i++) {
                boardNames[i] = mmSuccess.get(j).getBoardList().get(i).getBoardName();
                Basic_info_model basic_info_model = new Basic_info_model();
                basic_info_model.setName(mmSuccess.get(j).getBoardList().get(i).getBoardName());
                board_list.add(basic_info_model);
            }
        }


    }

    private void createSchoolNames() {

        for (int j = 0; j < mmSuccess.size(); j++) {
            arr = new String[mmSuccess.get(j).getSchoolInfoList().size()];
            for (int i = 0; i < mmSuccess.get(j).getSchoolInfoList().size(); i++) {
                arr[i] = mmSuccess.get(j).getSchoolInfoList().get(i).getSchoolName().toString();
                school_List.add(mmSuccess.get(j).getSchoolInfoList().get(i).getSchoolName().toString());

                AutocompleteModel autocompleteModel = new AutocompleteModel();
                autocompleteModel.setSchoolName(mmSuccess.get(j).getSchoolInfoList().get(i).getSchoolName().toString());
                autocompleteModel.setSchoolId(mmSuccess.get(j).getSchoolInfoList().get(i).getSchoolId().toString());
                school_details.add(autocompleteModel);
            }
        }
        if (school_List.size() > 0) {
            school_List.add(school_List.size(), "Other");
        }


        schoolList();

    }

    private String[] sectionNames = null;

    private void createSectionNames() {

        for (int j = 0; j < mmSuccess.size(); j++) {
            sectionNames = new String[mmSuccess.get(j).getSectionList().size()];
            for (int i = 0; i < mmSuccess.get(j).getSectionList().size(); i++) {
                sectionNames[i] = mmSuccess.get(j).getSectionList().get(i).toString();

                Basic_info_model basic_info_model = new Basic_info_model();
                basic_info_model.setName(mmSuccess.get(j).getSectionList().get(i).toString());
                section_List_new.add(basic_info_model);
            }
        }
    }

    private String[] classNames = null;

    private void createClassList() {

        for (int k = 0; k < mmSuccess.size(); k++) {
            for (int i = 0; i < mmSuccess.get(k).getBoardList().size(); i++) {

                if (board != null && board.equalsIgnoreCase(mmSuccess.get(k).getBoardList().get(i).getBoardName())) {
                    classNames = new String[mmSuccess.get(k).getBoardList().get(i).getClassList().size()];
                    for (int j = 0; j < mmSuccess.get(k).getBoardList().get(i).getClassList().size(); j++) {
                        classNames[j] = mmSuccess.get(k).getBoardList().get(i).getClassList().get(j).getClassName();
                        Basic_info_model basic_info_model = new Basic_info_model();
                        basic_info_model.setName(mmSuccess.get(k).getBoardList().get(i).getClassList().get(j).getClassName());
                        class_list.add(basic_info_model);

                        class_details.add(mmSuccess.get(k).getBoardList().get(i).getClassList().get(j).getClassName());
                    }
                }
            }

        }
    }


    private android.app.AlertDialog AskOptionDialog(String msg) {
        android.app.AlertDialog myQuittingDialogBox = new android.app.AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    private android.app.AlertDialog AskOptionDialogNew(String msg) {
        android.app.AlertDialog myQuittingDialogBox = new android.app.AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })

                .create();
        return myQuittingDialogBox;
    }

    public void schoolList() {
        String footerText = "<font color=\"red\">School name not in the list? </font>";



        adapter = new CustomAutoCompleteAdapter(
                this, android.R.layout.simple_list_item_1, arr, footerText);
        adapter.setOnFooterClickListener(new CustomAutoCompleteAdapter.OnFooterClickListener() {
            @Override
            public void onFooterClicked() {
                flag_other = true;

                // your custom item has been clicked, make some toast
                autocomplete.setText("School name not in the list?");
                view_line.setVisibility(View.VISIBLE);
                school_name.setVisibility(View.VISIBLE);
                InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(autocomplete.getWindowToken(), 0);
                autocomplete.setSelection(autocomplete.length());
                autocomplete.dismissDropDown();
            }
        });
        autocomplete.setThreshold(2);
        autocomplete.setAdapter(adapter);

    }

    public void boarddialog() {
        final Dialog dialog = new Dialog(BasicInfomationActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.setContentView(R.layout.board_editprofile_dialog);



        ListView listView = (ListView) dialog.findViewById(R.id.dialoglist);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);
        TextView select_class_tv = (TextView) dialog.findViewById(R.id.select_class_tv);
        select_class_tv.setVisibility(View.GONE);
        select_tv.setVisibility(View.VISIBLE);
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        boardAdapter = new Board_listview_DialogAdapter(board_list, getApplicationContext());

        listView.setAdapter(boardAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item text from ListView

                board_list.get(position).setFlag(true);
                for (int i = 0; i < board_list.size(); i++) {
                    if (position != i)
                        board_list.get(i).setFlag(false);
                }
                boardAdapter.notifyDataSetChanged();
            }
        });
        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Basic_info_model> list = boardAdapter.basic_info_modelArrayList();
                for (int i = 0; i < list.size(); i++) {
                    boolean flag = list.get(i).isFlag();
                    if (flag == true) {
                        Basic_info_model basic_info_model = list.get(i);
                        board = basic_info_model.getName();
                        createClassList();
                        //counter=counter+1;
                        prepareBoardName(board);

                    }
                }


                dialog.dismiss();

            }
        });

        dialog.show();
    }

    public void dialogclass_dialog() {
        final Dialog dialog = new Dialog(BasicInfomationActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.board_editprofile_dialog);
        // Set dialog title

        Set<String> primesWithoutDuplicates = new LinkedHashSet<String>(class_details);
        class_details.clear();
        class_details.addAll(primesWithoutDuplicates);
        class_list.clear();
        for (int i = 0; i < class_details.size(); i++) {


            Basic_info_model basic_info_model = new Basic_info_model();
            String name = class_details.get(i).toString();
            if (class_name.equals("null")) {

                basic_info_model.setName(name);
            } else {
                if (class_name.equals(name)) {
                    basic_info_model.setFlag(true);
                    basic_info_model.setName(name);
                } else {
                    basic_info_model.setFlag(false);
                    basic_info_model.setName(name);
                }
            }



            class_list.add(basic_info_model);

        }
        ListView listView = (ListView) dialog.findViewById(R.id.dialoglist);


        TextView select_class_tv = (TextView) dialog.findViewById(R.id.select_class_tv);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        select_class_tv.setVisibility(View.VISIBLE);
        select_tv.setVisibility(View.GONE);

        classAdapter = new Class_listview_DialogAdapter(class_list, getApplicationContext());

        class_detail_list.addAll(class_list);

        listView.setAdapter(classAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                class_list.get(position).setFlag(true);
                class_name = class_list.get(position).getName();
                for (int i = 0; i < class_list.size(); i++) {
                    if (position != i)
                        class_list.get(i).setFlag(false);
                }
                classAdapter.notifyDataSetChanged();

            }
        });

        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Basic_info_model> list = classAdapter.basic_info_modelArrayList();
                for (int i = 0; i < list.size(); i++) {
                    boolean flag = list.get(i).isFlag();
                    if (flag == true) {
                        Basic_info_model basic_info_model = list.get(i);
                        nameClass = basic_info_model.getName();
                        //counter=counter+1;
                        prepareClassName(nameClass);

                    }
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void schooldialog() {
        final Dialog dialog = new Dialog(BasicInfomationActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.board_editprofile_dialog);

        // Set dialog title

        school_list.clear();

        ListView listView = (ListView) dialog.findViewById(R.id.dialoglist);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);
        TextView select_class_tv = (TextView) dialog.findViewById(R.id.select_class_tv);
        TextView select_school_tv = (TextView) dialog.findViewById(R.id.select_school_tv);
        select_class_tv.setVisibility(View.GONE);
        select_tv.setVisibility(View.GONE);
        select_school_tv.setVisibility(View.VISIBLE);
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        Basic_info_model basic_info_model = new Basic_info_model();
        basic_info_model.setName("SSVM");
        school_list.add(basic_info_model);
        schoolAdapter = new School_listview_DialogAdapter(school_list, getApplicationContext());

        listView.setAdapter(schoolAdapter);


        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void sectiondialog() {
        section_List_new.clear();
        createSectionNames();
        final Dialog dialog = new Dialog(BasicInfomationActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.board_editprofile_dialog);

        // Set dialog title
        // dialog.setTitle("Custom Dialog");

        // set values for custom dialog components - text, image and button


        ListView listView = (ListView) dialog.findViewById(R.id.dialoglist);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);
        TextView select_class_tv = (TextView) dialog.findViewById(R.id.select_class_tv);
        TextView select_school_tv = (TextView) dialog.findViewById(R.id.select_school_tv);
        TextView select_section_tv = (TextView) dialog.findViewById(R.id.select_section_tv);
        select_class_tv.setVisibility(View.GONE);
        select_tv.setVisibility(View.GONE);
        select_school_tv.setVisibility(View.GONE);
        select_section_tv.setVisibility(View.VISIBLE);
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        //section_List.clear();

        for (int i = 0; i < section_List_new.size(); i++) {

        }
        schoolAdapter = new School_listview_DialogAdapter(section_List_new, getApplicationContext());

        listView.setAdapter(schoolAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                section_List_new.get(position).setFlag(true);
                section_name = section_List_new.get(position).getName();
                for (int i = 0; i < section_List_new.size(); i++) {
                    if (position != i)
                        section_List_new.get(i).setFlag(false);
                }
                schoolAdapter.notifyDataSetChanged();

            }
        });

        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Basic_info_model> list = schoolAdapter.basic_info_modelArrayList();
                for (int i = 0; i < list.size(); i++) {
                    boolean flag = list.get(i).isFlag();
                    if (flag == true) {
                        Basic_info_model basic_info_model = list.get(i);
                        nameSection = basic_info_model.getName();
                        //counter=counter+1;
                        prepareSectionName(nameSection);

                    }
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void showEmailList() {
        flag = false;
        final Dialog dialog = new Dialog(BasicInfomationActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.email_listlayout);

        // Set dialog title
        // dialog.setTitle("Custom Dialog");

        // set values for custom dialog components - text, image and button

        //  school_list.clear();
        ListView listView = (ListView) dialog.findViewById(R.id.dialoglist);
        get_all_mail();
        Set<String> primesWithoutDuplicates = new LinkedHashSet<String>(email_list);
        email_list.clear();
        email_list.addAll(primesWithoutDuplicates);
        emailAdapter = new Email_List_DialogAdapter(email_list, getApplicationContext());

        listView.setAdapter(emailAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item text from ListView


                String selectedItem = email_list.get(position);
                email_ed.setText(selectedItem);
                email_ed.setSelection(email_ed.length());

                dialog.dismiss();
                flag = true;

                // Display the selected item text on TextView
                //  prepareBoardName(selectedItem);
                //   dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void get_all_mail() {
        email_list.clear();
        String possibleEmail = "";


        try {
            Account[] accounts = AccountManager.get(this).getAccountsByType("com.google");
            for (Account account : accounts) {
                possibleEmail = account.name;
                // possibleEmail += "\n";
                email_list.add(possibleEmail);
            }
        } catch (Exception e) {
            ////Log.vvi("Exception", "Exception:" + e);
        }
    }


    private void EnableRuntimePermission() {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(BasicInfomationActivity.this, Manifest.permission.GET_ACCOUNTS);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.GET_ACCOUNTS},
                    REQUEST_CODE_ASK_PERMISSIONS);
            return;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
