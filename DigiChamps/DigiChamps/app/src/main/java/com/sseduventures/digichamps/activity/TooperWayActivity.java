package com.sseduventures.digichamps.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.adapter.CardviewTopperWay;
import com.sseduventures.digichamps.domain.TopperResponse;
import com.sseduventures.digichamps.domain.ToppersList;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import java.util.ArrayList;

public class TooperWayActivity extends FormActivity implements
ServiceReceiver.Receiver{

    RecyclerView recyclerview;
    Context mContext;
    CardviewTopperWay mCardviewTopperWay;
    ArrayList<ToppersList> mList;
    TextView tv_at;
    String schoolid;
    int classid;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private TopperResponse success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_material);
        setModuleName(LogEventUtil.EVENT_topper_way);
        logEvent(LogEventUtil.KEY_topper_way,LogEventUtil.EVENT_topper_way);

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        schoolid = RegPrefManager.getInstance(this).getSchoolId();
        classid = (int)RegPrefManager.getInstance(this).getclassid();


        mContext = this;
        tv_at =  findViewById(R.id.tv_at);
        tv_at.setText("TOPPER'S\nWAY");
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        ImageView imhLogo = (ImageView) findViewById(R.id.imhLogo);
        imhLogo.setBackground(mContext.getResources().getDrawable(R.drawable.tooper_answer));
        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        FontManage.setFontHeaderBold(mContext,tv_at);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        mList = new ArrayList<>();
        mCardviewTopperWay = new CardviewTopperWay(mList,mContext);

        getTopperListNew();
    }






    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mContext)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    private void getTopperListNew() {
        if(AppUtil.isInternetConnected(this)){


            NetworkService.startActionPostToppersDetails(TooperWayActivity.this
                    ,mServiceReceiver);

        }else{
            Intent in = new Intent(TooperWayActivity.this,
                    Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        //hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(TooperWayActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(TooperWayActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(TooperWayActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:

                success = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if(success.isStatus()){
                    if(success.getList()!=null
                            && success.getList().size()>0){
                        mList.addAll(success.getList());
                        mCardviewTopperWay = new CardviewTopperWay(mList,mContext);
                        recyclerview.setAdapter(mCardviewTopperWay);
                }else{
                        AskOptionDialog("No Topper Found").show();
                    }
                }

                break;

        }
    }

}
