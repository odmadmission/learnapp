package com.sseduventures.digichamps.Model;

/**
 * Created by Lincoln on 18/05/16.
 */
public class Questions_Review {
    private String name;
    private int numOfSongs;
    private int thumbnail;

    public Questions_Review() {
    }

    public Questions_Review(String name) {
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
