package com.sseduventures.digichamps.activities;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



import static com.sseduventures.digichamps.activities.ModelObject_Onboard.values;

public class CustomPagerAdapter_Onboard extends PagerAdapter {

    private Context mContext;


    public CustomPagerAdapter_Onboard(Context context) {
        mContext = context;

    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        ModelObject_Onboard modelObject = values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View layout =  inflater.inflate(modelObject.getLayoutResId(), collection, false);

        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        ModelObject_Onboard customPagerEnum = values()[position];
        return mContext.getString(customPagerEnum.getTitleResId());
    }



}