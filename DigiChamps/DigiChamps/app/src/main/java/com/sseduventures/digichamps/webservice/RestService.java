package com.sseduventures.digichamps.webservice;


import com.sseduventures.digichamps.Model.MentorStudentChat;
import com.sseduventures.digichamps.Model.VideoUsageTime;
import com.sseduventures.digichamps.domain.AddDiscussionRequest;
import com.sseduventures.digichamps.domain.AddToCartRequest;
import com.sseduventures.digichamps.domain.AssignedTeacherRequest;
import com.sseduventures.digichamps.domain.BoardRequest;
import com.sseduventures.digichamps.domain.BookMarkRequest;
import com.sseduventures.digichamps.domain.ChangePasswordRequest;
import com.sseduventures.digichamps.domain.ChapterDetailsRequestModel;
import com.sseduventures.digichamps.domain.CoinEarn;
import com.sseduventures.digichamps.domain.DailyTTRequest;
import com.sseduventures.digichamps.domain.DashboardRequest;
import com.sseduventures.digichamps.domain.DictonaryPostRequest;
import com.sseduventures.digichamps.domain.DiscussionRequest;
import com.sseduventures.digichamps.domain.EditProfileRequestNew;
import com.sseduventures.digichamps.domain.ExamScheduleDetailsRequest;
import com.sseduventures.digichamps.domain.ExamScheduleRequest;
import com.sseduventures.digichamps.domain.GetClassRequest;
import com.sseduventures.digichamps.domain.GetDiscussionDetailsRequest;
import com.sseduventures.digichamps.domain.HomeworkRequest;
import com.sseduventures.digichamps.domain.LoginDto;
import com.sseduventures.digichamps.domain.NewLearnRequest;
import com.sseduventures.digichamps.domain.NoticesRequest;
import com.sseduventures.digichamps.domain.PostDisscussionDetailsRequest;
import com.sseduventures.digichamps.domain.PsychometricPostRequest;
import com.sseduventures.digichamps.domain.Registration;
import com.sseduventures.digichamps.domain.RemoveCartRequest;
import com.sseduventures.digichamps.domain.ReportModel;
import com.sseduventures.digichamps.domain.ScLeaderboardRequest;
import com.sseduventures.digichamps.domain.SetBookmarkRequest;
import com.sseduventures.digichamps.domain.StudentActivityTime;
import com.sseduventures.digichamps.domain.StudyMaterialsRequest;
import com.sseduventures.digichamps.domain.ToppersRequest;
import com.sseduventures.digichamps.domain.UploadImageRequest;
import com.sseduventures.digichamps.domain.UserContact;
import com.sseduventures.digichamps.scholarship.ScholarShipScratch;
import com.sseduventures.digichamps.scholarship.ScratchSuccess;
import com.sseduventures.digichamps.webservice.model.FeedBackFormRequestModel;
import com.sseduventures.digichamps.webservice.model.GetCrtExamRequestParaModel;
import com.sseduventures.digichamps.webservice.model.PostQuestionRequestModel;
import com.sseduventures.digichamps.webservice.model.RecentlyWatchedRequestBody;
import com.sseduventures.digichamps.webservice.model.SubmitDoubtRequestParaModel;


import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by HP on 8/19/2017.
 */

public interface RestService {
    String PATH = "/digi-mentor-rest-api";


    String BASE_URL = "https://thedigichamps.com/api/";

    String BASE_URL_SCHOOL = "https://thedigichamps.com/api/SchoolAPI/";

    String TEST_BASE = "http://52.172.183.131:8080/digi-mentor-rest-api/";


    @POST(PATH + "/chats/create")
    @Headers("Content-Type: application/json")
    Call<MentorStudentChat> createChat(@Body MentorStudentChat
                                               mentorStudentChat);


    @GET(PATH + "/chats/getList")
    @Headers("Content-Type: application/json")
    Call<ArrayList<MentorStudentChat>> getList(@Query("regId") long regId);


    @GET(BASE_URL + "scholarshipscratchs/GetScratch")
    @Headers("Content-Type: application/json")
    Call<ScratchSuccess> GetScratch(@Query("RegId") long regId);


    @GET(BASE_URL + "scholarshipscratchs/SaveScratch")
    @Headers("Content-Type: application/json")
    Call<ScholarShipScratch> SaveScratch(@Query("RegId") long RegId,
                                         @Query("ScholarId") int ScholarId);


    @GET(BASE_URL + "Cart/Order_Confirmation/{user_id}/{order_id}/{track_id}/{discount}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> OrderConfirmation(@Path("user_id") String RegId,
                                         @Path("order_id") String order_id,
                                         @Path("track_id") String track_id,
                                         @Path("discount") int discount

    );

    @GET(BASE_URL + "Academic/GetPackage")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> GetPackages(@Header("Authorization") String token,
                                   @Query("id") int RegId);


    @POST(BASE_URL + "Cart/addtocart")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> addToCart(@Header("Authorization") String token,
                                 @Body AddToCartRequest body);


    @GET(BASE_URL + "Cart/cartdetail")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getCartData(@Header("Authorization") String token,
                                   @Query("id") int RegId);

    @POST(BASE_URL + "Cart/RemoveCart")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> removePackageFromCart(@Header("Authorization") String token,
                                             @Body RemoveCartRequest body);

    @GET(BASE_URL + "Cart/checkout")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getCheckoutData(@Header("Authorization") String token,
                                       @Query("id") String RegId);


    @GET(BASE_URL + "Exam/GetPsychometricExam")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getPsychometricExam(
            @Query("examId") int examId);

    @POST(BASE_URL + "Exam/PostPsychometricExamResult")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postPsychometricExam(
            @Body PsychometricPostRequest request);


    @GET(BASE_URL + "Learnnew/LearnChapterdetails/{RegId}/{ChapId}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> LearnChapterdetails(@Header("Authorization") String token,
                                           @Path("RegId") int RegId, @Path("ChapId") int chapId);

    @POST(BASE_URL + "Learn/SetBookMark")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> SetUnsetBookMark(@Body BookMarkRequest request);

    @GET(BASE_URL + "Exam/GetMentorshipExam")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> GetMentorshipExam(
            @Query("examId") int examId, @Query("regId") int regId);


    @POST(BASE_URL + "Exam/PostMentorshipExamResult")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> PostMentorshipExamResult(
            @Body PsychometricPostRequest request);


    @POST(BASE_URL + "Dictionary/GetDictionary")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> PostDictonaryResult(
            @Body DictonaryPostRequest request);


    @POST(BASE_URL + "UserContact/UploadContacts")
    @Headers("Content-Type: application/json")
    Call<List<UserContact>> UploadContacts(
            @Body List<UserContact> request);


    @GET(BASE_URL + "Orderdetails/myorders/{id}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> myorders(@Header("Authorization") String token,
                                @Path("id") int id);


    @GET(BASE_URL + "EditProfile/profile/{id}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> profileData(@Header("Authorization") String token,
                                   @Path("id") int id);


    @GET(BASE_URL + "FAQ/GetFAQ")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getFaqData(@Header("Authorization") String token);


    @GET(BASE_URL + "Feed/GetFeed")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getFeedData(@Header("Authorization") String token);


    @GET(BASE_URL + "CompetitiveExamQs/GetCompetitiveExamQs/{boardid}/{classid}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getOlympiadsData(@Path("boardid") int boardid,
                                        @Path("classid") int classid);


    @GET(BASE_URL + "PreviousYearPaper/GetPreviousYearPaper/{boardid}/{classid}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getPrvsYearPaperData(@Path("boardid") int boardid,
                                            @Path("classid") int classid);


    @GET(BASE_URL + "NCERTSolutions/GetNCERTSolutions/{boardid}/{classid}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getNcertSolutions(@Path("boardid") int boardid,
                                         @Path("classid") int classid);


    @GET(BASE_URL + "Exam/Test_Result/{regid}/{resultid}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getTestHighlightsData(@Path("regid") int regid,
                                             @Path("resultid") int resultid);


    @GET(BASE_URL + "Exam/LeaderBoards/{regid}/{resultid}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getLeaderBoardData(@Path("regid") int regid,
                                          @Path("resultid") int resultid);


    @GET(BASE_URL + "Login/logout/{sessionId}/{User_Id}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getLogoutStatus(@Path("sessionId") int sessionid,
                                       @Path("User_Id") int User_Id);

    @POST(TEST_BASE + "coins/save")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> saveVideoCoins(@Body List<CoinEarn> coins);

    @GET(BASE_URL + "Analitics/GetWeakSubConceptAnalytics")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getSubConceptAnalysis(@Query("regId") int regId,
                                             @Query("classId") int classId);

    @GET(BASE_URL + "Analitics/GetSubConceptAnalytics")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getAnalyticsSubject(@Query("regId") int regId,
                                           @Query("classId") int classId);

    @GET(BASE_URL + "Analitics/GetPersonalizedUserAnalytics")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getAnalyticsDetails(@Query("regId") int regId,
                                           @Query("classId") String classId);

    @GET(BASE_URL + "Exam/QuestionReview/{resultid}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getReviewSolutionData(@Path("resultid") int resultid);


    @GET(BASE_URL + "Doubt/Get_All_Doubts/{userid}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getDoubtListData(@Path("userid") int userid);


    @POST(BASE_URL + "EditProfile/Changepassword")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> Changepassword(@Header("Authorization") String token,
                                      @Body ChangePasswordRequest id);


    @POST(BASE_URL + "feedback/addfeedback")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getFeedbackForm(@Body FeedBackFormRequestModel feedBackFormRequestModel);


    @GET(BASE_URL + "Orderdetails/orderdetails/{user_id}/{id}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getorderDetails(
            @Header("Authorization") String token,
            @Path("user_id") String user_id,
            @Path("id") String id);


    @GET(BASE_URL + "Doubt/Get_Doubt_Details/{ticket_id}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody>   getDoubtDetails(
            @Header("Authorization") String token,
            @Path("ticket_id") String id);

    @GET(BASE_URL + "Exam/GetExamQuestions")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> GetQuestions(@Query("eid") String exam_id,
                                    @Query("regid") int user_id);


    @POST(BASE_URL + "Exam/Submit_Test")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postExamData(@Header("Authorization") String token,
                                    @Body PostQuestionRequestModel body);

    @POST(BASE_URL_SCHOOL + "ExamSchedules")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postExamSchedule(@Body ExamScheduleRequest body);

    @POST(BASE_URL_SCHOOL + "ExamScheduleByTypeId")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postExamScheduleDetails(@Body ExamScheduleDetailsRequest body);

    @POST(BASE_URL_SCHOOL + "TimeTableList")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postDailyTimeTable(@Body DailyTTRequest body);

    @GET(BASE_URL_SCHOOL + "getschool/{schoolid}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getInfoDetails(@Query("schoolid") String schoolid);


    @POST(BASE_URL_SCHOOL + "GetAssignTacherList")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postAssignedTeacher(@Body AssignedTeacherRequest body);

    @POST(BASE_URL_SCHOOL + "HomeWorkList")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postHomeworkDetails(@Body HomeworkRequest body);

    @POST(BASE_URL_SCHOOL + "StudyMaterialList")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postStudyMaterialsDetails(@Body StudyMaterialsRequest body);

    @POST(BASE_URL_SCHOOL + "NoticeList")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postNoticesDetails(@Body NoticesRequest body);

    @POST(BASE_URL_SCHOOL + "DiscussionForumList")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postDisscussionDetails(@Body DiscussionRequest body);

    @POST(BASE_URL_SCHOOL + "ToppersWay")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postToppersDetails(@Body ToppersRequest body);

    @POST(BASE_URL_SCHOOL + "BoardList")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postBoardDetails(@Body BoardRequest body);

    @POST(BASE_URL_SCHOOL + "ClassList")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postClassDetails(@Body GetClassRequest body);

    @POST(BASE_URL_SCHOOL + "LeadersBoardClassList")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postScLeaderboardDetails(@Body ScLeaderboardRequest body);

    @POST(BASE_URL_SCHOOL + "AddDiscussion")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postAddDiscussionDetails(@Body AddDiscussionRequest body);

    @POST(BASE_URL_SCHOOL + "DiscussionForumDetail")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getDetailsForumDiscussion(@Body GetDiscussionDetailsRequest body);


    @POST(BASE_URL_SCHOOL + "AddDiscussionDetail")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postAddForumDetails(@Body PostDisscussionDetailsRequest body);


    @GET(TEST_BASE + "signup/otp/{mobile}/{otp}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getSignUpOtp(@Path("mobile") int mobile,
                                    @Path("otp") int otp);

    @GET(TEST_BASE + "encash/address")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getPostencash(@Query("regId") long regId,
                                     @Query("coins") int coins,
                                     @Query("mobile") String mobile,
                                     @Query("address") String address);

    @POST(TEST_BASE + "users/login")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> login(@Body LoginDto loginDto);


    @GET(TEST_BASE + "profile/resetPassword")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getResetPassword(@Query("mobile") String mobile,
                                        @Query("otp") String otp);

    @GET(TEST_BASE + "security/changePassword")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getChangePassword(@Query("mobile") String mobile,
                                         @Query("password") String password,
                                         @Query("deviceId") String deviceId);

    @GET(TEST_BASE + "signup/otp")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getSignUpMobile(@Query("mobile") String mobile,
                                       @Query("otp") String otp);

    @GET(TEST_BASE + "boards/GetActiveBoards")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getBoardClass();


    @POST(TEST_BASE + "users/register")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postSignupDetails(@Body Registration signupRequest);


    @POST(BASE_URL + "Learn/GetDashboardLearn")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postLearnDetails(@Body NewLearnRequest newLearnRequest);


    @POST(BASE_URL + "Learn/GetDashboardNew")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getNewDashBoard(
            @Body DashboardRequest dashboardRequest);


    @GET(BASE_URL + "doubt/GetAllSubjects")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getSubjectName(@Query("regid") long regid);


    @GET(BASE_URL + "Doubt/Get_All_DoubtsNew")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getDoubtListNewData(@Query("id") long id,
                                           @Query("subid") int subid);


    @GET(BASE_URL + "Mentor/GetStudentTask/{regId}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getPMDetails(@Path("regId") long regId);


    @GET(BASE_URL + "Cart/Order_Confirmation/{regId}/{order_id}/{tracking_id}/{discount}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getOrderConf(@Path("regId") long regId,
                                    @Path("order_id") String order_id,
                                    @Path("tracking_id") String tracking_id,
                                    @Path("discount") int discount);


    @GET(BASE_URL + "Learn/GetBookmarkListVideo/{regId}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getBookMarkVideo(@Path("regId") long regId);

    @POST(BASE_URL + "Learn/SetBookmark")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> setUnsetBookmark(@Body SetBookmarkRequest setBookmarkRequest);

    @GET(BASE_URL + "Learn/GetBookmarkListStudynote/{regId}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getBookmarkStudyNotes(@Path("regId") long regId);

    @GET(BASE_URL + "Learn/GetBookmarkListQusetion/{regId}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getBookmarkQuestion(@Path("regId") long regId);


    //new
    @GET(TEST_BASE + "users/logout")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> logout(@Query("mobile") String mobile);


    @GET(TEST_BASE + "coins/getLeaderboard")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getCoinLeaderboard(@Query("id") long id);

    @POST(BASE_URL + "EditProfile/PostUserImage")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postEPImage(@Body UploadImageRequest uploadImageRequest);

    @POST(BASE_URL + "EditProfile/postprofileNew")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> postEditProfile(@Body EditProfileRequestNew editProfileRequest);


    @GET(BASE_URL + "Learn/Addschoolzone")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> AssignSchoolZone(@Query("regId") Long regId,
                                        @Query("schoolId") String schoolId,
                                        @Query("schoolCode") String scolCode
    );

    @POST(BASE_URL + "Learn/SetvideoLogTwo")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> RecentlyWatched(@Body RecentlyWatchedRequestBody recentlyWatchedRequestBody);


    //@POST(BASE_URL + "Doubt/AskDoubt")
//    @Headers("Content-Type: application/json")
//    Call<ResponseBody> postsubmitdoubt(@Header("Authorization") String token,
//                                       @Body SubmitDoubtRequestParaModel submitDoubtRequestParaModel);

    @POST(BASE_URL + "Doubt/AskDoubt")
    @Multipart
    Call<ResponseBody> postsubmitdoubt(@Part("Regd_ID") RequestBody regId,
                                       @Part("Class_id") RequestBody Class_id,
                                       @Part("Subject_ID") RequestBody Subject_ID,
                                       @Part("Board_id") RequestBody Board_id,
                                       @Part("Chapter_ID") RequestBody Chapter_ID,
                                       @Part("Question_Detail") RequestBody Question_Detail,
                                       @Part MultipartBody.Part Image

    );

    @POST(BASE_URL + "Doubt/Answer_Rply")
    @Multipart
    Call<ResponseBody> postReplyDoubt(@Part("Ticket_id") RequestBody Ticket_id,
                                      @Part("Answer_id") RequestBody Answer_id,
                                      @Part("msgbody") RequestBody msgbody,
                                      @Part("Reg_id") RequestBody Reg_id,
                                      @Part MultipartBody.Part Image

    );


    @POST(BASE_URL + "EditProfile/PostUserImage")
    @Multipart
    Call<ResponseBody> postImageEditProfile(@Part("Regd_ID") RequestBody regId,
                                            @Part MultipartBody.Part Image);


    @POST(TEST_BASE + "activity/create")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> activityTimeCreate(@Body List<StudentActivityTime> list);

    @GET(TEST_BASE + "resources/GetVersionLatest")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getLiveAppStatus(@Query("id") long regId, @Query("loginId") long loginId);


    @POST(BASE_URL + "Report/SaveReportModules")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> saveReports(@Body ReportModel list);

    @POST(TEST_BASE + "videos/usage/create")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> saveVideoUsage(@Body List<VideoUsageTime> list);

    @GET(BASE_URL + "Exam/GetCbtExam")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getcbtexam(@Query("id") long id, @Query("eid") String eid);


    @GET(BASE_URL + "Exam/GetPrtExam")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getPrtexam(@Query("id") long id, @Query("eid") String eid);

    @GET(BASE_URL + "Learn/LearnChapterdetails/{regId}/{ChapId}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getchapterdetails(@Path("regId") long regId, @Path("ChapId") String ChapId);

    @POST(BASE_URL + "Cart/checkout/{regId}/{coupon}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getDiscountCoupon(@Path("regId") long regId, @Path("coupon") String coupon);


    @GET(BASE_URL + "Learn/learnSubjectdetails/{regId}/{sub_id}")
    @Headers("Content-Type: application/json")
    Call<ResponseBody> getLearnChapDetails(@Path("regId") long regId, @Path("sub_id") String ChapId);

}
