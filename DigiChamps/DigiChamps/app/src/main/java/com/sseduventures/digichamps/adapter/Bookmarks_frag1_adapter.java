package com.sseduventures.digichamps.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.BookmarkOnlinePlayer;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.activity.PackageActivityNew;
import com.sseduventures.digichamps.domain.BookMarkVideoList;
import com.sseduventures.digichamps.fragment.Fragment_Bookmarks1;

import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.utils.AppUtil;

import java.util.ArrayList;

public class Bookmarks_frag1_adapter extends RecyclerView.Adapter<Bookmarks_frag1_adapter.MyViewHolder> {
    View view;
    private String resp, user_id, module_id, error, TAG = "Orders";
    private SpotsDialog dialog;
    private ImageView listener;
    private Fragment_Bookmarks1 fragment;
    private Context context;
    public ArrayList<BookMarkVideoList> moviesList;

    public Bookmarks_frag1_adapter(ArrayList<BookMarkVideoList> moviesList,
                                   Context context, Fragment_Bookmarks1 fragment) {
        this.moviesList = moviesList;
        this.context = context;
        this.fragment = fragment;

    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static boolean checkImageResource(Context ctx, ImageView imageView, int imageResource) {
        boolean result = false;

        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }

            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }

        return result;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_bookmark_frag1, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        BookMarkVideoList chap_detail = moviesList.get(position);
        holder.title.setText(chap_detail.getModule_Title());
        Picasso.with(context).load(moviesList.get(position).getModule_Image()).into(holder.thumbnail_image);

        listener = holder.play;

        // get values

        user_id = String.valueOf(RegPrefManager.getInstance(context).getRegId());

        //unbookmark button click
        holder.bookmark_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                module_id = moviesList.get(position).getModule_Id();

                fragment.setUnsetBookMark(module_id,"2");





            }
        });


        holder.container.setOnClickListener(onClickListener(position, holder));


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private View.OnClickListener onClickListener(final int position, final MyViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init();
                if (AppUtil.isInternetConnected(context)) {

                    if (moviesList.get(position).getIs_Expire().equalsIgnoreCase("true") && moviesList.get(position).getIs_Free().equalsIgnoreCase("false")) {

                        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyDialogTheme_buy);
                        LayoutInflater factory = LayoutInflater.from(context);
                        final View view = factory.inflate(R.layout.dialog_buy, null);
                        Button ok = (Button) view.findViewById(R.id.button2);
                        final AlertDialog alertDialog = builder.create();
                        ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                alertDialog.dismiss();
                                Intent i = new Intent(context, PackageActivityNew.class);
                                Activity activity = (Activity) context;
                                context.startActivity(i);


                            }
                        });

                        builder.setView(view);
                        builder.setCancelable(true);
                        builder.show();
                    } else {


                        Intent i = new Intent(context, BookmarkOnlinePlayer.class);
                        Activity activity = (Activity) context;
                        i.putExtra("videoID", moviesList.get(position).getVideoKey());
                        i.putParcelableArrayListExtra("list", moviesList);
                        context.startActivity(i);

                    }


                } else {
                    // redirect to No internet activity
                    Intent i = new Intent(context, Internet_Activity.class);
                    Activity activity = (Activity) context;

                    context.startActivity(i);

                }
            }

        };
    }


    private void init() {


        dialog = new SpotsDialog(context, "FUN + EDUCATION", R.style.Custom);


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;
        public ImageView play;
        ImageView left_drawable, right_drawable;
        CardView container;
        private ImageView thumbnail_image, bookmark_image;

        public MyViewHolder(View view) {
            super(view);
            container = (CardView) itemView.findViewById(R.id.card_vidoe);
            title = (TextView) view.findViewById(R.id.chap);
            play = (ImageView) view.findViewById(R.id.play);
            thumbnail_image = (ImageView) view.findViewById(R.id.thumbnail_imagev);
            bookmark_image = (ImageView) itemView.findViewById(R.id.bookmark);


        }
    }



}
