package com.sseduventures.digichamps.frags;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.PersonalMentorActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.PMCompletedAdapter;
import com.sseduventures.digichamps.domain.PMCompletedtaskDataList;
import com.sseduventures.digichamps.domain.PMResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;

public class PersonalMentorCompleted extends Fragment implements ServiceReceiver.Receiver {

    private RecyclerView rv_completed;
    private TextView no_completedtask;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private FormActivity mContext;
    private PMResponse success;
    private List<PMCompletedtaskDataList> pmCompletedtaskDataLists;
    private PMCompletedAdapter pmCompletedAdapter;
    PersonalMentorActivity context;
    private ShimmerFrameLayout mShimmerViewContainer;

    public PersonalMentorCompleted() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_completed_task, container, false);
        ((FormActivity)getActivity()).logEvent(LogEventUtil.
                        KEY_pm_completed_task,
                LogEventUtil.EVENT_pm_completed_task);

        ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_pm_completed_task);

        init(view);
        return view;
    }

    private void init(View view) {

        rv_completed = view.findViewById(R.id.rv_completed);
        no_completedtask = view.findViewById(R.id.no_completedtask);
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mContext = (PersonalMentorActivity) getActivity();
        pmCompletedtaskDataLists = new ArrayList<>();
        context = (PersonalMentorActivity) getActivity();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv_completed.setLayoutManager(mLayoutManager);

        pmCompletedAdapter = new PMCompletedAdapter(pmCompletedtaskDataLists, context);
        rv_completed.setAdapter(pmCompletedAdapter);

        getPmDetails();


    }


    private void getPmDetails() {

        if (AppUtil.isInternetConnected(getActivity())) {
            NetworkService.startActionGetPmDetails(getActivity(), mServiceReceiver);
        } else {
            mContext.showToast("No Internet Connection");
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);
            }
        },1000);


        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                mContext.hideDialog();
                Intent in = new Intent(getActivity(), ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                mContext.hideDialog();
                Intent intent = new Intent(getActivity(), Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                mContext.hideDialog();
                Intent inin = new Intent(getActivity(), ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:
                mContext.hideDialog();
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                setCompletedTask();


                break;


        }

    }


    private void setCompletedTask() {
        if (success != null
                && success.getSuccessresultTask() != null
                && success.getSuccessresultTask().getCompletedTaskList() != null
                && success.getSuccessresultTask().getCompletedTaskList()
                .getTaskDataList() != null
                && success.getSuccessresultTask().getCompletedTaskList()
                .getTaskDataList().size() > 0)

        {

            rv_completed.setVisibility(View.VISIBLE);
            no_completedtask.setVisibility(View.GONE);
            pmCompletedtaskDataLists.clear();
            pmCompletedtaskDataLists.addAll(success.getSuccessresultTask().getCompletedTaskList()
                    .getTaskDataList());
            pmCompletedAdapter.notifyDataSetChanged();

        } else {

            no_completedtask.setVisibility(View.VISIBLE);
            rv_completed.setVisibility(GONE);
        }

    }

    @Override
    public void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    public void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

}
