package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/25/2018.
 */

public class PMGoalDetails implements Parcelable {


   private String message;
   private boolean Is_Achieved;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isIs_Achieved() {
        return Is_Achieved;
    }

    public void setIs_Achieved(boolean is_Achieved) {
        Is_Achieved = is_Achieved;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeByte(this.Is_Achieved ? (byte) 1 : (byte) 0);
    }

    public PMGoalDetails() {
    }

    protected PMGoalDetails(Parcel in) {
        this.message = in.readString();
        this.Is_Achieved = in.readByte() != 0;
    }

    public static final Creator<PMGoalDetails> CREATOR = new Creator<PMGoalDetails>() {
        @Override
        public PMGoalDetails createFromParcel(Parcel source) {
            return new PMGoalDetails(source);
        }

        @Override
        public PMGoalDetails[] newArray(int size) {
            return new PMGoalDetails[size];
        }
    };
}
