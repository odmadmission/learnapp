package com.sseduventures.digichamps.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.firebase.LogEventUtil;

public class FeedKnowMoreActivity extends FormActivity {
    private WebView webview;
    private String uri;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_know_more);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        setModuleName(LogEventUtil.EVENT_FEED_KNOW_MORE_ACTIVITY);
        logEvent(LogEventUtil.KEY_FEED_KNOW_MORE_ACTIVITY,
                LogEventUtil.EVENT_FEED_KNOW_MORE_ACTIVITY);

        // EnableRuntimePermission();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        webview=(WebView)findViewById(R.id.webview);

        Intent iin= getIntent();
        Bundle b = iin.getExtras();
        if(b != null) {
            uri = (String) b.get("URI");
        }
        webview.setWebViewClient(new MyBrowser());
        webview.getSettings().setLoadsImagesAutomatically(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webview.loadUrl(uri);

    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    @Override
    public void onBackPressed() {

        finish();
    }

}


