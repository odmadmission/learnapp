package com.sseduventures.digichamps.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.Model.Chapter_detail_Model;
import com.sseduventures.digichamps.Model.ProgressModule;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.Bookmark_VideoView_Adapter;
import com.sseduventures.digichamps.adapter.Chapter_Details_Adapter;
import com.sseduventures.digichamps.adapter.VideoListAdapter;
import com.sseduventures.digichamps.database.DbContract;
import com.sseduventures.digichamps.domain.SetBookmarkResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.vdocipher.aegis.media.ErrorDescription;
import com.vdocipher.aegis.media.Track;
import com.vdocipher.aegis.player.VdoPlayer;
import com.vdocipher.aegis.player.VdoPlayerFragment;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import com.sseduventures.digichamps.R;

public class OnlinePlayerActivity extends
        FormActivity implements VdoPlayer.InitializationListener, ServiceReceiver.Receiver {

    private final String TAG = "OnlinePlayerActivity";

    private VdoPlayer player;
    private VdoPlayerFragment playerFragment;
    private VdoPlayerControlView playerControlView;
    //  private TextView eventLog;
    private String eventLogString = "";

    private boolean playWhenReady = false;
    private int currentOrientation;

    private volatile String mOtp;
    private volatile String mPlaybackInfo;
    private volatile String mVideoId;
    private RecyclerView mRecyclerView;
    private VideoListAdapter mVideoListAdapter;
    private ArrayList<Chapter_detail_Model> mList;

    private boolean seekDone = false;

    private long seenTime;
    private long rowId=0;


    private String subjectid=null;
    private String chapterid=null;
    private String moduleid=null;

    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  Log.v(TAG, "onCreate called");
        setContentView(R.layout.activity_online_player);
        logEvent(LogEventUtil.KEY_VIDEOS_PAGE,
                LogEventUtil.EVENT_VIDEOS_PAGE);
        setModuleName(LogEventUtil.EVENT_VIDEOS_PAGE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setModuleName(LogEventUtil.EVENT_VIDEOS_PAGE);

        getWindow().getDecorView().
                setOnSystemUiVisibilityChangeListener(uiVisibilityListener);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        setUpReciever();
        String id = null;

        if (savedInstanceState != null) {
            mOtp = savedInstanceState.getString("otp");
            mPlaybackInfo = savedInstanceState.getString("playbackInfo");
            mVideoId = savedInstanceState.getString("videoId");
            mList = savedInstanceState.getParcelableArrayList("list");
            rowId = savedInstanceState.getLong("rowId");
            subjectid = savedInstanceState.getString("subjectid");
            chapterid = savedInstanceState.getString("chapterid");
            moduleid=savedInstanceState.getString("moduleid");
            for (int i = 0; i < mList.size(); i++) {
                if (mVideoId.equals(mList.get(i).get_file_resource())) {

                    mList.get(i).setFlag(true);

                } else
                    mList.get(i).setFlag(false);
            }
        } else {
            subjectid =getIntent().getStringExtra("subjectid");
            chapterid = getIntent().getStringExtra("chapterid");
            moduleid=getIntent().getStringExtra("moduleid");
            mVideoId = getIntent().getStringExtra("videoid");
            mList = getIntent().getParcelableArrayListExtra("list");
            for (int i = 0; i < mList.size(); i++) {
                if (mVideoId.equals(mList.get(i).get_file_resource())) {
                    id = mList.get(i).getModule_Id();
                    mList.get(i).setFlag(true);

                } else
                    mList.get(i).setFlag(false);
            }
            save(id);
        }


        mRecyclerView = findViewById(R.id.videoList);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);

                mVideoListAdapter = new VideoListAdapter(mList, OnlinePlayerActivity.this, mVideoId);

                mRecyclerView.setAdapter(mVideoListAdapter);

            }
        },1000);



        playerFragment = (VdoPlayerFragment) getFragmentManager().
                findFragmentById(R.id.online_vdo_player_fragment);
        playerControlView = (VdoPlayerControlView) findViewById(R.id.player_control_view);
        //eventLog = (TextView)findViewById(R.id.event_log);
        // eventLog.setMovementMethod(ScrollingMovementMethod.getInstance());

        showControls(true);
        playerControlView.hideAfterTimeout();

        currentOrientation = getResources().getConfiguration().orientation;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

        initializePlayer();
    }

    public void save(String moduleId) {
      //  Log.v("DigiLog", moduleId);
        Bundle bundle = new Bundle();
        bundle.putString("moduleId", moduleId);

        NetworkService.startActionRecentlyWatched(
                this, mServiceReceiver, bundle);
    }

    @Override
    protected void onStart() {
       // Log.v(TAG, "onStart called");
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    protected void onStop() {
      //  Log.v(TAG, "onStop called");
        disablePlayerUI();
        if(rowId>0) {
            VideoLogUpdateTask task = new VideoLogUpdateTask();
            task.execute(rowId+"", 2 + "", player.getCurrentTime()+"");
        }
        super.onStop();
        mServiceReceiver.setReceiver(null);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
      //  Log.v(TAG, "onSaveInstanceState called");
        super.onSaveInstanceState(outState);
        if (mOtp != null && mPlaybackInfo != null && mVideoId != null) {
            outState.putString("otp", mOtp);
            outState.putString("playbackInfo", mPlaybackInfo);
            outState.putString("videoId", mVideoId);
            outState.putLong("rowId", rowId);
            outState.putString("subjectid", subjectid);
            outState.putString("chapterid", chapterid);
            outState.putString("moduleid", moduleid);
        }
    }

    private void initializePlayer() {
        if (mOtp != null) {
            // initialize the playerFragment; a VdoPlayer instance will be received
            // in onInitializationSuccess() callback
            playerFragment.initialize(OnlinePlayerActivity.this);
           // log("initializing player fragment");
        } else {
            // lets get otp and playbackInfo before creating the player
            if (mVideoId != null)
                obtainOtpAndPlaybackInfo();
            else
                showToast("No VideoId Found");
        }
    }

    /**
     * Fetch (otp + playbackInfo) and initialize VdoPlayer
     * here we're fetching a sample (otp + playbackInfo)
     * TODO you need to generate/fetch (otp + playbackInfo) OR (signature + playbackInfo) for the
     * video you wish to play
     */

    private void obtainOtpAndPlaybackInfo() {
        // todo use asynctask
        //log("fetching params...");
       new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Pair<String, String> pair =
                            Utils.postSampleOtpAndPlaybackInfo(
                                    mVideoId
                            );
                    mOtp = pair.first;
                    mPlaybackInfo = pair.second;
                  //  Log.i(TAG, "obtained new otp and playbackInfo");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initializePlayer();
                        }
                    });
                } catch (final Exception e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showToast("Error fetching otp and playbackInfo: "
                                    + e.getClass().getSimpleName());
                          //  log("error fetching otp and playbackInfo");
                        }
                    }

                    );
                }
            }
        }).start();
       // thread.start();
    }


    private void log(String msg) {
        eventLogString += (msg + "\n");
        //eventLog.setText(eventLogString);
    }

    private void showControls(boolean show) {
      //  Log.v(TAG, (show ? "show" : "hide") + " controls");
        if (show) {
            playerControlView.show();
        } else {
            playerControlView.hide();
        }
    }

    private void disablePlayerUI() {
        showControls(false);
    }

    public void playVideo(String videoId) {

        if(rowId>0) {
            long time=0;
            if(player!=null)
            time=player.getCurrentTime();
            else
                time=0;

            VideoLogUpdateTask task = new VideoLogUpdateTask();
            task.execute(rowId+"", 2 + "",time +"");
        }

        playerControlView.seekDone=false;
        seekDone = false;
        this.mVideoId = videoId;
        mOtp = null;
        mPlaybackInfo = null;
        playerControlView.stop();
        player = null;
        firstTime = false;
        showControls(true);
        playerControlView.hideAfterTimeout();

        for (int i = 0; i < mList.size(); i++) {
            if (videoId.equals(mList.get(i).get_file_resource()))
                mList.get(i).setFlag(true);

            else
                mList.get(i).setFlag(false);
        }




        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);

                mVideoListAdapter.notifyDataSetChanged();

            }
        },1000);



//        if(thread!=null&&thread.getState()== Thread.State.RUNNABLE) {
//            thread.interrupt();
//
//            thread=null;
//
//        }


        initializePlayer();


    }


    @Override
    public void onInitializationSuccess(VdoPlayer.PlayerHost playerHost,
                                        VdoPlayer player,
                                        boolean wasRestored) {
       // Log.i(TAG, "onInitializationSuccess");
       // log("onInitializationSuccess");
        VideoLogSaveTask task=new VideoLogSaveTask();
        task.execute(moduleid,1+"",0+"",moduleid,
                subjectid,chapterid);
        this.player = player;
        player.addPlaybackEventListener(playbackListener);
        playerControlView.setPlayer(player);
        // showControls(true);

        playerControlView.setFullscreenActionListener(fullscreenToggleListener);
        playerControlView.setControllerVisibilityListener(visibilityListener);


        // load a media to the player
        VdoPlayer.VdoInitParams vdoParams = new VdoPlayer.VdoInitParams.Builder()
                .setOtp(mOtp)
                .setPlaybackInfo(mPlaybackInfo)
                .setPreferredCaptionsLanguage("en")
                .build();
        player.load(vdoParams);

      //  log("loaded init params to player");
    }

    @Override
    public void onInitializationFailure(VdoPlayer.PlayerHost playerHost,
                                        ErrorDescription errorDescription) {
        String msg = "onInitializationFailure: errorCode = " +
                errorDescription.errorCode + ": " + errorDescription.errorMsg;
        //log(msg);
       // Log.e(TAG, msg);
        Toast.makeText(OnlinePlayerActivity.this,
                "initialization failure: " + errorDescription.errorMsg,
                Toast.LENGTH_LONG).show();
    }

    private boolean firstTime = false;
    private VdoPlayer.PlaybackEventListener playbackListener =
            new VdoPlayer.PlaybackEventListener() {
        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState)
        {

           // log(Utils.playbackStateString(playWhenReady, playbackState));
            OnlinePlayerActivity.this.playWhenReady = playWhenReady;
            if (!firstTime) {
                firstTime = true;
                playerControlView.play();
            }
        }

        @Override
        public void onTracksChanged(Track[] tracks, Track[] tracks1)
        {
//            Log.i(TAG, "onTracksChanged");
//            log("onTracksChanged");
        }

        @Override
        public void onBufferUpdate(long bufferTime)
        {

        }

        @Override
        public void onSeekTo(long millis) {
           // Log.i(TAG, "onSeekTo: " + String.valueOf(millis));
            seekDone = true;
        }

        @Override
        public void onProgress(long millis) {
        }

        @Override
        public void onPlaybackSpeedChanged(float speed) {
           // Log.i(TAG, "onPlaybackSpeedChanged " + speed);
            //log("onPlaybackSpeedChanged " + speed);
        }

        @Override
        public void onLoading(VdoPlayer.VdoInitParams vdoInitParams) {
           // Log.i(TAG, "onLoading");
            //log("onLoading");
        }

        @Override
        public void onLoadError(VdoPlayer.VdoInitParams vdoInitParams,
                                ErrorDescription errorDescription) {
            String err = "onLoadError code: " + errorDescription.errorCode;
            String errmsg = errorDescription.errorMsg;
           // Log.e(TAG, err);
            //Log("Abhishek", errmsg);
            //log(err);
        }

        @Override
        public void onLoaded(VdoPlayer.VdoInitParams vdoInitParams) {

        }

        @Override
        public void onError(VdoPlayer.VdoInitParams vdoParams,
                            ErrorDescription errorDescription) {
            String err = "onError code " + errorDescription.
                    errorCode + ": " + errorDescription.errorMsg;
            //Log.e(TAG, err);
           // log(err);
        }

        @Override
        public void onMediaEnded(VdoPlayer.VdoInitParams vdoInitParams) {

           Log.v(TAG,player.getDuration()+"");
            if(rowId>0) {
                VideoLogUpdateTask task = new VideoLogUpdateTask();
                task.execute(rowId+"", 2 + "", player.getDuration()+"");
            }
           // Toast.makeText(OnlinePlayerActivity.this,
             //       seekDone+":"+playerControlView.seekDone,Toast.LENGTH_SHORT).show();
            if (!seekDone&&!playerControlView.seekDone) {

                long id = 0;
                for (int i = 0; i < mList.size(); i++) {
                    if (mVideoId.equals(mList.get(i).get_file_resource())) {
                        id = Long.parseLong(mList.get(i).getModule_Id());
                    }
                }

                Bundle bundle = new Bundle();
                bundle.putLong("moduleid", id);
                NetworkService.startActionSaveVVideoCoins(OnlinePlayerActivity.
                        this, mServiceReceiver, bundle);
            }
        }
    };

    private VdoPlayerControlView.FullscreenActionListener fullscreenToggleListener = new VdoPlayerControlView.FullscreenActionListener() {
        @Override
        public boolean onFullscreenAction(boolean enterFullscreen) {
            showFullScreen(enterFullscreen);
            return true;
        }
    };

    private VdoPlayerControlView.ControllerVisibilityListener visibilityListener = new VdoPlayerControlView.ControllerVisibilityListener() {
        @Override
        public void onControllerVisibilityChange(int visibility) {

            if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                if (visibility != View.VISIBLE) {
                    showSystemUi(false);
                }
            }
        }
    };

    private boolean iWantToBeInPipModeNow() {

        return true;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        final int newOrientation = newConfig.orientation;
        final int oldOrientation = currentOrientation;
        currentOrientation = newOrientation;

        super.onConfigurationChanged(newConfig);
        if (newOrientation == oldOrientation) {

        } else if (newOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            // hide other views
            //(findViewById(R.id.title_text)).setVisibility(View.GONE);
            //(findViewById(R.id.log_container)).setVisibility(View.GONE);
            (findViewById(R.id.online_vdo_player_fragment)).setLayoutParams(new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
            playerControlView.setFitsSystemWindows(true);
            // hide system windows
            showSystemUi(false);
            showControls(false);
        } else {
            // show other views
            //(findViewById(R.id.title_text)).setVisibility(View.VISIBLE);
            // (findViewById(R.id.log_container)).setVisibility(View.VISIBLE);
            (findViewById(R.id.online_vdo_player_fragment)).setLayoutParams(new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            playerControlView.setFitsSystemWindows(false);
            playerControlView.setPadding(0, 0, 0, 0);
            // show system windows
            showSystemUi(true);
        }
    }

    @Override
    public void onBackPressed() {
        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            showFullScreen(false);
            playerControlView.setFullscreenState(false);
        } else {
            if(rowId>0) {
                long time=0;
                if(player!=null)
                    time=player.getCurrentTime();

                VideoLogUpdateTask task = new VideoLogUpdateTask();
                task.execute(rowId+"", 2 + "", time+"");
            }
            super.onBackPressed();
        }
    }

    private void showFullScreen(boolean show) {
       // Log.v(TAG, (show ? "enter" : "exit") + " fullscreen");
        if (show) {
            // go to landscape orientation for fullscreen mode
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        } else {
            // go to portrait orientation
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }

    private void showSystemUi(boolean show) {
       // Log.v(TAG, (show ? "show" : "hide") + " system ui");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (!show) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
            } else {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            }
        }
    }

    private View.OnSystemUiVisibilityChangeListener uiVisibilityListener = new View.OnSystemUiVisibilityChangeListener() {
        @Override
        public void onSystemUiVisibilityChange(int visibility) {
         //   Log.v(TAG, "onSystemUiVisibilityChange");
            // show player controls when system ui is showing
            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
              //  Log.v(TAG, "system ui visible, making controls visible");
                showControls(true);
            }
        }
    };


    public void setUnsetBookMark(String dataid,String dataType){
        if (AppUtil.isInternetConnected(this)){

            Bundle bun = new Bundle();
            bun.putString("dataType",dataType);
            bun.putString("dataId",dataid);

            NetworkService.startActionGetSetUsetBookmark(this,mServiceReceiver,bun);
        }else{
            Intent in = new Intent(this, Internet_Activity.class);
            startActivity(in);
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        switch(resultCode){
            case ResponseCodes.BOOKMARKED:
                SetBookmarkResponse msuccess = resultData.getParcelable(IntentHelper.RESULT_DATA);



                ArrayList<Chapter_detail_Model> li = new ArrayList<>();
                for(int i=0;i<mList.size();i++)
                {
                    String id=resultData.getString("dataId");

                    if(id.equals(mList.get(i).getModule_Id()))
                    {
                        mList.get(i).setIsBookmarked("true");

                    }

                    li.add(mList.get(i));

                }

                Toast.makeText(this, ""+msuccess.getSuccess().getMessage(), Toast.LENGTH_SHORT).show();


                mList.clear();
                mList.addAll(li);
                mVideoListAdapter.notifyDataSetChanged();




                break;
            case ResponseCodes.UNBOOKMARKED:
                SetBookmarkResponse mmsuccess =
                        resultData.getParcelable(IntentHelper.RESULT_DATA);




                ArrayList<Chapter_detail_Model> l=new ArrayList<>();
                for(int i=0;i<mList.size();i++)
                {
                    String id=resultData.getString("dataId");

                    if(id.equals(mList.get(i).getModule_Id()))
                    {
                        mList.get(i).setIsBookmarked("false");

                    }

                    l.add(mList.get(i));

                }
                Toast.makeText(this, ""+mmsuccess.getSuccess().getMessage(), Toast.LENGTH_SHORT).show();

                mList.clear();
                mList.addAll(l);
                mVideoListAdapter.notifyDataSetChanged();






                break;



        }

    }


    public class VideoLogSaveTask extends AsyncTask<String,Void,Long>
    {


        @Override
        protected Long doInBackground(String... strings)
        {
            try {
                ContentValues mContentValues = new ContentValues();
                mContentValues.put(DbContract.VideoLogTable.MODULEID,
                        strings[0]);
                mContentValues.put(DbContract.VideoLogTable.REGDID,
                        RegPrefManager.getInstance(OnlinePlayerActivity.this).getRegId());
                mContentValues.put(DbContract.VideoLogTable.SCHOOL_CODE,
                        RegPrefManager.getInstance(OnlinePlayerActivity.this).
                                getSchoolId());
                mContentValues.put(DbContract.VideoLogTable.SECTION_CODE,
                        RegPrefManager.getInstance(OnlinePlayerActivity.this).
                                getSectionId());
                mContentValues.put(DbContract.VideoLogTable.CLASS_CODE,
                        RegPrefManager.getInstance(OnlinePlayerActivity.this).
                                getclassid());

                mContentValues.put(DbContract.VideoLogTable.STATUS,
                        Integer.parseInt(strings[1]));

                mContentValues.put(DbContract.VideoLogTable.SYNC_DATE,
                        System.currentTimeMillis());

                mContentValues.put(DbContract.VideoLogTable.CREATE_DATE,
                        System.currentTimeMillis());

                mContentValues.put(DbContract.VideoLogTable.SEEN_TIME,
                        Integer.parseInt(strings[2]));

                mContentValues.put(DbContract.VideoLogTable.START_TIME,
                        System.currentTimeMillis());
                Log.v(TAG," VideoLog : "+mContentValues.toString());

                Uri uri = getContentResolver().
                        insert(DbContract.VideoLogTable.CONTENT_URI,
                                mContentValues);

                if (uri != null) {
                    rowId = Integer.parseInt(
                            uri.getLastPathSegment());

                    Log.v(TAG, uri.getLastPathSegment());
                }


                ProgressModule contact = new ProgressModule();
                contact.setModule_ID(Integer.parseInt(
                        strings[3]));
                contact.setModule_Type(Constants.PROGRESS_VIDEO);
                contact.setSection_ID(RegPrefManager.getInstance(
                        OnlinePlayerActivity.this
                ).getSectionId());
                contact.setClass_ID((int) RegPrefManager.getInstance(
                        OnlinePlayerActivity.this
                ).getKeyClassId());
                contact.setChapter_ID(Integer.parseInt(
                        strings[5]
                ));
                contact.setSubject_ID(Integer.parseInt(
                        strings[4]
                ));
                contact.setRegd_ID(RegPrefManager.getInstance(OnlinePlayerActivity.this)
                        .getRegId());
                contact.setInserted_On(new Date(System.currentTimeMillis()));
                ContentValues values = new ContentValues();


                values.put(
                        DbContract.ProgressTable.KEY_CLASS_ID
                        , contact.getClass_ID());
                values.put(
                        DbContract.ProgressTable.KEY_SECTION_ID,
                        contact.getSection_ID());
                values.put(DbContract.ProgressTable.KEY_CHAPTER_ID,
                        contact.getChapter_ID());
                values.put(DbContract.ProgressTable.KEY_SUBJECT_ID
                        , contact.getSubject_ID());
                values.put(DbContract.ProgressTable.KEY_MODULE_ID,
                        contact.getModule_ID());
                values.put(DbContract.ProgressTable.KEY_MODULE_TYPE,
                        contact.getModule_Type());

                values.put(DbContract.ProgressTable
                        .KEY_INSERT_DATE_TIME, contact.getInserted_On().getTime());
                values.put(DbContract.ProgressTable.KEY_REG_ID,
                        contact.getRegd_ID());

                Log.v(TAG,"Progress Module" +values.toString());
                Uri uri2 = getContentResolver().insert(
                        DbContract.ProgressTable.CONTENT_URI,
                        values);

                if (uri2 != null) {
                    Log.v(TAG, "Progress Module" + uri2.getLastPathSegment() + "");
                }

            }
            catch (Exception e)
            {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
        }
    }
    public class VideoLogUpdateTask extends AsyncTask<String,Void,Long>
    {


        @Override
        protected Long doInBackground(String... strings)
        {
            try {
                ContentValues mContentValues = new ContentValues();

                mContentValues.put(DbContract.VideoLogTable.STATUS,
                        Integer.parseInt(strings[1]));

                mContentValues.put(DbContract.VideoLogTable.END_TIME,
                        System.currentTimeMillis());

                mContentValues.put(DbContract.VideoLogTable.SEEN_TIME,
                        Integer.parseInt(strings[2]));

                Log.v(TAG,"VideoLog Module" +mContentValues.toString());
                long update = getContentResolver().
                        update(DbContract.VideoLogTable.CONTENT_URI,
                                mContentValues,
                                DbContract.VideoLogTable._ID + " LIKE ? ",
                                new String[]{strings[0]});


                Log.v(TAG, update + "");


                if (update == 1) {
                    rowId = 0;
                }

            }
            catch (Exception e)
            {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
        }
    }
}
