package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/25/2018.
 */

public class PMTeacherDetails implements Parcelable {


   private int teacherId;
   private String teacherName;
   private String teacherProfilePicture;
   private String teacherEmail;
   private String teachermobile;


    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherProfilePicture() {
        return teacherProfilePicture;
    }

    public void setTeacherProfilePicture(String teacherProfilePicture) {
        this.teacherProfilePicture = teacherProfilePicture;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public String getTeachermobile() {
        return teachermobile;
    }

    public void setTeachermobile(String teachermobile) {
        this.teachermobile = teachermobile;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.teacherId);
        dest.writeString(this.teacherName);
        dest.writeString(this.teacherProfilePicture);
        dest.writeString(this.teacherEmail);
        dest.writeString(this.teachermobile);
    }

    public PMTeacherDetails() {
    }

    protected PMTeacherDetails(Parcel in) {
        this.teacherId = in.readInt();
        this.teacherName = in.readString();
        this.teacherProfilePicture = in.readString();
        this.teacherEmail = in.readString();
        this.teachermobile = in.readString();
    }

    public static final Creator<PMTeacherDetails> CREATOR = new Creator<PMTeacherDetails>() {
        @Override
        public PMTeacherDetails createFromParcel(Parcel source) {
            return new PMTeacherDetails(source);
        }

        @Override
        public PMTeacherDetails[] newArray(int size) {
            return new PMTeacherDetails[size];
        }
    };
}
