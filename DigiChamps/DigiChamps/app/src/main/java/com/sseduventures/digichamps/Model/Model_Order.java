package com.sseduventures.digichamps.Model;

/**
 * Created by ntspl24 on 4/15/2017.
 */

public class Model_Order {
    private String date, order_no,
            Count,price,id;
    private String Discount;


    public Model_Order(String date, String order_no, String Count,String price,String id,
                       String Discount) {
        this.date = date;
        this.order_no = order_no;
        this.Count = Count;
        this.price = price;
        this.id = id;
        this.Discount = Discount;
    }

    public String getDate() {
        return date;
    }

    public String getOrder_no() {
        return order_no;
    }

    public String getCount() {
        return Count;
    }



    public String getPrice() {
        return price;
    }
    public String getId() {
        return id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }
}



