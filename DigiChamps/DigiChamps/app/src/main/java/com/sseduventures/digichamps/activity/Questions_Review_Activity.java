package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sseduventures.digichamps.Model.Model_Exam;
import com.sseduventures.digichamps.Model.ProgressModule;
import com.sseduventures.digichamps.Model.Questions_Review;
import com.sseduventures.digichamps.Model.RealmExamDetails;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewExamActivity;
import com.sseduventures.digichamps.adapter.Questions_Review_Adapter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.model.ExamAnswers;
import com.sseduventures.digichamps.webservice.model.ExamQuestion;
import com.sseduventures.digichamps.webservice.model.GetPostExamData;
import com.sseduventures.digichamps.webservice.model.GetQuestion;
import com.sseduventures.digichamps.webservice.model.PostQuestionRequestModel;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class Questions_Review_Activity extends FormActivity implements ServiceReceiver.Receiver{

    private static final String TAG=Questions_Review_Activity.class.getSimpleName();
    private RecyclerView recyclerView;
    private Questions_Review_Adapter adapter;
    private List<Questions_Review> albumList;
    private static CountDownTimer countDownTimer;
    ImageView back_arrow;
    private String exam_id;
    int result_id;
    Bundle extras, args;
    private boolean userIsOut;
    ArrayList<Model_Exam> object;
    TextView tv_timer;
    ImageView time_taken;
    Integer[] skipped, answered;
    public View.OnClickListener mOnClickListener;
    Button resume_btn;
    ArrayList<RealmExamDetails> results;

    String user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setModuleName(LogEventUtil.KEY_Question_Review);
        logEvent(LogEventUtil.KEY_Question_Review,
                LogEventUtil.EVENT_Question_Review);
        setContentView(R.layout.questions_review_activity);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setUpReciever();
        init();
        user_id = RegPrefManager.getInstance(this)
                .getRegId()+"";

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_ques);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);



        results=getIntent().getParcelableArrayListExtra("Results");
        initCollapsingToolbar();

        extras = getIntent().getExtras();
        object = this.getIntent().getExtras().getParcelableArrayList("Birds");
        exam_id =getIntent().getStringExtra("exam_id");
        result_id =getIntent().getIntExtra("result_id",0);
        if (extras != null) {


            String timer_value = extras.getString("timer_value");

            if (!timer_value.equalsIgnoreCase("")) {
                startTimer(cal_time_remaining(timer_value));
            }
        }

        albumList = new ArrayList<>();
        getSkipped();
        getAnswered();
        adapter = new Questions_Review_Adapter
                (this,
                exam_id, result_id, object,results);


        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 5);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(1), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkSnackbar();

            }
        };

        prepareAlbums();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
        hideDialog();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        hideDialog();
    }
    private Button end_test_text;
    public void init() {


        time_taken = (ImageView) findViewById(R.id.exam_time_text);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_ques);
        tv_timer = (TextView) findViewById(R.id.tv_clock);
        end_test_text = (Button) findViewById(R.id.end_test_text);
        resume_btn = (Button) findViewById(R.id.resume_btn);
        end_test_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(Questions_Review_Activity.this)
                        .setIcon(R.drawable.alert_warning)
                        .setTitle("End Test")
                        .setMessage("Do you want to End Test?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                DbTask dbTask=new DbTask();
                                dbTask.execute(getIntent()
                                                .getStringExtra("module_type"),
                                        getIntent()
                                                .getStringExtra("chapter_id"),
                                        getIntent()
                                                .getStringExtra("subject_id"));

                                if (AppUtil.isInternetConnected(Questions_Review_Activity.this)) {
                                    postExamData();
                                    countDownTimer.cancel();
                                } else {
                                    new AlertDialog.Builder(Questions_Review_Activity.this)
                                            .setIcon(R.drawable.alert_warning)
                                            .setTitle("No Internet!")
                                            .setMessage("Please Connect to Internet.")
                                            .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    checkSnackbar();
                                                }
                                            })
                                            .show();
                                }

                            }
                        })
                        .setNegativeButton("No", null).show();
            }
        });

        resume_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
    }


    public int cal_time_remaining(String timer_value) {

        String[] units = timer_value.split(":");
        int hours = Integer.parseInt(units[0]) * 3600;
        int minutes = Integer.parseInt(units[1]) * 60;
        int seconds = Integer.parseInt(units[2]);
        int duration = hours + minutes + seconds;
        return duration * 1000;
    }


    private void initCollapsingToolbar() {

    }

    public void getSkipped() {
        ArrayList<Integer> stringArrayList = new ArrayList<Integer>();

        for (int i = 0; i < results.size(); i++) {
            stringArrayList.add(results.get(i).getSkip()); //add to arraylist
       }

        skipped = stringArrayList.toArray(new Integer[stringArrayList.size()]);

    }

    public void getAnswered() {
        ArrayList<Integer> stringArrayList = new ArrayList<Integer>();

        for (int i = 0; i < results.size(); i++) {
            stringArrayList.add(results.get(i).getoption()); //add to arraylist
        }

        answered = stringArrayList.toArray(new Integer[stringArrayList.size()]);

    }


    private void prepareAlbums() {


        for (int i = 0; i < results.size(); i++) {
            Questions_Review a = new Questions_Review(String.valueOf(i + 1));
           albumList.add(a);

        }
        adapter.notifyDataSetChanged();
    }


    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();

        extras = getIntent().getExtras();
        object = this.getIntent().getExtras().getParcelableArrayList("Birds");
                if (extras != null) {

        exam_id = extras.getString("exam_id");
        result_id = getIntent().getIntExtra("result_id",0);
        String timer_value = extras.getString("timer_value");

        if (!timer_value.equalsIgnoreCase("")) {
            startTimer(cal_time_remaining(timer_value));
        }
    }
      return true;
}


    @Override
    public void onBackPressed() {

        extras = getIntent().getExtras();
        object = this.getIntent().getExtras().getParcelableArrayList("Birds");
        result_id =  this.getIntent().getIntExtra("result_id",0);
        if (extras != null) {

            exam_id = extras.getString("exam_id");

            String timer_value = extras.getString("timer_value");

            if (!timer_value.equalsIgnoreCase("")) {
                startTimer(cal_time_remaining(timer_value));
            }
        }
        finish();

    }


    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public void startTimer(int noOfMinutes) {
        countDownTimer = new CountDownTimer(noOfMinutes - 1, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis), TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

                if (exam_id == null) {

                } else {
                    tv_timer.setText(hms);
                }
            }

            public void onFinish() {
                if (exam_id == null) {

                } else {
                    tv_timer.setText("Time out!");
                }
                if (!userIsOut) {

                    Intent intent = new Intent(Questions_Review_Activity.this,
                            NewExamActivity.class);
                    intent.putExtra("position", object.size() - 1);//chap_id filelist
                    intent.putExtra("exam_id", exam_id);
                    intent.putExtra("result_id", result_id);
                    intent.putExtra("activity", "Question Review");
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("Birds", object);
                    bundle.putParcelableArrayList("Results", results);
                    intent.putExtras(bundle);
                    startActivity(intent);

                    finish();
                }

            }
        }.start();

    }

    @Override
    protected void onDestroy() {
        userIsOut = true;
        super.onDestroy();
    }

    private boolean isFullScreenTrue = false;
    private boolean submited = true;
    private boolean flag_submit = false;

    public void postExamData() {


        submited=false;
        PostQuestionRequestModel m = new PostQuestionRequestModel();

        GetQuestion q = new GetQuestion();
        q.setResult_id(result_id);
        q.setExam_id(String.valueOf(exam_id));
        q.setStudent_id(user_id);


        ArrayList<ExamQuestion> ql = new ArrayList<>();
        for (int i = 0; i < object.size(); i++) {


            ExamQuestion eq = new ExamQuestion();
            eq.setQuestion_id(object.get(i).getQuestion_Id());
            ArrayList<ExamAnswers> answers = new ArrayList<>();
            ExamAnswers ea=null;
            if(results.get(i).getAnswerId()!=null&&
                    !results.get(i).getAnswerId().equals("null")&&
                    results.get(i).getoption()>0) {
                ea = new ExamAnswers();
                ea.setAnswer_id(results.get(i).getAnswerId());


                answers.add(ea);
            }
            eq.setAnswers(answers);


            ql.add(eq);
        }
        q.setQuestion(ql);
        m.setGet_question(q);

        showDialog(null,"please wait..");
        Bundle bundle = new Bundle();
        bundle.putParcelable("data", m);
        NetworkService.startActionGetPostExamData(this, mServiceReceiver, bundle);
   }

    private class DbTask extends AsyncTask<String,Void,Void>
    {
        @Override
        protected Void doInBackground(String... strings)
        {
            try {
                Log.v(TAG,strings[0]+":"+strings[1]+":"+strings[2]);

                ProgressModule pm = new ProgressModule();
                pm.setModule_ID(Integer.parseInt(exam_id));
                pm.setModule_Type(strings[0]);
                pm.setSection_ID(RegPrefManager
                .getInstance(Questions_Review_Activity.this).getSectionId());

                pm.setClass_ID(
                        (int)RegPrefManager
                                .getInstance(Questions_Review_Activity.this).getclassid());
                pm.setChapter_ID(Integer.parseInt(strings[1]));
                pm.setSubject_ID(Integer.parseInt(strings[2]));



                Log.v(TAG, "Inserted rowId : " + rowId);

            }
            catch (Exception e)
            {
                Log.v(TAG,e.getMessage()+"");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    public void checkSnackbar() {


        // TODO Auto-generated method stub
        ConnectivityManager connectivityManager = (ConnectivityManager)
                getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfos = connectivityManager.getActiveNetworkInfo();
            //checking for internet
            if (netInfos != null) {
                if (netInfos.isConnected()) {

                    if (AppUtil.isInternetConnected(this)) {
                        postExamData();

                    }
                }

            } else {
                RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.rlRoot);
                Snackbar snackbar = Snackbar
                        .make(relativeLayout, "No internet connection", Snackbar.LENGTH_LONG)
                        .setAction("Retry", mOnClickListener);
                snackbar.setActionTextColor(Color.WHITE);
                View snackbarView = snackbar.getView();
                snackbarView.setBackgroundColor(Color.DKGRAY);
                TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
            }
        }
    }
    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(this, ErrorActivity.class);
                startActivity(inin);
                break;


            case ResponseCodes.STATUS:

                GetPostExamData getPostExamData = resultData.getParcelable(
                        IntentHelper.RESULT_DATA
                );

                if (getPostExamData == null||getPostExamData.getSuccess()==null) {
                    Toast.makeText(this,
                            "Something went wrong",
                            Toast.LENGTH_SHORT).show();
                } else {


                    String success_message = getPostExamData.getSuccess().getMessage();
                    String Result_id = String.valueOf(getPostExamData.getSuccess().getResult_id());
                    String moduleType = getIntent().getStringExtra("module_type");
                    String chapIdNew = getIntent().getStringExtra("chapter_id");
                    String subIdNew = getIntent().getStringExtra("subject_id");
                    String subNameNew = getIntent().getStringExtra("subNameNewNew");
                    String flagNameNew = getIntent().getStringExtra("flagNameNewNew");


                    Intent i = new Intent(this,
                            ScoreActivity.class);
                    i.putExtra("Result_ID", Result_id);
                    i.putExtra("exam_id", exam_id);
                    i.putExtra("chapter_id", chapIdNew);
                    i.putExtra("module_type", moduleType);
                    i.putExtra("subject_id", subIdNew);
                    i.putExtra("subName", subNameNew);
                    i.putExtra("flagName", flagNameNew);
                    i.putExtra("activity", "NewNewExamActivity");


                    startActivity(i);
                    overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
                    finish();
                    Toast.makeText(this, success_message, Toast.LENGTH_LONG).show();

                    break;
                }
        }

    }

}
