package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/11/2018.
 */

public class ExamScheduleDetailsResponse implements Parcelable{

    private List<ExamScheduleDetailsSuccess> ExamScheduleList;
    private String Message;
    private int ResultCount;

    public List<ExamScheduleDetailsSuccess> getExamScheduleList() {
        return ExamScheduleList;
    }

    public void setExamScheduleList(List<ExamScheduleDetailsSuccess> examScheduleList) {
        ExamScheduleList = examScheduleList;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getResultCount() {
        return ResultCount;
    }

    public void setResultCount(int resultCount) {
        ResultCount = resultCount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.ExamScheduleList);
        dest.writeString(this.Message);
        dest.writeInt(this.ResultCount);
    }

    public ExamScheduleDetailsResponse() {
    }

    protected ExamScheduleDetailsResponse(Parcel in) {
        this.ExamScheduleList = in.createTypedArrayList(ExamScheduleDetailsSuccess.CREATOR);
        this.Message = in.readString();
        this.ResultCount = in.readInt();
    }

    public static final Creator<ExamScheduleDetailsResponse> CREATOR = new Creator<ExamScheduleDetailsResponse>() {
        @Override
        public ExamScheduleDetailsResponse createFromParcel(Parcel source) {
            return new ExamScheduleDetailsResponse(source);
        }

        @Override
        public ExamScheduleDetailsResponse[] newArray(int size) {
            return new ExamScheduleDetailsResponse[size];
        }
    };
}
