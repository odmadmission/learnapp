package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class ScLeaderboardListData implements Parcelable {

    private String name;
    private int marks;
    private String imageURL;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.marks);
        dest.writeString(this.imageURL);
    }

    public ScLeaderboardListData() {
    }

    protected ScLeaderboardListData(Parcel in) {
        this.name = in.readString();
        this.marks = in.readInt();
        this.imageURL = in.readString();
    }

    public static final Creator<ScLeaderboardListData> CREATOR = new Creator<ScLeaderboardListData>() {
        @Override
        public ScLeaderboardListData createFromParcel(Parcel source) {
            return new ScLeaderboardListData(source);
        }

        @Override
        public ScLeaderboardListData[] newArray(int size) {
            return new ScLeaderboardListData[size];
        }
    };
}
