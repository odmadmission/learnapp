package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PostExamSuccess implements Parcelable {

    protected PostExamSuccess(Parcel in) {
        Message = in.readString();
        Result_id = in.readInt();
    }

    public static final Creator<PostExamSuccess> CREATOR = new Creator<PostExamSuccess>() {
        @Override
        public PostExamSuccess createFromParcel(Parcel in) {
            return new PostExamSuccess(in);
        }

        @Override
        public PostExamSuccess[] newArray(int size) {
            return new PostExamSuccess[size];
        }
    };

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getResult_id() {
        return Result_id;
    }

    public void setResult_id(int result_id) {
        Result_id = result_id;
    }

    private String Message;
    private int Result_id;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Message);
        dest.writeInt(Result_id);
    }
}
