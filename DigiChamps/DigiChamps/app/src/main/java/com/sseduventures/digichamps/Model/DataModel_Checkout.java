package com.sseduventures.digichamps.Model;

/**
 * Created by ntspl24 on 4/12/2017.
 */

public class DataModel_Checkout {
    String month;
    String chap;
    String status;
    String corss_price;
    String actual_price,discounted_price;
    String main_price,package_name,package_id,cart_id,image,discounPercentage;


    public DataModel_Checkout(String package_name, String package_id, String cart_id,
                              String month, String chap, String status,
                              String corss_price, String main_price, String actual_price, String discounted_price,String image,String discounPercentage) {
        this.month= month;
        this.chap = chap;
        this.status = status;
        this.corss_price = corss_price;
        this.main_price = main_price;
        this.package_name = package_name;
        this.package_id = package_id;
        this.cart_id = cart_id;
        this.actual_price= actual_price;
        this.discounted_price= discounted_price;
        this.image= image;
        this.discounPercentage =discounPercentage;


    }

    public String getPackage_id() {
        return package_id;
    }

    public String getCart_id() {
        return cart_id;
    }

    public String getMonth() {
        return month;
    }

    public String getchap() {
        return chap;
    }

    public String getStatus() {
        return status;
    }
    public String getpackage_name() {
        return package_name;
    }

    public String getcorss_price() {
        return corss_price;
    }

    public String getActual_price() {
        return actual_price;
    }

    public String getDiscounted_price() {
        return discounted_price;
    }

    public String getMain_price() {
        return main_price;
    }
    public String getImage() {
        return image;
    }
    public String getDiscounPercentage() {
        return discounPercentage;
    }


}



