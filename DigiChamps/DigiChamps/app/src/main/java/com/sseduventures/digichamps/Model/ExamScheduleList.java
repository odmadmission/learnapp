
package com.sseduventures.digichamps.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExamScheduleList implements Parcelable {


    private String ExamTypeId;

    private String ExamName;

    private String StartDate;

    public String getExamTypeId() {
        return ExamTypeId;
    }

    public void setExamTypeId(String examTypeId) {
        ExamTypeId = examTypeId;
    }

    public String getExamName() {
        return ExamName;
    }

    public void setExamName(String examName) {
        ExamName = examName;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ExamTypeId);
        dest.writeString(this.ExamName);
        dest.writeString(this.StartDate);
    }

    public ExamScheduleList() {
    }

    protected ExamScheduleList(Parcel in) {
        this.ExamTypeId = in.readString();
        this.ExamName = in.readString();
        this.StartDate = in.readString();
    }

    public static final Creator<ExamScheduleList> CREATOR = new Creator<ExamScheduleList>() {
        @Override
        public ExamScheduleList createFromParcel(Parcel source) {
            return new ExamScheduleList(source);
        }

        @Override
        public ExamScheduleList[] newArray(int size) {
            return new ExamScheduleList[size];
        }
    };
}
