package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.DiscussionDetailModel;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.domain.GetDiscussionDetailsList;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FontManage;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class CardviewDiscussionDetail extends RecyclerView.Adapter<CardviewDiscussionDetail.DataObjectHolder> {

    Context context;

    private int regId;
    private List<GetDiscussionDetailsList> mList = new ArrayList<>();

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        RelativeLayout rel1,rel2;
        CircleImageView mImageView1,mImageView2;
        TextView username1,username2,text1,text2;

        public DataObjectHolder(View itemView) {
            super(itemView);
            rel1 = itemView.findViewById(R.id.rel1);
            rel2 = itemView.findViewById(R.id.rel2);
            mImageView1 = itemView.findViewById(R.id.mImageView1);
            mImageView2 = itemView.findViewById(R.id.mImageView2);
            username1 = itemView.findViewById(R.id.username1);username2 = itemView.findViewById(R.id.username2);

            text1 = itemView.findViewById(R.id.text1);
            text2 = itemView.findViewById(R.id.text2);
        }
    }

    public CardviewDiscussionDetail(List<GetDiscussionDetailsList> mList, Context context) {

        this.context = context;
        this.mList = mList;
        regId= Integer.parseInt(String.valueOf(RegPrefManager.getInstance(context).getRegId()));
    }

    @Override
    public CardviewDiscussionDetail.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                        int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_discussion_reciever, parent, false);

        CardviewDiscussionDetail.DataObjectHolder dataObjectHolder = new CardviewDiscussionDetail.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final CardviewDiscussionDetail.DataObjectHolder holder, final int position) {



        Log.v("REGID",regId+":"+mList.get(position).getCreatedBy());

        if(regId!=mList.get(position).getCreatedBy()){
            holder.rel1.setVisibility(View.VISIBLE);
            holder.rel2.setVisibility(View.GONE);
            holder.text1.setText(mList.get(position).getDetailText());

            holder.username1.setText(mList.get(position).getCreatedName());

            holder.username1.setBackgroundResource(R.drawable.roundedcircle);

            if(mList.get(position).getPhotoURL()!=null&&!mList.get(position).getPhotoURL().equals("")){
                Picasso.with(context).load(Constants.SRV_URL+mList.get(position).getPhotoURL())
                        .placeholder(R.drawable.default_img)
                        .into(holder.mImageView1);


            }

            GradientDrawable drawableuser1 = (GradientDrawable) holder.username1.getBackground();
            drawableuser1.setColor(mList.get(position).getColorCode());
            holder.text1.setTextColor(mList.get(position).getColorCode());


            FontManage.setFontImpact(context,holder.username1);


        }else{
            holder.rel2.setVisibility(View.VISIBLE);
            holder.rel1.setVisibility(View.GONE);

            holder.text2.setText(mList.get(position).getDetailText());


            holder.username2.setText(mList.get(position).getCreatedName());


            holder.username2.setBackgroundResource(R.drawable.roundedcircle);

            if(mList.get(position).getPhotoURL()!=null&&!mList.get(position).getPhotoURL().equals("")){


                Picasso.with(context).load(Constants.SRV_URL+mList.get(position).getPhotoURL())
                        .placeholder(R.drawable.default_img)
                        .into(holder.mImageView2);
            }
            GradientDrawable drawableuser2 = (GradientDrawable) holder.username2.getBackground();
            drawableuser2.setColor(mList.get(position).getColorCode());


            holder.text2.setTextColor(mList.get(position).getColorCode());
            FontManage.setFontImpact(context,holder.username2);
        }







    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void setData(GetDiscussionDetailsList mMOdel){
        mList.add(0,mMOdel);
        notifyDataSetChanged();
    }

    public void setData(List<GetDiscussionDetailsList> list){
        mList.addAll(list);
        notifyDataSetChanged();
    }


}

