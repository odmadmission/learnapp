
package com.sseduventures.digichamps.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudyMaterialList {

    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("fileType")
    @Expose
    private String fileType;
    @SerializedName("fileURL")
    @Expose
    private String fileURL;
    @SerializedName("studyMaterial")
    @Expose
    private String studyMaterial;
    @SerializedName("topic")
    @Expose
    private String topic;
    @SerializedName("materialType")
    @Expose
    private String materialType;
    @SerializedName("subject")
    @Expose
    private String subject;

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }

    public String getStudyMaterial() {
        return studyMaterial;
    }

    public void setStudyMaterial(String studyMaterial) {
        this.studyMaterial = studyMaterial;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

}
