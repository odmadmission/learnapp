package com.sseduventures.digichamps.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sseduventures.digichamps.Model.DataModel_TicketList;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.TicketDetailsActivity;
import com.sseduventures.digichamps.domain.DoubtListData;

import java.util.List;
import java.util.StringTokenizer;

import jp.wasabeef.richeditor.RichEditor;



public class TicketListItem_Adapter extends RecyclerView.Adapter<TicketListItem_Adapter.MyTicketView> {


    private List<DoubtListData> ticketLists;
    CardView container, container1;
    Context context;
    public static int position;

    

    public static class MyTicketView extends RecyclerView.ViewHolder {

        TextView ticket_No, ticket_Status, ticket_Date;
        CardView container;
        RichEditor ticket_Sub;

        public MyTicketView(View view) {
            super(view);
            ticket_No = (TextView) view.findViewById(R.id.text_tic);
            ticket_Status = (TextView) view.findViewById(R.id.text_tic2);
            ticket_Sub = (RichEditor) view.findViewById(R.id.text_sub);
            ticket_Date = (TextView) view.findViewById(R.id.text_date);

            container = (CardView) view.findViewById(R.id.card_view_ticket);
            ticket_Sub.setClickable(false);
            ticket_Sub.setInputEnabled(false);
            ticket_Sub.setEnabled(true);
            ticket_Sub.loadCSS("* {" +
                    "   -webkit-user-select: none;" +
                    "}");


        }
    }

    public TicketListItem_Adapter(List<DoubtListData> ticketLists, Context context) {
        this.ticketLists = ticketLists;
        this.context = context;
    }

    @Override
    public MyTicketView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_list_item_activity, parent, false);

        MyTicketView myTicketView = new MyTicketView(view);
        return myTicketView;
    }

    @Override
    public void onBindViewHolder(final MyTicketView holder, final int listPosition) {
        position = listPosition;
        TextView ticket_no = holder.ticket_No;
        TextView ticket_status = holder.ticket_Status;
        RichEditor ticket_sub = holder.ticket_Sub;
        TextView ticket_date = holder.ticket_Date;
        
        
        String status = ticketLists.get(holder.getAdapterPosition()).getStatus();
        String statusNew = null;

        if(status.equalsIgnoreCase("O")){
            if(ticketLists.get(holder.getAdapterPosition()).isIs_Answred() == true){
                statusNew = "Answered";
            }else {
                statusNew = "Unanswered";
            }
        }
        else if(status.equalsIgnoreCase("R")){
            statusNew = "Rejected";
        }
        else if(status.equalsIgnoreCase("C")){
            statusNew = "Closed";
        }
        else if(status.equalsIgnoreCase("D")){
            statusNew = "Overdue";
        }

        String question = ticketLists.get(holder.getAdapterPosition()).getQuestion();
        String text_limit;
        if (question.length() > 100){
            text_limit = question.substring(0, 100)+ "...";
        }
        else {
            text_limit = question;
        }

        String createdDate = ticketLists.get(holder.getAdapterPosition()).getCreated_on();
        StringTokenizer tokens = new StringTokenizer(createdDate, "T");
        String date = tokens.nextToken();
        

        ticket_no.setText(ticketLists.get(holder.getAdapterPosition()).getTicket_No());
        ticket_status.setText(statusNew);
        ticket_sub.setHtml(text_limit);
        ticket_date.setText(date);
        this.container1 = container;

        holder.container.setOnClickListener(onClickListener(position));


        if (statusNew.equalsIgnoreCase("Answered")) {

            holder.ticket_Status.setTextColor(ContextCompat.getColor(context, R.color.close));


        }
         else if (statusNew.equalsIgnoreCase("Unanswered")) {
            holder.ticket_Status.setTextColor(ContextCompat.getColor(context, R.color.open));
        }

        else if (statusNew.equalsIgnoreCase("overdue")) {
            holder.ticket_Status.setTextColor(ContextCompat.getColor(context, R.color.overdue));
        }

         else if (statusNew.equalsIgnoreCase("Rejected")) {
            holder.ticket_Status.setTextColor(ContextCompat.getColor(context, R.color.reject));
        }

    }


    @Override
    public int getItemCount() {
        return ticketLists.size();
    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TicketDetailsActivity.class);
                intent.putExtra("ticket_id",ticketLists.get(position).getTicket_id()+"");
                intent.putExtra("activity","Ticket_list");
                Activity activity = (Activity) context;
                activity.startActivity(intent);
                activity.finish();

            }
        };

    }

}
