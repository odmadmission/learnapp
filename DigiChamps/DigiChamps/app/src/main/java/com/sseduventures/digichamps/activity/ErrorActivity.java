package com.sseduventures.digichamps.activity;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;
import android.net.ConnectivityManager;
import android.content.Context;
import android.net.NetworkInfo;

import com.sseduventures.digichamps.R;
import com.crashlytics.android.Crashlytics;
import com.sseduventures.digichamps.activities.FormActivity;

import io.fabric.sdk.android.Fabric;


public class ErrorActivity extends FormActivity {

    private Button retry_srv_err;
    Boolean isInternetPresent = false;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.server_error);

        retry_srv_err = (Button)findViewById(R.id.retry_srv_err);

        retry_srv_err.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
            // TODO Auto-generated method stub
            ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                NetworkInfo netInfos = connectivityManager.getActiveNetworkInfo();
                if(netInfos != null) {
                    if (netInfos.isConnected()) {
                        isInternetPresent = true;
                        if (isInternetPresent) {
                            back();
                        }
                    }
                }else {
                    startActivity(new Intent(ErrorActivity.this, Internet_Activity.class));
                }
            }
            }
        });
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.alert_warning)
                .setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", null).show();
    }
    public void back(){
        finish();
    }

}