package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/6/2018.
 */

public class DoubtListSuccess implements Parcelable{

    private List<DoubtListData> doubt_list;


    public List<DoubtListData> getDoubt_list() {
        return doubt_list;
    }

    public void setDoubt_list(List<DoubtListData> doubt_list) {
        this.doubt_list = doubt_list;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.doubt_list);
    }

    public DoubtListSuccess() {
    }

    protected DoubtListSuccess(Parcel in) {
        this.doubt_list = in.createTypedArrayList(DoubtListData.CREATOR);
    }

    public static final Creator<DoubtListSuccess> CREATOR = new Creator<DoubtListSuccess>() {
        @Override
        public DoubtListSuccess createFromParcel(Parcel source) {
            return new DoubtListSuccess(source);
        }

        @Override
        public DoubtListSuccess[] newArray(int size) {
            return new DoubtListSuccess[size];
        }
    };
}
