
package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class GetExamResponse implements Parcelable
{

private Success success;

    protected GetExamResponse(Parcel in) {
        success = in.readParcelable(Success.class.getClassLoader());
    }

    public static final Creator<GetExamResponse> CREATOR = new Creator<GetExamResponse>() {
        @Override
        public GetExamResponse createFromParcel(Parcel in) {
            return new GetExamResponse(in);
        }

        @Override
        public GetExamResponse[] newArray(int size) {
            return new GetExamResponse[size];
        }
    };

    public Success getSuccess() {
        return success;
    }

    public void setSuccess(Success success) {
        this.success = success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(success, flags);
    }


}