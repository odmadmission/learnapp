package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by NISHIKANT on 6/28/2018.
 */

public class DictonarySuccess implements Parcelable {

private ArrayList<DictonaryListData> list;

    public ArrayList<DictonaryListData> getList() {
        return list;
    }

    public void setList(ArrayList<DictonaryListData> list) {
        this.list = list;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.list);
    }

    public DictonarySuccess() {
    }

    protected DictonarySuccess(Parcel in) {
        this.list = in.createTypedArrayList(DictonaryListData.CREATOR);
    }

    public static final Creator<DictonarySuccess> CREATOR = new Creator<DictonarySuccess>() {
        @Override
        public DictonarySuccess createFromParcel(Parcel source) {
            return new DictonarySuccess(source);
        }

        @Override
        public DictonarySuccess[] newArray(int size) {
            return new DictonarySuccess[size];
        }
    };
}
