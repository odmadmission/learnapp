package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/27/2018.
 */

public class CoinLeaderboardResponse implements Parcelable {


    private String name;
    private String className;
    private String coins;
    private String imageUrl;
    private long regdId;

    public long getRegdId() {
        return regdId;
    }

    public void setRegdId(long regdId) {
        this.regdId = regdId;
    }

    protected CoinLeaderboardResponse(Parcel in) {
        name = in.readString();
        className = in.readString();
        coins = in.readString();
        imageUrl = in.readString();
    }

    public static final Creator<CoinLeaderboardResponse> CREATOR = new Creator<CoinLeaderboardResponse>() {
        @Override
        public CoinLeaderboardResponse createFromParcel(Parcel in) {
            return new CoinLeaderboardResponse(in);
        }

        @Override
        public CoinLeaderboardResponse[] newArray(int size) {
            return new CoinLeaderboardResponse[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCoins() {
        return coins;
    }

    public void setCoins(String coins) {
        this.coins = coins;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(className);
        parcel.writeString(coins);
        parcel.writeString(imageUrl);
    }


}
