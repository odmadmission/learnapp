package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/3/2018.
 */

public class PrvYrPaperListData implements Parcelable{

    private int PreviousYearPaper_ID;
    private String PreviousYearPaper_PDFName;
    private String PDF_UploadPath;
    private String PDFOnline;
    private String PDFBeta;


    public int getPreviousYearPaper_ID() {
        return PreviousYearPaper_ID;
    }

    public void setPreviousYearPaper_ID(int previousYearPaper_ID) {
        PreviousYearPaper_ID = previousYearPaper_ID;
    }

    public String getPreviousYearPaper_PDFName() {
        return PreviousYearPaper_PDFName;
    }

    public void setPreviousYearPaper_PDFName(String previousYearPaper_PDFName) {
        PreviousYearPaper_PDFName = previousYearPaper_PDFName;
    }

    public String getPDF_UploadPath() {
        return PDF_UploadPath;
    }

    public void setPDF_UploadPath(String PDF_UploadPath) {
        this.PDF_UploadPath = PDF_UploadPath;
    }

    public String getPDFOnline() {
        return PDFOnline;
    }

    public void setPDFOnline(String PDFOnline) {
        this.PDFOnline = PDFOnline;
    }

    public String getPDFBeta() {
        return PDFBeta;
    }

    public void setPDFBeta(String PDFBeta) {
        this.PDFBeta = PDFBeta;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.PreviousYearPaper_ID);
        dest.writeString(this.PreviousYearPaper_PDFName);
        dest.writeString(this.PDF_UploadPath);
        dest.writeString(this.PDFOnline);
        dest.writeString(this.PDFBeta);
    }

    public PrvYrPaperListData() {
    }

    protected PrvYrPaperListData(Parcel in) {
        this.PreviousYearPaper_ID = in.readInt();
        this.PreviousYearPaper_PDFName = in.readString();
        this.PDF_UploadPath = in.readString();
        this.PDFOnline = in.readString();
        this.PDFBeta = in.readString();
    }

    public static final Creator<PrvYrPaperListData> CREATOR = new Creator<PrvYrPaperListData>() {
        @Override
        public PrvYrPaperListData createFromParcel(Parcel source) {
            return new PrvYrPaperListData(source);
        }

        @Override
        public PrvYrPaperListData[] newArray(int size) {
            return new PrvYrPaperListData[size];
        }
    };
}
