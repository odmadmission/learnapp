package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/20/2018.
 */

public class NewLearnBannerList implements Parcelable {

     private int banner_Id;
     private String Image_URL;
     private String Image_Title;
     private String Banner_Description;
     private String BannerOnline;
     private String BannerBeta;

    public int getBanner_Id() {
        return banner_Id;
    }

    public void setBanner_Id(int banner_Id) {
        this.banner_Id = banner_Id;
    }

    public String getImage_URL() {
        return Image_URL;
    }

    public void setImage_URL(String image_URL) {
        Image_URL = image_URL;
    }

    public String getImage_Title() {
        return Image_Title;
    }

    public void setImage_Title(String image_Title) {
        Image_Title = image_Title;
    }

    public String getBanner_Description() {
        return Banner_Description;
    }

    public void setBanner_Description(String banner_Description) {
        Banner_Description = banner_Description;
    }

    public String getBannerOnline() {
        return BannerOnline;
    }

    public void setBannerOnline(String bannerOnline) {
        BannerOnline = bannerOnline;
    }

    public String getBannerBeta() {
        return BannerBeta;
    }

    public void setBannerBeta(String bannerBeta) {
        BannerBeta = bannerBeta;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.banner_Id);
        dest.writeString(this.Image_URL);
        dest.writeString(this.Image_Title);
        dest.writeString(this.Banner_Description);
        dest.writeString(this.BannerOnline);
        dest.writeString(this.BannerBeta);
    }

    public NewLearnBannerList() {
    }

    protected NewLearnBannerList(Parcel in) {
        this.banner_Id = in.readInt();
        this.Image_URL = in.readString();
        this.Image_Title = in.readString();
        this.Banner_Description = in.readString();
        this.BannerOnline = in.readString();
        this.BannerBeta = in.readString();
    }

    public static final Creator<NewLearnBannerList> CREATOR = new Creator<NewLearnBannerList>() {
        @Override
        public NewLearnBannerList createFromParcel(Parcel source) {
            return new NewLearnBannerList(source);
        }

        @Override
        public NewLearnBannerList[] newArray(int size) {
            return new NewLearnBannerList[size];
        }
    };
}
