package com.sseduventures.digichamps.firebase;

import android.util.Log;

import com.firebase.client.Firebase;
import com.freshchat.consumer.sdk.Freshchat;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.sseduventures.digichamps.helper.SharedPrefManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User25 on 10/11/2017.
 */

public class AppFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private  final static String TAG="FCM Token";
    protected Firebase ref;
    String uniqueId;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
       // Log.d(TAG, "Refreshed token: " + refreshedToken);

        //String token = FirebaseInstanceId.getInstance().getToken();
        Freshchat.getInstance(this).setPushRegistrationToken(refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }
    private void sendRegistrationToServer(String token){
        // TODO: Implement this method to send any registration to app's server.

        //saving the token on shared preferences
        SharedPrefManager.getInstance(getApplicationContext()).saveDeviceToken(token);
       // registerDevice();
    }

    // [END refresh_token]

    private void registerDevice() {

        //Creating a firebase object
           /* Firebase firebase = new Firebase("https://abc-notification.firebaseio.com");*/
        try {
            String firebaseUrl = "https://digichamps-f36d1.firebaseio.com";
            ref = new Firebase(firebaseUrl);
            //Pushing a new element to firebase it will automatically create a unique id
            Firebase newFirebase = ref.push();

            //Creating a map to store name value pair
            Map<String, String> val = new HashMap<>();

            //pushing msg = none in the map
            val.put("msg", "none");

            //saving the map to firebase
            newFirebase.setValue(val);

            //Getting the unique id generated at firebase
            uniqueId = newFirebase.getKey();
            Log.d(TAG, uniqueId);
            //Finally we need to implement a method to store this unique id to our server
            //sendTokenToServer(uniqueId);
        } catch (Exception e) {

        }

    }
}

