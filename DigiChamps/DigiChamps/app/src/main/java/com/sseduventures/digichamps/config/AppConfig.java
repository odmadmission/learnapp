package com.sseduventures.digichamps.config;

public class AppConfig {
    //
    public static String terms = "Privacy Policy\n" +
            "\n" +
            "We value your trust. In order to honour that trust, DIGICHAMPS adheres to ethical standards in gathering, using, and safeguarding any information provided by you.\n" +
            "\n" +
            "DIGICHAMPS, an initiative of SS Eduventures Pvt. Ltd. is a leading Ed-tech company, incorporated in India, for imparting learning and introducing new enhanced learning techniques to ensure the academic growth of the students. \n" +
            " \n" +
            "This privacy policy governs your use of the DIGICHAMPS application and the other associated applications and sites managed by SS Eduventures Pvt. Ltd. \n" +
            "\n" +
            "User Provided Information\n" +
            "\n" +
            "The Application obtains the information you provide when you download and register the Application or when you sign up on the website. When you register with us and use the Application, you generally provide (a) your name, email address, phone number, password, personal bio and your educational interests; (b) transaction-related information, such as when you make purchases, respond to any offers, or download or use applications from us; (c) information you provide us when you contact us for help; (d) information you enter into our system when using the Application, such as while asking doubts, participating in discussions, taking tests or discussing with our counsellors. \n" +
            "\n" +
            "We may use the information you provided us to contact you from time to time to provide you with important information, required notices and marketing promotions. We will ask you when we need more information that personally identifies you (personal information) or allows us to contact you.\n" +
            "\n" +
            "Automatically Collected Information\n" +
            "\n" +
            "In addition, the Application may collect certain information automatically, including, but not limited to, the type of mobile device you use, your mobile devices unique device ID, the IP address of your device, your operating system, the type of Internet browsers you use, and information about the way you use the Application. As is true of most Mobile applications, we also collect other relevant information as per the permissions that you provide.\n" +
            "\n" +
            "We use an outside payment processing company to bill you for goods and services. These companies do not retain, share, store or use personally identifiable information for any other purpose.\n" +
            "\n" +
            "We will retain User Provided data for as long as you use the Application and for a reasonable time thereafter. We will retain Automatically Collected information for up to a period of time and thereafter may store it in aggregate. \n" +
            "\n" +
            "Use of your Personal Information\n" +
            "\n" +
            "We use the collected information to analyse trends, to conduct research, to administer the application, to learn about user's learning patterns and movements around the application and to gather demographic information and usage behaviour about our user base as a whole. Aggregated and individual, anonymized and non-anonymized data may periodically be transmitted to external services to help us improve the Application and our service. We will share your information with third parties only in the ways that are described below in this privacy statement.\n" +
            "\n" +
            "We may use the individual data and behaviour patterns combined with personal information to provide you with personalized content, and better your learning objectives. Third parties provide certain services which we may use to analyze the data and information to personalize, drive insights and help us better your experience or reach out to you with more value added applications, products, information and services. However, these third party companies do not have any independent right to share this information.\n" +
            "\n" +
            "DIGICHAMPS will occasionally email notices or contact you to communicate about our services and benefits, as they are considered an essential part of the service you have chosen.\n" +
            "\n" +
            "We may disclose ‘User provided and Automatically Collected’ Information:\n" +
            "As required by law, such as to comply with a subpoena, or similar legal process;\n" +
            "\n" +
            "When we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request;\n" +
            "\n" +
            "With our trusted services providers who work on our behalf, do not have an independent use of the information we disclose to them, and have agreed to adhere to the rules set forth in this privacy statement;\n" +
            "\n" +
            "With third party service providers in order to personalize the for a better user experience and to perform behavioural analysis\n" +
            "\n" +
            "if DIGICHAMPS or SS Eduventures Pvt. Ltd. is involved in a merger, acquisition, or sale of all or a portion of its assets, you will be notified via email and/or a prominent notice on our Website or other channels of any change in ownership or uses of this information, as well as any choices you may have regarding this information.\n" +
            "\n" +
            "Access to your Personal Information\n" +
            "We will provide you with the means to ensure that your personal information is correct and current. If you have filled out a user profile, we will provide an obvious way for you to access and change your profile from our application. \n" +
            "\n" +
            "For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to Contacts , Device Information ,Internet , Read SMS, Connection State , Write External Storage . The information that we request is will be retained by us and used as described in this privacy policy.The app does use third party services that may collect information used to identify you. Link to privacy policy of third party service providers used by the app"+
            "Security\n" +
            "We are concerned about safeguarding the confidentiality of your information. We provide physical, electronic, and procedural safeguards to protect information we process and maintain. For example, we limit access to this information to authorized employees only who need to know that information in order to operate, develop or improve our Application. Please be aware that, although we endeavour to provide reasonable security for information we process and maintain, no security system can prevent all potential security breaches in which case SS Eduventures Pvt. Ltd. won’t be solely responsible. \n" +
            "\n" +
            "Changes to this Statement\n" +
            "As DIGICHAMPS evolves, our privacy policy will need to evolve as well to cover new situations.  You are advised to consult this Privacy Policy regularly for any changes, as continued use is deemed approval of all changes.\n" +
            "\n" +
            "Your Consent\n" +
            "By using the Application, you are consenting to our processing of your information as set forth in this Privacy Policy now and as amended by us. \"Processing,” means using or touching information in any way, including, but not limited to, collecting, storing, deleting, using, combining and disclosing information, all of which activities will take place in India. If you reside outside India your information will be transferred, processed and stored there under Indian privacy standards.\n" +
            "\n" +
            "Reach out to us on info@thedigichamps.com, in case of any queries.\n" +
            "\n" +
            "\n" +
            "Terms & Conditions\n" +
            "\n" +
            "1. All information, content and material, including the software, text, images, graphics, video and audio, contained in the SD Card and the products and services are copyrighted property of SS Eduventures Pvt. Ltd. All trademarks, services marks, trade names, and trade dress are proprietary to SS Eduventures Pvt. Ltd. You understand that \"DIGICHAMPS” is a registered trademark owned by SS Eduventures Pvt. Ltd. This trademark may not be used in any manner without prior written consent of SS Eduventures Pvt. Ltd. No information, content or material from our products and services may be copied, downloaded, reproduced, modified, republished, uploaded, posted, transmitted or distributed in any way without obtaining prior written permission from SS Eduventures Pvt. Ltd. and nothing on this SD card shall be deemed to confer a license of or any other right, interest or title to or in any of the intellectual property rights belonging to SS Eduventures Pvt. Ltd., to the User. You may own the medium on which the information, content or materials resides, but SS Eduventures Pvt. Ltd. retain full and complete title to the information, content or materials and all intellectual property rights therein.\n" +
            "\n" +
            "2. Your use of our products and services provided for herein is solely for your personal and non-commercial use. Any use of this SD Card or its content other than for personal purposes is prohibited. Your personal and non-commercial use of this SD Card and / or our services shall be subjected to the following restrictions:\n" +
            "\n" +
            "i) You may not decompile, reverse engineer, or disassemble the contents of the SD Card and / or our products, or (c) remove any copyright, trademark registration, or other proprietary notices from the contents of the SD Card and / or our products.\n" +
            "\n" +
            "ii) You will not (a) use our product or service for commercial purposes of any kind, or (b) advertise or sell any products, services or otherwise (whether or not for profit), or solicit others (including, without limitation, solicitations for contributions or donations) or use any public forum for commercial purposes of any kind, or (c) use the SD Card and / or our products and services in any way that is unlawful, or harms SS Eduventures Pvt. Ltd. or any other person or entity as determined by SS Eduventures Pvt. Ltd..\n" +
            "\n" +
            "3. SS Eduventures Pvt. Ltd. products and / or services, including the SS Eduventures Pvt. Ltd. application and content, are compatible with majority of the tablets/PCs/Mobile devices. However, SS Eduventures Pvt. Ltd.. shall not be obligated to provide workable products and / or services for any tablets/PCs/Mobile devices including the one recognised by us. The user should understand the tablet provided is directly procured from the company and for any issue with the tablet, the user should directly contact the company. However, as a gesture the company may lend its support in case any issue arises with the tablet provided by the company.  \n" +
            "\n" +
            "4. SS Eduventures Pvt. Ltd. or any of its subsidiaries or content partners shall have no responsibility for any loss or damage caused to tablet or any other hardware and / or software, including loss of data or effect on the processing speed of the tablet, those results from the use of our products and services. \n" +
            "\n" +
            "i) In no event will SS Eduventures Pvt. Ltd.. be liable for damages of any kind, including without limitation, (1) indirect, punitive, special, exemplary, incidental, or consequential damage (including loss of business, revenue, profits, use, data or other economic advantage, loss of programs or information), or (2) direct damages in excess of the amount paid to SS Eduventures Pvt. Ltd. for the product and / or service, arising out of the use of, misuse of or inability to use, or errors, omissions or other inaccuracies in the Site or the products and / or services, or any information provided on the Site. Some jurisdictions do not allow the limitation or exclusion of liability. Accordingly, in such instances, some of the above limitations may not apply to the User\n" +
            "\n" +
            "5. You have to specify the address to which the shipment has to be made at the time of purchase. All product(s) shall be delivered directly to the address as specified at the point of ordering [and you cannot, under no circumstances whatsoever, change the address after the order is processed. Any inconsistencies in name or address will result in non-delivery of the product(s).\n" +
            "\n" +
            "6. (a) For return of product(s) damaged at the time of delivery, the shipping charges shall be borne by SS Eduventures Pvt. Ltd. However, for return any of the product(s) for any other reasons, it shall be the responsibility of the registered User to arrange for the return of such cancelled product(s) and the shipping charges shall be borne by such User. (b)We request you not to accept any product package that seems to be tampered with, opened or damaged at the time of delivery. The products must be returned in the same condition / unopened as delivered by SS Eduventures Pvt. Ltd.. Any products returned showing signs of any use or damage in any manner shall not be accepted for return. (c)All requests for return of damaged products (due to damage during the transit/damaged at the time of delivery) have to be placed within 2 days from the date of delivery. Please note that no refunds shall be claimed or will be entertained post 2 days from the date of delivery.\n" +
            "\n" +
            "7. SS Eduventures Pvt. Ltd. may also contact the user through SMS, email and call to give notifications on various important updates. Therefore, User holds SS Eduventures Pvt. Ltd. non liable to any liabilities including financial penalties, damages, expenses in case the users mobile number is registered with Do not Call (DNC) database.\n" +
            "\n" +
            "8. You hereby indemnify, defend, and hold SS Eduventures Pvt. Ltd., SS Eduventures Pvt. Ltd.'s distributors, agents, representatives and other authorized users, and each of the foregoing entities\" respective resellers, distributors, service providers and suppliers, and all of the foregoing entities' respective officers, directors, owners, employees, agents, representatives, harmless from and against any and all losses, damages, liabilities and costs arising from your use of our product.\n" +
            "\n" +
            "9. In the event of breach of the terms and conditions of this Terms of Use by the User, the User shall be promptly liable to indemnify SS Eduventures Pvt. Ltd. for all the costs, losses and damages caused to SS Eduventures Pvt. Ltd. as a result of such a breach.\n" +
            "\n" +
            "10. In the event of your breach of this Terms of Use, you agree that SS Eduventures Pvt. Ltd. will be irreparably harmed and will not have an adequate remedy in money or damages. SS Eduventures Pvt. Ltd. therefore, shall be entitled in such event to obtain an injunction against such a breach from any court of competent jurisdiction immediately upon request. SS Eduventures Pvt. Ltd.’s right to obtain such relief shall not limit its right to obtain other remedies.\n" +
            "\n" +
            "11. SS Eduventures would never spy or collect personal information that could compromise the privacy of the user. SS Eduventures Pvt. Ltd. would install certificates on your device to secure its content and Intellectual Properties. In some Andriod devices there could be a warning or notification of these certificates as a possible spy on network. The certificate is issued to the local host and used for securing our Intellectual Properties. SS Eduventures or any of its partners do not have any means to spy or monitor your activities on your network. The root certificate can be requested from info@DIGICHAMPS.com for any validation if the user needs. \n" +
            "\n" +
            "12. Persons who are \"competent/capable\" of contracting within the meaning of the Indian Contract Act, 1872 shall be eligible to use our products or services. Persons who are minors, un-discharged insolvents etc. are not eligible to use our products or services. As a minor if you wish to use our products or services, such use shall be made available to you by your legal guardian or parents. SS Eduventures Pvt. Ltd. will not be responsible for any consequence that arises as a result of misuse of any kind of our products or services that may occur by virtue of any person including a minor using the services provided. By using the products or services you warrant that all the data provided by you is accurate and complete and that you have obtained the consent of parent/legal guardian(in case of minors). SS Eduventures Pvt. Ltd. reserves the right to terminate your subscription and / or refuse to provide you with access to the products or services if it is discovered that you are under the age of 18 years and the consent to use the products or services is not made by your parent/legal guardian or any information provided by you is inaccurate.\n" +
            "\n" +
            "13. We may disclose to third party services certain personally identifiable information listed below:\n" +
            "\n" +
            "•\tInformation you provide us such as name, email, mobile phone number\n" +
            "\n" +
            "•\tInformation we collect as you access and use our service, including device information and network carrier\n" +
            "\n" +
            "14. This information is shared with third party service providers so that we can:\n" +
            "\n" +
            "•\tPersonalize the app for you\n" +
            "\n" +
            "•\tPerform behavioural analytics\n";

    public static String SALT = "hhhpte1L9R";
    public static String KEY = "ZbKwHjmu";
    public static String MERCHANT_ID = "e5iIg1jwi8";
    public static String SURL = "https://thedigichamps.com/Cart/Order_Confirmation/";
    public static String FURL = "https://thedigichamps.com/Cart/Cancel_Order";
    // Authorization key
    public static String Auth_Key = "bearer nZURcdgCAdvIDg24xBVhJ9GquhejbpKdXf74zt4qf1_9XFZ1FUqmk__ryZs9cw620g-wVjYulYtHaGlw7jNOPsDj6COCy0fiaoDVqAMMeliMAWIPrIpeneeKShnrT7qw0ogHjaaxL7k2lCMst5aWkE8Iw6F1a5dWX8h60JhaOiWEvANCDPm2EE-iE72QmQJ4mbgNO09I07x3BdIXEytT_AkS1jwBLg0u5RU325mnRYv1cyDGx52tXhQZ5SjS_g6BdaZeYRs5jrJCiIbcde2GBF3Wx_qpBOf7LHfcDspDILgkwIBGKY66S1PrJte-K_hs7G4LKgQIuwuGgmjOqJjWQA";

    // server base url production

    public static String SRVR_URL = "https://thedigichamps.com/";
    // api base url production
    public static String BASE_URL = "https://thedigichamps.com/api/";

    //Login url
    public static String LOGIN = BASE_URL + "Login/login";
    //Signup url
    public static String SIGNUP = BASE_URL + "Registration/register";
    //Signup OTP
    public static String SIGNUP_OTP = BASE_URL + "Otp/otp_confirm";

    //Forgot Password url
    public static String FORGOT_PASSWORD_URL = BASE_URL + "ForgotPassword/password_change";
    // Forgot password OTP
    public static String FORGOT_PASSWORD_OTP_URL = BASE_URL + "Otp/otp_confirm";
    // RESEND OTP
    public static String RESEND_OTP = BASE_URL + "Registration/resend_otp?id=";
    // Orders url
    public static String ORDERS_CONFIRMATION_URL = BASE_URL + "Cart/Order_Confirmation/";
    // Orders url
    public static String ORDERS_URL = BASE_URL + "Orderdetails/myorders/";
    // Forgot password OTP
    public static String ORDERS_DETAILS_URL = BASE_URL + "Orderdetails/orderdetails/";
    // Security question get
    public static String GET_SECURITY_QUES = BASE_URL + "Registrationdetail/getquestion";
    //Class  url
    public static String CLASS = BASE_URL + "academic/GetClass?id=";
    //Class and board post  url
    public static String BOARD = BASE_URL + "academic/GetBoard";
    //Class and board post  url
    public static String POST_BOARD = BASE_URL + "Registrationdetail/registration_detail";
    //Profile image update  url
    public static String IMAGE_UPDATE = BASE_URL + "EditProfile/PostUserImage";
    //Profile details  url
    public static String PROFILE_DETAILS = BASE_URL + "EditProfile/profile/";
    //Get Profile details  url
    public static String GET_PROFILE_DETAILS = BASE_URL + "EditProfile/Getprofile/";
    //Profile details  url
    public static String GET_PACKAGES = BASE_URL + "Academic/GetPackage/";
    //Profile details  url
    public static String PACKAGE_DETAILS = BASE_URL + "Academic/GetPackageDetail/";
    //Change password url
    public static String CHANGE_PASSWORD_URL = BASE_URL + "EditProfile/Changepassword";
    //Dashboard url
    public static String DASHBOARD = BASE_URL + "dashboard/userdrawer/";
    //Cart detail url
    public static String CART_DATA = BASE_URL + "Cart/cartdetail/";

    //Add to Cart url
    public static String GET_CHECKOUT = BASE_URL + "Cart/checkout/";
    //Add to Cart url
    public static String APPLY_COUPON = BASE_URL + "Cart/checkout/";
    //Add to Cart url
    public static String DELETE_CART_ITEM = BASE_URL + "Cart/RemoveCart";
    //Add to Cart url
    public static String ADD_TO_CART = BASE_URL + "Cart/addtocart";
    //Chapter detail url
    public static String CHAPTER_DETAIL = BASE_URL + "Learn/LearnChapterdetails/";
    //Learn GET url
    public static String  GET_DASHBOARD = BASE_URL + "Learn/GetDashboard";
    //Learn GET url
    public static String LEARN_DETAILS = BASE_URL + "Learn/learnSubjectdetails/";
    //Edit profile url
    public static String EDIT_PROFILE_URL = BASE_URL + "EditProfile/postprofile";
    //Leader Board url
    public static String GET_LEADERBOARD_URL = BASE_URL + "Exam/LeaderBoards/";
    //GET EXAM DATA
    public static String GET_EXAM_DATA = BASE_URL + "Exam/Get_Questions/";
    //POST EXAM DATA
    public static String POST_EXAM_DATA =BASE_URL + "Exam/Submit_Test";

    // Get Ticket List URL
    public static String TICKETS_URL = BASE_URL + "Doubt/Get_All_Doubts/";

    // Get Doubt class list
    public static String DOUBT_CLASSES = BASE_URL + "Doubt/Classs/";
    // Get Doubt subject list
    public static String DOUBT_SUBJECTS = BASE_URL + "Doubt/Subject/";
    // Get Doubt chapter list
    public static String DOUBT_CHAPTERS = BASE_URL + "Doubt/Chapter/";
    // ASK Doubt form submit
    public static String DOUBT_FORM_SUBMIT = BASE_URL + "Doubt/AskDoubt";
    // Doubt REPLY
    public static String TICKET_REPLY = BASE_URL + "Doubt/Answer_Rply";
    //Ticket details url
    public static String GET_TICKET_DETAILS_URL = BASE_URL + "Doubt/Get_Doubt_Details/";

    //New Ticket details url
    public static String GET_TICKET_DETAILS_URL_NEW = BASE_URL + "Doubt/Get_Doubt_Details/";
    // Get Result
    public static String RESULT_HIGHLIGHT = BASE_URL + "Exam/Test_Result/";
    // GET Security Ques
    public static String SESSION_SECURITY_QUES = BASE_URL + "SecurityQustion/Get_security/";
    //Post Security Ans
    public static String SECURITY_ANS_URL = BASE_URL + "SecurityQustion/Security_question";
    //GET Forgot Security question
    public static String FORGOT_SECURITY_QUESTION_URL = BASE_URL + "SecurityQustion/Security_otpsend?Regid=";
    //POST Security question OTP
    public static String OTP_SECURITY_QUESTION_URL = BASE_URL + "SecurityQustion/security_confirm";
    //Check login status
    public static String STATUS_LOGIN = BASE_URL + "Login/Loginstatus/";
    //POST for logout session
    public static String STATUS_LOGOUT = BASE_URL + "Login/logout/";
    //Question_PDF URL
    public static String Question_PDF_URL = SRVR_URL + "Module/Question_PDF/";
    // get pdf
    public static String PDF_URL = SRVR_URL + "Module/PDF/";
    //Get for special exam
    public static String SPECIAL_EXAM_URL = BASE_URL + "editprofile/globaltest/";
    //Get package filter
    public static String GET_PACKAGE_FILTER = BASE_URL + "Academic/GetPackage/";
    // GET ALL EXAM
    public static String GET_EXAM_LIST_ONLINE = BASE_URL + "Exam/Get_Exam/";
    // GET ALL EXAM OFFLINE  GET_EXAM_PRE_TEST
    public static String GET_EXAM_LIST_OFFLINE = BASE_URL + "Exam/Get_Exam_Offline/";
    // GET ALL EXAM OFFLINE
    public static String GET_EXAM_PRE_TEST = BASE_URL + "Exam/Get_PRT/";
    // GET Analytic
    public static String GET_AnalyticDetails = BASE_URL + "dashboard/Getdashboard/";
    // GET RESEND_EMAIL
    public static String RESEND_EMAIL = BASE_URL + "Cart/OrderResendMail/";

    // POST FEEDBACK
    public static String SEND_FEEDBACK = BASE_URL + "feedback/Addfeedback";

    // contact us
    public static String CONTACT_US = BASE_URL + "Contact/Contact_Us";
    // GET FAQ
    public static String GET_FAQ = BASE_URL + "FAQ/GetFAQ";
    // GET PREV YEARS PAPERS
    public static String GET_PREV_YR_PAPERS = BASE_URL + "PreviousYearPaper/GetPreviousYearPaper/";
    // GET PREV YEARS PAPERS
    public static String GET_NCERT_PAPERS =BASE_URL + "NCERTSolutions/GetNCERTSolutions/";
    // GET PREV YEARS PAPERS
    public static String GET_OLYMPIADS = BASE_URL + "CompetitiveExamQs/GetCompetitiveExamQs/";
    //BOOKMARKED_VIDEO url
    public static String GET_BOOKMARKED_VIDEO = BASE_URL + "Learn/GetBookmarkListVideo/";
   // UNBOOKMARK_VIDEO
   public static String UNBOOKMARK_VIDEO = BASE_URL + "Learn/SetBookmark";
    //  GET_BOOKMARKED_QB
    public static String GET_BOOKMARKED_QB = BASE_URL + "Learn/GetBookmarkListQusetion/";
    //  GET_BOOKMARKED_SN
    public static String GET_BOOKMARKED_SN =   BASE_URL + "Learn/GetBookmarkListStudynote/";
    //TO MAINTAIN VIDEO LOG FOR RECENT WATCHED
    public static String VIDEO_LOG = BASE_URL + "Learn/SetvideoLog";
    //Dictonary url new
    public static String DICTONARY = BASE_URL + "Dictionary/GetDictionary";

    //Feedback url new
    public static String FEEDBACK = BASE_URL + "feedback/addfeedback";


    //Book UnBook mark api
    public static String BOOKMARK = BASE_URL + "Learn/SetBookMark";

    //DIY video details
    public static String DIYVIDEODETAILS = BASE_URL + "DIYVideo/GetDIYVideo";
    //DIY video details

    public static String FEED = BASE_URL + "Feed/GetFeed";

    //Personal mentor
    public static String PER_MENTOR = BASE_URL + "Mentor/GetStudentTask/";

    //Beta chapter details
    public static String CHAPTER_DETAIL_BETA = BASE_URL + "Learn/LearnChapterdetails/";

    //Beta chapter details
    public static String Review_Ques = BASE_URL + "Exam/QuestionReview/";

    //Edit profile url
    public static String SAVE_REPORT_URL = BASE_URL + "Report/SaveReportModules";

    public static String AnalyticsData = BASE_URL + "Analitics/GetPersonalizedUserAnalytics?";


    public static String GET_ANALYTICS_DETAIL = BASE_URL + "Analitics/GetSubConceptAnalytics?regId=";



    public static String SubConceptAnalysis = BASE_URL + "Analitics/GetWeakSubConceptAnalytics?regId=";


   // TESTIMONIALS
    public static String TESTIMONIALS = BASE_URL + "Testimonial/GetTestimonial";

}