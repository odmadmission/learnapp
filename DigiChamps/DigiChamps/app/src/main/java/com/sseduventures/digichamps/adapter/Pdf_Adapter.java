package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.Model.DataModel_Cart;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.config.AppConfig;

import java.util.ArrayList;
import java.util.List;


public class Pdf_Adapter  extends RecyclerView.Adapter<Pdf_Adapter.MyViewHolder>{

    public static ArrayList<String> dataSet = new ArrayList<>();
    public static ArrayList<String> dataName = new ArrayList<>();
    public static int position;
    static CardView container, container1;
    public static RelativeLayout relative;
    public ArrayList<DataModel_Cart> selected_usersList = new ArrayList<>();
    Context mContext;
    View view;
    String api;


    private Context context;
    private List<DataModel_Cart> dataModelActivities;


    public Pdf_Adapter(ArrayList<String> data,ArrayList<String> data_name, String api, Context context) {
        this.context = context;
        this.dataSet = data;
        this.dataName = data_name;
        this.api = api;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_pdfs, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        position = listPosition;
        TextView pdfs = holder.pdfs;


        pdfs.setText(dataName.get(holder.getAdapterPosition()));

        SharedPreferences preference = context.getApplicationContext().getSharedPreferences("user_data", context.getApplicationContext().MODE_PRIVATE);
        String flag = preference.getString("flag", null);

        if (flag.equalsIgnoreCase("#3949ab")) {


        }else if(flag.equalsIgnoreCase("#ffb74d")){
            holder.left_drawable.setBackgroundResource(R.drawable.card_orange_left);
            holder.right_drawable.setBackgroundResource(R.drawable.card_orange_right);
        }


        container = holder.container;
        holder.container.setOnClickListener(onClickListener(position,holder));

        if(selected_usersList.contains(dataName.get(position)))
            holder.container.setBackgroundColor(ContextCompat.getColor(context, R.color.list_item_selected_state));
        else
            holder.container.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
    }

    
    @Override
    public int getItemCount() {
        return dataName.size();
    }

    private View.OnClickListener onClickListener(final int position, final MyViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(api.equalsIgnoreCase("pdf")){
                    String PDF_URL= AppConfig.PDF_URL+dataSet.get(position);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(PDF_URL));
                    context.startActivity(browserIntent);
                }else{
                    String PDF_URL= AppConfig.Question_PDF_URL+dataSet.get(position);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(PDF_URL));
                    context.startActivity(browserIntent);
                }

            }

        };
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView feature, val, sub, type, details;
        public LinearLayout linear_pack;
        CardView container;
        TextView pdfs;
        ImageView left_drawable,right_drawable;

        public MyViewHolder(View itemView) {
            super(itemView);
            container = (CardView) itemView.findViewById(R.id.card_view_pdf);
            this.pdfs = (TextView) itemView.findViewById(R.id.tv_pdf);

        }

    }



}