package com.sseduventures.digichamps.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sseduventures.digichamps.Model.LeaderBoardList;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.adapter.CardviewLeader;
import com.sseduventures.digichamps.adapter.CardviewTopperWay;
import com.sseduventures.digichamps.domain.ScLeaderboardListData;
import com.sseduventures.digichamps.domain.ScLeaderboardResponse;

import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.sseduventures.digichamps.R.id.recyclerview;
import static com.sseduventures.digichamps.R.id.school;
import static com.sseduventures.digichamps.R.id.success_image;
import static com.sseduventures.digichamps.utils.Constants.LEADERBOARD;



public class LeaderBoardActivity extends FormActivity implements ServiceReceiver.Receiver {

    Context mContext;
    CardviewLeader mCardviewLeader;
    RecyclerView recyclerview;
    ArrayList<ScLeaderboardListData> mList;
    String ClassID = "";
    String SchoolId = "";


    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private ScLeaderboardResponse success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);
        setModuleName(LogEventUtil.Event_Leaderboard);
        logEvent(LogEventUtil.Key_Leaderboard,LogEventUtil.Event_Leaderboard);

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        mContext = this;
        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        TextView tv_at = findViewById(R.id.tv_at);
        TextView tv_points = findViewById(R.id.tv_points);
        TextView tv_rank = findViewById(R.id.tv_rank);
        TextView tv_name = findViewById(R.id.tv_name);
        FontManage.setFontMeiryoBold(mContext, tv_at);

        FontManage.setFontImpact(mContext, tv_points);
        FontManage.setFontImpact(mContext, tv_rank);
        FontManage.setFontImpact(mContext, tv_name);

        mList = new ArrayList<>();
        mCardviewLeader = new CardviewLeader(mList, mContext);

        SchoolId = RegPrefManager.getInstance(this).getSchoolId();
        ClassID = String.valueOf(RegPrefManager.getInstance(this).getKeyClassId());

        getLeaderBoardNew();

    }


    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mContext)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }


    private void getLeaderBoardNew() {
        if (AppUtil.isInternetConnected(this)) {


            Bundle bun = new Bundle();
            bun.putInt("classId", Integer.parseInt(ClassID));
            NetworkService.startActionPostScLeaderboardData(LeaderBoardActivity.this
                    , mServiceReceiver, bun);

        } else {
            Intent in = new Intent(LeaderBoardActivity.this,
                    Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(LeaderBoardActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(LeaderBoardActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(LeaderBoardActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if (success.isStatus()) {

                    if (success.getList() != null &&
                            success.getList().size() > 0) {
                        mList.addAll(success.getList());
                        mCardviewLeader = new CardviewLeader(mList, mContext);
                        recyclerview.setAdapter(mCardviewLeader);
                    }
                } else {
                    AskOptionDialog("No LeaderBoard Available").show();
                }

                break;

        }
    }
}
