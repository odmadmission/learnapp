package com.sseduventures.digichamps.webservice.model;


/**
 * Author Abhishek
 */

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class DoubtDetailsModel implements Parcelable {

    protected DoubtDetailsModel(Parcel in) {
    }

    public static final Creator<DoubtDetailsModel> CREATOR = new Creator<DoubtDetailsModel>() {
        @Override
        public DoubtDetailsModel createFromParcel(Parcel in) {
            return new DoubtDetailsModel(in);
        }

        @Override
        public DoubtDetailsModel[] newArray(int size) {
            return new DoubtDetailsModel[size];
        }
    };

    public DoubtSuccess getSuccess() {
        return success;
    }

    public void setSuccess(DoubtSuccess success) {
        this.success = success;
    }

    private DoubtSuccess success;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public class DoubtSuccess implements Parcelable {

        protected DoubtSuccess(Parcel in) {
        }

        public final Creator<DoubtSuccess> CREATOR = new Creator<DoubtSuccess>() {
            @Override
            public DoubtSuccess createFromParcel(Parcel in) {
                return new DoubtSuccess(in);
            }

            @Override
            public DoubtSuccess[] newArray(int size) {
                return new DoubtSuccess[size];
            }
        };

        public Doubt getDoubts() {
            return doubts;
        }

        public void setDoubts(Doubt doubts) {
            this.doubts = doubts;
        }

        private Doubt doubts;


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
        }
    }

    public class Doubt implements Parcelable {

        protected Doubt(Parcel in) {
        }

        public final Creator<Doubt> CREATOR = new Creator<Doubt>() {
            @Override
            public Doubt createFromParcel(Parcel in) {
                return new Doubt(in);
            }

            @Override
            public Doubt[] newArray(int size) {
                return new Doubt[size];
            }
        };

        public QuestionDetails getQuestiondetails() {
            return questiondetails;
        }

        public void setQuestiondetails(QuestionDetails questiondetails) {
            this.questiondetails = questiondetails;
        }

        public ArrayList<AnswerDetails> getAnswerdetails() {
            return answerdetails;
        }

        public void setAnswerdetails(ArrayList<AnswerDetails> answerdetails) {
            this.answerdetails = answerdetails;
        }

        private QuestionDetails questiondetails;
        private ArrayList<AnswerDetails> answerdetails = null;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
        }
    }

    public class QuestionDetails implements Parcelable {


        private int Ticket_id;
        private String Question;
        private String Question_image;

        public String getExamQuestion() {
            return ExamQuestion;
        }

        public void setExamQuestion(String examQuestion) {
            ExamQuestion = examQuestion;
        }

        private String ExamQuestion = "";

        protected QuestionDetails(Parcel in) {
            Ticket_id = in.readInt();
            Question = in.readString();
            Question_image = in.readString();
            Question_date = in.readString();
            Ticket_No = in.readString();
            Qsubject = in.readString();
            byte tmpIs_Answred = in.readByte();
            Is_Answred = tmpIs_Answred == 0 ? null : tmpIs_Answred == 1;
            Asked_by = in.readString();
            Asked_Profile_image = in.readString();
            ExamQuestion = in.readString();
            Status = in.readString();
        }

        public final Creator<QuestionDetails> CREATOR = new Creator<QuestionDetails>() {
            @Override
            public QuestionDetails createFromParcel(Parcel in) {
                return new QuestionDetails(in);
            }

            @Override
            public QuestionDetails[] newArray(int size) {
                return new QuestionDetails[size];
            }
        };

        public int getTicket_id() {
            return Ticket_id;
        }

        public void setTicket_id(int ticket_id) {
            Ticket_id = ticket_id;
        }

        public String getQuestion() {
            return Question;
        }

        public void setQuestion(String question) {
            Question = question;
        }

        public String getQuestion_image() {
            return Question_image;
        }

        public void setQuestion_image(String question_image) {
            Question_image = question_image;
        }

        public String getQuestion_date() {
            return Question_date;
        }

        public void setQuestion_date(String question_date) {
            Question_date = question_date;
        }

        public String getTicket_No() {
            return Ticket_No;
        }

        public void setTicket_No(String ticket_No) {
            Ticket_No = ticket_No;
        }

        public String getQsubject() {
            return Qsubject;
        }

        public void setQsubject(String qsubject) {
            Qsubject = qsubject;
        }

        public Boolean getIs_Answred() {
            return Is_Answred;
        }

        public void setIs_Answred(Boolean is_Answred) {
            Is_Answred = is_Answred;
        }

        public String getAsked_by() {
            return Asked_by;
        }

        public void setAsked_by(String asked_by) {
            Asked_by = asked_by;
        }

        public String getAsked_Profile_image() {
            return Asked_Profile_image;
        }

        public void setAsked_Profile_image(String asked_Profile_image) {
            Asked_Profile_image = asked_Profile_image;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String status) {
            Status = status;
        }

        private String Question_date;
        private String Ticket_No;
        private String Qsubject;
        private Boolean Is_Answred;
        private String Asked_by;
        private String Asked_Profile_image;
        private String Status;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(Ticket_id);
            dest.writeString(Question);
            dest.writeString(Question_image);
            dest.writeString(Question_date);
            dest.writeString(Ticket_No);
            dest.writeString(Qsubject);
            dest.writeByte((byte) (Is_Answred == null ? 0 : Is_Answred ? 1 : 2));
            dest.writeString(Asked_by);
            dest.writeString(Asked_Profile_image);
            dest.writeString(Status);
            dest.writeString(ExamQuestion);
        }
    }

    public class AnswerDetails implements Parcelable {

        private String Answer;
        private String Answer_image;
        private String Asked_by;
        private String Answered_by;
        private Object Status;
        private String Answer_date;

        protected AnswerDetails(Parcel in) {
            Answer = in.readString();
            Answer_image = in.readString();
            Asked_by = in.readString();
            Answered_by = in.readString();
            Answer_date = in.readString();
            answer_id = in.readInt();
            Answer_by_profile = in.readString();
            Asked_Profile_image = in.readString();
            byte tmpIs_teacher = in.readByte();
            is_teacher = tmpIs_teacher == 0 ? null : tmpIs_teacher == 1;
            commentid = in.readInt();
        }

        public final Creator<AnswerDetails> CREATOR = new Creator<AnswerDetails>() {
            @Override
            public AnswerDetails createFromParcel(Parcel in) {
                return new AnswerDetails(in);
            }

            @Override
            public AnswerDetails[] newArray(int size) {
                return new AnswerDetails[size];
            }
        };

        public String getAnswer() {
            return Answer;
        }

        public void setAnswer(String answer) {
            Answer = answer;
        }

        public String getAnswer_image() {
            return Answer_image;
        }

        public void setAnswer_image(String answer_image) {
            Answer_image = answer_image;
        }

        public String getAsked_by() {
            return Asked_by;
        }

        public void setAsked_by(String asked_by) {
            Asked_by = asked_by;
        }

        public String getAnswered_by() {
            return Answered_by;
        }

        public void setAnswered_by(String answered_by) {
            Answered_by = answered_by;
        }

        public Object getStatus() {
            return Status;
        }

        public void setStatus(Object status) {
            Status = status;
        }

        public String getAnswer_date() {
            return Answer_date;
        }

        public void setAnswer_date(String answer_date) {
            Answer_date = answer_date;
        }

        public int getAnswer_id() {
            return answer_id;
        }

        public void setAnswer_id(int answer_id) {
            this.answer_id = answer_id;
        }

        public Object getRemark() {
            return Remark;
        }

        public void setRemark(Object remark) {
            Remark = remark;
        }

        public String getAnswer_by_profile() {
            return Answer_by_profile;
        }

        public void setAnswer_by_profile(String answer_by_profile) {
            Answer_by_profile = answer_by_profile;
        }

        public String getAsked_Profile_image() {
            return Asked_Profile_image;
        }

        public void setAsked_Profile_image(String asked_Profile_image) {
            Asked_Profile_image = asked_Profile_image;
        }

        public Boolean getIs_teacher() {
            return is_teacher;
        }

        public void setIs_teacher(Boolean is_teacher) {
            this.is_teacher = is_teacher;
        }

        public int getCommentid() {
            return commentid;
        }

        public void setCommentid(int commentid) {
            this.commentid = commentid;
        }

        private int answer_id;
        private Object Remark;
        private String Answer_by_profile;
        private String Asked_Profile_image;
        private Boolean is_teacher;
        private int commentid;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(Answer);
            dest.writeString(Answer_image);
            dest.writeString(Asked_by);
            dest.writeString(Answered_by);
            dest.writeString(Answer_date);
            dest.writeInt(answer_id);
            dest.writeString(Answer_by_profile);
            dest.writeString(Asked_Profile_image);
            dest.writeByte((byte) (is_teacher == null ? 0 : is_teacher ? 1 : 2));
            dest.writeInt(commentid);
        }
    }
}
