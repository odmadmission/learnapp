package com.sseduventures.digichamps.Model;

/**
 * Created by NISHIKANT on 5/14/2018.
 */

public class TSSubConceptDataModel {


    int SubConceptId;
    String SubConceptName;
    int TotalQuestions;
    int TotalCorrect;
    int TotalInCorrect;
    int TotalSkipped;
    int Accuracy;


    public TSSubConceptDataModel(int subConceptId, String subConceptName, int totalQuestions, int totalCorrect, int totalInCorrect, int totalSkipped, int accuracy) {
        SubConceptId = subConceptId;
        SubConceptName = subConceptName;
        TotalQuestions = totalQuestions;
        TotalCorrect = totalCorrect;
        TotalInCorrect = totalInCorrect;
        TotalSkipped = totalSkipped;
        Accuracy = accuracy;
    }

    public int getSubConceptId() {
        return SubConceptId;
    }

    public void setSubConceptId(int subConceptId) {
        SubConceptId = subConceptId;
    }

    public String getSubConceptName() {
        return SubConceptName;
    }

    public void setSubConceptName(String subConceptName) {
        SubConceptName = subConceptName;
    }

    public int getTotalQuestions() {
        return TotalQuestions;
    }

    public void setTotalQuestions(int totalQuestions) {
        TotalQuestions = totalQuestions;
    }

    public int getTotalCorrect() {
        return TotalCorrect;
    }

    public void setTotalCorrect(int totalCorrect) {
        TotalCorrect = totalCorrect;
    }

    public int getTotalInCorrect() {
        return TotalInCorrect;
    }

    public void setTotalInCorrect(int totalInCorrect) {
        TotalInCorrect = totalInCorrect;
    }

    public int getTotalSkipped() {
        return TotalSkipped;
    }

    public void setTotalSkipped(int totalSkipped) {
        TotalSkipped = totalSkipped;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }
}
