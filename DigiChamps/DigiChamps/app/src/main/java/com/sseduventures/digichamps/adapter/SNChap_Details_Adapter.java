package com.sseduventures.digichamps.adapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sseduventures.digichamps.Model.ProgressModule;
import com.sseduventures.digichamps.Model.StudyNotes_Model;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.Chapter_Details;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.database.DbContract;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;

import java.util.List;



public class SNChap_Details_Adapter extends RecyclerView.Adapter<SNChap_Details_Adapter.MyViewHolder> {
    View view;
    private String resp, error,chap_id,sub_id, TAG = "Orders";
    private SpotsDialog dialog;
    private ImageView listener;
    String User_Id;
    private Chapter_Details mContext;


    private Context context;
    private List<StudyNotes_Model> moviesList;
    String moduleId;

    public SNChap_Details_Adapter(List<StudyNotes_Model> moviesList, Context context,String chap_id, String sub_id,
                                  Chapter_Details mContext) {
        this.moviesList = moviesList;
        this.context = context;
        this.chap_id = chap_id;
        this.sub_id = sub_id;
        this.mContext = mContext;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_sn_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.container.setTag(moviesList.get(position).getModuleId());
        holder.title.setText("Study Notes");

        holder.play.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.unbookmark)));

        if(moviesList.get(position).getIsSNBookmarked().equalsIgnoreCase("true")){

            holder.play.setImageResource(R.drawable.bookmarks);

        }else if(moviesList.get(position).getIsSNBookmarked().equalsIgnoreCase("false")){
            holder.play.setImageResource(R.drawable.bookmarksilver);
        }

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               DbTask dbTask=new  DbTask();
                dbTask.execute(chap_id,sub_id,(String) view.getTag());

                String PDF_URL= AppConfig.PDF_URL+moviesList.get(position).getPdfUrl();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(PDF_URL));
                context.startActivity(browserIntent);
            }
        });

        User_Id = String.valueOf(RegPrefManager.getInstance(context).getRegId());


        holder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moduleId = moviesList.get(position).getModuleId();
                mContext.setUnsetBookMark(moduleId,"3");



            }
        });

    }



    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private View.OnClickListener onClickListener(final int position, final Chapter_Details_Adapter.MyViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init();

                if (AppUtil.isInternetConnected(context)) {



                } else {
                    Intent i = new Intent(context, Internet_Activity.class);
                    Activity activity = (Activity) context;

                    context.startActivity(i);

                }
            }

        };
    }


    private void init() {


        dialog = new SpotsDialog(context, "FUN + EDUCATION", R.style.Custom);


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;
        public ImageView play;
        ImageView left_drawable, right_drawable;
        CardView container;

        public MyViewHolder(View view) {
            super(view);
            container = (CardView) itemView.findViewById(R.id.card_view_chapter);
            title = (TextView) view.findViewById(R.id.chap);
            play = (ImageView) view.findViewById(R.id.play);


        }
    }
    public  void setPlaying(boolean isPlaying){
        if(isPlaying){
            listener.setImageResource(R.drawable.pause_button);
        }else{
            listener.setImageResource(R.drawable.play_button);
        }
    }

    private class DbTask extends AsyncTask<String,Void,Void>
    {
        @Override
        protected Void doInBackground(String... strings)
        {
            Log.v(TAG,strings[0]+":"+strings[1]);
            try
            {


                ProgressModule pm = new ProgressModule();
                pm.setModule_ID(Integer.parseInt(strings[2]));
                pm.setModule_Type(Constants.PROGRESS_SN);
                pm.setSection_ID(
                        RegPrefManager.getInstance(context)
                        .getSectionId()
                );
                pm.setClass_ID(
                        (int)RegPrefManager.getInstance(context)
                                .getKeyClassId()
                );
                pm.setChapter_ID(Integer.parseInt(strings[0]));
                pm.setSubject_ID(Integer.parseInt(strings[1]));


                ContentValues values = new ContentValues();


                values.put(
                        DbContract.ProgressTable.KEY_CLASS_ID
                        ,pm.getClass_ID());
                values.put(
                        DbContract.ProgressTable.KEY_SECTION_ID,
                        pm.getSection_ID());
                values.put(DbContract.ProgressTable.KEY_CHAPTER_ID,
                        pm.getChapter_ID());
                values.put(DbContract.ProgressTable.KEY_SUBJECT_ID
                        ,pm.getSubject_ID());
                values.put(DbContract.ProgressTable.KEY_MODULE_ID,
                        pm.getModule_ID());
                values.put(DbContract.ProgressTable.KEY_MODULE_TYPE,
                        pm.getModule_Type());

                values.put(DbContract.ProgressTable
                        .KEY_INSERT_DATE_TIME,pm.getInserted_On().getTime());
                values.put(DbContract.ProgressTable.KEY_REG_ID,
                        pm.getRegd_ID());

                Uri uri2=context.getContentResolver().insert(
                        DbContract.ProgressTable.CONTENT_URI,
                        values);

                if(uri2!=null)
                {

                    Log.v(TAG,"Inserted rowId : "+uri2.getLastPathSegment()+"");
                }


            }
            catch (Exception e)
            {
                Log.v(TAG,"Exception : "+e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }



}
