package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by NISHIKANT on 6/8/2018.
 */

public class PsychometricPostRequest implements Parcelable
{

      private int ExamId;
    private int  RegId;
    private Date StartTime;
    private Date StopTime;
    private ArrayList<PsychometricQuestion> questions;


    public int getExamId() {
        return ExamId;
    }

    public void setExamId(int examId) {
        ExamId = examId;
    }

    public int getRegId() {
        return RegId;
    }

    public void setRegId(int regId) {
        RegId = regId;
    }

    public Date getStartTime() {
        return StartTime;
    }

    public void setStartTime(Date startTime) {
        StartTime = startTime;
    }

    public Date getStopTime() {
        return StopTime;
    }

    public void setStopTime(Date stopTime) {
        StopTime = stopTime;
    }

    public ArrayList<PsychometricQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<PsychometricQuestion> questions) {
        this.questions = questions;
    }

    public PsychometricPostRequest() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.ExamId);
        dest.writeInt(this.RegId);
        dest.writeLong(this.StartTime != null ? this.StartTime.getTime() : -1);
        dest.writeLong(this.StopTime != null ? this.StopTime.getTime() : -1);
        dest.writeTypedList(this.questions);
    }

    protected PsychometricPostRequest(Parcel in) {
        this.ExamId = in.readInt();
        this.RegId = in.readInt();
        long tmpStartTime = in.readLong();
        this.StartTime = tmpStartTime == -1 ? null : new Date(tmpStartTime);
        long tmpStopTime = in.readLong();
        this.StopTime = tmpStopTime == -1 ? null : new Date(tmpStopTime);
        this.questions = in.createTypedArrayList(PsychometricQuestion.CREATOR);
    }

    public static final Creator<PsychometricPostRequest> CREATOR = new Creator<PsychometricPostRequest>() {
        @Override
        public PsychometricPostRequest createFromParcel(Parcel source) {
            return new PsychometricPostRequest(source);
        }

        @Override
        public PsychometricPostRequest[] newArray(int size) {
            return new PsychometricPostRequest[size];
        }
    };
}
