package com.sseduventures.digichamps.frags;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sseduventures.digichamps.Model.Basic_info_model;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.BasicInfomationActivity;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.LearnAndEarnActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.Coin_DialogAdapter;
import com.sseduventures.digichamps.adapter.School_listview_DialogAdapter;
import com.sseduventures.digichamps.domain.EnCashResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;

public class LearnAndEarnFragmentPoint extends Fragment implements ServiceReceiver.Receiver {

    TextView your_coins, text, encash_tv;
    ArrayList<Basic_info_model> section_List;
    ListView listView;
    Coin_DialogAdapter coin_dialogAdapter;
    String coin_value;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;

    private EnCashResponse success;
    int CoinsCount;
    private FormActivity mContext;
    private String currentPhone="null", currentAddress="null";

    public LearnAndEarnFragmentPoint() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ((FormActivity) getActivity()).logEvent(LogEventUtil.
                        KEY_LearnAndEarnPoints_PAGE,
                LogEventUtil.EVENT_LearnAndEarnPoints_PAGE);

        ((FormActivity) getActivity()).setModuleName(LogEventUtil.EVENT_LearnAndEarnPoints_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.learn_and_earn_points, container, false);


        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        mContext = (LearnAndEarnActivity) getActivity();
        final int CoinCount = RegPrefManager.getInstance(getActivity()).getCoinCount();
        your_coins = view.findViewById(R.id.your_coins);

        text = view.findViewById(R.id.text);
        encash_tv = view.findViewById(R.id.encash_tv);
        text.setText(CoinCount + "");
        section_List = new ArrayList<>();
        encash_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CoinsCount = Integer.parseInt(text.getText().toString());


                if (CoinsCount < 1000) {


                    notcashdialog();
                } else if (CoinsCount >= 1000 && CoinsCount <= 1500) {
                    // CoinCount-1000;
                    section_List.clear();

                    for (int i = 0; i < 1; i++) {
                        Basic_info_model basic_info_model = new Basic_info_model();
                        basic_info_model.setName("1000");
                        section_List.add(basic_info_model);
                    }


                        NoteCashdialog();
                   // AddressDialogTwo();

                } else if (CoinsCount >= 1000 && CoinsCount <= 2000) {
                    section_List.clear();

                    for (int i = 0; i < 2; i++) {
                        if (i == 0) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1000");
                            section_List.add(basic_info_model);
                        }
                    /*    if (i == 1) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1500");
                            section_List.add(basic_info_model);
                        }*/


                    }


                       NoteCashdialog();
                   // AddressDialogTwo();
                } else if (CoinsCount >= 2000 && CoinsCount <= 3000) {
                    section_List.clear();

                    for (int i = 0; i < 4; i++) {
                        if (i == 0) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 1) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("2000");
                            section_List.add(basic_info_model);
                        }
                       /* if (i == 2) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("3000");
                            section_List.add(basic_info_model);
                        }*/

                    }


                      NoteCashdialog();
                   // AddressDialogTwo();
                } else if (CoinsCount >= 3000 && CoinsCount <= 4500) {
                    section_List.clear();

                    for (int i = 0; i < 4; i++) {
                        if (i == 0) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 1) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("2000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 2) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("3000");
                            section_List.add(basic_info_model);
                        }

                    }


                    NoteCashdialog();
                    // AddressDialogTwo();
                }
                else if (CoinsCount >= 4500 && CoinsCount <= 7500) {
                    section_List.clear();

                    for (int i = 0; i < 5; i++) {
                        if (i == 0) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1000");
                            section_List.add(basic_info_model);
                        }
                       /* if (i == 1) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1500");
                            section_List.add(basic_info_model);
                        }*/
                        if (i == 1) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("2000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 2) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("3000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 3) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("4500");
                            section_List.add(basic_info_model);
                        }


                    }


                       NoteCashdialog();
                   // AddressDialogTwo();
                } else if (CoinsCount >= 7500 && CoinsCount <= 10000) {
                    section_List.clear();

                    for (int i = 0; i < 6; i++) {
                        if (i == 0) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1000");
                            section_List.add(basic_info_model);
                        }
                       /* if (i == 1) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1500");
                            section_List.add(basic_info_model);
                        }*/
                        if (i == 1) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("2000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 2) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("3000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 3) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("4500");
                            section_List.add(basic_info_model);
                        }
                        if (i == 4) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("7500");
                            section_List.add(basic_info_model);
                        }


                    }


                      NoteCashdialog();
                   // AddressDialogTwo();
                } else if (CoinsCount >= 10000 && CoinsCount <= 15000) {
                    section_List.clear();

                    for (int i = 0; i < 7; i++) {
                        if (i == 0) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1000");
                            section_List.add(basic_info_model);
                        }
                      /*  if (i == 1) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1500");
                            section_List.add(basic_info_model);
                        }*/
                        if (i == 1) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("2000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 2) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("3000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 3) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("4500");
                            section_List.add(basic_info_model);
                        }  if (i == 4) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("7500");
                            section_List.add(basic_info_model);
                        }
                        if (i == 5) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("10000");
                            section_List.add(basic_info_model);
                        }


                    }


                       NoteCashdialog();
                   // AddressDialogTwo();
                } else if (CoinsCount >= 15000 && CoinsCount <= 20000) {
                    section_List.clear();

                    for (int i = 0; i < 8; i++) {
                        if (i == 0) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 1) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("2000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 2) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("3000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 3) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("4500");
                            section_List.add(basic_info_model);
                        }
                        if (i == 4) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("7500");
                            section_List.add(basic_info_model);
                        }
                        if (i == 5) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("10000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 6) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("15000");
                            section_List.add(basic_info_model);
                        }


                    }


                      NoteCashdialog();
                   // AddressDialogTwo();
                } else if (CoinsCount >= 20000 && CoinsCount <= 30000) {
                    section_List.clear();

                    for (int i = 0; i < 9; i++) {
                        if (i == 0) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 1) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("2000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 2) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("3000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 3) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("4500");
                            section_List.add(basic_info_model);
                        }
                        if (i == 4) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("7500");
                            section_List.add(basic_info_model);
                        }
                        if (i == 5) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("10000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 6) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("15000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 7) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("20000");
                            section_List.add(basic_info_model);
                        }

                    }


                       NoteCashdialog();
                   // AddressDialogTwo();
                } else if (CoinsCount >= 30000 && CoinsCount <= 50000) {
                    // Log.d("Tag","Value");
                    section_List.clear();

                    for (int i = 0; i < 10; i++) {
                        if (i == 0) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 1) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("2000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 2) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("3000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 3) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("4500");
                            section_List.add(basic_info_model);
                        }
                        if (i == 4) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("7500");
                            section_List.add(basic_info_model);
                        }
                        if (i == 5) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("10000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 6) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("15000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 7) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("20000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 8) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("30000");
                            section_List.add(basic_info_model);
                        }

                    }


                     NoteCashdialog();
                  //  AddressDialogTwo();
                } else if (CoinsCount >= 50000) {
                    section_List.clear();

                    for (int i = 0; i < 10; i++) {
                        if (i == 0) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("1000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 1) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("2000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 2) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("3000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 3) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("4500");
                            section_List.add(basic_info_model);
                        }
                        if (i == 4) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("7500");
                            section_List.add(basic_info_model);
                        }
                        if (i == 5) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("10000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 6) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("15000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 7) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("20000");
                            section_List.add(basic_info_model);
                        }
                        if (i == 8) {
                            Basic_info_model basic_info_model = new Basic_info_model();
                            basic_info_model.setName("30000");
                            section_List.add(basic_info_model);
                        }

                    }


                      NoteCashdialog();
                   // AddressDialogTwo();
                }

            }
        });


        return view;
    }

    public void notcashdialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.no_coin_dialog);
        dialog.setCanceledOnTouchOutside(false);
        TextView note_tv = (TextView) dialog.findViewById(R.id.note_tv);

        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);

        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void NoteCashdialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.coin_notecash_dialog);
        dialog.setCanceledOnTouchOutside(false);

        listView = (ListView) dialog.findViewById(R.id.dialoglist);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);


        select_tv.setVisibility(View.VISIBLE);
        select_tv.setText("Encash your coins");
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        TextView back_but = (TextView) dialog.findViewById(R.id.back_but);


        coin_dialogAdapter = new Coin_DialogAdapter(section_List, getContext());

        listView.setAdapter(coin_dialogAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                section_List.get(position).setFlag(true);
                String section_name = section_List.get(position).getName();
                for (int i = 0; i < section_List.size(); i++) {
                    if (position != i)
                        section_List.get(i).setFlag(false);
                }
                coin_dialogAdapter.notifyDataSetChanged();

            }
        });

        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<Basic_info_model> list = coin_dialogAdapter.coin_ModelArrayList();
                boolean flag1 = false;
                for (int i = 0; i < list.size(); i++) {
                    boolean flag = list.get(i).isFlag();
                    if (flag == true) {
                        flag1 = true;
                        Basic_info_model basic_info_model = list.get(i);
                        coin_value = basic_info_model.getName();


                    }


                }
                if (flag1 == true) {
                 //   getEncashData();
                    AddressDialogTwo();
                    dialog.dismiss();
                } else {
                    Toast.makeText(getActivity(), "Please Select your Coins", Toast.LENGTH_LONG).show();
                }
            }

        });

        back_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void AddressDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.address_layout);
        dialog.setCanceledOnTouchOutside(false);

        //listView= (ListView) dialog.findViewById(R.id.dialoglist);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);


        select_tv.setVisibility(View.VISIBLE);
        //select_tv.setText("Encash your coins");
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        TextView back_but = (TextView) dialog.findViewById(R.id.back_but);
        final EditText phoneed = (EditText) dialog.findViewById(R.id.phoneed);
        final EditText addressed = (EditText) dialog.findViewById(R.id.addressed);


        String phonePref = RegPrefManager.getInstance(getActivity()).getPhone();
        phoneed.setText(phonePref);

        phoneed.setSelection(phoneed.length());
        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (phoneed.getText().toString().isEmpty()) {
                    currentPhone="null";
                } else {
                    currentPhone = phoneed.getText().toString().trim();

                }

                if (addressed.getText().toString().isEmpty()) {
                    currentAddress="null";
                } else {
                    currentAddress = addressed.getText().toString().trim();

                }

                if (currentPhone.equals("null")) {
                    Toast.makeText(getActivity(), "Please Enter your Phone Number", Toast.LENGTH_LONG).show();
                }else if(currentAddress.equals("null")){
                    Toast.makeText(getActivity(), "Please Enter your Address", Toast.LENGTH_LONG).show();
                }

                else {
                    getEncashData();
                    dialog.dismiss();
                }
                    //NoteCashdialog();
                   // success_dialog();




            }

        });

        back_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void AddressDialogTwo() {
        final Dialog dialog = new Dialog(getContext());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.address_layout_two);
        dialog.setCanceledOnTouchOutside(false);

        //listView= (ListView) dialog.findViewById(R.id.dialoglist);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);


        select_tv.setVisibility(View.VISIBLE);

        //select_tv.setText("Encash your coins");
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        TextView back_but = (TextView) dialog.findViewById(R.id.back_but);



        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                AddressDialog();

            }

        });

        back_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void success_dialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.coin_layout);


        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);

        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }


    private void getEncashData() {
        int value = Integer.parseInt(coin_value);

        Bundle bun = new Bundle();

        bun.putInt("coins", value);
        bun.putString("mobile", currentPhone);
        bun.putString("address", currentAddress);

        if (AppUtil.isInternetConnected(getActivity())) {

            mContext.showDialog(null, "please wait");
            NetworkService.startActionGetPostEncash(getActivity(), mServiceReceiver, bun);

        } else {
            Intent in = new Intent(getActivity(), Internet_Activity.class);
            startActivity(in);
        }

    }

    @Override
    public void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }


    @Override
    public void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        mContext.hideDialog();
        switch (resultCode) {
            case ResponseCodes.SUCCESS:

                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if (success != null) {
                    int coins = success.getCount();
                    RegPrefManager.getInstance(getActivity()).setCoinCount(coins);
                    text.setText(coins + "");
                    success_dialog();
                  //  AddressDialogTwo();

                }

                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(), ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(), ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(), Internet_Activity.class);
                startActivity(intent);
                break;

            case ResponseCodes.SERVER_ERROR:
                break;
        }


    }
}