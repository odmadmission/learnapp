package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/9/2018.
 */

public class LogoutResponse implements Parcelable{

    private LogoutSuccess success;

    public LogoutSuccess getSuccess() {
        return success;
    }

    public void setSuccess(LogoutSuccess success) {
        this.success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.success, flags);
    }

    public LogoutResponse() {
    }

    protected LogoutResponse(Parcel in) {
        this.success = in.readParcelable(LogoutSuccess.class.getClassLoader());
    }

    public static final Creator<LogoutResponse> CREATOR = new Creator<LogoutResponse>() {
        @Override
        public LogoutResponse createFromParcel(Parcel source) {
            return new LogoutResponse(source);
        }

        @Override
        public LogoutResponse[] newArray(int size) {
            return new LogoutResponse[size];
        }
    };
}
