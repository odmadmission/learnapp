package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class HomeworkRequest implements Parcelable {

    private String SchoolId;
    private int ClassId;
    private String SectionId;
    private String HomeWorkDate;

    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public int getClassId() {
        return ClassId;
    }

    public void setClassId(int classId) {
        ClassId = classId;
    }

    public String getSectionId() {
        return SectionId;
    }

    public void setSectionId(String sectionId) {
        SectionId = sectionId;
    }

    public String getHomeWorkDate() {
        return HomeWorkDate;
    }

    public void setHomeWorkDate(String homeWorkDate) {
        HomeWorkDate = homeWorkDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.SchoolId);
        dest.writeInt(this.ClassId);
        dest.writeString(this.SectionId);
        dest.writeString(this.HomeWorkDate);
    }

    public HomeworkRequest() {
    }

    protected HomeworkRequest(Parcel in) {
        this.SchoolId = in.readString();
        this.ClassId = in.readInt();
        this.SectionId = in.readString();
        this.HomeWorkDate = in.readString();
    }

    public static final Creator<HomeworkRequest> CREATOR = new Creator<HomeworkRequest>() {
        @Override
        public HomeworkRequest createFromParcel(Parcel source) {
            return new HomeworkRequest(source);
        }

        @Override
        public HomeworkRequest[] newArray(int size) {
            return new HomeworkRequest[size];
        }
    };
}
