package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class SmsMessageData implements Parcelable
    {

        private String number;

        private String messageId;

        private String message;

        public SmsMessageData() {
        }

        protected SmsMessageData(Parcel in) {
            number = in.readString();
            messageId = in.readString();
            message = in.readString();
        }

        public static final Creator<SmsMessageData> CREATOR = new Creator<SmsMessageData>() {
            @Override
            public SmsMessageData createFromParcel(Parcel in) {
                return new SmsMessageData(in);
            }

            @Override
            public SmsMessageData[] newArray(int size) {
                return new SmsMessageData[size];
            }
        };

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getMessageId() {
            return messageId;
        }

        public void setMessageId(String messageId) {
            this.messageId = messageId;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(number);
            parcel.writeString(messageId);
            parcel.writeString(message);
        }


    }