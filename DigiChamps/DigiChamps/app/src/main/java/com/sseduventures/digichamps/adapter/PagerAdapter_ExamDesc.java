package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sseduventures.digichamps.Model.ModelViewPager_ExamDesc;


public class PagerAdapter_ExamDesc extends PagerAdapter {

    private Context mContext;
    private String module;
    public PagerAdapter_ExamDesc(Context context,String text) {
        mContext = context;
        module=text;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        ViewGroup layout=null;
        if(module.equalsIgnoreCase("psychometric"))
        {
            ModelViewPager_ExamDesc modelObject = ModelViewPager_ExamDesc.values()[position];
            LayoutInflater inflater = LayoutInflater.from(mContext);
            layout = (ViewGroup) inflater.inflate(modelObject.getLayoutResId(), collection, false);
            collection.addView(layout);
        }
        else {
            ModelViewPager_ExamDesc modelObject = ModelViewPager_ExamDesc.values()[position];
            LayoutInflater inflater = LayoutInflater.from(mContext);
             layout = (ViewGroup) inflater.inflate(modelObject.getLayoutResId(), collection, false);
            collection.addView(layout);
        }
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return ModelViewPager_ExamDesc.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        ModelViewPager_ExamDesc customPagerEnum = ModelViewPager_ExamDesc.values()[position];

        return mContext.getString(customPagerEnum.getTitleResId());
    }

}
