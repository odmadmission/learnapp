package com.sseduventures.digichamps.database;

import android.content.ContentValues;

import com.sseduventures.digichamps.domain.FeedListData;
import com.sseduventures.digichamps.modelnew.DictionaryModel;


/**
 * Created by user on 4/27/2018.
 */

public class ContentValueHelper
{

    public static ContentValues createFeedValues(FeedListData data)
    {
        ContentValues cValues=new ContentValues();
        cValues.put(DbContract.FeedTable.FEED_ID,data.getFeed_ID());
        cValues.put(DbContract.FeedTable.TITLE,data.getFeed_Name());
        cValues.put(DbContract.FeedTable.TEXT,data.getFeed_Description());
        cValues.put(DbContract.FeedTable.IMAGE_URL,data.getFeedOnline());
        cValues.put(DbContract.FeedTable.LINK,data.getFeed_Path());
        return  cValues;
    }

    public static ContentValues createDictionaryValues(DictionaryModel data)
    {
        ContentValues cValues=new ContentValues();
        cValues.put(DbContract.DictionaryTable.DICTIONARY_ID,
                data.getDictionary_ID());
        cValues.put(DbContract.DictionaryTable.WORD,
                data.getDictionary_WordName());
        cValues.put(DbContract.DictionaryTable.IMAGE_URL,
                data.getDictionary_Images());
        cValues.put(DbContract.DictionaryTable.PATH,
                data.getDictionary_Path());
        cValues.put(DbContract.DictionaryTable.INSERTED_ON,
                data.getInserted_On().getTime());

        return  cValues;
    }



}
