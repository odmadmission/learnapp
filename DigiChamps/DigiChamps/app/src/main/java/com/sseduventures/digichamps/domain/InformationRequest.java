package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class InformationRequest implements Parcelable{

    private List<InfoList> schoolInformation;
    private String Message;
    private int ResultCount;

    public List<InfoList> getSchoolInformation() {
        return schoolInformation;
    }

    public void setSchoolInformation(List<InfoList> schoolInformation) {
        this.schoolInformation = schoolInformation;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getResultCount() {
        return ResultCount;
    }

    public void setResultCount(int resultCount) {
        ResultCount = resultCount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.schoolInformation);
        dest.writeString(this.Message);
        dest.writeInt(this.ResultCount);
    }

    public InformationRequest() {
    }

    protected InformationRequest(Parcel in) {
        this.schoolInformation = in.createTypedArrayList(InfoList.CREATOR);
        this.Message = in.readString();
        this.ResultCount = in.readInt();
    }

    public static final Creator<InformationRequest> CREATOR = new Creator<InformationRequest>() {
        @Override
        public InformationRequest createFromParcel(Parcel source) {
            return new InformationRequest(source);
        }

        @Override
        public InformationRequest[] newArray(int size) {
            return new InformationRequest[size];
        }
    };
}
