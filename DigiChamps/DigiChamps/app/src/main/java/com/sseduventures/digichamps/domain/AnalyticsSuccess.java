package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public  class AnalyticsSuccess implements Parcelable{

        private List<UserReportAnalyticsList> userReportAnalyticsList;
        private List<TestAnalysisAnalyticsList> testAnalysisAnalyticsList;
        private List<SubConceptAnalyticsList> subConceptAnalyticsList;
        private DoubtsAnalytics doubtsAnalytics;
        private ArrayList<String> subjectList;

        public List<UserReportAnalyticsList> getUserReportAnalyticsList() {
            return userReportAnalyticsList;
        }

        public void setUserReportAnalyticsList(List<UserReportAnalyticsList> userReportAnalyticsList) {
            this.userReportAnalyticsList = userReportAnalyticsList;
        }

        public List<TestAnalysisAnalyticsList> getTestAnalysisAnalyticsList() {
            return testAnalysisAnalyticsList;
        }

        public void setTestAnalysisAnalyticsList(List<TestAnalysisAnalyticsList> testAnalysisAnalyticsList) {
            this.testAnalysisAnalyticsList = testAnalysisAnalyticsList;
        }

        public List<SubConceptAnalyticsList> getSubConceptAnalyticsList() {
            return subConceptAnalyticsList;
        }

        public void setSubConceptAnalyticsList(List<SubConceptAnalyticsList> subConceptAnalyticsList) {
            this.subConceptAnalyticsList = subConceptAnalyticsList;
        }

        public DoubtsAnalytics getDoubtsAnalytics() {
            return doubtsAnalytics;
        }

        public void setDoubtsAnalytics(DoubtsAnalytics doubtsAnalytics) {
            this.doubtsAnalytics = doubtsAnalytics;
        }

        public ArrayList<String> getSubjectList() {
            return subjectList;
        }

        public void setSubjectList(ArrayList<String> subjectList) {
            this.subjectList = subjectList;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeList(this.userReportAnalyticsList);
            dest.writeList(this.testAnalysisAnalyticsList);
            dest.writeList(this.subConceptAnalyticsList);
            dest.writeParcelable((Parcelable) this.doubtsAnalytics, flags);
            dest.writeStringList(this.subjectList);
        }

        public AnalyticsSuccess() {
        }

        protected AnalyticsSuccess(Parcel in) {
            this.userReportAnalyticsList = new ArrayList<UserReportAnalyticsList>();
            in.readList(this.userReportAnalyticsList, UserReportAnalyticsList.class.getClassLoader());
            this.testAnalysisAnalyticsList = new ArrayList<TestAnalysisAnalyticsList>();
            in.readList(this.testAnalysisAnalyticsList, TestAnalysisAnalyticsList.class.getClassLoader());
            this.subConceptAnalyticsList = new ArrayList<SubConceptAnalyticsList>();
            in.readList(this.subConceptAnalyticsList, SubConceptAnalyticsList.class.getClassLoader());
            this.doubtsAnalytics = in.readParcelable(DoubtsAnalytics.class.getClassLoader());
            this.subjectList = in.createStringArrayList();
        }

        public static final Creator<AnalyticsSuccess> CREATOR = new Creator<AnalyticsSuccess>() {
            @Override
            public AnalyticsSuccess createFromParcel(Parcel source) {
                return new AnalyticsSuccess(source);
            }

            @Override
            public AnalyticsSuccess[] newArray(int size) {
                return new AnalyticsSuccess[size];
            }
        };
    }