package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.domain.CartPackage;

import java.util.ArrayList;


public class Cart_Custom_Adapter extends RecyclerView.Adapter<Cart_Custom_Adapter.MyViewHolder> {

    public   ArrayList<CartPackage> dataSet;
    private Context mContext;
    private int discount=0;

    public ArrayList<CartPackage> selected_usersList = new ArrayList<>();

    public Cart_Custom_Adapter(ArrayList<CartPackage> data,
                               Context context,int discount) {


        this.mContext = context;
        this.dataSet = data;
        this.discount=discount;


    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_layout_cart_new, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView month = holder.month;
        TextView chap = holder.chap;
        TextView cross_price = holder.cross_price;
        TextView main_price = holder.main_price;
        TextView txt_saving = holder.txt_saving;


        cross_price.setPaintFlags(cross_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if(discount==1){


            int newPrice = dataSet.get(listPosition).getPackage_Price();
            double discPrice = .65*newPrice;

            main_price.setText("\u20B9 "+(int)discPrice);

            int totalSavingNew = dataSet.get(listPosition).getPackage_Price() - (int)discPrice;
            txt_saving.setText("\u20B9 "+(int)totalSavingNew);

            holder.offer_discount_new.setText("35"+"% Discount");



        }else{


            main_price.setText("\u20B9 "+dataSet.get(listPosition).getDiscounted_Price());
            txt_saving.setText("\u20B9 "+dataSet.get(listPosition).getDiscount());


            holder.offer_discount_new.setText(dataSet.get(listPosition).getDiscountpercentage()+"% Discount");


        }

        month.setText(dataSet.get(listPosition).getValidity()+" days");
        chap.setText(dataSet.get(listPosition).getSubscripttion_Limit()+" chapters");

        if(dataSet.get(listPosition).getPackage_Name().contains("BEGINNER"))
        holder.chapter.setText("BEGINNER");
        else
        if(dataSet.get(listPosition).getPackage_Name().contains("PRO"))
            holder.chapter.setText("PRO");
        else
        if(dataSet.get(listPosition).getPackage_Name().contains("EXPERT"))
            holder.chapter.setText("EXPERT");
        else
        if(dataSet.get(listPosition).getPackage_Name().contains("CHAMPION"))
            holder.chapter.setText("CHAMPION");



        cross_price.setText(Html.fromHtml("<strike>\u20B9 "+dataSet.get(listPosition).getPackage_Price()+"</strike>"));


        Picasso.with(mContext).load(dataSet.get(listPosition).getPackageimage())
                .into(holder.imageView_pack);
        holder.linear_pack.setVisibility(View.VISIBLE);
        holder.imageView_offer.setImageResource(R.drawable.offer_pack);
        holder.offer.setText(dataSet.get(listPosition).getDiscountpercentage()+"");





        if (selected_usersList.contains(dataSet.get(listPosition)))
            holder.container.setBackgroundColor(mContext.getResources().getColor(R.color.semiTransparent));
        else
            holder.container.setBackgroundColor(mContext.getResources().getColor(R.color.white));
    }



    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView feature, val, sub, type, details;
        public LinearLayout linear_pack;

        public LinearLayout linear1;

        CardView container;
        TextView offer, offer_name, chapter,
                month, chap, status, cross_price, main_price,offer_discount_new,txt_saving;
        ImageView imageView_offer, imageView_pack;


        public MyViewHolder(View itemView) {
            super(itemView);

            linear1 = (LinearLayout) itemView.findViewById(R.id.linear1);

            container = (CardView) itemView.findViewById(R.id.card_cart);
            this.offer = (TextView) itemView.findViewById(R.id.offer_discount_cart);
            this.offer_name = (TextView) itemView.findViewById(R.id.test_cart_intro);
            this.chapter = (TextView) itemView.findViewById(R.id.text_champ_cart);
            this.feature = (TextView) itemView.findViewById(R.id.text_feature_cart);
            this.val = (TextView) itemView.findViewById(R.id.text_val_cart);
            this.month = (TextView) itemView.findViewById(R.id.text_month_cart);
            this.sub = (TextView) itemView.findViewById(R.id.text_subcrib_cart);
            this.chap = (TextView) itemView.findViewById(R.id.text_chap_cart);
            this.type = (TextView) itemView.findViewById(R.id.text_type_cart);
            this.status = (TextView) itemView.findViewById(R.id.text_status_cart);
            this.details = (TextView) itemView.findViewById(R.id.text_price);
            this.cross_price = (TextView) itemView.findViewById(R.id.text_price_cross);
            this.main_price = (TextView) itemView.findViewById(R.id.text_price_cart);
            this.offer_discount_new = (TextView) itemView.findViewById(R.id.offer_discount_new);
            this.txt_saving = (TextView) itemView.findViewById(R.id.txt_saving);

            this.linear_pack = (LinearLayout) itemView.findViewById(R.id.liner_offer_cart);

            imageView_offer = (ImageView) itemView.findViewById(R.id.back_image_cart);
            imageView_pack = (ImageView) itemView.findViewById(R.id.cart_header);


        }

    }


}


