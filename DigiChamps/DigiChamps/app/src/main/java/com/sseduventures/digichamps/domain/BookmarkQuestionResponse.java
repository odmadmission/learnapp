package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 9/5/2018.
 */

public class BookmarkQuestionResponse implements Parcelable{


    private BookmarkQuestionSuccess SuccessBookmarkedQuestion;

    protected BookmarkQuestionResponse(Parcel in) {
        SuccessBookmarkedQuestion = in.readParcelable(BookmarkQuestionSuccess.class.getClassLoader());
    }

    public static final Creator<BookmarkQuestionResponse> CREATOR = new Creator<BookmarkQuestionResponse>() {
        @Override
        public BookmarkQuestionResponse createFromParcel(Parcel in) {
            return new BookmarkQuestionResponse(in);
        }

        @Override
        public BookmarkQuestionResponse[] newArray(int size) {
            return new BookmarkQuestionResponse[size];
        }
    };

    public BookmarkQuestionSuccess getSuccessBookmarkedQuestion() {
        return SuccessBookmarkedQuestion;
    }

    public void setSuccessBookmarkedQuestion(BookmarkQuestionSuccess successBookmarkedQuestion) {
        SuccessBookmarkedQuestion = successBookmarkedQuestion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(SuccessBookmarkedQuestion, i);
    }
}
