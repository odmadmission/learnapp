package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class StudyMaterialsResponse implements Parcelable{

    private List<StudyMaterialsList> list;
    private boolean status;
    private String message;

    public List<StudyMaterialsList> getList() {
        return list;
    }

    public void setList(List<StudyMaterialsList> list) {
        this.list = list;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.list);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.message);
    }

    public StudyMaterialsResponse() {
    }

    protected StudyMaterialsResponse(Parcel in) {
        this.list = in.createTypedArrayList(StudyMaterialsList.CREATOR);
        this.status = in.readByte() != 0;
        this.message = in.readString();
    }

    public static final Creator<StudyMaterialsResponse> CREATOR = new Creator<StudyMaterialsResponse>() {
        @Override
        public StudyMaterialsResponse createFromParcel(Parcel source) {
            return new StudyMaterialsResponse(source);
        }

        @Override
        public StudyMaterialsResponse[] newArray(int size) {
            return new StudyMaterialsResponse[size];
        }
    };
}
