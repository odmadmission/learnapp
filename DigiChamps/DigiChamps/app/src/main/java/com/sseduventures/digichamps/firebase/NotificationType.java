package com.sseduventures.digichamps.firebase;

/**
 * Created by NISHIKANT on 8/11/2018.
 */

public interface NotificationType
{
    int REG_REFERRAL=1;
    int TEST_RESULT=2;
    int ORDER_CONFIRM=3;
    int DOUBT_ANSWER=4;
    int NEW_FEED=5;
    int MENTOR_ASSIGN=6;
    int NEW_MENTOR_TASK=7;
    int UPDATE_MENTOR_TASK=8;
    int OVERDUE_MENTOR_TASK=9;
    int NEW_PACKAGE=10;
    int NEW_VIDEO=11;
    int PACKAGE_ACTIVE=12;
    int BACKEND_MESSAGE=13;




}
