package com.sseduventures.digichamps.Model;

public class AnalysisDetaillistModel {


    String ChapterId, Chapter, PRT_TotalQnos, CBT_TotalQnos, PRT_TotalAns,CBT_TotalAns;


    public AnalysisDetaillistModel(String ChapterId, String Chapter, String PRT_TotalQnos,String CBT_TotalQnos,String PRT_TotalAns, String CBT_TotalAns) {
        this.ChapterId = ChapterId;
        this.Chapter = Chapter;
        this.PRT_TotalQnos = PRT_TotalQnos;
        this.PRT_TotalAns = PRT_TotalAns;
        this.CBT_TotalAns = CBT_TotalAns;
        this.CBT_TotalQnos = CBT_TotalQnos;
    }

    public String getChapterId() {
        return ChapterId;
    }

    public String getChapter() {
        return Chapter;
    }

    public String getPRT_TotalQnos() {
        return PRT_TotalQnos;
    }

    public String getCBT_TotalQnos() {
        return CBT_TotalQnos;
    }

    public String getPRT_TotalAns() {
        return PRT_TotalAns;
    }

    public String getCBT_TotalAns() {
        return CBT_TotalAns;
    }
}