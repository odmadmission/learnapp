package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class ToppersRequest implements Parcelable{

    private String SchoolId;
    private int ClassId;

    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public int getClassId() {
        return ClassId;
    }

    public void setClassId(int classId) {
        ClassId = classId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.SchoolId);
        dest.writeInt(this.ClassId);
    }

    public ToppersRequest() {
    }

    protected ToppersRequest(Parcel in) {
        this.SchoolId = in.readString();
        this.ClassId = in.readInt();
    }

    public static final Creator<ToppersRequest> CREATOR = new Creator<ToppersRequest>() {
        @Override
        public ToppersRequest createFromParcel(Parcel source) {
            return new ToppersRequest(source);
        }

        @Override
        public ToppersRequest[] newArray(int size) {
            return new ToppersRequest[size];
        }
    };
}
