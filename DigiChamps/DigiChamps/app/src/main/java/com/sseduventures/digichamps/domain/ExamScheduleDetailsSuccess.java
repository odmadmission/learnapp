package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/11/2018.
 */

public class ExamScheduleDetailsSuccess implements Parcelable {

    private String StartDate;
    private String TimeSlot;
    private String SubjectName;

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getTimeSlot() {
        return TimeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        TimeSlot = timeSlot;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.StartDate);
        dest.writeString(this.TimeSlot);
        dest.writeString(this.SubjectName);
    }

    public ExamScheduleDetailsSuccess() {
    }

    protected ExamScheduleDetailsSuccess(Parcel in) {
        this.StartDate = in.readString();
        this.TimeSlot = in.readString();
        this.SubjectName = in.readString();
    }

    public static final Creator<ExamScheduleDetailsSuccess> CREATOR = new Creator<ExamScheduleDetailsSuccess>() {
        @Override
        public ExamScheduleDetailsSuccess createFromParcel(Parcel source) {
            return new ExamScheduleDetailsSuccess(source);
        }

        @Override
        public ExamScheduleDetailsSuccess[] newArray(int size) {
            return new ExamScheduleDetailsSuccess[size];
        }
    };
}
