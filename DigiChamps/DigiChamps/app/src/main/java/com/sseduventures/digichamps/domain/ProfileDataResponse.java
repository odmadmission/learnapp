package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/2/2018.
 */

public class ProfileDataResponse implements Parcelable {


    private ProfileDataSuccess success;

    public ProfileDataSuccess getSuccess() {
        return success;
    }

    public void setSuccess(ProfileDataSuccess success) {
        this.success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.success, flags);
    }

    public ProfileDataResponse() {
    }

    protected ProfileDataResponse(Parcel in) {
        this.success = in.readParcelable(ProfileDataSuccess.class.getClassLoader());
    }

    public static final Creator<ProfileDataResponse> CREATOR = new Creator<ProfileDataResponse>() {
        @Override
        public ProfileDataResponse createFromParcel(Parcel source) {
            return new ProfileDataResponse(source);
        }

        @Override
        public ProfileDataResponse[] newArray(int size) {
            return new ProfileDataResponse[size];
        }
    };
}
