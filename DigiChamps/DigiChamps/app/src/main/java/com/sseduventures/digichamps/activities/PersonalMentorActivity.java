package com.sseduventures.digichamps.activities;

import android.app.Dialog;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.activity.PackageActivityNew;
import com.sseduventures.digichamps.domain.PMResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.frags.PersonalMentorActiveTask;
import com.sseduventures.digichamps.frags.PersonalMentorCompleted;
import com.sseduventures.digichamps.frags.PersonalMentorOverDueTask;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;
import java.util.List;

public class PersonalMentorActivity extends FormActivity implements ServiceReceiver.Receiver {


    private FloatingActionButton fab;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    boolean checkMentor = false;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private PMResponse success;
    private CollapsingToolbarLayout collapsing_toolbar;
    private AppBarLayout appBarLayout;
    private Toolbar toolbar;


    private TextView mentor_icon;




    private String teacherName, teacherImage, teacherMail, teacherPh, goal, remarks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personal_mentor);
        logEvent(LogEventUtil.KEY_Mentor_Page_PAGE,
                LogEventUtil.EVENT_Mentor_Page_PAGE);
        setModuleName(LogEventUtil.EVENT_Mentor_Page_PAGE);

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        appBarLayout = findViewById(R.id.appbar);
        collapsing_toolbar = findViewById(R.id.collapsing_toolbar);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mentor_icon = findViewById(R.id.mentor_icon);
        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        checkMentor = RegPrefManager.getInstance(this).getIsMentor();
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsing_toolbar.setTitle("Personal Mentor");
                    isShow = true;
                } else if (isShow) {
                    collapsing_toolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });





        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AppUtil.isInternetConnected(PersonalMentorActivity.this)) {

                    Intent intent = new Intent(PersonalMentorActivity.this, PersonalMentorChatActivity.class);
                    startActivity(intent);
                } else {
                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }
            }
        });

        if (checkMentor) {
            if (AppUtil.isInternetConnected(PersonalMentorActivity.this)) {
                getPmDetails();
            } else {
                AskOptionDialogNew("Please Check Your Internet Connection").show();
            }
        } else {

            dialog();
        }

        mentor_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (AppUtil.isInternetConnected(PersonalMentorActivity.this)) {

                    startActivity(new Intent(
                            PersonalMentorActivity.this,
                            MentorDetails.class).putExtra("TName", teacherName).putExtra("TImage", teacherImage)
                            .putExtra("TMail", teacherMail).putExtra("TPh", teacherPh).putExtra("Goal", goal)
                            .putExtra("Tremarks", remarks));

                } else {

                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }
            }
        });


    }


    private void dialog() {

        final Dialog dialog = new Dialog(PersonalMentorActivity.this);
        dialog.setContentView(R.layout.no_mentor_custom_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final String mentorVideo = RegPrefManager.getInstance(this).getMentorVideoLink();

        TextView how_it_works = dialog.findViewById(R.id.how_it_works);
        TextView subscribe = dialog.findViewById(R.id.subscribe);
        TextView goBack = dialog.findViewById(R.id.goBack);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        how_it_works.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mentorVideo)));
            }
        });


        subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtil.isInternetConnected(PersonalMentorActivity.this)) {
                    Intent in = new Intent(PersonalMentorActivity.this, PackageActivityNew.class);
                    startActivity(in);
                } else {
                    startActivity(new Intent(PersonalMentorActivity.this, Internet_Activity.class));
                }

            }
        });

        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtil.isInternetConnected(PersonalMentorActivity.this)) {
                    Intent intent = new Intent(PersonalMentorActivity.this, NewDashboardActivity.class);
                    startActivity(intent);
                } else {
                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }
            }
        });

        dialog.show();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new PersonalMentorActiveTask(), "Active Tasks");
        adapter.addFragment(new PersonalMentorOverDueTask(), "Over Due Tasks");
        adapter.addFragment(new PersonalMentorCompleted(), "Completed Tasks");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



    private void getPmDetails() {

        if (AppUtil.isInternetConnected(this)) {
            showDialog(null, "Please wait...");
            NetworkService.startActionGetPmDetails(this, mServiceReceiver);
        } else {
            showToast("No Internet Connection");
        }
    }

    @Override
    public void onBackPressed() {

        if (AppUtil.isInternetConnected(PersonalMentorActivity.this)) {

            Intent in = new Intent(PersonalMentorActivity.this, NewDashboardActivity.class);
            startActivity(in);
        } else {
            AskOptionDialogNew("Please Check Your Internet Connection").show();
        }
    }

    @Override
    protected void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                hideDialog();
                Intent in = new Intent(PersonalMentorActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                hideDialog();
                Intent intent = new Intent(PersonalMentorActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                hideDialog();
                Intent inin = new Intent(PersonalMentorActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:
                hideDialog();
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);



                if (success.getSuccessresultTask().getTeacherDetails() != null) {
                    teacherName = success.getSuccessresultTask()
                            .getTeacherDetails().getTeacherName();
                    teacherImage = success.getSuccessresultTask()
                            .getTeacherDetails().getTeacherProfilePicture();
                    teacherMail = success.getSuccessresultTask()
                            .getTeacherDetails().getTeacherEmail();
                    teacherPh = success.getSuccessresultTask()
                            .getTeacherDetails().getTeachermobile();
                    goal = success.getSuccessresultTask()
                            .getGoalDetails().getMessage();
                    remarks = success.getSuccessresultTask()
                            .getRemark();
                }else {
                    AskOptionDialogNew1
                            ("Oops, For some reason we have not been able to assign you your personal mentor yet. Please call us on 1800 212 4322")
                            .show();
                }

                break;


        }

    }


    public void showItemDialog(String taskName, String taskType, String taskDetails, String startTime, String endTime) {
        final Dialog dialog = new Dialog(PersonalMentorActivity.this);
        dialog.setContentView(R.layout.mentor_custom_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView title = dialog.findViewById(R.id.title);
        TextView taskT = dialog.findViewById(R.id.task_type);
        TextView detailsTask = dialog.findViewById(R.id.txt_task_details);
        TextView taskStart = dialog.findViewById(R.id.start_time);
        TextView taskEnd = dialog.findViewById(R.id.deadline_time);


        title.setText(taskName.trim());
        taskT.setText(taskType);
        detailsTask.setText(taskDetails.trim());
        taskStart.setText("Start Date: " + startTime);
        taskEnd.setText("End Date: " + endTime);


        dialog.show();

    }

    private android.app.AlertDialog AskOptionDialogNew(String msg) {
        android.app.AlertDialog myQuittingDialogBox = new android.app.AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })

                .create();
        return myQuittingDialogBox;
    }

    private android.app.AlertDialog AskOptionDialogNew1(String msg) {
        android.app.AlertDialog myQuittingDialogBox = new android.app.AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                       Intent intent=new Intent(PersonalMentorActivity.this,NewDashboardActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }
                })

                .create();
        return myQuittingDialogBox;
    }
}
