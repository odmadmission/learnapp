package com.sseduventures.digichamps.utils;

import android.graphics.Color;

/**
 * Created by user on 22-01-2018.
 */

public class Constants {



    public static final int[] MATERIAL_COLORS = {
            rgb("#689F38"), rgb("#FC0707"),
            rgb("#FF6F00")
    };
    public static int rgb(String hex) {
        int color = (int) Long.parseLong(hex.replace("#", ""), 16);
        int r = (color >> 16) & 0xFF;
        int g = (color >> 8) & 0xFF;
        int b = (color >> 0) & 0xFF;
        return Color.rgb(r, g, b);
    }
    public static String PACKAGENAME = "com.sseduventures.digichamps";

    public static final String APP_PREF =
            PACKAGENAME+".pref.digimentor";

    public static final String LOGIN =
            PACKAGENAME+".pref.data.login";

    public static final String FCM_REG_ID =
            PACKAGENAME+".pref.data.fcm_reg_id";



    public static final String TEACHER_ID =
            PACKAGENAME+".pref.data.teacher_id";

    public static final String IMAGE_BASE_URL
            ="http://thedigichamps.com/Images/ChatImages/";


    public static final int CHAT_OUT=1;
    public static final int CHAT_IN=2;

    public static final int CHAT_STATUS_WAIT=1;
    public static final int CHAT_STATUS_SENT=2;

    public static final int CHAT_MIME_TEXT=1;
    public static final int CHAT_MIME_MEDIA=2;

    public static final int SENDER_TEACHER=1;

    public static final int SENDER_STUDENT=2;


    public static String ACTION_SET_ALARM = PACKAGENAME+".action.alarm.register.sync";
    public static String REPORT_USAGE="Usage";
    public static String REPORT_TEST="Test";

    public static String REPORT_MODULE_VIDEO="Module_Video";
    public static String REPORT_MODULE_TEST="Module_Test";



    public static String BASE_URL = "https://thedigichamps.com/api/SchoolAPI/";
    public static String IMAGE_URL = "https://thedigichamps.com/";
    public static String SRV_URL = "https://thedigichamps.com/images/profile/";
    public static String GET_SCHOOL = "getschool";
    public static String EXAM_SCHEDULE = "ExamSchedules";
    public static String EXAM_SCHEDULE_BY_ID = "ExamScheduleByTypeId";
    public static String ADD_DISCUSSION = "AddDiscussion";
    public static String ADD_DISCUSSION_DETAIL = "AddDiscussionDetail";
    public static String GET_DISCUSSION_LIST = "DiscussionForumList";
    public static String HOMEWORKLIST = "HomeWorkList";
    public static String TIMETABLELIST = "TimeTableList";
    public static String TOPPERWAY = "ToppersWay";
    public static String NOTICELIST = "NoticeList";
    public static String CLASSLIST = "ClassList";
    public static String BOARDLIST = "BoardList";
    public static String LEADERBOARD = "LeadersBoardClassList";
    public static String STUDYMATERIAL = "StudyMaterialList";
    public static String DISCUSSION_DETAIL = "DiscussionForumDetail";
    //public static String SCHOOL_ID = "15AEAB67-3EC9-44AA-ADFA-F2807032BC2F";
   //public static String CLASS_ID = "1021";
   // public static String ROLE_ID = "0";
 //   public static String STUDENT_ID = "163";
    public static int PAGE_SIZE = 250;
   // public static String SECTION_ID = "83818445-C837-465D-9BD0-DCA38B8CBABD";
    public static String ASSIGNTEACHER = "GetAssignTeachers?schoolId=15AEAB67-3EC9-44AA-ADFA-F2807032BC2F&ClassId=1021&SectionId=83818445-C837-465D-9BD0-DCA38B8CBABD";


    public static String PROGRESS_VIDEO = "VIDEO";
    public static String PROGRESS_CBT = "CBT";
    public static String PROGRESS_PRT = "PRT";
    public static String PROGRESS_QB = "QB";
    public static String PROGRESS_SN = "SN";




}
