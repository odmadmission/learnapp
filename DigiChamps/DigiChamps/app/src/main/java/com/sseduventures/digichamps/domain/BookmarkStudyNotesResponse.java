package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 9/5/2018.
 */

public class BookmarkStudyNotesResponse implements Parcelable{

    private BookmarkStudyNotesSuccess SuccessBookmarkedStudynote;


    protected BookmarkStudyNotesResponse(Parcel in) {
        SuccessBookmarkedStudynote = in.readParcelable(BookmarkStudyNotesSuccess.class.getClassLoader());
    }

    public static final Creator<BookmarkStudyNotesResponse> CREATOR = new Creator<BookmarkStudyNotesResponse>() {
        @Override
        public BookmarkStudyNotesResponse createFromParcel(Parcel in) {
            return new BookmarkStudyNotesResponse(in);
        }

        @Override
        public BookmarkStudyNotesResponse[] newArray(int size) {
            return new BookmarkStudyNotesResponse[size];
        }
    };

    public BookmarkStudyNotesSuccess getSuccessBookmarkedStudynote() {
        return SuccessBookmarkedStudynote;
    }

    public void setSuccessBookmarkedStudynote(BookmarkStudyNotesSuccess successBookmarkedStudynote) {
        SuccessBookmarkedStudynote = successBookmarkedStudynote;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(SuccessBookmarkedStudynote, i);
    }
}
