package com.sseduventures.digichamps.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activity.DailyTTActivity;
import com.sseduventures.digichamps.activity.ExamScheduleActivity;
import com.sseduventures.digichamps.activity.MainActivity;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.utils.FontManage;


/**
 * Created by user on 06-12-2017.
 */

public class ScheduleFragment extends Fragment {

    Context mContext;
    RelativeLayout btnDailyExam,btnDailyTt;
    TextView lblDaily,ll_want,lblschool,lblupcoming,ll_want1,lblplan,llToolbar,click2,here2,click1,here1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_schedules, container, false);
        mContext = getActivity();
        if(getActivity() instanceof FormActivity)
        {
            ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_schedule_frag);
            ((FormActivity)getActivity()).logEvent(LogEventUtil.KEY_schedule_frag,
                    LogEventUtil.EVENT_schedule_frag);
        }
        lblDaily = view.findViewById(R.id.lblDaily);
        ll_want = view.findViewById(R.id.ll_want);
        lblschool = view.findViewById(R.id.lblschool);
        lblupcoming = view.findViewById(R.id.lblupcoming);
        ll_want1 = view.findViewById(R.id.ll_want1);
        lblplan = view.findViewById(R.id.lblplan);
        llToolbar = (TextView) view.findViewById(R.id.llToolbar);
        click2 = (TextView) view.findViewById(R.id.click2);
        here2 = (TextView) view.findViewById(R.id.here2);
        click1 = (TextView) view.findViewById(R.id.click1);
        here1 = (TextView) view.findViewById(R.id.here1);

        btnDailyTt = view.findViewById(R.id.btnDailyTt);
        btnDailyExam =  view.findViewById(R.id.btnDailyExam);
        btnDailyExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, ExamScheduleActivity.class);
                startActivity(i);
            }
        });
        btnDailyTt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, DailyTTActivity.class);
                startActivity(i);
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                    Intent i = new Intent(mContext, MainActivity.class);
                    mContext.startActivity(i);
                    return true;
                } else {
                    return false;
                }
            }
        });

        FontManage.setFontHeaderBold(mContext,llToolbar);
        FontManage.setFontTextLandingBold(mContext,ll_want);
        FontManage.setFontTextLandingBold(mContext,lblplan);
        FontManage.setFontTextLandingBold(mContext,lblschool);
        FontManage.setFontTextLandingBold(mContext,ll_want1);
        FontManage.setFontMeiryo(mContext,click1);
        FontManage.setFontMeiryo(mContext,here1);
        FontManage.setFontMeiryo(mContext,click2);
        FontManage.setFontMeiryo(mContext,here2);
        FontManage.setFontImpact(mContext,lblDaily);
        FontManage.setFontImpact(mContext,lblupcoming);

        return view;
    }


}
