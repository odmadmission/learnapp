package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sseduventures.digichamps.Model.DataModel_OrderDetails;
import com.sseduventures.digichamps.R;

import java.util.ArrayList;




public class OrderDetails_Adapter extends RecyclerView.Adapter<OrderDetails_Adapter.MyViewHolder> {

        private ArrayList<DataModel_OrderDetails> dataSet;
        private Context context;
        public int x;
        View view;
        int i = 0;
        CardView container;

        public static class MyViewHolder extends RecyclerView.ViewHolder {

            TextView package_name,sub,val,date,chap,val_upto;

            CardView container;

            public MyViewHolder(View view) {
                super(view);
                container = (CardView) itemView.findViewById(R.id.pack);
                package_name = (TextView) view.findViewById(R.id.tv_pkg_text);
                date = (TextView) view.findViewById(R.id.tv_date_text);
                sub = (TextView) view.findViewById(R.id.tv_sub_text);
                chap = (TextView) view.findViewById(R.id.tv_chap_text);
                val = (TextView) view.findViewById(R.id.tv_val_text);
                val_upto = (TextView) view.findViewById(R.id.tv_val_upto);

            }
        }


        public OrderDetails_Adapter(ArrayList<DataModel_OrderDetails> data, Context context) {
            this.context = context;
            this.dataSet = data;
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                                                         int viewType) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_orderdetails, parent, false);

            MyViewHolder myViewHolder = new MyViewHolder(view);
            return myViewHolder;
        }


        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

            TextView package_name_tv = holder.package_name;
            TextView date_tv = holder.date;
            TextView sub_tv = holder.sub;
            TextView val_tv = holder.val;
            TextView val_upto_tv = holder.val_upto;
            TextView chap_tv = holder.chap;

            package_name_tv.setText(dataSet.get(listPosition).getPackageName());
            date_tv.setText(dataSet.get(listPosition).getDate());
            sub_tv.setText(dataSet.get(listPosition).getSubjects());
            chap_tv.setText(dataSet.get(listPosition).getChapters());
            val_tv.setText(dataSet.get(listPosition).getVal());
            val_upto_tv.setText(dataSet.get(listPosition).getValUpto());


        }


        @Override
        public int getItemCount() {
            return dataSet.size();
        }


        public int count() {
            return x;
        }

        public ArrayList<DataModel_OrderDetails> getStudentist() {
            return dataSet;
        }


    }

