package com.sseduventures.digichamps.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.freshchat.consumer.sdk.Freshchat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sseduventures.digichamps.Model.MentorStudentChat;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.NewNotificationActivity;
import com.sseduventures.digichamps.activities.PersonalMentorActivity;
import com.sseduventures.digichamps.activities.PersonalMentorChatActivity;

import com.sseduventures.digichamps.activity.Order;
import com.sseduventures.digichamps.activity.PackageActivityNew;
import com.sseduventures.digichamps.activity.TicketDetailsActivity;
import com.sseduventures.digichamps.database.DbContract;
import com.sseduventures.digichamps.domain.CoinEarnDto;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.JsonDateDeserializer;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;



/**
 * Created by User25 on 10/11/2017.
 */

public class AppFCMService extends FirebaseMessagingService {
    private final static String TAG =AppFCMService.class.getSimpleName();
    PendingIntent pendingIntent;
    String imageName,imageForPackages,imageForVideo;

    //Realm Id
    private Integer id;


    private static AppFCMService instance;

    public static AppFCMService getInstance() {
        return instance;
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        //Log(TAG, remoteMessage.toString());
        Log.v(TAG,remoteMessage.toString());
        try {
            if (RegPrefManager.getInstance(this).getRegId() > 0) {
                //Log(TAG, remoteMessage.toString());
                if (Freshchat.isFreshchatNotification(remoteMessage)) {
                    Freshchat.getInstance(this).handleFcmMessage(remoteMessage);
                } else if (remoteMessage != null &&
                        remoteMessage.getData() != null &&
                        remoteMessage.getData().containsKey("dataMessage")) {


                    DataMessage dataMessage = new Gson().
                            fromJson(remoteMessage.
                                    getData().get("dataMessage"), DataMessage.class);
                    Log.v(TAG, dataMessage.toString());
                    if (dataMessage != null) {
                        if (dataMessage.getType() == 2) {
                            long value=Long.parseLong((String) dataMessage.getMessageObject());
                            if (RegPrefManager.getInstance(this)
                                    .getLoginId()!=value) {
                                Log.v(TAG, dataMessage.getMessageObject() + "");

                                RegPrefManager.getInstance(this).logout();

                                Intent i = new Intent();
                                i.setAction(IntentHelper.ACTION_LOGOUT_NOTIFI);
                                LocalBroadcastManager.getInstance(this)
                                        .sendBroadcast(i);
                            }

                        } else {
                            if (dataMessage.getType() == 3) {

                                    //Log(TAG, dataMessage.getMessageObject() + "");
                                CoinEarnDto msc = new Gson().
                                        fromJson(new Gson().
                                                        toJson(dataMessage.
                                                                getMessageObject()),
                                                CoinEarnDto.class);
                                    RegPrefManager.getInstance(this).setCoinCount(
                                            RegPrefManager.getInstance(this)
                                            .getCoinCount()+30
                                    );

                                    Intent i = new Intent();
                                    i.setAction(IntentHelper.ACTION_COIN_RELOAD);
                                    LocalBroadcastManager.getInstance(this)
                                            .sendBroadcast(i);
                                saveNotification(
                                        "Refer friend",
                                        msc.getName()+" registered successfully.",
                                        msc.getCoins()+"",
                                        NotificationType.REG_REFERRAL + "",
                                        System.currentTimeMillis() + "");
                                showNotificationMessage("Refer friend",
                                        msc.getName()+" registered successfully.",
                                        NewNotificationActivity.class, 125);


                            }
                            else {
                                Gson gson = new GsonBuilder()
                                        .registerTypeAdapter(Date.class,
                                                new JsonDateDeserializer())

                                        .create();
                                NotificationMessage notificationMessage = gson.fromJson(
                                        gson.toJson(dataMessage.getNotificationObject()),
                                        NotificationMessage.class);


                                MentorStudentChat msc = gson.
                                        fromJson(gson.toJson(dataMessage.getMessageObject()),
                                                MentorStudentChat.class);


                                if (msc != null) {
                                    SharedPreferences pf = getSharedPreferences("paper_zone",
                                            MODE_PRIVATE);
                                    int chat = pf.getInt("chat_opened", 0);
                                    if (chat == 0) {
                                        //notifications
                                        String title = "New Mentor Message";
                                        String text = msc.getMessage();


                                        showNotificationMessage(title, text,
                                                PersonalMentorChatActivity.class, 129);
                                    }

                                    Intent intent = new Intent();
                                    intent.setAction(IntentHelper.ACTION_NEW_MESSAGE);
                                    intent.putExtra("msc", msc);
                                    LocalBroadcastManager.getInstance(this).
                                            sendBroadcast(intent);
                                }
                            }
                        }


                    }
                } else {

                    if (
                            remoteMessage.getData() == null)
                        return;
                  //  Log.d(TAG, "From: " + remoteMessage.getFrom());
                    //Log.d(TAG, "Message data payload: " + remoteMessage.getData());
                    // Log.d(TAG, "Message data payloads: " + remoteMessage.getNotification());
                    //  Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
                    // String imageUri = remoteMessage.getData().get("image");

                    // Uri Str1 = remoteMessage.getNotification().getLink();

                    String strIcon = null;

                    String Str = remoteMessage.getData().get("body");
                    String[] tmp;
                    tmp = Str.split("#");

                    String type = null;
                    String data = null;
                    String title = null;
                    String message = null;
                    if (tmp != null && tmp.length > 0) {

                        type = tmp[0];

                        if (type.equals("ER")) {
                            type = tmp[0];
                            data = tmp[1];
                            title = tmp[2];
                            message = tmp[3];
                            //  String title = "Test Submitted";
                            //String message = "Hello " + temp3 + "! You've successfully submitted the test. Analyze your performance, " +
                            //      "see where you stand in the leaderboard and check on your weak sub-concepts.";

                            saveNotification(
                                    title,
                                    message,
                                    data,
                                    NotificationType.TEST_RESULT + "",
                                    System.currentTimeMillis() + "");
                            showNotificationMessage(title, message,
                                    NewNotificationActivity.class, 125);
                            // NotificationUtil.showRegistrationReferralNotification(this,title,message,);


                            ////Log(TAG, title + message + ExamId + temp3 + packageId);

                            // DbTask dbTask=new DbTask(this);
                            //  dbTask.execute(title,message);


                        } else if (type.equals("ORID")) {
                            type = tmp[0];
                            data = tmp[1];
                            title = tmp[2];
                            message = tmp[3];
                            // title = "Order Confirmed";
                            // message = "Your Package is active now. Now explore more with the app.";

                            saveNotification(
                                    title,
                                    message,
                                    data,
                                    NotificationType.ORDER_CONFIRM + "",
                                    System.currentTimeMillis() + "");
                            showNotificationMessage(title, message,
                                    Order.class, 127);


                        } else if (type.equals("DBTANS")) {



                            type = tmp[0];
                            data = tmp[1];
                            title = tmp[3];
                            message = tmp[4];
                            // title = "Task Completed";
                            // message = "Good Going! Keep practicing more";
                            RegPrefManager.getInstance(this).setCoinCount(
                                    RegPrefManager.getInstance(this).getCoinCount()+
                                            Integer.parseInt(tmp[2])
                            );


                            // title = "Doubt Answered";
                            // message = "Your cool DIGIGuru just solved your doubt. " +
//                             "Time to get some concepts cleared.";
                            Intent intent = new Intent();
                            intent.setAction(IntentHelper.ACTION_COIN_RELOAD);
                            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                            saveNotification(
                                    title,
                                    message,
                                    data,
                                    NotificationType.DOUBT_ANSWER + "",
                                    System.currentTimeMillis() + "");
                            showNotificationMessageDbtAns(title, message,
                                    TicketDetailsActivity.class, 125,data);

                        }
                        else if (type.equals("DBTRPLY")) {

                            type = tmp[0];
                            data = tmp[1];
                            title = tmp[2];
                            message = tmp[3];

                            saveNotification(
                                    title,
                                    message,
                                    data,
                                    NotificationType.DOUBT_ANSWER + "",
                                    System.currentTimeMillis() + "");
                            showNotificationMessageDbtAns(title, message,
                                    TicketDetailsActivity.class, 125,data);

                        }else if (type.equals("FD")) {

                            type = tmp[0];
                            data = tmp[1];
                            title = tmp[2];
                            message = tmp[3];
                            // title = "Knock Knock";
                            // message = tmp[3];
                            // //Log(TAG, title + msg + temp3 + NotificationType.NEW_FEED + System.currentTimeMillis());

                            strIcon = remoteMessage.getData().get("icon");
                            imageName = "https://thedigichamps.com/Images/Feed/" + strIcon;
                            // imageForPackages = "https://thedigichamps.com/Images/" + strIcon;
                            // imageForVideo = strIcon;
                            saveNotification(
                                    title,
                                    message,
                                    data,
                                    NotificationType.NEW_FEED + "",
                                    System.currentTimeMillis() + "");
                            showNotificationBigImage(title, message, imageName);

                        } else if (type.equals("PKG")) {

                            type = tmp[0];
                            data = tmp[1];
                            title = tmp[2];
                            message = tmp[3];
                            //  title = "New Package";
                            //  message = "Check out our new package.It might have something interesting for you.";

                            saveNotification(
                                    title,
                                    message,
                                    data,
                                    NotificationType.NEW_PACKAGE + "",
                                    System.currentTimeMillis() + "");

                            showNotificationMessage(title, message,
                                    PackageActivityNew.class, 125);
                        } else if (type.equals("NVID")) {
                            type = tmp[0];
                            data = tmp[1];
                            title = tmp[2];
                            message = tmp[3];
                            // title = "New Video";
                            // message = "Don't miss the new video. Check it out";
                            strIcon = remoteMessage.getData().get("icon");
                            //imageName = "https://thedigichamps.com/Images/Feed/" + strIcon;
                            // imageForPackages = "https://thedigichamps.com/Images/" + strIcon;
                            imageForVideo = strIcon;
                            saveNotification(
                                    title,
                                    message,
                                    data,
                                    NotificationType.NEW_VIDEO + "",
                                    System.currentTimeMillis() + "");


                            showNotificationBigImage(title, message, imageForVideo);
                        } else if (type.equals("NMNTR")) {

                            // title = "New Mentor";
                            // message = "Your Personal Mentor is now assigned to you. " +
                            //      "Know more about him and Bon Voyage!";
                            type = tmp[0];
                            data = tmp[1];
                            title = tmp[2];
                            message = tmp[3];
                            saveNotification(
                                    title,
                                    message,
                                    data,
                                    NotificationType.MENTOR_ASSIGN + "",
                                    System.currentTimeMillis() + "");

                            showNotificationMessage(title, message,
                                    NewNotificationActivity.class, 125);
                        } else if (type.equals("NTSK")) {

                            //Log(TAG, "NTSk");
                            // title = "New Task";
                            //// message = "Your Personal Mentor just added  a new task. Take a look, " +
                            //      " get onboard and don't miss the deadline";

                            type = tmp[0];
                            data = tmp[1];
                            title = tmp[2];
                            message = tmp[3];
                            saveNotification(
                                    title,
                                    message,
                                    data,
                                    NotificationType.NEW_MENTOR_TASK + "",
                                    System.currentTimeMillis() + "");

                            showNotificationMessage(title, message,
                                    PersonalMentorActivity.class, 125);

                        } else if (type.contains("TSKU")) {
                            type = tmp[0];
                            data = tmp[1];
                            title = tmp[3];
                            message = tmp[4];
                            // title = "Task Completed";
                            // message = "Good Going! Keep practicing more";
                            RegPrefManager.getInstance(this).setCoinCount(
                                    RegPrefManager.getInstance(this).getCoinCount()+
                                            Integer.parseInt(tmp[2])
                            );

                            Intent intent = new Intent();
                            intent.setAction(IntentHelper.ACTION_COIN_RELOAD);
                            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                            saveNotification(
                                    title,
                                    message,
                                    data,
                                    NotificationType.UPDATE_MENTOR_TASK + "",
                                    System.currentTimeMillis() + "");

                            showNotificationMessage(title, message,
                                    PersonalMentorActivity.class, 125);
                        } else if (type.equals("TSKO")) {

                            // title = "Task Overdue";
                            // message = "Hello! Your task got overdued.  Complete it soon.";
                            type = tmp[0];
                            data = tmp[1];
                            title = tmp[2];
                            message = tmp[3];
                            saveNotification(
                                    title,
                                    message,
                                    data,
                                    NotificationType.OVERDUE_MENTOR_TASK + "",
                                    System.currentTimeMillis() + "");

                            showNotificationMessage(title, message,
                                    PersonalMentorActivity.class, 125);
                        } else {
                            type = tmp[0];


                           // if(type.equals("New Notification")) {
                                data = tmp[1];
                                //Log(TAG,data);
                                title = "Notification";
                                saveNotification(
                                        title,
                                        data,
                                        data,
                                        NotificationType.BACKEND_MESSAGE + "",
                                        System.currentTimeMillis() + "");

                                showNotificationMessage(title, data,
                                        NewNotificationActivity.class, 130);

//                            }
                        }

                    }
                }

            }
        }
        catch (Exception e)
        {

        }

    }


    private void showNotificationBigImage(String title1,
                                          String message,
                                          String image) {
        Intent intent = new Intent(this, NewNotificationActivity.class);
        intent.putExtra("notify",1);
        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        String name = getResources().getString(R.string.appname)
                +System.currentTimeMillis();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            String description = name+" desc";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new
                    NotificationChannel(
                    name,
                    name,
                    importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        Bitmap notifyImage = getBitmapFromURL(image);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this,name)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title1)
                .setContentText(message.trim().toString())
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(notifyImage).setBigContentTitle(message.trim().toString()))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(104, notificationBuilder.build());
    }



    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void showNotificationMessage(String title1,
                                         String msg,
                                         Class cls,int code) {

        Intent intent = new Intent(this, cls);
        intent.putExtra("notify",1);
        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        String name = getResources().getString(R.string.appname)
                +System.currentTimeMillis();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            String description = name+" desc";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new
                    NotificationChannel(
                    name,
                    name,
                    importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        //Bitmap notifyImage = getBitmapFromURL(imageForPackages);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this,name)
                        .setSmallIcon(R.drawable.logo_digichamps)
                .setContentTitle(title1)
                .setColor(Color.parseColor("#2CB3E1"))
                .setContentText(msg.trim().toString())
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg.trim().toString()))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(code, notificationBuilder.build());
    }

    private void showNotificationMessageDbtAns(String title1,
                                         String msg,
                                         Class cls,int code, String data) {

        Intent intent = new Intent(this, cls);
        intent.putExtra("notify",1);
        intent.putExtra("activity","Ticket_Reply");
        intent.putExtra("ticket_id",data);

        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        String name = getResources().getString(R.string.appname)
                +System.currentTimeMillis();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            String description = name+" desc";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new
                    NotificationChannel(
                    name,
                    name,
                    importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        Bitmap notifyImage = getBitmapFromURL(imageForPackages);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this,name)
                        .setSmallIcon(R.drawable.logo_digichamps)
                        .setContentTitle(title1)
                        .setColor(Color.parseColor("#2CB3E1"))
                        .setContentText(msg.trim().toString())
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(msg.trim().toString()))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(code, notificationBuilder.build());
    }



    private void saveNotification(String... strings)
    {
        ContentValues values=new ContentValues();
        values.put(DbContract.NotificationTable.TITLE,
                strings[0]);
        values.put(DbContract.NotificationTable.MESSAGE,strings[1]);
        values.put(DbContract.NotificationTable.DATA,strings[2]);
        values.put(DbContract.NotificationTable.TYPE,strings[3]);
        values.put(DbContract.NotificationTable.INSERTED_DATE,strings[4]);


        //Log(TAG,values.toString());
        Uri mUri= getContentResolver().
                insert(DbContract.NotificationTable.CONTENT_URI,values);

        if(mUri!=null)
        {
            //Log(TAG,mUri.getLastPathSegment()+"");
        }
    }

}
