package com.sseduventures.digichamps.Model;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Package_Model implements Parcelable {

    private String offer_name;
    private String month;
    private String chap;
    private String status;
    private String off_price;
    private String main_price;
    private int disc_price;
    private String package_name;
    private String package_id;
    private String package_desc;
    private String Disc_Percent;
    private String thumbnail;
    private ArrayList<Integer> chapters;
    public Package_Model(String month, String chap, String status, String off_price, String main_price,
                         String package_name, String package_id, String package_desc,
                         String Disc_Percent,String thumbnail,int disc_price,ArrayList<Integer> chapters) {

        this.month= month;
        this.chap = chap;
        this.status = status;
        this.off_price = off_price;
        this.main_price = main_price;
        this.package_name = package_name;
        this.package_id = package_id;
        this.package_desc = package_desc;
        this.Disc_Percent = Disc_Percent;
        this.disc_price = disc_price;
        this.thumbnail = thumbnail;
        this.chapters=chapters;


    }

    public String getMonth() {
        return month;
    }

    public String getchap() {
        return chap;
    }

    public String getStatus() {
        return status;
    }

    public String getOff_price() {
        return off_price;
    }

    public String getMain_price() {
        return main_price;
    }


    public String getpackage_id() {
        return package_id;
    }

    public String getpackage_name() {
        return package_name;
    }

    public String getpackage_desc() {
        return package_desc;
    }

    public String getDisc_Percent() {
        return Disc_Percent;
    }

    public String getThumbnail() {
        return thumbnail;
    }


    public int getDisc_price() {
        return disc_price;
    }

    public void setDisc_price(int disc_price) {
        this.disc_price = disc_price;
    }

    protected Package_Model(Parcel in) {
        offer_name = in.readString();
        month = in.readString();
        chap = in.readString();
        status = in.readString();
        off_price = in.readString();
        main_price = in.readString();
        package_name = in.readString();
        package_id = in.readString();
        package_desc = in.readString();
        Disc_Percent = in.readString();
        thumbnail = in.readString();
        disc_price = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(offer_name);
        dest.writeString(month);
        dest.writeString(chap);
        dest.writeString(status);
        dest.writeString(off_price);
        dest.writeString(main_price);
        dest.writeString(package_name);
        dest.writeString(package_id);
        dest.writeString(package_desc);
        dest.writeString(Disc_Percent);
        dest.writeString(thumbnail);
        dest.writeInt(disc_price);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Package_Model> CREATOR = new Parcelable.Creator<Package_Model>() {
        @Override
        public Package_Model createFromParcel(Parcel in) {
            return new Package_Model(in);
        }

        @Override
        public Package_Model[] newArray(int size) {
            return new Package_Model[size];
        }
    };
}