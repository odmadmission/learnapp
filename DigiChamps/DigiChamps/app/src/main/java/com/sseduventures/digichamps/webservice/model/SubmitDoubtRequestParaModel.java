package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.nio.file.Path;

public class SubmitDoubtRequestParaModel implements Parcelable {

    public SubmitDoubtRequestParaModel() {

    }

    public static final Creator<SubmitDoubtRequestParaModel> CREATOR = new Creator<SubmitDoubtRequestParaModel>() {
        @Override
        public SubmitDoubtRequestParaModel createFromParcel(Parcel in) {
            return new SubmitDoubtRequestParaModel();
        }

        @Override
        public SubmitDoubtRequestParaModel[] newArray(int size) {
            return new SubmitDoubtRequestParaModel[size];
        }
    };

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    public String getClass_id() {
        return Class_id;
    }

    public void setClass_id(String class_id) {
        Class_id = class_id;
    }

    public String getSubject_ID() {
        return Subject_ID;
    }

    public void setSubject_ID(String subject_ID) {
        Subject_ID = subject_ID;
    }

    public String getBoard_id() {
        return Board_id;
    }

    public void setBoard_id(String board_id) {
        Board_id = board_id;
    }

    public String getChapter_ID() {
        return Chapter_ID;
    }

    public void setChapter_ID(String chapter_ID) {
        Chapter_ID = chapter_ID;
    }

    public String getQuestion_Detail() {
        return Question_Detail;
    }

    public void setQuestion_Detail(String question_Detail) {
        Question_Detail = question_Detail;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    private String regId;
    private String Class_id;
    private String Subject_ID;
    private String Board_id;
    private String Chapter_ID;
    private String Question_Detail;
    private String Image;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(regId);
        parcel.writeString(Class_id);
        parcel.writeString(Subject_ID);
        parcel.writeString(Board_id);
        parcel.writeString(Chapter_ID);
        parcel.writeString(Question_Detail);
        parcel.writeString(Image);
    }
}
