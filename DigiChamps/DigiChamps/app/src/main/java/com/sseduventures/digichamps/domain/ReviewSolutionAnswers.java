package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/5/2018.
 */

public class ReviewSolutionAnswers implements Parcelable {

    private int UserAnswerId;
    private int CorrectAnswerId;
    private String Answer_desc;
    private String Answer_Image;

    public int getUserAnswerId() {
        return UserAnswerId;
    }

    public void setUserAnswerId(int userAnswerId) {
        UserAnswerId = userAnswerId;
    }

    public int getCorrectAnswerId() {
        return CorrectAnswerId;
    }

    public void setCorrectAnswerId(int correctAnswerId) {
        CorrectAnswerId = correctAnswerId;
    }

    public String getAnswer_desc() {
        return Answer_desc;
    }

    public void setAnswer_desc(String answer_desc) {
        Answer_desc = answer_desc;
    }

    public String getAnswer_Image() {
        return Answer_Image;
    }

    public void setAnswer_Image(String answer_Image) {
        Answer_Image = answer_Image;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.UserAnswerId);
        dest.writeInt(this.CorrectAnswerId);
        dest.writeString(this.Answer_desc);
        dest.writeString(this.Answer_Image);
    }

    public ReviewSolutionAnswers() {
    }

    protected ReviewSolutionAnswers(Parcel in) {
        this.UserAnswerId = in.readInt();
        this.CorrectAnswerId = in.readInt();
        this.Answer_desc = in.readString();
        this.Answer_Image = in.readString();
    }

    public static final Creator<ReviewSolutionAnswers> CREATOR = new Creator<ReviewSolutionAnswers>() {
        @Override
        public ReviewSolutionAnswers createFromParcel(Parcel source) {
            return new ReviewSolutionAnswers(source);
        }

        @Override
        public ReviewSolutionAnswers[] newArray(int size) {
            return new ReviewSolutionAnswers[size];
        }
    };
}
