package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class NoticesList implements Parcelable{

    private String createdDate;
    private String fileURL;
    private String description;
    private String title;

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.createdDate);
        dest.writeString(this.fileURL);
        dest.writeString(this.description);
        dest.writeString(this.title);
    }

    public NoticesList() {
    }

    protected NoticesList(Parcel in) {
        this.createdDate = in.readString();
        this.fileURL = in.readString();
        this.description = in.readString();
        this.title = in.readString();
    }

    public static final Creator<NoticesList> CREATOR = new Creator<NoticesList>() {
        @Override
        public NoticesList createFromParcel(Parcel source) {
            return new NoticesList(source);
        }

        @Override
        public NoticesList[] newArray(int size) {
            return new NoticesList[size];
        }
    };
}
