package com.sseduventures.digichamps.scholarship;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/9/2018.
 */

public class ScratchSuccess implements Parcelable
{

    private ScholarShip scratch;
    private int cart;

    public ScholarShip getScratch() {
        return scratch;
    }

    public void setScratch(ScholarShip scratch) {
        this.scratch = scratch;
    }

    public int getCart() {
        return cart;
    }

    public void setCart(int cart) {
        this.cart = cart;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.scratch, flags);
        dest.writeInt(this.cart);
    }

    public ScratchSuccess() {
    }

    protected ScratchSuccess(Parcel in) {
        this.scratch = in.readParcelable(ScholarShip.class.getClassLoader());
        this.cart = in.readInt();
    }

    public static final Creator<ScratchSuccess> CREATOR = new Creator<ScratchSuccess>() {
        @Override
        public ScratchSuccess createFromParcel(Parcel source) {
            return new ScratchSuccess(source);
        }

        @Override
        public ScratchSuccess[] newArray(int size) {
            return new ScratchSuccess[size];
        }
    };
}
