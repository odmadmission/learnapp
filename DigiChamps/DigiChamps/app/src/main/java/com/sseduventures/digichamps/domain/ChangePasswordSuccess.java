package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/2/2018.
 */

public class ChangePasswordSuccess implements Parcelable
{
    public ChangePasswordResponse success;

    public ChangePasswordResponse getSuccess() {
        return success;
    }

    public void setSuccess(ChangePasswordResponse success) {
        this.success = success;
    }

    public ChangePasswordSuccess() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.success, flags);
    }

    protected ChangePasswordSuccess(Parcel in) {
        this.success = in.readParcelable(ChangePasswordResponse.class.getClassLoader());
    }

    public static final Creator<ChangePasswordSuccess> CREATOR = new Creator<ChangePasswordSuccess>() {
        @Override
        public ChangePasswordSuccess createFromParcel(Parcel source) {
            return new ChangePasswordSuccess(source);
        }

        @Override
        public ChangePasswordSuccess[] newArray(int size) {
            return new ChangePasswordSuccess[size];
        }
    };
}
