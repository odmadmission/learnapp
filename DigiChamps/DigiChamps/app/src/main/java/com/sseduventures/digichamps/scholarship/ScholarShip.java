package com.sseduventures.digichamps.scholarship;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class ScholarShip implements Parcelable
    {

        public int ID;

        public int Discount;
        public int Validity;
        public boolean Active;
        public boolean Deleted;
        public Date InsertDate;

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public int getDiscount() {
            return Discount;
        }

        public void setDiscount(int discount) {
            Discount = discount;
        }

        public int getValidity() {
            return Validity;
        }

        public void setValidity(int validity) {
            Validity = validity;
        }

        public boolean isActive() {
            return Active;
        }

        public void setActive(boolean active) {
            Active = active;
        }

        public boolean isDeleted() {
            return Deleted;
        }

        public void setDeleted(boolean deleted) {
            Deleted = deleted;
        }

        public Date getInsertDate() {
            return InsertDate;
        }

        public void setInsertDate(Date insertDate) {
            InsertDate = insertDate;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.ID);
            dest.writeInt(this.Discount);
            dest.writeInt(this.Validity);
            dest.writeByte(this.Active ? (byte) 1 : (byte) 0);
            dest.writeByte(this.Deleted ? (byte) 1 : (byte) 0);
            dest.writeLong(this.InsertDate != null ? this.InsertDate.getTime() : -1);
        }

        public ScholarShip() {
        }

        protected ScholarShip(Parcel in) {
            this.ID = in.readInt();
            this.Discount = in.readInt();
            this.Validity = in.readInt();
            this.Active = in.readByte() != 0;
            this.Deleted = in.readByte() != 0;
            long tmpInsertDate = in.readLong();
            this.InsertDate = tmpInsertDate == -1 ? null : new Date(tmpInsertDate);
        }

        public static final Creator<ScholarShip> CREATOR = new Creator<ScholarShip>() {
            @Override
            public ScholarShip createFromParcel(Parcel source) {
                return new ScholarShip(source);
            }

            @Override
            public ScholarShip[] newArray(int size) {
                return new ScholarShip[size];
            }
        };
    }