package com.sseduventures.digichamps.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.intentpicker.IntentPickerSheetView;

import java.util.Collections;
import java.util.Comparator;

public class ShareWithFrndBottonSheet extends FormActivity {


    protected BottomSheetLayout bottomSheetLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_with_frnd_botton_sheet);

                logEvent(LogEventUtil.KEY_ShareWithFriends_PAGE,
                LogEventUtil.EVENT_ShareWithFriends_PAGE);
                setModuleName(LogEventUtil.EVENT_ShareWithFriends_PAGE);
        bottomSheetLayout = (BottomSheetLayout) findViewById(R.id.bottomsheet);

    }


    public void openBottomSheet(){

        String referUrl = RegPrefManager.getInstance(this).getFamilyInviteLink();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);


        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, referUrl);
        shareIntent.setType("text/plain");
        IntentPickerSheetView intentPickerSheet = new IntentPickerSheetView(
                this, shareIntent, "Share with...", new IntentPickerSheetView.OnIntentPickedListener() {
            @Override
            public void onIntentPicked(IntentPickerSheetView.ActivityInfo activityInfo) {
                bottomSheetLayout.dismissSheet();
                startActivity(activityInfo.getConcreteIntent(shareIntent));
            }
        });
        // Filter out built in sharing options such as bluetooth and beam.
        intentPickerSheet.setFilter(new IntentPickerSheetView.Filter() {
            @Override
            public boolean include(IntentPickerSheetView.ActivityInfo info) {
                return !info.componentName.getPackageName().startsWith("com.android");
            }
        });
        // Sort activities in reverse order for no good reason
        intentPickerSheet.setSortMethod(new Comparator<IntentPickerSheetView.ActivityInfo>() {
            @Override
            public int compare(IntentPickerSheetView.ActivityInfo lhs, IntentPickerSheetView.ActivityInfo rhs) {
                return rhs.label.compareTo(lhs.label);
            }
        });

        // Add custom mixin example
        Drawable customDrawable = ResourcesCompat.getDrawable(getResources(),
                R.mipmap.ic_launcher, null);
        IntentPickerSheetView.ActivityInfo customInfo =
                new IntentPickerSheetView.ActivityInfo(customDrawable, "Custom mix-in",
                        this, NewDashboardActivity.class);
        intentPickerSheet.setMixins(Collections.singletonList(customInfo));

        bottomSheetLayout.showWithSheetView(intentPickerSheet);
    }
}
