package com.sseduventures.digichamps.Model;

/**
 * Created by User24 on 10/20/2017.
 */

public class DataModel_Faq {

    private String qstnNumber, question, answer;


    public DataModel_Faq(String qstnNumber, String question, String answer) {
        this.qstnNumber = qstnNumber;
        this.question = question;
        this.answer = answer;

    }

    public String getQstnNumber() {
        return qstnNumber;
    }

    public void setQstnNumber(String qstnNumber) {
        this.qstnNumber = qstnNumber;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }
    public  void setAnswer(String answer){
        this.answer = answer;
    }
}
