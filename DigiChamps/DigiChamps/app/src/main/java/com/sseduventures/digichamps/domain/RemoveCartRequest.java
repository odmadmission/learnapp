package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/5/2018.
 */

public class RemoveCartRequest implements Parcelable
{

 private RemoveCartPackageList deleteitem;

    public RemoveCartPackageList getDeleteitem() {
        return deleteitem;
    }

    public void setDeleteitem(RemoveCartPackageList deleteitem) {
        this.deleteitem = deleteitem;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.deleteitem, flags);
    }

    public RemoveCartRequest() {
    }

    protected RemoveCartRequest(Parcel in) {
        this.deleteitem = in.readParcelable(RemoveCartPackageList.class.getClassLoader());
    }

    public static final Creator<RemoveCartRequest> CREATOR = new Creator<RemoveCartRequest>() {
        @Override
        public RemoveCartRequest createFromParcel(Parcel source) {
            return new RemoveCartRequest(source);
        }

        @Override
        public RemoveCartRequest[] newArray(int size) {
            return new RemoveCartRequest[size];
        }
    };
}
