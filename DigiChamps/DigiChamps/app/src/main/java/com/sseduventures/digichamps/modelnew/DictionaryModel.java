package com.sseduventures.digichamps.modelnew;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class DictionaryModel implements Parcelable
{
    private long Dictionary_ID;
    private String Dictionary_WordName;
    private String Dictionary_Images;
    private String Dictionary_Path;
    private String Dictionary_Description;
    private Date Inserted_On;
    private String Inserted_By;
    private Date Modified_Date;
    private String Modified_By;
    private Boolean Is_Active;
    private Boolean Is_Delete;


    public long getDictionary_ID() {
        return Dictionary_ID;
    }

    public void setDictionary_ID(long dictionary_ID) {
        Dictionary_ID = dictionary_ID;
    }

    public String getDictionary_WordName() {
        return Dictionary_WordName;
    }

    public void setDictionary_WordName(String dictionary_WordName) {
        Dictionary_WordName = dictionary_WordName;
    }

    public String getDictionary_Images() {
        return Dictionary_Images;
    }

    public void setDictionary_Images(String dictionary_Images) {
        Dictionary_Images = dictionary_Images;
    }

    public String getDictionary_Path() {
        return Dictionary_Path;
    }

    public void setDictionary_Path(String dictionary_Path) {
        Dictionary_Path = dictionary_Path;
    }

    public String getDictionary_Description() {
        return Dictionary_Description;
    }

    public void setDictionary_Description(String dictionary_Description) {
        Dictionary_Description = dictionary_Description;
    }

    public Date getInserted_On() {
        return Inserted_On;
    }

    public void setInserted_On(Date inserted_On) {
        Inserted_On = inserted_On;
    }

    public String getInserted_By() {
        return Inserted_By;
    }

    public void setInserted_By(String inserted_By) {
        Inserted_By = inserted_By;
    }

    public Date getModified_Date() {
        return Modified_Date;
    }

    public void setModified_Date(Date modified_Date) {
        Modified_Date = modified_Date;
    }

    public String getModified_By() {
        return Modified_By;
    }

    public void setModified_By(String modified_By) {
        Modified_By = modified_By;
    }

    public Boolean getIs_Active() {
        return Is_Active;
    }

    public void setIs_Active(Boolean is_Active) {
        Is_Active = is_Active;
    }

    public Boolean getIs_Delete() {
        return Is_Delete;
    }

    public void setIs_Delete(Boolean is_Delete) {
        Is_Delete = is_Delete;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.Dictionary_ID);
        dest.writeString(this.Dictionary_WordName);
        dest.writeString(this.Dictionary_Images);
        dest.writeString(this.Dictionary_Path);
        dest.writeString(this.Dictionary_Description);
        dest.writeLong(this.Inserted_On != null ? this.Inserted_On.getTime() : -1);
        dest.writeString(this.Inserted_By);
        dest.writeLong(this.Modified_Date != null ? this.Modified_Date.getTime() : -1);
        dest.writeString(this.Modified_By);
        dest.writeValue(this.Is_Active);
        dest.writeValue(this.Is_Delete);
    }

    public DictionaryModel() {
    }

    protected DictionaryModel(Parcel in) {
        this.Dictionary_ID = in.readLong();
        this.Dictionary_WordName = in.readString();
        this.Dictionary_Images = in.readString();
        this.Dictionary_Path = in.readString();
        this.Dictionary_Description = in.readString();
        long tmpInserted_On = in.readLong();
        this.Inserted_On = tmpInserted_On == -1 ? null : new Date(tmpInserted_On);
        this.Inserted_By = in.readString();
        long tmpModified_Date = in.readLong();
        this.Modified_Date = tmpModified_Date == -1 ? null : new Date(tmpModified_Date);
        this.Modified_By = in.readString();
        this.Is_Active = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.Is_Delete = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Creator<DictionaryModel> CREATOR = new Creator<DictionaryModel>() {
        @Override
        public DictionaryModel createFromParcel(Parcel source) {
            return new DictionaryModel(source);
        }

        @Override
        public DictionaryModel[] newArray(int size) {
            return new DictionaryModel[size];
        }
    };
}
