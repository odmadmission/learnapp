package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by NISHIKANT on 6/5/2018.
 */

public class CartPackageWrapper implements Parcelable
{
    private ArrayList<CartPackage> cartdata;


    private int Original_price;
    private int discountprice;
    private int payblamt;
    private int taxx;
    private int cart_item;
    private int discountpercentage;
    private int discounted_price;
    private String couponname;

    public ArrayList<CartPackage> getCartdata() {
        return cartdata;
    }

    public void setCartdata(ArrayList<CartPackage> cartdata) {
        this.cartdata = cartdata;
    }

    public int getOriginal_price() {
        return Original_price;
    }

    public void setOriginal_price(int original_price) {
        Original_price = original_price;
    }

    public int getDiscountprice() {
        return discountprice;
    }

    public void setDiscountprice(int discountprice) {
        this.discountprice = discountprice;
    }

    public int getPayblamt() {
        return payblamt;
    }

    public void setPayblamt(int payblamt) {
        this.payblamt = payblamt;
    }

    public int getTaxx() {
        return taxx;
    }

    public void setTaxx(int taxx) {
        this.taxx = taxx;
    }

    public int getCart_item() {
        return cart_item;
    }

    public void setCart_item(int cart_item) {
        this.cart_item = cart_item;
    }

    public int getDiscountpercentage() {
        return discountpercentage;
    }

    public void setDiscountpercentage(int discountpercentage) {
        this.discountpercentage = discountpercentage;
    }

    public int getDiscounted_price() {
        return discounted_price;
    }

    public void setDiscounted_price(int discounted_price) {
        this.discounted_price = discounted_price;
    }

    public String getCouponname() {
        return couponname;
    }

    public void setCouponname(String couponname) {
        this.couponname = couponname;
    }

    public CartPackageWrapper() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.cartdata);
        dest.writeInt(this.Original_price);
        dest.writeInt(this.discountprice);
        dest.writeInt(this.payblamt);
        dest.writeInt(this.taxx);
        dest.writeInt(this.cart_item);
        dest.writeInt(this.discountpercentage);
        dest.writeInt(this.discounted_price);
        dest.writeString(this.couponname);
    }

    protected CartPackageWrapper(Parcel in) {
        this.cartdata = in.createTypedArrayList(CartPackage.CREATOR);
        this.Original_price = in.readInt();
        this.discountprice = in.readInt();
        this.payblamt = in.readInt();
        this.taxx = in.readInt();
        this.cart_item = in.readInt();
        this.discountpercentage = in.readInt();
        this.discounted_price = in.readInt();
        this.couponname = in.readString();
    }

    public static final Creator<CartPackageWrapper> CREATOR = new Creator<CartPackageWrapper>() {
        @Override
        public CartPackageWrapper createFromParcel(Parcel source) {
            return new CartPackageWrapper(source);
        }

        @Override
        public CartPackageWrapper[] newArray(int size) {
            return new CartPackageWrapper[size];
        }
    };
}
