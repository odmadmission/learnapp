package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.MentorStudentChat;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.FullImageActivity;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.DateTimeUtil;
import com.sseduventures.digichamps.utils.ImFlexboxLayout;

import org.apache.commons.lang3.time.DateUtils;

import java.util.ArrayList;



public class ChatAdapter extends BaseAdapter
{
    private static final String TAG=ChatAdapter.class.getSimpleName();
    private Context mContext;
    private ArrayList<MentorStudentChat> mList;
    private LayoutInflater inflater;
    private int width;
    private int height;

    public ChatAdapter(Context context,
                       ArrayList<MentorStudentChat> list,int w,int h)
    {
        this.mContext=context;
        this.mList=list;
        this.height=h;
        this.width=w;

        inflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ChatHolder viewHolder=null;
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.
                            chat_item_new, parent,
                    false);

            viewHolder=new ChatHolder(mContext,convertView);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder=(ChatHolder) convertView.getTag();


        }
        boolean shouldShowHeader = false;
        if (position == 0
                || !DateUtils.isSameDay(mList.get(position).getInsertedDate(),
                mList.get(position-1).getInsertedDate()))
            shouldShowHeader = true;

        if (shouldShowHeader) {
            viewHolder.tvDate.setVisibility(View.VISIBLE);
            viewHolder.tvDate.setText(DateTimeUtil.
                    convertMillisAsDateString(mList.
                            get(position).getInsertedDate().getTime()));
        } else {
            viewHolder.tvDate.setVisibility(View.GONE);
        }
        viewHolder.llIn.setVisibility(View.GONE);
        viewHolder.llOut.setVisibility(View.GONE);
        viewHolder.rid=mList.get(position).getRegId();
        viewHolder.text=mList.get(position).getMessage();
        viewHolder.inout=mList.get(position).getSender();
        viewHolder.type=mList.get(position).getMime();
        viewHolder.time=mList.get(position).getInsertedDate().
                getTime();

        if (mList.get(position).getSender()== 2)
        {
            viewHolder.llIn.setVisibility(View.GONE);
            viewHolder.llOut.setVisibility(View.VISIBLE);
            switch (mList.get(position).getMime()) {

                case Constants.CHAT_MIME_TEXT:



                    viewHolder.tvMessageThem.setText(mList.get(position).getMessage());
                    viewHolder.tvTimeThem.setText(
                            DateTimeUtil.convertMillisAsTimeString(
                                    mList.get(position).getInsertedDate().getTime()));


                    viewHolder.imflTextThem.setVisibility(View.VISIBLE);
                    viewHolder.llImvThem.setVisibility(View.GONE);
                    viewHolder.tvStatusThem.setVisibility(View.VISIBLE);
                    viewHolder.tvStatusThem.
                            setImageResource(R.drawable.ic_single_tick);
                    break;


                case Constants.CHAT_MIME_MEDIA:

                    Picasso.with(mContext)
                            .load(Constants.IMAGE_BASE_URL+
                                    mList.get(position).getMessage())
                            .resize(width/2, height / 3)
                            .placeholder(R.drawable.rectangle_green)
                            .error(R.drawable.rectangle_green)
                            .centerCrop()
                            .into(viewHolder.imvMessageThem);

                    viewHolder.imvMessageThem.setTag(
                            Constants.IMAGE_BASE_URL+
                                    mList.get(position).getMessage()+""
                    );
                    viewHolder.tvTimeThemImv.setText(
                            DateTimeUtil.convertMillisAsTimeString(mList.get(position)
                                    .getInsertedDate().getTime()));
                    viewHolder.imflTextThem.setVisibility(View.GONE);
                    viewHolder.llImvThem.setVisibility(View.VISIBLE);
                    viewHolder.imvStatusThemImv.
                            setImageResource(R.drawable.ic_single_tick);

                    viewHolder.imvStatusThemImv.setVisibility(View.VISIBLE);
                    viewHolder.imvStatusThemImv.
                            setImageResource(R.drawable.ic_single_tick);
                    break;
            }
        }
        else
        if (mList.get(position).getSender()== 1)
        {
            viewHolder.llOut.setVisibility(View.GONE);
            viewHolder.llIn.setVisibility(View.VISIBLE);
            switch (mList.get(position).getMime()) {

                case Constants.CHAT_MIME_TEXT:



                    viewHolder.tvMessageIn.setText(
                            mList.get(position).getMessage());
                    viewHolder.tvTimeIn.setText(
                            DateTimeUtil.convertMillisAsTimeString(
                                    mList.get(position).getInsertedDate().getTime()
                            ));

                    viewHolder.imflTextIn.setVisibility(View.VISIBLE);
                    viewHolder.llImvIn.setVisibility(View.GONE);
                    break;

                case Constants.CHAT_MIME_MEDIA:
                    Log.v(TAG,mList.get(position).getMessage()+"");
                    viewHolder.imvMessageIn.setVisibility(View.VISIBLE);
                    viewHolder.tvTimeInImv.setVisibility(View.VISIBLE);
                    viewHolder.tvTimeInImv.setText(
                            DateTimeUtil.convertMillisAsTimeString(mList.
                                    get(position).getInsertedDate().getTime()));
                    Picasso.with(mContext)
                            .load(Constants.IMAGE_BASE_URL+
                                    mList.get(position).getMessage())
                            .resize(width/2, height / 3)
                            .placeholder(R.drawable.rectangle_white)
                            .error(R.drawable.rectangle_white)
                            .centerCrop()
                            .into(viewHolder.imvMessageIn);

                    viewHolder.imflTextIn.setVisibility(View.GONE);
                    viewHolder.llImvIn.setVisibility(View.VISIBLE);
                    viewHolder.imvMessageIn.setTag(
                            Constants.IMAGE_BASE_URL+
                                    mList.get(position).getMessage()+""
                    );



                    break;

            }
        }

        return convertView;
    }

    public   class ChatHolder {


        public long rid = 0;
        public long inout = 0;
        public long type = 0;
        public long subtype = 0;
        public long time = 0;
        public String text = null;
        public String name = null;

        public LinearLayout llIn;
        public LinearLayout llOut;

        //text them
        public ImFlexboxLayout imflTextThem;
        public TextView tvMessageThem;
        public TextView tvTimeThem;
        public ImageView tvStatusThem;
        //imv them
        public LinearLayout llImvThem;
        public ImageView imvMessageThem;
        public TextView tvTimeThemImv;
        public ImageView imvStatusThemImv;


        public LinearLayout imflTextIn;
        public TextView tvMessageIn;
        public TextView tvTimeIn;

        public TextView tvDate;

        //imv In
        public LinearLayout llImvIn;
        public ImageView imvMessageIn;
        public TextView tvTimeInImv;


        public TextView tvName;
        public TextView tvNameImv;


        public ChatHolder(final Context context, final View view) {


            llIn = (LinearLayout) view.findViewById(R.id.llIn);
            llOut = (LinearLayout) view.findViewById(R.id.llOut);

            imflTextThem = (ImFlexboxLayout) view.findViewById(R.id.imflTextThem);
            tvMessageThem = (TextView) view.findViewById(R.id.tvMessageThem);
            tvStatusThem = (ImageView) view.findViewById(R.id.imvStatusThem);
            tvTimeThem = (TextView) view.findViewById(R.id.tvTimeThem);

            tvName = (TextView) view.findViewById(R.id.tvName);

            tvDate = (TextView) view.findViewById(R.id.tvDate);

            tvNameImv = (TextView) view.findViewById(R.id.tvNameImv);

            llImvThem = (LinearLayout) view.findViewById(R.id.llMessageThem);
            imvMessageThem = (ImageView) view.findViewById(R.id.imvMessageThem);
            imvStatusThemImv = (ImageView) view.findViewById(R.id.imvStatusThemImv);
            tvTimeThemImv = (TextView) view.findViewById(R.id.tvTimeThemImv);


            llImvIn = (LinearLayout) view.findViewById(R.id.llImageIn);
            imvMessageIn = (ImageView) view.findViewById(R.id.imvMessageIn);
            tvTimeInImv = (TextView) view.findViewById(R.id.tvTimeInImv);


            imflTextIn = (LinearLayout) view.findViewById(R.id.flTextIn);
            tvMessageIn = (TextView) view.findViewById(R.id.tvMessageIn);
            tvTimeIn = (TextView) view.findViewById(R.id.tvTimeIn);
            imvMessageIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, FullImageActivity.class);
                    intent.putExtra("activity","FromChat");
                    intent.putExtra("url",
                            (String) imvMessageIn.getTag()
                    );
                    context.startActivity(intent);
                }
            });
            imvMessageThem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context, FullImageActivity.class);
                    intent.putExtra("activity","FromChat");
                    intent.putExtra("url",
                            (String) imvMessageThem.getTag()
                    );
                    context.startActivity(intent);
                }
            });
        }

    }

}
