package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/3/2018.
 */

public class FAQListData implements Parcelable {

    private int FAQ_Id;
    private String Answer;
    private String Question;

    public int getFAQ_Id() {
        return FAQ_Id;
    }

    public void setFAQ_Id(int FAQ_Id) {
        this.FAQ_Id = FAQ_Id;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.FAQ_Id);
        dest.writeString(this.Answer);
        dest.writeString(this.Question);
    }

    public FAQListData() {
    }

    protected FAQListData(Parcel in) {
        this.FAQ_Id = in.readInt();
        this.Answer = in.readString();
        this.Question = in.readString();
    }

    public static final Creator<FAQListData> CREATOR = new Creator<FAQListData>() {
        @Override
        public FAQListData createFromParcel(Parcel source) {
            return new FAQListData(source);
        }

        @Override
        public FAQListData[] newArray(int size) {
            return new FAQListData[size];
        }
    };
}
