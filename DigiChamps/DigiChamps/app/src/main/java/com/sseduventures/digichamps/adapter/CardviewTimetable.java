package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.sseduventures.digichamps.Model.TimeTableList;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.domain.DailyTTList;
import com.sseduventures.digichamps.utils.FontManage;

import java.util.ArrayList;
import java.util.List;


public class CardviewTimetable extends RecyclerView.Adapter<CardviewTimetable.DataObjectHolder> {

    Context context;
    private List<DailyTTList> list = new ArrayList<>();

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        LinearLayout linearMaster;
        android.widget.RadioButton RadioButton;
        TextView tv_number,tv_timeslot,tv_subject;

        public DataObjectHolder(View itemView) {
            super(itemView);
            tv_number = (TextView) itemView.findViewById(R.id.tv_number);
            tv_timeslot = (TextView) itemView.findViewById(R.id.tv_timeslot);
            tv_subject = (TextView) itemView.findViewById(R.id.tv_subject);
        }
    }

    public CardviewTimetable(List<DailyTTList> getlist, Context context) {
        this.list = getlist;
        this.context = context;
    }

    @Override
    public CardviewTimetable.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_school_titmetable, parent, false);

        CardviewTimetable.DataObjectHolder dataObjectHolder = new CardviewTimetable.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final CardviewTimetable.DataObjectHolder holder, final int position) {
       holder.tv_number.setText(list.get(position).getPeroidName());
       holder.tv_timeslot.setText(list.get(position).getTimeFrom()+"-"+list.get(position).getTimeTo());
       holder.tv_subject.setText(list.get(position).getSubject());

        FontManage.setFontMeiryo(context,holder.tv_number);
        FontManage.setFontMeiryo(context,holder.tv_timeslot);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}

