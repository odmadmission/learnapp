package com.sseduventures.digichamps.Model;

/**
 * Created by ARR on 6/21/2017.
 */

public class DataModel_LeaderBoard {

    String pro_image;
    String user_name;
    String rank;
    String time;
    String tol_no_que;
    String tol_no_ans;
    String tol_no_wro;
    Double accuracy;
    private boolean isSelected;


    public DataModel_LeaderBoard(String pro_image,  String user_name, String rank, String time, String tol_no_que, String tol_no_ans, String tol_no_wro,Double accuracy) {
        this.pro_image = pro_image;
        this.user_name= user_name;
        this.rank = rank;
        this.time = time;
        this.tol_no_que = tol_no_que;
        this.tol_no_ans = tol_no_ans;
        this.tol_no_wro = tol_no_wro;
        this.accuracy = accuracy;




    }

    public String getPro_image() {
        return pro_image;
    }
    public Double getAccuracy() {
        return accuracy;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getRank() {
        return rank;
    }

    public String getTime() {
        return time;
    }

    public String getTol_no_que() {
        return tol_no_que;
    }

    public String getTol_no_ans() {
        return tol_no_ans;
    }

    public String getTol_no_wro() {
        return tol_no_wro;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
