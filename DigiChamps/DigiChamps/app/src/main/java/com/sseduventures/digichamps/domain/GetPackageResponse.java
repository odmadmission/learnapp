package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/5/2018.
 */

public class GetPackageResponse implements Parcelable
{

    private PackageSuccess success;

    public PackageSuccess getSuccess() {
        return success;
    }

    public void setSuccess(PackageSuccess success) {
        this.success = success;
    }


    public GetPackageResponse() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.success, flags);
    }

    protected GetPackageResponse(Parcel in) {
        this.success = in.readParcelable(PackageSuccess.class.getClassLoader());
    }

    public static final Creator<GetPackageResponse> CREATOR = new Creator<GetPackageResponse>() {
        @Override
        public GetPackageResponse createFromParcel(Parcel source) {
            return new GetPackageResponse(source);
        }

        @Override
        public GetPackageResponse[] newArray(int size) {
            return new GetPackageResponse[size];
        }
    };
}
