package com.sseduventures.digichamps.dotdialog;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.sseduventures.digichamps.R;


public class SpotsDialog extends AlertDialog {

    private static final int DELAY = 150;
    private static final int DURATION = 1500;

    private int size;
    private AnimatedView[] spots;
    private AnimatorPlayer animator;
    private String message;
    private Activity activity;
    private int icon;

    public SpotsDialog(Context context) {
        this(context, R.style.CustomSpot);
    }

    public SpotsDialog(Context context, String message) {
        this(context);
        this.message = message;
    }

    public SpotsDialog(Context context, String message,int icon, int theme) {
        this(context, theme);
        this.activity=(Activity) context;
        this.message = message;
        this.icon=icon;
    }

    public SpotsDialog(Context context, int theme) {
        super(context, theme);
    }

    public SpotsDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.custom_spots_dialog);
        setCanceledOnTouchOutside(false);
        setCancelable(false);

        initMessage();
        initProgress();
    }

    @Override
    protected void onStart() {
        super.onStart();

        animator = new AnimatorPlayer(createAnimations());
        animator.play();
    }

    @Override
    protected void onStop() {
        super.onStop();

        animator.stop();
    }

    @Override
    public void setMessage(CharSequence message) {
        ((TextView) findViewById(R.id.dialog_spots_title)).setText(message);
    }
    private void initMessage() {
        if (message != null && message.length() > 0) {
            TextView textView=(TextView) findViewById(R.id.dialog_spots_title);
          //  Typeface robotoMedium= TypefaceUtil.getRobotoMedium(activity);
            //textView.setTypeface(robotoMedium);
            textView.setText(message);
            if(icon!=-1)
            textView.setCompoundDrawablesWithIntrinsicBounds(0,icon,0,0);
        }
    }

    private void initProgress() {
        ProgressLayout progress = (ProgressLayout) findViewById(R.id.dialog_spots_progress);
        size = progress.getSpotsCount();

        spots = new AnimatedView[size];
        int size =getContext().getResources().getDimensionPixelSize(R.dimen.spot_size);
        int progressWidth = getContext().getResources().getDimensionPixelSize(R.dimen.progress_width);
        for (int i = 0; i < spots.length; i++) {
            AnimatedView v = new AnimatedView(getContext());
            v.setBackgroundResource(R.drawable.custom_spots_dialog_spot);
            v.setTarget(progressWidth);
            v.setXFactor(-1f);
            progress.addView(v, size, size);
            spots[i] = v;
        }
    }

    private Animator[] createAnimations() {
        Animator[] animators = new Animator[size];
        for (int i = 0; i < spots.length; i++) {
            Animator move = ObjectAnimator.ofFloat(spots[i], "xFactor", 0, 1);
            move.setDuration(DURATION);
            move.setInterpolator(new HesitateInterpolator());
            move.setStartDelay(DELAY * i);
            animators[i] = move;
        }
        return animators;
    }


}
