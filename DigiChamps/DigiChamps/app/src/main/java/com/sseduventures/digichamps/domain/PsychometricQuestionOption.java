package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/7/2018.
 */

public class PsychometricQuestionOption implements Parcelable
{

    private int OptionId;
    private String Option;
    private String OptionImage;

    private String OptionLabel;

    private boolean IsCorrect;

    public int getScore() {
        return Score;
    }

    public void setScore(int score) {
        Score = score;
    }

    private int Score;
    public boolean isCorrect() {
        return IsCorrect;
    }

    public void setCorrect(boolean correct) {
        IsCorrect = correct;
    }

    public String getOptionLabel() {
        return OptionLabel;
    }

    public void setOptionLabel(String optionLabel) {
        OptionLabel = optionLabel;
    }

    public int getOptionId() {
        return OptionId;
    }

    public void setOptionId(int optionId) {
        OptionId = optionId;
    }

    public String getOption() {
        return Option;
    }

    public void setOption(String option) {
        Option = option;
    }

    public String getOptionImage() {
        return OptionImage;
    }

    public void setOptionImage(String optionImage) {
        OptionImage = optionImage;
    }

    public PsychometricQuestionOption() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.OptionId);
        dest.writeString(this.Option);
        dest.writeString(this.OptionImage);
        dest.writeString(this.OptionLabel);
        dest.writeByte(this.IsCorrect ? (byte) 1 : (byte) 0);
        dest.writeInt(this.Score);
    }

    protected PsychometricQuestionOption(Parcel in) {
        this.OptionId = in.readInt();
        this.Option = in.readString();
        this.OptionImage = in.readString();
        this.OptionLabel = in.readString();
        this.IsCorrect = in.readByte() != 0;
        this.Score = in.readInt();
    }

    public static final Creator<PsychometricQuestionOption> CREATOR = new Creator<PsychometricQuestionOption>() {
        @Override
        public PsychometricQuestionOption createFromParcel(Parcel source) {
            return new PsychometricQuestionOption(source);
        }

        @Override
        public PsychometricQuestionOption[] newArray(int size) {
            return new PsychometricQuestionOption[size];
        }
    };
}
