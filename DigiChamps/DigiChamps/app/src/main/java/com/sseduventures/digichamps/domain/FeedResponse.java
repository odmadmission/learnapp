package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/6/2018.
 */

public class FeedResponse implements Parcelable{


    private FeedSuccessData Success;

    public FeedSuccessData getSuccess() {
        return Success;
    }

    public void setSuccess(FeedSuccessData success) {
        Success = success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.Success, flags);
    }

    public FeedResponse() {
    }

    protected FeedResponse(Parcel in) {
        this.Success = in.readParcelable(FeedSuccessData.class.getClassLoader());
    }

    public static final Creator<FeedResponse> CREATOR = new Creator<FeedResponse>() {
        @Override
        public FeedResponse createFromParcel(Parcel source) {
            return new FeedResponse(source);
        }

        @Override
        public FeedResponse[] newArray(int size) {
            return new FeedResponse[size];
        }
    };
}
