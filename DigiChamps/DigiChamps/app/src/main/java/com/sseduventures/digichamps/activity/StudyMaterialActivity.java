package com.sseduventures.digichamps.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.adapter.CardviewStudyMaterial;
import com.sseduventures.digichamps.domain.StudyMaterialsList;
import com.sseduventures.digichamps.domain.StudyMaterialsResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import java.util.ArrayList;

/**
 * Created by user on 22-01-2018.
 */

public class StudyMaterialActivity extends FormActivity implements ServiceReceiver.Receiver{

    Context mContext;
    CardviewStudyMaterial mCardviewStudyMaterial;
    RecyclerView recyclerview;
    ArrayList<StudyMaterialsList> mList;
    String schoolid,sectionid;
    int classid;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private StudyMaterialsResponse success;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_material);


        setModuleName(LogEventUtil.EVENT_studymaterial);
        logEvent(LogEventUtil.KEY_studymaterial,LogEventUtil.EVENT_studymaterial);

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        schoolid = RegPrefManager.getInstance(this).getSchoolId();
        classid = (int)RegPrefManager.getInstance(this).getclassid();
        sectionid = RegPrefManager.getInstance(this).getSectionId();

        mContext = this;
        mList = new ArrayList<>();
        mCardviewStudyMaterial = new CardviewStudyMaterial(mList,mContext);
        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView tv_at =  findViewById(R.id.tv_at);
        FontManage.setFontHeaderBold(mContext,tv_at);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());

        getStudyMaterialsList();

    }



    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mContext)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }


    private void getStudyMaterialsList() {
        if(AppUtil.isInternetConnected(this)){


            NetworkService.startActionPostStudyMaterialsList(StudyMaterialActivity.this
                    ,mServiceReceiver);

        }else{
            Intent in = new Intent(StudyMaterialActivity.this,
                    Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        //hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(StudyMaterialActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(StudyMaterialActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(StudyMaterialActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:

                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if(success!=null && success.isStatus()){
                    if(success.getList().size()>0 && success.getList()!=null){
                        mList.addAll(success.getList());
                        mCardviewStudyMaterial = new CardviewStudyMaterial(mList,mContext);
                        recyclerview.setAdapter(mCardviewStudyMaterial);

                    }
                }else{
                    AskOptionDialog("No Study Material Available").show();
                }


                break;

        }
    }
}
