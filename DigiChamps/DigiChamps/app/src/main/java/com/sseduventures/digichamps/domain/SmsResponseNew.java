package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;


import java.util.ArrayList;
import java.util.List;


public class SmsResponseNew implements Parcelable
{




    private String errorCode;

    private String errorMessage;

    private String jobId;

    private List<SmsMessageData> messageData;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public List<SmsMessageData> getMessageData() {
        return messageData;
    }

    public void setMessageData(List<SmsMessageData> messageData) {
        this.messageData = messageData;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.errorCode);
        dest.writeString(this.errorMessage);
        dest.writeString(this.jobId);
        dest.writeList(this.messageData);
    }

    public SmsResponseNew() {
    }

    protected SmsResponseNew(Parcel in) {
        this.errorCode = in.readString();
        this.errorMessage = in.readString();
        this.jobId = in.readString();
        this.messageData = new ArrayList<SmsMessageData>();
        in.readList(this.messageData, SmsMessageData.class.getClassLoader());
    }

    public static final Creator<SmsResponseNew> CREATOR = new Creator<SmsResponseNew>() {
        @Override
        public SmsResponseNew createFromParcel(Parcel source) {
            return new SmsResponseNew(source);
        }

        @Override
        public SmsResponseNew[] newArray(int size) {
            return new SmsResponseNew[size];
        }
    };
}


