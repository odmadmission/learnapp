package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/11/2018.
 */

public class ExamScheduleRequest implements Parcelable{


    private String SchoolId;
    private int ClassId;


    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public int getClassId() {
        return ClassId;
    }

    public void setClassId(int classId) {
        ClassId = classId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.SchoolId);
        dest.writeInt(this.ClassId);
    }

    public ExamScheduleRequest() {
    }

    protected ExamScheduleRequest(Parcel in) {
        this.SchoolId = in.readString();
        this.ClassId = in.readInt();
    }

    public static final Creator<ExamScheduleRequest> CREATOR = new Creator<ExamScheduleRequest>() {
        @Override
        public ExamScheduleRequest createFromParcel(Parcel source) {
            return new ExamScheduleRequest(source);
        }

        @Override
        public ExamScheduleRequest[] newArray(int size) {
            return new ExamScheduleRequest[size];
        }
    };
}
