package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/11/2018.
 */

public class DailyTTList implements Parcelable {

    private String peroidName;
    private String timeFrom;
    private String timeTo;
    private String subject;
    private String day;

    public String getPeroidName() {
        return peroidName;
    }

    public void setPeroidName(String peroidName) {
        this.peroidName = peroidName;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.peroidName);
        dest.writeString(this.timeFrom);
        dest.writeString(this.timeTo);
        dest.writeString(this.subject);
        dest.writeString(this.day);
    }

    public DailyTTList() {
    }

    protected DailyTTList(Parcel in) {
        this.peroidName = in.readString();
        this.timeFrom = in.readString();
        this.timeTo = in.readString();
        this.subject = in.readString();
        this.day = in.readString();
    }

    public static final Creator<DailyTTList> CREATOR = new Creator<DailyTTList>() {
        @Override
        public DailyTTList createFromParcel(Parcel source) {
            return new DailyTTList(source);
        }

        @Override
        public DailyTTList[] newArray(int size) {
            return new DailyTTList[size];
        }
    };
}
