package com.sseduventures.digichamps.helper;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

/**
 * Created by NTSPL-19 on 10/14/2017.
 */

public class MyYAxisValueFormatter implements IAxisValueFormatter {

    private DecimalFormat mFormat;

    public MyYAxisValueFormatter() {
        mFormat = new DecimalFormat("###,###,##0.0"); // use one decimal
    }

    @Override
    public String getFormattedValue(float value, AxisBase yAxis) {
        // write your logic here
        // access the YAxis object to get more information
        return mFormat.format(value) + ""; // e.g. append a dollar-sign
    }
}

