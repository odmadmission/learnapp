package com.sseduventures.digichamps.webservice;

/**
 * Created by user on 4/23/2018.
 */

public class ResponseCodes {
    public static final int LOGIN_SUCCESS = 100;
    public static final int LOGIN_FAILURE = 101;
    public static final int UNEXPECTED_ERROR = 99;
    public static final int NO_INTERNET = 102;
    public static final int RESPONSE_NULL = 103;
    public static final int EXCEPTION = 104;
    public static final int CURSOR_NULL_GALLERY = 105;
    public static final int BASE64_NULL = 106;

    public static final int DISCOUNTSUCCESS=5001;
    public static final int DISCOUNTUNSUCCESS = 5002;


    public static final int SCRATCH_UPDATE_SUCCESS = 107;
    public static final int SCRATCH_UPDATE_FAILURE = 108;


    public static final int ORDER_CONFIRM_SUCCESS = 109;
    public static final int ORDER_CONFIRM_FAILURE = 110;

    public static final int PACKAGES_SUCCESS = 111;
    public static final int PACKAGES_FAILURE = 112;


    public static final int ADD_TO_CART_SUCCESS = 113;
    public static final int ADD_TO_CART_FAILURE = 114;


    public static final int GET_CART_DATA_SUCCESS = 115;
    public static final int GET_CART_DATA_FAILURE = 116;

    public static final int MULTI_CART_REMOVE_SUCCESS = 117;
    public static final int MULTI_CART_REMOVE_FAILURE = 118;

    public static final int GET_CART_SUCCESS = 119;
    public static final int GET_CART_FAILURE = 120;

    public static final int GET_PSYCHO_EXAM_DATA_SUCCESS = 121;
    public static final int GET_PSYCHO_EXAM_DATA_FAILURE = 122;

    public static final int POST_PSYCHO_EXAM_DATA_SUCCESS = 123;
    public static final int POST_PSYCHO_EXAM_DATA_FAILURE = 124;
    public static final int SUCCESS = 1001;
    public static final int FAILURE = 1002;
    public static final int ERROR = 1003;
    public static final int LOGOUTSUCCESS = 2001;
    public static final int LOGOUTFAILURE = 2002;
    public static final int STATUS = 1005;
    public static final int CLASSSUCCESS = 1006;
    public static final int ADDFORUMSUCCESS = 1007;
    public static final int IFMOBILEEXITS = 409;
    public static final int ORDERCONF = 611;
    public static final int ORDERCONF_FAILURE = 612;

    public static final int SUCCESSPRT = 410;
    public static final int NOLIST = 411;
    public static final int BOOKMARKED = 412;
    public static final int UNBOOKMARKED = 413;
    public static final int CHAPTERDETAILS = 5000;


    public static final int NO_USER_FOUND = 204;
    public static final int PASSWORD_MISMATCH = 400;
    public static final int SERVER_ERROR = 500;
    public static final int USER_ALREADY_DONE = 501;
    public static final int SUCCESSFULLYSIGNEDUP = 502;
    public static final int GETDOUBTSUCCESS = 503;
    public static final int IMAGESUCCESS = 504;
    public static final int PROFILESUBMITSUCCESS = 505;


}
