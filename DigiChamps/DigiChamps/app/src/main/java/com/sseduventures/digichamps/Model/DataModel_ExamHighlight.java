package com.sseduventures.digichamps.Model;

/**
 * Created by RKB on 6/28/2017.
 */

public class DataModel_ExamHighlight {

    private String chap_name,total_qstn,correct_num,percentage_num,
            SubConceptName, TotalQuestions, TotalCorrect, TotalInCorrect,TotalSkipped,Accuracy,
            TotalQuestion, TotalCorect, TotalInCorect, TotalSkiped,Acuracy,diff_lev_type;
    int qstn_number;

    public DataModel_ExamHighlight(String SubConceptName, String TotalQuestions,String TotalCorrect,String TotalInCorrect,String TotalSkipped,String Accuracy) {

        this.SubConceptName = SubConceptName;
        this.TotalQuestions = TotalQuestions;
        this.TotalCorrect = TotalCorrect;
        this.TotalInCorrect = TotalInCorrect;
        this.TotalSkipped = TotalSkipped;
        this.Accuracy = Accuracy;

    }

    public String getSubConceptName() {
        return SubConceptName;
    }

    public String getTotalQuestions() {
        return TotalQuestions;
    }

    public String getTotalCorrect() {
        return TotalCorrect;
    }

    public String getTotalInCorrect() {
        return TotalInCorrect;
    }

    public String getTotalSkipped() {
        return TotalSkipped;
    }

    public String getAccuracy() {
        return Accuracy;
    }



    public DataModel_ExamHighlight(String TotalQuestion, String TotalCorect, String TotalInCorect, String TotalSkiped, String Acuracy) {

        this.TotalQuestion = TotalQuestion;
        this.TotalCorect = TotalCorect;
        this.TotalInCorect = TotalInCorect;
        this.TotalSkiped = TotalSkiped;
        this.Acuracy = Acuracy;


    }

    public String getDiff_lev_type() {
        return diff_lev_type;
    }

    public int getQstn_number() {
        return qstn_number;
    }

    public DataModel_ExamHighlight(String diff_lev_type, int qstn_number) {

        this.diff_lev_type = diff_lev_type;
        this.qstn_number = qstn_number;




    }

    public DataModel_ExamHighlight(String chap_name, String total_qstn, String correct_num, String percentage_num) {

        this.chap_name = chap_name;
        this.total_qstn = total_qstn;
        this.correct_num = correct_num;
        this.percentage_num = percentage_num;

    }
    public String getTotalQuestion() {
        return TotalQuestion;
    }

    public String getTotalCorect() {
        return TotalCorect;
    }

    public String getTotalInCorect() {
        return TotalInCorect;
    }

    public String getTotalSkiped() {
        return TotalSkiped;
    }

    public String getAcuracy() {
        return Acuracy;
    }


    public String getChap_name() {
        return chap_name;
    }

    public void setChap_name(String chap_name) {
        this.chap_name = chap_name;
    }
    public String getTotal_qstn() {
        return total_qstn;
    }


    public void setTotal_qstn(String total_qstn) {
        this.total_qstn = total_qstn;
    }
    public String getCorrect_num() {
        return correct_num;
    }
    public void setCorrect_num(String correct_num){
        this.correct_num=correct_num;
    }



    public String getPercentage_num(){
        return percentage_num;
    }
    public void setPercentage_num(String percentage_num){
        this.percentage_num = percentage_num;
    }





}
