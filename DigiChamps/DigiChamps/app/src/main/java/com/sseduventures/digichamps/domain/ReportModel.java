package com.sseduventures.digichamps.domain;

import com.sseduventures.digichamps.Model.ProgressModule;
import com.sseduventures.digichamps.Model.ReportModule;

import java.util.ArrayList;

public class ReportModel
{

    private ArrayList<ReportModule> ReportModules;

    private ArrayList<ProgressModule> ProgressModules;


    public ArrayList<ReportModule> getReportModules() {
        return ReportModules;
    }

    public void setReportModules(ArrayList<ReportModule> reportModules) {
        ReportModules = reportModules;
    }

    public ArrayList<ProgressModule> getProgressModules() {
        return ProgressModules;
    }

    public void setProgressModules(ArrayList<ProgressModule> progressModules) {
        ProgressModules = progressModules;
    }
}
