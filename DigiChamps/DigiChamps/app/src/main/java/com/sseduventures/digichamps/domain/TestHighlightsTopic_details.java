package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/4/2018.
 */

public class TestHighlightsTopic_details implements Parcelable{


    private int Topic_ID;
    private String Topic_Name;
    private int Total_question;
    private int Correct_answer;
    private int Incorrect_answer;
    private int Percentage;
    private int Remark;


    public int getTopic_ID() {
        return Topic_ID;
    }

    public void setTopic_ID(int topic_ID) {
        Topic_ID = topic_ID;
    }

    public String getTopic_Name() {
        return Topic_Name;
    }

    public void setTopic_Name(String topic_Name) {
        Topic_Name = topic_Name;
    }

    public int getTotal_question() {
        return Total_question;
    }

    public void setTotal_question(int total_question) {
        Total_question = total_question;
    }

    public int getCorrect_answer() {
        return Correct_answer;
    }

    public void setCorrect_answer(int correct_answer) {
        Correct_answer = correct_answer;
    }

    public int getIncorrect_answer() {
        return Incorrect_answer;
    }

    public void setIncorrect_answer(int incorrect_answer) {
        Incorrect_answer = incorrect_answer;
    }

    public int getPercentage() {
        return Percentage;
    }

    public void setPercentage(int percentage) {
        Percentage = percentage;
    }

    public int getRemark() {
        return Remark;
    }

    public void setRemark(int remark) {
        Remark = remark;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Topic_ID);
        dest.writeString(this.Topic_Name);
        dest.writeInt(this.Total_question);
        dest.writeInt(this.Correct_answer);
        dest.writeInt(this.Incorrect_answer);
        dest.writeInt(this.Percentage);
        dest.writeInt(this.Remark);
    }

    public TestHighlightsTopic_details() {
    }

    protected TestHighlightsTopic_details(Parcel in) {
        this.Topic_ID = in.readInt();
        this.Topic_Name = in.readString();
        this.Total_question = in.readInt();
        this.Correct_answer = in.readInt();
        this.Incorrect_answer = in.readInt();
        this.Percentage = in.readInt();
        this.Remark = in.readInt();
    }

    public static final Creator<TestHighlightsTopic_details> CREATOR = new Creator<TestHighlightsTopic_details>() {
        @Override
        public TestHighlightsTopic_details createFromParcel(Parcel source) {
            return new TestHighlightsTopic_details(source);
        }

        @Override
        public TestHighlightsTopic_details[] newArray(int size) {
            return new TestHighlightsTopic_details[size];
        }
    };
}
