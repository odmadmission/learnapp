package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/4/2018.
 */

public class TestHighLightsDifficulty implements Parcelable{


    private int PowerId;
    private int TotalQuestions;
    private int TotalCorrect;
    private int TotalInCorrect;
    private int TotalSkipped;
    private int Accuracy;


    public int getPowerId() {
        return PowerId;
    }

    public void setPowerId(int powerId) {
        PowerId = powerId;
    }

    public int getTotalQuestions() {
        return TotalQuestions;
    }

    public void setTotalQuestions(int totalQuestions) {
        TotalQuestions = totalQuestions;
    }

    public int getTotalCorrect() {
        return TotalCorrect;
    }

    public void setTotalCorrect(int totalCorrect) {
        TotalCorrect = totalCorrect;
    }

    public int getTotalInCorrect() {
        return TotalInCorrect;
    }

    public void setTotalInCorrect(int totalInCorrect) {
        TotalInCorrect = totalInCorrect;
    }

    public int getTotalSkipped() {
        return TotalSkipped;
    }

    public void setTotalSkipped(int totalSkipped) {
        TotalSkipped = totalSkipped;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.PowerId);
        dest.writeInt(this.TotalQuestions);
        dest.writeInt(this.TotalCorrect);
        dest.writeInt(this.TotalInCorrect);
        dest.writeInt(this.TotalSkipped);
        dest.writeInt(this.Accuracy);
    }

    public TestHighLightsDifficulty() {
    }

    protected TestHighLightsDifficulty(Parcel in) {
        this.PowerId = in.readInt();
        this.TotalQuestions = in.readInt();
        this.TotalCorrect = in.readInt();
        this.TotalInCorrect = in.readInt();
        this.TotalSkipped = in.readInt();
        this.Accuracy = in.readInt();
    }

    public static final Creator<TestHighLightsDifficulty> CREATOR = new Creator<TestHighLightsDifficulty>() {
        @Override
        public TestHighLightsDifficulty createFromParcel(Parcel source) {
            return new TestHighLightsDifficulty(source);
        }

        @Override
        public TestHighLightsDifficulty[] newArray(int size) {
            return new TestHighLightsDifficulty[size];
        }
    };
}
