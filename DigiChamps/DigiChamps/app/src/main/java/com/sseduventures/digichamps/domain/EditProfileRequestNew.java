package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class EditProfileRequestNew implements Parcelable {

    public int Board_ID,Class_ID ;
    public String Customer_Name;

    private String Organisation_Name;

    public EditProfileRequestNew() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Board_ID);
        dest.writeInt(Class_ID);
        dest.writeString(Customer_Name);
        dest.writeString(Organisation_Name);
        dest.writeInt(Security_Question_ID);
        dest.writeString(Security_Answer);
        dest.writeLong(Regd_ID);
        dest.writeString(Email);
        dest.writeString(Phone);
        dest.writeString(SectionId);
        dest.writeString(SchoolName);
        dest.writeString(Board_Name);
        dest.writeString(Class_Name);
        dest.writeString(sectionname);
        dest.writeString(School_Id);
        dest.writeString(sectionid);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EditProfileRequestNew> CREATOR = new Creator<EditProfileRequestNew>() {
        @Override
        public EditProfileRequestNew createFromParcel(Parcel in) {
            return new EditProfileRequestNew();
        }

        @Override
        public EditProfileRequestNew[] newArray(int size) {
            return new EditProfileRequestNew[size];
        }
    };

    public String getOrganisation_Name() {
        return Organisation_Name;
    }

    public void setOrganisation_Name(String organisation_Name) {
        Organisation_Name = organisation_Name;
    }

    public int getSecurity_Question_ID() {
        return Security_Question_ID;
    }

    public void setSecurity_Question_ID(int security_Question_ID) {
        Security_Question_ID = security_Question_ID;
    }

    public String getSecurity_Answer() {
        return Security_Answer;
    }

    public void setSecurity_Answer(String security_Answer) {
        Security_Answer = security_Answer;
    }

    public long getRegd_ID() {
        return Regd_ID;
    }

    public void setRegd_ID(long regd_ID) {
        Regd_ID = regd_ID;
    }

    private int Security_Question_ID;
    private String Security_Answer;
    private long Regd_ID;

    public int getBoard_ID() {
        return Board_ID;
    }

    public void setBoard_ID(int board_ID) {
        Board_ID = board_ID;
    }

    public int getClass_ID() {
        return Class_ID;
    }

    public void setClass_ID(int class_ID) {
        Class_ID = class_ID;
    }

    public String getCustomer_Name() {
        return Customer_Name;
    }

    public void setCustomer_Name(String customer_Name) {
        Customer_Name = customer_Name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getSectionId() {
        return SectionId;
    }

    public void setSectionId(String sectionId) {
        SectionId = sectionId;
    }

    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }

    public String getBoard_Name() {
        return Board_Name;
    }

    public void setBoard_Name(String board_Name) {
        Board_Name = board_Name;
    }

    public String getClass_Name() {
        return Class_Name;
    }

    public void setClass_Name(String class_Name) {
        Class_Name = class_Name;
    }

    public String getSectionname() {
        return sectionname;
    }

    public void setSectionname(String sectionname) {
        this.sectionname = sectionname;
    }

    public String getSchool_Id() {
        return School_Id;
    }

    public void setSchool_Id(String school_Id) {
        School_Id = school_Id;
    }

    public String getSectionid() {
        return sectionid;
    }

    public void setSectionid(String sectionid) {
        this.sectionid = sectionid;
    }



    public String Email;
    public String Phone;
    public String SectionId;
    public String SchoolName;
    public String Board_Name;
    public String Class_Name;
    public String sectionname;
    public String School_Id;
    public String sectionid ;

    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public String SchoolId;


}
