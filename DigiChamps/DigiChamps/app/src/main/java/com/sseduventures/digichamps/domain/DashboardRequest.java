package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/24/2018.
 */

public class DashboardRequest implements Parcelable {

    private int RegID;


    public int getRegID() {
        return RegID;
    }

    public void setRegID(int regID) {
        RegID = regID;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.RegID);
    }

    public DashboardRequest() {
    }

    protected DashboardRequest(Parcel in) {
        this.RegID = in.readInt();
    }

    public static final Creator<DashboardRequest> CREATOR = new Creator<DashboardRequest>() {
        @Override
        public DashboardRequest createFromParcel(Parcel source) {
            return new DashboardRequest(source);
        }

        @Override
        public DashboardRequest[] newArray(int size) {
            return new DashboardRequest[size];
        }
    };
}
