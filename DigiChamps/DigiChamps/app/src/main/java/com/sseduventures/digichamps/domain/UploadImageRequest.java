package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/26/2018.
 */

public class UploadImageRequest implements Parcelable {

    private long Regd_ID;
    private String Image;

    public long getRegd_ID() {
        return Regd_ID;
    }

    public void setRegd_ID(long regd_ID) {
        Regd_ID = regd_ID;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.Regd_ID);
        dest.writeString(this.Image);
    }

    public UploadImageRequest() {
    }

    protected UploadImageRequest(Parcel in) {
        this.Regd_ID = in.readLong();
        this.Image = in.readString();
    }

    public static final Creator<UploadImageRequest> CREATOR = new Creator<UploadImageRequest>() {
        @Override
        public UploadImageRequest createFromParcel(Parcel source) {
            return new UploadImageRequest(source);
        }

        @Override
        public UploadImageRequest[] newArray(int size) {
            return new UploadImageRequest[size];
        }
    };
}
