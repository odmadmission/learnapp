package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class GetQuestion implements Parcelable {

    protected GetQuestion(Parcel in) {
        Result_id = in.readInt();
        Student_id = in.readString();
        Exam_id = in.readString();
        Question = in.createTypedArrayList(ExamQuestion.CREATOR);
    }

    public GetQuestion() {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Result_id);
        dest.writeString(Student_id);
        dest.writeString(Exam_id);
        dest.writeTypedList(Question);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GetQuestion> CREATOR = new Creator<GetQuestion>() {
        @Override
        public GetQuestion createFromParcel(Parcel in) {
            return new GetQuestion(in);
        }

        @Override
        public GetQuestion[] newArray(int size) {
            return new GetQuestion[size];
        }
    };

    public int getResult_id() {
        return Result_id;
    }

    public void setResult_id(int result_id) {
        Result_id = result_id;
    }

    public String getStudent_id() {
        return Student_id;
    }

    public void setStudent_id(String student_id) {
        Student_id = student_id;
    }

    public String getExam_id() {
        return Exam_id;
    }

    public void setExam_id(String exam_id) {
        Exam_id = exam_id;
    }


    public ArrayList<ExamQuestion> getQuestion() {
        return Question;
    }

    public void setQuestion(ArrayList<ExamQuestion> question) {
        Question = question;
    }

    private int Result_id;
    private String Student_id;
    private String Exam_id;
    private ArrayList<ExamQuestion> Question;

}
