
package com.sseduventures.digichamps.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssignDetail {

    @SerializedName("ClassName")
    @Expose
    private String className;
    @SerializedName("SubjectName")
    @Expose
    private String subjectName;
    @SerializedName("TeacherNameFirstName")
    @Expose
    private String teacherNameFirstName;
    @SerializedName("TeacherNameLastName")
    @Expose
    private String teacherNameLastName;
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("SectionName")
    @Expose
    private String sectionName;

    @SerializedName("TeacherName")
    @Expose
    private String teacherName;


    @SerializedName("ImageUrl")
    @Expose
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getTeacherNameFirstName() {
        return teacherNameFirstName;
    }

    public void setTeacherNameFirstName(String teacherNameFirstName) {
        this.teacherNameFirstName = teacherNameFirstName;
    }

    public String getTeacherNameLastName() {
        return teacherNameLastName;
    }

    public void setTeacherNameLastName(String teacherNameLastName) {
        this.teacherNameLastName = teacherNameLastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

}
