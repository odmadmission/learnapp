package com.sseduventures.digichamps.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.PersistableBundle;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.Spannable;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.activities.SettingsActivity;
import com.sseduventures.digichamps.database.DbContract;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.utils.URLSpanNoUnderline;
import com.sseduventures.digichamps.utils.avi.AVLoadingIndicatorView;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ScholarShipActivity
        extends FormActivity
        implements ServiceReceiver.Receiver {


    private static final String TAG = ScholarShipActivity.
            class.getSimpleName();


    private TextView tvTop, tvTopTwo;
    private TextView tvBottom, tvBottomTwo;
    private TextView tvCall;
    private ImageView imageView, sch_back_arrow;

    private boolean status = false;

    private Handler mHandler;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private ServiceReceiver mServiceReceiver;
    private AVLoadingIndicatorView avl;
    private LinearLayout llData, lnr_call_us;
    private String user_id;
    private Long userId;
    private TextView tvCallTwo;
    String phone = "1800-212-4322";
    private int cartSize = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_scholar_ship);
        logEvent(LogEventUtil.KEY_GiftCard_PAGE,
                LogEventUtil.EVENT_GiftCard_PAGE);
        setModuleName(LogEventUtil.EVENT_scratch_activity);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        init();

    }

    private void init() {

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);


        llData = findViewById(R.id.llData);
        lnr_call_us = findViewById(R.id.lnr_call_us);
        lnr_call_us.setVisibility(View.GONE);
        avl = findViewById(R.id.spinner);


        tvTop = findViewById(R.id.tvTop);
        tvTopTwo = findViewById(R.id.tvTopTwo);

        tvBottom = findViewById(R.id.tvBottom);
        tvBottomTwo = findViewById(R.id.tvBottomTwo);
        tvCall = findViewById(R.id.tvCall);

        imageView = findViewById(R.id.scratchImv);
        sch_back_arrow = findViewById(R.id.sch_back_arrow);

        tvCallTwo = (TextView) findViewById(R.id.tvCallTwo);
        tvCallTwo.setLinkTextColor(getResources().getColor(R.color.white));
        tvCallTwo.setText(phone);

        tvCallTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission(ScholarShipActivity.this);
            }
        });

        Spannable s = Spannable.Factory.getInstance().newSpannable(tvCallTwo.getText()
                .toString());
        removeUnderlines(s);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity();
            }
        });

        sch_back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ScholarShipActivity.this, NewDashboardActivity.class);
                startActivity(in);
                finish();
            }
        });

    }

    private void openActivity() {
        Intent intent = new Intent(this,
                ScholarScratchActivity.class);
        intent.putExtra("status", status);
        intent.putExtra("cart", cartSize);
        startActivity(intent);

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ScholarShipActivity.this, NewDashboardActivity.class);
        startActivity(i);

        finish();

    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);

        getData();

    }

    @Override
    protected void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);

    }

    private void getData() {
        showDialog();
        Bundle bundle = new Bundle();
        bundle.putLong("RegID", RegPrefManager.getInstance(this).getRegId());
        NetworkService.
                startActionGetScratch(this, mServiceReceiver, bundle);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public  boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.
                    CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)
                        context, Manifest.permission.CALL_PHONE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Call permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                callSetting();
                return true;
            }
        } else {
            callSetting();
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){

            callSetting();
        }


    }

    @SuppressLint("MissingPermission")
    public void callSetting(){

        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "1800 212 4322", null));
        startActivity(intent);
    }

    @Override
    public void onSaveInstanceState(Bundle outState,
                                    PersistableBundle outPersistentState) {
        hideDialog();
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        hideDialog();
        switch (resultCode) {
            case ResponseCodes.NO_INTERNET:
                break;
            case ResponseCodes.UNEXPECTED_ERROR:
                Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
                break;

            case 200:
                cartSize = resultData.getInt(
                        IntentHelper.EXTRA_DATA1);
                setData(resultData.getBoolean(
                        IntentHelper.EXTRA_DATA2
                ));
                Log.v(TAG, cartSize + ":" + resultData.getBoolean(
                        IntentHelper.EXTRA_DATA2
                ));
                break;


        }
    }

    private void setData(boolean status) {


        llData.setVisibility(View.VISIBLE);
        this.status = status;
        if (status) {
            imageView.setImageResource(R.drawable.ic_afterscratch);

            tvTop.setTextSize(27);
            tvTopTwo.setTextSize(22);
            tvBottom.setTextSize(20);
            tvBottomTwo.setTextSize(20);
            tvTop.setText(Html.fromHtml("<font color='#FE6E5D'>CONG</font><font color='#FF9160'>RATULATIONS! </font>"));
            tvTopTwo.setText(Html.fromHtml("<font color='#133049'>You have earned a  scholarship of </font>"));


            tvBottom.setText(Html.fromHtml("<font color='#000000'>Now, You can avail this scholarship</font>"));
            tvBottomTwo.setText(Html.fromHtml("<font color='#000000'>on your Package Subscription</font>"));


            lnr_call_us.setVisibility(View.VISIBLE);


        } else {
            imageView.setImageResource(R.drawable.fore);

            tvTop.setTextSize(25);
            tvTopTwo.setTextSize(25);

            tvBottom.setTextSize(20);
            tvBottomTwo.setTextSize(20);


            tvTop.setText(Html.fromHtml("<font color='#000000'>Get a Chance to </font> <font color='#FE6E5D'>W</font><font color='#FF9160'>IN</font> <font color='#000000'>your</font>"));
            tvTopTwo.setText(Html.fromHtml("<font color='#FE6E5D'>Educatio</font><font color='#FF9160'>nal Scholarship</font>"));

            tvBottom.setText(Html.fromHtml("<font color='#9b9b9b'>Scratch the card to</font>"));
            tvBottomTwo.setText(Html.fromHtml("<font color='#9b9b9b'>see what is in store for you</font>"));

            lnr_call_us.setVisibility(View.GONE);


        }
    }

    public void showDialog() {
        llData.setVisibility(View.GONE);
        avl.show();
    }

    public void hideDialog() {
        avl.hide();
    }

    public static void removeUnderlines(Spannable p_Text) {
        URLSpan[] spans = p_Text.getSpans(0, p_Text.length(), URLSpan.class);

        for (URLSpan span : spans) {
            int start = p_Text.getSpanStart(span);
            int end = p_Text.getSpanEnd(span);
            p_Text.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            p_Text.setSpan(span, start, end, 0);
        }
    }

}