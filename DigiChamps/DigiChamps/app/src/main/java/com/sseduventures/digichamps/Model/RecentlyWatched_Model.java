package com.sseduventures.digichamps.Model;

/**
 * Created by Tech_1 on 1/16/2018.
 */

public class RecentlyWatched_Model {
    private String Module_Id, Module_Title, Module_Name, Module_Video, Description,
            Module_Image, Image_Key, Is_Avail, Is_Free, Chapter_Id, media_id, video_key,Is_Expire;

    public RecentlyWatched_Model(String Module_Id, String Module_Title, String Module_Name, String Module_Video,
                                 String Description, String Module_Image,
                                 String Image_Key, String Is_Avail, String Is_Free,String Chapter_Id,
                                 String media_id,
                                 String video_key, String Is_Expire
                                 ) {


        this.Module_Id = Module_Id;
        this.Module_Title = Module_Title;
        this.Module_Name = Module_Name;
        this.Module_Video = Module_Video;
        this.Description = Description;
        this.Module_Image = Module_Image;
        this.Image_Key = Image_Key;
        this.Is_Avail = Is_Avail;
        this.Is_Free = Is_Free;
        this.Is_Avail = Is_Avail;
        this.Is_Free = Is_Free;
        this.Chapter_Id = Chapter_Id;
        this.media_id = media_id;
        this.video_key = video_key;
        this.Is_Expire = Is_Expire;
    }

    public String getModule_Id() {
        return Module_Id;
    }

    public String getModule_Title() {
        return Module_Title;
    }

    public String getModule_Name() {
        return Module_Name;
    }

    public String getModule_Video() {
        return Module_Video;
    }

    public String getDescription() {
        return Description;
    }

    public String getModule_Image() {
        return Module_Image;
    }

    public String getImage_Key() {
        return Image_Key;
    }

    public String getIs_Avail() {
        return Is_Avail;
    }

    public String getIs_Free() {
        return Is_Free;
    }

    public String getChapter_Id() {
        return Chapter_Id;
    }

    public String getMedia_id() {
        return media_id;
    }

    public String getVideo_key() {
        return video_key;
    }

    public String getIs_Expire() {
        return Is_Expire;
    }



}


