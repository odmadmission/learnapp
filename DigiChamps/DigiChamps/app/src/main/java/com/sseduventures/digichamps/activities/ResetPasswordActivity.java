package com.sseduventures.digichamps.activities;

/**
 * Author Abhishek
 */

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SmsListener;
import com.sseduventures.digichamps.helper.SmsReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.Random;


public class ResetPasswordActivity extends FormActivity implements ServiceReceiver.Receiver {

    private static final String TAG = ResetPasswordActivity.class.getSimpleName();
    private Button button;
    private TextInputLayout otp_layout, phone;
    private TextInputEditText otp_edit, ph_num_ed;
    private int PERMISSION_REQUEST_CODE = 1000;

    private int otp;
    private IntentFilter intentFilter;

    private Button btn;

    private String phoneStr;
    private TextView otp_msg,resend_tv,error_phone,error_otp;
    private Toolbar toolbar;

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    boolean flag_resend=true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_reset_password);

        requestContactPermission();
        init();

        if (AppUtil.isInternetConnected(ResetPasswordActivity.this)) {
            init();
        } else {

            AskOptionDialogNew("Please Check Your Internet Connection").show();
        }



    }

    private void init() {

        setUpReciever();
        intentFilter = new IntentFilter();
        intentFilter.addAction(IntentHelper.ACTION_SMS_RECIEVED);

        button = findViewById(R.id.submit_but);
        ph_num_ed = findViewById(R.id.ph_num_ed);
        phone = findViewById(R.id.phone);
        error_phone=findViewById(R.id.error_phone);


        Intent in = getIntent();

        if(in == null){

            ph_num_ed.setText("");

        }else if(in!=null){

            ph_num_ed.setText(in.getStringExtra("mobileNum"));

        }



        ph_num_ed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
               /* if (!ph_num_ed.getText().toString().isEmpty()) {
                    phone.setError(null);
                    phone.setErrorEnabled(false);
                }*/
                error_phone.setVisibility(View.GONE);
            }
        });





        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //  finish();
                Intent intent = new Intent(ResetPasswordActivity.this, OnBoardActivity.class);
                startActivity(intent);
                finish();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialog();
                flag_resend=true;
                if (validationPhone()) {
                    sendOtpToReset();
                }


            }
        });

    }


    @Override
    public void onBackPressed() {
        finish();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction() != null && intent.getAction().
                    equals(IntentHelper.ACTION_SMS_RECIEVED)) {

                fillOtp();
            }

        }
    };

    private void fillOtp() {
        int rCode = RegPrefManager.getInstance(this).getSmsTestRecievedCode();
        otp_edit.setText(rCode + "");
        otp_edit.setSelection(otp_edit.length());


    }

    private void dialog() {
        flag_resend=false;
        final Dialog dialog = new Dialog(ResetPasswordActivity.this);
        dialog.setContentView(R.layout.custom_dialog_otp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        ph_num_ed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!ph_num_ed.getText().toString().isEmpty()) {
                    phone.setError(null);
                    phone.setErrorEnabled(false);
                }
            }
        });

        btn = dialog.findViewById(R.id.btn);
        otp_layout = dialog.findViewById(R.id.otp_layout);
        otp_edit = dialog.findViewById(R.id.otp_edit);
        error_otp=dialog.findViewById(R.id.error_otp);

        otp_msg = dialog.findViewById(R.id.otp_msg);
        resend_tv=dialog.findViewById(R.id.resend_tv);

        otp_msg.setText("We've sent an sms with the OTP to " + phoneStr);

        otp_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
               /* if (!otp_edit.getText().toString().isEmpty()) {
                    otp_layout.setErrorEnabled(false);
                    otp_layout.setError(null);
                }*/
                error_otp.setVisibility(View.GONE);
            }
        });


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate(otp_layout, otp_edit)) {
                    hideKeyboard();

                    String checkOtp = RegPrefManager.getInstance(ResetPasswordActivity.this)
                            .getSmsTestSavedCode() + "";

                    otp_edit.setText(checkOtp);


                    String otp_value=otp_edit.getText().toString().trim();
                    if (checkOtp.equals(otp_value)) {
                        //otp_layout.setErrorEnabled(true);
                        dialog.dismiss();
                        Intent intent = new Intent(ResetPasswordActivity.this,
                                ChangePasswordActivity.class);

                        intent.putExtra("phNum", ph_num_ed.getText().toString().trim());
                        startActivity(intent);

                    } else {
                        //otp_layout.setErrorEnabled(true);
                       // otp_layout.setError("Enter correct OTP");
                       /* error_otp.setVisibility(View.VISIBLE);
                        error_otp.setText("Enter correct OTP");*/
                        Toast.makeText(getApplicationContext(), "Enter correct OTP"
                                , Toast.LENGTH_SHORT).show();
                    }



                }

            }
        });
        resend_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  getSignupOTP();
                sendOtpToReset();
            }
        });

        dialog.show();


        SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                otp_edit.setText(messageText);
            }
        });
    }
    /*private void getSignupOTP() {


        Random r = new Random(System.currentTimeMillis());
        otp = ((1 + r.nextInt(2)) * 100000 + r.nextInt(100000));


        Bundle bun = new Bundle();
        bun.putString("mobile", phoneStr);
        bun.putString("otp", otp + "");

        if (AppUtil.isInternetConnected(ResetPasswordActivity.this)) {

          showDialog(null, "Please wait");
            NetworkService.startActionGetSignUpMobile(ResetPasswordActivity.this, mServiceReceiver
                    , bun);

        } else {

            AskOptionDialogNew("Please check your Internet Connection").show();
            //Toast.makeText(mContext, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
        }
        dialog.dismiss();

    }*/
    private Boolean validate(final TextInputLayout otp_layout, final TextInputEditText otp_edit) {

        String otp_text = otp_edit.getText().toString().trim();

        if (otp_text.isEmpty()) {
            //AskOptionDialogNew("Please Enter OTP").show();
           // otp_layout.setError("Please Enter OTP");
            error_otp.setVisibility(View.VISIBLE);
            error_otp.setText("Please Enter OTP");
            return false;
        }

        if (otp_text.length() != 6) {

           // AskOptionDialogNew("Enter 6 Digits OTP").show();
            //otp_layout.setError("Enter 6 Digits OTP");
            error_otp.setVisibility(View.VISIBLE);
            error_otp.setText("Enter 6 Digits OTP");
            return false;
        }



        return true;
    }


    private void sendOtpToReset() {

        hideKeyboard();
        Random r = new Random(System.currentTimeMillis());
        otp = ((1 + r.nextInt(2)) * 100000 + r.nextInt(100000));


        String phNumber = ph_num_ed.getText().toString().trim();
        Bundle bun = new Bundle();
        bun.putString("mobile", phNumber);
        bun.putString("otp", otp + "");

        if (AppUtil.isInternetConnected(this)) {

            showDialog(null, "please wait");
            NetworkService.startActionGetResetPassword(ResetPasswordActivity.this,
                    mServiceReceiver, bun);


        } else {
            AskOptionDialogNew("Please Check Your Internet Connection").show();
        }
    }

    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                intentFilter);
    }

    @Override
    protected void onStop() {

        hideDialog();
        mServiceReceiver.setReceiver(null);
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        hideDialog();
        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                RegPrefManager.getInstance(this).setSmsTestSavedCode(otp);
                if(flag_resend==true) {
                    dialog();
                }
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(ResetPasswordActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(ResetPasswordActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(ResetPasswordActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.SERVER_ERROR:
                break;
            case ResponseCodes.NO_USER_FOUND:
                //AskOptionDialog("Mobile number not registered").show();
                error_phone.setVisibility(View.VISIBLE);
                error_phone.setText("Mobile number not registered");
                break;
        }
    }

    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    private AlertDialog AskOptionDialogNew(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })

                .create();
        return myQuittingDialogBox;
    }


    private boolean validationPhone() {

        phoneStr = ph_num_ed.getText().toString().trim();

        if (phoneStr.isEmpty()) {
            //AskOptionDialogNew("Please Enter Your Phone Number").show();
           // phone.setError("Please Enter Your Phone Number");
            error_phone.setVisibility(View.VISIBLE);
            error_phone.setText("Please Enter Your Phone Number");
            return false;
        }

        if (phoneStr.length() != 10) {
            //AskOptionDialogNew("Please Enter 10 Digit Mobile Number").show();
          //  phone.setError("Please Enter 10 Digit Mobile Number");
            error_phone.setVisibility(View.VISIBLE);
            error_phone.setText("Please Enter 10 Digit Mobile Number");
            return false;
        }


        return true;
    }

    private void requestContactPermission() {

        int hasContactPermission = ActivityCompat.checkSelfPermission(ResetPasswordActivity.this, Manifest.permission.RECEIVE_SMS);

        if (hasContactPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ResetPasswordActivity.this, new String[]{Manifest.permission.RECEIVE_SMS}, PERMISSION_REQUEST_CODE);
        } else {
            //Toast.makeText(AddContactsActivity.this, "Contact Permission is already granted", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                // Check if the only required permission has been granted
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                  //  Toast.makeText(this, "Contact Permission is Granted", Toast.LENGTH_SHORT).show();
                } else {

                }
                break;
        }
    }
}
