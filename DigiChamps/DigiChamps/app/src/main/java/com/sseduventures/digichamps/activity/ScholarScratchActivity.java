package com.sseduventures.digichamps.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.PersistableBundle;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.database.DbContract;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.scratch.ScratchView;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.DateTimeUtil;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
public class ScholarScratchActivity extends Activity
        implements ServiceReceiver.Receiver
{

    private static final String TAG=ScholarShipActivity.
            class.getSimpleName();

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private ScratchView mScratchView;
    private ImageView mImageView,sch_back_arrow_scr;
    private boolean started=false;
    private ProgressDialog progressDialog;
    private long userId;
    private String user_id;
    private TextView bfr_scratch_test;
    private Button btn_back;
    private FrameLayout frm_lay;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_scholar_scratch);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        init();
    }
    public void logEvent(String key,String event)
    {
        Bundle bundle = new Bundle();
        bundle.putString(key, DateTimeUtil.convertMillisAsDateTimeString(
                System.currentTimeMillis()));
        mFirebaseAnalytics.logEvent(event, bundle);
    }


    private void init()
    {

        userId = RegPrefManager.getInstance(this).getRegId();

        mHandler=new Handler();
        mServiceReceiver=new ServiceReceiver(mHandler);

        mScratchView=findViewById(R.id.scratch);
        mImageView=findViewById(R.id.scratchImv);

        btn_back = (Button) findViewById(R.id.btn_back);
        bfr_scratch_test = (TextView) findViewById(R.id.bfr_scratch_test);

        sch_back_arrow_scr = (ImageView) findViewById(R.id.sch_back_arrow_scr);

        frm_lay = (FrameLayout) findViewById(R.id.frm_lay);

        frm_lay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    if(getIntent().getBooleanExtra("status",false))
                    {
                       finish();
                    }



                }

                return true;
            }
        });


        if(getIntent().getBooleanExtra("status",false))
        {
            mImageView.setVisibility(View.VISIBLE);
            mScratchView.setVisibility(View.GONE);
            btn_back.setVisibility(View.VISIBLE);
            bfr_scratch_test.setVisibility(View.GONE);
        }
        else {
            mScratchView.setVisibility(View.VISIBLE);
            mImageView.setVisibility(View.GONE);
            btn_back.setVisibility(View.GONE);
            bfr_scratch_test.setVisibility(View.VISIBLE);

        }


        mScratchView.setOnScratchCompletedListener(
                new ScratchView.OnScratchCompletedListener() {
            @Override
            public void onScratchCompleted(ScratchView scratchView)
            {
               if(!started) {
                   started=true;
                   saveScratch();


               }
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RegPrefManager.getInstance(
                        ScholarScratchActivity.this
                ).setReedeem(1);
                if(getIntent().getIntExtra("cart",0)>0) {
                    Intent in = new Intent(ScholarScratchActivity.this,
                            Cart_Activity.class);
                    in.putExtra("discount",1);
                    startActivity(in);
                    finish();

                }
                else
                {
                    Intent in = new Intent(ScholarScratchActivity.this,
                            PackageActivityNew.class);
                    in.putExtra("discount",1);
                    startActivity(in);
                    finish();
                }

            }
        });

        sch_back_arrow_scr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ScholarScratchActivity.this,ScholarShipActivity.class);
                startActivity(in);
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);


    }

    @Override
    protected void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);

    }

    private void saveScratch()
    {


            showDialog();
            Bundle bundle = new Bundle();
            bundle.putLong("RegId", userId);
            bundle.putInt("ScholarId", 1);
            NetworkService.
                    startActionUpdateScratch(this,
                            mServiceReceiver, bundle);

    }

    @Override
    public void onSaveInstanceState(Bundle outState,
                                    PersistableBundle outPersistentState) {
           hideDialog();
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData)
    {



        hideDialog();
        switch (resultCode)
        {
            case ResponseCodes.NO_INTERNET :
                mImageView.setVisibility(View.GONE);
                mScratchView.setVisibility(View.VISIBLE);
                started=false;
                break;
            case ResponseCodes.UNEXPECTED_ERROR :
                mImageView.setVisibility(View.GONE);
                mScratchView.setVisibility(View.VISIBLE);
                started=false;
                break;

            case 200 :
                mImageView.setVisibility(View.VISIBLE);
                mScratchView.setVisibility(View.GONE);
                btn_back.setVisibility(View.VISIBLE);
                bfr_scratch_test.setVisibility(View.GONE);
                break;



        }
    }


    private void showDialog()
    {
        if(progressDialog==null)
        {
            progressDialog=
                    ProgressDialog.
                            show(this,null,"please wait");
        }
    }

    private void hideDialog()
    {

        if(progressDialog!=null)
        {
            progressDialog.dismiss();
            progressDialog=null;

        }
    }

    protected String moduleName;
    protected long rowId=0;
    public void setModuleName(String moduleName)
    {
        this.moduleName=moduleName;
    }



    public class ActivityLogSaveTask extends AsyncTask<String,Void,Long>
    {


        @Override
        protected Long doInBackground(String... strings)
        {

            long time=System.currentTimeMillis();
            ContentValues mContentValues=new ContentValues();
            mContentValues.put(DbContract.
                    ActivityTimeTable.MODULEID,strings[0]);

            mContentValues.put(DbContract.ActivityTimeTable.REGDID,
                    RegPrefManager.getInstance(ScholarScratchActivity.this).getRegId());

            mContentValues.put(DbContract.ActivityTimeTable.SCHOOL_CODE,
                    RegPrefManager.getInstance(ScholarScratchActivity.this).
                            getSchoolId());
            mContentValues.put(DbContract.ActivityTimeTable.SECTION_CODE,
                    RegPrefManager.getInstance(ScholarScratchActivity.this).
                            getSectionId());
            mContentValues.put(DbContract.ActivityTimeTable.CLASS_CODE,
                    RegPrefManager.getInstance(ScholarScratchActivity.this).
                            getclassid());

            mContentValues.put(DbContract.ActivityTimeTable.STATUS,
                    Integer.parseInt(strings[1]));

            mContentValues.put(DbContract.ActivityTimeTable.START_TIME,
                    time);

            mContentValues.put(DbContract.ActivityTimeTable.END_TIME,
                    time);


            mContentValues.put(DbContract.ActivityTimeTable.SYNC_DATE,
                    time);

            mContentValues.put(DbContract.ActivityTimeTable.CREATE_DATE,
                    time);

            mContentValues.put(DbContract.ActivityTimeTable.SEEN_TIME,
                    Integer.parseInt(strings[2]));



            Uri uri= getContentResolver().
                    insert(DbContract.ActivityTimeTable.CONTENT_URI,
                            mContentValues);

            if(uri!=null)
            {
                rowId=Integer.parseInt(
                        uri.getLastPathSegment());

                Log.v(TAG,uri.getLastPathSegment());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
        }
    }
    public class ActivityLogUpdateTask extends AsyncTask<String,Void,Long>
    {


        @Override
        protected Long doInBackground(String... strings)
        {
            ContentValues mContentValues=new ContentValues();

            mContentValues.put(DbContract.ActivityTimeTable.STATUS,
                    Integer.parseInt(strings[1]));

            mContentValues.put(DbContract.ActivityTimeTable.SEEN_TIME,
                    Integer.parseInt(strings[2]));

            mContentValues.put(DbContract.ActivityTimeTable.END_TIME,
                    System.currentTimeMillis());

            long update= getContentResolver().
                    update(DbContract.ActivityTimeTable.CONTENT_URI,
                            mContentValues,
                            DbContract.ActivityTimeTable._ID+" LIKE ? ",
                            new String[]{strings[0]});



            Log.v(TAG,update+"");


            if(update==1)
            {
                rowId=0;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
        }
    }

}
