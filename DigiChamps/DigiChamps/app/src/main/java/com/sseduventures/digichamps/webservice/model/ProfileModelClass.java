package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ProfileModelClass implements Parcelable {

    private ArrayList<BoardList> boardList = null;

    public ArrayList<BoardList> getBoardList() {
        return boardList;
    }

    public ArrayList<String> getSectionList() {
        return sectionList;
    }

    public void setSectionList(ArrayList<String> sectionList) {
        this.sectionList = sectionList;
    }

    public ArrayList<String> sectionList =null;

    public void setBoardList(ArrayList<BoardList> boardList) {
        this.boardList = boardList;
    }

    public ArrayList<SchoolInfoList> getSchoolInfoList() {
        return schoolInfoList;
    }

    public void setSchoolInfoList(ArrayList<SchoolInfoList> schoolInfoList) {
        this.schoolInfoList = schoolInfoList;
    }

    private ArrayList<SchoolInfoList> schoolInfoList = null;

    protected ProfileModelClass(Parcel in) {
    }

    public static final Creator<ProfileModelClass> CREATOR = new Creator<ProfileModelClass>() {
        @Override
        public ProfileModelClass createFromParcel(Parcel in) {
            return new ProfileModelClass(in);
        }

        @Override
        public ProfileModelClass[] newArray(int size) {
            return new ProfileModelClass[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }


    public class BoardList implements Parcelable {

        protected BoardList(Parcel in) {
            if (in.readByte() == 0) {
                boardId = null;
            } else {
                boardId = in.readInt();
            }
            boardName = in.readString();
        }

        public final Creator<BoardList> CREATOR = new Creator<BoardList>() {
            @Override
            public BoardList createFromParcel(Parcel in) {
                return new BoardList(in);
            }

            @Override
            public BoardList[] newArray(int size) {
                return new BoardList[size];
            }
        };

        public Integer getBoardId() {
            return boardId;
        }

        public void setBoardId(Integer boardId) {
            this.boardId = boardId;
        }

        public ArrayList<ClassList> getClassList() {
            return classList;
        }

        public void setClassList(ArrayList<ClassList> classList) {
            this.classList = classList;
        }

        public String getBoardName() {
            return boardName;
        }

        public void setBoardName(String boardName) {
            this.boardName = boardName;
        }

        private Integer boardId;
        private ArrayList<ClassList> classList = null;
        private String boardName;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (boardId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(boardId);
            }
            dest.writeString(boardName);
        }
    }

    public class ClassList implements Parcelable {

        private Integer classId;
        private Object boardId;
        private String className;
        private Object insertedBy;

        protected ClassList(Parcel in) {
            if (in.readByte() == 0) {
                classId = null;
            } else {
                classId = in.readInt();
            }
            className = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (classId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(classId);
            }
            dest.writeString(className);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public final Creator<ClassList> CREATOR = new Creator<ClassList>() {
            @Override
            public ClassList createFromParcel(Parcel in) {
                return new ClassList(in);
            }

            @Override
            public ClassList[] newArray(int size) {
                return new ClassList[size];
            }
        };

        public Integer getClassId() {
            return classId;
        }

        public void setClassId(Integer classId) {
            this.classId = classId;
        }

        public Object getBoardId() {
            return boardId;
        }

        public void setBoardId(Object boardId) {
            this.boardId = boardId;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public Object getInsertedBy() {
            return insertedBy;
        }

        public void setInsertedBy(Object insertedBy) {
            this.insertedBy = insertedBy;
        }

        public Object getModifiedDate() {
            return modifiedDate;
        }

        public void setModifiedDate(Object modifiedDate) {
            this.modifiedDate = modifiedDate;
        }

        public Object getModifiedBy() {
            return modifiedBy;
        }

        public void setModifiedBy(Object modifiedBy) {
            this.modifiedBy = modifiedBy;
        }

        public Object getActive() {
            return active;
        }

        public void setActive(Object active) {
            this.active = active;
        }

        public Object getDeleted() {
            return deleted;
        }

        public void setDeleted(Object deleted) {
            this.deleted = deleted;
        }

        public Object getInsertedDate() {
            return insertedDate;
        }

        public void setInsertedDate(Object insertedDate) {
            this.insertedDate = insertedDate;
        }

        private Object modifiedDate;
        private Object modifiedBy;
        private Object active;
        private Object deleted;
        private Object insertedDate;

    }

    public class SchoolInfoList implements Parcelable {
        private String schoolId;
        private String schoolName;

        protected SchoolInfoList(Parcel in) {
            schoolId = in.readString();
            schoolName = in.readString();
            schoolDescription = in.readString();
            schoolLogo = in.readString();
            schoolVideo = in.readString();
            schoolThumbnail = in.readString();
            modifiedDate = in.readString();
            byte tmpActive = in.readByte();
            active = tmpActive == 0 ? null : tmpActive == 1;
            creationDate = in.readString();
        }

        public final Creator<SchoolInfoList> CREATOR = new Creator<SchoolInfoList>() {
            @Override
            public SchoolInfoList createFromParcel(Parcel in) {
                return new SchoolInfoList(in);
            }

            @Override
            public SchoolInfoList[] newArray(int size) {
                return new SchoolInfoList[size];
            }
        };

        public String getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(String schoolId) {
            this.schoolId = schoolId;
        }

        public String getSchoolName() {
            return schoolName;
        }

        public void setSchoolName(String schoolName) {
            this.schoolName = schoolName;
        }

        public String getSchoolDescription() {
            return schoolDescription;
        }

        public void setSchoolDescription(String schoolDescription) {
            this.schoolDescription = schoolDescription;
        }

        public String getSchoolLogo() {
            return schoolLogo;
        }

        public void setSchoolLogo(String schoolLogo) {
            this.schoolLogo = schoolLogo;
        }

        public String getSchoolVideo() {
            return schoolVideo;
        }

        public void setSchoolVideo(String schoolVideo) {
            this.schoolVideo = schoolVideo;
        }

        public String getSchoolThumbnail() {
            return schoolThumbnail;
        }

        public void setSchoolThumbnail(String schoolThumbnail) {
            this.schoolThumbnail = schoolThumbnail;
        }

        public String getModifiedDate() {
            return modifiedDate;
        }

        public void setModifiedDate(String modifiedDate) {
            this.modifiedDate = modifiedDate;
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        private String schoolDescription;
        private String schoolLogo;
        private String schoolVideo;
        private String schoolThumbnail;
        private String modifiedDate;
        private Boolean active;
        private String creationDate;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(schoolId);
            dest.writeString(schoolName);
            dest.writeString(schoolDescription);
            dest.writeString(schoolLogo);
            dest.writeString(schoolVideo);
            dest.writeString(schoolThumbnail);
            dest.writeString(modifiedDate);
            dest.writeByte((byte) (active == null ? 0 : active ? 1 : 2));
            dest.writeString(creationDate);
        }
    }


}

