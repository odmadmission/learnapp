package com.sseduventures.digichamps.Model;

/**
 * Created by Tech_1 on 1/9/2018.
 */

public class Bookmarks_frag2_model {

    private String Module_Id, ModuleName,pdf_path,Is_Expire,Is_Free,Is_Avail ;

        public Bookmarks_frag2_model( String Module_Id,String ModuleName,String pdf_path,String Is_Expire,String Is_Free,String Is_Avail
        ) {

            this.Module_Id = Module_Id;
            this.ModuleName = ModuleName;
            this.pdf_path = pdf_path;
            this.Is_Expire = Is_Expire;
            this.Is_Free = Is_Free;
            this.Is_Avail = Is_Avail;

        }

    public String getModule_Id() {
        return Module_Id;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public String getPdf_path() {
        return pdf_path;
    }

    public String getIs_Expire() {
        return Is_Expire;
    }

    public String getIs_Free() {
        return Is_Free;
    }

    public String getIs_Avail() {
        return Is_Avail;
    }
}



