package com.sseduventures.digichamps.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by user on 4/23/2018.
 */

public class IntentHelper
{
    public static final String ACTION_LOGIN =
            Constants.PACKAGENAME+".services.action.LOGIN";

    public static final String ACTION_COIN_RELOAD =
            Constants.PACKAGENAME+".services.action.coin.reload";

    public static final String ACTION_LOGOUT_NOTIFI =
            Constants.PACKAGENAME+".services.action.logout.notification";


    public static final String ACTION_SEND_MESSAGE =
            Constants.PACKAGENAME+".services.action.send.message";
    public static final String ACTION_LEARN_CHAP_DETAILS =
            Constants.PACKAGENAME+".services.action.learn.chap.details";

    public static final String ACTION_SET_UNSET_BOOKMARKS =
            Constants.PACKAGENAME+".services.action.set.unset.bookmarks";


    public static final String ACTION_POST_FEEDEBACK_DATA =
            Constants.PACKAGENAME+".services.action.get.post.feedbackform.data";

    public static final String ACTION_POST_ORDERDETAILS_DATA =
            Constants.PACKAGENAME+".services.action.get.post.orderdetails.data";

    public static final String ACTION_GET_EXAM =
            Constants.PACKAGENAME+".services.action.get.exam.data";

    public static final String ACTION_POST_EXAM_DATA =
            Constants.PACKAGENAME+".services.action.get.post.exam.data";

    public static final String ACTION_POST_DOUBTDETAILS_DATA =
            Constants.PACKAGENAME+".services.action.get.post.doubtdetails.data";



    public static final String ACTION_ADD_TO_CART =
            Constants.PACKAGENAME+".services.action.add.to.cart";


    public static final String ACTION_GET_CART_DATA =
            Constants.PACKAGENAME+".services.action.get.cart.data";

    public static final String ACTION_REMOVE_CART_DATA =
            Constants.PACKAGENAME+".services.action.remove.cart.data";

    public static final String ACTION_EXAM_SCHEDULE=
            Constants.PACKAGENAME+".services.action.get.exam.schedule";

    public static final String ACTION_EXAM_SCHEDULE_DETAILS=
            Constants.PACKAGENAME+".services.action.get.exam.schedule.details";

    public static final String ACTION_DAILYTT_DETAILS=
            Constants.PACKAGENAME+".services.action.get.dailytt.details";

    public static final String ACTION_ASSIGN_TEACHER_DETAILS=
            Constants.PACKAGENAME+".services.action.post.assign.teacher.details";

    public static final String ACTION_STUDY_MATERIALS_DETAILS=
            Constants.PACKAGENAME+".services.action.post.study.materials.details";

    public static final String ACTION_BOARD_DETAILS=
            Constants.PACKAGENAME+".services.action.post.board.details";

    public static final String ACTION_CLASS_DETAILS=
            Constants.PACKAGENAME+".services.action.post.class.details";

    public static final String ACTION_TOPPERS_DETAILS=
            Constants.PACKAGENAME+".services.action.post.toppers.details";

    public static final String ACTION_NOTICES_DETAILS=
            Constants.PACKAGENAME+".services.action.post.notices.details";

    public static final String ACTION_ADD_DISCUSSION_DETAILS=
            Constants.PACKAGENAME+".services.action.post.add.discussion.details";

    public static final String ACTION_ADD_FORUM_DETAILS=
            Constants.PACKAGENAME+".services.action.post.add.forum.details";


    public static final String ACTION_DISCUSSION_DETAILS=
            Constants.PACKAGENAME+".services.action.post.discussion.details";

    public static final String ACTION_DETAILS_FORUM_DETAILS=
            Constants.PACKAGENAME+".services.action.post.forum.details";


    public static final String ACTION_POST_HOMEWORK=
            Constants.PACKAGENAME+"services.action.post.homework";


    public static final String ACTION_GET_INFO=
            Constants.PACKAGENAME+".services.action.get.info.details";

    public static final String ACTION_GET_PM_DETAILS=
            Constants.PACKAGENAME+".services.action.get.pm.details";


    public static final String ACTION_GET_BOOKMARK_VIDEO=
            Constants.PACKAGENAME+".services.action.get.bookmark.video";

    public static final String ACTION_GET_BOOKMARK_STUDY_NOTES=
            Constants.PACKAGENAME+".services.action.get.bookmark.study.notes";

    public static final String ACTION_GET_BOOKMARK_QUESTION=
            Constants.PACKAGENAME+".services.action.get.bookmark.questions";

    public static final String ACTION_GET_BOARD_CLASS=
            Constants.PACKAGENAME+".services.action.get.board.class.details";

    public static final String ACTION_GET_ORDER_CONF=
            Constants.PACKAGENAME+".services.action.get.order.conf.details";


    public static final String ACTION_GET_SUBMIT_DOUBT=
        Constants.PACKAGENAME+".services.action.get.submit.doubt";

    public static final String ACTION_GET_REPLY_DOUBT=
            Constants.PACKAGENAME+".services.action.get.reply.doubt";

    public static final String ACTION_GET_CBT_EXAM=
            Constants.PACKAGENAME+".services.action.get.cbt.exam";

    public static final String ACTION_GET_PRT_EXAM=
            Constants.PACKAGENAME+".services.action.get.prt.exam";


    public static final String ACTION_GET_PACKAGES =
            Constants.PACKAGENAME+".services.action.get.packages";


    public static final String ACTION_PSYCHO_GET_EXAM =
            Constants.PACKAGENAME+".services.action.get.psycho.exam";

    public static final String ACTION_PSYCHO_POST_EXAM =
            Constants.PACKAGENAME+".services.action.post.psycho.exam";

    public static final String ACTION_SCLEADERBOARD_DATA =
            Constants.PACKAGENAME+".services.action.post.scleaderboard.data";



    public static final String ACTION_DICTONARY_POST =
            Constants.PACKAGENAME+".services.action.post.dictonary";

    public static final String ACTION_ORDER_CONFIRMATION =
            Constants.PACKAGENAME+".services.action.order.confirm";


    public static final String ACTION_GET_ORDERS =
            Constants.PACKAGENAME+".services.action.get.orders";

    public static final String ACTION_GET_PROFILE_DATA =
            Constants.PACKAGENAME+".services.action.get.profile.data";

    public static final String ACTION_GET_FAQ_DATA =
            Constants.PACKAGENAME+".services.action.get.faq.data";

    public static final String ACTION_GET_FEED_DATA =
            Constants.PACKAGENAME+".services.action.get.feed.data";


    public static final String ACTION_SMS_RECIEVED =
            Constants.PACKAGENAME+".services.action.sms.recieved";

    public static final String ACTION_GET_RESET_PASSWORD =
            Constants.PACKAGENAME+".services.action.get.reset.password.data";

    public static final String ACTION_GET_SIGNUP_MOBILE =
            Constants.PACKAGENAME+".services.action.get.signup.mobile.data";


    public static final String ACTION_GET_ENCASH =
            Constants.PACKAGENAME+".services.action.get.encash.data";


    public static final String ACTION_GET_SUBJECT_DETAILS =
            Constants.PACKAGENAME+".services.action.get.subject.details.data";


    public static final String ACTION_GET_CHANGE_PASSWORD =
            Constants.PACKAGENAME+".services.action.get.change.password.data";

    public static final String ACTION_GET_SIGNUP =
            Constants.PACKAGENAME+".services.action.get.signup.data";

    public static final String ACTION_GET_OLYMPIAD_DATA =
            Constants.PACKAGENAME+".services.action.get.olympiad.data";

    public static final String ACTION_GET_PRVS_YR_PAPER_DATA =
            Constants.PACKAGENAME+".services.action.get.prvs.yr.paper.data";

    public static final String ACTION_GET_NCERT_SOLUTION_DATA =
            Constants.PACKAGENAME+".services.action.get.ncert.solution.data";

    public static final String ACTION_GET_DOUBT_LIST_DATA =
            Constants.PACKAGENAME+".services.action.get.doubt.data";

    public static final String ACTION_GET_NEW_DOUBT_LIST =
            Constants.PACKAGENAME+".services.action.get.new.doubt.list.data";

    public static final String ACTION_GET_APP_STATUS =
            Constants.PACKAGENAME+".services.action.get.app.status.data";


    public static final String ACTION_POST_CHANGE_PASSWORD =
            Constants.PACKAGENAME+".services.action.post.change.password";

    public static final String ACTION_GET_SCRATCH =
            Constants.PACKAGENAME+".services.action.get.scratch";

    public static final String ACTION_ASSIGN_SCHOOL_ZONE =
            Constants.PACKAGENAME+".services.action.assign.schoolzone";

    public static final String ACTION_CHAPTER_DETAILS =
            Constants.PACKAGENAME+".services.action.chapter.details";

    public static final String ACTION_RECENTLY_WATCHED =
            Constants.PACKAGENAME+".services.action.recently.watched";

    public static final String ACTION_DISCOUNT_COUPON =
            Constants.PACKAGENAME+".services.action.discount.coupon";

    public static final String ACTION_LEARN_CHAP_DETAILS_NEW =
            Constants.PACKAGENAME+".services.action.learn.chap.details.new";




    public static final String ACTION_SAVE_VIDEO_COINS =
            Constants.PACKAGENAME+".services.action.save.video.coins";

    public static final String ACTION_GET_COIN_LEADERBOARD =
            Constants.PACKAGENAME+".services.action.get.coins.leaderboard";

    public static final String ACTION_LOGOUT =
            Constants.PACKAGENAME+".services.action.logout";

    public static final String ACTION_NEW_LOGIN =
            Constants.PACKAGENAME+".services.action.new.login";

    public static final String ACTION_NEW_SIGNUP =
            Constants.PACKAGENAME+".services.action.new.signup";

    public static final String ACTION_SET_UNSET_BOOKMARK =
            Constants.PACKAGENAME+".services.action.set.unset.bookmark";


    public static final String ACTION_NEW_LEARN =
            Constants.PACKAGENAME+".services.action.new.learn";
    public static final String ACTION_GET_NEW_DASHBOARD =
            Constants.PACKAGENAME+".services.action.get.new.dashboard";


    public static final String ACTION_UPDATE_SCRATCH =
            Constants.PACKAGENAME+".services.action.update.scratch";

    public static final String ACTION_GET_TESTHIGLIGHTS_DATA =
            Constants.PACKAGENAME+".services.action.get.testhiglights.data";

    public static final String ACTION_GET_LEADERBOARD_DATA =
            Constants.PACKAGENAME+".services.action.get.leaderboard.data";
    public static final String ACTION_GET_LOGOUT_DATA =
            Constants.PACKAGENAME+".services.action.get.logout.data";

    public static final String ACTION_GET_SUBCONCEPT_DATA =
            Constants.PACKAGENAME+".services.action.get.subconcept.data";

    public static final String ACTION_GET_ANALYTICS_SUBJECT_DATA =
            Constants.PACKAGENAME+".services.action.get.analytics.subject.data";
    public static final String ACTION_GET_ANALYTICS_DETAILS_DATA =
            Constants.PACKAGENAME+".services.action.get.analytics.details.data";

    public static final String ACTION_GET_REVIEW_SOLUTION_DATA =
            Constants.PACKAGENAME+".services.action.get.review.solution.data";
    public static final String ACTION_GET_CHECKOUT_DATA =
            Constants.PACKAGENAME+".services.action.get.checkout.data";

    public static final String ACTION_POST_IMAGE =
            Constants.PACKAGENAME+".services.action.post.image.data";

    public static final String ACTION_POST_EDIT_PROFILE =
            Constants.PACKAGENAME+".services.action.post.edit.profile.data";

    public static final String ACTION_NEW_MESSAGE =
            Constants.PACKAGENAME+".services.action.new.message";

    public static final String ACTION_SEND_IMAGE =
            Constants.PACKAGENAME+".services.action.send.image";

    public static final String ACTION_GET_LIST =
            Constants.PACKAGENAME+".services.action.get.list";

    public static final String EXTRA_DATA1 =
            Constants.PACKAGENAME+".extras.data.data1";

    public static final String EXTRA_DATA2 =
            Constants.PACKAGENAME+".extras.data.data2";

    public static final String RESULT_DATA =
            Constants.PACKAGENAME+".extras.data.result";


    public static final String BUNDLE_DATA1 =
            Constants.PACKAGENAME+".extras.data.bundle1";

    public static final String BUNDLE_DATA2 =
            Constants.PACKAGENAME+".extras.data.bundle2";


    public static final String EXTRA_PARCELABLE =
            Constants.PACKAGENAME+".extras.data.parcelable";



    public static final String INTENT_DATA =
            Constants.PACKAGENAME+".intent.data";

    public static final String RESULT_EXCEPTION =
            Constants.PACKAGENAME+".extras.result.exception";


    public static void openActivity(Context context,
                                    Class cl, Bundle data, boolean finish)
    {

        Intent intent=new Intent(context,cl);
        if(data!=null)
        intent.putExtra(INTENT_DATA,data);
        context.startActivity(intent);

        if(finish)
        {
            if(context instanceof Activity)
            ((Activity)context).finish();
            else
            throw new UnsupportedOperationException("Context is not a Activity");
        }


    }
}
