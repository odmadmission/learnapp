package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.activities.SettingsActivity;
import com.sseduventures.digichamps.adapter.Order_Adapter;
import com.sseduventures.digichamps.adapter.RecyclerTouchListener;
import com.sseduventures.digichamps.domain.MyOrdersResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Order extends FormActivity implements ServiceReceiver.Receiver{

    private List<com.sseduventures.digichamps.domain.Order> list;
    private RecyclerView recyclerView;
    private Order_Adapter mAdapter;
    private String TAG = Order.class.getSimpleName();
    private SpotsDialog dialog;
    private Toolbar toolbar;
    private LinearLayout  order_recycle;
    private Button buy_btn;
    private RelativeLayout rltv_out;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private MyOrdersResponse success;
    private ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_order);
        setModuleName(LogEventUtil.EVENT_Order);

        logEvent(LogEventUtil.KEY_Order,LogEventUtil.EVENT_Order);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        init();

        if (AppUtil.isInternetConnected(this)) {

            getOrders();

        } else {

            Intent intent = new Intent(Order.this, Internet_Activity.class);
            startActivity(intent);
            finish();

        }


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        buy_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Order.this, PackageActivityNew.class));


            }
        });

    }


    private void init() {

        mHandler=new Handler();
        mServiceReceiver=new ServiceReceiver(mHandler);
        toolbar = (Toolbar) findViewById(R.id.toolbar_order);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Orders");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        dialog = new SpotsDialog(this, getResources().getString(R.string.dialog_text), R.style.Custom);

        list = new ArrayList<>();
        buy_btn = (Button) findViewById(R.id.buy_now);
        rltv_out = (RelativeLayout) findViewById(R.id.rlEmpty);

        order_recycle = (LinearLayout) findViewById(R.id.order_linlayout);
        mAdapter=new Order_Adapter(list,this);
        recyclerView.setAdapter(mAdapter);
    }

    private void getOrders()
    {
        if(AppUtil.isInternetConnected(this))
        {
            NetworkService.startActionGetOrders(this,mServiceReceiver);
        }
        else
        {

        }
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onBackPressed() {

        if(getIntent().getIntExtra("activity",0)==0) {
            Intent in = new Intent(Order.this,
                    NewDashboardActivity.class);
            startActivity(in);
            finish();
        }
        else
        {
            finish();

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }


    @Override
    protected void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        //hideDialog();
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        switch (resultCode)
        {
            case ResponseCodes.SUCCESS:
                 success=resultData.getParcelable(IntentHelper.RESULT_DATA);

                 if(success!=null&&success.getSuccess()!=null
                         &&success.getSuccess().getOrder()!=null&&
                         success.getSuccess().getOrder().size()>0) {
                     list.clear();
                     list.addAll(success.getSuccess().getOrder());
                     mAdapter.notifyDataSetChanged();
                     rltv_out.setVisibility(View.GONE);
                     order_recycle.setVisibility(View.VISIBLE);
                 }
                 else
                 {
                     order_recycle.setVisibility(View.GONE);
                     rltv_out.setVisibility(View.generateViewId());
                 }
                break;
            case ResponseCodes.FAILURE:
                order_recycle.setVisibility(View.GONE);
                rltv_out.setVisibility(View.GONE);
                break;
            case ResponseCodes.EXCEPTION:
                order_recycle.setVisibility(View.GONE);
                rltv_out.setVisibility(View.GONE);
                break;
            case ResponseCodes.NO_INTERNET:
                order_recycle.setVisibility(View.GONE);
                rltv_out.setVisibility(View.GONE);
                break;
        }
    }

}
