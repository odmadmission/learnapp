package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 8/31/2018.
 */

public class DoubtReplySuccess implements Parcelable{

    private String message;

    protected DoubtReplySuccess(Parcel in) {
        message = in.readString();
    }

    public static final Creator<DoubtReplySuccess> CREATOR = new Creator<DoubtReplySuccess>() {
        @Override
        public DoubtReplySuccess createFromParcel(Parcel in) {
            return new DoubtReplySuccess(in);
        }

        @Override
        public DoubtReplySuccess[] newArray(int size) {
            return new DoubtReplySuccess[size];
        }
    };

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(message);
    }
}
