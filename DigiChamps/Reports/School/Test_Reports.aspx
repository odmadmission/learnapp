﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test_Reports.aspx.cs" Inherits="DigiChamps.Reports.School.Test_Reports" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

</head>
<body>
    <form id="form1" runat="server">
        <div id="print" runat="server">
            <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
            <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
            <style>
                #LineChart1__ParentDiv {
                    border-color: transparent;
                }
            </style>
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
            <script type="text/javascript">
                window.onload = function () {
                    var $svg = $("#LineChart1").find("svg");
                    var $series = $svg.find("text[id=SeriesAxis]");
                    $series.each(function (index, element) {
                        var $element = $(element);
                        var x = $element.attr("x");
                        var y = +$element.attr("y") + 10;
                        $element.removeAttr("x");
                        $element.removeAttr("y");
                        $element.attr("transform", "translate(" + x + ", " + y + ") rotate(-270)");
                    });
                };
                
                function printDiv(divName) {
                    var printContents = document.getElementById(divName).innerHTML;
                    var originalContents = document.body.innerHTML;

                    document.body.innerHTML = printContents;

                    window.print();

                    document.body.innerHTML = originalContents;
                }
            </script>
           
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>

            <div class="container">
                <div class="col-md-12">
                    <div class="form-group">
                        <%--<div class="col-md-2">--%>
                            <asp:Image ID="imglogo" runat="server" Height="120" Width="120" />
                        <%--</div>--%>
                       <%-- <div class="col-md-10" style="font-weight: bold; font-size: 18px;">--%>
                            <asp:Label ID="lblschoolname" runat="server" style="font-weight: bold; font-size: 18px;"></asp:Label>
                       <%-- </div>--%>
                    </div>
                    <div class="clearfix" style="margin-bottom: 8px;"></div>
                    <div class="form-group text-center" style="font-size: 15px; font-weight: bold; text-decoration: underline;">
                        TEST& SUB-CONCEPT ANALYSIS
                    </div>
                    <div class="form-group" style="font-size: 15px">
                        <b>Class :</b> &nbsp;&nbsp;
                    <asp:Label ID="lblclass" runat="server"></asp:Label>&nbsp;&nbsp;
                   <b>Section :</b> &nbsp;&nbsp;
                    <asp:Label ID="lblsection" runat="server"></asp:Label>&nbsp;&nbsp;
                    </div>
                    <div class="form-group" style="font-size: 15px">
                        <b>Chapter :</b> &nbsp;&nbsp;
                    <asp:Label ID="lblchapter" runat="server"></asp:Label>&nbsp;&nbsp;                   
                    </div>
                    <div class="form-group" style="font-size: 15px">
                        <b>Test :</b> &nbsp;&nbsp;
                    <asp:Label ID="lbltest" runat="server"></asp:Label>&nbsp;&nbsp;                   
                    </div>
                    <div class="form-group" style="font-size: 15px; font-weight: bold;">
                        CLASS PERFORMANCE GRAPH
                    </div>
                    <div class="form-group">
                         
                          <asp:LineChart ID="LineChart1" runat="server" ChartHeight="400" ChartWidth="750"
    ChartType="Basic" ChartTitleColor="#0E426C" CategoryAxisLineColor="#D08AD9"
    ValueAxisLineColor="#D08AD9" BaseLineColor="#A156AB" BorderStyle="None" Width="730px">
                     
                    </asp:LineChart>
                    </div>
                    <div class="form-group" style="font-size: 15px">
                        <b>Full Mark :</b> &nbsp;&nbsp;
                    <asp:Label ID="lblfullmark" runat="server"></asp:Label>&nbsp;&nbsp;                   
                    </div>
                    <div class="form-group" style="font-size: 15px">
                        <b>Class Average Percentage :</b> &nbsp;&nbsp;
                    <asp:Label ID="lblpercentage" runat="server"></asp:Label>&nbsp;&nbsp;                   
                    </div>
                    <div class="form-group" style="font-size: 15px; font-weight: bold;">
                        INDIVIDUAL ANALYSIS
                    </div>
                    <div class="form-group text-center">
                        <asp:GridView ID="gvindividual" runat="server" AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="Sl. No.">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="STUDENT'S NAME">
                                    <ItemTemplate>
                                        <asp:Label ID="lblname" runat="server" Text='<%# Eval("Student_Name") %>'></asp:Label>
                                        <asp:Label ID="lblid" runat="server" Text='<%# Eval("Regd_ID") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="MARK">
                                    <ItemTemplate>
                                        <asp:Label ID="lblmark" runat="server" Text='<%# Eval("Mark") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Topics">
                                    <ItemTemplate>
                                        <asp:DataList ID="ddltopics" runat="server">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstudent" runat="server" Text='<%# Eval("Topic_Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="form-group" style="font-size: 15px; font-weight: bold;">
                        CUMULATIVE CLASS ANALYSIS
                    </div>
                    <div class="form-group text-center">
                        <asp:GridView ID="gvcumulative" runat="server" AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderText="Sl. No.">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SUB-CONCEPTS">
                                    <ItemTemplate>
                                        <asp:Label ID="lblname" runat="server" Text='<%# Eval("Topic_Name") %>'></asp:Label>
                                        <asp:Label ID="lblid" runat="server" Text='<%# Eval("Topic_ID") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="AVERAGE CLASS %">
                                    <ItemTemplate>
                                        <asp:Label ID="lblmark" runat="server" Text='<%# Eval("Mark_Percent") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="WEAK STUDENTS">
                                    <ItemTemplate>
                                        <asp:DataList ID="ddltopics" runat="server">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstudent" runat="server" Text='<%# Eval("Student_Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="REMARKS FOR TEACHERS">
                                    <ItemTemplate>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="form-group text-center" style="font-size: 13px; font-weight: bold;">
                        NOTE: Students scoring less than 35% in each sub-concept is considered as a Weak Sub-concept.
                    </div>
                </div>
            </div>
        
        </div>
    </form>
</body>
</html>
