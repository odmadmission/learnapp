﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DigiChamps.Reports
{
    public partial class Reports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SchoolCls scl = new SchoolCls();
            DigiChampsEntities DbContext = new DigiChampsEntities();
            string type = Request.QueryString["type"].ToString();
            switch (type)
            {
                case "School_Usage_Report":
                    DateTime f_Date = Convert.ToDateTime(Request.QueryString["f_Date"].ToString());
                    DateTime t_Date = Convert.ToDateTime(Request.QueryString["t_Date"].ToString());
                    int C_Id = Convert.ToInt32(Request.QueryString["C_Id"].ToString());
                    string S_Id = Request.QueryString["S_Id"].ToString();
                    Guid school_id = Guid.Parse(Request.QueryString["school_id"].ToString());
                    DataSet ds = scl.GetReportData(f_Date, t_Date, C_Id, S_Id, school_id);
                    // ds.WriteXml(@"D:\parismita\XML_FILES\Usage_Report.xml");


                    ReportDocument rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Reports/School/School_UsageReport.rpt"));
                    rd.SetDataSource(ds);
                    CrystalReportViewer1.ReportSource = rd;

                    TextObject txtObj = (TextObject)rd.ReportDefinition.Sections["Section1"].ReportObjects["Text1"];
                    txtObj.Text = "From :" + Convert.ToDateTime(f_Date).ToString("dd/MM/yyyy") + "   To :" + Convert.ToDateTime(t_Date).ToString("dd/MM/yyyy");

                    TextObject txtObj1 = (TextObject)rd.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                    txtObj1.Text = Convert.ToInt32(C_Id) == 0 ? "TOTAL USERS (ALL STUDENTS)" : "TOTAL USERS (" + DbContext.tbl_DC_Class.ToList().Where(a => a.Class_Id == Convert.ToInt32(C_Id)).FirstOrDefault().Class_Name + ")";

                    TextObject txtObj2 = (TextObject)rd.Subreports[0].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text1"];
                    txtObj2.Text = Convert.ToInt32(C_Id) == 0 ? "ACTIVITY TIME (ALL STUDENTS)" : "ACTIVITY TIME (" + DbContext.tbl_DC_Class.ToList().Where(a => a.Class_Id == Convert.ToInt32(C_Id)).FirstOrDefault().Class_Name + ")";

                    //ChartObject chartControl1=(ChartObject)rd.Subreports[1].ReportDefinition.Sections["]
                    //this.chartControl1.PrimaryXAxis.LabelRotate = true;
                    //this.chartControl1.PrimaryXAxis.LabelRotateAngle = 135;

                    ////Alignment for axis labels
                    //this.chartControl1.PrimaryXAxis.LabelAlignment = StringAlignment.Near;

                    System.IO.Stream m = null;
                    m = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    byte[] byteArray = null;
                    byteArray = new byte[m.Length];
                    m.Read(byteArray, 0, Convert.ToInt32(m.Length - 1));
                    rd.Close();
                    rd.Dispose();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(byteArray);

                    break;
                case "School_Usage_Report_Excel":

                    f_Date = Convert.ToDateTime(Request.QueryString["f_Date"].ToString());
                    t_Date = Convert.ToDateTime(Request.QueryString["t_Date"].ToString());
                    C_Id = Convert.ToInt32(Request.QueryString["C_Id"].ToString());
                    S_Id = Request.QueryString["S_Id"].ToString();
                    school_id = Guid.Parse(Request.QueryString["school_id"].ToString());
                    ds = scl.GetReportData(f_Date, t_Date, C_Id, S_Id, school_id);

                    // ds.WriteXml(@"D:\parismita\XML_FILES\Usage_Report.xml");
                    rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Reports/School/School_UsageReport.rpt"));
                    rd.SetDataSource(ds);
                    CrystalReportViewer1.ReportSource = rd;

                    txtObj = (TextObject)rd.ReportDefinition.Sections["Section1"].ReportObjects["Text1"];
                    txtObj.Text = "From :" + Convert.ToDateTime(f_Date).ToString("dd/MM/yyyy") + "   To :" + Convert.ToDateTime(t_Date).ToString("dd/MM/yyyy");

                    txtObj1 = (TextObject)rd.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                    txtObj1.Text = Convert.ToInt32(C_Id) == 0 ? "TOTAL USERS (ALL STUDENTS)" : "TOTAL USERS (" + DbContext.tbl_DC_Class.ToList().Where(a => a.Class_Id == Convert.ToInt32(C_Id)).FirstOrDefault().Class_Name + ")";

                    txtObj2 = (TextObject)rd.Subreports[0].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text1"];
                    txtObj2.Text = Convert.ToInt32(C_Id) == 0 ? "ACTIVITY TIME (ALL STUDENTS)" : "ACTIVITY TIME (" + DbContext.tbl_DC_Class.ToList().Where(a => a.Class_Id == Convert.ToInt32(C_Id)).FirstOrDefault().Class_Name + ")";


                    rd.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "Usage Report");
                    Response.End();

                    break;
                case "School_Test_Report":
                    C_Id = Convert.ToInt32(Request.QueryString["C_Id"].ToString());
                    S_Id = Request.QueryString["S_Id"].ToString();
                    school_id = Guid.Parse(Request.QueryString["school_id"].ToString());
                    int Sub_Id = Convert.ToInt32(Request.QueryString["Sub_Id"].ToString());
                    int Chap_Id = Convert.ToInt32(Request.QueryString["Chap_Id"].ToString());
                    int T_Id = Convert.ToInt32(Request.QueryString["T_Id"].ToString());
                    int B_Id = Convert.ToInt32(Request.QueryString["B_Id"].ToString());
                    ds = new DataSet();
                    ds = scl.GetSchoolTestReport(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id);

                    //   ds.WriteXml(@"D:\parismita\XML_FILES\Test_Report.xml");
                    rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Reports/School/School_Test_Report.rpt"));
                    rd.SetDataSource(ds);
                    CrystalReportViewer1.ReportSource = rd;
                    int s = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Max(a => a.Question_Nos).Value;
                    //int g = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Sum(a => a.Total_Correct_Ans).Value;
                    //int t = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Count();

                    string avg = DbContext.Database.SqlQuery<string>(@"SELECT [odm_lms].[dbo].[FN_DC_GET_AVERAGE_CLASS_PERCENTAGE](@Exam_ID,@Board_Id,@Class_Id,@Subject_Id,@Chapter_Id,@SchoolId,@SectionId)", new SqlParameter("@Exam_ID", T_Id), new SqlParameter("@Board_Id", B_Id), new SqlParameter("@Class_Id", C_Id), new SqlParameter("@Subject_Id", Sub_Id), new SqlParameter("@Chapter_Id", Chap_Id), new SqlParameter("@SchoolId", school_id.ToString()), new SqlParameter("@SectionId", S_Id)).FirstOrDefault();

                    txtObj = (TextObject)rd.Subreports[1].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text8"];
                    txtObj.Text = "FULL MARKS: " + s.ToString();

                    txtObj1 = (TextObject)rd.Subreports[1].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text11"];
                    txtObj1.Text = "CLASS AVERAGE PERCENTAGE: " + avg;

                    //txtObj2 = (TextObject)rd.Subreports[0].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text1"];
                    //txtObj2.Text = Convert.ToInt32(C_Id) == 0 ? "ACTIVITY TIME (ALL STUDENTS)" : "ACTIVITY TIME (" + DbContext.tbl_DC_Class.ToList().Where(a => a.Class_Id == Convert.ToInt32(C_Id)).FirstOrDefault().Class_Name + ")";


                    m = null;
                    m = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    byteArray = null;
                    byteArray = new byte[m.Length];
                    m.Read(byteArray, 0, Convert.ToInt32(m.Length - 1));
                    rd.Close();
                    rd.Dispose();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(byteArray);
                    break;
                case "School_Test_Report_Excel":
                    C_Id = Convert.ToInt32(Request.QueryString["C_Id"].ToString());
                    S_Id = Request.QueryString["S_Id"].ToString();
                    school_id = Guid.Parse(Request.QueryString["school_id"].ToString());
                    Sub_Id = Convert.ToInt32(Request.QueryString["Sub_Id"].ToString());
                    Chap_Id = Convert.ToInt32(Request.QueryString["Chap_Id"].ToString());
                    T_Id = Convert.ToInt32(Request.QueryString["T_Id"].ToString());
                    B_Id = Convert.ToInt32(Request.QueryString["B_Id"].ToString());
                    ds = new DataSet();
                    ds = scl.GetAllSchoolTestReport(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id);

                   //  ds.WriteXml(@"C:\Code\New folder\Digi\XML_FILES\All_Test_Report.xml");
                    rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Reports/School/School_All_Test_Report.rpt"));
                    rd.SetDataSource(ds);
                    CrystalReportViewer1.ReportSource = rd;
                    // s = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Max(a => a.Question_Nos).Value;
                    //int g = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Sum(a => a.Total_Correct_Ans).Value;
                    //int t = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Count();
                    //  avg = DbContext.Database.SqlQuery<string>(@"SELECT [odm_lms].[dbo].[FN_DC_GET_AVERAGE_CLASS_PERCENTAGE](@Exam_ID,@Board_Id,@Class_Id,@Subject_Id,@Chapter_Id,@SchoolId,@SectionId)", new SqlParameter("@Exam_ID", T_Id), new SqlParameter("@Board_Id", B_Id), new SqlParameter("@Class_Id", C_Id), new SqlParameter("@Subject_Id", Sub_Id), new SqlParameter("@Chapter_Id", Chap_Id), new SqlParameter("@SchoolId", school_id.ToString()), new SqlParameter("@SectionId", S_Id)).FirstOrDefault();

                    // avg = Convert.ToDecimal((g / (s * t)) * 100) + " %";

                    //txtObj = (TextObject)rd.Subreports[1].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text8"];
                    //txtObj.Text ="FULL MARKS: "+s.ToString();

                    //txtObj1 = (TextObject)rd.Subreports[1].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text11"];
                    //txtObj1.Text = "CLASS AVERAGE PERCENTAGE: " + avg;
                    m = null;
                    m = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    byteArray = null;
                    byteArray = new byte[m.Length];
                    m.Read(byteArray, 0, Convert.ToInt32(m.Length - 1));
                    rd.Close();
                    rd.Dispose();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(byteArray);

                    break;
                case "School_Marksheet_Report":                    
                    B_Id = Convert.ToInt32(Request.QueryString["Id"].ToString());
                    
                    var marks = DbContext.Tbl_Student_Marksheet.Where(a => a.Is_Active == true && a.StudentmarksheetId==B_Id).FirstOrDefault();
                    var marksdet = DbContext.Tbl_StudentMarksheetDet.Where(a => a.Is_Active == true && a.StudentmarksheetId == B_Id).ToList();
                    List<marksheetreport> marksheetreports = new List<marksheetreport>();
                    marksheetreport marksheet = new marksheetreport();
                    marksheet.ClassName = marks.ClassName;
                    marksheet.OnlineExamTypeName = DbContext.OnlineExamTypes.Where(a=>a.OnlineExamTypeId== marks.OnlineExamTypeId).FirstOrDefault().OnlineExamTypeName;
                    marksheet.Percentage_Objective = marks.Percentage_Objective;
                    marksheet.Percentage_Subjective = marks.Percentage_Subjective;
                    marksheet.RegdId = marks.RegdId;
                    marksheet.Roll_No = marks.Roll_No;
                    marksheet.School_No = marks.School_No;
                    marksheet.section = marks.section;
                    marksheet.Session = marks.Session;
                    marksheet.StudentmarksheetId = marks.StudentmarksheetId;
                    marksheet.StudentName = marks.StudentName;
                    marksheet.TotalObjectiveScore =marks.Total_Objective==null?(marks.typeId==4?500: marksdet.Count()*100).ToString(): marks.Total_Objective;
                    marksheet.TotalSubjectiveScore =marks.Total_Subjective==null?(marks.typeId == 4 ? 500 : marksdet.Count()*100).ToString():marks.Total_Subjective;
                    marksheet.Total_Mark_Objective = marks.Total_Mark_Objective;
                    marksheet.Total_Mark_Subjective = marks.Total_Mark_Subjective;
                    marksheetreports.Add(marksheet);


                    ds = new DataSet("Marksheet_Report");
                    
                    DataTable dt2 = new DataTable();
                    dt2 =scl.ConvertToDataTable(marksheetreports);
                    dt2.TableName = "Basic_Details";
                    ds.Tables.Add(dt2.Copy());

                    DataTable dt3 = new DataTable();
                    dt3 =scl.ConvertToDataTable(marksdet);
                    dt3.TableName = "Marksheet_Details";
                    ds.Tables.Add(dt3.Copy());
                 //   ds.WriteXml(@"F:\xmlfile\Marksheet_Report.xml");
                    rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Reports/learnapp/Marksheet_Report.rpt"));
                    rd.SetDataSource(ds);
                    CrystalReportViewer1.ReportSource = rd;

                    Response.Buffer = false;
                    // Clear the response content and headers
                    Response.ClearContent();
                    Response.ClearHeaders();
                    try
                    {
                        // Export the Report to Response stream in PDF format and file name Customers
                        rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "Marksheet_Report");
                        rd.Close();
                        rd.Dispose();
                        // There are other format options available such as Word, Excel, CVS, and HTML in the ExportFormatType Enum given by crystal reports
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        rd.Close();
                        rd.Dispose();
                    }


                    //m = null;
                    //m = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    //byteArray = null;
                    //byteArray = new byte[m.Length];
                    //m.Read(byteArray, 0, Convert.ToInt32(m.Length - 1));
                    //rd.Close();
                    //rd.Dispose();
                    //Response.ClearContent();
                    //Response.ClearHeaders();
                    //Response.Buffer = true;
                    //Response.ContentType = "application/pdf";
                    //Response.BinaryWrite(byteArray);

                    break;

                case "School_Half_Yearly_Marksheet_Report":
                    B_Id = Convert.ToInt32(Request.QueryString["Id"].ToString());

                    var hmarks = DbContext.Tbl_Student_Halfyearly_Marksheet.Where(a => a.Is_Active == true && a.StudentmarksheetId == B_Id).FirstOrDefault();
                    var hmarksdet = DbContext.Tbl_Halfyearly_StudentMarksheetDet.Where(a => a.Is_Active == true && a.StudentmarksheetId == B_Id && a.SubjectiveScore!="NA").ToList();
                    List<marksheetreport> hmarksheetreports = new List<marksheetreport>();
                    marksheetreport hmarksheet = new marksheetreport();
                    hmarksheet.ClassName = hmarks.ClassName.ToUpper();
                    hmarksheet.OnlineExamTypeName = hmarks.OnlineExamType;
                    hmarksheet.Percentage_Objective = hmarks.Percentage_Objective.ToString();
                    hmarksheet.Percentage_Subjective = hmarks.Percentage_Subjective.ToString();
                    hmarksheet.RegdId = hmarks.RegdId;
                    hmarksheet.Roll_No = hmarks.Remarks;
                    hmarksheet.School_No = hmarks.School_No;
                    hmarksheet.section = hmarks.section.ToUpper();
                    hmarksheet.Session = hmarks.Session;
                    hmarksheet.StudentmarksheetId = hmarks.StudentmarksheetId;
                    hmarksheet.StudentName = hmarks.StudentName.ToUpper();
                    hmarksheet.TotalObjectiveScore = (hmarksdet.Count() * 100).ToString();
                    hmarksheet.TotalSubjectiveScore =Convert.ToInt32(hmarks.Total_Mark_Objective).ToString();
                    hmarksheet.Total_Mark_Objective = hmarks.Total_Mark_Objective.ToString();
                    hmarksheet.Total_Mark_Subjective = hmarks.Total_Mark_Subjective.ToString();
                    hmarksheetreports.Add(hmarksheet);


                    ds = new DataSet("Marksheet_Report");

                    dt2 = new DataTable();
                    dt2 = scl.ConvertToDataTable(hmarksheetreports);
                    dt2.TableName = "Basic_Details";
                    ds.Tables.Add(dt2.Copy());

                    dt3 = new DataTable();
                    dt3 = scl.ConvertToDataTable(hmarksdet);
                    dt3.TableName = "Marksheet_Details";
                    ds.Tables.Add(dt3.Copy());
                    //ds.WriteXml(@"E:\XML\Marksheet_Report.xml");
                    rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Reports/learnapp/HalfyearlyReport.rpt"));
                    rd.SetDataSource(ds);
                    CrystalReportViewer1.ReportSource = rd;

                    Response.Buffer = false;
                    // Clear the response content and headers
                    Response.ClearContent();
                    Response.ClearHeaders();
                    try
                    {
                        // Export the Report to Response stream in PDF format and file name Customers
                        rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "Marksheet_Report");
                        rd.Close();
                        rd.Dispose();
                        // There are other format options available such as Word, Excel, CVS, and HTML in the ExportFormatType Enum given by crystal reports
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        rd.Close();
                        rd.Dispose();
                    }
                    break;

            

                case "AcademicSchool_Marksheet_Report":
                    B_Id = Convert.ToInt32(Request.QueryString["Id"].ToString());

                    var marks1 = DbContext.Tbl_Student_Marksheet.Where(a => a.Is_Active == true && a.StudentmarksheetId == B_Id).FirstOrDefault();
                    var marksdet1 = DbContext.Tbl_StudentMarksheetDet.Where(a => a.Is_Active == true && a.StudentmarksheetId == B_Id).ToList();
                    List<marksheetreport> marksheetreports1 = new List<marksheetreport>();
                    marksheetreport marksheet1 = new marksheetreport();
                    marksheet1.ClassName = marks1.ClassName;
                    marksheet1.OnlineExamTypeName = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == marks1.OnlineExamTypeId).FirstOrDefault().OnlineExamTypeName;
                    marksheet1.Percentage_Objective = marks1.Percentage_Objective;
                    marksheet1.Percentage_Subjective = marks1.Percentage_Subjective;
                    marksheet1.RegdId = marks1.RegdId;
                    marksheet1.Roll_No = marks1.Roll_No;
                    marksheet1.School_No = marks1.School_No;
                    marksheet1.section = marks1.section;
                    marksheet1.Session = marks1.Session;
                    marksheet1.StudentmarksheetId = marks1.StudentmarksheetId;
                    marksheet1.StudentName = marks1.StudentName;
                    marksheet1.TotalObjectiveScore = marks1.typeId == 4 ? "500" : (marksdet1.Count() * 100).ToString();
                    marksheet1.TotalSubjectiveScore = marks1.typeId == 4 ? "500" : (marksdet1.Count() * 100).ToString();
                    marksheet1.Total_Mark_Objective = marks1.Total_Mark_Objective;
                    marksheet1.Total_Mark_Subjective = marks1.Total_Mark_Subjective;
                    marksheetreports1.Add(marksheet1);
                    ds = new DataSet("StudentAcademicMarksheet_Rep");
                    DataTable dt4 = new DataTable();
                    dt4 = scl.ConvertToDataTable(marksheetreports1);
                    dt4.TableName = "Basic_Details";
                    ds.Tables.Add(dt4.Copy());

                    DataTable dt5 = new DataTable();
                    dt5 = scl.ConvertToDataTable(marksdet1);
                    dt5.TableName = "Marksheet_Details";
                    ds.Tables.Add(dt5.Copy());
                    //   ds.WriteXml(@"F:\xmlfile\Marksheet_Report.xml");
                    rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Reports/learnapp/StudentAcademicMarksheet_Rep.rpt"));
                    rd.SetDataSource(ds);
                    CrystalReportViewer1.ReportSource = rd;

                    Response.Buffer = false;
                    // Clear the response content and headers
                    Response.ClearContent();
                    Response.ClearHeaders();
                    try
                    {
                        // Export the Report to Response stream in PDF format and file name Customers
                        rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "StudentAcademicMarksheet_Rep");
                        rd.Close();
                        rd.Dispose();
                        // There are other format options available such as Word, Excel, CVS, and HTML in the ExportFormatType Enum given by crystal reports
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        rd.Close();
                        rd.Dispose();
                    }
                    break;
                case "AllAcademicSchool_Marksheet_Report":
                    B_Id = Convert.ToInt32(Request.QueryString["Id"].ToString());

                    var marks2 = DbContext.Tbl_Student_Marksheet.Where(a => a.Is_Active == true && a.StudentmarksheetId == B_Id).FirstOrDefault();
                    var marksdet2 = DbContext.Tbl_StudentMarksheetDet.Where(a => a.Is_Active == true && a.StudentmarksheetId == B_Id).ToList();
                    List<marksheetreport> marksheetreports2 = new List<marksheetreport>();
                    marksheetreport marksheet2 = new marksheetreport();
                    marksheet2.ClassName = marks2.ClassName;
                    marksheet2.OnlineExamTypeName = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == marks2.OnlineExamTypeId).FirstOrDefault().OnlineExamTypeName;
                    marksheet2.Percentage_Objective = marks2.Percentage_Objective;
                    marksheet2.Percentage_Subjective = marks2.Percentage_Subjective;
                    marksheet2.RegdId = marks2.RegdId;
                    marksheet2.Roll_No = marks2.Roll_No;
                    marksheet2.School_No = marks2.School_No;
                    marksheet2.section = marks2.section;
                    marksheet2.Session = marks2.Session;
                    marksheet2.StudentmarksheetId = marks2.StudentmarksheetId;
                    marksheet2.StudentName = marks2.StudentName;
                    marksheet2.TotalObjectiveScore = marks2.typeId == 4 ? "500" : (marksdet2.Count() * 100).ToString();
                    marksheet2.TotalSubjectiveScore = marks2.typeId == 4 ? "500" : (marksdet2.Count() * 100).ToString();
                    marksheet2.Total_Mark_Objective = marks2.Total_Mark_Objective;
                    marksheet2.Total_Mark_Subjective = marks2.Total_Mark_Subjective;
                    marksheetreports2.Add(marksheet2);
                    ds = new DataSet("AllAcademicMarksheet_Rep");
                    DataTable dt6 = new DataTable();
                    dt6 = scl.ConvertToDataTable(marksheetreports2);
                    dt6.TableName = "Basic_Details";
                    ds.Tables.Add(dt6.Copy());

                    DataTable dt7 = new DataTable();
                    dt7 = scl.ConvertToDataTable(marksdet2);
                    dt7.TableName = "Marksheet_Details";
                    ds.Tables.Add(dt7.Copy());
                    //   ds.WriteXml(@"F:\xmlfile\Marksheet_Report.xml");
                    rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Reports/learnapp/AllAcademicMarksheet_Rep.rpt"));
                    rd.SetDataSource(ds);
                    CrystalReportViewer1.ReportSource = rd;

                    Response.Buffer = false;
                    // Clear the response content and headers
                    Response.ClearContent();
                    Response.ClearHeaders();
                    try
                    {
                        // Export the Report to Response stream in PDF format and file name Customers
                        rd.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "AllAcademicMarksheet_Rep");
                        rd.Close();
                        rd.Dispose();
                        // There are other format options available such as Word, Excel, CVS, and HTML in the ExportFormatType Enum given by crystal reports
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        rd.Close();
                        rd.Dispose();
                    }
                    break;
            }
        }
        public class marksheetreport
        {
            public long StudentmarksheetId { get; set; }
            public string OnlineExamTypeName { get; set; }
            public Nullable<int> RegdId { get; set; }
            public string Session { get; set; }
            public string Total_Mark_Subjective { get; set; }
            public string Total_Mark_Objective { get; set; }
            public string Percentage_Subjective { get; set; }
            public string Percentage_Objective { get; set; }           
            public string Roll_No { get; set; }
            public string School_No { get; set; }
            public string StudentName { get; set; }
            public string ClassName { get; set; }
            public string section { get; set; }
            public string TotalSubjectiveScore { get; set; }
            public string TotalObjectiveScore { get; set; }
        }
     
    }
}