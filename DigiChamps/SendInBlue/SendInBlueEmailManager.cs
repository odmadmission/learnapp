﻿
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DigiChamps.SendInBlue
{
    public class SendInBlueEmailManager
    {
        private static string SEND_IN_BLUE_API_KEY = "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";
        private static string FromName = "ODM EDUCATIONAL GROUP";
        private static string HOST = "https://api.sendinblue.com/v3/smtp/email";
        private static string FromEmail = "tracq@odmegroup.org";


        private string SendMail(List<SendInBlueReceiverSender> receivers, string html, string subject)
        {

            try
            {
                SendInBlueRequestBody body = new SendInBlueRequestBody(html, receivers);
                SendInBlueReceiverSender sender = new SendInBlueReceiverSender();
                sender.name = FromName;
                sender.email = FromEmail;

                SendInBlueReceiverSender replyTo = new SendInBlueReceiverSender();
                replyTo.name = FromName;
                replyTo.email = FromEmail;

                body.replyTo = replyTo;
                body.sender = sender;
                body.subject = subject;

                var client = new RestClient(HOST);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("api-key", SEND_IN_BLUE_API_KEY);
                request.AddJsonBody(body);
                IRestResponse response = client.Execute(request);

                return response.StatusCode.ToString();

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void SendExammail
            (
             string student, string classname,
            string url,
            string receiver,string path
            
            )
        {
            SendInBlueReceiverSender oner = new SendInBlueReceiverSender();
            oner.name = student;
            oner.email = receiver;
            List<SendInBlueReceiverSender> r = new List<SendInBlueReceiverSender>();
            r.Add(oner);

            string body = string.Empty;
            using (StreamReader reader = new StreamReader(path))
            {

                body = reader.ReadToEnd().Replace("{{Student_Name}}", student).Replace("{{Class}}", classname).Replace("{{href}}", url);

            }
           
            Thread t1 = new Thread(delegate ()
            {
                SendMail(r, body, "Online Test Result | ODM Public School");

            });
            t1.Start();
           
        }
      



    }


}
