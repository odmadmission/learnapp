﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class DictionaryController : ApiController
    {
        DigiChampsEntities db = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public class ErrorResult
        {
            public ErrorResponse Error { get; set; }
        }

        public class ErrorResponse
        {
            public string Message { get; set; }
        }

        public class SuccessResponse
        {
            public string Message { get; set; }
        }
        public class SuccessResult
        {
            public List<Dictionary> BookMarks { get; set; }
        }
        public class Dictionary
        {
            public int? Dictionary_ID { get; set; }
            public string Dictionary_WordName { get; set; }
            public string Dictionary_Images { get; set; }
            public string Dictionary_Path { get; set; }
            public string Dictionary_Description { get; set; }

            public string DictionaryOnline { get; set; }
            public string DictionaryBeta { get; set; }
        }
        public class Dictionary_List
        {
            public List<Dictionary> list { get; set; }
        }
        public class success_Dictionary
        {
            public Dictionary_List Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }
        public class DictionaryKeyword
        {
            public string word { get; set; }
        }

        [HttpPost]
        public HttpResponseMessage GetDictionary([FromBody]DictionaryKeyword dic)
        {
            string word = dic.word;

            var getDictionary = db.tbl_DC_Dictionary.Where(x => x.Is_Active == true && x.Is_Delete == false && x.Dictionary_WordName.StartsWith(word)).ToList();
            try
            {
                var obj = new success_Dictionary
                {
                    Success = new Dictionary_List
                    {
                        list = (from c in getDictionary
                                select new Dictionary
                                {
                                    Dictionary_ID = c.Dictionary_ID,
                                    Dictionary_WordName = c.Dictionary_WordName,
                                    Dictionary_Images = c.Dictionary_Images,
                                    //Dictionary_Images = c.Dictionary_Images == null ? "" : "http://beta.thedigichamps.com/Images/Dictionary/" + c.Dictionary_Images,
                                    DictionaryOnline = "https://learn.odmps.org/Images/Dictionary/" + c.Dictionary_Images + "",
                                    DictionaryBeta = "http://beta.thedigichamps.com/Images/Dictionary/" + c.Dictionary_Images + "",
                                    Dictionary_Path = c.Dictionary_Path,
                                    Dictionary_Description = c.Dictionary_Description
                                }).ToList(),
                    }
                };

                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            catch (Exception)
            {
                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }
    }
}
