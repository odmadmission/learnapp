﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DigiChamps.Controllers
{
    public class MentorController : ApiController
    {

        DateTime today = DigiChampsModel.datetoserver();
        DigiChampsEntities DbContext = new DigiChampsEntities();
        public class TaskData
        {
            public int taskId { get; set; }
            public string taskHeder { get; set; }
            public string taskDetails { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
        }

        public class TeacherDetails
        {
            public int teacherId { get; set; }
            public string teacherName { get; set; }
            public string teacherProfilePicture { get; set; }
            public string teacherEmail { get; set; }
            public string teachermobile { get; set; }
        }


        public class GoalData
        {
            public string golaText { get; set; }
            public bool Is_Acived { get; set; }
        }
        public class TaskList
        {
            public List<TaskData> taskDataList { get; set; }
        }
        public class ActiveTaskList : TaskList
        {

        }
        public class OverDueTaskList : TaskList
        {

        }
        public class CompletedTaskList : TaskList
        {

        }


        public class SuccessTask
        {
            public SuccessresultTask SuccessresultTask { get; set; }
        }
        public class SuccessresultTask
        {
            public ActiveTaskList ActiveTaskList { get; set; }
            public CompletedTaskList CompletedTaskList { get; set; }
            public OverDueTaskList OverDueTaskList { get; set; }
            public TeacherDetails TeacherDetails { get; set; }
            public MentortaskGoal GoalDetails { get; set; }
            public string studentName { get; set; }
            public decimal? rating { get; set; }
            public string studentimage { get; set; }
            public string Remark { get; set; }

        }
        public class MentortaskGoal
        {
            public string message { get; set; }
            public bool? Is_Achieved_ { get; set; }
        }
        //var obj2 = new Digichamps.ErrorResult
        //{
        //    error = new Digichamps.ErrorResponse
        //    {
        //        Message = "OTP can't send more than 3  times"
        //    }
        //};
        [HttpGet]
        public HttpResponseMessage GetStudentTask(int? id)
        {
            try
            {
                var getTeacher = GetTeacherDetailsbyStudent(id);
                int teacherID = 0;
                int count = 0;
                if (getTeacher != null)
                {
                     teacherID = Convert.ToInt32(getTeacher.Teacher_ID);
                    //GetTeacherDetails(teacherID);
                    // int teacherID = Convert.ToInt32(getTeacher.Teacher_ID);
                     var  getTaskDetails = DbContext.tbl_DC_MentorTask.Where(x => x.Student_ID == id
                     && x.Teacher_ID == teacherID).ToList();

                    if(getTaskDetails!=null)
                    {
                        count = getTaskDetails.Count;
                    }
                }

                tbl_DC_GoalStatus gs = GetgoalDetails(id);

                if (count > 0)
                {
                    
                    var getTaskDetails = DbContext.tbl_DC_MentorTask.Where(x => x.Student_ID == id
                    && x.Teacher_ID == teacherID).ToList();
                    var getTeacherDeatils = GetTeacherDetails(teacherID);
                    MakeOverDue(id);
                    var obj = new SuccessTask
                    {
                        SuccessresultTask = new SuccessresultTask
                        {
                            ActiveTaskList = new ActiveTaskList
                            {
                                taskDataList = (from at in getTaskDetails.Where(x => x.Status == "N").ToList()
                                                select new TaskData
                                                {
                                                    taskId = at.MentorTask_ID,
                                                    taskHeder = at.Task_Heading,
                                                    taskDetails = at.Task_Details,
                                                    StartDate = at.Start_Date.Value.ToString("MM/dd/yyyy"),
                                                    EndDate = at.End_Date.Value.ToString("MM/dd/yyyy"),
                                                }).ToList(),
                            },
                            CompletedTaskList = new CompletedTaskList
                            {
                                taskDataList = (from at in getTaskDetails.Where(x => x.Status == "C").ToList()
                                                select new TaskData
                                                {
                                                    taskId = at.MentorTask_ID,
                                                    taskHeder = at.Task_Heading,
                                                    taskDetails = at.Task_Details,
                                                    StartDate = at.Start_Date.Value.ToString("MM/dd/yyyy"),
                                                    EndDate = at.End_Date.Value.ToString("MM/dd/yyyy"),
                                                }).ToList(),
                            },

                            OverDueTaskList = new OverDueTaskList
                            {
                                taskDataList = (from at in getTaskDetails.Where(x => x.Status == "O").ToList()
                                                select new TaskData
                                                {
                                                    taskId = at.MentorTask_ID,
                                                    taskHeder = at.Task_Heading,
                                                    taskDetails = at.Task_Details,
                                                    StartDate = at.Start_Date.Value.ToString("MM/dd/yyyy"),
                                                    EndDate = at.End_Date.Value.ToString("MM/dd/yyyy"),
                                                }).ToList(),
                            },
                            TeacherDetails = new TeacherDetails
                            {
                                teacherId = teacherID,
                                teacherName = getTeacherDeatils.Teacher_Name,
                                teacherEmail = getTeacherDeatils.Email_ID,
                                teachermobile = getTeacherDeatils.Mobile,
                                teacherProfilePicture = "http://beta.thedigichamps.com/Images/Teacherprofile/" + getTeacherDeatils.Image
                            },
                            GoalDetails = new MentortaskGoal
                            {
                                message = gs==null?null:gs.GoalText,
                                Is_Achieved_=gs==null?null:gs.Is_Achieved

                            },
                            studentName = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == id).
                            Select(x => x.Customer_Name).FirstOrDefault(),
                            studentimage = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == id).
                            Select(x => "http://learn.odmps.org/Images/Profile/" + x.Image).FirstOrDefault(),
                            rating = DbContext.tbl_DC_MentorMaster.Where(x=>x.Student_ID == id).
                            Select(x=>x.Rating).FirstOrDefault(),
                            Remark = DbContext.tbl_DC_MentorMaster.Where(x=>x.Student_ID == id).
                            Select(x=>x.Remark).FirstOrDefault(),
                        }
                    };

                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
                else
                {

                   
                    var getTeacherDeatils = GetTeacherDetails(teacherID);
                    TeacherDetails td = null;
                    if (getTeacherDeatils != null && !getTeacherDeatils.Equals("null"))
                    {
                        td = new TeacherDetails
                             {
                                 teacherId = teacherID,
                                 teacherName = getTeacherDeatils.Teacher_Name,
                                 teacherEmail = getTeacherDeatils.Email_ID,
                                 teachermobile = getTeacherDeatils.Mobile,
                                 teacherProfilePicture = "http://beta.thedigichamps.com/Images/Teacherprofile/" + getTeacherDeatils.Image
                             };
                    }
                    MakeOverDue(id);
                    var obj = new SuccessTask
                    {
                        SuccessresultTask = new SuccessresultTask
                        {
                            ActiveTaskList = new ActiveTaskList
                            {
                                //taskDataList = (from at in getTaskDetails.Where(x => x.Status == "N").ToList()
                                //                select new TaskData
                                //                {
                                //                    taskId = at.MentorTask_ID,
                                //                    taskHeder = at.Task_Heading,
                                //                    taskDetails = at.Task_Details,
                                //                    StartDate = at.Start_Date.Value.ToString("MM/dd/yyyy"),
                                //                    EndDate = at.End_Date.Value.ToString("MM/dd/yyyy"),
                                //                }).ToList(),
                            },
                            CompletedTaskList = new CompletedTaskList
                            {
                                //taskDataList = (from at in getTaskDetails.Where(x => x.Status == "C").ToList()
                                //                select new TaskData
                                //                {
                                //                    taskId = at.MentorTask_ID,
                                //                    taskHeder = at.Task_Heading,
                                //                    taskDetails = at.Task_Details,
                                //                    StartDate = at.Start_Date.Value.ToString("MM/dd/yyyy"),
                                //                    EndDate = at.End_Date.Value.ToString("MM/dd/yyyy"),
                                //                }).ToList(),
                            },

                            OverDueTaskList = new OverDueTaskList
                            {
                                //taskDataList = (from at in getTaskDetails.Where(x => x.Status == "O").ToList()
                                //                select new TaskData
                                //                {
                                //                    taskId = at.MentorTask_ID,
                                //                    taskHeder = at.Task_Heading,
                                //                    taskDetails = at.Task_Details,
                                //                    StartDate = at.Start_Date.Value.ToString("MM/dd/yyyy"),
                                //                    EndDate = at.End_Date.Value.ToString("MM/dd/yyyy"),
                                //                }).ToList(),
                            },

                            TeacherDetails = td,
                            GoalDetails = new MentortaskGoal
                            {
                                message = gs == null ? null : gs.GoalText,
                                Is_Achieved_ = gs == null ? null : gs.Is_Achieved

                            }
                            ,
                            studentName = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == id).Select(x => x.Customer_Name).FirstOrDefault(),
                            studentimage = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == id).Select(x => "http://learn.odmps.org/Images/Profile/" + x.Image).FirstOrDefault(),
                            rating = DbContext.tbl_DC_MentorMaster.Where(x=>x.Student_ID == id).Select(x=>x.Rating).FirstOrDefault(),
                            Remark = DbContext.tbl_DC_MentorMaster.Where(x=>x.Student_ID == id).Select(x=>x.Remark).FirstOrDefault(),
                

                        }
                    };

                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                    //var obj2 = new Digichamps.ErrorResult
                    //{
                    //    error = new Digichamps.ErrorResponse
                    //    {
                    //        Message = "No Mentor Assigned."
                    //    }
                    //};
                    //return Request.CreateResponse(HttpStatusCode.OK, obj2);
                }
            }
            catch (Exception ex)
            {

                throw ex;
                //var obj2 = new Digichamps.ErrorResult
                //{
                //    error = new Digichamps.ErrorResponse
                //    {
                //        Message = "Something went wrong."
                //    }
                //};
                //return Request.CreateResponse(HttpStatusCode.OK, obj2);

            }

        }

        public tbl_DC_Teacher GetTeacherDetails(int TeacherId)
        {
            return DbContext.tbl_DC_Teacher.Where(x => x.Teach_ID == TeacherId).FirstOrDefault();
        }
        public tbl_DC_MentorMaster GetTeacherDetailsbyStudent(int? studentid)
        {
            return DbContext.tbl_DC_MentorMaster.Where(x => x.Student_ID == studentid).FirstOrDefault();
        }

        public void MakeOverDue(int? studentid)
        {
            var getstatusoftask = DbContext.tbl_DC_MentorTask.Where(x => x.Student_ID == studentid).ToList();
            foreach (var v in getstatusoftask)
            {
                if (v.End_Date < today && v.Status != "C")
                {
                    v.Status = "O";
                    DbContext.SaveChanges();
                }
            }
        }

        public tbl_DC_GoalStatus GetgoalDetails(int? studentID)
        {
            return DbContext.tbl_DC_GoalStatus.Where(x => x.StudentID == studentID).FirstOrDefault();
        }
    }
}
