﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClosedXML.Excel;
using DigiChamps.Models;
namespace DigiChamps.Controllers
{
    public class OnlineExamDetController : Controller
    {
        //
        // GET: /OnlineExamDet/
        DigiChampsEntities Dbcontext = new DigiChampsEntities();
        public ActionResult Index()
        {
            return View();
        }
        #region Exam type
        public ActionResult Examtype()
        {
            ViewBag.examttypelist = Dbcontext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList().OrderByDescending(a => a.InsertedOn);
            return View();
        }
        public ActionResult removeExamtype(int id)
        {
            var ob = Dbcontext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == id).ToList();
            if (ob.Count > 0)
            {
                OnlineExamType obj = Dbcontext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == id).FirstOrDefault();
                obj.Is_Active = false;
                obj.ModifiedOn = DateTime.Now;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("Examtype");
        }
        public ActionResult createoreditExamtype(int? id)
        {
            if (id == null)
            {
                Session["rid"] = null;
                ViewBag.name = "";
            }
            else
            {
                Session["rid"] = id;
                OnlineExamType ob = Dbcontext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == id).FirstOrDefault();
                ViewBag.name = ob.OnlineExamTypeName;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdateExamtype(string name)
        {
            if (Session["rid"] == null)
            {
                OnlineExamType ob = new OnlineExamType();
                ob.OnlineExamTypeName = name;
                ob.InsertedOn = DateTime.Now;
                ob.ModifiedOn = DateTime.Now;
                ob.InsertedId = 1;
                ob.ModifiedId = 1;
                ob.Is_Active = true;
                Dbcontext.OnlineExamTypes.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["rid"]));
                OnlineExamType ob = Dbcontext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == id).FirstOrDefault();
                ob.OnlineExamTypeName = name;
                ob.ModifiedId = 1;
                ob.ModifiedOn = DateTime.Now;
                ob.Is_Active = true;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("Examtype");
        }
        #endregion

        #region Exam Date
        public ActionResult Examdate()
        {
            ViewBag.examtdatelist = Dbcontext.OnlineExamDates.Where(a => a.Is_Active == true).ToList().OrderByDescending(a => a.OnlineExamDate1);
            return View();
        }
        public ActionResult removeExamdate(int id)
        {
            var ob = Dbcontext.OnlineExamDates.Where(a => a.OnlineExamDateId == id).ToList();
            if (ob.Count > 0)
            {
                OnlineExamDate obj = Dbcontext.OnlineExamDates.Where(a => a.OnlineExamDateId == id).FirstOrDefault();
                obj.Is_Active = false;
                obj.ModifiedOn = DateTime.Now;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("Examdate");
        }
        public ActionResult createoreditExamdate(int? id)
        {
            if (id == null)
            {
                Session["rid"] = null;
                ViewBag.name = "";
            }
            else
            {
                Session["rid"] = id;
                OnlineExamDate ob = Dbcontext.OnlineExamDates.Where(a => a.OnlineExamDateId == id).FirstOrDefault();
                ViewBag.name = ob.OnlineExamDate1.Value.ToString("yyyy-MM-dd");
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdateExamdate(DateTime name)
        {
            if (Session["rid"] == null)
            {
                var datename = Dbcontext.OnlineExamDates.Where(a => a.OnlineExamDate1 == name && a.Is_Active==true).FirstOrDefault();
               
                if(datename==null)
                {
                    OnlineExamDate ob = new OnlineExamDate();
                    ob.OnlineExamDate1 = name;
                    ob.OnlineExamDateName = name.ToString("dd MMM yyyy");
                    ob.InsertedOn = DateTime.Now;
                    ob.ModifiedOn = DateTime.Now;
                    ob.InsertedId = 1;
                    ob.ModifiedId = 1;
                    ob.Is_Active = true;
                    Dbcontext.OnlineExamDates.Add(ob);
                    Dbcontext.SaveChanges();
                    TempData["SuccessMessage"] = "Saved Successfully";
                    return RedirectToAction("Examdate");
                }
                else
                {
                   
                    TempData["ErrorMessage"] = "Date Already Present.";
                    return RedirectToAction("createoreditExamdate");
                }
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["rid"]));
                var datename = Dbcontext.OnlineExamDates.Where(a => a.OnlineExamDate1 == name && a.Is_Active == true && a.OnlineExamDateId!= id).FirstOrDefault();
                if (datename == null)
                {
                   
                    OnlineExamDate ob = Dbcontext.OnlineExamDates.Where(a => a.OnlineExamDateId == id).FirstOrDefault();
                    ob.OnlineExamDate1 = name;
                    ob.OnlineExamDateName = name.ToString("dd MMM yyyy");
                    ob.ModifiedId = 1;
                    ob.ModifiedOn = DateTime.Now;
                    ob.Is_Active = true;
                    Dbcontext.SaveChanges();
                    TempData["SuccessMessage"] = "Updated Successfully";
                    return RedirectToAction("Examdate");
                }
                {

                    TempData["ErrorMessage"] = "Date Already Present.";
                    return RedirectToAction("createoreditExamdate", "OnlineExamDet", new { id = id});
                }
            }
           
        }
        #endregion
        #region Exam Time
        public class OnlineExamTimecls
        {
            public long OnlineExamTimeId { get; set; }
            public Nullable<long> OnlineExamDateId { get; set; }
            public String OnlineExamDateName { get; set; }
            public Nullable<System.TimeSpan> Start_Time { get; set; }
            public Nullable<System.TimeSpan> End_Time { get; set; }
            public string Start_Time_Name { get; set; }
            public string End_Time_Name { get; set; }
            public Nullable<bool> Is_Active { get; set; }
            public Nullable<int> InsertedId { get; set; }
            public Nullable<int> ModifiedId { get; set; }
            public Nullable<System.DateTime> InsertedOn { get; set; }
            public Nullable<System.DateTime> ModifiedOn { get; set; }
        }
        public ActionResult Examtime()
        {
            ViewBag.Examtimelist = (from a in Dbcontext.OnlineExamTimes.Where(a => a.Is_Active == true).ToList()
                                    join b in Dbcontext.OnlineExamDates.Where(a => a.Is_Active == true).ToList() on a.OnlineExamDateId equals b.OnlineExamDateId
                                    select new OnlineExamTimecls
                                    {
                                        End_Time = a.End_Time,
                                        End_Time_Name = a.End_Time_Name,
                                        InsertedId = a.InsertedId,
                                        InsertedOn = a.InsertedOn,
                                        Is_Active = a.Is_Active,
                                        ModifiedId = a.ModifiedId,
                                        ModifiedOn = a.ModifiedOn,
                                        OnlineExamDateId = a.OnlineExamDateId,
                                        OnlineExamDateName = b.OnlineExamDateName,
                                        OnlineExamTimeId = a.OnlineExamTimeId,
                                        Start_Time = a.Start_Time,
                                        Start_Time_Name = a.Start_Time_Name
                                    }).OrderByDescending(a => a.InsertedOn);
            return View();
        }
        public ActionResult removeExamtime(int id)
        {
            var ob = Dbcontext.OnlineExamTimes.Where(a => a.OnlineExamTimeId == id).ToList();
            if (ob.Count > 0)
            {
                OnlineExamTime obj = Dbcontext.OnlineExamTimes.Where(a => a.OnlineExamTimeId == id).FirstOrDefault();
                obj.Is_Active = false;
                obj.ModifiedOn = DateTime.Now;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("Examtime");
        }
        public ActionResult createoreditExamtime(int? id)
        {
            ViewBag.onlinedates = Dbcontext.OnlineExamDates.Where(a => a.Is_Active == true).ToList().OrderByDescending(a => a.OnlineExamDateId);
            if (id == null)
            {
                Session["rid"] = null;
                ViewBag.dateid = 0;
                ViewBag.start = "";
                ViewBag.end = "";

            }
            else
            {
                Session["rid"] = id;
                OnlineExamTime am = Dbcontext.OnlineExamTimes.Where(a => a.OnlineExamTimeId == id).FirstOrDefault();
                //  OnlineExamDate ob = Dbcontext.OnlineExamDates.Where(a => a.OnlineExamDateId == am.OnlineExamDateId).FirstOrDefault();
                ViewBag.dateid = am.OnlineExamDateId;
                ViewBag.start = am.Start_Time;
                ViewBag.end = am.End_Time;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveorUpdateExamtime(long dateid, TimeSpan start, TimeSpan end)
        {
            if (Session["rid"] == null)
            {
                OnlineExamTime ob = new OnlineExamTime();
                ob.OnlineExamDateId = dateid;
                ob.Start_Time = start;
                ob.End_Time = end;
                ob.Start_Time_Name = DateTime.ParseExact(start.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt");
                ob.End_Time_Name = DateTime.ParseExact(end.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt");

                ob.InsertedOn = DateTime.Now;
                ob.ModifiedOn = DateTime.Now;
                ob.InsertedId = 1;
                ob.ModifiedId = 1;
                ob.Is_Active = true;
                Dbcontext.OnlineExamTimes.Add(ob);
                Dbcontext.SaveChanges();
            }
            else
            {
                int id = Convert.ToInt32(Convert.ToString(Session["rid"]));
                OnlineExamTime ob = Dbcontext.OnlineExamTimes.Where(a => a.OnlineExamTimeId == id).FirstOrDefault();
                ob.OnlineExamDateId = dateid;
                ob.Start_Time = start;
                ob.End_Time = end;
                ob.Start_Time_Name = DateTime.ParseExact(start.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt");
                ob.End_Time_Name = DateTime.ParseExact(end.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt");

                ob.ModifiedId = 1;
                ob.ModifiedOn = DateTime.Now;
                ob.Is_Active = true;
                Dbcontext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Saved Successfully";
            return RedirectToAction("Examtime");
        }
        #endregion

        #region Online Exam
        public JsonResult GetExamTypeList()
        {
            var examtype = Dbcontext.OnlineExamTypes.Where(a => a.Is_Active == true);

            var res = (from a in examtype
                       select new
                       {
                           OnlineExamTypeId = a.OnlineExamTypeId,
                           OnlineExamTypeName = a.OnlineExamTypeName
                       }).ToList();
            return Json(res.ToList(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetExamDateList()
        {
            var examtype = Dbcontext.OnlineExamDates.Where(a => a.Is_Active == true);

            var res = (from a in examtype
                       select new
                       {
                           OnlineExamDateId = a.OnlineExamDateId,
                           OnlineExamDateName = a.OnlineExamDateName
                       }).ToList();
            return Json(res.ToList(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetExamDateTimeList(long id)
        {
            var examtype = Dbcontext.OnlineExamTimes.Where(a => a.Is_Active == true && a.OnlineExamDateId == id);

            var res = (from a in examtype
                       select new
                       {
                           OnlineExamTimeId = a.OnlineExamTimeId,
                           Start_Time = a.Start_Time,
                           End_Time = a.End_Time,
                           Start_Time_Name = a.Start_Time_Name,
                           End_Time_Name = a.End_Time_Name,
                       }).ToList();
            return Json(res.ToList(), JsonRequestBehavior.AllowGet);

        }
        public class OnlineExamModel
        {

            public long OnlineExamId { get; set; }
            public string OnlineExamCode { get; set; }
            public Nullable<long> OnlineExamTypeId { get; set; }
            public string OnlineExamTypeName { get; set; }
            public Nullable<long> OnlineExamDateId { get; set; }
            public string OnlineExamDateName { get; set; }
            public Nullable<System.DateTime> OnlineExamDate1 { get; set; }
            public Nullable<long> OnlineExamTimeId { get; set; }
            public Nullable<System.TimeSpan> Start_Time { get; set; }
            public Nullable<System.TimeSpan> End_Time { get; set; }
            public string Start_Time_Name { get; set; }
            public string End_Time_Name { get; set; }
            public Nullable<System.Guid> SchoolId { get; set; }
            public string SchoolName { get; set; }
            public Nullable<int> ClassId { get; set; }
            public string ClassName { get; set; }
            public Nullable<int> SubjectId { get; set; }
            public string SubjectName { get; set; }
            public Nullable<Guid> SectionId { get; set; }
            public string SectionName { get; set; }
            public string Exam_Instruction { get; set; }
            public string PDF { get; set; }
            public Nullable<bool> Is_Active { get; set; }
            public Nullable<int> InsertedId { get; set; }
            public Nullable<int> ModifiedId { get; set; }
            public Nullable<System.DateTime> InsertedOn { get; set; }
            public Nullable<System.DateTime> ModifiedOn { get; set; }
        }
        public ActionResult RemoveOnlineExam(long id)
        {
            OnlineExam res = Dbcontext.OnlineExams.Where(a => a.OnlineExamId == id).FirstOrDefault();
            res.Is_Active = false;
            res.ModifiedOn = DateTime.Now;
            Dbcontext.SaveChanges();
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("OnlineExam");
        }
        public ActionResult OnlineExam(Guid? schoolid, int? boardid, int? classid, int? Subjectid, int? ExamType, int? ExamDate, Nullable<TimeSpan> StartTime, Nullable<TimeSpan> EndTime)
        {


            ViewBag.school = null;
            ViewBag.board = null;
            ViewBag.cls = null;

            ViewBag.subject = null;
            ViewBag.examtype = null;
            ViewBag.examdate = null;
            ViewBag.startime = null;
            ViewBag.endtime = null;
            var OnlineExamTypeList = Dbcontext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            var OnlineExamDateList = Dbcontext.OnlineExamDates.Where(a => a.Is_Active == true).ToList();
            var OnlineExamTimeList = Dbcontext.OnlineExamTimes.Where(a => a.Is_Active == true).ToList();
            var OnlineExamList = Dbcontext.OnlineExams.Where(a => a.Is_Active == true).ToList();
            var SchoolList = Dbcontext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var ClassList = Dbcontext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var SubjectList = Dbcontext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var res = (from a in OnlineExamList
                       join b in OnlineExamTimeList on a.OnlineExamTimeId equals b.OnlineExamTimeId
                       join c in OnlineExamTypeList on a.OnlineExamTypeId equals c.OnlineExamTypeId
                       join d in OnlineExamDateList on a.OnlineExamDateId equals d.OnlineExamDateId
                       join e in ClassList on a.ClassId equals e.Class_Id
                       join f in SchoolList on a.SchoolId equals f.SchoolId
                       join g in SubjectList on a.SubjectId equals g.Subject_Id

                       select new OnlineExamModel
                       {
                           OnlineExamId = a.OnlineExamId,
                           OnlineExamCode = a.OnlineExamCode,
                           OnlineExamTypeId = a.OnlineExamTypeId,
                           OnlineExamTypeName = c.OnlineExamTypeName,
                           OnlineExamDateId = a.OnlineExamDateId,
                           OnlineExamDateName = d.OnlineExamDateName,
                           Start_Time = b.Start_Time,
                           Start_Time_Name = b.Start_Time_Name,
                           End_Time = b.End_Time,
                           End_Time_Name = b.End_Time_Name,
                           OnlineExamTimeId = a.OnlineExamTimeId,
                           SchoolId = a.SchoolId,
                           SubjectId = a.SubjectId,
                           ClassId = a.ClassId,
                           ClassName = e.Class_Name,
                           SchoolName = f.SchoolName,
                           SubjectName = g.Subject,
                           Exam_Instruction = a.Exam_Instruction,
                           PDF = a.PDF,
                           InsertedOn = a.InsertedOn,
                           ModifiedOn = a.ModifiedOn

                       }).ToList();
            if (schoolid != null)
            {
                res = res.Where(a => a.SchoolId == schoolid).ToList();
                ViewBag.school = schoolid;

            }
            if (boardid != null && boardid != 0)
            {
                ViewBag.board = boardid;

            }
            if (classid != null && classid != 0)
            {
                res = res.Where(a => a.ClassId == classid).ToList();
                ViewBag.cls = classid;
            }
            if (Subjectid != null && Subjectid != 0)
            {
                res = res.Where(a => a.SubjectId == Subjectid).ToList();
                ViewBag.subject = Subjectid;
            }
            if (ExamType != null && ExamType != 0)
            {
                res = res.Where(a => a.OnlineExamTypeId == ExamType).ToList();
                ViewBag.examtype = ExamType;


            }
            if (ExamDate != null && ExamDate != 0)
            {
                res = res.Where(a => a.OnlineExamDateId == ExamDate).ToList();
                ViewBag.examdate = ExamDate;

            }
            //if (StartTime != null)
            //{
            //    res = res.Where(a => a.Start_Time == StartTime).ToList();
            //    ViewBag.startime = StartTime;

            //}
            //if (EndTime != null)
            //{
            //    res = res.Where(a => a.End_Time == EndTime).ToList();
            //    ViewBag.endtime = EndTime;

            //}
            if (StartTime != null && EndTime != null)
            {
                res = res.Where(a => a.Start_Time >= StartTime && a.End_Time <= EndTime).ToList();
                ViewBag.startime = StartTime;
                ViewBag.endtime = EndTime;
            }

            return View(res.OrderByDescending(a => a.OnlineExamId));
        }
        public ActionResult CreateOnlineExam()
        {
            ViewBag.boardlist = Dbcontext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            ViewBag.schoollist = Dbcontext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.examtypes = Dbcontext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            ViewBag.examdates = Dbcontext.OnlineExamDates.Where(a => a.Is_Active == true).ToList();
            return View();
        }
        public ActionResult EditOnlineExam(long id)
        {
            var res = Dbcontext.OnlineExams.Where(a => a.OnlineExamId == id).FirstOrDefault();
            ViewBag.boardid = Dbcontext.tbl_DC_Class.Where(a => a.Class_Id == res.ClassId).FirstOrDefault().Board_Id;
            ViewBag.onlineexam = res;


            ViewBag.boardlist = Dbcontext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            ViewBag.schoollist = Dbcontext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.examtypes = Dbcontext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            ViewBag.examdates = Dbcontext.OnlineExamDates.Where(a => a.Is_Active == true).ToList();

            return View();
        }
        public JsonResult GetExamTimeList(long id)
        {
            var res = Dbcontext.OnlineExamTimes.Where(a => a.OnlineExamDateId == id && a.Is_Active == true).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveOnlineExam(int board, Guid[] school, int cls, int Subject, long examdate, long examtime, long examtype, string instruction, HttpPostedFileBase quespdf)
        {
            if (school.Count() > 0)
            {
                foreach (var a in school)
                {
                    OnlineExam onlineExam = new OnlineExam();
                    onlineExam.ClassId = cls;
                    onlineExam.Exam_Instruction = instruction;
                    onlineExam.InsertedId = 1;
                    onlineExam.InsertedOn = DateTime.Now;
                    onlineExam.Is_Active = true;
                    onlineExam.ModifiedId = 1;
                    onlineExam.ModifiedOn = DateTime.Now;
                    var res = Dbcontext.OnlineExams.ToList();
                    if (res.Count == 0)
                    {
                        onlineExam.OnlineExamCode = "ODM-" + 1;
                    }
                    else
                    {
                        onlineExam.OnlineExamCode = "ODM-" + (res.LastOrDefault().OnlineExamId + 1);
                    }
                    onlineExam.OnlineExamDateId = examdate;
                    onlineExam.OnlineExamTimeId = examtime;
                    onlineExam.OnlineExamTypeId = examtype;
                    onlineExam.SchoolId = a;
                    onlineExam.SubjectId = Subject;
                    if (quespdf != null)
                    {
                        string guid = Guid.NewGuid().ToString();
                        string path = string.Empty;
                        var fileName = Path.GetFileName(quespdf.FileName.
                            Replace(quespdf.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                        path = Path.Combine(Server.MapPath("~/Images/OnlineExam/"), fileName);
                        quespdf.SaveAs(path);
                        onlineExam.PDF = fileName;
                    }
                    Dbcontext.OnlineExams.Add(onlineExam);
                    Dbcontext.SaveChanges();
                    TempData["SuccessMessage"] = "Saved Successfully";
                }
            }
            return RedirectToAction("OnlineExam");
        }
        public ActionResult UpdateOnlineExam(long id, Guid school, int cls, int Subject, long examdate, long examtime, long examtype, string instruction, HttpPostedFileBase quespdf)
        {

            OnlineExam onlineExam = Dbcontext.OnlineExams.Where(a => a.OnlineExamId == id).FirstOrDefault();
            onlineExam.ClassId = cls;
            onlineExam.Exam_Instruction = instruction;
            onlineExam.Is_Active = true;
            onlineExam.ModifiedId = 1;
            onlineExam.ModifiedOn = DateTime.Now;
            onlineExam.OnlineExamDateId = examdate;
            onlineExam.OnlineExamTimeId = examtime;
            onlineExam.OnlineExamTypeId = examtype;
            onlineExam.SchoolId = school;
            onlineExam.SubjectId = Subject;
            if (quespdf != null)
            {
                string guid = Guid.NewGuid().ToString();
                string path = string.Empty;
                var fileName = Path.GetFileName(quespdf.FileName.
                    Replace(quespdf.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                path = Path.Combine(Server.MapPath("~/Images/OnlineExam/"), fileName);
                quespdf.SaveAs(path);
                onlineExam.PDF = fileName;
            }
            Dbcontext.SaveChanges();
            TempData["SuccessMessage"] = "Updated Successfully";
            return RedirectToAction("OnlineExam");
        }
        #endregion

        #region Answersheet Details


        #endregion

        #region Online Objective Exam
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        public ActionResult DownloadSampleObjectiveQuesExcel()
        {
            XLWorkbook oWB = new XLWorkbook();

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Question");
            validationTable.Columns.Add("Option 1");
            validationTable.Columns.Add("Option 2");
            validationTable.Columns.Add("Option 3");
            validationTable.Columns.Add("Option 4");
            validationTable.Columns.Add("Correct Answer(A,B,C,D)");
            validationTable.Columns.Add("Mark per Question");
            validationTable.TableName = "ObjectiveQuestion_Details";
            var worksheet = oWB.AddWorksheet(validationTable);

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "SampleObjectiveQuestion.xlsx");
        }


        public ActionResult OnlineObjectiveExamList(Guid? schoolid, int? boardid, int? classid, int? Subjectid, int? ExamType, int? ExamDate, Nullable<TimeSpan> StartTime, Nullable<TimeSpan> EndTime)
        {

            try
            {
                var OnlineObjectiveExamList = Dbcontext.OnlineObjectiveExams.Where(a => a.Is_Active == true).ToList();
                var OnlineObjectiveExamQuestionList = Dbcontext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true).ToList();

                var OnlineExamDateList = Dbcontext.OnlineExamDates.Where(a => a.Is_Active == true).ToList();
                var OnlineExamTimesList = Dbcontext.OnlineExamTimes.Where(a => a.Is_Active == true).ToList();
                var OnlineExamTypessList = Dbcontext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
                //var StudentList = Dbcontext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var SubjectList = Dbcontext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                var SchoolList = Dbcontext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
                var ClassList = Dbcontext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var BoardList = Dbcontext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();



                var res = (from a in OnlineObjectiveExamList
                           join d in OnlineExamDateList on a.OnlineExamDateId equals d.OnlineExamDateId
                           join e in OnlineExamTimesList on a.OnlineExamTimeId equals e.OnlineExamTimeId
                           join f in OnlineExamTypessList on a.OnlineExamTypeId equals f.OnlineExamTypeId
                           join g in SubjectList on a.SubjectId equals g.Subject_Id
                           join h in SchoolList on a.SchoolId equals h.SchoolId
                           join j in ClassList on a.ClassId equals j.Class_Id
                           join k in BoardList on a.BoardId equals k.Board_Id


                           select new ObjectiveExamDetailsModel
                           {
                               OnlineObjectiveExamId = a.OnlineObjectiveExamId,
                               SchoolId = a.SchoolId,
                               SchoolName = h.SchoolName,
                               ClassId = a.ClassId,
                               ClassName = j.Class_Name,
                               BoardId = a.BoardId,
                               BoardName = k.Board_Name,
                               OnlineExamDate = d.OnlineExamDate1,
                               OnlineExamDateId = a.OnlineExamDateId,
                               OnlineExamTypeId = a.OnlineExamTypeId,
                               OnlineExamDateName = d.OnlineExamDateName,
                               StartTime = e.Start_Time,
                               StartTimeName = e.Start_Time_Name,
                               EndTime = e.End_Time,
                               EndTimeName = e.End_Time_Name,
                               OnlineExamTypeName = f.OnlineExamTypeName,
                               SubjectName = g.Subject,
                             
                           }).ToList();
                if (schoolid != null)
                {
                    res = res.Where(a => a.SchoolId == schoolid).ToList();
                    ViewBag.school = schoolid;

                }
                if (boardid != null && boardid != 0)
                {
                    res = res.Where(a => a.BoardId == boardid).ToList();
                    ViewBag.board = boardid;

                }
                if (classid != null && classid != 0)
                {
                    res = res.Where(a => a.ClassId == classid).ToList();
                    ViewBag.cls = classid;
                }
                if (Subjectid != null && Subjectid != 0)
                {
                    res = res.Where(a => a.SubjectId == Subjectid).ToList();
                    ViewBag.subject = Subjectid;
                }
                if (ExamType != null && ExamType != 0)
                {
                    res = res.Where(a => a.OnlineExamTypeId == ExamType).ToList();
                    ViewBag.examtype = ExamType;


                }
                if (ExamDate != null && ExamDate != 0)
                {
                    res = res.Where(a => a.OnlineExamDateId == ExamDate).ToList();
                    ViewBag.examdate = ExamDate;

                }
                if (StartTime != null && EndTime != null)
                {
                    res = res.Where(a => a.StartTime >= StartTime && a.EndTime <= EndTime).ToList();
                    ViewBag.startime = StartTime;
                    ViewBag.endtime = EndTime;
                }
                return View(res.OrderByDescending(a=>a.OnlineObjectiveExamId).ToList());
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public ActionResult RemoveObjectiveExam(long id)
        {
            OnlineObjectiveExam online = Dbcontext.OnlineObjectiveExams.Where(a => a.OnlineObjectiveExamId == id).FirstOrDefault();
            online.Is_Active = false;
            online.ModifiedOn = DateTime.Now;
            Dbcontext.SaveChanges();
            return RedirectToAction("OnlineObjectiveExamList");
        }
        public ActionResult CreateObjectiveExam()
        {
            ViewBag.boardlist = Dbcontext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            ViewBag.schoollist = Dbcontext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.examtypes = Dbcontext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            ViewBag.examdates = Dbcontext.OnlineExamDates.Where(a => a.Is_Active == true).OrderByDescending(a=>a.OnlineExamDateName).ToList();
            ViewBag.negativemark = Dbcontext.Negativemarks.Where(a => a.Is_Active == true).ToList();
            return View();
        }

        [HttpPost]
        public JsonResult SaveOnlineObjectiveExam(onlineobjexamcls exam)
        {
            foreach (var a in exam.school)
            {
                OnlineObjectiveExam objexam = new OnlineObjectiveExam();
                objexam.BoardId = exam.board;
                objexam.ClassId = exam.cls;
                objexam.Exam_Instruction = exam.instruction;
                objexam.InsertedId = 1;
                objexam.InsertedOn = DateTime.Now;
                objexam.IsNagativeMarking = exam.isnegative;
                objexam.Is_Active = true;
                objexam.ModifiedId = 1;
                objexam.ModifiedOn = DateTime.Now;
                objexam.NagativeMarkingId = exam.negativemark;
                objexam.OnlineExamDateId = exam.examdate;
                objexam.OnlineExamTimeId = exam.examtime;
                objexam.OnlineExamTypeId = exam.examtype;
                var res = Dbcontext.OnlineObjectiveExams.ToList();
                if (res.Count == 0)
                {
                    objexam.OnlineObjectiveExamCode = "ODM-" + 1;
                }
                else
                {
                    objexam.OnlineObjectiveExamCode = "ODM-" + (res.LastOrDefault().OnlineObjectiveExamId + 1);
                }
                objexam.SchoolId = a;
                objexam.SubjectId = exam.subject;
                Dbcontext.OnlineObjectiveExams.Add(objexam);
                Dbcontext.SaveChanges();
                long objid = objexam.OnlineObjectiveExamId;
                if (exam.question.Count > 0)
                {
                    foreach (var ques in exam.question)
                    {
                        if (ques.question != null)
                        {
                            OnlineObjectiveExamQuestion examQuestion = new OnlineObjectiveExamQuestion();
                            examQuestion.InsertedId = 1;
                            examQuestion.InsertedOn = DateTime.Now;
                            examQuestion.Is_Active = true;
                            examQuestion.Mark = ques.mark;
                            examQuestion.ModifiedId = 1;
                            examQuestion.ModifiedOn = DateTime.Now;
                            examQuestion.OnlineObjectiveExamId = objid;
                            examQuestion.Question = ques.question;
                            if (ques.questionimage != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(ques.questionimage.FileName.
                                    Replace(ques.questionimage.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Images/ObjectiveQuestion/"), fileName);
                                ques.questionimage.SaveAs(path);
                                examQuestion.QuestionImage = fileName;
                            }
                            Dbcontext.OnlineObjectiveExamQuestions.Add(examQuestion);
                            Dbcontext.SaveChanges();
                            long quesid = examQuestion.OnlineObjectiveExamQuestionId;
                            OnlineObjectiveExamAnswer examAnswer = new OnlineObjectiveExamAnswer();
                            examAnswer.InsertedId = 1;
                            examAnswer.InsertedOn = DateTime.Now;
                            examAnswer.Is_Active = true;
                            examAnswer.ModifiedId = 1;
                            examAnswer.ModifiedOn = DateTime.Now;
                            examAnswer.OnlineObjectiveExamQuestionId = quesid;
                            examAnswer.Option = ques.option1;
                            if (ques.option1image != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(ques.option1image.FileName.
                                    Replace(ques.option1image.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Images/ObjectiveOption/"), fileName);
                                ques.option1image.SaveAs(path);
                                examAnswer.OptionImage = fileName;
                            }
                            examAnswer.IsCorrect = ques.CorrectAns == 1 ? true : false;
                            Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                            Dbcontext.SaveChanges();
                            examAnswer = new OnlineObjectiveExamAnswer();
                            examAnswer.InsertedId = 1;
                            examAnswer.InsertedOn = DateTime.Now;
                            examAnswer.Is_Active = true;
                            examAnswer.ModifiedId = 1;
                            examAnswer.ModifiedOn = DateTime.Now;
                            examAnswer.OnlineObjectiveExamQuestionId = quesid;
                            examAnswer.Option = ques.option2;
                            if (ques.option2image != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(ques.option2image.FileName.
                                    Replace(ques.option2image.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Images/ObjectiveOption/"), fileName);
                                ques.option2image.SaveAs(path);
                                examAnswer.OptionImage = fileName;
                            }
                            examAnswer.IsCorrect = ques.CorrectAns == 2 ? true : false;
                            Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                            Dbcontext.SaveChanges();
                            examAnswer = new OnlineObjectiveExamAnswer();
                            examAnswer.InsertedId = 1;
                            examAnswer.InsertedOn = DateTime.Now;
                            examAnswer.Is_Active = true;
                            examAnswer.ModifiedId = 1;
                            examAnswer.ModifiedOn = DateTime.Now;
                            examAnswer.OnlineObjectiveExamQuestionId = quesid;
                            examAnswer.Option = ques.option3;
                            if (ques.option3image != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(ques.option3image.FileName.
                                    Replace(ques.option3image.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Images/ObjectiveOption/"), fileName);
                                ques.option3image.SaveAs(path);
                                examAnswer.OptionImage = fileName;
                            }
                            examAnswer.IsCorrect = ques.CorrectAns == 3 ? true : false;
                            Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                            Dbcontext.SaveChanges();
                            examAnswer = new OnlineObjectiveExamAnswer();
                            examAnswer.InsertedId = 1;
                            examAnswer.InsertedOn = DateTime.Now;
                            examAnswer.Is_Active = true;
                            examAnswer.ModifiedId = 1;
                            examAnswer.ModifiedOn = DateTime.Now;
                            examAnswer.OnlineObjectiveExamQuestionId = quesid;
                            examAnswer.Option = ques.option4;
                            if (ques.option4image != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(ques.option4image.FileName.
                                    Replace(ques.option4image.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Images/ObjectiveOption/"), fileName);
                                ques.option4image.SaveAs(path);
                                examAnswer.OptionImage = fileName;
                            }
                            examAnswer.IsCorrect = ques.CorrectAns == 4 ? true : false;
                            Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                            Dbcontext.SaveChanges();
                        }
                    }
                }
                if (exam.filebulkques != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(exam.filebulkques.FileName).ToLower();
                    if (allowedExtensions.Contains(checkextension))
                    {
                        if (exam.filebulkques != null && exam.filebulkques.ContentLength > 0)
                        {
                            var path = Path.Combine(Server.MapPath("~/Excels/" + exam.filebulkques.FileName));
                            //save File
                            exam.filebulkques.SaveAs(path);
                            string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                            OleDbConnection connection = new OleDbConnection();
                            connection.ConnectionString = excelConnectionString;
                            OleDbDataAdapter command = new OleDbDataAdapter("select * from [ObjectiveQuestion_Details$]", connection);
                            // connection.Open();
                            // Create DbDataReader to Data Worksheet
                            DataTable dt = new DataTable();
                            command.Fill(dt);

                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {                                    
                                    string ques = dr["Question"].ToString();
                                    if (ques != "")
                                    {
                                        OnlineObjectiveExamQuestion examQuestion = new OnlineObjectiveExamQuestion();
                                        examQuestion.InsertedId = 1;
                                        examQuestion.InsertedOn = DateTime.Now;
                                        examQuestion.Is_Active = true;
                                        examQuestion.Mark =Convert.ToDecimal(dr["Mark per Question"].ToString());
                                        examQuestion.ModifiedId = 1;
                                        examQuestion.ModifiedOn = DateTime.Now;
                                        examQuestion.OnlineObjectiveExamId = objid;
                                        examQuestion.Question = dr["Question"].ToString();                                       
                                        Dbcontext.OnlineObjectiveExamQuestions.Add(examQuestion);
                                        Dbcontext.SaveChanges();
                                        long quesid = examQuestion.OnlineObjectiveExamQuestionId;
                                        OnlineObjectiveExamAnswer examAnswer = new OnlineObjectiveExamAnswer();
                                        examAnswer.InsertedId = 1;
                                        examAnswer.InsertedOn = DateTime.Now;
                                        examAnswer.Is_Active = true;
                                        examAnswer.ModifiedId = 1;
                                        examAnswer.ModifiedOn = DateTime.Now;
                                        examAnswer.OnlineObjectiveExamQuestionId = quesid;
                                        examAnswer.Option = dr["Option 1"].ToString();                                       
                                        examAnswer.IsCorrect = dr["Correct Answer(A,B,C,D)"].ToString() == "A" ? true : false;
                                        Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                                        Dbcontext.SaveChanges();
                                        examAnswer = new OnlineObjectiveExamAnswer();
                                        examAnswer.InsertedId = 1;
                                        examAnswer.InsertedOn = DateTime.Now;
                                        examAnswer.Is_Active = true;
                                        examAnswer.ModifiedId = 1;
                                        examAnswer.ModifiedOn = DateTime.Now;
                                        examAnswer.OnlineObjectiveExamQuestionId = quesid;
                                        examAnswer.Option = dr["Option 2"].ToString();
                                        examAnswer.IsCorrect = dr["Correct Answer(A,B,C,D)"].ToString() == "B" ? true : false;
                                        Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                                        Dbcontext.SaveChanges();
                                        examAnswer = new OnlineObjectiveExamAnswer();
                                        examAnswer.InsertedId = 1;
                                        examAnswer.InsertedOn = DateTime.Now;
                                        examAnswer.Is_Active = true;
                                        examAnswer.ModifiedId = 1;
                                        examAnswer.ModifiedOn = DateTime.Now;
                                        examAnswer.OnlineObjectiveExamQuestionId = quesid;
                                        examAnswer.Option = dr["Option 3"].ToString();
                                        examAnswer.IsCorrect = dr["Correct Answer(A,B,C,D)"].ToString() == "C" ? true : false;
                                        Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                                        Dbcontext.SaveChanges();
                                        examAnswer = new OnlineObjectiveExamAnswer();
                                        examAnswer.InsertedId = 1;
                                        examAnswer.InsertedOn = DateTime.Now;
                                        examAnswer.Is_Active = true;
                                        examAnswer.ModifiedId = 1;
                                        examAnswer.ModifiedOn = DateTime.Now;
                                        examAnswer.OnlineObjectiveExamQuestionId = quesid;
                                        examAnswer.Option = dr["Option 4"].ToString();
                                        examAnswer.IsCorrect = dr["Correct Answer(A,B,C,D)"].ToString() == "D" ? true : false;
                                        Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                                        Dbcontext.SaveChanges();
                                    }
                                }
                            }
                            if ((System.IO.File.Exists(path)))
                            {
                                System.IO.File.Delete(path);
                            }
                        }
                    }
                }
                
            }
            return Json(1, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ViewQuestionObjectiveExam(long id)
        {
            var OnlineObjectiveExamList = Dbcontext.OnlineObjectiveExams.Where(a => a.Is_Active == true && a.OnlineObjectiveExamId == id).ToList();
            var OnlineObjectiveExamQuestionList = Dbcontext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true && a.OnlineObjectiveExamId == id).ToList();
            var OnlineObjectiveExamAswerList = Dbcontext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();

            var OnlineExamDateList = Dbcontext.OnlineExamDates.Where(a => a.Is_Active == true).ToList();
            var OnlineExamTimesList = Dbcontext.OnlineExamTimes.Where(a => a.Is_Active == true).ToList();
            var OnlineExamTypessList = Dbcontext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            //var StudentList = Dbcontext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var SubjectList = Dbcontext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
            var SchoolList = Dbcontext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var ClassList = Dbcontext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var BoardList = Dbcontext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var negativemarkList = Dbcontext.Negativemarks.Where(a => a.Is_Active == true).ToList();

            var objexam = (from a in OnlineObjectiveExamList
                           join d in OnlineExamDateList on a.OnlineExamDateId equals d.OnlineExamDateId
                           join e in OnlineExamTimesList on a.OnlineExamTimeId equals e.OnlineExamTimeId
                           join f in OnlineExamTypessList on a.OnlineExamTypeId equals f.OnlineExamTypeId
                           join g in SubjectList on a.SubjectId equals g.Subject_Id
                           join h in SchoolList on a.SchoolId equals h.SchoolId
                           join j in ClassList on a.ClassId equals j.Class_Id
                           join k in BoardList on a.BoardId equals k.Board_Id
                           select new ObjectiveExamDetailsModel
                           {
                               OnlineObjectiveExamId = a.OnlineObjectiveExamId,
                               SchoolId = a.SchoolId,
                               SchoolName = h.SchoolName,
                               ClassId = a.ClassId,
                               ClassName = j.Class_Name,
                               BoardId = a.BoardId,
                               BoardName = k.Board_Name,
                               OnlineExamDate = d.OnlineExamDate1,
                               OnlineExamDateId = a.OnlineExamDateId,
                               OnlineExamTypeId = a.OnlineExamTypeId,
                               OnlineExamDateName = d.OnlineExamDateName,
                               StartTime = e.Start_Time,
                               StartTimeName = e.Start_Time_Name,
                               EndTime = e.End_Time,
                               EndTimeName = e.End_Time_Name,
                               OnlineExamTypeName = f.OnlineExamTypeName,
                               SubjectName = g.Subject,
                               IsNegative = a.IsNagativeMarking.Value,
                               NegativeMark = a.IsNagativeMarking == true ? (negativemarkList.Where(m => m.NegativeMarkID == a.NagativeMarkingId).FirstOrDefault().NegativeMark1.ToString()) : ""
                           }).FirstOrDefault();
            var objques = (from a in OnlineObjectiveExamQuestionList
                           select new objectiveallquescls
                           {
                               examQuestion = a,
                               examAnswers = OnlineObjectiveExamAswerList.Where(m => m.OnlineObjectiveExamQuestionId == a.OnlineObjectiveExamQuestionId).ToList()
                           }).ToList();
            ViewObjectiveExamcls viewObjective = new ViewObjectiveExamcls();
            viewObjective.objectiveExam = objexam;
            viewObjective.questions = objques;

            return View(viewObjective);
        }
        public ActionResult EditObjectiveExam(long id)
        {
            ViewBag.boardlist = Dbcontext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            ViewBag.schoollist = Dbcontext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.examtypes = Dbcontext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            ViewBag.examdates = Dbcontext.OnlineExamDates.Where(a => a.Is_Active == true).OrderByDescending(a => a.OnlineExamDateName).ToList();
            ViewBag.negativemark = Dbcontext.Negativemarks.Where(a => a.Is_Active == true).ToList();
            var OnlineObjectiveExamQuestionList = Dbcontext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true && a.OnlineObjectiveExamId == id).ToList();
            var OnlineObjectiveExamAswerList = Dbcontext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();

            editobjectiveexamcls editobjective = new editobjectiveexamcls();
            editobjective.exam = Dbcontext.OnlineObjectiveExams.Where(a => a.OnlineObjectiveExamId == id).FirstOrDefault();
            var objques = (from a in OnlineObjectiveExamQuestionList
                           select new objectiveallquescls
                           {
                               examQuestion = a,
                               examAnswers = OnlineObjectiveExamAswerList.Where(m => m.OnlineObjectiveExamQuestionId == a.OnlineObjectiveExamQuestionId).ToList()
                           }).ToList();
            editobjective.questions = objques;
            return View(editobjective);
        }
        [HttpPost]
        public JsonResult UpdateOnlineObjectiveExam(onlineobjexamcls exam)
        {

            OnlineObjectiveExam objexam = Dbcontext.OnlineObjectiveExams.Where(a => a.OnlineObjectiveExamId == exam.objectiveid).FirstOrDefault();
            objexam.BoardId = exam.board;
            objexam.ClassId = exam.cls;
            objexam.Exam_Instruction = exam.instruction;
            objexam.IsNagativeMarking = exam.isnegative;
            objexam.Is_Active = true;
            objexam.ModifiedOn = DateTime.Now;
            objexam.NagativeMarkingId = exam.negativemark;
            objexam.OnlineExamDateId = exam.examdate;
            objexam.OnlineExamTimeId = exam.examtime;
            objexam.OnlineExamTypeId = exam.examtype;
            objexam.SchoolId = exam.schoolid;
            objexam.SubjectId = exam.subject;
            Dbcontext.SaveChanges();
            long objid = objexam.OnlineObjectiveExamId;
            if (exam.question!=null)
            {
                foreach (var ques in exam.question.Where(a => a.questionid == 0).ToList())
                {
                    if (ques.question != null)
                    {
                        OnlineObjectiveExamQuestion examQuestion = new OnlineObjectiveExamQuestion();
                        examQuestion.InsertedId = 1;
                        examQuestion.InsertedOn = DateTime.Now;
                        examQuestion.Is_Active = true;
                        examQuestion.Mark = ques.mark;
                        examQuestion.ModifiedId = 1;
                        examQuestion.ModifiedOn = DateTime.Now;
                        examQuestion.OnlineObjectiveExamId = objid;
                        examQuestion.Question = ques.question;
                        if (ques.questionimage != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(ques.questionimage.FileName.
                                Replace(ques.questionimage.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Images/ObjectiveQuestion/"), fileName);
                            ques.questionimage.SaveAs(path);
                            examQuestion.QuestionImage = fileName;
                        }
                        Dbcontext.OnlineObjectiveExamQuestions.Add(examQuestion);
                        Dbcontext.SaveChanges();
                        long quesid = examQuestion.OnlineObjectiveExamQuestionId;
                        OnlineObjectiveExamAnswer examAnswer = new OnlineObjectiveExamAnswer();
                        examAnswer.InsertedId = 1;
                        examAnswer.InsertedOn = DateTime.Now;
                        examAnswer.Is_Active = true;
                        examAnswer.ModifiedId = 1;
                        examAnswer.ModifiedOn = DateTime.Now;
                        examAnswer.OnlineObjectiveExamQuestionId = quesid;
                        examAnswer.Option = ques.option1;
                        if (ques.option1image != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(ques.option1image.FileName.
                                Replace(ques.option1image.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Images/ObjectiveOption/"), fileName);
                            ques.option1image.SaveAs(path);
                            examAnswer.OptionImage = fileName;
                        }
                        examAnswer.IsCorrect = ques.CorrectAns == 1 ? true : false;
                        Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                        Dbcontext.SaveChanges();
                        examAnswer = new OnlineObjectiveExamAnswer();
                        examAnswer.InsertedId = 1;
                        examAnswer.InsertedOn = DateTime.Now;
                        examAnswer.Is_Active = true;
                        examAnswer.ModifiedId = 1;
                        examAnswer.ModifiedOn = DateTime.Now;
                        examAnswer.OnlineObjectiveExamQuestionId = quesid;
                        examAnswer.Option = ques.option2;
                        if (ques.option2image != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(ques.option2image.FileName.
                                Replace(ques.option2image.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Images/ObjectiveOption/"), fileName);
                            ques.option2image.SaveAs(path);
                            examAnswer.OptionImage = fileName;
                        }
                        examAnswer.IsCorrect = ques.CorrectAns == 2 ? true : false;
                        Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                        Dbcontext.SaveChanges();
                        examAnswer = new OnlineObjectiveExamAnswer();
                        examAnswer.InsertedId = 1;
                        examAnswer.InsertedOn = DateTime.Now;
                        examAnswer.Is_Active = true;
                        examAnswer.ModifiedId = 1;
                        examAnswer.ModifiedOn = DateTime.Now;
                        examAnswer.OnlineObjectiveExamQuestionId = quesid;
                        examAnswer.Option = ques.option3;
                        if (ques.option3image != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(ques.option3image.FileName.
                                Replace(ques.option3image.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Images/ObjectiveOption/"), fileName);
                            ques.option3image.SaveAs(path);
                            examAnswer.OptionImage = fileName;
                        }
                        examAnswer.IsCorrect = ques.CorrectAns == 3 ? true : false;
                        Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                        Dbcontext.SaveChanges();
                        examAnswer = new OnlineObjectiveExamAnswer();
                        examAnswer.InsertedId = 1;
                        examAnswer.InsertedOn = DateTime.Now;
                        examAnswer.Is_Active = true;
                        examAnswer.ModifiedId = 1;
                        examAnswer.ModifiedOn = DateTime.Now;
                        examAnswer.OnlineObjectiveExamQuestionId = quesid;
                        examAnswer.Option = ques.option4;
                        if (ques.option4image != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(ques.option4image.FileName.
                                Replace(ques.option4image.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Images/ObjectiveOption/"), fileName);
                            ques.option4image.SaveAs(path);
                            examAnswer.OptionImage = fileName;
                        }
                        examAnswer.IsCorrect = ques.CorrectAns == 4 ? true : false;
                        Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                        Dbcontext.SaveChanges();
                    }
                }
                foreach (var ques in exam.question.Where(a => a.questionid != 0).ToList())
                {
                    if(exam.remvques!=null && exam.remvques.Contains(ques.questionid))
                    {
                        OnlineObjectiveExamQuestion examQuestion = Dbcontext.OnlineObjectiveExamQuestions.Where(m=>m.OnlineObjectiveExamQuestionId==ques.questionid).FirstOrDefault();
                        
                        examQuestion.Is_Active = false;
                        examQuestion.ModifiedOn = DateTime.Now;
                        Dbcontext.SaveChanges();
                    }
                    else
                    {
                        OnlineObjectiveExamQuestion examQuestion = Dbcontext.OnlineObjectiveExamQuestions.Where(m => m.OnlineObjectiveExamQuestionId == ques.questionid).FirstOrDefault();
                        
                        examQuestion.Is_Active = true;
                        examQuestion.Mark = ques.mark;
                        examQuestion.ModifiedId = 1;
                        examQuestion.ModifiedOn = DateTime.Now;
                        examQuestion.OnlineObjectiveExamId = objid;
                        examQuestion.Question = ques.question;
                        if (ques.questionimage != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(ques.questionimage.FileName.
                                Replace(ques.questionimage.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Images/ObjectiveQuestion/"), fileName);
                            ques.questionimage.SaveAs(path);
                            examQuestion.QuestionImage = fileName;
                        }
                        Dbcontext.SaveChanges();
                        long quesid = examQuestion.OnlineObjectiveExamQuestionId;
                        var answers = Dbcontext.OnlineObjectiveExamAnswers.Where(m => m.Is_Active == true && m.OnlineObjectiveExamQuestionId == quesid).ToList();
                        OnlineObjectiveExamAnswer examAnswer = answers[0];
                        examAnswer.Is_Active = true;
                        examAnswer.ModifiedId = 1;
                        examAnswer.ModifiedOn = DateTime.Now;
                        examAnswer.OnlineObjectiveExamQuestionId = quesid;
                        examAnswer.Option = ques.option1;
                        if (ques.option1image != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(ques.option1image.FileName.
                                Replace(ques.option1image.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Images/ObjectiveOption/"), fileName);
                            ques.option1image.SaveAs(path);
                            examAnswer.OptionImage = fileName;
                        }
                        examAnswer.IsCorrect = ques.CorrectAns == 1 ? true : false;
                        Dbcontext.SaveChanges();
                        //examAnswer = new OnlineObjectiveExamAnswer();
                        examAnswer = answers[1];
                        examAnswer.Is_Active = true;
                        examAnswer.ModifiedId = 1;
                        examAnswer.ModifiedOn = DateTime.Now;
                        examAnswer.OnlineObjectiveExamQuestionId = quesid;
                        examAnswer.Option = ques.option2;
                        if (ques.option2image != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(ques.option2image.FileName.
                                Replace(ques.option2image.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Images/ObjectiveOption/"), fileName);
                            ques.option2image.SaveAs(path);
                            examAnswer.OptionImage = fileName;
                        }
                        examAnswer.IsCorrect = ques.CorrectAns == 2 ? true : false;
                        Dbcontext.SaveChanges();
                        examAnswer = answers[2];
                        examAnswer.Is_Active = true;
                        examAnswer.ModifiedId = 1;
                        examAnswer.ModifiedOn = DateTime.Now;
                        examAnswer.OnlineObjectiveExamQuestionId = quesid;
                        examAnswer.Option = ques.option3;
                        if (ques.option3image != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(ques.option3image.FileName.
                                Replace(ques.option3image.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Images/ObjectiveOption/"), fileName);
                            ques.option3image.SaveAs(path);
                            examAnswer.OptionImage = fileName;
                        }
                        examAnswer.IsCorrect = ques.CorrectAns == 3 ? true : false;
                        Dbcontext.SaveChanges();
                        examAnswer = answers[3];
                        examAnswer.Is_Active = true;
                        examAnswer.ModifiedId = 1;
                        examAnswer.ModifiedOn = DateTime.Now;
                        examAnswer.OnlineObjectiveExamQuestionId = quesid;
                        examAnswer.Option = ques.option4;
                        if (ques.option4image != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(ques.option4image.FileName.
                                Replace(ques.option4image.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Images/ObjectiveOption/"), fileName);
                            ques.option4image.SaveAs(path);
                            examAnswer.OptionImage = fileName;
                        }
                        examAnswer.IsCorrect = ques.CorrectAns == 4 ? true : false;
                        Dbcontext.SaveChanges();
                    }
                }
            }
            if (exam.filebulkques != null)
            {
                var allowedExtensions = new[] { ".xlsx", ".xls" };

                var checkextension = Path.GetExtension(exam.filebulkques.FileName).ToLower();
                if (allowedExtensions.Contains(checkextension))
                {
                    if (exam.filebulkques != null && exam.filebulkques.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + exam.filebulkques.FileName));
                        //save File
                        exam.filebulkques.SaveAs(path);
                        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                        OleDbConnection connection = new OleDbConnection();
                        connection.ConnectionString = excelConnectionString;
                        OleDbDataAdapter command = new OleDbDataAdapter("select * from [ObjectiveQuestion_Details$]", connection);
                        // connection.Open();
                        // Create DbDataReader to Data Worksheet
                        DataTable dt = new DataTable();
                        command.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string ques = dr["Question"].ToString();
                                if (ques != "")
                                {
                                    OnlineObjectiveExamQuestion examQuestion = new OnlineObjectiveExamQuestion();
                                    examQuestion.InsertedId = 1;
                                    examQuestion.InsertedOn = DateTime.Now;
                                    examQuestion.Is_Active = true;
                                    examQuestion.Mark = Convert.ToDecimal(dr["Mark per Question"].ToString());
                                    examQuestion.ModifiedId = 1;
                                    examQuestion.ModifiedOn = DateTime.Now;
                                    examQuestion.OnlineObjectiveExamId = objid;
                                    examQuestion.Question = dr["Question"].ToString();
                                    Dbcontext.OnlineObjectiveExamQuestions.Add(examQuestion);
                                    Dbcontext.SaveChanges();
                                    long quesid = examQuestion.OnlineObjectiveExamQuestionId;
                                    OnlineObjectiveExamAnswer examAnswer = new OnlineObjectiveExamAnswer();
                                    examAnswer.InsertedId = 1;
                                    examAnswer.InsertedOn = DateTime.Now;
                                    examAnswer.Is_Active = true;
                                    examAnswer.ModifiedId = 1;
                                    examAnswer.ModifiedOn = DateTime.Now;
                                    examAnswer.OnlineObjectiveExamQuestionId = quesid;
                                    examAnswer.Option = dr["Option 1"].ToString();
                                    examAnswer.IsCorrect = dr["Correct Answer(A,B,C,D)"].ToString() == "A" ? true : false;
                                    Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                                    Dbcontext.SaveChanges();
                                    examAnswer = new OnlineObjectiveExamAnswer();
                                    examAnswer.InsertedId = 1;
                                    examAnswer.InsertedOn = DateTime.Now;
                                    examAnswer.Is_Active = true;
                                    examAnswer.ModifiedId = 1;
                                    examAnswer.ModifiedOn = DateTime.Now;
                                    examAnswer.OnlineObjectiveExamQuestionId = quesid;
                                    examAnswer.Option = dr["Option 2"].ToString();
                                    examAnswer.IsCorrect = dr["Correct Answer(A,B,C,D)"].ToString() == "B" ? true : false;
                                    Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                                    Dbcontext.SaveChanges();
                                    examAnswer = new OnlineObjectiveExamAnswer();
                                    examAnswer.InsertedId = 1;
                                    examAnswer.InsertedOn = DateTime.Now;
                                    examAnswer.Is_Active = true;
                                    examAnswer.ModifiedId = 1;
                                    examAnswer.ModifiedOn = DateTime.Now;
                                    examAnswer.OnlineObjectiveExamQuestionId = quesid;
                                    examAnswer.Option = dr["Option 3"].ToString();
                                    examAnswer.IsCorrect = dr["Correct Answer(A,B,C,D)"].ToString() == "C" ? true : false;
                                    Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                                    Dbcontext.SaveChanges();
                                    examAnswer = new OnlineObjectiveExamAnswer();
                                    examAnswer.InsertedId = 1;
                                    examAnswer.InsertedOn = DateTime.Now;
                                    examAnswer.Is_Active = true;
                                    examAnswer.ModifiedId = 1;
                                    examAnswer.ModifiedOn = DateTime.Now;
                                    examAnswer.OnlineObjectiveExamQuestionId = quesid;
                                    examAnswer.Option = dr["Option 4"].ToString();
                                    examAnswer.IsCorrect = dr["Correct Answer(A,B,C,D)"].ToString() == "D" ? true : false;
                                    Dbcontext.OnlineObjectiveExamAnswers.Add(examAnswer);
                                    Dbcontext.SaveChanges();
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                    }
                }
            }
            return Json(1, JsonRequestBehavior.AllowGet);

        }
        #endregion
    }
}
