﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DigiChamps.Controllers
{
    public class OnlineAPIController : ApiController
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();
        [HttpGet]
        public HttpResponseMessage OnlineClassDetails(int id, int studentid)
        {
            try
            {
                var onlineClsList = DbContext.OnlineClasses.Where(a => a.Active == true);
                var schoolList = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true);
                var boardList = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true);
                var classList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true);
                var sectionList = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true);
                var teacherList = DbContext.Teachers.Where(a => a.Active == 1);
                var subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true);
                adminviewonlineclasses adminviewonlineclasses = new adminviewonlineclasses();
                var onlineClasses = (from a in onlineClsList
                                     join b in teacherList on a.TeacherId equals b.TeacherId
                                     join c in subjectList on a.SubjectId equals c.Subject_Id
                                     join d in sectionList on a.SectionId equals d.SectionId
                                     join e in schoolList on b.SchoolId equals e.SchoolId
                                     join f in classList on c.Class_Id equals f.Class_Id
                                     join g in boardList on f.Board_Id equals g.Board_Id
                                     where a.ID == id
                                     select new OnlineClassModel
                                     {
                                         ID = a.ID,
                                        // TeacherId = Decimal.ToInt32(a.TeacherId.Value),
                                         // TeacherName = teacherList.Where(x=>x.TeacherId == a.TeacherId).Select(x=>x.Name).FirstOrDefault(),
                                         TeacherName=b.Name,
                                         SubjectId = a.SubjectId,
                                         SubjectName = c.Subject,
                                         FromDate = a.FromDate,
                                         StartTime = a.StartTime,
                                         EndTime = a.EndTime,
                                         SectionId = a.SectionId,
                                         SectionName = d.SectionName,
                                         ClassId = c.Class_Id,
                                         ClassName = f.Class_Name,
                                         BoardId = f.Board_Id,
                                         BoardName = g.Board_Name,
                                         SchoolId = b.SchoolId,
                                         SchoolName = e.SchoolName
                                     }).FirstOrDefault();
                var onlineClassSyllabus = DbContext.OnlineClassSyllabus.Where(a => a.Active == true && a.OnlineClassId == id);
                var chapterList = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true);

                var syllabus = (from a in onlineClassSyllabus
                                join b in chapterList on a.ChapterId equals b.Chapter_Id
                                select new OnlineClassSyllabusModel
                                {
                                    ID = a.ID,
                                    OnlineClassId = a.OnlineClassId.Value,
                                    SyllabusTitle = a.SyllabusTitle,
                                    SyllabusDescription = a.SyllabusDescription,
                                    ChapterId = a.ChapterId.Value,
                                    ChapterName = b.Chapter,
                                    status = a.Status,
                                    modifieddate = a.ModifiedDate
                                }).ToList();
                var studentattendance = DbContext.OnlineClassAttendences.Where(a => a.Active == true && a.OnlineClassId == id && a.StudentId== studentid).ToList();
                var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var studentdetails = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true).ToList();

                List<onlinestudentattendance> attendance = null;

                if (onlineClasses!=null)
                {
                    attendance = (from a in students
                                      join b in studentdetails on a.Regd_ID equals b.Regd_ID
                                      where a.SchoolId == onlineClasses.SchoolId && a.SectionId == onlineClasses.SectionId && b.Class_ID == onlineClasses.ClassId && b.Board_ID == onlineClasses.BoardId
                                      select new onlinestudentattendance
                                      {
                                          StudentId = a.Regd_ID,
                                          Student_Name = a.Customer_Name,
                                          IsPresent = studentattendance.Where(m => m.OnlineClassId == id && m.Active == true).FirstOrDefault() == null ? false : studentattendance.Where(m => m.StudentId == a.Regd_ID && m.OnlineClassId == id && m.Active == true).FirstOrDefault().IsPresent.Value,
                                          modifieddate = studentattendance.Where(m => m.OnlineClassId == id && m.Active == true).FirstOrDefault() == null ? null : studentattendance.Where(m => m.StudentId == a.Regd_ID && m.OnlineClassId == id && m.Active == true).FirstOrDefault().ModifiedDate
                                      }).ToList();
                }     

                var studentHomeWork = DbContext.OnlineClassStudentHomeWorks.Where(a => a.Active == true);
                var homeWorkList = DbContext.OnlineClassHomeWorks.Where(a => a.Active == true && a.OnlineClassId == id);
                var registrationList = students.Where(a => a.Is_Active == true && a.SectionId == onlineClasses.SectionId).Count();


                var res2 = (from a in homeWorkList
                            select new OnlineHomeWorkModel
                            {
                                ID = a.ID,
                                HomeWorkPdf = "/Images/Homeworkquespdf/" + a.HomeWorkPdf,
                                InsertedDate = a.InsertedDate.Value,
                                TotalStudentCount = registrationList,
                                UploadedStudentCount = studentHomeWork.Where(m => m.OnlineClassHomeWorkId == a.ID).ToList().Count(),
                                NotUploaded = (registrationList - (studentHomeWork.Where(m => m.OnlineClassHomeWorkId == a.ID).ToList().Count()))
                            }).ToList();
                var feedbackList = DbContext.OnlineClassFeedbacks.Where(a => a.Active == true && a.OnlineClassId == id && a.StudentId== studentid).ToList();

                List<OnlineClassFeedback> feedback = null;

                if(feedbackList!=null)
                {
                    feedback = (from a in feedbackList
                                select new OnlineClassFeedback
                                {
                                    ID = a.ID,
                                    Review = a.Review!=null?a.Review:"No Review",
                                    InsertedDate = a.InsertedDate.Value,
                                    Rating = a.Rating,

                                }).ToList();
                }
                else
                {
                    feedback = null;
                }
               


                adminviewonlineclasses.HomeWorks = res2;
                adminviewonlineclasses.classSyllabusModel = syllabus;
                adminviewonlineclasses.onlineClass = onlineClasses;
                adminviewonlineclasses.onlinestudentattendance = attendance;
                adminviewonlineclasses.Feedbacks = feedback;

                return Request.CreateResponse(HttpStatusCode.OK, new {alldetails= adminviewonlineclasses, msg = "success" });
            }

            catch(Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }

        [HttpPost]
        public HttpResponseMessage OnlineRating(long onlineclassid,int studentid, int rating, string review)
        {
            try
            {
                OnlineClassFeedback studentfeedback = DbContext.OnlineClassFeedbacks.Where(a => a.Active == true && a.OnlineClassId == onlineclassid && a.StudentId == studentid).FirstOrDefault();
                if (studentfeedback == null)
                {
                    studentfeedback = new OnlineClassFeedback();
                    studentfeedback.Active = true;
                    studentfeedback.InsertedDate = DateTime.Now;
                    studentfeedback.InsertedId = studentid;
                    studentfeedback.ModifiedDate = DateTime.Now;
                    studentfeedback.ModifiedId = studentid;
                    studentfeedback.OnlineClassId = onlineclassid;
                    studentfeedback.Rating = rating;
                    studentfeedback.Review = review;
                    studentfeedback.StudentId = studentid;
                    DbContext.OnlineClassFeedbacks.Add(studentfeedback);
                    DbContext.SaveChanges();

                }
                else
                {
                    studentfeedback.ModifiedDate = DateTime.Now;
                    studentfeedback.ModifiedId = studentid;
                    studentfeedback.OnlineClassId = onlineclassid;
                    studentfeedback.Rating = rating;
                    studentfeedback.Review = review;
                    studentfeedback.StudentId = studentid;
                    DbContext.SaveChanges();
                }


                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
    }
}
