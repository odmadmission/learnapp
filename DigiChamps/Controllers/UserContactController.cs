﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class UserContactController : ApiController
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();
        


        [HttpPost]
        public List<UserContact>  UploadContacts(List<UserContact> userContacts)
        {
           
            try
            {
                for(int i=0;i<userContacts.Count;i++)
                {
                    
                   tbl_DC_UserContact con=new tbl_DC_UserContact();
                    con.Name=userContacts[i].Name;
                    if (userContacts[i].Mobile.Length<=30)
                    con.Mobile=userContacts[i].Mobile;

                    con.RegdId=userContacts[i].RegId;
                    con.Inserted_On = DateTime.Now;
                    con.Is_Active = true;

                    con.Is_Delete = false;
                    DbContext.tbl_DC_UserContact.Add(con);
                }
               
                int count=DbContext.SaveChanges();

            }
            catch (Exception ex)
            {
                
            }
            return userContacts;
        }


        public class UserContact
        {
            public string Name { set; get; }
            public string Mobile { set; get; }
            public int? RegId { set; get; }

        }
     

    }
}
