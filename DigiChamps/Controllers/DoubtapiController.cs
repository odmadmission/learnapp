﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;
using System.Threading.Tasks;
using System.Web;
using System.Net.Mail;
using System.IO;
using static DigiChamps.Controllers.DoubtController;

namespace DigiChamps.Controllers
{
    public class DoubtapiController : ApiController
    {
        //
        // GET: /Doubtapi/
        DigiChampsEntities DbContext = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();
        public class classcls
        {
            public int id { get; set; }
            public string name { get; set; }
            public string sts { get; set; }
        }
        public class sectioncls
        {
            public Guid? id { get; set; }
            public string name { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetBoard(int teacherId)
        {
            try
            {
                var assignsubjectlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherId).Select(a => new { a.Board_Id }).ToList().Distinct();
                var getboards = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var res = (from a in assignsubjectlist
                           join b in getboards on a.Board_Id equals b.Board_Id
                           select new classcls
                           {
                               id = a.Board_Id,
                               name = b.Board_Name
                           }).ToList().Distinct();
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = true, boardlist = res });
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = false });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetStatus()
        {
            try
            {
                List<classcls> cls = new List<classcls>();
                cls.Add(new classcls { name = "Open Ticket", sts = "O" });
                cls.Add(new classcls { name = "Reject Ticket", sts = "R" });
                cls.Add(new classcls { name = "Close Ticket", sts = "C" });
                cls.Add(new classcls { name = "Overdue Ticket", sts = "D" });
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = true, statuslist = cls });
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = false });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetClasses(int teacherId, int boardid)
        {
            try
            {
                var assignsubjectlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherId).Select(a => new { a.Class_Id }).ToList().Distinct();
                var getclasses = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Board_Id == boardid).ToList();
                var res = (from a in assignsubjectlist
                           join b in getclasses on a.Class_Id equals b.Class_Id
                           select new classcls
                           {
                               id = a.Class_Id,
                               name = b.Class_Name
                           }).ToList().Distinct();
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = true, classlist = res });
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = false });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetSectionwithsubjects(int teacherId, int classid)
        {
            try
            {
                var assignsubjectlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherId).ToList();
                var getsections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true && a.Class_Id == classid).ToList();
                var sectionlist = (from a in assignsubjectlist.Select(m => new { m.SectionId }).ToList().Distinct()
                                   join b in getsections on a.SectionId equals b.SectionId
                                   select new sectioncls
                                   {
                                       id = a.SectionId,
                                       name = b.SectionName
                                   }).ToList().Distinct();
                var getsubjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Class_Id == classid).ToList();
                var subjectslist = (from a in assignsubjectlist.Select(m => new { m.Subject_Id }).ToList().Distinct()
                                    join b in getsubjects on a.Subject_Id equals b.Subject_Id
                                    select new classcls
                                    {
                                        id = a.Subject_Id,
                                        name = b.Subject
                                    }).ToList().Distinct();
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = true, sectionlist = sectionlist, subjectslist = subjectslist });
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = false });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetChapters(int subjectid)
        {
            try
            {
                var getchapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true && a.Subject_Id == subjectid).ToList();
                var chapterlist = (from b in getchapters
                                   select new classcls
                                   {
                                       id = b.Chapter_Id,
                                       name = b.Chapter
                                   }).ToList().Distinct();

                return Request.CreateResponse(HttpStatusCode.OK, new { sts = true, chapterlist = chapterlist });
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = false });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetDoubts(int classid, Guid sec, int subjectid, int chapterid)
        {
            try
            {
                var doubts = DbContext.View_DC_All_Tickets_Details.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Class_ID == classid && a.Subject_ID == subjectid && a.Chapter_Id == chapterid && a.SectionId == sec).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = true, doubts = doubts });
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = false });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetDoubtsByStatus(int classid, Guid sec, int subjectid, int chapterid, string status)
        {
            try
            {
                var doubts = DbContext.View_DC_All_Tickets_Details.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Class_ID == classid && a.Subject_ID == subjectid && a.Chapter_Id == chapterid && a.SectionId == sec && a.Status == status).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = true, doubts = doubts });
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = false });
            }
        }

        public bool answer_mail(string parameter, string email, string name, string ticket_no)
        {
            var getall = DbContext.SP_DC_Get_maildetails(parameter).FirstOrDefault();
            string eidTo = email;
            string toshow = getall.SMTP_Sender.ToString();
            string from_mail = getall.SMTP_Email;
            string eidFrom = getall.SMTP_User.ToString();
            string password = getall.SMTP_Pwd.ToString();
            string ticket = ticket_no;
            string msgsub = getall.Email_Subject.Replace("{{ticketno}}", ticket);
            string hostname = getall.SMTP_HostName;
            string portname = getall.SMTP_Port.ToString();
            bool ssl_tof = true;
            string msgbody = getall.EmailConf_Body;

            msgbody = getall.EmailConf_Body.ToString().Replace("{{name}}", name).Replace("{{ticketno}}", ticket).Replace("{{date}}", DateTime.Now.ToString());


            MailMessage greetings = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            try
            {
                greetings.From = new MailAddress(from_mail, toshow);//sendername
                greetings.To.Add(eidTo);//to whom
                greetings.IsBodyHtml = true;
                greetings.Priority = MailPriority.High;
                greetings.Body = msgbody;
                greetings.Subject = msgsub;
                smtp.Host = hostname;//host name
                smtp.EnableSsl = ssl_tof;//ssl
                smtp.Port = Convert.ToInt32(portname);//port
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(eidFrom, password);//from(user)//password
                smtp.Send(greetings);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool sendMail_close_reject(string parameter, string email, string name, string ticket_no, string typ, string remark)
        {
            var getall = DbContext.SP_DC_Get_maildetails(parameter).FirstOrDefault();
            string eidTo = email;
            string toshow = getall.SMTP_Sender.ToString();
            string from_mail = getall.SMTP_Email;
            string eidFrom = getall.SMTP_User.ToString();
            string password = getall.SMTP_Pwd.ToString();
            string ticket = ticket_no;
            string msgsub = string.Empty;
            if (typ == "R")
            {
                msgsub = getall.Email_Subject.ToString().Replace("{{ticketno}}", ticket_no).Replace("closed", "Rejected");
            }
            else if (typ == "C")
            {
                msgsub = getall.Email_Subject.ToString().Replace("{{ticketno}}", ticket_no);
            }
            string hostname = getall.SMTP_HostName;
            string portname = getall.SMTP_Port.ToString();
            bool ssl_tof = true;
            string msgbody = getall.EmailConf_Body;
            if (typ == "R")
            {
                msgbody = getall.EmailConf_Body.ToString().Replace("{{name}}", name).Replace("{{ticketno}}", ticket).Replace("{{tickettype}}", "rejected").Replace("{{date}}", DateTime.Now.ToString()).Replace("{{remark}}", remark);
            }
            else if (typ == "C")
            {
                if (remark != "")
                {
                    msgbody = getall.EmailConf_Body.ToString().Replace("{{name}}", name).Replace("{{ticketno}}", ticket).Replace("{{tickettype}}", "closed").Replace("{{date}}", DateTime.Now.ToString()).Replace("{{remark}}", remark);
                }
                else
                {
                    msgbody = getall.EmailConf_Body.ToString().Replace("{{name}}", name).Replace("{{ticketno}}", ticket).Replace("{{tickettype}}", "closed").Replace("{{date}}", DateTime.Now.ToString()).Replace("{{remark}}", "");
                }

            }

            MailMessage greetings = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            try
            {
                greetings.From = new MailAddress(from_mail, toshow);//sendername
                greetings.To.Add(eidTo);//to whom
                greetings.IsBodyHtml = true;
                greetings.Priority = MailPriority.High;
                greetings.Body = msgbody;
                greetings.Subject = msgsub;
                smtp.Host = hostname;//host name
                smtp.EnableSsl = ssl_tof;//ssl
                smtp.Port = Convert.ToInt32(portname);//port
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(eidFrom, password);//from(user)//password
                smtp.Send(greetings);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpPost]
        public HttpResponseMessage AnswerDoubts()
        {
            var httprequest = HttpContext.Current.Request;
            string h_tkid = httprequest.Form["Ticket_ID"].ToString();
            string Answer_Ticket = httprequest.Form["Answer_Doubt"].ToString();
            int replied_byid = Convert.ToInt32(httprequest.Form["Teacher_id"]);
            string h_sname = httprequest.Form["Regd_id"].ToString();
            string h_ticno = httprequest.Form["Ticket_No"].ToString();


            int id = Convert.ToInt32(h_tkid);


            try
            {
                if (h_tkid != "")
                {
                    if (Answer_Ticket.Trim() != "")
                    {
                        var _find_assin_or_not = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (_find_assin_or_not.Teach_ID != null)
                        {


                            tbl_DC_Ticket_Dtl tk_dtl = new tbl_DC_Ticket_Dtl();
                            tk_dtl.Ticket_ID = id;
                            tk_dtl.Answer = Answer_Ticket;
                            tk_dtl.Replied_By = replied_byid;
                            tk_dtl.Replied_Date = DateTime.Now;
                            tk_dtl.Is_Active = true;
                            tk_dtl.Is_Deleted = false;
                            if (httprequest.Files != null && httprequest.Files.Count > 0)
                            {

                                string guid = Guid.NewGuid().ToString();
                                var fileName = Path.GetFileName(httprequest.Files[0].FileName.Replace(httprequest.Files[0].FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/Answerimage/"), fileName);
                                httprequest.Files[0].SaveAs(path);
                                tk_dtl.Answer_Image = fileName.ToString();

                            }
                            DbContext.tbl_DC_Ticket_Dtl.Add(tk_dtl);
                            DbContext.SaveChanges();
                            if (_find_assin_or_not.Status == "D")
                            {
                                _find_assin_or_not.Status = "O";
                                DbContext.SaveChanges();
                            }
                            int student_id_ = Convert.ToInt32(h_sname);

                            var student_mail = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _find_assin_or_not.Student_ID && x.Is_Active == true).FirstOrDefault();
                            //Teacher_sendMail(student_mail.Customer_Name, student_mail.Email, h_ticno);
                            //////  tbl_DC_CoinEarn ob = new tbl_DC_CoinEarn();
                            //////  ob.TypeId = tk_dtl.Ticket_ID;
                            //////  ob.RegdId = student_mail.Regd_ID;
                            //////  ob.CoinType = 3;
                            //////  ob.Active = true;
                            //////  ob.Coins = DbContext.tbl_DC_CoinType.Where(a => a.CoinTypeId == 3).FirstOrDefault().Coins;
                            //////  ob.InsertedOn = DateTime.Now;
                            //////  DbContext.tbl_DC_CoinEarn.Add(ob);
                            //////  DbContext.SaveChanges();


                            ////////  answer_mail("Ticket_answer", student_mail.Email, student_mail.Customer_Name, h_ticno.ToString());
                            if (student_mail != null)
                            {
                                try
                                {
                                    var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _find_assin_or_not.Student_ID)

                                                   select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                                    string body = "DBTANS#{{tktid}}#Doubt Answered#Your teacher just answered your doubt(" + _find_assin_or_not.Ticket_No + ")";
                                    string msg = body.ToString().Replace("{{tktid}}", tk_dtl.Ticket_ID.ToString())
                                       ;

                                    if (pushnot != null)
                                    {
                                        if (pushnot.Device_id != null)
                                        {
                                            var note = new PushNotiStatus("Doubt Answered", msg, pushnot.Device_id);
                                        }
                                    }
                                }
                                catch (Exception e)
                                {


                                }
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", ticketId = h_tkid });

                        }
                        else
                        {
                            tbl_DC_Ticket_Assign _ticket_Assign = new tbl_DC_Ticket_Assign();
                            _ticket_Assign.Ticket_ID = id;
                            _ticket_Assign.Ticket_No = _find_assin_or_not.Ticket_No;
                            _ticket_Assign.Student_ID = _find_assin_or_not.Student_ID;
                            _ticket_Assign.Teach_ID = replied_byid;
                            _ticket_Assign.Assign_Date = DateTime.Now;
                            _ticket_Assign.Is_Close = false;
                            _ticket_Assign.Inserted_By = replied_byid;
                            _ticket_Assign.Inserted_Date = DateTime.Now;
                            _ticket_Assign.Is_Active = true;
                            _ticket_Assign.Is_Deleted = false;
                            DbContext.tbl_DC_Ticket_Assign.Add(_ticket_Assign);
                            DbContext.SaveChanges();
                            tbl_DC_Ticket _ticket = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id).FirstOrDefault();

                            _ticket.Teach_ID = replied_byid;
                            _ticket.Assign_Date = DateTime.Now;
                            DbContext.SaveChanges();
                            #region ticketanswer
                            tbl_DC_Ticket_Dtl tk_dtl = new tbl_DC_Ticket_Dtl();
                            tk_dtl.Ticket_ID = id;
                            tk_dtl.Answer = Answer_Ticket;
                            tk_dtl.Replied_By = replied_byid;
                            tk_dtl.Replied_Date = DateTime.Now;
                            tk_dtl.Is_Active = true;
                            tk_dtl.Is_Deleted = false;
                            if (httprequest.Files.Count > 0)
                            {

                                string guid = Guid.NewGuid().ToString();
                                var fileName = Path.GetFileName(httprequest.Files[0].FileName.Replace(httprequest.Files[0].FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/Answerimage/"), fileName);
                                httprequest.Files[0].SaveAs(path);
                                tk_dtl.Answer_Image = fileName.ToString();

                            }

                            DbContext.tbl_DC_Ticket_Dtl.Add(tk_dtl);
                            DbContext.SaveChanges();
                            if (_find_assin_or_not.Status == "D")
                            {
                                _find_assin_or_not.Status = "O";
                                DbContext.SaveChanges();
                            }
                            int student_id_ = Convert.ToInt32(h_sname);
                            var student_mail = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _find_assin_or_not.Student_ID && x.Is_Active == true).FirstOrDefault();
                            //Teacher_sendMail(student_mail.Customer_Name, student_mail.Email, h_ticno);
                            //////  tbl_DC_CoinEarn ob = new tbl_DC_CoinEarn();
                            //////  ob.TypeId = tk_dtl.Ticket_ID;
                            //////  ob.RegdId = student_mail.Regd_ID;
                            //////  ob.CoinType = 3;
                            //////  ob.Active = true;
                            //////  ob.Coins = DbContext.tbl_DC_CoinType.Where(a => a.CoinTypeId == 3).FirstOrDefault().Coins;
                            //////  ob.InsertedOn = DateTime.Now;
                            //////  DbContext.tbl_DC_CoinEarn.Add(ob);
                            //////  DbContext.SaveChanges();


                            ////////  answer_mail("Ticket_answer", student_mail.Email, student_mail.Customer_Name, h_ticno.ToString());
                            if (student_mail != null)
                            {
                                try
                                {
                                    var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _find_assin_or_not.Student_ID)

                                                   select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                                    string body = "DBTANS#{{tktid}}#Doubt Answered#Your teacher just answered your doubt(" + _find_assin_or_not.Ticket_No + ")";
                                    string msg = body.ToString().Replace("{{tktid}}", tk_dtl.Ticket_ID.ToString())
                                       ;

                                    if (pushnot != null)
                                    {
                                        if (pushnot.Device_id != null)
                                        {
                                            var note = new PushNotiStatus("Doubt Answered", msg, pushnot.Device_id);
                                        }
                                    }
                                }
                                catch (Exception e)
                                {


                                }
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", ticketId = h_tkid });

                            #endregion

                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new { sts = "E", ticketId = h_tkid });//enter answer of the question

                    }
                }

                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "T", ticketId = h_tkid });//Try again


                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "W" + ex.Message, ticketId = h_tkid }); //Something went wrong

            }



        }
        [HttpPost]
        public HttpResponseMessage AnswerReply()
        {
            var httprequest = HttpContext.Current.Request;
            int Ticket_id = Convert.ToInt32(httprequest.Form["Ticket_ID"]);
            int Ticket_answerid = Convert.ToInt32(httprequest.Form["Answer_Id"]);
            int _Teacher_id = Convert.ToInt32(httprequest.Form["Teacher_id"]);
            string msgbody = httprequest.Form["Reply_Message"].ToString();
            string close = httprequest.Form["close"].ToString();
            string remark = httprequest.Form["remark"].ToString();
            string message = string.Empty;

            if (msgbody != "")
            {
                try
                {

                    tbl_DC_Ticket_Thread _ticket_thred = new tbl_DC_Ticket_Thread();

                    _ticket_thred.Ticket_ID = Ticket_id;
                    _ticket_thred.Ticket_Dtl_ID = Ticket_answerid;
                    _ticket_thred.User_Comment = msgbody;
                    _ticket_thred.User_Comment_Date = DateTime.Now;
                    _ticket_thred.User_Id = _Teacher_id;
                    _ticket_thred.Is_Teacher = true;
                    _ticket_thred.Is_Active = true;
                    _ticket_thred.Is_Deleted = false;
                    if (httprequest.Files != null && httprequest.Files.Count > 0)
                    {

                        string guid = Guid.NewGuid().ToString();
                        var fileName = Path.GetFileName(httprequest.Files[0].FileName.Replace(httprequest.Files[0].FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                        var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/Qusetionimages/"), fileName);
                        httprequest.Files[0].SaveAs(path);
                        _ticket_thred.R_image = fileName.ToString();

                    }
                    DbContext.tbl_DC_Ticket_Thread.Add(_ticket_thred);
                    DbContext.SaveChanges();
                    if (close == "on")
                    {
                        tbl_DC_Ticket_Assign _tbl_close = DbContext.tbl_DC_Ticket_Assign.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                        _tbl_close.Is_Close = true;
                        _tbl_close.Remark = remark;
                        _tbl_close.Close_Date = DateTime.Now;
                        _tbl_close.Modified_By = _Teacher_id;
                        _tbl_close.Modified_Date = DateTime.Now;
                        DbContext.SaveChanges();
                        tbl_DC_Ticket _tbl_status = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                        _tbl_status.Status = "C";
                        _tbl_status.Modified_By = _Teacher_id;
                        _tbl_status.Modified_Date = DateTime.Now;
                        DbContext.SaveChanges();
                        var get_student = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _tbl_close.Student_ID).FirstOrDefault();
                        if (get_student != null)
                        {
                            //Close_ticket_mail(_tbl_close.Student_ID.ToString(), _tbl_close.Ticket_No);
                            //   sendMail_close_reject("Ticket_close", get_student.Email.ToString(), get_student.Customer_Name, _tbl_close.Ticket_No.ToString(), "C", remark.ToString());
                        }
                    }
                    tbl_DC_Ticket _tbl_status2 = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();



                    var student_mail = DbContext.tbl_DC_Registration.
                        Where(x => x.Regd_ID == _tbl_status2.Student_ID && x.Is_Active == true).FirstOrDefault();

                    if (student_mail != null)
                    {
                        try
                        {
                            var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student_mail.Regd_ID)

                                           select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                            string body = "DBTRPLY#{{tktid}}#Doubt Reply#Your teacher just replied your doubt(" + _tbl_status2.Ticket_No + ")";
                            string msg = body.ToString().Replace("{{tktid}}", _tbl_status2.Ticket_ID.ToString());

                            if (pushnot != null)
                            {
                                if (pushnot.Device_id != null)
                                {
                                    var note = new PushNotiStatus("Doubt Reply", msg, pushnot.Device_id);
                                }
                            }
                        }
                        catch (Exception)
                        {


                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", id = Ticket_id });

                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "E", id = Ticket_id });//Something went wrong.

                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "R", id = Ticket_id });//Reply should not be empty.                    
            }
        }

        [HttpPost]
        public HttpResponseMessage RejectTicket(string Remark_Reject, string h_tkid)
        {

            try
            {
                if (Remark_Reject.Trim() != "")
                {
                    int id = Convert.ToInt32(h_tkid);
                    tbl_DC_Ticket tkt_rj = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    tkt_rj.Status = "R";
                    tkt_rj.Remark = Remark_Reject;
                    DbContext.SaveChanges();
                    var get_student = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == tkt_rj.Student_ID).FirstOrDefault();
                    if (get_student != null)
                    {
                        //var student_mail = DbContext.tbl_DC_Registration.
                        //Where(x => x.Regd_ID == _tbl_status2.Student_ID && x.Is_Active == true).FirstOrDefault();

                        //if (student_mail != null)
                        //{
                        try
                        {
                            //var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student_mail.Regd_ID)

                            //               select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                            string body = "DBTRJT#{{tktid}}#Doubt Rejected#Your teacher just rejected your doubt (" + tkt_rj.Ticket_No + ")";
                            string msg = body.ToString().Replace("{{tktid}}", tkt_rj.Ticket_ID.ToString());


                            if (get_student.Device_id != null)
                            {
                                var note = new PushNotiStatus("Doubt Rejected", msg, get_student.Device_id);
                            }

                        }
                        catch (Exception)
                        {


                        }
                        //}
                        //}

                        return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", id = h_tkid });
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", id = h_tkid });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "R", id = h_tkid });//Please provide reason of rejection.
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "W", id = h_tkid });//Something went wrong.

            }
        }
        [HttpGet]
        public HttpResponseMessage DeleteThreadComment(int id)
        {
            tbl_DC_Ticket_Thread s = DbContext.tbl_DC_Ticket_Thread.Where(a => a.Comment_ID == id).FirstOrDefault();
            s.Is_Active = false;
            s.Is_Deleted = true;
            s.Modified_Date = today;
            DbContext.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", id = s.Ticket_ID });
        }
        public class TicketDetailscls
        {

            public Doubtcls doubt { get; set; }


            public List<tbl_DC_Ticket_Dtl> all_ticket_answer { get; set; }
            public List<tbl_DC_Ticket_Thread> comments { get; set; }
            public string isclosed { get; set; }
            public tbl_DC_Ticket_Dtl check_answer { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage TicketDetails(int? id)
        {
            try
            {
                if (id != null)
                {

                    var ticket_qsn = DbContext.View_DC_All_Tickets_Details.Where(x => x.Ticket_ID == id).FirstOrDefault();

                    var ClassName = DbContext.tbl_DC_Class.Where(a => a.Class_Id == ticket_qsn.Class_ID).Select(a => a.Class_Name).FirstOrDefault();
                    var Section = DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == ticket_qsn.SectionId).Select(a => a.SectionName).FirstOrDefault();

                    var cData = ClassName + " / " + "Section- " + Section + " / " + ticket_qsn.Subject + " / " + ticket_qsn.Chapter;


                    TicketDetailscls ob = new TicketDetailscls();
                    //ob.student_name = ticket_qsn.Customer_Name;
                    //ob.TClass_name = ticket_qsn.Class_Name;
                    //ob.tquestion = ticket_qsn.Question;
                    //ob.questionImage = ticket_qsn.Question_Image;
                    //ob.classData = cData;
                    //ob.status = ticket_qsn.Status;
                    //ob.TicketId = ticket_qsn.Ticket_ID;
                    //ob.ticketno = ticket_qsn.Ticket_No;
                    //ob.studentId = ticket_qsn.Student_ID;

                    ob.all_ticket_answer = (from a in DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList()
                                            select new tbl_DC_Ticket_Dtl
                                            {
                                                Answer = a.Answer,
                                                Answer_Image = a.Answer_Image != null ? ("/Images/Answerimage/" + a.Answer_Image) : "",
                                                Is_Active = a.Is_Active,
                                                Is_Deleted = a.Is_Deleted,
                                                Replied_By = a.Replied_By,
                                                Replied_Date = a.Replied_Date,
                                                Ticket_Dtls_ID = a.Ticket_Dtls_ID,
                                                Ticket_ID = a.Ticket_ID
                                            }).ToList();
                    ob.comments = (from a in DbContext.tbl_DC_Ticket_Thread.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList()
                                   select new tbl_DC_Ticket_Thread
                                   {
                                       Comment_ID = a.Comment_ID,
                                       Is_Active = a.Is_Active,
                                       Is_Deleted = a.Is_Deleted,
                                       Is_Taecher_Read = a.Is_Taecher_Read,
                                       Is_Teacher = a.Is_Teacher,
                                       Modified_By = a.Modified_By,
                                       Modified_Date = a.Modified_Date,
                                       R_image = a.R_image != null ? ("/Images/Qusetionimages/" + a.R_image) : "",
                                       Ticket_Dtl_ID = a.Ticket_Dtl_ID,
                                       Ticket_ID = a.Ticket_ID,
                                       User_Comment = a.User_Comment,
                                       User_Comment_Date = a.User_Comment_Date,
                                       User_Id = a.User_Id
                                   }).ToList();
                    ob.isclosed = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Status).FirstOrDefault();
                    ob.check_answer = (from a in DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList()
                                       select new tbl_DC_Ticket_Dtl
                                       {
                                           Answer = a.Answer,
                                           Answer_Image = a.Answer_Image != null ? ("/Images/Answerimage/" + a.Answer_Image) : "",
                                           Is_Active = a.Is_Active,
                                           Is_Deleted = a.Is_Deleted,
                                           Replied_By = a.Replied_By,
                                           Replied_Date = a.Replied_Date,
                                           Ticket_Dtls_ID = a.Ticket_Dtls_ID,
                                           Ticket_ID = a.Ticket_ID
                                       }).FirstOrDefault();

                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", ticketdetails = ob });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "N" });//No details
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "W" });//Something went wrong.                   
            }
        }
        public class Ticketcounts
        {
            public int TotalDoubts { get; set; }
            public int AnsweredDoubts { get; set; }
            public int RejectDoubts { get; set; }
            public int PendingDoubts { get; set; }
            public int FirstDoubts { get; set; }
            public int SecondDoubts { get; set; }
        }

        [HttpGet]
        public HttpResponseMessage GetTicketDashboard(int classid, Guid sec, int subjectid)
        {
            try
            {
                var ticket_qsn = DbContext.View_DC_All_Tickets_Details.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Class_ID == classid && a.Subject_ID == subjectid && a.SectionId == sec).ToList();
                var ticket_dtl = DbContext.tbl_DC_Ticket_Dtl.Where(a => a.Is_Active == true).Select(a => new { a.Ticket_Dtls_ID, a.Ticket_ID }).ToList().Distinct();
                var ticket_thread = DbContext.tbl_DC_Ticket_Thread.Where(a => a.Is_Active == true).Select(a => new { a.Ticket_Dtl_ID, a.Comment_ID }).ToList().Distinct();
                Ticketcounts cnt = new Ticketcounts();
                cnt.TotalDoubts = ticket_qsn.ToList().Count();
                cnt.RejectDoubts = ticket_qsn.Where(a => a.Status == "R").ToList().Count();
                cnt.AnsweredDoubts = (from a in ticket_qsn
                                      join b in ticket_dtl on a.Ticket_ID equals b.Ticket_ID
                                      select a).ToList().Count();
                cnt.PendingDoubts = cnt.TotalDoubts - (cnt.AnsweredDoubts + cnt.RejectDoubts);
                cnt.SecondDoubts = (from a in ticket_qsn
                                    join b in ticket_dtl on a.Ticket_ID equals b.Ticket_ID
                                    join c in ticket_thread on b.Ticket_Dtls_ID equals c.Ticket_Dtl_ID
                                    select a).ToList().Count();
                cnt.FirstDoubts = cnt.AnsweredDoubts - cnt.SecondDoubts;
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", ticketdetails = cnt });

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "W" });//Something went wrong.                   
            }
        }



        public class Ticket_Cls
        {
            public int Ticket_ID { get; set; }
            public string Ticket_No { get; set; }
            public Nullable<int> Student_ID { get; set; }
            public string Customer_Name { get; set; }
            public Nullable<int> Board_ID { get; set; }
            public string Board_Name { get; set; }
            public string Teacher_Name { get; set; }
            public Nullable<int> Class_ID { get; set; }
            public string Class_Name { get; set; }
            public Nullable<int> Subject_ID { get; set; }
            public Nullable<int> Teach_ID { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> Inserted_Date { get; set; }
            public Nullable<int> Is_newReply { get; set; }
            public int Is_new_count { get; set; }
        }

        #region New Doubt API
        #region Teacher
        [HttpGet]
        public HttpResponseMessage GetAllStatus()
        {
            try
            {
                List<classcls> cls = new List<classcls>();
                cls.Add(new classcls { name = "Pending", sts = "O" });
                cls.Add(new classcls { name = "Answered", sts = "A" });
                cls.Add(new classcls { name = "Rejected", sts = "R" });
                cls.Add(new classcls { name = "Closed", sts = "C" });
                cls.Add(new classcls { name = "Solved Over a Call", sts = "SOC" });
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = true, statuslist = cls });
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = false });
            }
        }
        public class statuswiseDoubtcls
        {
            public List<Doubtcls> pending { get; set; }
            public List<Doubtcls> answered { get; set; }
            public List<Doubtcls> closed { get; set; }
            public List<Doubtcls> rejected { get; set; }

        }
        public class Doubtcls
        {
            public int subjectId { get; set; }
            public int chapterId { get; set; }
            public bool Answers { get; set; }
            public int RegdId { get; set; }
            public int doubtId { get; set; }
            public string studentName { get; set; }
            public string studentMobile { get; set; }
            public string mobile { get; set; }
            public string zoomId { get; set; }
            public string zoomCode { get; set; }
            public string zoomMailId { get; set; }
            public string zoomPassword { get; set; }
            public string tquestion { get; set; }
            public string questionImage { get; set; }
            public string className { get; set; }
            public string sectionName { get; set; }
            public string subjectName { get; set; }
            public string chapterName { get; set; }
            public string raiseDate { get; set; }
            public string ticketNo { get; set; }
            public string status { get; set; }
            public string statusname { get; set; }
            public string TeacherName { get; set; }
            public string Reason { get; set; }
        }
        public class teacherfiltercls
        {
           public int? board { get; set; }
           public int? cls { get; set; }
           public Guid[] section { get; set; }
           public int? subject { get; set; }
           public int[] chapter { get; set; }
           public string fdt { get; set; }
           public string tdt { get; set; }
           public string studentname { get; set; }
           public int teacherId { get; set; }
           public string status { get; set; }
        }
        [HttpPost]
        public HttpResponseMessage GetDoubtListForTeacher(teacherfiltercls ob)
        {
            try
            {
                var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == ob.teacherId).ToList();
                var boardlistss = assigndet.Select(a => a.Board_Id).ToList();
                var classlist = assigndet.Select(a => a.Class_Id).ToList();
                var sectionlist = assigndet.Select(a => a.SectionId).ToList();
                var subjectlist = assigndet.Select(a => a.Subject_Id).ToList();
                var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var doubts = DbContext.View_DC_All_Tickets_Details.Where(a => a.Is_Active == true && a.Is_Deleted == false && boardlistss.Contains(a.Board_ID.Value) && classlist.Contains(a.Class_ID.Value) && sectionlist.Contains(a.SectionId) && subjectlist.Contains(a.Subject_ID.Value)).ToList();
                var reglist = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var teachers = DbContext.Tbl_Teacher_Zoom_Det.Where(a => a.TeacherId == ob.teacherId).FirstOrDefault();
                if (ob.board != null && ob.board != 0)
                {
                    doubts = doubts.Where(a => a.Board_ID == ob.board).ToList();
                }
                if (ob.cls != null && ob.cls != 0)
                {
                    doubts = doubts.Where(a => a.Class_ID == ob.cls).ToList();
                }

                if (ob.section != null && ob.section.ToList().Count()>0 && ob.section.ToList()[0] != Guid.Empty)
                {
                    doubts = doubts.Where(a => ob.section.Contains(a.SectionId.Value)).ToList();
                }
                if (ob.subject != null && ob.subject != 0)
                {
                    doubts = doubts.Where(a => a.Subject_ID == ob.subject).ToList();
                }
                if (ob.chapter != null && ob.chapter.ToList().Count() > 0 && ob.chapter.ToList()[0] != 0)
                {
                    doubts = doubts.Where(a => ob.chapter.Contains(a.Chapter_Id)).ToList();
                }
                if (ob.fdt != null && ob.tdt != null && ob.fdt != "null" && ob.tdt != "null" && ob.fdt != "" && ob.tdt != "")
                {
                    DateTime fromDate = Convert.ToDateTime(ob.fdt);
                    DateTime toDate = Convert.ToDateTime(ob.tdt);
                    doubts = doubts.Where(a => a.Inserted_Date.Value.Date >= fromDate.Date && a.Inserted_Date.Value.Date <= toDate.Date).ToList();
                }
                if (ob.studentname != null && ob.studentname != "" && ob.studentname != "null")
                {
                    doubts = doubts.Where(a => a.Customer_Name.ToLower().Contains(ob.studentname.ToLower())).ToList();
                }
                var res = (from a in doubts
                           join b in reglist on a.Regd_ID equals b.Regd_ID
                           select new Doubtcls
                           {
                               chapterName = a.Chapter,
                               className = a.Class_Name,
                               raiseDate = a.Inserted_Date.Value.ToString("dd-MM-yyyy"),
                               RegdId = a.Regd_ID,
                               sectionName = sections.Where(m => m.SectionId == a.SectionId).FirstOrDefault().SectionName,
                               status = a.Status,
                               statusname = a.Status == "O" ? (Convert.ToBoolean(a.Answer) == false ? "PENDING" : "ANSWERED") : (a.Status == "C" ? "CLOSED" : "REJECTED"),
                               studentName = a.Customer_Name,
                               subjectName = a.Subject,
                               ticketNo = a.Ticket_No,
                               doubtId = a.Ticket_ID,
                               mobile = b.Mobile,
                               zoomCode = teachers != null ? teachers.Zoom_Code : "",
                               zoomId = teachers != null ? teachers.ZooMId : "",
                               zoomMailId = teachers != null ? teachers.Zoom_MailId : "",
                               zoomPassword = teachers != null ? teachers.Zoom_Password : "",
                               Answers = Convert.ToBoolean(a.Answer),
                               tquestion = a.Question,
                               chapterId=a.Chapter_Id,
                               subjectId=a.Subject_ID.Value,
                               Reason=a.Remark,
                               questionImage = a.Question_Image == null ? "" : "/Images/Qusetionimages/" + a.Question_Image
                           }).ToList();
                if (ob.status != null && ob.status != "" && ob.status != "null")
                {
                    if (ob.status == "O")
                    {
                        res = res.Where(a => a.status == "O" && a.Answers == false).ToList();

                    }
                    else if(ob.status =="A")
                    {
                        res = res.Where(a => a.status == "O" && a.Answers == true).ToList();
                    }
                    else if(ob.status =="R")
                    {
                        res = res.Where(a => a.status == "R").ToList();
                    }
                    else if(ob.status =="C")
                    {
                        res = res.Where(a => a.status == "C").ToList();
                    }
                    else if (ob.status == "SOC")
                    {
                        res = res.Where(a => a.status == "SOC").ToList();
                    }
                }
                //statuswiseDoubtcls statuswise = new statuswiseDoubtcls();
                //statuswise.pending = res.Where(a => a.status == "O" && a.Answers == false).ToList();
                //statuswise.answered = res.Where(a => a.status == "O" && a.Answers == true).ToList();
                //statuswise.closed = res.Where(a => a.status == "C").ToList();
                //statuswise.rejected = res.Where(a => a.status == "R").ToList();


                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", doubts = res.OrderByDescending(a=>a.doubtId).ToList() });

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "W" });//Something went wrong.                   
            }
        }
        [HttpPost]
        public HttpResponseMessage SaveDoubtAnswer()
        {
            var httprequest = HttpContext.Current.Request;
            string h_tkid = httprequest.Form["Ticket_ID"].ToString();
            int Ticket_answerid = Convert.ToInt32(httprequest.Form["Answer_Id"]);
            string Answer_Ticket = httprequest.Form["Answer_Doubt"].ToString();
            int replied_byid = Convert.ToInt32(httprequest.Form["Teacher_id"]);
            string h_sname = httprequest.Form["Regd_id"].ToString();
            string h_ticno = httprequest.Form["Ticket_No"].ToString();
            bool isaudio = Convert.ToBoolean(httprequest.Form["isaudio"].ToString());
            int id = Convert.ToInt32(h_tkid);


            try
            {
                if (Ticket_answerid == 0)
                {
                    
                        var _find_assin_or_not = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (_find_assin_or_not.Teach_ID != null)
                        {


                            tbl_DC_Ticket_Dtl tk_dtl = new tbl_DC_Ticket_Dtl();
                            tk_dtl.Ticket_ID = id;
                            tk_dtl.Answer = Answer_Ticket;
                            tk_dtl.Replied_By = replied_byid;
                            tk_dtl.Replied_Date = DateTime.Now;
                            tk_dtl.Is_Active = true;
                            tk_dtl.Is_Deleted = false;
                            if (httprequest.Files != null && httprequest.Files.Count > 0)
                            {

                                string guid = Guid.NewGuid().ToString();
                                var fileName = Path.GetFileName(httprequest.Files[0].FileName.Replace(httprequest.Files[0].FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/Answerimage/"), fileName);
                                httprequest.Files[0].SaveAs(path);
                                tk_dtl.Answer_Image = fileName.ToString();

                            }
                            tk_dtl.Is_Audio = isaudio;
                            DbContext.tbl_DC_Ticket_Dtl.Add(tk_dtl);
                            DbContext.SaveChanges();
                            if (_find_assin_or_not.Status == "D")
                            {
                                _find_assin_or_not.Status = "O";
                                DbContext.SaveChanges();
                            }
                            int student_id_ = Convert.ToInt32(h_sname);

                            var student_mail = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _find_assin_or_not.Student_ID && x.Is_Active == true).FirstOrDefault();
                            if (student_mail != null)
                            {
                                try
                                {
                                    var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _find_assin_or_not.Student_ID)

                                                   select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                                    string body = "DBTANS#{{tktid}}#Doubt Answered#Your teacher just answered your doubt(" + _find_assin_or_not.Ticket_No + ")";
                                    string msg = body.ToString().Replace("{{tktid}}", tk_dtl.Ticket_ID.ToString())
                                       ;

                                    if (pushnot != null)
                                    {
                                        if (pushnot.Device_id != null)
                                        {
                                            var note = new PushNotiStatus("Doubt Answered", msg, pushnot.Device_id);
                                        }
                                    }
                                }
                                catch (Exception e)
                                {


                                }
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", ticketId = h_tkid });

                        }
                        else
                        {
                            tbl_DC_Ticket_Assign _ticket_Assign = new tbl_DC_Ticket_Assign();
                            _ticket_Assign.Ticket_ID = id;
                            _ticket_Assign.Ticket_No = _find_assin_or_not.Ticket_No;
                            _ticket_Assign.Student_ID = _find_assin_or_not.Student_ID;
                            _ticket_Assign.Teach_ID = replied_byid;
                            _ticket_Assign.Assign_Date = DateTime.Now;
                            _ticket_Assign.Is_Close = false;
                            _ticket_Assign.Inserted_By = replied_byid;
                            _ticket_Assign.Inserted_Date = DateTime.Now;
                            _ticket_Assign.Is_Active = true;
                            _ticket_Assign.Is_Deleted = false;
                            DbContext.tbl_DC_Ticket_Assign.Add(_ticket_Assign);
                            DbContext.SaveChanges();
                            tbl_DC_Ticket _ticket = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id).FirstOrDefault();

                            _ticket.Teach_ID = replied_byid;
                            _ticket.Assign_Date = DateTime.Now;
                            DbContext.SaveChanges();
                            #region ticketanswer
                            tbl_DC_Ticket_Dtl tk_dtl = new tbl_DC_Ticket_Dtl();
                            tk_dtl.Ticket_ID = id;
                            tk_dtl.Answer = Answer_Ticket;
                            tk_dtl.Replied_By = replied_byid;
                            tk_dtl.Replied_Date = DateTime.Now;
                            tk_dtl.Is_Active = true;
                            tk_dtl.Is_Deleted = false;
                            tk_dtl.Is_Audio = isaudio;
                            if (httprequest.Files.Count > 0)
                            {

                                string guid = Guid.NewGuid().ToString();
                                var fileName = Path.GetFileName(httprequest.Files[0].FileName.Replace(httprequest.Files[0].FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/Answerimage/"), fileName);
                                httprequest.Files[0].SaveAs(path);
                                tk_dtl.Answer_Image = fileName.ToString();

                            }

                            DbContext.tbl_DC_Ticket_Dtl.Add(tk_dtl);
                            DbContext.SaveChanges();
                            if (_find_assin_or_not.Status == "D")
                            {
                                _find_assin_or_not.Status = "O";
                                DbContext.SaveChanges();
                            }
                            int student_id_ = Convert.ToInt32(h_sname);
                            var student_mail = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _find_assin_or_not.Student_ID && x.Is_Active == true).FirstOrDefault();
                            if (student_mail != null)
                            {
                                try
                                {
                                    var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _find_assin_or_not.Student_ID)

                                                   select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                                    string body = "DBTANS#{{tktid}}#Doubt Answered#Your teacher just answered your doubt(" + _find_assin_or_not.Ticket_No + ")";
                                    string msg = body.ToString().Replace("{{tktid}}", tk_dtl.Ticket_ID.ToString())
                                       ;

                                    if (pushnot != null)
                                    {
                                        if (pushnot.Device_id != null)
                                        {
                                            var note = new PushNotiStatus("Doubt Answered", msg, pushnot.Device_id);
                                        }
                                    }
                                }
                                catch (Exception e)
                                {


                                }
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", ticketId = h_tkid });

                            #endregion

                        }
                    //}
                    //else
                    //{
                    //    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "E", ticketId = h_tkid });//enter answer of the question

                    //}
                }

                else
                {
                    tbl_DC_Ticket_Thread _ticket_thred = new tbl_DC_Ticket_Thread();

                    _ticket_thred.Ticket_ID = id;
                    _ticket_thred.Ticket_Dtl_ID = Ticket_answerid;
                    _ticket_thred.User_Comment = Answer_Ticket;
                    _ticket_thred.User_Comment_Date = DateTime.Now;
                    _ticket_thred.User_Id = replied_byid;
                    _ticket_thred.Is_Teacher = true;
                    _ticket_thred.Is_Active = true;
                    _ticket_thred.Is_Deleted = false;
                    _ticket_thred.Is_Audio = isaudio;
                    _ticket_thred.Is_Taecher_Read = false;
                    if (httprequest.Files.Count > 0 )
                    {

                        string guid = Guid.NewGuid().ToString();
                        var fileName = Path.GetFileName(httprequest.Files[0].FileName.Replace(httprequest.Files[0].FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                        var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/Qusetionimages/"), fileName);
                        httprequest.Files[0].SaveAs(path);
                        _ticket_thred.R_image = fileName.ToString();

                    }
                    DbContext.tbl_DC_Ticket_Thread.Add(_ticket_thred);
                    DbContext.SaveChanges();

                    tbl_DC_Ticket _tbl_status2 = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id).FirstOrDefault();



                    var student_mail = DbContext.tbl_DC_Registration.
                        Where(x => x.Regd_ID == _tbl_status2.Student_ID && x.Is_Active == true).FirstOrDefault();

                    if (student_mail != null)
                    {
                        try
                        {
                            var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student_mail.Regd_ID)

                                           select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                            string body = "DBTRPLY#{{tktid}}#Doubt Reply#Your teacher just replied your doubt(" + _tbl_status2.Ticket_No + ")";
                            string msg = body.ToString().Replace("{{tktid}}", _tbl_status2.Ticket_ID.ToString());

                            if (pushnot != null)
                            {
                                if (pushnot.Device_id != null)
                                {
                                    var note = new PushNotiStatus("Doubt Reply", msg, pushnot.Device_id);
                                }
                            }
                        }
                        catch (Exception)
                        {


                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", ticketId = id });

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "W",message=ex.Message, ticketId = h_tkid }); //Something went wrong

            }



        }
        [HttpPost]
        public HttpResponseMessage ReplyOnDoubtAnswer()
        {
            var httprequest = HttpContext.Current.Request;
            int Ticket_id = Convert.ToInt32(httprequest.Form["Ticket_ID"]);
            int Ticket_answerid = Convert.ToInt32(httprequest.Form["Answer_Id"]);
            int _Teacher_id = Convert.ToInt32(httprequest.Form["Teacher_id"]);
            string msgbody = httprequest.Form["Reply_Message"].ToString();
            string close = httprequest.Form["close"].ToString();
            string remark = httprequest.Form["remark"].ToString();
            string message = string.Empty;
            bool isaudio = Convert.ToBoolean(httprequest.Form["isaudio"].ToString());

            if (msgbody != "" || (httprequest.Files != null && httprequest.Files.Count > 0))
            {
                try
                {

                    tbl_DC_Ticket_Thread _ticket_thred = new tbl_DC_Ticket_Thread();

                    _ticket_thred.Ticket_ID = Ticket_id;
                    _ticket_thred.Ticket_Dtl_ID = Ticket_answerid;
                    _ticket_thred.User_Comment = msgbody;
                    _ticket_thred.User_Comment_Date = DateTime.Now;
                    _ticket_thred.User_Id = _Teacher_id;
                    _ticket_thred.Is_Teacher = true;
                    _ticket_thred.Is_Active = true;
                    _ticket_thred.Is_Deleted = false;
                    _ticket_thred.Is_Audio = isaudio;
                    if (httprequest.Files != null && httprequest.Files.Count > 0)
                    {

                        string guid = Guid.NewGuid().ToString();
                        var fileName = Path.GetFileName(httprequest.Files[0].FileName.Replace(httprequest.Files[0].FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                        var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/Qusetionimages/"), fileName);
                        httprequest.Files[0].SaveAs(path);
                        _ticket_thred.R_image = fileName.ToString();

                    }
                    DbContext.tbl_DC_Ticket_Thread.Add(_ticket_thred);
                    DbContext.SaveChanges();

                    tbl_DC_Ticket _tbl_status2 = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();



                    var student_mail = DbContext.tbl_DC_Registration.
                        Where(x => x.Regd_ID == _tbl_status2.Student_ID && x.Is_Active == true).FirstOrDefault();

                    if (student_mail != null)
                    {
                        try
                        {
                            var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student_mail.Regd_ID)

                                           select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                            string body = "DBTRPLY#{{tktid}}#Doubt Reply#Your teacher just replied your doubt(" + _tbl_status2.Ticket_No + ")";
                            string msg = body.ToString().Replace("{{tktid}}", _tbl_status2.Ticket_ID.ToString());

                            if (pushnot != null)
                            {
                                if (pushnot.Device_id != null)
                                {
                                    var note = new PushNotiStatus("Doubt Reply", msg, pushnot.Device_id);
                                }
                            }
                        }
                        catch (Exception)
                        {


                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", id = Ticket_id });

                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "E", id = Ticket_id });//Something went wrong.

                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "R", id = Ticket_id });//Reply should not be empty.                    
            }
        }
        [HttpPost]
        public HttpResponseMessage RejectDoubt()
        {

            try
            {

                var httprequest = HttpContext.Current.Request;
                string Remark_Reject = httprequest.Form["Remark_Reject"];
                string h_tkid =httprequest.Form["h_tkid"];
                if (Remark_Reject.Trim() != "")
                {
                    int id = Convert.ToInt32(h_tkid);
                    tbl_DC_Ticket tkt_rj = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    tkt_rj.Status = "R";
                    tkt_rj.Remark = Remark_Reject;
                    DbContext.SaveChanges();
                    var get_student = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == tkt_rj.Student_ID).FirstOrDefault();
                    if (get_student != null)
                    {
                        //var student_mail = DbContext.tbl_DC_Registration.
                        //Where(x => x.Regd_ID == _tbl_status2.Student_ID && x.Is_Active == true).FirstOrDefault();

                        //if (student_mail != null)
                        //{
                        try
                        {
                            //var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student_mail.Regd_ID)

                            //               select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                            string body = "DBTRJT#{{tktid}}#Doubt Rejected#Your teacher just rejected your doubt (" + tkt_rj.Ticket_No + ")";
                            string msg = body.ToString().Replace("{{tktid}}", tkt_rj.Ticket_ID.ToString());


                            if (get_student.Device_id != null)
                            {
                                var note = new PushNotiStatus("Doubt Rejected", msg, get_student.Device_id);
                            }

                        }
                        catch (Exception)
                        {


                        }
                        //}
                        //}

                        return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", id = h_tkid });
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", id = h_tkid });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "R", id = h_tkid });//Please provide reason of rejection.
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "W" });//Something went wrong.

            }
        }
        [HttpPost]
        public HttpResponseMessage CloseDoubt()
        {

            try
            {
                var httprequest = HttpContext.Current.Request;
                int Ticket_id = Convert.ToInt32(httprequest.Form["Ticket_ID"]);
                int _Teacher_id = Convert.ToInt32(httprequest.Form["Teacher_id"]);
               bool close = Convert.ToBoolean(Convert.ToInt32(httprequest.Form["close"].ToString()));
                string remark = httprequest.Form["remark"].ToString();
                if (remark != "")
                {
                    tbl_DC_Ticket_Assign _tbl_close = DbContext.tbl_DC_Ticket_Assign.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                    if (_tbl_close != null)
                    {
                        _tbl_close.Is_Close = true;
                        _tbl_close.Remark = remark;
                        _tbl_close.Close_Date = DateTime.Now;
                        _tbl_close.Modified_By = _Teacher_id;
                        _tbl_close.Modified_Date = DateTime.Now;
                        DbContext.SaveChanges();
                    }
                    else
                    {
                        tbl_DC_Ticket tkt = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                        _tbl_close = new tbl_DC_Ticket_Assign();
                        _tbl_close.Ticket_ID = Ticket_id;
                        _tbl_close.Ticket_No = tkt.Ticket_No;
                        _tbl_close.Student_ID = tkt.Student_ID;
                        _tbl_close.Teach_ID = _Teacher_id;
                        _tbl_close.Assign_Date = DateTime.Now;
                        _tbl_close.Is_Close = true;
                        _tbl_close.Remark = remark;
                        _tbl_close.Close_Date = DateTime.Now;
                        _tbl_close.Inserted_By = _Teacher_id;
                        _tbl_close.Inserted_Date = DateTime.Now;
                        _tbl_close.Modified_By = _Teacher_id;
                        _tbl_close.Modified_Date = DateTime.Now;
                        _tbl_close.Is_Active = true;
                        _tbl_close.Is_Deleted = false;
                        DbContext.tbl_DC_Ticket_Assign.Add(_tbl_close);
                        DbContext.SaveChanges();
                    }
                    tbl_DC_Ticket _tbl_status = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                    _tbl_status.Status = "C";
                    _tbl_status.Remark = remark;
                    _tbl_status.Modified_By = _Teacher_id;
                    _tbl_status.Modified_Date = DateTime.Now;
                    DbContext.SaveChanges();
                    ////var get_student = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _tbl_close.Student_ID).FirstOrDefault();
                    ////if (get_student != null)
                    ////{
                    //Close_ticket_mail(_tbl_close.Student_ID.ToString(), _tbl_close.Ticket_No);
                    //   sendMail_close_reject("Ticket_close", get_student.Email.ToString(), get_student.Customer_Name, _tbl_close.Ticket_No.ToString(), "C", remark.ToString());
                    //}
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", id = Ticket_id });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "R", id = Ticket_id });
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "W",message = ex.Message });//Something went wrong.

            }
        }
        [HttpGet]
        public HttpResponseMessage DeleteReplyComment(int id)
        {
            tbl_DC_Ticket_Thread s = DbContext.tbl_DC_Ticket_Thread.Where(a => a.Comment_ID == id).FirstOrDefault();
            s.Is_Active = false;
            s.Is_Deleted = true;
            s.Modified_Date = today;
            DbContext.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", id = s.Ticket_ID });
        }
        [HttpGet]
        public HttpResponseMessage DoubtDetailsById(int? id)
        {
            try
            {
                if (id != null)
                {

                    var ticket_qsn = DbContext.View_DC_All_Tickets_Details.Where(x => x.Ticket_ID == id).ToList();
                    int reg = ticket_qsn.ToList()[0].Regd_ID;
                    var regd = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Regd_ID == reg).FirstOrDefault();
                    var regddet = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Regd_ID == reg).FirstOrDefault();

                    var Section = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                    int sub= ticket_qsn.ToList()[0].Subject_ID.Value;
                    var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.SectionId == regd.SectionId && a.Board_Id == regddet.Board_ID && a.Class_Id == regddet.Class_ID && a.Subject_Id == sub).FirstOrDefault();
                    var teachers = DbContext.Tbl_Teacher_Zoom_Det.Where(a => a.TeacherId == assigndet.Teacher_ID).FirstOrDefault();
                    var teacherdet = DbContext.Teachers.Where(a => a.Active == 1 && a.TeacherId == assigndet.Teacher_ID).FirstOrDefault();
                    // var cData = ClassName + " / " + "Section- " + Section + " / " + ticket_qsn.Subject + " / " + ticket_qsn.Chapter;

                    var res = (from a in ticket_qsn
                               select new Doubtcls
                               {
                                   chapterName = a.Chapter,
                                   className = a.Class_Name,
                                   raiseDate = a.Inserted_Date.Value.ToString("dd-MM-yyyy"),
                                   RegdId = a.Regd_ID,
                                   sectionName = Section.Where(m => m.SectionId == a.SectionId).FirstOrDefault().SectionName,
                                   status = a.Status,
                                   studentName = a.Customer_Name,
                                   subjectName = a.Subject,                                   
                                   ticketNo = a.Ticket_No,
                                   doubtId = a.Ticket_ID,
                                   mobile = regd.Mobile,
                                   zoomCode =teachers!=null? teachers.Zoom_Code:"",
                                   zoomId = teachers != null ? teachers.ZooMId:"",
                                   zoomMailId = teachers != null ? teachers.Zoom_MailId:"",
                                   zoomPassword = teachers != null ? teachers.Zoom_Password:"",
                                   Answers = Convert.ToBoolean(a.Answer),
                                   tquestion = a.Question,
                                   TeacherName = teacherdet.Name,
                                   chapterId = a.Chapter_Id,
                                   subjectId = a.Subject_ID.Value,
                                   questionImage = a.Question_Image == null ? "" : "/Images/Qusetionimages/" + a.Question_Image
                               }).FirstOrDefault();
                    TicketDetailscls ob = new TicketDetailscls();
                    ob.doubt = res;
                    var tick = DbContext.tbl_DC_Ticket.Where(a => a.Ticket_ID == res.doubtId).FirstOrDefault();
                    List<tbl_DC_Ticket_Thread> threadslist = new List<tbl_DC_Ticket_Thread>();
                    tbl_DC_Ticket_Thread th = new tbl_DC_Ticket_Thread();
                    th.Is_Teacher = false;
                    th.User_Comment = res.tquestion;
                    th.R_image = res.questionImage;
                    th.Is_Active = true;
                    th.Is_Deleted = false;
                    th.User_Id = res.RegdId;
                    th.User_Comment_Date = ticket_qsn.FirstOrDefault().Inserted_Date;
                    th.Ticket_Dtl_ID = 0;
                    th.Ticket_ID = res.doubtId;
                    th.Is_Audio = tick.Is_Audio;
                    th.Is_Taecher_Read = true;
                    th.Comment_ID = 0;
                    threadslist.Add(th);
                    var dtl = (from a in DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList()
                               select new tbl_DC_Ticket_Thread
                               {
                                   Is_Teacher = true,
                                   User_Comment = a.Answer,
                                   R_image = a.Answer_Image != null ? ("/Images/Answerimage/" + a.Answer_Image) : "",
                                   Is_Active = a.Is_Active,
                                   Is_Deleted = a.Is_Deleted,
                                   User_Id = a.Replied_By,
                                   User_Comment_Date = a.Replied_Date,
                                   Ticket_Dtl_ID = (a.Ticket_Dtls_ID) as Nullable<int>,
                                   Ticket_ID = a.Ticket_ID,
                                   Is_Audio = a.Is_Audio,
                                   Is_Taecher_Read = true,
                                   Comment_ID=0
                               }).ToList();
                    threadslist.AddRange(dtl);
                    var comments = (from a in DbContext.tbl_DC_Ticket_Thread.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList()
                                    select new tbl_DC_Ticket_Thread
                                    {
                                        Comment_ID = a.Comment_ID,
                                        Is_Active = a.Is_Active,
                                        Is_Deleted = a.Is_Deleted,
                                        Is_Taecher_Read = a.Is_Taecher_Read,
                                        Is_Teacher = a.Is_Teacher,
                                        Modified_By = a.Modified_By,
                                        Modified_Date = a.Modified_Date,
                                        R_image = a.R_image != null ? ("/Images/Qusetionimages/" + a.R_image) : "",
                                        Ticket_Dtl_ID = a.Ticket_Dtl_ID,
                                        Ticket_ID = a.Ticket_ID,
                                        User_Comment = a.User_Comment,
                                        User_Comment_Date = a.User_Comment_Date,
                                        User_Id = a.User_Id,
                                        Is_Audio = a.Is_Audio
                                    }).ToList();
                    threadslist.AddRange(comments);
                    ob.comments = threadslist;
                    ob.isclosed = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Status).FirstOrDefault();
                    ob.check_answer = (from a in DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList()
                                       select new tbl_DC_Ticket_Dtl
                                       {
                                           Answer = a.Answer,
                                           Answer_Image = a.Answer_Image != null ? ("/Images/Answerimage/" + a.Answer_Image) : "",
                                           Is_Active = a.Is_Active,
                                           Is_Deleted = a.Is_Deleted,
                                           Replied_By = a.Replied_By,
                                           Replied_Date = a.Replied_Date,
                                           Ticket_Dtls_ID = a.Ticket_Dtls_ID,
                                           Ticket_ID = a.Ticket_ID,
                                           Is_Audio = a.Is_Audio
                                       }).FirstOrDefault();

                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", ticketdetails = ob });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "N" });//No details
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "W",message=ex.Message });//Something went wrong.                   
            }
        }

        [HttpPost]
        public HttpResponseMessage SolveDoubt()
        {

            try
            {
                var httprequest = HttpContext.Current.Request;
                int Ticket_id = Convert.ToInt32(httprequest.Form["Ticket_ID"]);
                int _Teacher_id = Convert.ToInt32(httprequest.Form["Teacher_id"]);
                bool close = Convert.ToBoolean(Convert.ToInt32(httprequest.Form["close"].ToString()));
               
                if (_Teacher_id != 0)
                {
                    tbl_DC_Ticket_Assign _tbl_close = DbContext.tbl_DC_Ticket_Assign.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                    if (_tbl_close != null)
                    {
                        _tbl_close.Is_Close = true;                       
                        _tbl_close.Close_Date = DateTime.Now;
                        _tbl_close.Modified_By = _Teacher_id;
                        _tbl_close.Modified_Date = DateTime.Now;
                        DbContext.SaveChanges();
                    }
                    else
                    {
                        tbl_DC_Ticket tkt = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                        _tbl_close = new tbl_DC_Ticket_Assign();
                        _tbl_close.Ticket_ID = Ticket_id;
                        _tbl_close.Ticket_No = tkt.Ticket_No;
                        _tbl_close.Student_ID = tkt.Student_ID;
                        _tbl_close.Teach_ID = _Teacher_id;
                        _tbl_close.Assign_Date = DateTime.Now;
                        _tbl_close.Is_Close = true;
                        _tbl_close.Remark = "";
                        _tbl_close.Close_Date = DateTime.Now;
                        _tbl_close.Inserted_By = _Teacher_id;
                        _tbl_close.Inserted_Date = DateTime.Now;
                        _tbl_close.Modified_By = _Teacher_id;
                        _tbl_close.Modified_Date = DateTime.Now;
                        _tbl_close.Is_Active = true;
                        _tbl_close.Is_Deleted = false;
                        DbContext.tbl_DC_Ticket_Assign.Add(_tbl_close);
                        DbContext.SaveChanges();
                    }
                    tbl_DC_Ticket _tbl_status = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                    _tbl_status.Status = "SOC";                  
                    _tbl_status.Modified_By = _Teacher_id;
                    _tbl_status.Modified_Date = DateTime.Now;
                    DbContext.SaveChanges();
                    ////var get_student = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _tbl_close.Student_ID).FirstOrDefault();
                    ////if (get_student != null)
                    ////{
                    //Close_ticket_mail(_tbl_close.Student_ID.ToString(), _tbl_close.Ticket_No);
                    //   sendMail_close_reject("Ticket_close", get_student.Email.ToString(), get_student.Customer_Name, _tbl_close.Ticket_No.ToString(), "C", remark.ToString());
                    //}
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", id = Ticket_id });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { sts = "R", id = Ticket_id });
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "W", message = ex.Message });//Something went wrong.

            }
        }
        #endregion
        #region Student
        public class studentfiltercls
        {
           public int? subject { get; set; }
           public int? chapter { get; set; }
           public string fdt { get; set; }
           public string tdt { get; set; }
           public int regdId { get; set; }
           public string status { get; set; }
        }
        [HttpPost]
        public HttpResponseMessage GetDoubtListForStudent(studentfiltercls ob)
        {
            try
            {
                var regd = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Regd_ID == ob.regdId).FirstOrDefault();
                var regddet = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Regd_ID == ob.regdId).FirstOrDefault();

                var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var doubts = DbContext.View_DC_All_Tickets_Details.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Regd_ID == ob.regdId).ToList();
                var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.SectionId == regd.SectionId && a.Board_Id == regddet.Board_ID && a.Class_Id == regddet.Class_ID).ToList();
                var teacherszoom = DbContext.Tbl_Teacher_Zoom_Det.Where(a => a.Is_Active == true).ToList();

                if (ob.subject != null && ob.subject != 0)
                {
                    doubts = doubts.Where(a => a.Subject_ID == ob.subject).ToList();
                }
                if (ob.chapter != null && ob.chapter != 0)
                {
                    doubts = doubts.Where(a => a.Chapter_Id == ob.chapter).ToList();
                }
                if (ob.fdt != null && ob.tdt != null && ob.fdt != "null" && ob.tdt != "null" && ob.fdt != "" && ob.tdt != "")
                {
                    DateTime fromDate = Convert.ToDateTime(ob.fdt);
                    DateTime toDate = Convert.ToDateTime(ob.tdt);
                    doubts = doubts.Where(a => a.Inserted_Date.Value.Date >= fromDate.Date && a.Inserted_Date.Value.Date <= toDate.Date).ToList();
                }

                var res = (from a in doubts
                           select new Doubtcls
                           {
                               chapterName = a.Chapter,
                               className = a.Class_Name,
                               raiseDate = a.Inserted_Date.Value.ToString("dd-MM-yyyy"),
                               RegdId = a.Regd_ID,
                               sectionName = sections.Where(m => m.SectionId == a.SectionId).FirstOrDefault().SectionName,
                               status = a.Status,
                               studentName = a.Customer_Name,
                               subjectName = a.Subject,
                               ticketNo = a.Ticket_No,
                               doubtId = a.Ticket_ID,
                               mobile = regd.Mobile,
                               statusname = a.Status == "O" ? (Convert.ToBoolean(a.Answer) == false ? "PENDING" : "ANSWERED") : (a.Status == "C" ? "CLOSED" : "REJECTED"),
                               zoomCode = (assigndet.Count()>0 && a.Teach_ID!=null)? (teacherszoom.Where(s => s.Is_Active == true && s.TeacherId == assigndet.Where(m => m.Subject_Id == a.Subject_ID).FirstOrDefault().Teacher_ID).FirstOrDefault()==null?"": teacherszoom.Where(s => s.Is_Active == true && s.TeacherId == assigndet.Where(m => m.Subject_Id == a.Subject_ID).FirstOrDefault().Teacher_ID).FirstOrDefault().Zoom_Code):"",
                               zoomId = (assigndet.Count() > 0 && a.Teach_ID != null) ? (teacherszoom.Where(s => s.Is_Active == true && s.TeacherId == assigndet.Where(m => m.Subject_Id == a.Subject_ID).FirstOrDefault().Teacher_ID).FirstOrDefault() == null ? "" : teacherszoom.Where(s => s.Is_Active == true && s.TeacherId == assigndet.Where(m => m.Subject_Id == a.Subject_ID).FirstOrDefault().Teacher_ID).FirstOrDefault().ZooMId):"",
                               zoomMailId = (assigndet.Count() > 0 && a.Teach_ID != null) ? (teacherszoom.Where(s => s.Is_Active == true && s.TeacherId == assigndet.Where(m => m.Subject_Id == a.Subject_ID).FirstOrDefault().Teacher_ID).FirstOrDefault() == null ? "" : teacherszoom.Where(s => s.Is_Active == true && s.TeacherId == assigndet.Where(m => m.Subject_Id == a.Subject_ID).FirstOrDefault().Teacher_ID).FirstOrDefault().Zoom_MailId):"",
                               zoomPassword = (assigndet.Count() > 0 && a.Teach_ID != null) ? (teacherszoom.Where(s => s.Is_Active == true && s.TeacherId == assigndet.Where(m => m.Subject_Id == a.Subject_ID).FirstOrDefault().Teacher_ID).FirstOrDefault() == null ? "" : teacherszoom.Where(s => s.Is_Active == true && s.TeacherId == assigndet.Where(m => m.Subject_Id == a.Subject_ID).FirstOrDefault().Teacher_ID).FirstOrDefault().Zoom_Password):"",
                               Answers = Convert.ToBoolean(a.Answer),
                               tquestion = a.Question,
                               chapterId = a.Chapter_Id,
                               subjectId = a.Subject_ID.Value,
                               Reason = a.Remark,
                               questionImage = a.Question_Image == null ? "" : "/Images/Qusetionimages/" + a.Question_Image
                           }).ToList();
                if (ob.status != null && ob.status != "" && ob.status != "null")
                {
                    if (ob.status == "O")
                    {
                        res = res.Where(a => a.status == "O" && a.Answers == false).ToList();

                    }
                    else if (ob.status == "A")
                    {
                        res = res.Where(a => a.status == "O" && a.Answers == true).ToList();
                    }
                    else if (ob.status == "R")
                    {
                        res = res.Where(a => a.status == "R").ToList();
                    }
                    else if (ob.status == "C")
                    {
                        res = res.Where(a => a.status == "C").ToList();
                    }
                    else if (ob.status == "SOC")
                    {
                        res = res.Where(a => a.status == "SOC").ToList();
                    }
                }
                //statuswiseDoubtcls statuswise = new statuswiseDoubtcls();
                //statuswise.pending = res.Where(a => a.status == "O" && a.Answers == false).ToList();
                //statuswise.answered = res.Where(a => a.status == "O" && a.Answers == true).ToList();
                //statuswise.closed = res.Where(a => a.status == "C").ToList();
                //statuswise.rejected = res.Where(a => a.status == "R").ToList();


                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", doubts = res.OrderByDescending(a => a.doubtId).ToList() });

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "W" });//Something went wrong.                   
            }
        }
        [HttpPost]
        public HttpResponseMessage SaveDoubtDetails()
        {
            try
            {
                var httprequest = HttpContext.Current.Request;
                int? Subject_ID = Convert.ToInt32(httprequest.Form["Subject_ID"]);
                int? Chapter_ID = Convert.ToInt32(httprequest.Form["Chapter_ID"]);
                int? _board_id = Convert.ToInt32(httprequest.Form["Board_id"]);
                string msgbody = Convert.ToString(httprequest.Form["msgbody"]);
                int? _class_id = Convert.ToInt32(httprequest.Form["Class_id"]);
                int? _student_id = Convert.ToInt32(httprequest.Form["Regd_ID"]);
                int? Ticket_id = Convert.ToInt32(httprequest.Form["Ticket_id"]);
                //int? Answer_id = Convert.ToInt32(httprequest.Form["Answer_id"]);              
                bool isaudio = Convert.ToBoolean(httprequest.Form["isaudio"].ToString());
                if (Ticket_id == 0)
                {
                    var _student_details = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _student_id).FirstOrDefault();

                    var ticket_autoid = DbContext.SP_DC_Generate_Ticket_ID().FirstOrDefault() + _student_id;
                    tbl_DC_Ticket _ticket = new tbl_DC_Ticket();
                    _ticket.Ticket_No = Convert.ToString(ticket_autoid);
                    _ticket.Student_ID = _student_id;
                    _ticket.Board_ID = _board_id;
                    _ticket.Class_ID = _class_id;
                    _ticket.Subject_ID = Subject_ID;
                    _ticket.Chapter_ID = Chapter_ID;
                    _ticket.Question = msgbody;
                    _ticket.Is_Audio = isaudio;
                    string guid = Guid.NewGuid().ToString();
                    //var docfile = new List<string>(); 
                    if (httprequest.Files.Count > 0)
                    {
                        foreach (string files in httprequest.Files)
                        {
                            var postedfile = httprequest.Files[files];
                            var path = HttpContext.Current.Server.MapPath("~/Images/Qusetionimages/" + guid + postedfile.FileName);
                            postedfile.SaveAs(path);
                            //docfile.Add(path);
                            _ticket.Question_Image = guid + postedfile.FileName;
                        }
                    }
                    _ticket.Inserted_Date = today;
                    _ticket.Inserted_By = _student_id;
                    _ticket.Status = "O";
                    _ticket.Is_Active = true;
                    _ticket.Is_Deleted = false;
                    DbContext.tbl_DC_Ticket.Add(_ticket);
                    DbContext.SaveChanges();
                    //sendMail_ticketgenerate("Ticket_Generate", _student_details.Email, _student_details.Customer_Name, ticket_autoid.ToString());


                    tbl_DC_Registration reg = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == _student_id).FirstOrDefault();
                    if (reg != null)
                    {

                        tbl_DC_AssignSubjectToTeacher teacherid = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Board_Id == _board_id.Value &&
                            a.Class_Id == _class_id.Value && a.SectionId == reg.SectionId && a.Subject_Id == Subject_ID).FirstOrDefault();
                        if (teacherid != null)
                        {
                            try
                            {
                                var pushnot = (from c in DbContext.Teachers.Where(x => x.TeacherId == teacherid.Teacher_ID)

                                               select new { c.TeacherId, c.fcmId }).FirstOrDefault();
                                if (pushnot != null && pushnot.fcmId != null)
                                {



                                    String Board = DbContext.tbl_DC_Board.Where(a => a.Board_Id == 1).Select(a => a.Board_Name).FirstOrDefault();
                                    String Class = DbContext.tbl_DC_Class.Where(a => a.Class_Id == _ticket.Class_ID).Select(a => a.Class_Name).FirstOrDefault();
                                    String Section = DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == reg.SectionId).Select(a => a.SectionName).FirstOrDefault();
                                    String Subject = DbContext.tbl_DC_Subject.Where(a => a.Subject_Id == Subject_ID).Select(a => a.Subject).FirstOrDefault();
                                    String Chapter = DbContext.tbl_DC_Chapter.Where(a => a.Chapter_Id == Chapter_ID).Select(a => a.Chapter).FirstOrDefault();

                                    String tvData = "Class - " + Class + " / Section - " + Section + " / " + Subject + " / " + Chapter;

                                    string body = "DBTNEW#{{tktid}}#New Doubt#{{name}} has raised new doubt (" + _ticket.Ticket_No.ToString() + ")";
                                    string msg = body.ToString().Replace("{{name}}", reg.Customer_Name).
                                        Replace("{{tktid}}", _ticket.Ticket_ID.ToString());

                                    var note = new PushNotiStatus("New Doubt", msg, pushnot.fcmId);
                                }


                            }
                            catch (Exception e)
                            {


                            }
                        }
                    }

                    var obj = new Doubtsuccess
                    {
                        success = new message_doubt
                        {
                            Message = "S",
                            Ticket_id = _ticket.Ticket_ID
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);

                }
                else
                {
                    tbl_DC_Ticket_Dtl dtl = DbContext.tbl_DC_Ticket_Dtl.Where(a => a.Ticket_ID == Ticket_id && a.Is_Active == true).FirstOrDefault();

                    tbl_DC_Ticket_Thread _ticket_thred = new tbl_DC_Ticket_Thread();

                    _ticket_thred.Ticket_ID = Ticket_id;
                    _ticket_thred.Ticket_Dtl_ID =dtl==null?0: dtl.Ticket_Dtls_ID;
                    _ticket_thred.User_Comment = msgbody;
                    _ticket_thred.User_Comment_Date = today;
                    _ticket_thred.User_Id = _student_id;
                    _ticket_thred.Is_Taecher_Read = false;
                    _ticket_thred.Is_Teacher = false;
                    _ticket_thred.Is_Audio = isaudio;
                    _ticket_thred.Modified_By = _student_id.ToString();
                    _ticket_thred.Modified_Date = DateTime.Now;
                    string guid = Guid.NewGuid().ToString();

                    if (httprequest.Files.Count > 0)
                    {
                        foreach (string files in httprequest.Files)
                        {
                            var postedfile = httprequest.Files[files];
                            var path = HttpContext.Current.Server.MapPath("~/Images/Qusetionimages/" + guid + postedfile.FileName);
                            postedfile.SaveAs(path);
                            //docfile.Add(path);
                            _ticket_thred.R_image = guid + postedfile.FileName;
                        }
                    }
                    _ticket_thred.Is_Active = true;
                    _ticket_thred.Is_Deleted = false;
                    DbContext.tbl_DC_Ticket_Thread.Add(_ticket_thred);
                    DbContext.SaveChanges();

                    tbl_DC_Ticket ticket = DbContext.tbl_DC_Ticket.Where(a => a.Ticket_ID == _ticket_thred.Ticket_ID).FirstOrDefault();
                    tbl_DC_Registration reg = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == _student_id).FirstOrDefault();

                    var student_mail = DbContext.Teachers.
                           Where(x => x.TeacherId == ticket.Teach_ID).FirstOrDefault();

                    if (student_mail != null && student_mail.fcmId != null)
                    {
                        try
                        {




                            String Board = DbContext.tbl_DC_Board.Where(a => a.Board_Id == 1).Select(a => a.Board_Name).FirstOrDefault();
                            String Class = DbContext.tbl_DC_Class.Where(a => a.Class_Id == ticket.Class_ID).Select(a => a.Class_Name).FirstOrDefault();
                            String Section = DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == reg.SectionId).Select(a => a.SectionName).FirstOrDefault();
                            String Subject = DbContext.tbl_DC_Subject.Where(a => a.Subject_Id == ticket.Subject_ID).Select(a => a.Subject).FirstOrDefault();
                            String Chapter = DbContext.tbl_DC_Chapter.Where(a => a.Chapter_Id == ticket.Chapter_ID).Select(a => a.Chapter).FirstOrDefault();

                            String tvData = "Class - " + Class + " / Section - " + Section + " / " + Subject + " / " + Chapter;

                            //string body = "DBTRPLYSTD#{{tktid}}#{{tktno}}#{{stdid}}#{{ques}}#{{quesimage}}#New Doubt#{{name}}#{{name}} has been replied doubt (" + ticket.Ticket_No.ToString() + ")" + "#" + tvData;
                            //string msg = body.ToString().Replace("{{name}}", reg.Customer_Name).Replace("{{stdid}}", reg.Regd_ID.ToString()).
                            //    Replace("{{tktid}}",ticket.Ticket_ID.ToString()).Replace("{{tktno}}", ticket.Ticket_No.ToString()).Replace("{{ques}}", ticket.Question.ToString()).Replace("{{quesimage}}", ticket.Question_Image);

                            string body = "DBTRPLYSTD#{{tktid}}#Doubt Reply#{{name}} just replied for the doubt (" + ticket.Ticket_No.ToString() + ")";
                            string msg = body.ToString().Replace("{{tktid}}", ticket.Ticket_ID.ToString()).Replace("{{name}}", reg.Customer_Name);


                            var note = new PushNotiStatus("Doubt Reply", msg, student_mail.fcmId);


                        }
                        catch (Exception)
                        {


                        }
                    }


                    var obj = new Doubtsuccess
                    {
                        success = new message_doubt
                        {
                            Message = "S",
                            Ticket_id = ticket.Ticket_ID
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            catch (Exception e)
            {
                var obj = new Doubtsuccess
                {
                    success = new message_doubt
                    {
                        Message = "E",
                        Ticket_id = 0,
                       
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }
        public class TicketcountsModel
        {
            public int TotalDoubts { get; set; }
            public int AnsweredDoubts { get; set; }
            public int RejectDoubts { get; set; }
            public int PendingDoubts { get; set; }
            public int ClosedCount { get; set; }
            public int SlovedCount { get; set; }
        }

        public class doubtcountfiltercls
        {
            public int teacherid { get; set; }
            public int? classid { get; set; }
            public Guid? sec { get; set; }
            public int? subjectid { get; set; }
            public int? chapterid { get; set; }

        }

        [HttpPost]
        public HttpResponseMessage GetAllCountTicketDashboard(doubtcountfiltercls dbt)
        {
            try
            {

                var doubts = DbContext.View_DC_All_Tickets_Details.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == dbt.teacherid).ToList();
                var boardlistss = assigndet.Select(a => a.Board_Id).ToList();
                var classlist = assigndet.Select(a => a.Class_Id).ToList();
                var sectionlist = assigndet.Select(a => a.SectionId).ToList();
                var subjectlist = assigndet.Select(a => a.Subject_Id).ToList();
                doubts = doubts.Where(a => boardlistss.Contains(a.Board_ID.Value) && classlist.Contains(a.Class_ID.Value) && sectionlist.Contains(a.SectionId) && subjectlist.Contains(a.Subject_ID.Value)).ToList();
                var ticket_dtl = DbContext.tbl_DC_Ticket_Dtl.Where(a => a.Is_Active == true).Select(a => new { a.Ticket_Dtls_ID, a.Ticket_ID }).ToList().Distinct();
                var ticket_thread = DbContext.tbl_DC_Ticket_Thread.Where(a => a.Is_Active == true).Select(a => new { a.Ticket_Dtl_ID, a.Comment_ID }).ToList().Distinct();

                if (dbt.classid != null && dbt.classid != 0)
                {
                    doubts = doubts.Where(a => a.Class_ID == dbt.classid).ToList();
                }
                if (dbt.sec != null && dbt.sec != Guid.Empty)
                {
                    doubts = doubts.Where(a => a.SectionId == dbt.sec).ToList();
                }
                if (dbt.subjectid != null && dbt.subjectid != 0)
                {
                    doubts = doubts.Where(a => a.Subject_ID == dbt.subjectid).ToList();
                }
                if (dbt.chapterid != null && dbt.chapterid != 0)
                {
                    doubts = doubts.Where(a => a.Chapter_Id == dbt.chapterid).ToList();
                }
                TicketcountsModel cnt = new TicketcountsModel();
                cnt.TotalDoubts = doubts.ToList().Count();
                cnt.RejectDoubts = doubts.Where(a => a.Status == "R").ToList().Count();
                cnt.AnsweredDoubts = (from a in doubts.Where(c=>c.Status=="O")
                                      join b in ticket_dtl on a.Ticket_ID equals b.Ticket_ID
                                      select a).ToList().Count();
                cnt.ClosedCount = doubts.Where(a => a.Status == "C").ToList().Count();
                cnt.SlovedCount = doubts.Where(a => a.Status == "SOC").ToList().Count();
                cnt.PendingDoubts = cnt.TotalDoubts - (cnt.AnsweredDoubts + cnt.RejectDoubts + cnt.ClosedCount + cnt.SlovedCount);


                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "S", ticketdetails = cnt });

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { sts = "W" });//Something went wrong.                   
            }
        }
        #endregion
        #endregion

    }
}
