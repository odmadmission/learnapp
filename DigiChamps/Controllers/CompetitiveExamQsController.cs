﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class CompetitiveExamQsController : ApiController
    {
        DigiChampsEntities db = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public class ErrorResult
        {
            public ErrorResponse Error { get; set; }
        }

        public class ErrorResponse
        {
            public string Message { get; set; }
        }

        public class SuccessResponse
        {
            public string Message { get; set; }
        }
       
        public class SuccessResult
        {
            public SuccessResponse Success { get; set; }
        }
        public class CompetitiveExamQs
        {
            public int? CompetitiveExamQs_ID { get; set; }
            public string CompetitiveExamQs_PDFName { get; set; }
            //public string BoardName { get; set; }
            //public string ClassName { get; set; }
            public string PDF_UploadPath { get; set; }
            //public string Descriptions { get; set; }
            public string PDFOnline { get; set; }
            public string PDFBeta { get; set; }
        }
        public class CompetitiveExamQs_List
        {
            public List<CompetitiveExamQs> list { get; set; }
        }
        public class success_CompetitiveExamQs
        {
            public CompetitiveExamQs_List Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }
        public class Boardkey
        {
            public int boardid { get; set; }
        }
        public class Classkey
        {
            public int classid { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetCompetitiveExamQs(int id, int eid)
        {
            int boardid = id;
            int classid = eid;
            var getPaper = db.tbl_DC_CompetitiveExamQs.Where(x => x.Is_Active == true && x.Is_Delete == false && x.BoardID == boardid && x.ClassID == classid).ToList();
            try
            {
                var obj = new success_CompetitiveExamQs
                {
                    Success = new CompetitiveExamQs_List
                    {
                        list = (from c in getPaper
                                select new CompetitiveExamQs
                                {
                                    CompetitiveExamQs_ID = c.CompetitiveExamQs_ID,
                                    CompetitiveExamQs_PDFName = c.CompetitiveExamQs_PDFName,
                                    //BoardName = db.tbl_DC_Board.Where(x => x.Board_Id == c.BoardID).Select(x => x.Board_Name).FirstOrDefault(),
                                    //ClassName = db.tbl_DC_Class.Where(x => x.Class_Id == c.ClassID).Select(x => x.Class_Name).FirstOrDefault(),
                                    PDF_UploadPath = c.PDF_UploadPath,
                                    PDFOnline = "https://learn.odmps.org/Module/PDF/" + c.PDF_UploadPath + "",
                                    PDFBeta = "http://beta.thedigichamps.com/Module/PDF/" + c.PDF_UploadPath + "",
                                    //Descriptions = c.Descriptions
                                }).ToList(),
                    }
                };

                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }
        [HttpPost]
        public HttpResponseMessage AddCompetitiveExamQs([FromBody] CompetitiveExamQs obj)
        {
            try
            {
                if (obj.CompetitiveExamQs_ID != null)
                {
                    var data = db.tbl_DC_CompetitiveExamQs.Where(x => x.CompetitiveExamQs_ID == obj.CompetitiveExamQs_ID).FirstOrDefault();
                    if (data != null)
                    {
                        data.CompetitiveExamQs_PDFName = obj.CompetitiveExamQs_PDFName;
                        //data.BoardName = obj.BoardName;
                        //data.ClassName = obj.ClassName;
                        data.PDF_UploadPath = obj.PDF_UploadPath;
                        //data.Descriptions = obj.Descriptions;
                        data.Modified_By = Convert.ToString(obj.CompetitiveExamQs_ID);
                        data.Modified_Date = today;

                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                        var result = new SuccessResult { Success = new SuccessResponse { Message = "Previous Year Paper PDF Uploaded Sucessfully" } };
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        tbl_DC_CompetitiveExamQs tr = new Models.tbl_DC_CompetitiveExamQs();
                        tr.CompetitiveExamQs_PDFName = obj.CompetitiveExamQs_PDFName;
                        //tr.BoardName = obj.BoardName;
                        //tr.ClassName = obj.ClassName;
                        tr.PDF_UploadPath = obj.PDF_UploadPath;
                        //tr.Descriptions = obj.Descriptions;
                        tr.Inserted_On = today;
                        tr.Inserted_By = Convert.ToString(obj.CompetitiveExamQs_ID);
                        tr.Is_Active = true;
                        tr.Is_Delete = false;
                        db.tbl_DC_CompetitiveExamQs.Add(tr);
                        db.SaveChanges();
                        var result = new SuccessResult { Success = new SuccessResponse { Message = "Previous Year Paper PDF Uploaded Sucessfully" } };
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                }
            }
            catch (Exception)
            {
                var result = new ErrorResult { Error = new ErrorResponse { Message = " Something went wrong." } };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}