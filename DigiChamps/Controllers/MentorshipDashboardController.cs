﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
//using System.Web.Http;
using System.Web.Mvc;
using DigiChamps.Models;
using System.Data;


namespace DigiChamps.Controllers
{
    public class MentorshipDashboardController : Controller
    {        
        DigiChampsEntities ob = new DigiChampsEntities();
        public ActionResult Index()
        {

            return RedirectToAction("Login");

        }
        public ActionResult Logout()
        {
            Session["MentorLogin"] = null;
            Session.Abandon();

            return RedirectToAction("Login");
        }
        [HttpGet]
        public ActionResult Login()
        {
            if (Session["MentorLogin"] != null)
            {

                return RedirectToAction("Dashboard");


            }
            else
            {
                return View();
            }
        }
        public ActionResult Login(string User_name, string password)
        {

            var data = ob.tbl_DC_MentorDashboardLogin.Where(x => x.Username.Equals(User_name)
                   && x.Password.Equals(password) && x.Is_Active == true && x.Is_Delete == false).FirstOrDefault();
            if (data != null)
            {
                Session["MentorLogin"] = "Y";
                return RedirectToAction("Dashboard");
            }
            else
            {
                TempData["ErrorMessage"] = "Invalid credential for admin login.";
                return View();
            }

        }
        public ActionResult Dashboard()
        {
            if (Session["MentorLogin"] == null)
                return RedirectToAction("Login");
            else
                return View();
        }
        public ActionResult ViewExamDetails()
        {

            if (Request.QueryString["ResId"] == null)
            {
                return View("Index");
            }
            else
            {
                Session["ResId"] = Request.QueryString["ResId"].ToString();
            }
            return View();
        }
        public ActionResult ViewStudentDetails()
        {

            if (Request.QueryString["RegdId"] == null)
            {
                return View("Index");
            }
            else
            {
                Session["RegdId"] = Request.QueryString["RegdId"].ToString();
            }
            return View();
        }
        public class ExamDetailsCls
        {


            public int? CountAppeared { get; set; }
            public int? CountAnswered { get; set; }
            public int? slno { get; set; }


            public int? Score { get; set; }

            public string IndividualScore { get; set; }

            //public string AcademicResult { get; set; }
            //public string SocialResult { get; set; }
            public string studentname { get; set; }
            public string mobile { get; set; }
            public DateTime? examdate { get; set; }
            public int? totalquestions { get; set; }
            public int? Attemptedques { get; set; }
            public int? NotAttemptedques { get; set; }
            public int? Correctques { get; set; }
            public int? Result_ID { get; set; }
            public int? Regd_ID { get; set; }
        }
        public class ExamQuesAnsCls
        {
            public int Result_ID { get; set; }
            public string Question { get; set; }
            public string First_Option { get; set; }
            public string Second_Option { get; set; }
            public string Third_Option { get; set; }
            public string Fourth_Option { get; set; }
            public string Answer { get; set; }
            public string Correct_Answer { get; set; }
        }
        public ActionResult GetAllExamDetails()
        {
            try
            {
                var regs = ob.tbl_DC_Registration.ToList();
                var exams = ob.tbl_DC_Mentor_Exam_Result.ToList();
                var scores = ob.tbl_DC_Mentorship_Exam_Score.ToList();
                List<ExamDetailsCls> res = (from a in regs //ob.tbl_DC_Registration
                                            join b in exams on a.Regd_ID equals b.Regd_ID



                                            //let b = ob.tbl_DC_Mentor_Exam_Result
                                            //    .Where(
                                            //         cl => cl.Regd_ID == a.Regd_ID
                                            //        )
                                            //    .OrderBy(cl => cl.Result_ID) // Todo: Might need to be descending?
                                            //    .FirstOrDefault()
                                            //where b != null
                                            //orderby b.Regd_ID descending                           
                                            select new ExamDetailsCls
                                            {
                                                studentname = a.Customer_Name,
                                                mobile = a.Mobile,
                                                examdate = a.Modified_Date,
                                                totalquestions = b.Question_Nos,
                                                Attemptedques = b.Question_Attempted,
                                                NotAttemptedques = b.Question_Nos - b.Question_Attempted,
                                                Correctques = b.Total_Correct_Ans,
                                                Result_ID = b.Result_ID,
                                                Regd_ID = b.Regd_ID,
                                                IndividualScore =
                                                "Academic : " + ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == b.Result_ID)
                                                .Select(x => x.Academic).FirstOrDefault() + "," +
                                                "Doubts : " + ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == b.Result_ID)
                                                .Select(x => x.Doubt).FirstOrDefault() + " , " +
                                                 "Interest Level : " + ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == b.Result_ID)
                                                .Select(x => x.Interest).FirstOrDefault() + " , " +
                                                 "Time Management : " + ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == b.Result_ID)
                                                .Select(x => x.Time).FirstOrDefault() + " , " +
                                                 "SelfStudy : " + ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == b.Result_ID)
                                                .Select(x => x.SelfStudy).FirstOrDefault() + " , " +
                                                 "School and Tuitions : " + ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == b.Result_ID)
                                                .Select(x => x.School).FirstOrDefault() + " , " +
                                                 "Personal : " + ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == b.Result_ID)
                                                .Select(x => x.Personal).FirstOrDefault() + " , " +
                                                 "Mentorship : " + ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == b.Result_ID)
                                                .Select(x => x.Mentorship).FirstOrDefault()
                                                ,
                                                Score = ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == b.Result_ID)
                                                .Select(x => x.Total).FirstOrDefault(),


                                                //AcademicResult = ob.tbl_DC_Psychometric_Result_Pattern
                                                //            .Where(x => x.Pattern_Code.Equals(b.AcademicId) && x.PsychometricType == 1).
                                                //            Select(x => x.Comment).FirstOrDefault(),
                                                //SocialResult = ob.tbl_DC_Psychometric_Result_Pattern
                                                //.Where(x => x.Pattern_Code.Equals(b.InterpersonalId) && x.PsychometricType == 2).
                                                //Select(x => x.Comment).FirstOrDefault(),

                                            }).ToList();
                int cnt = 1;
                foreach (var a in res.ToList())
                {
                    a.slno = cnt++;
                }
                var result = new
                {
                    draw = 1,
                    recordsTotal = res.ToList().Count(),
                    recordsFiltered = res.ToList().Count(),
                    data = res.ToList().ToArray()
                };

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetDashboardCount()
        {
            try
            {
                //var regs = ob.tbl_DC_Registration.ToList();
                //var exams = ob.tbl_DC_Psychometric_Exam_Result.ToList();
                List<ExamDetailsCls> res = (from a in ob.tbl_DC_Registration
                                            let b = ob.tbl_DC_Mentor_Exam_Result
                                                .Where(
                                                     cl => cl.Regd_ID == a.Regd_ID
                                                    )
                                                .OrderBy(cl => cl.Result_ID) // Todo: Might need to be descending?
                                                .FirstOrDefault()
                                            where b != null
                                            orderby b.Regd_ID descending
                                            select new ExamDetailsCls
                                            {
                                                studentname = a.Customer_Name,
                                                mobile = a.Mobile,
                                                examdate = a.Modified_Date,
                                                totalquestions = b.Question_Nos,
                                                Attemptedques = b.Question_Attempted,
                                                NotAttemptedques = b.Question_Nos - b.Question_Attempted,
                                                Correctques = b.Total_Correct_Ans,
                                                Result_ID = b.Result_ID,
                                                Regd_ID = b.Regd_ID,


                                                //AcademicResult = ob.tbl_DC_Psychometric_Result_Pattern
                                                //            .Where(x => x.Pattern_Code.Equals(b.AcademicId) && x.PsychometricType == 1).
                                                //            Select(x => x.Comment).FirstOrDefault(),
                                                //SocialResult = ob.tbl_DC_Psychometric_Result_Pattern
                                                //.Where(x => x.Pattern_Code.Equals(b.InterpersonalId) && x.PsychometricType == 2).
                                                //Select(x => x.Comment).FirstOrDefault(),

                                            }).ToList();
                List<ExamDetailsCls> resAnswered = (from a in ob.tbl_DC_Registration
                                                    let b = ob.tbl_DC_Mentor_Exam_Result
                                                .Where(
                                                     cl => cl.Regd_ID == a.Regd_ID && cl.Total_Correct_Ans == 20
                                                    )
                                                .OrderBy(cl => cl.Result_ID) // Todo: Might need to be descending?
                                                .FirstOrDefault()
                                                    where b != null
                                                    orderby b.Regd_ID descending
                                                    select new ExamDetailsCls
                                                    {
                                                        studentname = a.Customer_Name,
                                                        mobile = a.Mobile,
                                                        examdate = a.Modified_Date,
                                                        totalquestions = b.Question_Nos,
                                                        Attemptedques = b.Question_Attempted,
                                                        //NotAttemptedques = ,
                                                        //Correctques = b.Inserted_By.Split(":"),
                                                        Result_ID = b.Result_ID,
                                                        Regd_ID = b.Regd_ID,


                                                        //AcademicResult = ob.tbl_DC_Psychometric_Result_Pattern
                                                        //            .Where(x => x.Pattern_Code.Equals(b.AcademicId) && x.PsychometricType == 1).
                                                        //            Select(x => x.Comment).FirstOrDefault(),
                                                        //SocialResult = ob.tbl_DC_Psychometric_Result_Pattern
                                                        //.Where(x => x.Pattern_Code.Equals(b.InterpersonalId) && x.PsychometricType == 2).
                                                        //Select(x => x.Comment).FirstOrDefault(),

                                                    }).ToList();


                var result = new
                {
                    CountAppeared = res.Count,
                    CountAnswered = resAnswered.Count,
                };

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllquestions()
        {
            int id = Convert.ToInt32(Session["ResId"].ToString());
            var Result = ob.tbl_DC_Mentor_Exam_Result.Where(x => x.Result_ID == id).FirstOrDefault();
            int? RegId = Result.Regd_ID;
            var regs = ob.tbl_DC_Registration.ToList();
            var exams = ob.tbl_DC_Mentorship_Exam_Result_Dtl.ToList();
            var ques = ob.tbl_DC_MentorshipExam_Question.ToList();
            var ans = ob.tbl_DC_MentorshipExam_Question_Answer.ToList();
            var result = (from a in exams
                          join b in ques on a.Question_ID equals b.Question_ID
                          where a.Result_ID == id
                          select new ExamQuesAnsCls
                          {
                              Result_ID = Convert.ToInt32(a.Result_ID),
                              Question = b.Question,
                              First_Option = ans.ToList().Where(m => m.Question_ID == b.Question_ID).ToList()[0].Option_Desc,
                              Second_Option = ans.ToList().Where(m => m.Question_ID == b.Question_ID).ToList()[1].Option_Desc,
                              Third_Option = ans.ToList().Where(m => m.Question_ID == b.Question_ID).ToList()[2].Option_Desc,
                              //Fourth_Option = ans.ToList().Where(m => m.Question_ID == b.Question_ID).ToList()[3].Option_Desc,
                              Answer = ans.ToList().Where(m => m.Answer_ID == a.Answer_ID)
                              .Select(x => x.Option_Desc).FirstOrDefault(),
                              Correct_Answer = ans.ToList().Where(m => m.Question_ID == b.Question_ID
                                  && m.Answer_ID == a.Answer_ID).ToList()[0].Score + "",
                          }).ToList();

            int? RegdId = RegId;//.Convert.ToInt32(Session["RegdId"].ToString());
            var data = (from a in ob.tbl_DC_Registration.
                           Where(x => x.Regd_ID == RegdId && x.Is_Active == true && x.Is_Deleted == false)
                        join b in ob.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == RegdId)
                       on a.Regd_ID equals b.Regd_ID
                        select new DigiChamps.Models.Digichamps.Student_Profile
                        {
                            Customer_Name = a.Customer_Name,
                            Mobile = a.Mobile,
                            Image_Url = "https://learn.odmps.org/Images/Profile/" + a.Image,
                            Email = a.Email,
                            DOB = a.DateOfBirth,
                            SchoolName = ob.tbl_DC_School_Info.Where(x => x.SchoolId == a.SchoolId).
                            Select(x => x.SchoolName).FirstOrDefault(),
                            Board_Name = ob.tbl_DC_Board.Where(x => x.Board_Id == b.Board_ID).Select(x => x.Board_Name).FirstOrDefault(),
                            Class_Name = ob.tbl_DC_Class.Where(x => x.Class_Id == b.Class_ID).Select(x => x.Class_Name).FirstOrDefault(),
                            SectionName = ob.tbl_DC_Class_Section.Where(x => x.SectionId == a.SectionId).
                            Select(x => x.SectionName).FirstOrDefault(),
                            Gender = a.Gender == true ? "Male" : "Female",
                        }
                     ).FirstOrDefault();

            var AcademicResult = ob.tbl_DC_MentorshipExam_Result_Pattern
                        .Where(x => x.Pattern_Code == Result.AcademicId && x.CategoryType == 1).
                        Select(x => x.Comment).FirstOrDefault();
            var SocialResult = ob.tbl_DC_MentorshipExam_Result_Pattern
            .Where(x => x.Pattern_Code == Result.DoubtId && x.CategoryType == 2).
            Select(x => x.Comment).FirstOrDefault();

            var InterestResult = ob.tbl_DC_MentorshipExam_Result_Pattern
           .Where(x => x.Pattern_Code == Result.InterestId && x.CategoryType == 3).
           Select(x => x.Comment).FirstOrDefault();

            var TimeResult = ob.tbl_DC_MentorshipExam_Result_Pattern
           .Where(x => x.Pattern_Code == Result.TimeManageId && x.CategoryType == 4).
           Select(x => x.Comment).FirstOrDefault();

            var SelfStudyResult = ob.tbl_DC_MentorshipExam_Result_Pattern
           .Where(x => x.Pattern_Code == Result.SelfStudyId && x.CategoryType == 5).
           Select(x => x.Comment).FirstOrDefault();


            var SchoolResult = ob.tbl_DC_MentorshipExam_Result_Pattern
           .Where(x => x.Pattern_Code == Result.SchoolId && x.CategoryType == 6).
           Select(x => x.Comment).FirstOrDefault();


            var PersonalResult = ob.tbl_DC_MentorshipExam_Result_Pattern
           .Where(x => x.Pattern_Code == Result.PersonalId && x.CategoryType == 7).
           Select(x => x.Comment).FirstOrDefault();


            var MentorshipResult = ob.tbl_DC_MentorshipExam_Result_Pattern
           .Where(x => x.Pattern_Code == Result.MentorshipId && x.CategoryType == 8).
           Select(x => x.Comment).FirstOrDefault();

            var d = new ExamData
            {
                profile = data,
                list = result,
                AcademicResult = AcademicResult,
                SocialResult = SocialResult,

                InterestResult = InterestResult,
                TimeResult = TimeResult,

                SelfStudyResult = SelfStudyResult,
                SchoolResult = SchoolResult,

                PersonalResult = PersonalResult,
                MentorshipResult = MentorshipResult,

                TQ = Result.Question_Nos,
                QAT = Result.Question_Attempted,
                QANS = ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == Result.Result_ID)
                               .Select(x => x.Total).FirstOrDefault(),

                QANS1 = ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == Result.Result_ID)
                .Select(x => x.Academic).FirstOrDefault(),
                QANS2 = ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == Result.Result_ID)
                               .Select(x => x.Doubt).FirstOrDefault(),

                QANS3 = ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == Result.Result_ID)
                .Select(x => x.Interest).FirstOrDefault(),
                QANS4 = ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == Result.Result_ID)
                               .Select(x => x.Time).FirstOrDefault(),
                QANS5 = ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == Result.Result_ID)
                .Select(x => x.SelfStudy).FirstOrDefault(),
                QANS6 = ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == Result.Result_ID)
                               .Select(x => x.School).FirstOrDefault(),
                QANS7 = ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == Result.Result_ID)
                .Select(x => x.Personal).FirstOrDefault(),
                QANS8 = ob.tbl_DC_Mentorship_Exam_Score.Where(x => x.ExamResultID == Result.Result_ID)
                               .Select(x => x.Mentorship).FirstOrDefault(),



            };
            return Json(d, JsonRequestBehavior.AllowGet);
        }
        public class ExamData
        {
            public DigiChamps.Models.Digichamps.Student_Profile profile;
            public List<ExamQuesAnsCls> list;

            public string AcademicResult;
            public string SocialResult;

            public string InterestResult;
            public string TimeResult;

            public string SelfStudyResult;
            public string SchoolResult;

            public string PersonalResult;
            public string MentorshipResult;

            public int? TQ;
            public int? QAT;
            public int? QANS;

            public int? QANS1;
            public int? QANS2;
            public int? QANS3;
            public int? QANS4;
            public int? QANS5;
            public int? QANS6;
            public int? QANS7;
            public int? QANS8;

        }
        public ActionResult GetStudentDetails()
        {
            int RegdId = Convert.ToInt32(Session["RegdId"].ToString());
            var data = (from a in ob.tbl_DC_Registration.
                           Where(x => x.Regd_ID == RegdId && x.Is_Active == true && x.Is_Deleted == false)
                        join b in ob.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == RegdId)
                       on a.Regd_ID equals b.Regd_ID
                        select new DigiChamps.Models.Digichamps.Student_Profile
                        {
                            Customer_Name = a.Customer_Name,
                            Mobile = a.Mobile,
                            Email = a.Email,
                            Image_Url = "https://learn.odmps.org/Images/" + a.Image,
                            DOB = a.DateOfBirth,
                            SchoolName = ob.tbl_DC_School_Info.Where(x => x.SchoolId == a.SchoolId).
                            Select(x => x.SchoolName).FirstOrDefault(),
                            Board_Name = ob.tbl_DC_Board.Where(x => x.Board_Id == b.Board_ID).Select(x => x.Board_Name).FirstOrDefault(),
                            Class_Name = ob.tbl_DC_Class.Where(x => x.Class_Id == b.Class_ID).Select(x => x.Class_Name).FirstOrDefault(),
                            SectionName = ob.tbl_DC_Class_Section.Where(x => x.SectionId == a.SectionId).
                            Select(x => x.SectionName).FirstOrDefault(),
                            Gender = a.Gender == true ? "Male" : "Female",
                        }
                     ).FirstOrDefault();

            return Json(data, JsonRequestBehavior.AllowGet);

        }
        #region mentorship registration
        public ActionResult Registration()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetBoardDetails()
        {
            var regs = ob.tbl_DC_Board.ToList().Where(a => a.Is_Deleted == false && a.Is_Active == true).ToList();

            return Json(regs, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetSchoolDetails()
        {
            var regs = ob.tbl_DC_School_Info.ToList().Where(a => a.IsActive == true).ToList();

            return Json(regs, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetClassDetails(int id)
        {
            var cls = ob.tbl_DC_Class.ToList().Where(a => a.Board_Id == id && a.Is_Deleted == false && a.Is_Active == true).ToList();

            return Json(cls, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddMentorRegistration(string name, string mobile, string school, string board, string classes)
        {
            try
            {
                tbl_Mentorship_Registration mr = new tbl_Mentorship_Registration();
                mr.Full_Name = name;
                mr.Mobile = mobile;
                mr.School = school;
                mr.Board = board;
                mr.Class = classes;
                mr.InsertedON = DateTime.Now;
                mr.IsActive = true;
                mr.IsDeleted = false;
                ob.tbl_Mentorship_Registration.Add(mr);
                ob.SaveChanges();
                Session["M_Regd_Id"] = mr.Mentorship_ID;
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Mentorship Exam
        public ActionResult MentorshipExam()
        {
            if (Session["M_Regd_Id"] == null)
            {
                return View("Registration");
            }
            if (Session["Ques"] == null)
            {
                GetallMentorshipQues();
            }
            Session["S_Time"] = System.DateTime.Now;
            return View();
        }
        #region Class For Mentorship Exam
        public class OptionList
        {
            public int Answer_ID { get; set; }
            public string Option_Desc { get; set; }
            public int Score { get; set; }
        }
        public class QuestionsCls
        {
            public int Id { get; set; }
            public int Question_ID { get; set; }
            public string Question { get; set; }
            public List<OptionList> options { get; set; }
            public int Answer { get; set; }
            public string btnchk { get; set; }
        }
        #endregion      
        public JsonResult GetQuestions(int id)
        {
            var resultCount = ob.tbl_DC_Mentor_Exam_Result.ToList().Where(x => x.Regd_ID == Convert.ToInt32(Session["M_Regd_Id"].ToString()) && x.Is_Active == true
                         && x.Is_Deleted == false).ToList() == null ? 0 : ob.tbl_DC_Mentor_Exam_Result.ToList().Where(x => x.Regd_ID == Convert.ToInt32(Session["M_Regd_Id"].ToString()) && x.Is_Active == true
                         && x.Is_Deleted == false).ToList().Count();
            if (resultCount <= 3)
            {
                if (Session["Ques"] != null)
                {
                    List<QuestionsCls> ques = Session["Ques"] as List<QuestionsCls>;
                    var res = ques.ToList().Where(a => a.Id == id).ToList();
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(3, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult PutAnswer(int id, int qid)
        {
            if (Session["Ques"] != null)
            {
                List<QuestionsCls> ques = Session["Ques"] as List<QuestionsCls>;
                QuestionsCls res = ques.ToList().Where(a => a.Id == qid).FirstOrDefault();
                res.Answer = id;
                Session["Ques"] = ques.ToList();
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        public void GetallMentorshipQues()
        {
            var ques = ob.tbl_DC_MentorshipExam_Question.ToList().Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            List<QuestionsCls> q = new List<QuestionsCls>();
            int cnt = 1;
            foreach (var a in ques.ToList())
            {
                QuestionsCls qc = new QuestionsCls();
                qc.Question_ID = a.Question_ID;
                qc.Question = a.Question;
                qc.Answer = 0;
                if (cnt == 1)
                {
                    qc.btnchk = "F";
                }
                else if (cnt == ques.Count)
                {
                    qc.btnchk = "L";
                }
                else
                {
                    qc.btnchk = "N";
                }
                List<OptionList> optionlst = new List<OptionList>();
                var options = ob.tbl_DC_MentorshipExam_Question_Answer.ToList().Where(m => m.Is_Active == true && m.Is_Deleted == false && m.Question_ID == a.Question_ID).ToList();
                foreach (var b in options.ToList())
                {
                    OptionList option = new OptionList();
                    option.Answer_ID = b.Answer_ID;
                    option.Option_Desc = b.Option_Desc;
                    option.Score = Convert.ToInt32(b.Score);
                    optionlst.Add(option);
                }
                qc.Id = cnt;
                qc.options = optionlst;
                q.Add(qc);
                cnt++;
            }
            Session["Ques"] = q.ToList();
        }
        public JsonResult SaveMentorshipExamResult()
        {
            if (Session["Ques"] != null)
            {

                List<QuestionsCls> ques = Session["Ques"] as List<QuestionsCls>;
                if (ques.ToList().Where(v => v.Answer == 0).ToList().Count > 0)
                {
                    return Json(2, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    List<DigiChamps.Controllers.ExamController.PsychometricQuestionItem> pquesitem = new List<ExamController.PsychometricQuestionItem>();
                    foreach (var a in ques.ToList())
                    {
                        DigiChamps.Controllers.ExamController.PsychometricQuestionItem Qitem = new ExamController.PsychometricQuestionItem();
                        Qitem.QuestionId = a.Question_ID;
                        Qitem.Type = ob.tbl_DC_MentorshipExam_Question.ToList().Where(b => b.Question_ID == a.Question_ID).FirstOrDefault().Type;
                        Qitem.AnsweredId = a.Answer;
                        Qitem.ReviewedId = 0;
                        Qitem.SkippedId = 0;
                        Qitem.SubType = 0;
                        var options = a.options.ToList();
                        List<DigiChamps.Controllers.ExamController.PsychometricQuestionOptionItem> pques = new List<ExamController.PsychometricQuestionOptionItem>();
                        foreach (var c in options.ToList())
                        {
                            DigiChamps.Controllers.ExamController.PsychometricQuestionOptionItem itm = new ExamController.PsychometricQuestionOptionItem();
                            itm.OptionId = c.Answer_ID;
                            itm.Score = c.Score;
                            itm.Option = c.Option_Desc;
                            pques.Add(itm);
                        }
                        Qitem.optionList = pques;
                        pquesitem.Add(Qitem);
                    }
                    DigiChamps.Controllers.ExamController.PsychometricExamResult pexam = new ExamController.PsychometricExamResult();
                    pexam.ExamId = 1;
                    DateTime sdt= Convert.ToDateTime(Session["S_Time"].ToString());
                    DateTime Edt = DateTime.Now;
                    pexam.RegId = Convert.ToInt32(Session["M_Regd_Id"].ToString());
                    pexam.StartTime =sdt;
                    pexam.StopTime = Edt;
                    pexam.questions = pquesitem;
                    return Json(pexam, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SendDataToNextPage(DigiChamps.Controllers.ExamController.PsychometricResult res)
        {
            Session["M_Result"] = res;
            Session["Ques"] = null;
            return Json(1,JsonRequestBehavior.AllowGet);
        }
        #endregion
        public ActionResult ExamResult()
        {
            if (Session["M_Result"] != null)
            {
                DigiChamps.Controllers.ExamController.PsychometricResult obj = (DigiChamps.Controllers.ExamController.PsychometricResult)Session["M_Result"];
                return View(obj);
            }
            else
            {
                return View("Registration");
            }
        }
    }
}
