﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    //  [Authorize]
    public class ExamController : ApiController
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();


        public class Success_leader
        {
            public Leader_Board success { get; set; }
        }
        public class Leader_Board
        {
            public List<Myresult> My_Result { get; set; }
            public List<Leader_result> Top10 { get; set; }

        }
        public class Myresult
        {
            public Nullable<int> Max_rid { get; set; }
            public Nullable<int> Regd_ID { get; set; }
            public Nullable<int> Totaltime { get; set; }
            public Nullable<int> Question_Nos { get; set; }
            public Nullable<int> Total_Correct_Ans { get; set; }
            public Nullable<int> Appear { get; set; }
            public string Customer_Name { get; set; }
            public string Image { get; set; }
            public int Rank { get; set; }
            public int? Incorrect { get; set; }
            public int? Accuracy { get; set; }
        }
        public class Leader_result
        {

            public int Rank { get; set; }
            public Nullable<int> Max_rid { get; set; }
            public Nullable<int> Regd_ID { get; set; }
            public Nullable<int> Totaltime { get; set; }
            public Nullable<int> Question_Nos { get; set; }
            public Nullable<int> Total_Correct_Ans { get; set; }
            public Nullable<int> Appear { get; set; }
            public string Customer_Name { get; set; }
            public string Image { get; set; }
            public int? Accuracy { get; set; }
            public int? Incorrect { get; set; }


            public DateTime? StartTime { get; set; }
        }

        public class Success_message_Blank
        {
            public Message_for_success success { get; set; }
        }
        public class Message_for_success
        {
            public string message { get; set; }
        }
        public class Result_Succes
        {
            public Result_Data success { get; set; }
        }

        public class DifficultyData
        {
            public int? PowerId { get; set; }
            public int? TotalQuestions { get; set; }


            public int? TotalCorrect { get; set; }
            public int? TotalInCorrect { get; set; }
            public int? TotalSkipped { get; set; }

            public int? Accuracy { get; set; }

        }
        public class SubConceptData
        {
            public int? SubConceptId { get; set; }
            public string SubConceptName { get; set; }
            public int? TotalQuestions { get; set; }


            public int? TotalCorrect { get; set; }
            public int? TotalInCorrect { get; set; }
            public int? TotalSkipped { get; set; }

            public int? Accuracy { get; set; }

        }
        public class Result_Data
        {
            public string ChapterName { get; set; }
            public string TestName { get; set; }
            public string TestType { get; set; }
            public List<SubConceptData> subConcept { get; set; }

            public List<DifficultyData> difficulty { get; set; }

            public Nullable<int> Question_Nos { get; set; }
            public Nullable<int> Total_Correct_Ans { get; set; }
            public Nullable<double> Totaltime { get; set; }
            public Nullable<int> wrong { get; set; }
            public int Accuracy { get; set; }
            public int Rank { get; set; }
            public int Performance { get; set; }
            public List<Difficulty> Difficultys { get; set; }
            public List<Startegic_Report_Result> Topic_details { get; set; }
           
            public int? Exam_ID { get; set; }

            public int? Total_Incorrec_Ans { get; set; }

            public int? Question_Attempted { get; set; }

            public int? Unanswered { get; set; }
        }

        public class Startegic_Report_Result
        {
            public Nullable<int> Topic_ID { get; set; }
            public string Topic_Name { get; set; }
            public Nullable<int> Total_question { get; set; }
            public int Correct_answer { get; set; }
            public int Incorrect_answer { get; set; }
            public Nullable<int> Percentage { get; set; }
            public string Remark { get; set; }
        }

        public class Difficulty
        {
            public int? No_of_Question { get; set; }
            public int? Power_ID { get; set; }
            public string Power_Type { get; set; }
        }




        [HttpGet]
        public HttpResponseMessage Get_Exam(int? id, int? eid)//redg id
        {
            if (id != null)
            {
                try
                {
                    var chaptersubscribes = DbContext.SP_DC_Getchapter(id).Distinct().ToList();
                    if (chaptersubscribes.Count > 0)
                    {
                        var examdetails = DbContext.SP_ExamList(id, 5).ToList();
                        //var examdetails1 = DbContext.SP_ExamList(id, 1).ToList();
                        if (examdetails.Count > 0)
                        {
                            var obj = new DigiChamps.Models.Digichamps.SuccessResult_Exam1
                            {
                                success = new Digichamps.SuccessResponse_Exam1
                                {
                                    Online_Exam_Count = examdetails.Where(x => x.Chapter_Id == eid).ToList().Count(),

                                    Onine_Exam_List = (from c in examdetails.Where(x => x.Chapter_Id == eid)
                                                       select new DigiChamps.Models.Digichamps.Exam_List
                                                       {

                                                           Exam_ID = c.Exam_ID,
                                                           Exam_Name = c.Exam_Name,
                                                           Chapter_Id = c.Chapter_Id,
                                                           Board_Id = c.Board_Id,
                                                           Class_Id = c.Class_Id,
                                                           Is_Global = c.Is_Global,
                                                           Question_nos = c.Question_nos,
                                                           Subject = c.Subject,
                                                           Subject_Id = c.Subject_Id,
                                                           Time = c.Time,
                                                           Attempt_nos = c.Attempt_nos,
                                                           student_Attempt = c.student_Attempt,
                                                           Participants = c.Participants,
                                                           is_free = false,
                                                           Validity = 0
                                                       }).ToList(),
                                    //    Pre_Requisite_test = (from a in examdetails1.Where(x => x.Chapter_Id == eid)
                                    //                          select new DigiChamps.Models.Digichamps.Offline_exam_Pre_Requisite_test1
                                    //                          {
                                    //                              Exam_ID = a.Exam_ID,

                                    //                              Attempt_nos = a.Attempt_nos,
                                    //                              Exam_Name = a.Exam_Name,
                                    //                              Chapter_Id = a.Chapter_Id,
                                    //                              Time = a.Time,
                                    //                              Exam_type = 1,
                                    //                              Subject_Id = a.Subject_Id,
                                    //                              Question_nos = a.Question_nos,
                                    //                              student_attempt = a.student_Attempt,
                                    //                              Participants = a.Participants
                                    //                          }).ToList(),
                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                            {
                                error = new Digichamps.ErrorResponse_Exam
                                {
                                    Message = "No exams Found."
                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                    }
                    else
                    {
                        var free_test = DbContext.Sp_DC_Free_Exam(id).ToList();
                        //  var data_pre = DbContext.SP_DC_Offline_exams(id, 1).Where(x => x.Chapter_Id == eid).ToList();

                        if (free_test.Count > 0)
                        {
                            var obj = new DigiChamps.Models.Digichamps.Success_freeexam
                            {
                                success = new Digichamps.SuccessResponse_Free_Exam
                                {
                                    Onine_Exam_List = (from c in free_test.Where(x => x.Exam_type == 5)
                                                       select new DigiChamps.Models.Digichamps.Free_Exam_List
                                                       {
                                                           Exam_ID = c.Exam_ID,
                                                           Exam_Name = c.Exam_Name,
                                                           Subject_Id = c.Subject_Id,
                                                           Question_nos = c.Question_nos,
                                                           Attempt_nos = c.Attempt_nos,
                                                           Time = c.Time,
                                                           is_free = true,
                                                           Validity = c.Validity,
                                                           Chapter_Id = 0,
                                                           Board_Id = 0,
                                                           Class_Id = 0,
                                                           Is_Global = false,
                                                           Subject = "",
                                                           student_Attempt = c.stu_Attempt_nos,
                                                           Participants = c.Participants,


                                                       }).ToList(),
                                    //Pre_Requisite_test = (from a in free_test.Where(x => x.Exam_type == 1)
                                    //                      select new DigiChamps.Models.Digichamps.Offline_exam_Pre_Requisite_test1
                                    //                      {
                                    //                          Exam_ID = a.Exam_ID,

                                    //                          Attempt_nos = a.Attempt_nos,
                                    //                          Exam_Name = a.Exam_Name,
                                    //                          Chapter_Id = a.Chapter_Id,
                                    //                          Time = a.Time,
                                    //                          Exam_type = a.Exam_type,
                                    //                          Subject_Id = a.Subject_Id,
                                    //                          Question_nos = a.Question_nos,
                                    //                          student_attempt = a.stu_Attempt_nos,
                                    //                          Participants = a.Participants
                                    //                      }).ToList(),

                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                            {
                                error = new Digichamps.ErrorResponse_Exam
                                {
                                    Message = "No exams Found."
                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                    }
                }
                catch
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "Something went wrong."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            else
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "Please provide data correctly."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetCbtExam(int? id,int? eid)//redg id
        {



            
                try
                {
                    Boolean status=true;

                    if (status)
                    {

                        var exam = DbContext.tbl_DC_Exam.SqlQuery(@"SELECT [Exam_ID]
      ,[Exam_Name]
      ,[Board_Id]
      ,[Class_Id]
      ,[Subject_Id]
      ,[Chapter_Id]
      ,[Question_nos]
      ,[Attempt_nos]
      ,[Time]
      ,[Validity]
      ,[Is_Global]
      ,[Inserted_By]
      ,[Modified_Date]
      ,[Modified_By]
      ,[Is_Active]
      ,[Is_Deleted]
      ,[Exam_type]
      ,[Percentage_retest]
      ,[Shedule_date]
      ,[Shedule_time]
  FROM [odm_lms].[dbo].[tbl_DC_Exam]").ToList();

                        var examdetails = (from c in exam.Where(x => x.Chapter_Id == eid && x.Exam_type == 5)
                                           select new DigiChamps.Models.Digichamps.Exam_List
                                             {

                                                 Exam_ID = c.Exam_ID,
                                                 Exam_Name = c.Exam_Name,
                                                 Chapter_Id = c.Chapter_Id,
                                                 Board_Id = c.Board_Id,
                                                 Class_Id = c.Class_Id,
                                                 Is_Global = c.Is_Global,
                                                 Question_nos = c.Question_nos,

                                                 Subject_Id = c.Subject_Id,
                                                 Time = c.Time,
                                                 Attempt_nos = c.Attempt_nos,

                                                 is_free = false,
                                                 Validity = 0
                                             }).ToList();

                        return Request.CreateResponse(HttpStatusCode.OK, examdetails);

                    }
                    else
                    {
                        var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                        {
                            error = new Digichamps.ErrorResponse_Exam
                            {
                                Message = "Expired"
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.NoContent, obj);
                    }
                    
                }
                catch
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "Something went wrong."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            
            

        }
        [HttpGet]
        public HttpResponseMessage GetPrtExam(int? id,int? eid)//redg id
        {




            try
            {
                Boolean status = true;

                if (status)
                {

                    var exam = DbContext.tbl_DC_Exam.SqlQuery(@"SELECT [Exam_ID]
      ,[Exam_Name]
      ,[Board_Id]
      ,[Class_Id]
      ,[Subject_Id]
      ,[Chapter_Id]
      ,[Question_nos]
      ,[Attempt_nos]
      ,[Time]
      ,[Validity]
      ,[Is_Global]
      ,[Inserted_By]
      ,[Modified_Date]
      ,[Modified_By]
      ,[Is_Active]
      ,[Is_Deleted]
      ,[Exam_type]
      ,[Percentage_retest]
      ,[Shedule_date]
      ,[Shedule_time]
  FROM [odm_lms].[dbo].[tbl_DC_Exam]").ToList();

                    var examdetails = (from c in exam.Where(x => x.Chapter_Id == eid && x.Exam_type == 1)
                                       select new DigiChamps.Models.Digichamps.Exam_List
                                       {

                                           Exam_ID = c.Exam_ID,
                                           Exam_Name = c.Exam_Name,
                                           Chapter_Id = c.Chapter_Id,
                                           Board_Id = c.Board_Id,
                                           Class_Id = c.Class_Id,
                                           Is_Global = c.Is_Global,
                                           Question_nos = c.Question_nos,

                                           Subject_Id = c.Subject_Id,
                                           Time = c.Time,
                                           Attempt_nos = c.Attempt_nos,

                                           is_free = false,
                                           Validity = 0
                                       }).ToList();

                    return Request.CreateResponse(HttpStatusCode.OK, examdetails);

                }
                else
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "Expired"
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.NoContent, obj);
                }

            }
            catch
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }



        }

        [HttpGet]
        public HttpResponseMessage Get_PRT(int? id, int? eid)//redg id prt
        {
            if (id != null)
            {
                try
                {
                    var chaptersubscribes = DbContext.SP_DC_Getchapter(id).Distinct().ToList();
                    if (chaptersubscribes.Count > 0)
                    {
                        // var examdetails = DbContext.SP_ExamList(id, 5).ToList();
                        var examdetails1 = DbContext.SP_ExamList(id, 1).ToList();
                        if (examdetails1.Count > 0)
                        {
                            var obj = new DigiChamps.Models.Digichamps.SuccessResult_Exam2
                            {
                                success = new Digichamps.SuccessResponse_Exam2
                                {
                                    //Online_Exam_Count = examdetails.Where(x => x.Chapter_Id == eid).ToList().Count(),

                                    //Onine_Exam_List = (from c in examdetails.Where(x => x.Chapter_Id == eid)
                                    //                   select new DigiChamps.Models.Digichamps.Exam_List
                                    //                   {

                                    //                       Exam_ID = c.Exam_ID,
                                    //                       Exam_Name = c.Exam_Name,
                                    //                       Chapter_Id = c.Chapter_Id,
                                    //                       Board_Id = c.Board_Id,
                                    //                       Class_Id = c.Class_Id,
                                    //                       Is_Global = c.Is_Global,
                                    //                       Question_nos = c.Question_nos,
                                    //                       Subject = c.Subject,
                                    //                       Subject_Id = c.Subject_Id,
                                    //                       Time = c.Time,
                                    //                       Attempt_nos = c.Attempt_nos,
                                    //                       student_Attempt = c.student_Attempt,
                                    //                       Participants = c.Participants,
                                    //                       is_free = false,
                                    //                       Validity = 0
                                    //                   }).ToList(),
                                    Pre_Requisite_test = (from a in examdetails1.Where(x => x.Chapter_Id == eid)
                                                          select new DigiChamps.Models.Digichamps.Offline_exam_Pre_Requisite_test1
                                                          {
                                                              Exam_ID = a.Exam_ID,

                                                              Attempt_nos = a.Attempt_nos,
                                                              Exam_Name = a.Exam_Name,
                                                              Chapter_Id = a.Chapter_Id,
                                                              Time = a.Time,
                                                              Exam_type = 1,
                                                              Subject_Id = a.Subject_Id,
                                                              Question_nos = a.Question_nos,
                                                              student_attempt = a.student_Attempt,
                                                              Participants = a.Participants
                                                          }).ToList(),
                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                            {
                                error = new Digichamps.ErrorResponse_Exam
                                {
                                    Message = "No exams Found."
                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                    }
                    else
                    {
                        var free_test = DbContext.Sp_DC_Free_Exam(id).ToList();
                        //  var data_pre = DbContext.SP_DC_Offline_exams(id, 1).Where(x => x.Chapter_Id == eid).ToList();

                        if (free_test.Count > 0)
                        {
                            var obj = new DigiChamps.Models.Digichamps.Success_freeexam1
                            {
                                success = new Digichamps.SuccessResponse_Free_Exam1
                                {

                                    Pre_Requisite_test = (from a in free_test.Where(x => x.Exam_type == 1)
                                                          select new DigiChamps.Models.Digichamps.Offline_exam_Pre_Requisite_test1
                                                          {
                                                              Exam_ID = a.Exam_ID,

                                                              Attempt_nos = a.Attempt_nos,
                                                              Exam_Name = a.Exam_Name,
                                                              Chapter_Id = a.Chapter_Id,
                                                              Time = a.Time,
                                                              Exam_type = a.Exam_type,
                                                              Subject_Id = a.Subject_Id,
                                                              Question_nos = a.Question_nos,
                                                              student_attempt = a.stu_Attempt_nos,
                                                              Participants = a.Participants
                                                          }).ToList(),

                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                            {
                                error = new Digichamps.ErrorResponse_Exam
                                {
                                    Message = "No exams Found."
                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                    }
                }
                catch
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "Something went wrong."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            else
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "Please provide data correctly."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }
        [HttpGet]
        public HttpResponseMessage Get_Exam_Offline(int? id, int? eid)//redg id cbt
        {
            if (id != null)
            {
                try
                {
                    var chaptersubscribes = DbContext.SP_DC_Getchapter(id).Distinct().ToList();
                    if (chaptersubscribes.Count > 0)
                    {

                        var data_pre = DbContext.SP_DC_Offline_exams(id, 1).Where(x => x.Chapter_Id == eid).ToList();

                        var data_prac = DbContext.SP_DC_Offline_exams(id, 3).Where(x => x.Chapter_Id == eid).ToList();

                        var data_Retest = DbContext.SP_DC_Offline_exams(id, 2).Where(x => x.Chapter_Id == eid).ToList();

                        var data_Online = DbContext.SP_DC_Offline_exams(id, 5).Where(x => x.Chapter_Id == eid).ToList();

                        var data_Sche = DbContext.SP_DC_Offline_exams(id, 4).Where(x => x.Chapter_Id == eid).ToList();

                        var examdetails = DbContext.SP_ExamList(id, 5).ToList();

                        var examdetails1 = examdetails.Where(x => x.Chapter_Id == eid).ToList();
                        var data_pre1 = data_pre.Where(x => x.Chapter_Id == eid).ToList();
                        var data_prac1 = data_prac.Where(x => x.Chapter_Id == eid).ToList();
                        var data_Retest1 = data_Retest.Where(x => x.Chapter_Id == eid).ToList();
                        var data_Sche1 = data_Sche.Where(x => x.Chapter_Id == eid).ToList();
                        var data_Online1 = data_Online.Where(x => x.Chapter_Id == eid).ToList();
                        string status = string.Empty;
                        var data = (from c in DbContext.tbl_DC_Package_Dtl.Where(x => x.Chapter_Id == eid && x.Is_Active == true && x.Is_Deleted == false)
                                    join d in DbContext.tbl_DC_Package.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                    on c.Package_ID equals d.Package_ID

                                    select new DigiChamps.Models.Digichamps.Is_Offline_Result
                                    {
                                        Is_offline = d.Is_Offline
                                    }).FirstOrDefault();
                        if (data != null)
                        {
                            status = Convert.ToString(data.Is_offline);
                        }
                        if (examdetails1.Count > 0 || data_pre1.Count > 0 || data_Retest1.Count > 0 || data_prac1.Count > 0 || data_Sche1.Count > 0 || data_Online1.Count > 0)
                        {
                            var obj = new DigiChamps.Models.Digichamps.SuccessResult_Exam
                            {
                                success = new Digichamps.SuccessResponse_Exam
                                {
                                    Is_Offline = status,
                                    offline_Exam = new Digichamps.Offline_success
                                    {
                                        Total_Requisite_test = data_pre.Count,
                                        Total_Practice_Test = data_prac.Count,
                                        Total_Retest = data_Retest.Count,

                                        Pre_Requisite_test = (from a in data_pre
                                                              select new DigiChamps.Models.Digichamps.Offline_exam_Pre_Requisite_test
                                                              {
                                                                  Exam_ID = a.Exam_ID,

                                                                  Attempt_nos = a.Attempt_nos,
                                                                  Exam_Name = a.Exam_Name,
                                                                  Chapter_Id = a.Chapter_Id,
                                                                  Chapter = a.Chapter,
                                                                  Time = a.Time,
                                                                  Exam_type = a.Exam_type,
                                                                  Subject_Id = a.Subject_Id,
                                                                  Subject = a.Subject,
                                                                  Question_nos = a.Question_nos,
                                                                  max_attempt = a.max_attempt,
                                                                  Participants = a.Participants
                                                              }).ToList(),
                                        Practice = (from b in data_prac
                                                    select new DigiChamps.Models.Digichamps.Offline_exam_Practice
                                                    {
                                                        Exam_ID = b.Exam_ID,

                                                        Attempt_nos = b.Attempt_nos,
                                                        Exam_Name = b.Exam_Name,
                                                        Chapter_Id = b.Chapter_Id,
                                                        Chapter = b.Chapter,
                                                        Time = b.Time,
                                                        Exam_type = b.Exam_type,
                                                        Subject_Id = b.Subject_Id,
                                                        Subject = b.Subject,
                                                        Question_nos = b.Question_nos,
                                                        max_attempt = b.max_attempt,
                                                        Participants = b.Participants
                                                    }).ToList(),
                                        Re_Test = (from c in data_Retest
                                                   select new DigiChamps.Models.Digichamps.Offline_exam_Re_Test
                                                   {
                                                       Exam_ID = c.Exam_ID,

                                                       Attempt_nos = c.Attempt_nos,
                                                       Exam_Name = c.Exam_Name,
                                                       Chapter_Id = c.Chapter_Id,
                                                       Chapter = c.Chapter,
                                                       Time = c.Time,
                                                       Exam_type = c.Exam_type,
                                                       Subject_Id = c.Subject_Id,
                                                       Subject = c.Subject,
                                                       Question_nos = c.Question_nos,
                                                       max_attempt = c.max_attempt,
                                                       Participants = c.Participants
                                                   }).ToList()
                                    }
                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, obj);

                        }
                        else
                        {
                            var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                            {
                                error = new Digichamps.ErrorResponse_Exam
                                {
                                    Message = "No exams Found."
                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                    }
                    else
                    {
                        var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                        {
                            error = new Digichamps.ErrorResponse_Exam
                            {
                                Message = "No exams Found."
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    //else
                    //{
                    //    var free_test = DbContext.Sp_DC_Free_Exam(id).ToList();
                    //    if (free_test.Count > 0)
                    //    {
                    //        var obj = new DigiChamps.Models.Digichamps.Success_freeexam
                    //        {
                    //            success = new Digichamps.SuccessResponse_Free_Exam
                    //            {
                    //                Onine_Exam_List = (from c in free_test
                    //                             select new DigiChamps.Models.Digichamps.Free_Exam_List
                    //                             {
                    //                                 Exam_ID = c.Exam_ID,
                    //                                 Exam_Name = c.Exam_Name,
                    //                                 Subject_Id = c.Subject_Id,
                    //                                 Question_nos = c.Question_nos,
                    //                                 Attempt_nos = c.Attempt_nos,
                    //                                 Time = c.Time,
                    //                                 Validity = c.Validity,
                    //                                 stu_Attempt_nos = c.Attempt_nos,
                    //                             }).ToList()

                    //            }
                    //        };
                    //        return Request.CreateResponse(HttpStatusCode.OK, obj);
                    //    }
                    //    else
                    //    {
                    //        var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    //        {
                    //            error = new Digichamps.ErrorResponse_Exam
                    //            {
                    //                Message = "No exams Found."
                    //            }
                    //        };
                    //        return Request.CreateResponse(HttpStatusCode.OK, obj);
                    //    }
                    //}
                }
                catch
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "Something went wrong."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            else
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "Please provide data correctly."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }

        [HttpGet]
        public HttpResponseMessage Get_Questions(int? id, int? eid)
        {
            int exam_typ = 0;
            if (id != null && eid != null)
            {
                try
                {


                    var exam = DbContext.tbl_DC_Exam.Where(x => x.Exam_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    var count = DbContext.tbl_DC_Exam_Result.Where(x => x.Regd_ID == eid && x.Exam_ID == id).ToList().Count();
                    if (exam != null)
                    {
                        if (Convert.ToInt32(count) == Convert.ToInt32(exam.Attempt_nos))
                        {
                            var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                            {
                                error = new Digichamps.ErrorResponse_Exam
                                {
                                    Message = "Your no of attemp has been exceeded."
                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }

                        tbl_DC_Exam_Result result = new tbl_DC_Exam_Result();
                        result.Exam_ID = id;
                        result.StartTime = today;
                        result.Exam_Name = exam.Exam_Name;
                        DbContext.tbl_DC_Exam_Result.Add(result);
                        DbContext.SaveChanges();


                        exam_typ = Convert.ToInt32(exam.Exam_type);
                        var getdata = DbContext.SP_DC_Procedurefor_test(id, eid, exam_typ).ToList();
                        if (getdata.Count > 0)
                        {
                            var obj = new DigiChamps.Models.Digichamps.successexamstart
                            {
                                success = new Digichamps.examstart
                                {
                                    Start_Time = today,
                                    End_Time = today.AddMinutes(Convert.ToInt32(exam.Time)),
                                    Result_ID = result.Result_ID,
                                    ExamData = (from c in DbContext.SP_DC_Procedurefor_test(id, eid, exam_typ)
                                                select new DigiChamps.Models.Digichamps.examstart_data
                                                {

                                                    RowID = c.RowID,
                                                    question_id = c.question_id,
                                                    Board_Id = c.Board_Id,
                                                    Class_Id = c.Class_Id,
                                                    Subject_Id = c.Subject_Id,
                                                    ch_id = c.ch_id,
                                                    topicId = c.topicId,
                                                    power_id = c.power_id,
                                                    question = c.question,
                                                    Qustion_Desc = c.Qustion_Desc,

                                                    Image = (from i in DbContext.tbl_DC_Question_Images.Where(x => x.Is_Active == true && x.Is_Deleted == false && x.Question_ID == (Int32)c.question_id)
                                                             select new DigiChamps.Models.Digichamps.Question_image
                                                             {
                                                                 Question_desc_Image = i.Question_desc_Image == null ? "" : "/Content/Qusetion/" + i.Question_desc_Image

                                                             }).ToList(),
                                                    Options = (from o in DbContext.tbl_DC_Question_Answer.Where(x => x.Is_Active == true && x.Is_Deleted == false && x.Question_ID == (Int32)c.question_id)
                                                               select new DigiChamps.Models.Digichamps.Question_options
                                                               {
                                                                   Answer_ID = o.Answer_ID,
                                                                   Question_ID = o.Question_ID,
                                                                   Option_Desc = o.Option_Desc,
                                                                   Option_Image = o.Option_Image == null ? "" : "/Content/Qusetion/" + o.Option_Image,
                                                                   Answer_desc = o.Answer_desc,
                                                                   Answer_Image = o.Answer_Image == null ? "" : "/Content/Qusetion/" + o.Answer_Image,
                                                                   Is_Answer = o.Is_Answer
                                                               }).ToList()
                                                }).ToList()
                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                            {
                                error = new Digichamps.ErrorResponse_Exam
                                {
                                    Message = "No questions left."
                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }



                    }
                    else
                    {
                        var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                        {
                            error = new Digichamps.ErrorResponse_Exam
                            {
                                Message = "Wrong data given."
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                }

                catch (Exception ex)
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "Something went wrong."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            else
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "Please provide data correctly."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }

        }



        [HttpGet]
        public HttpResponseMessage GetExamQuestions(int eid, int regid)
        {
           
                try
                {



                    var exam = DbContext.tbl_DC_Exam.Where(x => x.Exam_ID == eid && x.Is_Active == true && x.Is_Deleted == false
                        ).FirstOrDefault();

                    var erc = DbContext.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regid && x.Exam_ID == eid).ToList().Count();
                    List<int> first =DbContext.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regid && x.Exam_ID ==eid).Select(x=>x.Result_ID).ToList();

                    if (first == null)
                        first = new List<int>();

                    List<int?> erids = first.Cast<int?>().ToList();
                    if (exam != null)
                    {
                        int? type = exam.Exam_type;

                        List<int?> eqidsEasy = null;
                        List<int?> eqidsMedium = null;
                        List<int?> eqidsDiffyculty = null;

                        List<int> qIdsEasy = null;
                        List<int> qIdsMedium = null;
                        List<int> qIdsDiffyculty = null;

                        List<int?> qIdsTotal =null;


                        int? easyCount = DbContext.tbl_DC_Exam_Power.Where(x => x.Exam_ID == eid
                                && x.Power_Id == 1).Select(x => x.No_Of_Qstn).FirstOrDefault();


                        int? mediumCount = DbContext.tbl_DC_Exam_Power.Where(x => x.Exam_ID == eid
                                && x.Power_Id == 2).Select(x => x.No_Of_Qstn).FirstOrDefault();


                        int? diffycultyCount = DbContext.tbl_DC_Exam_Power.Where(x => x.Exam_ID == eid
                                && x.Power_Id == 3).Select(x => x.No_Of_Qstn).FirstOrDefault();

                        if (erids != null && erids.Count>0)
                        {
                            if(type==1)
                            eqidsEasy=(from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                Where(x=>erids.Contains(x.Result_ID))
                                       join b in DbContext.tbl_DC_Question.Where(x => x.Power_ID == 1 &&
                                           x.Is_PreRequisite == true && x.Is_Active == true
                                           && x.Is_Deleted == false && x.Chapter_Id == exam.Chapter_Id)
                                on a.Question_ID equals b.Question_ID
                                               select new
                                               {
                                                     Question_ID=a.Question_ID
                                               }



                                                   ).Select(x => x.Question_ID).ToList();
                            else
                                eqidsEasy = (from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                  Where(x => erids.Contains(x.Result_ID))
                                             join b in DbContext.tbl_DC_Question.Where(x => x.Power_ID == 1 && x.Is_online == true
                                                 && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == exam.Chapter_Id)
                                             on a.Question_ID equals b.Question_ID
                                             select new
                                             {
                                                 Question_ID = a.Question_ID
                                             }



                                                   ).Select(x => x.Question_ID).ToList();


                            //eqidsEasy = DbContext.tbl_DC_Exam_Result_Dtl.
                             //   Where(x=>x.Power_ID==1&&erids.Contains(x.Result_ID)).Select(x => x.Question_ID).ToList();
                            if(type==1)
                            eqidsMedium = (from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                 Where(x=>erids.Contains(x.Result_ID))
                                           join b in DbContext.tbl_DC_Question.Where(x => x.Power_ID == 2 && x.Is_PreRequisite == true
                                               && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == exam.Chapter_Id)
                                           on a.Question_ID equals b.Question_ID
                                           select new
                                           {
                                               Question_ID = a.Question_ID
                                           }



                                                   ).Select(x => x.Question_ID).ToList();
                            else
                                eqidsMedium = (from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                 Where(x => erids.Contains(x.Result_ID))
                                               join b in DbContext.tbl_DC_Question.Where(x => x.Power_ID == 2 && x.Is_online == true
                                                   && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == exam.Chapter_Id)
                                               on a.Question_ID equals b.Question_ID
                                               select new
                                               {
                                                   Question_ID = a.Question_ID
                                               }



                                                   ).Select(x => x.Question_ID).ToList();



                                //= DbContext.tbl_DC_Exam_Result_Dtl.
                               //Where(x => x.Power_ID == 2 && erids.Contains(x.Result_ID)).Select(x => x.Question_ID).ToList();

                            if(type==1)
                            eqidsDiffyculty =
                                (from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                 Where(x => erids.Contains(x.Result_ID))
                                 join b in DbContext.tbl_DC_Question.Where(x => x.Power_ID == 3
                                     && x.Is_PreRequisite == true && x.Is_Active == true && x.Is_Deleted == false
                                     && x.Chapter_Id == exam.Chapter_Id)
                                 on a.Question_ID equals b.Question_ID
                                 select new
                                 {
                                     Question_ID = a.Question_ID
                                 }



                                                   ).Select(x => x.Question_ID).ToList();
                            else
                                eqidsDiffyculty =
                                                                (from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                                                 Where(x => erids.Contains(x.Result_ID))
                                                                 join b in DbContext.tbl_DC_Question.Where(x => x.Power_ID == 3 &&
                                                                     x.Is_online == true && x.Is_Active == true && x.Is_Deleted == false
                                                                     && x.Chapter_Id == exam.Chapter_Id)
                                                                 on a.Question_ID equals b.Question_ID
                                                                 select new
                                                                 {
                                                                     Question_ID = a.Question_ID
                                                                 }



                                                                                   ).Select(x => x.Question_ID).ToList();
                                //DbContext.tbl_DC_Exam_Result_Dtl.
                               //Where(x => x.Power_ID == 3 && erids.Contains(x.Result_ID)).Select(x => x.Question_ID).ToList();
                        }


                        if (erc == null)
                            erc = 0;
                        

                        int count = Convert.ToInt32(erc);


                        if(type==1)
                        {
                                 if (count == 1)
                                 {
                                     qIdsTotal = new List<int?>();
                                     
                               
                                     int? rid = 0;
                                     rid = erids[0];
                                     //qIdsTotal = DbContext.tbl_DC_Exam_Result_Dtl.Where(x => x.Result_ID == rid).
                                     //    Select(x => x.Question_ID).ToList();
                                     if (type == 1)
                                         qIdsTotal = (from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                         Where(x => x.Result_ID == rid)
                                                      join b in DbContext.tbl_DC_Question.Where(x => x.Is_PreRequisite == true
                                                       && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == exam.Chapter_Id)
                                                      on a.Question_ID equals b.Question_ID
                                                      select new
                                                      {
                                                          Question_ID = a.Question_ID
                                                      }



                                                           ).Select(x => x.Question_ID).ToList();
                                     else
                                         qIdsTotal = (from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                         Where(x => x.Result_ID == rid)
                                                      join b in DbContext.tbl_DC_Question.Where(x => x.Is_online == true
                                                       && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == exam.Chapter_Id)
                                                      on a.Question_ID equals b.Question_ID
                                                      select new
                                                      {
                                                          Question_ID = a.Question_ID
                                                      }



                                                           ).Select(x => x.Question_ID).ToList();

                                 }
                                 else
                                 {
                                     qIdsTotal = new List<int?>();
                                     if (erids != null && erids.Count > 0)
                                     {

                                         if (type == 1)
                                             qIdsEasy = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                             && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 1 && x.Is_PreRequisite == true &&
                                             eqidsEasy.Contains(x.Question_ID) && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Question_ID).Take(Convert.ToInt32(easyCount)).ToList();
                                         else
                                             qIdsEasy = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                            && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 1 && x.Is_online == true &&
                                            !eqidsEasy.Contains(x.Question_ID) && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Question_ID).Take(Convert.ToInt32(easyCount)).ToList();

                                         if (type == 1)
                                             qIdsMedium = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                        && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 2 && x.Is_PreRequisite == true &&
                                        eqidsMedium.Contains(x.Question_ID) && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Question_ID).Take(Convert.ToInt32(mediumCount)).ToList();
                                         else
                                             qIdsMedium = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                       && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 2 && x.Is_online == true &&
                                       !eqidsMedium.Contains(x.Question_ID) && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Question_ID).Take(Convert.ToInt32(mediumCount)).ToList();


                                         if (type == 1)
                                             qIdsDiffyculty = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                      && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 3 && x.Is_PreRequisite == true &&
                                      eqidsDiffyculty.Contains(x.Question_ID) && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Question_ID).Take(Convert.ToInt32(diffycultyCount)).ToList();
                                         else
                                             qIdsDiffyculty = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                      && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 3 && x.Is_online == true &&
                                      !eqidsDiffyculty.Contains(x.Question_ID) && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Question_ID).Take(Convert.ToInt32(diffycultyCount)).ToList();





                                         qIdsTotal.AddRange(qIdsEasy.Cast<int?>().ToList());
                                         qIdsTotal.AddRange(qIdsMedium.Cast<int?>().ToList());
                                         qIdsTotal.AddRange(qIdsDiffyculty.Cast<int?>().ToList());
                                     }

                                     else
                                     {
                                         if (type == 1)
                                             qIdsEasy = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                                                            && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 1 && x.Is_PreRequisite == true
                                                                             && x.Is_Active == true && x.Is_Deleted == false)
                                                                            .Select(x => x.Question_ID).Take(Convert.ToInt32(easyCount)).ToList();
                                         else
                                             qIdsEasy = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                                                        && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 1 && x.Is_online == true
                                                                         && x.Is_Active == true && x.Is_Deleted == false)
                                                                        .Select(x => x.Question_ID).Take(Convert.ToInt32(easyCount)).ToList();

                                         if (type == 1)
                                             qIdsMedium = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                                                       && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 2 && x.Is_PreRequisite == true
                                                                        && x.Is_Active == true && x.Is_Deleted == false)
                                                                       .Select(x => x.Question_ID).Take(Convert.ToInt32(mediumCount)).ToList();
                                         else
                                             qIdsMedium = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                                                      && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 2 && x.Is_online == true
                                                                       && x.Is_Active == true && x.Is_Deleted == false)
                                                                      .Select(x => x.Question_ID).Take(Convert.ToInt32(mediumCount)).ToList();


                                         if (type == 1)
                                             qIdsDiffyculty = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                                                       && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 3 && x.Is_PreRequisite == true
                                                                        && x.Is_Active == true && x.Is_Deleted == false)
                                                                       .Select(x => x.Question_ID).Take(Convert.ToInt32(diffycultyCount)).ToList();
                                         else
                                             qIdsDiffyculty = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                                                                                           && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 3
                                                                                                           && x.Is_online == true
                                                                                                            && x.Is_Active == true && x.Is_Deleted == false)
                                                                                                           .Select(x => x.Question_ID).Take(Convert.ToInt32(diffycultyCount)).ToList();


                                         qIdsTotal.AddRange(qIdsEasy.Cast<int?>().ToList());
                                         qIdsTotal.AddRange(qIdsMedium.Cast<int?>().ToList());
                                         qIdsTotal.AddRange(qIdsDiffyculty.Cast<int?>().ToList());

                                     }


                                 }
                        }
                        else
                        if (count >= 3)
                        {
                            qIdsTotal=new List<int?>();
                            count = count + 1;
                            int remainder = count % 3;
int? rid=0;
                            switch (remainder)
                            {


                                   


                            //eqidsEasy = DbContext.tbl_DC_Exam_Result_Dtl.
                             //   Where(x=>x.Power_ID==1&&erids.Contains(x.Result_ID)).Select(x => x.Question_ID).ToList();
                           



                                //= DbContext.tbl_DC_Exam_Result_Dtl.
                               //Where(x => x.Power_ID == 2 && erids.Contains(x.Result_ID)).Select(x => x.Question_ID).ToList();

                            

                                case 0:
                                    rid=erids[2];
                                    //qIdsTotal = DbContext.tbl_DC_Exam_Result_Dtl.Where(x => x.Result_ID == rid).
                                    //    Select(x => x.Question_ID).ToList();
                                    if (type == 1)
                                        qIdsTotal = (from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                              Where(x => x.Result_ID == rid)
                                                     join b in DbContext.tbl_DC_Question.Where( x=>x.Is_PreRequisite == true
                                                      && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == exam.Chapter_Id)
                                                     on a.Question_ID equals b.Question_ID
                                                     select new
                                                     {
                                                         Question_ID = a.Question_ID
                                                     }



                                                               ).Select(x => x.Question_ID).ToList();
                                    else
                                        qIdsTotal = (from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                          Where(x => x.Result_ID == rid)
                                                     join b in DbContext.tbl_DC_Question.Where(x=>x.Is_online == true
                                                      && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == exam.Chapter_Id)
                                                     on a.Question_ID equals b.Question_ID
                                                     select new
                                                     {
                                                         Question_ID = a.Question_ID
                                                     }



                                                           ).Select(x => x.Question_ID).ToList();
                                    break;
                                case 1:
                                    rid = erids[0];
                                    //qIdsTotal = DbContext.tbl_DC_Exam_Result_Dtl.Where(x => x.Result_ID == rid).
                                    //    Select(x => x.Question_ID).ToList();
                             if(type==1)
                                 qIdsTotal = (from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                 Where(x => x.Result_ID == rid)
                                           join b in DbContext.tbl_DC_Question.Where(x =>  x.Is_PreRequisite == true
                                            && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == exam.Chapter_Id)
                                           on a.Question_ID equals b.Question_ID
                                           select new
                                           {
                                               Question_ID = a.Question_ID
                                           }



                                                   ).Select(x => x.Question_ID).ToList();
                            else
                                 qIdsTotal = (from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                 Where(x => x.Result_ID == rid)
                                               join b in DbContext.tbl_DC_Question.Where(x =>  x.Is_online == true
                                                && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == exam.Chapter_Id)
                                               on a.Question_ID equals b.Question_ID
                                               select new
                                               {
                                                   Question_ID = a.Question_ID
                                               }



                                                   ).Select(x => x.Question_ID).ToList();
                                    break;
                                case 2:
                                    rid = erids[1];
                                    //qIdsTotal = DbContext.tbl_DC_Exam_Result_Dtl.Where(x => x.Result_ID == rid).
                                    //    Select(x => x.Question_ID).ToList();
                            if(type==1)
                            qIdsTotal =
                                (from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                 Where(x => x.Result_ID==rid)
                                 join b in DbContext.tbl_DC_Question.Where(x =>  x.Is_PreRequisite == true
                                  && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == exam.Chapter_Id)
                                 on a.Question_ID equals b.Question_ID
                                 select new
                                 {
                                     Question_ID = a.Question_ID
                                 }



                                                   ).Select(x => x.Question_ID).ToList();
                            else
                                qIdsTotal =
                                                                (from a in DbContext.tbl_DC_Exam_Result_Dtl.
                                                                 Where(x => x.Result_ID==rid)
                                                                 join b in DbContext.tbl_DC_Question.Where(x =>  x.Is_online == true
                                                                  && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == exam.Chapter_Id)
                                                                 on a.Question_ID equals b.Question_ID
                                                                 select new
                                                                 {
                                                                     Question_ID = a.Question_ID
                                                                 }



                                                                                   ).Select(x => x.Question_ID).ToList();
                                    break;
                            }

                        }
                        else
                        {
                            qIdsTotal = new List<int?>();
                             if (erids != null && erids.Count>0)
                            {

                            if(type==1)
                                qIdsEasy = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 1 &&x.Is_PreRequisite==true&&
                                !eqidsEasy.Contains(x.Question_ID) && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Question_ID).Take(Convert.ToInt32(easyCount)).ToList();
                            else
                                qIdsEasy = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                               && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 1 &&x.Is_online==true&&
                               !eqidsEasy.Contains(x.Question_ID) && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Question_ID).Take(Convert.ToInt32(easyCount)).ToList();

                             if(type==1)
                                 qIdsMedium = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                            && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 2 &&x.Is_PreRequisite==true&&
                            !eqidsMedium.Contains(x.Question_ID) && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Question_ID).Take(Convert.ToInt32(mediumCount)).ToList();
                             else
                                 qIdsMedium = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                           && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 2 &&x.Is_online==true&&
                           !eqidsMedium.Contains(x.Question_ID) && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Question_ID).Take(Convert.ToInt32(mediumCount)).ToList();


                                 if(type==1)
                                     qIdsDiffyculty = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                              && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 3 &&x.Is_PreRequisite==true&&
                              !eqidsDiffyculty.Contains(x.Question_ID) && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Question_ID).Take(Convert.ToInt32(diffycultyCount)).ToList();
                                 else
                                     qIdsDiffyculty = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                              && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 3 && x.Is_online == true &&
                              !eqidsDiffyculty.Contains(x.Question_ID) && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Question_ID).Take(Convert.ToInt32(diffycultyCount)).ToList();





                            qIdsTotal.AddRange(qIdsEasy.Cast<int?>().ToList());
                            qIdsTotal.AddRange(qIdsMedium.Cast<int?>().ToList());
                            qIdsTotal.AddRange(qIdsDiffyculty.Cast<int?>().ToList());
                             }

                             else
                             {
                                 if(type==1)
                                 qIdsEasy = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                                                && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 1 && x.Is_PreRequisite == true
                                                                 && x.Is_Active == true && x.Is_Deleted == false)
                                                                .Select(x => x.Question_ID).Take(Convert.ToInt32(easyCount)).ToList();
                                 else
                                     qIdsEasy = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                                                && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 1&&x.Is_online==true
                                                                 && x.Is_Active == true && x.Is_Deleted == false)
                                                                .Select(x => x.Question_ID).Take(Convert.ToInt32(easyCount)).ToList();

                                 if (type == 1)
                                     qIdsMedium = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                                               && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 2 && x.Is_PreRequisite == true
                                                                && x.Is_Active == true && x.Is_Deleted == false)
                                                               .Select(x => x.Question_ID).Take(Convert.ToInt32(mediumCount)).ToList();
                                 else
                                     qIdsMedium = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                                              && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 2 && x.Is_online == true
                                                               && x.Is_Active == true && x.Is_Deleted == false)
                                                              .Select(x => x.Question_ID).Take(Convert.ToInt32(mediumCount)).ToList();


                                 if (type == 1)
                                     qIdsDiffyculty = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                                               && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 3 && x.Is_PreRequisite == true
                                                                && x.Is_Active == true && x.Is_Deleted == false)
                                                               .Select(x => x.Question_ID).Take(Convert.ToInt32(diffycultyCount)).ToList();
                                 else
                                     qIdsDiffyculty = DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == exam.Chapter_Id
                                                                                                   && x.Is_Active == true && x.Is_Deleted == false && x.Power_ID == 3 
                                                                                                   && x.Is_online == true
                                                                                                    && x.Is_Active == true && x.Is_Deleted == false)
                                                                                                   .Select(x => x.Question_ID).Take(Convert.ToInt32(diffycultyCount)).ToList();


                                 qIdsTotal.AddRange(qIdsEasy.Cast<int?>().ToList());
                                 qIdsTotal.AddRange(qIdsMedium.Cast<int?>().ToList());
                                 qIdsTotal.AddRange(qIdsDiffyculty.Cast<int?>().ToList());

                             }


                        }
                        tbl_DC_Exam_Result result = new tbl_DC_Exam_Result();
                        result.Exam_ID = eid;
                        result.StartTime = today;
                        result.Exam_Name = exam.Exam_Name;
                        DbContext.tbl_DC_Exam_Result.Add(result);
                        DbContext.SaveChanges();

                            var questions = DbContext.tbl_DC_Question.Where(x => qIdsTotal.Contains(x.Question_ID)).ToList();



                            var counter = 1;
                            var obj = new DigiChamps.Models.Digichamps.successexamstart
                            {
                                success = new Digichamps.examstart
                                {
                                    Start_Time = today,
                                    End_Time = today.AddMinutes(Convert.ToInt32(exam.Time)),
                                    Result_ID = result.Result_ID,
                                    ExamData = (from c in questions
                                                select new DigiChamps.Models.Digichamps.examstart_data
                                                {

                                                    RowID=counter++,
                                                    question_id = c.Question_ID,
                                                    Board_Id = c.Board_Id,
                                                    Class_Id = c.Class_Id,
                                                    Subject_Id = c.Subject_Id,
                                                    ch_id = c.Chapter_Id,
                                                    topicId = c.Topic_ID,
                                                    power_id = c.Power_ID,
                                                    question = c.Question,
                                                    Qustion_Desc = c.Qustion_Desc,

                                                    Image = (from i in DbContext.tbl_DC_Question_Images.
                                                                 Where(x => x.Is_Active == true && x.Is_Deleted == false
                                                                     && x.Question_ID == (Int32)c.Question_ID)
                                                             select new DigiChamps.Models.Digichamps.Question_image
                                                             {
                                                                 Question_desc_Image = i.Question_desc_Image == null ? "" : "/Content/Qusetion/" + i.Question_desc_Image

                                                             }).ToList(),
                                                    Options = (from o in DbContext.tbl_DC_Question_Answer.Where(x => x.Is_Active == true && x.Is_Deleted == false && x.Question_ID ==
                                                        (Int32)c.Question_ID)
                                                               select new DigiChamps.Models.Digichamps.Question_options
                                                               {
                                                                   Answer_ID = o.Answer_ID,
                                                                   Question_ID = o.Question_ID,
                                                                   Option_Desc = o.Option_Desc,
                                                                   Option_Image = o.Option_Image == null ? "" : "/Content/Qusetion/" + o.Option_Image,
                                                                   Answer_desc = o.Answer_desc,
                                                                   Answer_Image = o.Answer_Image == null ? "" : "/Content/Qusetion/" + o.Answer_Image,
                                                                   Is_Answer = o.Is_Answer
                                                               }).ToList()
                                                }).ToList()
                                }
                            };

                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                       else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "");
                    }
                   
                }

                catch (Exception ex)
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "Something went wrong."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
           
        }

        [HttpGet]
        public HttpResponseMessage Leader_Boards(int? id, int? eid)
        {
            try
            {
                int? examId = DbContext.tbl_DC_Exam_Result.Where(x => x.Result_ID == eid).Select(x => x.Exam_ID).FirstOrDefault();
                var examresult_cal = DbContext.SP_DC_Examresultcalulation(id, eid).ToList();
                var Leader = DbContext.SP_DC_lead(examId).ToList();
                if (Leader.Count > 0)
                {

                    if (Leader.Take(10).Where(x => x.Regd_ID == id).FirstOrDefault() == null)
                    {
                        int accuracy = Convert.ToInt32(Convert.ToDecimal(examresult_cal.
                            FirstOrDefault().Total_Correct_Ans) / Convert.ToDecimal(examresult_cal.FirstOrDefault().Question_Nos) * 100);
                        int count = Leader.FindIndex(X => X.Regd_ID == id);
                        if (count != -1)
                        {
                            int myrank = count + 1;
                            //Mylead = Leader.Where(x => x.Regd_ID == id).ToList();

                            var obj = new Success_leader
                            {
                                success = new Leader_Board
                                {
                                    Top10 = (from c in Leader.Take(10)
                                             select new Leader_result
                                             {

                                                 Max_rid = c.Max_rid,
                                                 Regd_ID = c.Regd_ID,
                                                 Totaltime = c.Totaltime,
                                                 Question_Nos = c.Question_Nos,
                                                 Total_Correct_Ans = c.Total_Correct_Ans,
                                                 Incorrect = (Convert.ToInt32(c.Question_Nos) - Convert.ToInt32(c.Total_Correct_Ans)),
                                                 Appear = c.Appear,
                                                 Customer_Name = c.Customer_Name,
                                                 Image = c.Image == null ? "" : "/Images/Profile/" + c.Image,
                                                 //Accuracy = (int)((c.Appear / c.Total_Correct_Ans) * 100)
                                                 //Accuracy = (int)((c.Total_Correct_Ans / c.Appear)* 100)
                                                // Accuracy = accuracy
                                             }).Take(10).ToList(),

                                    My_Result = (from c in Leader.Where(x => x.Regd_ID == id)
                                                 select new Myresult
                                                 {
                                                     Rank = myrank,
                                                     Max_rid = c.Max_rid,
                                                     Regd_ID = c.Regd_ID,
                                                     Totaltime = c.Totaltime,
                                                     Question_Nos = c.Question_Nos,
                                                     Total_Correct_Ans = c.Total_Correct_Ans,
                                                     Incorrect = (Convert.ToInt32(c.Question_Nos) - Convert.ToInt32(c.Total_Correct_Ans)),
                                                     Appear = c.Appear,
                                                     Customer_Name = c.Customer_Name,
                                                     Image = c.Image == null ? "" : "/Images/Profile/" + c.Image,
                                                     //Accuracy = accuracy
                                                 }).ToList()
                                }
                            };
                            for (int i = 0; i < obj.success.Top10.Count; i++)
                            {
                                if (obj.success.Top10[i].Appear > 0 && obj.success.Top10[i].Total_Correct_Ans >0)
                                    obj.success.Top10[i].Accuracy = (obj.success.Top10[i].Appear*100)/ obj.success.Top10[i].Total_Correct_Ans;

                            }
                            for (int i = 0; i < obj.success.My_Result.Count; i++)
                            {
                                if (obj.success.My_Result[i].Appear > 0 && obj.success.My_Result[i].Total_Correct_Ans > 0)
                                    obj.success.My_Result[i].Accuracy = (obj.success.My_Result[i].Appear *100)/ obj.success.My_Result[i].Total_Correct_Ans;

                            }

                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            Myresult ob = new Myresult();
                            var obj = new Success_leader
                            {
                                success = new Leader_Board
                                {
                                    Top10 = (from c in Leader.Take(10)
                                             select new Leader_result
                                             {
                                                 Max_rid = c.Max_rid,
                                                 Regd_ID = c.Regd_ID,
                                                 Totaltime = c.Totaltime,
                                                 Question_Nos = c.Question_Nos,
                                                 Total_Correct_Ans = c.Total_Correct_Ans,
                                                 Incorrect = (Convert.ToInt32(c.Question_Nos) - Convert.ToInt32(c.Total_Correct_Ans)),
                                                 Appear = c.Appear,
                                                 Customer_Name = c.Customer_Name,
                                                 Image = c.Image == null ? "" : "/Images/Profile/" + c.Image,
                                                 //Accuracy = accuracy
                                             }).Take(10).ToList(),
                                    My_Result = (from c in Leader.Where(x => x.Regd_ID == id)
                                                 select new Myresult
                                                 {
                                                     Max_rid = ob.Max_rid,
                                                     Regd_ID = ob.Regd_ID,
                                                     Totaltime = ob.Totaltime,
                                                     Question_Nos = ob.Question_Nos,
                                                     Total_Correct_Ans = ob.Total_Correct_Ans,
                                                     Incorrect = ob.Incorrect,
                                                     Appear = ob.Appear,
                                                     Customer_Name = ob.Customer_Name,
                                                     Image = ob.Image,
                                                     //Accuracy = accuracy
                                                 }).ToList()

                                }
                            };
                            for (int i = 0; i < obj.success.Top10.Count; i++)
                            {
                                if (obj.success.Top10[i].Appear > 0 && obj.success.Top10[i].Total_Correct_Ans > 0)
                                    obj.success.Top10[i].Accuracy = obj.success.Top10[i].Appear / obj.success.Top10[i].Total_Correct_Ans;

                            }
                            for (int i = 0; i < obj.success.My_Result.Count; i++)
                            {
                                if (obj.success.My_Result[i].Appear > 0 && obj.success.My_Result[i].Total_Correct_Ans > 0)
                                    obj.success.My_Result[i].Accuracy = obj.success.My_Result[i].Appear / obj.success.My_Result[i].Total_Correct_Ans;

                            }

                           
                            return Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                    }
                    else
                    {
                      

                        Myresult ob = new Myresult();
                        var obj = new Success_leader
                        {
                            success = new Leader_Board
                            {
                                Top10 = (from c in Leader.Take(10)
                                         join d in DbContext.tbl_DC_Exam_Result.Where(x=>x.Exam_ID==examId).OrderByDescending(x=>x.Total_Correct_Ans) on c.Regd_ID equals d.Regd_ID
                                         select new Leader_result 
                                         {
                                             Rank=Leader.FindIndex(X => X.Regd_ID == id)+1,
                                             Max_rid = c.Max_rid,
                                             Regd_ID = c.Regd_ID,
                                             Totaltime = c.Totaltime,
                                             Question_Nos = c.Question_Nos,
                                             Total_Correct_Ans = c.Total_Correct_Ans,
                                             Appear = c.Appear,
                                             Incorrect = (Convert.ToInt32(c.Question_Nos) - Convert.ToInt32(c.Total_Correct_Ans)),
                                             Customer_Name = c.Customer_Name,
                                             Image = c.Image == null ? "" : "/Images/Profile/" + c.Image,
                                             Accuracy = 0
                                             //Accuracy = (c.Appear / c.Total_Correct_Ans) * 100
                                             //Accuracy = (c.Total_Correct_Ans / c.Appear) * 100
                                         }).Take(10).ToList(),
                                My_Result = (from c in Leader
                                             join d in DbContext.tbl_DC_Exam_Result.Where(x => x.Exam_ID == examId&&x.Regd_ID==id).OrderByDescending(x => x.Total_Correct_Ans) on c.Regd_ID equals d.Regd_ID
                                             select new Myresult
                                             {
                                                 Max_rid = c.Max_rid,
                                                 Regd_ID = c.Regd_ID,
                                                 Totaltime =c.Totaltime,
                                                 Question_Nos =c.Question_Nos,
                                                 Total_Correct_Ans = c.Total_Correct_Ans,
                                                 Incorrect = (d.Question_Nos-d.Total_Correct_Ans),
                                                 Appear = c.Appear,
                                                 Customer_Name = c.Customer_Name,
                                                 Image = c.Image,
                                                 Accuracy=0
                                                 //Accuracy = (c.Appear / c.Total_Correct_Ans) * 100
                                                 //Accuracy = (c.Total_Correct_Ans / c.Appear) * 100
                                             }).ToList()

                            }
                        };


                        List<Leader_result> topTen=new List<Leader_result>();
                          for (int i = 0; i < obj.success.Top10.Count; i++)
                            {

                              Boolean status=false;
                              for(int j=0;j<topTen.Count;j++)
                               {
                                  if(topTen[j].Regd_ID.Equals(obj.success.Top10[i].Regd_ID))
                                  {
                                      if (topTen[j].Total_Correct_Ans > obj.success.Top10[i].Total_Correct_Ans)
                                      status = true;
                                  }
                               }

                                if(!status)
                                topTen.Add(obj.success.Top10[i]);

                              if( obj.success.Top10[i].Appear>0&& obj.success.Top10[i].Total_Correct_Ans>0)
                                obj.success.Top10[i].Accuracy = (obj.success.Top10[i].Appear*100) 
                                    / obj.success.Top10[i].Total_Correct_Ans;
                         
                            }
                          for (int i = 0; i < obj.success.My_Result.Count; i++)
                          {
                              if (obj.success.My_Result[i].Appear > 0 && obj.success.My_Result[i].Total_Correct_Ans > 0)
                              obj.success.My_Result[i].Accuracy = (obj.success.My_Result[i].Appear*100)
                                  / obj.success.My_Result[i].Total_Correct_Ans;

                          }
                          obj.success.Top10 = topTen;
                        return Request.CreateResponse(HttpStatusCode.OK, obj);
                    }


                }
                else
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "No one given exam."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }


            }
            catch
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "No exams Found."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }

        [HttpGet]
        public HttpResponseMessage Test_Result(int? id, int? eid)//registration Id/ Result Id
        {
            try
            {
                int? chapterId = DbContext.tbl_DC_Exam_Result_Dtl.Where(x => x.Result_ID ==eid).
                    Select(x => x.Chapter_Id).FirstOrDefault();
                var startegic = DbContext.SP_DC_Startegic_Report(eid).ToList();

                var examresult_cal = DbContext.SP_DC_Examresultcalulation(id, eid).ToList();

                var question = DbContext.SP_DC_Getallquestion_Appeard(eid);

                var get_Level = DbContext.SP_DC_Get_Power_Result(eid).ToList();

                var get_result_data = DbContext.tbl_DC_Exam_Result.Where(x => x.Result_ID == eid).FirstOrDefault();

                DateTime sdt = Convert.ToDateTime(get_result_data.StartTime);

                DateTime edt = Convert.ToDateTime(get_result_data.EndTime);

                var diff = DbContext.DifficultyBasisTestStatistics(eid);
                var subConcept = DbContext.SubConceptWiseTestStatistics(eid); 


               
            
                var hours = (edt - sdt).TotalMilliseconds;
                var Leader = DbContext.SP_DC_lead(eid).ToList();
                int count = Leader.FindIndex(X => X.Regd_ID == id);
                int? examId = DbContext.tbl_DC_Exam_Result.Where(x => x.Result_ID == eid).
                    Select(x => x.Exam_ID).FirstOrDefault();
                 var LeaderBoard = DbContext.SP_DC_LeaderBoard(examId).ToList();

                 int myRank = 0;
                 int lRank = 0;

                 if (LeaderBoard.Count == 1)
                    {
                        lRank = 1;
                    }
                    else
                    {
                        for (int i = 0; i < LeaderBoard.Count; i++)
                        {
                            if (LeaderBoard[i].Regd_ID == id)
                            {


                                lRank = (i + 1);

                                
                           }
                        }
                    }

                int accuracy = Convert.ToInt32(Convert.ToDecimal(examresult_cal.FirstOrDefault().Total_Correct_Ans) 
                    / Convert.ToDecimal(examresult_cal.FirstOrDefault().Question_Nos) * 100);
               
                if(accuracy>0&&accuracy<=35)
                {
                    myRank = 1;
                }
                else
                    if (accuracy > 35 && accuracy <= 60)
                    {
                        myRank = 2;
                    }
                    else
                if (accuracy > 60 && accuracy <= 75)
                {
                    myRank = 3;
                }
                else
                if (accuracy > 75 && accuracy <= 90)
                {
                    myRank = 4;
                }
                else
                if (accuracy > 90 && accuracy <= 100)
                {
                    myRank = 5;
                }

                
                tbl_DC_Exam exam = DbContext.tbl_DC_Exam.Where(x => x.Exam_ID == examId).FirstOrDefault();
                var obj = new Result_Succes
                {
                    success = new Result_Data
                    {
                        Question_Nos = examresult_cal.FirstOrDefault().Question_Nos,
                        Total_Correct_Ans = examresult_cal.FirstOrDefault().Total_Correct_Ans,
                        Total_Incorrec_Ans = examresult_cal.FirstOrDefault().Total_InCorrect_Ans,
                        Question_Attempted = examresult_cal.FirstOrDefault().Question_Attempted,
                        Unanswered = examresult_cal.FirstOrDefault().Question_Nos - examresult_cal.FirstOrDefault().Question_Attempted,
                        Totaltime = hours,
                        Exam_ID = get_result_data.Exam_ID,
                        wrong = Convert.ToInt32(examresult_cal.FirstOrDefault().wrong),
                        Accuracy = accuracy,
                        Performance = myRank,
                        Rank = lRank,
                        //Performance = "",
                        ChapterName=DbContext.tbl_DC_Chapter.Where(x => x.Chapter_Id ==chapterId).Select(x => x.Chapter).FirstOrDefault(),
                        TestName = DbContext.tbl_DC_Exam.Where(x => x.Exam_ID == exam.Exam_ID).Select(x => x.Exam_Name).FirstOrDefault(),
                        TestType = DbContext.tbl_DC_Exam_Type.Where(x => x.E_ID == exam.Exam_type).Select(x => x.Exam_Type).FirstOrDefault(),
                        difficulty=(from c in diff
                                       select new DifficultyData
                                       {
                                           PowerId = c.Power_ID,
                                           TotalQuestions = c.TotalQno,
                                           TotalCorrect = c.TotalCorrect,
                                            TotalInCorrect = c.TotalIncorrect,
                                             TotalSkipped = c.TotalSkipped,
                                             Accuracy=c.Accuracy
                                            
                                       }).ToList(),
                        subConcept = (from c in subConcept
                                      select new SubConceptData
                                      {
                                          SubConceptId = c.Topic_ID,
                                          SubConceptName = c.Topic_Name,
                                          TotalQuestions = c.TotalQuestions,
                                          TotalCorrect = c.TotalCorrect,
                                          TotalInCorrect = c.TotalInCorrect,
                                          TotalSkipped = c.TotalSkipped,
                                          Accuracy = c.Accuracy
                                      }).ToList(),
                        Difficultys = (from c in get_Level
                                       select new Difficulty
                                       {
                                           No_of_Question = c.No_of_Question,
                                           Power_ID = c.Power_ID,
                                           Power_Type = c.Power_Type
                                       }).ToList(),
                        Topic_details = (from d in startegic
                                         select new Startegic_Report_Result
                                         {
                                             Topic_ID = d.Topic_ID,
                                             Topic_Name = d.Topic_Name,
                                             Total_question = d.Total_question,
                                             Correct_answer = d.Correct_answer,
                                             Incorrect_answer = d.Incorrect_answer,
                                             Percentage = d.Percentage,
                                             Remark = d.Remark
                                         }).ToList()
                    }
                };
                int? total = 0;
                int? incorrect = 0;
                 int? correct = 0;
                int? skipped = 0;
                for (int i = 0; i < obj.success.subConcept.Count; i++)
                {
                    total = total+obj.success.subConcept[i].TotalQuestions;
                    incorrect = incorrect + obj.success.subConcept[i].TotalInCorrect;
                     correct = correct + obj.success.subConcept[i].TotalCorrect;
                    skipped = skipped + obj.success.subConcept[i].TotalSkipped;
                }

                obj.success.Question_Nos = total;
                obj.success.Total_Correct_Ans =correct;
                obj.success.Total_Incorrec_Ans = incorrect;
                obj.success.Total_Correct_Ans = correct;


                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            catch
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "No data Found."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }

        }

        //[HttpGet]
        //public HttpResponseMessage DifficultyBasisTestAnalysis(int? RegId, int? ExamId)
        //{

                
           

        //}
        public class RootObject2
        {
            public Get_all_question Get_question { get; set; }
        }
        public class Get_all_question
        {
            public int? Result_id { get; set; }
            public int? Student_id { get; set; }
            public int? Exam_id { get; set; }
            //public DateTime? Start_time { get; set; }

            public List<Questions> Question { get; set; }
        }
        public class Questions
        {
            public int Question_id { get; set; }
            public List<Answer> Answers { get; set; }
        }
        public class Answer
        {
            public int Answer_id { get; set; }
        }
        public class Result_success
        {
            public success_Result success { get; set; }
        }
        public class success_Result
        {
            public string Message { get; set; }
            public int? Result_id { get; set; }
        }
        //[HttpPost]
        //public HttpResponseMessage Submit_Test([FromBody] RootObject2 root)
        //{
        //    try
        //    {

        //        if (root == null)
        //        {
        //            var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
        //            {
        //                error = new Digichamps.ErrorResponse_Exam
        //                {
        //                    Message = "No data Found."
        //                }
        //            };
        //            return Request.CreateResponse(HttpStatusCode.OK, obj);
        //        }
        //        else
        //        {
        //            List<int> Question_attempted_list = new List<int>();
        //            List<int> Question_answer_real = new List<int>();
        //            List<int> Question_answer_given = new List<int>();
        //            bool equals = false;
        //            int not_atm_qsn = 0;
        //            int total_correct = 0;
        //            int Qsn_atemp = 0;
        //            var Get_result = DbContext.tbl_DC_Exam_Result.Where(x => x.Result_ID == root.Get_question.Result_id).FirstOrDefault();
        //            var Get_student_data = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == root.Get_question.Student_id).FirstOrDefault();
        //            var Get_exam = DbContext.tbl_DC_Exam.Where(x => x.Exam_ID == root.Get_question.Exam_id).FirstOrDefault();
        //            DateTime End_time = today;
        //            var Question_lists = root.Get_question.Question.ToList();

        //            if (Get_result != null && Get_student_data != null && Get_exam != null)
        //            {
        //                foreach (var v in Question_lists)
        //                {
        //                    int Question_id = Convert.ToInt32(v.Question_id);
        //                    var Get_Question_Dtl = DbContext.tbl_DC_Question.Where(x => x.Question_ID == Question_id).FirstOrDefault();
        //                    var Answers = DbContext.tbl_DC_Question_Answer.Where(x => x.Question_ID == Question_id && x.Is_Answer == true && x.Is_Active == true && x.Is_Deleted == false).ToList();
        //                    foreach (var ans in Answers)
        //                    {
        //                        Question_answer_real.Add(ans.Answer_ID);

        //                    }
        //                    if (v.Answers.ToList().Count > 0)  //if answer  given or attempted
        //                    {
        //                        Qsn_atemp = Qsn_atemp + 1;

        //                        foreach (var qa in v.Answers)
        //                        {

        //                            Question_answer_given.Add(qa.Answer_id);
        //                            //check for multiple answer is correct or not
        //                            var set = new HashSet<int>(Question_answer_given);
        //                            equals = set.SetEquals(Question_answer_real);
        //                        }

        //                        if (equals == true)
        //                        {
        //                            //Answer is correct
        //                            total_correct = total_correct + 1;
        //                            tbl_DC_Exam_Result_Dtl _result_dtl = new tbl_DC_Exam_Result_Dtl();
        //                            _result_dtl.Result_ID = root.Get_question.Result_id;
        //                            _result_dtl.Question_ID = Question_id;
        //                            _result_dtl.Answer_ID = Question_answer_given[0];
        //                            _result_dtl.Is_Active = true;
        //                            _result_dtl.Is_Deleted = false;
        //                            _result_dtl.Is_Correct = true;
        //                            _result_dtl.Board_Id = Get_Question_Dtl.Board_Id;
        //                            _result_dtl.Class_Id = Get_Question_Dtl.Class_Id;
        //                            _result_dtl.Chapter_Id = Get_Question_Dtl.Chapter_Id;
        //                            _result_dtl.Subject_Id = Get_Question_Dtl.Subject_Id;
        //                            _result_dtl.Topic_ID = Get_Question_Dtl.Topic_ID;
        //                            _result_dtl.Question = Get_Question_Dtl.Question;
        //                            DbContext.tbl_DC_Exam_Result_Dtl.Add(_result_dtl);
        //                            DbContext.SaveChanges();
        //                            Question_answer_real.Clear();
        //                            Question_answer_given.Clear();
        //                        }
        //                        else
        //                        {
        //                            //Answer is incorrect

        //                            tbl_DC_Exam_Result_Dtl _result_dtl = new tbl_DC_Exam_Result_Dtl();
        //                            _result_dtl.Result_ID = root.Get_question.Result_id;
        //                            _result_dtl.Question_ID = Question_id;
        //                            _result_dtl.Is_Active = true;
        //                            _result_dtl.Is_Deleted = false;
        //                            _result_dtl.Is_Correct = false;
        //                            _result_dtl.Answer_ID = Question_answer_given[0];
        //                            _result_dtl.Board_Id = Get_Question_Dtl.Board_Id;
        //                            _result_dtl.Class_Id = Get_Question_Dtl.Class_Id;
        //                            _result_dtl.Chapter_Id = Get_Question_Dtl.Chapter_Id;
        //                            _result_dtl.Topic_ID = Get_Question_Dtl.Topic_ID;
        //                            _result_dtl.Question = Get_Question_Dtl.Question;
        //                            _result_dtl.Subject_Id = Get_Question_Dtl.Subject_Id;
        //                            DbContext.tbl_DC_Exam_Result_Dtl.Add(_result_dtl);
        //                            DbContext.SaveChanges();
        //                            Question_answer_real.Clear();
        //                            Question_answer_given.Clear();
        //                        }
        //                    }
        //                    else //if answer not  given
        //                    {
        //                        not_atm_qsn = not_atm_qsn + 1;
        //                        //if Question not attempted
        //                        //insert into exam Result_dtl
        //                        tbl_DC_Exam_Result_Dtl _result_dtl = new tbl_DC_Exam_Result_Dtl();
        //                        _result_dtl.Result_ID = root.Get_question.Result_id;
        //                        _result_dtl.Question_ID = Question_id;
        //                        _result_dtl.Is_Active = true;
        //                        _result_dtl.Is_Deleted = false;
        //                        _result_dtl.Is_Correct = false;
        //                        _result_dtl.Board_Id = Get_Question_Dtl.Board_Id;
        //                        _result_dtl.Class_Id = Get_Question_Dtl.Class_Id;
        //                        _result_dtl.Chapter_Id = Get_Question_Dtl.Chapter_Id;
        //                        _result_dtl.Option_Desc = "1";
        //                        _result_dtl.Subject_Id = Get_Question_Dtl.Subject_Id;
        //                        _result_dtl.Topic_ID = Get_Question_Dtl.Topic_ID;
        //                        _result_dtl.Question = Get_Question_Dtl.Question;
        //                        DbContext.tbl_DC_Exam_Result_Dtl.Add(_result_dtl);
        //                        DbContext.SaveChanges();
        //                        Question_answer_real.Clear();
        //                        Question_answer_given.Clear();
        //                    }

        //                }

        //                Get_result.Board_Id = Get_exam.Board_Id;
        //                Get_result.Subject_Id = Get_exam.Subject_Id;
        //                Get_result.Chapter_Id = Get_exam.Chapter_Id;
        //                Get_result.Class_Id = Get_exam.Class_Id;
        //                Get_result.EndTime = today;
        //                Get_result.Exam_ID = Get_exam.Exam_ID;
        //                Get_result.Question_Nos = Get_exam.Question_nos;
        //                Get_result.Question_Attempted = Qsn_atemp;
        //                Get_result.Total_Correct_Ans = total_correct;
        //                Get_result.Is_Active = true;
        //                Get_result.Is_Deleted = false;
        //                Get_result.Regd_ID = Get_student_data.Regd_ID;
        //                Get_result.Regd_No = Get_student_data.Regd_No;
        //                DbContext.SaveChanges();

        //                tbl_DC_CoinEarn ob = new tbl_DC_CoinEarn();
        //                ob.TypeId = Get_result.Result_ID;
        //                ob.RegdId = Get_student_data.Regd_ID;
        //                ob.CoinType = 4;
        //                ob.Active = true;
        //                ob.Coins = 2*total_correct;
        //                ob.InsertedOn = DateTime.Now;
        //                DbContext.tbl_DC_CoinEarn.Add(ob);
        //                DbContext.SaveChanges();

        //                try
        //                {
        //                    var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == Get_student_data.Regd_ID)

        //                                   select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
        //                    string body = "Resultid#{{resid}}#Examid#{{examid}}#{{name}}";
        //                    string msg = body.ToString().Replace("{{name}}", Get_student_data.Customer_Name).Replace("{{correctno}}", total_correct.ToString()).Replace("{{test}}", Get_exam.Exam_Name).Replace("{{resid}}", root.Get_question.Result_id.ToString()).Replace("{{examid}}", root.Get_question.Exam_id.ToString());

        //                   // string body = "Test#Hello {{name}}! You've successfully submitted the test. Analyze your performance, see where you stand in the leaderboard and check on your weak sub-concepts.";
        //                    //string msg = body.ToString().Replace("{{name}}", Get_student_data.Customer_Name);

        //                    if (pushnot != null)
        //                    {
        //                        if (pushnot.Device_id != null)
        //                        {

                                  

        //                            var note = new PushNotiStatus(msg, pushnot.Device_id);
        //                        }
        //                    }
        //                }
        //                catch (Exception)
        //                {

        //                }
        //                var obj = new Result_success
        //                {
        //                    success = new success_Result
        //                    {
        //                        Message = "Test submitted sucessfully.",
        //                        Result_id = root.Get_question.Result_id

        //                    }
        //                };
        //                return Request.CreateResponse(HttpStatusCode.OK, obj);
        //            }
        //            else
        //            {
        //                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
        //                {
        //                    error = new Digichamps.ErrorResponse_Exam
        //                    {
        //                        Message = "No data Found."
        //                    }
        //                };
        //                return Request.CreateResponse(HttpStatusCode.OK, obj);
        //            }
        //        }



        //    }
        //    catch
        //    {
        //        var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
        //        {
        //            error = new Digichamps.ErrorResponse_Exam
        //            {
        //                Message = "No data Found."
        //            }
        //        };
        //        return Request.CreateResponse(HttpStatusCode.OK, obj);
        //    }
        //}
        
        //[HttpPost]
        //public HttpResponseMessage Submit_Test([FromBody] RootObject2 root)
        //{
        //    try
        //    {

        //        if (root == null)
        //        {
        //            var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
        //            {
        //                error = new Digichamps.ErrorResponse_Exam
        //                {
        //                    Message = "No data Found."
        //                }
        //            };
        //            return Request.CreateResponse(HttpStatusCode.OK, obj);
        //        }
        //        else
        //        {
        //            List<int> Question_attempted_list = new List<int>();
        //            List<int> Question_answer_real = new List<int>();
        //            List<int> Question_answer_given = new List<int>();
        //            bool equals = false;
        //            int not_atm_qsn = 0;
        //            int total_correct = 0;
        //            int Qsn_atemp = 0;
        //            var Get_result = DbContext.tbl_DC_Exam_Result.Where(x => x.Result_ID == root.Get_question.Result_id).FirstOrDefault();
        //            var Get_student_data = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == root.Get_question.Student_id).FirstOrDefault();
        //            var Get_exam = DbContext.tbl_DC_Exam.Where(x => x.Exam_ID == root.Get_question.Exam_id).FirstOrDefault();
        //            DateTime End_time = today;
        //            var Question_lists = root.Get_question.Question.ToList();

        //            if (Get_result != null && Get_student_data != null && Get_exam != null)
        //            {
        //                foreach (var v in Question_lists)
        //                {
        //                    int Question_id = Convert.ToInt32(v.Question_id);
        //                    var Get_Question_Dtl = DbContext.tbl_DC_Question.Where(x => x.Question_ID == Question_id).FirstOrDefault();
        //                    var Answers = DbContext.tbl_DC_Question_Answer.Where(x => x.Question_ID == Question_id && x.Is_Answer == true && x.Is_Active == true && x.Is_Deleted == false).ToList();
        //                    foreach (var ans in Answers)
        //                    {
        //                        Question_answer_real.Add(ans.Answer_ID);

        //                    }
        //                    if (v.Answers.ToList().Count > 0)  //if answer  given or attempted
        //                    {
        //                        Qsn_atemp = Qsn_atemp + 1;

        //                        foreach (var qa in v.Answers)
        //                        {

        //                            Question_answer_given.Add(qa.Answer_id);
        //                            //check for multiple answer is correct or not
        //                            var set = new HashSet<int>(Question_answer_given);
        //                            equals = set.SetEquals(Question_answer_real);
        //                        }

        //                        if (equals == true)
        //                        {
        //                            //Answer is correct
        //                            total_correct = total_correct + 1;
        //                            tbl_DC_Exam_Result_Dtl _result_dtl = new tbl_DC_Exam_Result_Dtl();
        //                            _result_dtl.Result_ID = root.Get_question.Result_id;
        //                            _result_dtl.Question_ID = Question_id;
        //                            _result_dtl.Is_Active = true;
        //                            _result_dtl.Is_Deleted = false;
        //                            _result_dtl.Is_Correct = true;
        //                            _result_dtl.Board_Id = Get_Question_Dtl.Board_Id;
        //                            _result_dtl.Class_Id = Get_Question_Dtl.Class_Id;
        //                            _result_dtl.Chapter_Id = Get_Question_Dtl.Chapter_Id;
        //                            _result_dtl.Topic_ID = Get_Question_Dtl.Topic_ID;
        //                            _result_dtl.Question = Get_Question_Dtl.Question;
        //                            DbContext.tbl_DC_Exam_Result_Dtl.Add(_result_dtl);
        //                            DbContext.SaveChanges();
        //                            Question_answer_real.Clear();
        //                            Question_answer_given.Clear();
        //                        }
        //                        else
        //                        {
        //                            //Answer is incorrect

        //                            tbl_DC_Exam_Result_Dtl _result_dtl = new tbl_DC_Exam_Result_Dtl();
        //                            _result_dtl.Result_ID = root.Get_question.Result_id;
        //                            _result_dtl.Question_ID = Question_id;
        //                            _result_dtl.Is_Active = true;
        //                            _result_dtl.Is_Deleted = false;
        //                            _result_dtl.Is_Correct = false;
        //                            _result_dtl.Board_Id = Get_Question_Dtl.Board_Id;
        //                            _result_dtl.Class_Id = Get_Question_Dtl.Class_Id;
        //                            _result_dtl.Chapter_Id = Get_Question_Dtl.Chapter_Id;
        //                            _result_dtl.Topic_ID = Get_Question_Dtl.Topic_ID;
        //                            _result_dtl.Question = Get_Question_Dtl.Question;
        //                            DbContext.tbl_DC_Exam_Result_Dtl.Add(_result_dtl);
        //                            DbContext.SaveChanges();
        //                            Question_answer_real.Clear();
        //                            Question_answer_given.Clear();
        //                        }
        //                    }
        //                    else //if answer not  given
        //                    {
        //                        not_atm_qsn = not_atm_qsn + 1;
        //                        //if Question not attempted
        //                        //insert into exam Result_dtl
        //                        tbl_DC_Exam_Result_Dtl _result_dtl = new tbl_DC_Exam_Result_Dtl();
        //                        _result_dtl.Result_ID = root.Get_question.Result_id;
        //                        _result_dtl.Question_ID = Question_id;
        //                        _result_dtl.Is_Active = true;
        //                        _result_dtl.Is_Deleted = false;
        //                        _result_dtl.Is_Correct = false;
        //                        _result_dtl.Board_Id = Get_Question_Dtl.Board_Id;
        //                        _result_dtl.Class_Id = Get_Question_Dtl.Class_Id;
        //                        _result_dtl.Chapter_Id = Get_Question_Dtl.Chapter_Id;
        //                        _result_dtl.Topic_ID = Get_Question_Dtl.Topic_ID;
        //                        _result_dtl.Question = Get_Question_Dtl.Question;
        //                        DbContext.tbl_DC_Exam_Result_Dtl.Add(_result_dtl);
        //                        DbContext.SaveChanges();
        //                        Question_answer_real.Clear();
        //                        Question_answer_given.Clear();
        //                    }

        //                }

        //                Get_result.Board_Id = Get_exam.Board_Id;
        //                Get_result.Chapter_Id = Get_exam.Chapter_Id;
        //                Get_result.Class_Id = Get_exam.Class_Id;
        //                Get_result.EndTime = today;
        //                Get_result.Exam_ID = Get_exam.Exam_ID;
        //                Get_result.Question_Nos = Get_exam.Question_nos;
        //                Get_result.Question_Attempted = Qsn_atemp;
        //                Get_result.Total_Correct_Ans = total_correct;
        //                Get_result.Is_Active = true;
        //                Get_result.Is_Deleted = false;
        //                Get_result.Regd_ID = Get_student_data.Regd_ID;
        //                Get_result.Regd_No = Get_student_data.Regd_No;
        //                DbContext.SaveChanges();
        //                try
        //                {
        //                    var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == Get_student_data.Regd_ID)

        //                                   select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
        //                    string body = "Resultid#{{resid}}#Examid#{{examid}}#  Exam Appeared : Hello {{name}},! You have attempted {{correctno}} Correct Answers in {{test}} exam. View your test details and leader board now.";
        //                    string msg = body.ToString().Replace("{{name}}", Get_student_data.Customer_Name).Replace("{{correctno}}", total_correct.ToString()).Replace("{{test}}", Get_exam.Exam_Name).Replace("{{resid}}", root.Get_question.Result_id.ToString()).Replace("{{examid}}", root.Get_question.Exam_id.ToString());

        //                    if (pushnot != null)
        //                    {
        //                        if (pushnot.Device_id != null)
        //                        {
        //                            var note = new PushNotiStatus(msg, pushnot.Device_id);
        //                        }
        //                    }
        //                }
        //                catch (Exception)
        //                {

        //                }
        //                var obj = new Result_success
        //                {
        //                    success = new success_Result
        //                    {
        //                        Message = "Test submitted sucessfully.",
        //                        Result_id = root.Get_question.Result_id

        //                    }
        //                };
        //                return Request.CreateResponse(HttpStatusCode.OK, obj);
        //            }
        //            else
        //            {
        //                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
        //                {
        //                    error = new Digichamps.ErrorResponse_Exam
        //                    {
        //                        Message = "No data Found."
        //                    }
        //                };
        //                return Request.CreateResponse(HttpStatusCode.OK, obj);
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
        //        {
        //            error = new Digichamps.ErrorResponse_Exam
        //            {
        //                Message = "No data Found."
        //            }
        //        };
        //        return Request.CreateResponse(HttpStatusCode.OK, obj);
        //    }
        //}
        //[HttpGet]
        //public HttpResponseMessage Get_quest()
        //{
        //    var obj = new RootObject2
        //    {
        //        Get_question = new Get_all_question
        //        {
        //            Exam_id = 19,
        //            Student_id = 3,
        //            Result_id = 2010,
        //            Start_time = today,
        //            Question = (from c in DbContext.tbl_DC_Question.Where(x => x.Chapter_Id == 1).Take(9).ToList()
        //                        select new Questions
        //                            {
        //                                Question_id = c.Question_ID,
        //                                Answers = (from d in DbContext.tbl_DC_Question_Answer.Where(x => x.Question_ID == c.Question_ID && x.Is_Answer == true).ToList()
        //                                           select new Answer
        //                                               {
        //                                                   Answer_id=d.Answer_ID
        //                                               }).ToList()
        //                            }).ToList()

        //        }
        //    };
        //    return Request.CreateResponse(HttpStatusCode.OK,obj);
        //}     



        [HttpPost]
        public HttpResponseMessage Submit_Test([FromBody] RootObject2 root)
        {
            try
            {

                if (root == null)
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "No data Found."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
                else
                {
                    List<int> Question_attempted_list = new List<int>();
                    List<int> Question_answer_real = new List<int>();
                    List<int> Question_answer_given = new List<int>();
                    bool equals = false;
                    int not_atm_qsn = 0;
                    int total_correct = 0;
                    int Qsn_atemp = 0;
                    var Get_result = DbContext.tbl_DC_Exam_Result.Where(x => x.Result_ID == root.Get_question.Result_id).FirstOrDefault();
                    var Get_student_data = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == root.Get_question.Student_id).FirstOrDefault();
                    var Get_exam = DbContext.tbl_DC_Exam.Where(x => x.Exam_ID == root.Get_question.Exam_id).FirstOrDefault();

                    var Chapter = DbContext.tbl_DC_Chapter.Where(x => x.Chapter_Id == Get_exam.Chapter_Id).FirstOrDefault();

                    DateTime End_time = today;
                    var Question_lists = root.Get_question.Question.ToList();

                    if (Get_result != null && Get_student_data != null && Get_exam != null)
                    {
                        foreach (var v in Question_lists)
                        {
                            int Question_id = Convert.ToInt32(v.Question_id);
                            var Get_Question_Dtl = DbContext.tbl_DC_Question.Where(x => x.Question_ID == Question_id).FirstOrDefault();
                            var Answers = DbContext.tbl_DC_Question_Answer.Where(x => x.Question_ID == Question_id && x.Is_Answer == true && x.Is_Active == true && x.Is_Deleted == false).ToList();
                            foreach (var ans in Answers)
                            {
                                Question_answer_real.Add(ans.Answer_ID);

                            }
                            if (v.Answers.ToList().Count > 0)  //if answer  given or attempted
                            {
                                Qsn_atemp = Qsn_atemp + 1;

                                foreach (var qa in v.Answers)
                                {

                                    Question_answer_given.Add(qa.Answer_id);
                                    //check for multiple answer is correct or not
                                    var set = new HashSet<int>(Question_answer_given);
                                    equals = set.SetEquals(Question_answer_real);
                                }

                                if (equals == true)
                                {
                                    //Answer is correct
                                    total_correct = total_correct + 1;
                                    tbl_DC_Exam_Result_Dtl _result_dtl = new tbl_DC_Exam_Result_Dtl();
                                    _result_dtl.Result_ID = root.Get_question.Result_id;
                                    _result_dtl.Question_ID = Question_id;
                                    _result_dtl.Answer_ID = Question_answer_given[0];
                                    _result_dtl.Is_Active = true;
                                    _result_dtl.Is_Deleted = false;
                                    _result_dtl.Is_Correct = true;
                                    _result_dtl.Board_Id = Get_Question_Dtl.Board_Id;
                                    _result_dtl.Class_Id = Get_Question_Dtl.Class_Id;
                                    _result_dtl.Chapter_Id = Get_Question_Dtl.Chapter_Id;
                                    _result_dtl.Subject_Id = Get_Question_Dtl.Subject_Id;
                                    _result_dtl.Topic_ID = Get_Question_Dtl.Topic_ID;
                                    _result_dtl.Question = Get_Question_Dtl.Question;
                                    DbContext.tbl_DC_Exam_Result_Dtl.Add(_result_dtl);
                                    DbContext.SaveChanges();
                                    Question_answer_real.Clear();
                                    Question_answer_given.Clear();
                                }
                                else
                                {
                                    //Answer is incorrect

                                    tbl_DC_Exam_Result_Dtl _result_dtl = new tbl_DC_Exam_Result_Dtl();
                                    _result_dtl.Result_ID = root.Get_question.Result_id;
                                    _result_dtl.Question_ID = Question_id;
                                    _result_dtl.Is_Active = true;
                                    _result_dtl.Is_Deleted = false;
                                    _result_dtl.Is_Correct = false;
                                    _result_dtl.Answer_ID = Question_answer_given[0];
                                    _result_dtl.Board_Id = Get_Question_Dtl.Board_Id;
                                    _result_dtl.Class_Id = Get_Question_Dtl.Class_Id;
                                    _result_dtl.Chapter_Id = Get_Question_Dtl.Chapter_Id;
                                    _result_dtl.Topic_ID = Get_Question_Dtl.Topic_ID;
                                    _result_dtl.Question = Get_Question_Dtl.Question;
                                    _result_dtl.Subject_Id = Get_Question_Dtl.Subject_Id;
                                    DbContext.tbl_DC_Exam_Result_Dtl.Add(_result_dtl);
                                    DbContext.SaveChanges();
                                    Question_answer_real.Clear();
                                    Question_answer_given.Clear();
                                }
                            }
                            else //if answer not  given
                            {
                                not_atm_qsn = not_atm_qsn + 1;
                                //if Question not attempted
                                //insert into exam Result_dtl
                                tbl_DC_Exam_Result_Dtl _result_dtl = new tbl_DC_Exam_Result_Dtl();
                                _result_dtl.Result_ID = root.Get_question.Result_id;
                                _result_dtl.Question_ID = Question_id;
                                _result_dtl.Is_Active = true;
                                _result_dtl.Is_Deleted = false;
                                _result_dtl.Is_Correct = false;
                                _result_dtl.Board_Id = Get_Question_Dtl.Board_Id;
                                _result_dtl.Class_Id = Get_Question_Dtl.Class_Id;
                                _result_dtl.Chapter_Id = Get_Question_Dtl.Chapter_Id;
                                _result_dtl.Option_Desc = "1";
                                _result_dtl.Subject_Id = Get_Question_Dtl.Subject_Id;
                                _result_dtl.Topic_ID = Get_Question_Dtl.Topic_ID;
                                _result_dtl.Question = Get_Question_Dtl.Question;
                                DbContext.tbl_DC_Exam_Result_Dtl.Add(_result_dtl);
                                DbContext.SaveChanges();
                                Question_answer_real.Clear();
                                Question_answer_given.Clear();
                            }

                        }

                        Get_result.Board_Id = Get_exam.Board_Id;
                        Get_result.Subject_Id = Get_exam.Subject_Id;
                        Get_result.Chapter_Id = Get_exam.Chapter_Id;
                        Get_result.Class_Id = Get_exam.Class_Id;
                        Get_result.EndTime = today;
                        Get_result.Exam_ID = Get_exam.Exam_ID;
                        Get_result.Question_Nos = Get_exam.Question_nos;
                        Get_result.Question_Attempted = Qsn_atemp;
                        Get_result.Total_Correct_Ans = total_correct;
                        Get_result.Is_Active = true;
                        Get_result.Is_Deleted = false;
                        Get_result.Regd_ID = Get_student_data.Regd_ID;
                        Get_result.Regd_No = Get_student_data.Regd_No;
                        DbContext.SaveChanges();

                       

                        try
                        {

                            int id = Get_result.Result_ID;
                            tbl_DC_CoinEarn ob = new tbl_DC_CoinEarn();
                            ob.TypeId = Get_result.Result_ID;
                            ob.RegdId = Get_student_data.Regd_ID;
                            ob.CoinType = 4;
                            ob.Active = true;
                            ob.Coins = (DbContext.tbl_DC_CoinType.Where(a=>a.CoinTypeId==4).FirstOrDefault().Coins) * total_correct;
                            ob.InsertedOn = DateTime.Now;
                            DbContext.tbl_DC_CoinEarn.Add(ob);
                            DbContext.SaveChanges();
                            var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == Get_student_data.Regd_ID)

                                           select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                           // string body = "Resultid#{{resid}}#Examid#{{examid}}#{{name}}";
                           // string msg = body.ToString().Replace("{{name}}",
                            //    Get_student_data.Customer_Name).Replace("{{correctno}}", 
                             //   total_correct.ToString()).Replace("{{test}}",
                             //   Get_exam.Exam_Name).Replace("{{resid}}", 
                             //   root.Get_question.Result_id.ToString()).Replace("{{examid}}", root.Get_question.Exam_id.ToString());
                            string type="";
                            if(Get_exam.Exam_type==1)
                                type="PRT";
                            else
                                 type="CBT";

                             string body = "ER#{{rid}}#Test#Hello {{name}}! You've successfully submitted the "+type+" test in "+Chapter.Chapter+
                                 ". Analyze your performance, see where you stand in the leaderboard and check on your weak sub-concepts.";
                            string msg = body.ToString().Replace("{{name}}", Get_student_data.Customer_Name)
                                .Replace("{{rid}}", id+"");

                            if (pushnot != null)
                            {
                                if (pushnot.Device_id != null)
                                {

                                    //DataMessage dm = new DataMessage();
                                    //dm.type = NotificationType.TEST_RESULT;

                                    //NotificationMessage nm = new NotificationMessage();
                                    //nm.title = "Test Result";
                                    //nm.text = "Test submitted successfully";

                                    //dm.notificationObject = nm;
                                    //dm.messageObject = Get_result.Result_ID;


                                    var note = new PushNotiStatus( msg,pushnot.Device_id);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;

                        }
                        var obj = new Result_success
                        {
                            success = new success_Result
                            {
                                Message = "Test submitted sucessfully.",
                                Result_id = root.Get_question.Result_id

                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                        {
                            error = new Digichamps.ErrorResponse_Exam
                            {
                                Message = "No data Found."
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                }



            }
            catch
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "No data Found."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }

        public class SuccessForReview
        {
            public SuccessResultForQuestionReview SuccessReviewQuestion { get; set; }
        }
        public class SuccessResultForQuestionReview
        {
            public List<QuestionListReview> QuestionReviewList { get; set; }
        }

        public class QuestionListReview
        {
            public string Question { get; set; }
            public string Image { get; set; }
            public List<OptionReview> Options { get; set; }
            public AnswerId Answers { get; set; }
        }

        public class OptionReview
        {
            public string Option { get; set; }
            public string Image { get; set; }
            public int OptionID { get; set; }
        }

        public class AnswerId
        {
            public int? UserAnswerId { get; set; }
            public int? CorrectAnswerId { get; set; }

            public string Answer_desc { get; set; }
            public string Answer_Image { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage QuestionReview(int id)
        {
            var Get_ResultDetails = DbContext.tbl_DC_Exam_Result.Where(x => x.Result_ID == id).FirstOrDefault();
            var GetResultDTL = DbContext.tbl_DC_Exam_Result_Dtl.Where(x => x.Result_ID == id).ToList();
            if (Get_ResultDetails != null)
            {
                var objs = new SuccessForReview
                {
                    SuccessReviewQuestion = new SuccessResultForQuestionReview
                    {
                        QuestionReviewList = (from c in GetResultDTL
                                              select new QuestionListReview
                                              {
                                                  Question = c.Question,
                                                  Image=DbContext.tbl_DC_Question_Images.Where(x=>x.Question_ID==c.Question_ID).
                                                  FirstOrDefault()==null?null:DbContext.tbl_DC_Question_Images.Where(x=>x.Question_ID==c.Question_ID).Select(x=>x.Question_desc_Image)
                                                  .FirstOrDefault(),
                                                  Options = (from op in DbContext.tbl_DC_Question_Answer.
                                                                 Where(x => x.Question_ID == c.Question_ID).ToList()
                                                             select new OptionReview
                                                             {
                                                                 Option = op.Option_Desc,
                                                                 OptionID = op.Answer_ID,
                                                                 Image = op.Option_Image,
                                                             }).ToList(),
                                                  Answers = new AnswerId
                                                  {
                                                      CorrectAnswerId = DbContext.tbl_DC_Question_Answer.Where
                                                      (x => x.Question_ID == c.Question_ID && x.Is_Answer == true).
                                                      FirstOrDefault().Answer_ID,
                                                      UserAnswerId = c.Answer_ID==null?0:c.Answer_ID,
                                                      Answer_desc = DbContext.tbl_DC_Question_Answer.Where
                                                      (x => x.Question_ID == c.Question_ID && x.Is_Answer == true).
                                                      FirstOrDefault().Answer_desc,
                                                      Answer_Image = DbContext.tbl_DC_Question_Answer.Where
                                                      (x => x.Question_ID == c.Question_ID && x.Is_Answer == true).
                                                      FirstOrDefault().Answer_Image

                                                  }
                                              }).ToList()
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, objs);
            }
            else
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "No result found."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }

        }





        [HttpGet]
        public HttpResponseMessage LeaderBoards(int? id, int? eid)
        {
            try
            {
                int? examId = DbContext.tbl_DC_Exam_Result.Where(x => x.Result_ID == eid).
                    Select(x => x.Exam_ID).FirstOrDefault();
              
                var Leader = DbContext.SP_DC_LeaderBoard(examId).ToList();
                if (Leader.Count > 0)
                {
                    int myRank = 0;
                    
                    List<Leader_result> leaders = new List<Leader_result>();
                    List<Myresult> myResult=new List<Myresult>();
                    if (Leader.Count == 1)
                    {
                        myRank = 1;
                    }
                    else
                    {
                        for (int i = 0; i < Leader.Count; i++)
                        {



                            if (Leader[i].Regd_ID == id)
                            {
                                Myresult myResul = new Myresult();

                                myResul.Max_rid = Leader[i].Max_rid;
                                myResul.Regd_ID = Leader[i].Regd_ID;
                                myResul.Total_Correct_Ans = Leader[i].Total_Correct_Ans;
                                myResul.Question_Nos = Leader[i].Question_Nos;
                                myResul.Appear = Leader[i].Appear;
                                myResul.Incorrect = Leader[i].Question_Nos - Leader[i].Total_Correct_Ans;

                                myResul.Totaltime = Leader[i].Totaltime;

                                myResul.Customer_Name = Leader[i].Customer_Name;
                                myResul.Accuracy = Leader[i].Accuracy;

                                myResul.Image = Leader[i].Image;
                                if (myRank == 1)
                                    myResul.Rank = myRank;
                                else
                                    myResul.Rank = (i + 1);

                                myResult.Add(myResul);
                            }
                        }
                    }

                    int size = Leader.Count;

                    if (Leader.Count > 10)
                    {
                        size = 10;
                    }
                    




                    for (int i = 0; i < size; i++)
                    {

                        Leader_result result = new Leader_result();
                        result.Max_rid = Leader[i].Max_rid;
                        result.Regd_ID = Leader[i].Regd_ID;
                        result.Total_Correct_Ans = Leader[i].Total_Correct_Ans;
                        result.Question_Nos = Leader[i].Question_Nos;
                        result.Appear = Leader[i].Appear;
                        result.Incorrect = Leader[i].Question_Nos-Leader[i].Total_Correct_Ans;
                        result.StartTime = Leader[i].StartTime;
                        result.Totaltime = Leader[i].Totaltime;

                        result.Customer_Name = Leader[i].Customer_Name;
                        result.Accuracy = Leader[i].Accuracy;

                        result.Image = Leader[i].Image;
                        result.Rank = (i + 1);
                        leaders.Add(result);

                        if (myResult.Count==0&&Leader[i].Regd_ID == id)
                        {
                            Myresult myResul = new Myresult();

                            myResul.Max_rid = Leader[i].Max_rid;
                            myResul.Regd_ID = Leader[i].Regd_ID;
                            myResul.Total_Correct_Ans = Leader[i].Total_Correct_Ans;
                            myResul.Question_Nos = Leader[i].Question_Nos;
                            myResul.Appear = Leader[i].Appear;
                            myResul.Incorrect = Leader[i].Question_Nos - Leader[i].Total_Correct_Ans;

                            myResul.Totaltime = Leader[i].Totaltime;

                            myResul.Customer_Name = Leader[i].Customer_Name;
                            myResul.Accuracy = Leader[i].Accuracy;

                            myResul.Image = Leader[i].Image;
                            if (myRank==1)
                                myResul.Rank = myRank;
                            else
                                myResul.Rank = (i + 1);

                            myResult.Add(myResul);
                        }

                    }


                       if(myResult.Count==0)
                       {
                           var MyRes = DbContext.SP_DC_MyRankInLeaderBoard(examId, id).ToList() ;

                           Myresult myResul = new Myresult();

                           myResul.Max_rid = MyRes[0].Max_rid;
                           myResul.Regd_ID = MyRes[0].Regd_ID;
                           myResul.Total_Correct_Ans = MyRes[0].Total_Correct_Ans;
                           myResul.Question_Nos = MyRes[0].Question_Nos;
                           myResul.Appear = MyRes[0].Appear;
                           myResul.Incorrect = MyRes[0].Question_Nos - MyRes[0].Total_Correct_Ans;

                           myResul.Totaltime = MyRes[0].Totaltime;

                           myResul.Customer_Name = MyRes[0].Customer_Name;
                           myResul.Accuracy = MyRes[0].Accuracy;

                           myResul.Image = MyRes[0].Image;
                           if (myRank == 1)
                               myResul.Rank = myRank;
                           else
                               myResul.Rank = 1;

                           myResult.Add(myResul);
                       }
                   
                   
                        var obj = new Success_leader
                        {
                            success = new Leader_Board
                            {
                                Top10 = leaders,
                                My_Result =myResult 

                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, obj);
                    //}
                    //else
                    //{

                    //    //List<Leader_result> accuracy=leaders.
                    //    //    OrderByDescending(c => c.Total_Correct_Ans).ThenByDescending(c => c.Accuracy).
                    //    //    ThenBy(c => c.Totaltime).ThenBy(c => c.StartTime).ToList();
                       
                    //    return Request.CreateResponse(HttpStatusCode.OK, accuracy);

                    //}



                }
                else
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "No one given exam."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }


            }
            catch
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "No exams Found."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }





         public class PsychometricExamResult
        {
              public int ExamId { set; get; }
              public int RegId { set; get; }

            

              public DateTime? StartTime { set; get; }


              public DateTime? StopTime { set; get; }
              
             public List<PsychometricQuestionItem> questions { set; get; }
        }
     

        public class PsychometricExamItemList
        {
            public List<PsychometricExamItem> success { set; get; }
        }

        public class PsychometricExamItem
        {
            public int ExamId { set; get; }
            public string ExamName { set; get; }
        }



        public class PsychometricExamQuestions
        {
            public List<PsychometricQuestionItem> success { set; get; }
        }

        public class PsychometricQuestionItem
        {
            public int QuestionId { set; get; }
            public string Question { set; get; }
            public string QuestionDesc { set; get; }

            public string QuestionImage { set; get; }


            public int? Type { set; get; }
            public int? SubType { set; get; }
             public int AnsweredId { set; get; }

             public int ReviewedId { set; get; }
             public int SkippedId { set; get; }

            public List<PsychometricQuestionOptionItem> optionList { set; get; }

        }
        public class PsychometricQuestionOptionItem
        {
            public int OptionId { set; get; }

            public string Option { set; get; }

            public string OptionImage { set; get; }


            public bool? IsCorrect { set; get; }

            public int? Score { set; get; }

        }

        [HttpGet]
        public HttpResponseMessage GetPsychometricExamList(int? studentId)//redg id
        {
            if (studentId != null)
            {
                try
                {
                    var exams = DbContext.tbl_DC_Psychometric_Exam.Where(x => x.Is_Active == true).ToList();

                    if (exams != null)
                    {
                        var obj = new PsychometricExamItemList
                        {
                            success = (from a in exams
                                            select new PsychometricExamItem
                                            {
                                                ExamId = a.Exam_ID,
                                                ExamName = a.Exam_Name,
                                            }).ToList()
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                        {
                            error = new Digichamps.ErrorResponse_Exam
                            {
                                Message = "No Exams Found"
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.NoContent, obj);
                    }

                }
                catch
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "Something went wrong."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }

            else
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "Please provide data correctly."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }


        public class PsychometricMessage
        {
            public string AcademicMessage {set; get;}
            public string InterpersonalMessage {set; get;}

        }



        [HttpGet]
        public HttpResponseMessage GetPsychometricExam(int? examId)//redg id
        {
            if (examId != null)
            {
                try
                {
                      var obj = new PsychometricExamQuestions
                        {
                            success = (from a in DbContext.tbl_DC_Psychometric_Question
                                       select new PsychometricQuestionItem
                                       {
                                           QuestionId=a.Question_ID,
                                           Question = a.Question,
                                           QuestionDesc = a.Qustion_Desc,
                                           QuestionImage = DbContext.tbl_DC_Psychometric_Question_Images
                                           .Where(x=>x.Question_ID==a.Question_ID).Select(x=>x.Question_desc_Image).
                                           FirstOrDefault(),
                                           Type = a.Type,
                                           SubType = a.SubType,
                                           optionList = (from b in DbContext.
                                                             tbl_DC_Psychometric_Question_Answer.
                                                             Where(x=>x.Question_ID==a.Question_ID)
                                       select new PsychometricQuestionOptionItem
                                       {
                                         OptionId=b.Answer_ID,
                                         Option = b.Option_Desc,
                                         OptionImage = b.Option_Image,
                                         IsCorrect = b.Is_Answer,

                                       }).ToList(),
                                          
                                       }).ToList()
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, obj);
                   

                }
                catch
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "Something went wrong."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }

            else
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "Please provide data correctly."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }

        public class PsychometricResultSuccess
        {

            public PsychometricResult success;
        }

        public class PsychometricResult
        {
            public string AcademicMessage { set; get; }
            public string InterpersonalMessage { set; get; }

            public string SkippedMessage { set; get; }


            public string Academic { set; get; }
            public string Doubts { set; get; }
            public string Interest { set; get; }
            public string Time { set; get; }
            public string SelfStudy { set; get; }
            public string School { set; get; }
            public string Personal { set; get; }
            public string Mentorship { set; get; }

        }

        [HttpPost]
        public HttpResponseMessage PostPsychometricExamResult([FromBody]PsychometricExamResult result)//redg id
        {
            if (result != null)
            {
                try
                {


                    int questionAttempt=0;
                    int questionAns=0;

                      int academic1=0;
                     int academic2=0;
                     int academic3=0;
                    int interpersonal1=0;
                     int interpersonal2=0;
               
                    

                    for(int i=0;i<result.questions.Count;i++)
                    {
                        if(result.questions[i].AnsweredId>0)
                        {
                            questionAttempt=questionAttempt+1;


                             for(int j=0;j<result.questions[i].optionList.Count;j++)
                        {
                            if(result.questions[i].optionList[j].IsCorrect==true)
                            {
                                if(result.questions[i].optionList[j].OptionId==result.questions[i].AnsweredId)
                            {
                                questionAns=questionAns+1;

                                   if(result.questions[i].Type==3)

                                   {switch(result.questions[i].SubType)
                                    {
                                       case 6: academic1++;break; 
                                           case 7: academic2++;break; 
                                           case 8: academic3++;break; 
                                    
                                   }
                                   }
                                   else
                                   {
                                       if(result.questions[i].Type==4)

                                      {
                                           switch(result.questions[i].SubType)
                                    {
                                           case 9: interpersonal1++;break; 
                                           case 10: interpersonal2++;break; 
                                         
                                    }

                                       }


                            }
                        }

                       
                        }

                       


                    }
                        }
                    }


                    int statuscode = 0;
                    //+++
                    if (academic1 >= 3 && academic2 >= 3 && academic3 >= 3)
                    {
                        statuscode = 1;
                    }
                    else

                        if (academic1 >= 3 && academic2 >= 3 && academic3 < 3)
                        {
                            //++-
                            statuscode = 2;
                        }
                        else

                            if (academic1 >= 3 && academic2 < 3 && academic3 >= 3)
                            {
                                //+-+
                                statuscode = 3;

                            }
                            else

                                if (academic1 >= 3 && academic2 < 3 && academic3 < 3)
                                {
                                    //+--
                                    statuscode = 4;
                                }

                                else

                                    if (academic1 < 3 && academic2 >= 3 && academic3 >= 3)
                                    {
                                        //-++
                                        statuscode = 5;
                                    }

                                    else

                                        if (academic1 < 3 && academic2 < 3 && academic3 >= 3)
                                        {
                                            //--+
                                            statuscode = 6;
                                        }

                                        else

                                            if (academic1 < 3 && academic2 >= 3 && academic3 < 3)
                                            {
                                                //-+-
                                                statuscode = 7;
                                            }

                                            else

                                                if (academic1 < 3 && academic2 < 3 && academic3 < 3)
                                                {
                                                    //---
                                                    statuscode = 8;
                                                }

                    int interCode = 0;

                    if (academic1 >= 3 && academic2 >= 3)
                    {
                        //++
                        interCode = 1;
                    }

                    else

                        if (academic1 >= 3 && academic2 < 3)
                        {
                            //+-
                            interCode = 2;
                        }

                        else

                            if (academic1 < 3 && academic2 >= 3)
                            {
                                //-+
                                interCode = 3;
                            }

                            else

                                if (academic1 < 3 && academic2 < 3)
                                {
                                    //--
                                    interCode = 4;
                                }

                    string acdemic = statuscode + "";
                    string interpersonal = interCode + "";

                    tbl_DC_Psychometric_Exam_Result tblResult=new tbl_DC_Psychometric_Exam_Result();

                     tblResult.Exam_ID=result.ExamId;
                     tblResult.Regd_ID=result.RegId;
                     tblResult.StartTime=result.StartTime;
                     tblResult.EndTime=result.StopTime;
                     tblResult.Exam_ID=result.ExamId;
                  
                    tblResult.Question_Nos=result.questions.Count;
                     tblResult.Question_Attempted=questionAttempt;
                     tblResult.Total_Correct_Ans=questionAns;


                    tblResult.Is_Global=true;
                    tblResult.Modified_By="Admin";
                    tblResult.Modified_Date=DateTime.Now;
                    tblResult.Is_Active=true;
                    tblResult.Is_Deleted=false;
                    tblResult.AcademicId = statuscode+"";
                    tblResult.InterpersonalId = interCode+"";
                    
                    DbContext.tbl_DC_Psychometric_Exam_Result.Add(tblResult);

                    DbContext.SaveChanges();
                    for(int i=0;i<result.questions.Count;i++)
                    {
                       
                     tbl_DC_Psychometric_Exam_Result_Dtl rdtl=new tbl_DC_Psychometric_Exam_Result_Dtl();
                     rdtl.Answer_ID = result.questions[i].AnsweredId;
                        rdtl.Is_Active=true;
                         rdtl.Is_Deleted=false;
                        rdtl.Board_Id=0;
                        rdtl.Class_Id=0;
                         rdtl.Inserted_By="Admin";
                              rdtl.Modified_Date=DateTime.Now;

                         rdtl.Question_ID=result.questions[i].QuestionId;
                    rdtl.Result_ID=tblResult.Result_ID;
                        
                        DbContext.tbl_DC_Psychometric_Exam_Result_Dtl.Add(rdtl);
                    }

                    DbContext.SaveChanges();

                   
                  PsychometricResult r=   new PsychometricResult()
                    {

                        AcademicMessage=DbContext.tbl_DC_Psychometric_Result_Pattern.
                        Where(x=>x.Pattern_Code==acdemic&&x.PsychometricType==1).Select(x=>x.Comment).FirstOrDefault(),
                        InterpersonalMessage = DbContext.tbl_DC_Psychometric_Result_Pattern.
                        Where(x => x.Pattern_Code == interpersonal && x.PsychometricType == 2).Select(x => x.Comment).FirstOrDefault()
                    };

                  if (questionAttempt==0)
                      r.SkippedMessage = "please answer the questions for get the analysis";

                  var obj = new PsychometricResultSuccess()
                  {
                      success = r
                  };
                  return Request.CreateResponse(HttpStatusCode.OK, obj);

                }
                catch
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "Something went wrong."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }

            else
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "Please provide data correctly."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }


       



        [HttpGet]
        public HttpResponseMessage GetMentorshipExam(int? examId,int? regId)//redg id
        {
            if (examId != null)
            {
                int? resultCount = DbContext.tbl_DC_Mentor_Exam_Result.Where(x => x.Regd_ID == regId && x.Is_Active == true
                            && x.Is_Deleted == false).ToList().Count();


               
               
                try
                {
                    if (resultCount < 3)
                    {
                        var obj = new PsychometricExamQuestions
                        {
                            success = (from a in DbContext.tbl_DC_MentorshipExam_Question
                                       select new PsychometricQuestionItem
                                       {
                                           QuestionId = a.Question_ID,
                                           Question = a.Question,
                                           QuestionDesc = a.Qustion_Desc,
                                           QuestionImage = DbContext.tbl_DC_Mentorship_Question_Images
                                           .Where(x => x.Question_ID == a.Question_ID).Select(x => x.Question_desc_Image).
                                           FirstOrDefault(),
                                           Type = a.Type,
                                           SubType = a.SubType,
                                           optionList = (from b in DbContext.
                                                             tbl_DC_MentorshipExam_Question_Answer.
                                                             Where(x => x.Question_ID == a.Question_ID)
                                                         select new PsychometricQuestionOptionItem
                                                         {
                                                             OptionId = b.Answer_ID,
                                                             Option = b.Option_Desc,
                                                             OptionImage = b.Option_Image,
                                                             IsCorrect = b.Is_Answer,
                                                             Score = b.Score

                                                         }).ToList(),

                                       }).ToList()
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, obj);


                    }
                    else
                    {
                        var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                        {
                            error = new Digichamps.ErrorResponse_Exam
                            {
                                Message = "Max Limits Exceeded"
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.PreconditionFailed, obj);
                    }
                }
                catch
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "Something went wrong."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }


            else
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "Please provide data correctly."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }



        [HttpPost]
        public HttpResponseMessage PostMentorshipExamResult([FromBody]PsychometricExamResult result)//redg id
        {
            if (result != null)
            {
                try
                {


                    int questionAttempt = 0;
                    int questionAns = 0;

                    int academic1 = 0;
                    int academic2 = 0;
                    int academic3 = 0;
                    int interpersonal1 = 0;
                    int interpersonal2 = 0;



                    int? academic = 0;
                    int? doubts = 0;
                    int? interest = 0;
                    int? time = 0;
                    int? selfstudy = 0;
                    int? school = 0;
                    int? personal = 0;
                    int? mentorship = 0;


                    for (int i = 0; i < result.questions.Count; i++)
                    {
                        if (result.questions[i].AnsweredId > 0)
                        {
                            questionAttempt = questionAttempt + 1;


                            for (int j = 0; j < result.questions[i].optionList.Count; j++)
                            {
                               
                                    if (result.questions[i].optionList[j].OptionId == result.questions[i].AnsweredId)
                                    {
                                        questionAns = questionAns + 1;

                                        
                                            switch (result.questions[i].Type)
                                            {
                                                case 1: academic = academic + result.questions[i].optionList[j].Score; break;
                                                case 2: doubts = doubts + result.questions[i].optionList[j].Score; break;
                                                case 3: interest = interest + result.questions[i].optionList[j].Score; break;
                                                case 4: time = time + result.questions[i].optionList[j].Score; break;
                                                case 5: selfstudy = selfstudy + result.questions[i].optionList[j].Score; break;
                                                case 6: school = school + result.questions[i].optionList[j].Score; break;
                                                case 7: personal = personal + result.questions[i].optionList[j].Score; break;
                                                case 8: mentorship = mentorship + result.questions[i].optionList[j].Score; break;
                                               

                                            }
                                        
                                       
                                    }


                                




                            }
                        }
                    }

                     int? academicID = 0;
                    int? doubtsID = 0;
                    int? interestID = 0;
                    int? timeID = 0;
                    int? selfstudyID = 0;
                    int? schoolID = 0;
                    int? personalID = 0;
                    int? mentorshipID = 0;

                    
                   
                    if (academic==8||academic==9)
                    {
                        academicID = 98;
                    }
                    else
                        if (academic == 7 || academic == 6 || academic == 5)
                        {
                            academicID = 765;
                        }
                        else
                            if (academic == 4 || academic == 3)
                            {
                                academicID = 43;
                            }


                    if (doubts == 8 || doubts == 9)
                    {
                        doubtsID = 98;
                    }
                    else
                        if (doubts == 7 || doubts == 6 || doubts == 5)
                        {
                            doubtsID = 765;
                        }
                        else
                            if (doubts == 4 || doubts == 3)
                            {
                                doubtsID = 43;
                            }


                    if (interest == 8 || interest == 9)
                    {
                        interestID = 98;
                    }
                    else
                        if (interest == 7 || interest == 6 || interest == 5)
                        {
                            interestID = 765;
                        }
                        else
                            if (interest == 4 || interest == 3)
                            {
                                interestID = 43;
                            }



                    if (time == 8 || time == 9)
                    {
                        timeID = 98;
                    }
                    else
                        if (time == 7 || time == 6 || time == 5)
                        {
                            timeID = 765;
                        }
                        else
                            if (time == 4 || time == 3)
                            {
                                timeID = 43;
                            }


                    if (selfstudy == 8 || selfstudy == 9)
                    {
                        selfstudyID = 98;
                    }
                    else
                        if (selfstudy == 7 || selfstudy == 6 || selfstudy == 5)
                        {
                            selfstudyID = 765;
                        }
                        else
                            if (selfstudy == 4 || selfstudy == 3)
                            {
                                selfstudyID = 43;
                            }


                    if (school == 8 || school == 9)
                    {
                        schoolID = 98;
                    }
                    else
                        if (school == 7 || school == 6 || school == 5)
                        {
                            schoolID = 765;
                        }
                        else
                            if (school == 4 || school == 3)
                            {
                                schoolID = 43;
                            }



                    if (personal == 8 || personal == 9)
                    {
                        personalID = 98;
                    }
                    else
                        if (personal == 7 || personal == 6 || personal == 5)
                        {
                            personalID = 765;
                        }
                        else
                            if (personal == 4 || personal == 3)
                            {
                                personalID = 43;
                            }


                    if (mentorship == 8 || mentorship == 9)
                    {
                        mentorshipID = 98;
                    }
                    else
                        if (mentorship == 7 || mentorship == 6 || mentorship == 5)
                        {
                            mentorshipID = 765;
                        }
                        else
                            if (mentorship == 4 || mentorship == 3)
                            {
                                mentorshipID = 43;
                            }




                   

                    tbl_DC_Mentor_Exam_Result tblResult = new tbl_DC_Mentor_Exam_Result();

                    tblResult.Exam_ID = result.ExamId;
                    tblResult.Regd_ID = result.RegId;
                    tblResult.StartTime = result.StartTime;
                    tblResult.EndTime = result.StopTime;
                    tblResult.Exam_ID = result.ExamId;

                    tblResult.Question_Nos = result.questions.Count;
                    tblResult.Question_Attempted = questionAttempt;
                    tblResult.Total_Correct_Ans = questionAns;


                    tblResult.Is_Global = true;
                    tblResult.Modified_By = "Admin";
                    tblResult.Inserted_By = academic + ":" + doubts+":"+
                        interest + ":" + time + ":" + selfstudy + ":" + school + ":" + personal + ":" + mentorship;
                    tblResult.Modified_Date = DateTime.Now;
                    tblResult.Is_Active = true;
                    tblResult.Is_Deleted = false;
                    tblResult.AcademicId = academicID;
                    tblResult.DoubtId = doubtsID;
                    tblResult.InterestId = interestID;
                    tblResult.TimeManageId = timeID;
                    tblResult.SelfStudyId = selfstudyID;
                    tblResult.SchoolId = schoolID;
                    tblResult.PersonalId = personalID;
                    tblResult.MentorshipId = mentorshipID;

                    DbContext.tbl_DC_Mentor_Exam_Result.Add(tblResult);

                    DbContext.SaveChanges();
                     tbl_DC_Mentorship_Exam_Score score = new tbl_DC_Mentorship_Exam_Score();

                     score.ExamResultID = tblResult.Result_ID;
                     score.Academic = academic;
                     score.Doubt = doubts;
                     score.Interest = interest;
                     score.Time = time;
                     score.SelfStudy =selfstudy;
                     score.School = school;
                     score.Personal = personal;
                     score.Mentorship = mentorship;

                     score.Total = academic + doubts + interest + time + selfstudy + school + personal + mentorship;
                     DbContext.tbl_DC_Mentorship_Exam_Score.Add(score);

                     DbContext.SaveChanges();

                    for (int i = 0; i < result.questions.Count; i++)
                    {

                        tbl_DC_Mentorship_Exam_Result_Dtl rdtl = new tbl_DC_Mentorship_Exam_Result_Dtl();
                        rdtl.Answer_ID = result.questions[i].AnsweredId;
                        rdtl.Is_Active = true;
                        rdtl.Is_Deleted = false;
                        rdtl.Board_Id = 0;
                        rdtl.Class_Id = 0;
                        rdtl.Inserted_By = "Admin";
                        rdtl.Modified_Date = DateTime.Now;

                        rdtl.Question_ID = result.questions[i].QuestionId;
                        rdtl.Result_ID = tblResult.Result_ID;

                        DbContext.tbl_DC_Mentorship_Exam_Result_Dtl.Add(rdtl);
                    }

                    DbContext.SaveChanges();


                    PsychometricResult r = new PsychometricResult()
                    {

                        Academic = DbContext.tbl_DC_MentorshipExam_Result_Pattern.
                        Where(x => x.Pattern_Code == academicID && x.CategoryType == 1).Select(x => x.Comment).FirstOrDefault(),
                        Doubts = DbContext.tbl_DC_MentorshipExam_Result_Pattern.
                        Where(x => x.Pattern_Code == doubtsID && x.CategoryType == 2).Select(x => x.Comment).FirstOrDefault(),
                        Interest = DbContext.tbl_DC_MentorshipExam_Result_Pattern.
                       Where(x => x.Pattern_Code == interestID && x.CategoryType == 3).Select(x => x.Comment).FirstOrDefault(),
                        Time = DbContext.tbl_DC_MentorshipExam_Result_Pattern.
                        Where(x => x.Pattern_Code == timeID && x.CategoryType == 4).Select(x => x.Comment).FirstOrDefault(),
                        SelfStudy = DbContext.tbl_DC_MentorshipExam_Result_Pattern.
                       Where(x => x.Pattern_Code == selfstudyID && x.CategoryType == 5).Select(x => x.Comment).FirstOrDefault(),
                        School = DbContext.tbl_DC_MentorshipExam_Result_Pattern.
                        Where(x => x.Pattern_Code == schoolID && x.CategoryType == 6).Select(x => x.Comment).FirstOrDefault(),
                        Personal = DbContext.tbl_DC_MentorshipExam_Result_Pattern.
                       Where(x => x.Pattern_Code == personalID && x.CategoryType == 7).Select(x => x.Comment).FirstOrDefault(),
                        Mentorship = DbContext.tbl_DC_MentorshipExam_Result_Pattern.
                        Where(x => x.Pattern_Code == mentorshipID && x.CategoryType == 8).Select(x => x.Comment).FirstOrDefault(),
                    };

                    

                    var obj = new PsychometricResultSuccess()
                    {
                        success = r
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);

                }
                catch
                {
                    var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                    {
                        error = new Digichamps.ErrorResponse_Exam
                        {
                            Message = "Something went wrong."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }

            else
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "Please provide data correctly."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }


      

    }
    

    }




  



