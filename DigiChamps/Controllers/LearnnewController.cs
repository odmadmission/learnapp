﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;
using System.Data.SqlClient;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class LearnnewController : ApiController
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();
        #region DashboardModel
        public class Banner : Digichamps_web_Api.learnresultRESPONSE
        {
            public int? banner_Id { get; set; }
            public string Image_URL { get; set; }
            public string Image_Title { get; set; }
            public string Banner_Description { get; set; }
        }

        public class Banner_List
        {
            public List<Banner> list { get; set; }
        }
        public class success_Banner
        {
            public Banner_List Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }

        public class StudentDetails
        {
            public int RegID { get; set; }
            public int PKGID { get; set; }
            public Guid? schoolid { get; set; }
            public int? Class_ID { get; set; }
        }
        #endregion
        [HttpPost]
        public HttpResponseMessage GetDashboardNew([FromBody]StudentDetails student)
        {
            try
            {

                List<Digichamps_web_Api.Pkglearnmodel> datapoints = new List<Digichamps_web_Api.Pkglearnmodel>();
                var reportList = DbContext.Database.SqlQuery<Digichamps_web_Api.Pkglearnmodel>(@"select * from [odm_lms].[dbo].[Get_Dashboard_Details](@RegID)", new SqlParameter("@RegID", student.RegID)).FirstOrDefault<Digichamps_web_Api.Pkglearnmodel>();
                var resultobj1 = new Digichamps_web_Api.learnresultRESPONSE();
                resultobj1.success=reportList;
               
                return Request.CreateResponse(HttpStatusCode.OK, resultobj1);
            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetDashboardLearn([FromBody]StudentDetails student)
        {
            try
            {
                List<Digichamps_web_Api.Pkglearnmodel> datapoints = new List<Digichamps_web_Api.Pkglearnmodel>();
                var reportList = DbContext.Database.SqlQuery<Digichamps_web_Api.Pkglearnmodel>(@"select * from [odm_lms].[dbo].[Get_Dashboard_Details](@RegID)", new SqlParameter("@RegID", student.RegID)).FirstOrDefault<Digichamps_web_Api.Pkglearnmodel>();
                var Subjectlists = DbContext.Database.SqlQuery<Digichamps_web_Api.pkgLearnSubjects>(@"select * from [odm_lms].[dbo].[Get_Dashboard_Subject_Details](@RegID)", new SqlParameter("@RegID", student.RegID)).ToList<Digichamps_web_Api.pkgLearnSubjects>();
                var BannerList = (from c in DbContext.tbl_DC_Banner.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                  select new Digichamps_web_Api.BannerModel
                                  {
                                      banner_Id = c.banner_Id,
                                      Image_URL = c.Image_URL,
                                      BannerOnline = "https://learn.odmps.org/Images/Banner/" + c.Image_URL + "",
                                      BannerBeta = "http://beta.thedigichamps.com/Images/Banner/" + c.Image_URL + "",
                                      Image_Title = c.Image_Title,
                                      //Banner_Description = c.Banner_Description,
                                  }).OrderByDescending(c => c.banner_Id).ToList();
                var DiyModuleLists = (from c in DbContext.tbl_DC_DIY_Video.Where(x => x.Is_Active == true && x.Is_Delete == false)
                                      select new Digichamps_web_Api.DiyModuleList
                                      {
                                          DIYVideo_ID = c.DIYVideo_ID,
                                          DIYVideo_Name = c.DIYVideo_Name,
                                          DIYVideo_Upload = c.DIYVideo_Upload,
                                          DIYImages = c.DIYImages,
                                          DiyPosterImage_production = c.DIYImages,
                                          DiyPosterImage_beta = c.DIYImages,
                                          DIYVideo_Description = c.DIYVideo_Description
                                      }).OrderByDescending(c => c.DIYVideo_ID).ToList();
                var Recentwatchedvideos = DbContext.Database.SqlQuery<Digichamps_web_Api.Recentvideos>(@"select * from [odm_lms].[dbo].[FN_Recent_watched_videos](@RegID)", new SqlParameter("@RegID", student.RegID)).ToList<Digichamps_web_Api.Recentvideos>();
                var resultobj1 = new Digichamps_web_Api.learnresultRESPONSE();
                resultobj1.success = reportList;
                resultobj1.success.Subjectlists = Subjectlists;
                resultobj1.success.BannerList = BannerList;
                resultobj1.success.DiyModuleLists = DiyModuleLists;
                resultobj1.success.Recentwatchedvideos = Recentwatchedvideos;
                return Request.CreateResponse(HttpStatusCode.OK, resultobj1);
            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }

        [HttpGet]
        public HttpResponseMessage learnSubjectdetails(int id, int eid)
        {
            try
            {
                int i=0;
                var reportList = DbContext.Database.SqlQuery<Digichamps_web_Api.Pkgsubjectwisemodel>(@"select * from [odm_lms].[dbo].[FN_DC_Pkgsubjectwisemodel](@Subject_Id,@RegID)", new SqlParameter("@Subject_Id", eid), new SqlParameter("@RegID", id)).FirstOrDefault<Digichamps_web_Api.Pkgsubjectwisemodel>();
                var subjectList = DbContext.Database.SqlQuery<Digichamps_web_Api.ProgressData>(@"select * from [odm_lms].[dbo].[FN_DC_Progress_Data](@Subject_Id,@Regd_ID,@chapterId)", new SqlParameter("@Subject_Id", eid), new SqlParameter("@Regd_ID", id), new SqlParameter("@chapterId", i)).FirstOrDefault<Digichamps_web_Api.ProgressData>();

                var chapterList = DbContext.Database.SqlQuery<Digichamps_web_Api.pkgLearnChapters>(@"select * from [odm_lms].[dbo].[FN_DC_pkgLearnChapters](@Subject_Id,@RegID)", new SqlParameter("@Subject_Id", eid), new SqlParameter("@RegID", id)).ToList<Digichamps_web_Api.pkgLearnChapters>();
                var resultobj1 = new Digichamps_web_Api.learnsubjectwiseRESPONSE();
                resultobj1.success = reportList;
                resultobj1.success.SubjectProgressData = subjectList;
                resultobj1.success.Chapterlist = chapterList;
                return Request.CreateResponse(HttpStatusCode.OK, resultobj1);
            }
            catch
            {
                var errobj = new Digichamps_web_Api.Errorresult
                {
                    Error = new Digichamps_web_Api.Errorresponse
                    {
                        Message = "Something went wrong.",
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, errobj);
            }
        }
        public bool FindModuleasBookmarked(int moduleid, int regid, int dataid)
        {
            var Is_Bookmarked = DbContext.tbl_DC_BookMark.Where(x => x.DataID == moduleid && x.RedgID == regid && x.TypeID == dataid).FirstOrDefault();
            if (Is_Bookmarked == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        [HttpGet]
        public HttpResponseMessage LearnChapterdetails(int id, int eid) //Student id and Chapter id
        {
            try { 
            var reportList = DbContext.Database.SqlQuery<Digichamps_web_Api.chapterlist>(@"select * from [odm_lms].[dbo].[FN_DC_chapterlist](@Chapter_Id,@Regd_Id)", new SqlParameter("@Chapter_Id", eid), new SqlParameter("@Regd_Id", id)).FirstOrDefault<Digichamps_web_Api.chapterlist>();
            var chaptermodules = DbContext.Database.SqlQuery<Digichamps_web_Api.ChapterModuleList>(@"select * from [odm_lms].[dbo].[FN_DC_ChapterModuleList](@Regd_ID,@Chapter_ID)", new SqlParameter("@Regd_ID", id), new SqlParameter("@Chapter_ID", eid)).ToList<Digichamps_web_Api.ChapterModuleList>();
            DigiChamps.Models.Digichamps_web_Api.ChapterDetailsRESPONSE m = new Models.Digichamps_web_Api.ChapterDetailsRESPONSE();
            m.success = reportList;
            m.success.ChapterModules = chaptermodules;
             var v = DbContext.SP_DC_GetModules_for_api(eid, id).ToList();
             var listdata = (from c in v.Where(x => x.Question_PDF != null)
                                        select new
                                            DigiChamps.Models.Digichamps_web_Api.Questionbanks
                                        {
                                            noofques = c.No_Of_Question,
                                            Question_Pdfs = c.Question_PDF,
                                            Modulename = c.Module_Name,
                                            moduleIDss = c.Module_ID

                                        }).ToList();
             m.success.Quesbank = (listdata == null) ? (from c in v select new DigiChamps.Models.Digichamps_web_Api.Questionbanks { noofques = 0, Question_Pdfs = "", Modulename = "" }).ToList() : (from c in listdata.Where(x => x.Question_Pdfs != null) select new DigiChamps.Models.Digichamps_web_Api.Questionbanks { noofques = c.noofques, Question_Pdfs = c.Question_Pdfs, Modulename = c.Modulename, is_question_bookmarked = FindModuleasBookmarked(c.moduleIDss, id, 1), moduleIDss = c.moduleIDss }).ToList();
             m.success.pdfs = (from c in v.Where(x => x.Module_Content != null)
                               select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                               {
                                   moduleID = c.Module_ID,
                                   Url = c.Module_Content,
                                   Modulename = c.Module_Name,
                                   is_studynotebookmarked = FindModuleasBookmarked(c.Module_ID, id, 3),
                               }).ToList() == null ? (from c in v
                                                      select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                                                      {
                                                          Url = "",
                                                          Modulename = ""
                                                      }).ToList() : (from c in v.Where(x => x.Module_Content != null)
                                                                     select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                                                                     {
                                                                         moduleID = c.Module_ID,
                                                                         Url = c.Module_Content,
                                                                         Modulename = c.Module_Name,
                                                                         is_studynotebookmarked = FindModuleasBookmarked(c.Module_ID, id, 3),
                                                                     }).ToList();
             DateTime diff = new DateTime(2019, 3, 31);
             if (DateTime.Today.Date <= diff.Date)
             {

                 for (int i = 0; i < m.success.ChapterModules.Count; i++)
                 {
                     m.success.ChapterModules[i].Is_Expire = false;

                 }

             }
             return Request.CreateResponse(HttpStatusCode.OK, m);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
