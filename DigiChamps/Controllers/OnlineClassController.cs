﻿using DigiChamps.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DigiChamps.Controllers
{
    public class OnlineClassController : Controller
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();
        // GET: /OnlineClass/

        public ActionResult Index()
        {
            return View();
        }



        #region PostSingleData

        public ActionResult CreateSingleOnlineClass()
        {
            ViewBag.School = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true);
            ViewBag.Teacher = DbContext.Teachers.Where(a => a.Active == 1);
            ViewBag.Board = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true);
            ViewBag.Subject = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true);

            return View();
        }

        //public ActionResult CreateSyllabus(long ID,int SubId)
        //{
        //    var onlineClassList = DbContext.OnlineClasses.Where(a => a.Active == true && a.SubjectId == SubId).FirstOrDefault();
        //    ViewBag.OnlineCls = DbContext.OnlineClasses.Where(a => a.ID == ID && a.Active == true).FirstOrDefault();
        //    ViewBag.Subject = DbContext.tbl_DC_Subject.Where(b => b.Subject_Id == onlineClassList.SubjectId).Select(b => new { b.Subject_Id, b.Subject }).ToList();
        //    ViewBag.Chapter = DbContext.tbl_DC_Chapter.Where(b => b.Subject_Id == DbContext.tbl_DC_Subject.Where(c => c.Subject_Id == onlineClassList.SubjectId).FirstOrDefault().Subject_Id).ToList();

        //    return View();
        //}


        //[HttpPost]
        //public ActionResult SaveSingleOnlineClass(OnlineClassModel classModel)
        //{
        //    string u_code = Session["USER_CODE"].ToString();

        //    OnlineClass onlineClass = new OnlineClass();
        //    onlineClass.TeacherId = classModel.TeacherId;
        //    onlineClass.Date = classModel.Date;
        //    onlineClass.StartTime = classModel.StartTime;
        //    onlineClass.EndTime = classModel.EndTime;
        //    onlineClass.Status = "Created";
        //    onlineClass.InsertedId = Int64.Parse(u_code);
        //    onlineClass.InsertedDate = DateTime.Now;
        //    onlineClass.ModifiedId = Int64.Parse(u_code);
        //    onlineClass.ModifiedDate = DateTime.Now;
        //    onlineClass.SubjectId = classModel.SubjectId;
        //    onlineClass.SectionId = classModel.SectionId;
        //    onlineClass.Active = true;

        //    DbContext.OnlineClasses.Add(onlineClass);
        //    DbContext.SaveChanges();

        //    return RedirectToAction("OnlineClassList");
        //}


        #endregion


        public ActionResult ViewOnlineClassses(int? id)
        {
            var onlineClsList = DbContext.OnlineClasses.Where(a => a.Active == true).ToList();
            var schoolList = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var boardList = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            var classList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
            var sectionList = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var teacherList = DbContext.Teachers.Where(a => a.Active == 1).ToList();
            var subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
            adminviewonlineclasses adminviewonlineclasses = new adminviewonlineclasses();
            var onlineClasses = (from a in onlineClsList
                                 join b in teacherList on a.TeacherId equals b.TeacherId
                                 join c in subjectList on a.SubjectId equals c.Subject_Id
                                 join d in sectionList on a.SectionId equals d.SectionId
                                 join e in schoolList on b.SchoolId equals e.SchoolId
                                 join f in classList on c.Class_Id equals f.Class_Id
                                 join g in boardList on f.Board_Id equals g.Board_Id
                                 where a.ID == id
                                 select new OnlineClassModel
                                 {
                                     ID = a.ID,
                                     TeacherId = Convert.ToInt32(a.TeacherId),
                                     TeacherName = b.Name,
                                     SubjectId = a.SubjectId,
                                     SubjectName = c.Subject,
                                     FromDate = a.FromDate,
                                     ToDate = a.ToDate,
                                     StartTime = a.StartTime,
                                     EndTime = a.EndTime,
                                     SectionId = a.SectionId,
                                     SectionName = d.SectionName,
                                     ClassId = c.Class_Id,
                                     ClassName = f.Class_Name,
                                     BoardId = f.Board_Id,
                                     BoardName = g.Board_Name,
                                     SchoolId = b.SchoolId,
                                     SchoolName = e.SchoolName,
                                     Zoom_Password = a.Zoom_Password,
                                     Zoom_Url = a.Zoom_Url
                                 }).FirstOrDefault();
            //var onlineClassSyllabus = DbContext.OnlineClassSyllabus.Where(a => a.Active == true && a.OnlineClassId == id).ToList();
            //var chapterList = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();

            //var syllabus = (from a in onlineClassSyllabus
            //                join b in chapterList on a.ChapterId equals b.Chapter_Id
            //                select new OnlineClassSyllabusModel
            //                {
            //                    ID = a.ID,
            //                    OnlineClassId = a.OnlineClassId.Value,
            //                    SyllabusTitle = a.SyllabusTitle,
            //                    SyllabusDescription = a.SyllabusDescription,
            //                    ChapterId = a.ChapterId.Value,
            //                    ChapterName = b.Chapter,
            //                    status = a.Status,
            //                    modifieddate = a.ModifiedDate
            //                }).ToList();
            //var studentattendance = DbContext.OnlineClassAttendences.ToList().Where(a => a.Active == true && a.OnlineClassId == id).ToList();
            //var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
            //var studentfeedback = DbContext.OnlineClassFeedbacks.ToList().Count() > 0 ? null : DbContext.OnlineClassFeedbacks.ToList().Where(a => a.Active == true).ToList();
            //if (studentattendance.ToList().Count > 0)
            //{
            //    var attendance = (from a in studentattendance
            //                      join b in students on a.StudentId equals b.Regd_ID
            //                      select new onlinestudentattendance
            //                      {
            //                          ID = a.ID,
            //                          OnlineClassId = a.OnlineClassId.Value,
            //                          StudentId = a.StudentId.Value,
            //                          Student_Name = b.Customer_Name,
            //                          Rating = studentfeedback != null ? (studentfeedback.Where(m => m.OnlineClassId == a.OnlineClassId && m.StudentId == a.StudentId).FirstOrDefault().Rating.Value) : 0,
            //                          Review = studentfeedback != null ? (studentfeedback.Where(m => m.OnlineClassId == a.OnlineClassId && m.StudentId == a.StudentId).FirstOrDefault().Review) : "",
            //                          IsPresent = a.IsPresent.Value,
            //                          modifieddate = a.ModifiedDate
            //                      }).ToList();
            //    adminviewonlineclasses.onlinestudentattendance = attendance;
            //}
            //else
            //{
            //    adminviewonlineclasses.onlinestudentattendance = null;
            //}
            //var studentHomeWork = DbContext.OnlineClassStudentHomeWorks.Where(a => a.Active == true);
            //var homeWorkList = DbContext.OnlineClassHomeWorks.Where(a => a.Active == true && a.OnlineClassId == id);
            //var registrationList = students.Where(a => a.Is_Active == true && a.SectionId == onlineClasses.SectionId).Count();

            //var res2 = (from a in homeWorkList
            //            select new OnlineHomeWorkModel
            //            {
            //                ID = a.ID,
            //                HomeWorkPdf = "/Images/Homeworkquespdf/" + a.HomeWorkPdf,
            //                InsertedDate = a.InsertedDate.Value,
            //                TotalStudentCount = registrationList,
            //                UploadedStudentCount = studentHomeWork.Where(m => m.OnlineClassHomeWorkId == a.ID).ToList().Count(),
            //                NotUploaded = (registrationList - (studentHomeWork.Where(m => m.OnlineClassHomeWorkId == a.ID).ToList().Count()))
            //            }).ToList();

            var onlinevideos = DbContext.OnlineclassVideoes.Where(a => a.IsActive == true).ToList();
            var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
            var videolinks = DbContext.OnlineclassvideoLinks.Where(a => a.IsActive == true).ToList();
            var videodet = (from a in onlinevideos
                            join b in chapters on a.ChapterId equals b.Chapter_Id
                            where a.OnlineClassId == id
                            select new onlineclassvideos
                            {
                                ChapterId = a.ChapterId,
                                ChapterName = b.Chapter,
                                OnlineClassId = a.OnlineClassId,
                                OnlineClassVideoId = a.OnlineClassVideoId,
                                Inserted_On = a.Inserted_On,
                                Topic_Description = a.Topic_Description,
                                Topic_Name = a.Topic_Name,
                                Video_Link=a.Video_Link,
                                Password=a.Password
                               // VideoLink = videolinks.Where(m => m.OnlineClassVideoId == a.OnlineClassVideoId).ToList()
                            }).ToList();


            adminviewonlineclasses.onlineclassvideos = videodet;
           // adminviewonlineclasses.classSyllabusModel = syllabus;
            adminviewonlineclasses.onlineClass = onlineClasses;
           // adminviewonlineclasses.HomeWorks = res2;
            return View(adminviewonlineclasses);
        }
        public ActionResult OnlineClasses(Guid? schoolid, int? boardid, int? classid, Guid? sec, DateTime? date, int? Subjectid, int? teacherid)
        {
            var onlineClsList = DbContext.OnlineClasses.Where(a => a.Active == true).ToList();
            var schoolList = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var boardList = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            var classList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
            var sectionList = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var teacherList = DbContext.Teachers.Where(a => a.Active == 1).ToList();
            var subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();

            var res = (from a in onlineClsList
                       join b in teacherList on a.TeacherId equals b.TeacherId
                       join c in subjectList on a.SubjectId equals c.Subject_Id
                       join d in sectionList on a.SectionId equals d.SectionId
                       join e in schoolList on b.SchoolId equals e.SchoolId
                       join f in classList on c.Class_Id equals f.Class_Id
                       join g in boardList on f.Board_Id equals g.Board_Id
                       select new OnlineClassModel
                       {
                           ID = a.ID,
                           TeacherId = Convert.ToInt32(a.TeacherId),
                           TeacherName = b.Name,
                           SubjectId = a.SubjectId,
                           SubjectName = c.Subject,
                           FromDate = a.FromDate,
                           ToDate = a.ToDate,
                           StartTime = a.StartTime,
                           EndTime = a.EndTime,
                           SectionId = a.SectionId,
                           SectionName = d.SectionName,
                           ClassId = c.Class_Id,
                           ClassName = f.Class_Name,
                           BoardId = f.Board_Id,
                           BoardName = g.Board_Name,
                           SchoolId = b.SchoolId,
                           SchoolName = e.SchoolName
                       }).ToList();
            ViewBag.school = null;
            ViewBag.board = null;
            ViewBag.cls = null;
            ViewBag.sec = null;
            ViewBag.subject = null;
            ViewBag.teacher = null;
            ViewBag.date = null;
            if (schoolid != null && schoolid != Guid.Empty)
            {
                res = res.Where(a => a.SchoolId == schoolid.Value).ToList();
                ViewBag.school = schoolid;
            }

            if (boardid != null && boardid != 0)
            {
                res = res.Where(a => a.BoardId == boardid.Value).ToList();
                ViewBag.board = boardid;
            }
            if (classid != null && classid != 0)
            {
                res = res.Where(a => a.ClassId == classid.Value).ToList();
                ViewBag.cls = classid;
            }
            if (sec != null && sec != Guid.Empty)
            {
                res = res.Where(a => a.SectionId == sec.Value).ToList();
                ViewBag.sec = sec;
            }
            if (date != null)
            {
                res = res.Where(a => a.FromDate.Value.Date <= date.Value.Date && a.ToDate.Value.Date >= date.Value.Date).ToList();
                ViewBag.date = date;
            }
            if (Subjectid != null && Subjectid != 0)
            {
                res = res.Where(a => a.SubjectId == Subjectid).ToList();
                ViewBag.subject = Subjectid;
            }
            if (teacherid != null && teacherid != 0)
            {
                res = res.Where(a => a.TeacherId == teacherid).ToList();
                ViewBag.teacher = teacherid;
            }
            return View(res);
        }
        public ActionResult CreateOnlineClass(int? id, string type, Guid? schoolid, int? boardid, int? classid, Guid? sec)
        {
            if (id == null)
            {
                ViewBag.school = null;
                ViewBag.board = null;
                ViewBag.cls = null;
                ViewBag.sec = null;
                ViewBag.subject = null;
                ViewBag.teacher = null;
                ViewBag.startdate = null;
                ViewBag.enddate = null;
                ViewBag.id = null;
                ViewBag.start = null;
                ViewBag.end = null;
                ViewBag.zoomurl = "";
                ViewBag.zoompassword = "";
            }
            else
            {
                OnlineClass onlineClass = DbContext.OnlineClasses.Where(a => a.ID == id).FirstOrDefault();
                ViewBag.school = schoolid;
                ViewBag.board = boardid;
                ViewBag.cls = classid;
                ViewBag.sec = sec;
                ViewBag.subject = onlineClass.SubjectId;
                ViewBag.teacher = onlineClass.TeacherId;
                ViewBag.startdate = onlineClass.FromDate;
                ViewBag.enddate = onlineClass.ToDate;
                ViewBag.id = id;
                ViewBag.start = onlineClass.StartTime;
                ViewBag.end = onlineClass.EndTime;
                ViewBag.zoomurl = onlineClass.Zoom_Url;
                ViewBag.zoompassword = onlineClass.Zoom_Password;
            }
            //if (type == null)
            //{
            //  //  ViewBag.classesdet = null;
            //}
            //else if (type == "N")
            //{
            //ViewBag.school = schoolid;
            //ViewBag.board = boardid;
            //ViewBag.cls = classid;
            //ViewBag.sec = sec;
            //ViewBag.startdate = startdate.Value.ToString("yyyy-MM-dd");
            //ViewBag.enddate = enddate.Value.ToString("yyyy-MM-dd");
            // ViewBag.date = date.Value.ToString("yyyy-MM-dd");
            //var onlineClsList = DbContext.OnlineClasses.Where(a => a.Active == true);
            //var schoolList = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true);
            //var boardList = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true);
            //var classList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true);
            //var sectionList = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true);
            //var teacherList = DbContext.Teachers.Where(a => a.Active == 1);
            //var subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true);

            //var res = (from a in onlineClsList
            //           join b in teacherList on a.TeacherId equals b.TeacherId
            //           join c in subjectList on a.SubjectId equals c.Subject_Id
            //           join d in sectionList on a.SectionId equals d.SectionId
            //           join e in schoolList on b.SchoolId equals e.SchoolId
            //           join f in classList on c.Class_Id equals f.Class_Id
            //           join g in boardList on f.Board_Id equals g.Board_Id
            //           select new OnlineClassModel
            //           {
            //               ID = a.ID,
            //               TeacherId = a.TeacherId,
            //               TeacherName = b.Name,
            //               SubjectId = a.SubjectId,
            //               SubjectName = c.Subject,
            //               FromDate = a.FromDate,
            //               ToDate = a.ToDate,
            //               StartTime = a.StartTime,
            //               EndTime = a.EndTime,
            //               SectionId = a.SectionId,
            //               SectionName = d.SectionName,
            //               ClassId = c.Class_Id,
            //               ClassName = f.Class_Name,
            //               BoardId = f.Board_Id,
            //               BoardName = g.Board_Name,
            //               SchoolId = b.SchoolId,
            //               SchoolName = e.SchoolName
            //           }).ToList();

            //if (schoolid != null && schoolid != Guid.Empty)
            //{
            //    res = res.Where(a => a.SchoolId == schoolid.Value).ToList();
            //}

            //if (boardid != null && boardid != 0)
            //{
            //    res = res.Where(a => a.BoardId == boardid.Value).ToList();
            //}
            //if (classid != null && classid != 0)
            //{
            //    res = res.Where(a => a.ClassId == classid.Value).ToList();
            //}
            //if (sec != null && sec != Guid.Empty)
            //{
            //    res = res.Where(a => a.SectionId == sec.Value).ToList();
            //}
            //if (date != null)
            //{
            //    res = res.Where(a => a.Date.Value.Date == date.Value.Date).ToList();
            //}
            //ViewBag.classesdet = res;
            //}
            return View();
        }


        [HttpPost]
        public ActionResult SaveOnlineClass(OnlineClassModel classModel, string Submit)
        {
            // string u_code = Session["USER_CODE"].ToString();
            if (classModel.ID == 0)
            {
                OnlineClass onlineClass = new OnlineClass();
                onlineClass.TeacherId = classModel.TeacherId;
                onlineClass.FromDate = classModel.FromDate;
                onlineClass.ToDate = classModel.ToDate;
                onlineClass.StartTime = classModel.StartTime;
                onlineClass.EndTime = classModel.EndTime;
                onlineClass.Status = "Created";
                onlineClass.InsertedId = 1;
                onlineClass.InsertedDate = DateTime.Now;
                onlineClass.ModifiedId = 1;
                onlineClass.ModifiedDate = DateTime.Now;
                onlineClass.SubjectId = classModel.SubjectId;
                onlineClass.SectionId = classModel.SectionId;
                onlineClass.Zoom_Password = classModel.Zoom_Password;
                onlineClass.Zoom_Url = classModel.Zoom_Url;
                onlineClass.Active = true;
                DbContext.OnlineClasses.Add(onlineClass);
                DbContext.SaveChanges();
            }
            else
            {
                OnlineClass onlineClass = DbContext.OnlineClasses.Where(a => a.ID == classModel.ID).FirstOrDefault();
                onlineClass.TeacherId = classModel.TeacherId;
                onlineClass.FromDate = classModel.FromDate;
                onlineClass.ToDate = classModel.ToDate;
                onlineClass.StartTime = classModel.StartTime;
                onlineClass.EndTime = classModel.EndTime;
                onlineClass.Status = "Updated";
                onlineClass.ModifiedId = 1;
                onlineClass.ModifiedDate = DateTime.Now;
                onlineClass.SubjectId = classModel.SubjectId;
                onlineClass.SectionId = classModel.SectionId;
                onlineClass.Zoom_Password = classModel.Zoom_Password;
                onlineClass.Zoom_Url = classModel.Zoom_Url;
                DbContext.SaveChanges();
            }
            if (Submit == "Assign")
            {
                return RedirectToAction("OnlineClasses");
            }
            else
            {
                return RedirectToAction("CreateOnlineClass", new { type = "N", schoolid = classModel.SchoolId, boardid = classModel.BoardId, classid = classModel.ClassId, sec = classModel.SectionId });
            }
        }
        public ActionResult RemoveOnlineClass(int? id, string type, Guid? schoolid, int? boardid, int? classid, Guid? sec)
        {
            OnlineClass onlineClass = DbContext.OnlineClasses.Where(a => a.ID == id).FirstOrDefault();
            onlineClass.Active = false;
            onlineClass.ModifiedDate = DateTime.Now;
            DbContext.SaveChanges();
            if (type == null)
            {
                return RedirectToAction("OnlineClasses");
            }
            else if (type == "N")
            {
                return RedirectToAction("CreateOnlineClass", new { type = "N", schoolid = schoolid, boardid = boardid, classid = classid, sec = sec });
            }
            return RedirectToAction("OnlineClasses");
        }
        //[HttpPost]
        //public JsonResult SaveSessionOnlineClass(OnlineClassModel classModel)
        //{
        //    List<OnlineClass> classes = new List<OnlineClass>();
        //    if (Session["classes"] != null)
        //    {
        //        classes = Session["classes"] as List<OnlineClass>;
        //    }
        //    OnlineClass cls = new OnlineClass();
        //    cls.TeacherId = classModel.TeacherId;
        //    cls.ID = classes.Count() + 1;
        //    cls.Date = classModel.Date;
        //    cls.StartTime = classModel.StartTime;
        //    cls.EndTime = classModel.EndTime;
        //    cls.InsertedId = classModel.InsertedId;
        //    cls.SubjectId = classModel.SubjectId;
        //    cls.SectionId = classModel.SectionId;
        //    cls.Active = true;
        //    classes.Add(cls);
        //    Session["classes"] = classes;
        //    return Json(classes.Where(a => a.Active == true).ToList(), JsonRequestBehavior.AllowGet);
        //}

        public JsonResult GetClassByBoard(int boardId)
        {
            var res = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Board_Id == boardId).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSectionByClass(int classId)
        {
            var res = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true && a.Class_Id == classId).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSubjectByClass(long classId)
        {
            var res = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Class_Id == classId).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult HomeWorkDetails(long id, Nullable<System.Guid> SectionId)
        {
            OnlineClassHomeWorkDetails onlineHwModel = new OnlineClassHomeWorkDetails();

            var studentHomeWork = DbContext.OnlineClassStudentHomeWorks.Where(a => a.Active == true && a.OnlineClassHomeWorkId == id);
            var homeWorkList = DbContext.OnlineClassHomeWorks.Where(a => a.Active == true && a.ID == id);
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
            var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
            var registrationList = students.Where(a => a.SectionId == SectionId).ToList();

            OnlineHomeWorkModel HomeWork = new OnlineHomeWorkModel();
            HomeWork.ID = homeWorkList.FirstOrDefault().ID;
            HomeWork.HomeWorkPdf = "/Images/Homeworkquespdf/" + homeWorkList.FirstOrDefault().HomeWorkPdf;
            HomeWork.InsertedDate = homeWorkList.FirstOrDefault().InsertedDate.Value;
            HomeWork.TotalStudentCount = registrationList.Count();
            HomeWork.UploadedStudentCount = studentHomeWork.ToList().Count();
            HomeWork.NotUploaded = (HomeWork.TotalStudentCount - HomeWork.UploadedStudentCount);



            var notUploaded = registrationList.Where(a => !studentHomeWork.Select(m => m.StudentId).Contains(a.Regd_ID)).ToList();
            var uploadedStudent = registrationList.Where(a => studentHomeWork.Select(m => m.StudentId).Contains(a.Regd_ID)).ToList();

            onlineHwModel.WorkModel = HomeWork;

            var uploadedStudentDetails = (from a in uploadedStudent
                                          join b in registrationList on a.Regd_ID equals b.Regd_ID
                                          join c in studentHomeWork on a.Regd_ID equals c.StudentId
                                          select new StudentDetails
                                          {
                                              ID = c.ID,
                                              StudentId = a.Regd_ID,
                                              StudentName = b.Customer_Name,
                                              Mobile = b.Mobile,
                                              UplodedDate = c.InsertedDate,
                                              uploadedpdf = "/Images/HomeworkAnswerPDF/" + c.UploadedPdf,
                                              Verified = c.Verified.Value,
                                              VerifiedTeacherId = c.VerifiedTeacherId.Value,
                                              verifiedteachername = c.VerifiedTeacherId == null ? "" : teachers.Where(m => m.TeacherId == c.VerifiedTeacherId).FirstOrDefault().Name
                                          }).ToList();

            onlineHwModel.UploadedDetails = uploadedStudentDetails;

            var notUploadedStudent = (from a in notUploaded
                                      join b in registrationList on a.Regd_ID equals b.Regd_ID
                                      select new StudentDetails
                                      {
                                          ID = a.Regd_ID,
                                          StudentId = a.Regd_ID,
                                          StudentName = b.Customer_Name,
                                          Mobile = b.Mobile
                                      }).ToList();

            onlineHwModel.NotUploadedDetails = notUploadedStudent;

            return View(onlineHwModel);
        }
        public ActionResult CreateOnlineVideos(long? id, int onlineclassid)
        {
            Session["classvideos"] = null;
            var online = DbContext.OnlineClasses.Where(a => a.ID == onlineclassid).FirstOrDefault();
            ViewBag.Chapter = DbContext.tbl_DC_Chapter.Where(b => b.Subject_Id == online.SubjectId && b.Is_Active == true).ToList();
            ViewBag.OnlineClassId = onlineclassid;
            ViewBag.id = id;
            if (id != null)
            {
                var onlinevideos = DbContext.OnlineclassVideoes.Where(a => a.IsActive == true).ToList();
                var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var videolinks = DbContext.OnlineclassvideoLinks.Where(a => a.IsActive == true).ToList();
                var videodet = (from a in onlinevideos
                                join b in chapters on a.ChapterId equals b.Chapter_Id
                                where a.OnlineClassVideoId == id
                                select a).FirstOrDefault();
                ViewBag.videodet = videodet;
                //List<OnlineclassvideoLink> am = new List<OnlineclassvideoLink>();
                //if (videodet.VideoLink.Count > 0)
                //{
                //    int i = 0;
                //    foreach (var a in videodet.VideoLink)
                //    {
                //        OnlineclassvideoLink link = new OnlineclassvideoLink();
                //        link.VideoLink = a.VideoLink;
                //        link.Password = a.Password;
                //        link.ID = a.ID;
                //        link.InsertedId = i + 1;
                //        link.IsActive = true;
                //        am.Add(link);
                //    }
                //}
                //Session["classvideos"] = am;
            }
            else
            {
                ViewBag.videodet = null;
            }
            return View();
        }
        public ActionResult RemoveOnlineVideos(int id)
        {
            OnlineclassVideo syllabus = DbContext.OnlineclassVideoes.Where(a => a.OnlineClassVideoId == id).FirstOrDefault();
            syllabus.IsActive = false;
            syllabus.Modified_On = DateTime.Now;
            DbContext.SaveChanges();
            var videolinks = DbContext.OnlineclassvideoLinks.Where(a => a.OnlineClassVideoId == id).ToList();
            foreach (OnlineclassvideoLink link in videolinks)
            {
                link.IsActive = false;
                link.Modified_Date = DateTime.Now;
                DbContext.SaveChanges();
            }

            return RedirectToAction("ViewOnlineClassses", new { id = syllabus.OnlineClassId });
        }

        [HttpPost]
        public ActionResult SaveOnlineVideos(long? videoid, int ChapterId, string Title, string Description, DateTime date, int onlineClassId,string videourl,string password)
        {
            //var teacherId = Convert.ToInt32(Session["TeacherId"]);
            // string u_code = Session["USER_CODE"].ToString();
            if (videoid == null)
            {
                OnlineclassVideo syllabus = new OnlineclassVideo();
                syllabus.OnlineClassId = onlineClassId;
                syllabus.ChapterId = ChapterId;
                syllabus.Topic_Description = Description;
                syllabus.Topic_Name = Title;
                syllabus.Is_Taken = true;
                syllabus.Taken_Date = date;
                syllabus.IsActive = true;
                syllabus.InsertedId = 1;
                syllabus.Inserted_On = DateTime.Now;
                syllabus.ModifiedId = 1;
                syllabus.Modified_On = DateTime.Now;
                syllabus.Video_Link = videourl;
                syllabus.Password = password;
                DbContext.OnlineclassVideoes.Add(syllabus);
                DbContext.SaveChanges();
                var id = syllabus.OnlineClassVideoId;
                //List<OnlineclassvideoLink> am = new List<OnlineclassvideoLink>();
                //if (Session["classvideos"] != null)
                //{
                //    am = Session["classvideos"] as List<OnlineclassvideoLink>;
                //}
                //if (am.Count > 0)
                //{
                //    foreach (var a in am)
                //    {
                //        OnlineclassvideoLink link = new OnlineclassvideoLink();
                //        link.OnlineClassVideoId = id;
                //        link.Password = a.Password;
                //        link.VideoLink = a.VideoLink;
                //        link.IsActive = true;
                //        link.InsertedId = 1;
                //        link.Inserted_Date = DateTime.Now;
                //        link.ModififedId = 1;
                //        link.Modified_Date = DateTime.Now;
                //        DbContext.OnlineclassvideoLinks.Add(link);
                //        DbContext.SaveChanges();
                //    }
                //}
            }
            else
            {
                OnlineclassVideo syllabus = DbContext.OnlineclassVideoes.Where(a => a.OnlineClassVideoId == videoid && a.IsActive == true).FirstOrDefault();
                syllabus.ChapterId = ChapterId;
                syllabus.Topic_Description = Description;
                syllabus.Topic_Name = Title;
                syllabus.Is_Taken = true;
                syllabus.Taken_Date = date;
                syllabus.IsActive = true;
                syllabus.ModifiedId = 1;
                syllabus.Modified_On = DateTime.Now;
                DbContext.SaveChanges();
                var id = videoid;
                //List<OnlineclassvideoLink> am = new List<OnlineclassvideoLink>();
                //if (Session["classvideos"] != null)
                //{
                //    am = Session["classvideos"] as List<OnlineclassvideoLink>;
                //}
                //if (am.Count > 0)
                //{
                //    foreach (var a in am)
                //    {
                //        if (a.ID == 0)
                //        {
                //            OnlineclassvideoLink link = new OnlineclassvideoLink();
                //            link.OnlineClassVideoId = id;
                //            link.Password = a.Password;
                //            link.VideoLink = a.VideoLink;
                //            link.IsActive = true;
                //            link.InsertedId = 1;
                //            link.Inserted_Date = DateTime.Now;
                //            link.ModififedId = 1;
                //            link.Modified_Date = DateTime.Now;
                //            DbContext.OnlineclassvideoLinks.Add(link);
                //            DbContext.SaveChanges();
                //        }
                //        else
                //        {
                //            OnlineclassvideoLink link = DbContext.OnlineclassvideoLinks.Where(m => m.ID == a.ID).FirstOrDefault();
                //            link.OnlineClassVideoId = id;
                //            link.Password = a.Password;
                //            link.VideoLink = a.VideoLink;
                //            link.IsActive = a.IsActive;
                //            link.ModififedId = 1;
                //            link.Modified_Date = DateTime.Now;
                //            DbContext.SaveChanges();
                //        }
                //    }
                //}
            }

            return RedirectToAction("ViewOnlineClassses", new { id = onlineClassId });
        }


        public JsonResult SaveTempVideos(string videourl, string password)
        {
            List<OnlineclassvideoLink> am = new List<OnlineclassvideoLink>();
            if (Session["classvideos"] != null)
            {
                am = Session["classvideos"] as List<OnlineclassvideoLink>;
            }
            OnlineclassvideoLink link = new OnlineclassvideoLink();
            link.VideoLink = videourl;
            link.Password = password;
            link.ID = 0;
            link.InsertedId = am.Count() + 1;
            link.IsActive = true;
            am.Add(link);
            Session["classvideos"] = am;
            return Json(am.Where(a => a.IsActive == true).ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemoveTempVideos(int id)
        {
            List<OnlineclassvideoLink> am = new List<OnlineclassvideoLink>();
            if (Session["classvideos"] != null)
            {
                am = Session["classvideos"] as List<OnlineclassvideoLink>;
            }
            OnlineclassvideoLink link = am.Where(a => a.InsertedId == id).FirstOrDefault();

            link.IsActive = false;
            Session["classvideos"] = am;
            return Json(am.Where(a => a.IsActive == true).ToList(), JsonRequestBehavior.AllowGet);
        }

    }
}
