﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class RecentActivityController : ApiController
    {
        DigiChampsEntities db = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public class ErrorResult
        {
            public ErrorResponse Error { get; set; }
        }

        public class ErrorResponse
        {
            public string Message { get; set; }
        }

        public class SuccessResponse
        {
            public string Message { get; set; }
        }
        public class SuccessResult
        {
            public List<RecentActivity> RecentActivitys { get; set; }
        }
        public class RecentActivity
        {
            public int? Module_ID { get; set; }
            public string Module_Name { get; set; }
            public string Module_video { get; set; }
            public string Module_Desc { get; set; }
            public int Regd_ID { get; set; }
            public string Customer_Name { get; set; }

        }
        public class RecentActivity_List
        {
            public List<RecentActivity> list { get; set; }
        }
        public class success_RecentActivity
        {
            public RecentActivity_List Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetRecentActivity()
        {
            try
            {
                var obj = new success_RecentActivity
                {
                    Success = new RecentActivity_List
                    {
                        list = (from c in db.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                from d in db.tbl_DC_Registration.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                select new RecentActivity
                                {
                                    Module_ID = c.Module_ID,
                                    Module_Name = c.Module_Name,
                                    Module_video = c.Module_video,
                                    Module_Desc = c.Module_Desc,
                                    Regd_ID = d.Regd_ID,
                                    Customer_Name = d.Customer_Name
                                }).OrderByDescending(a => a.Module_ID).Take(5).ToList(),
                    }
                };

                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }
    }
}
