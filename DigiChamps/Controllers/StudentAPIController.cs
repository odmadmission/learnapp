﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DigiChamps.Controllers
{
    public class StudentAPIController : ApiController
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();

        [HttpGet]
        public HttpResponseMessage GetOnlineClassTimeTableByStudentId(int studentId, int? subjectId, Nullable<DateTime> date)
        {
            var studentDetails = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Regd_ID == studentId).FirstOrDefault().SectionId;
            var onlineClassList = DbContext.OnlineClasses.Where(a => a.Active == true && a.SectionId == studentDetails).ToList();
            var teacherList = DbContext.Teachers.Where(a => a.Active == 1).ToList();
            var sectionList = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var schoolList = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var classList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
            var boardList = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            var subList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();

            IEnumerable<OnlineClassModel> result = (from a in onlineClassList
                                                    join b in teacherList on a.TeacherId equals b.TeacherId
                                                    join c in sectionList on a.SectionId equals c.SectionId
                                                    join d in schoolList on c.School_Id equals d.SchoolId
                                                    join e in classList on c.Class_Id equals e.Class_Id
                                                    join f in boardList on e.Board_Id equals f.Board_Id
                                                    join g in subList on a.SubjectId equals g.Subject_Id
                                                    select new OnlineClassModel
                                                    {
                                                        ID = a.ID,
                                                        TeacherName = b.Name,
                                                        TeacherId = converttoint(a.TeacherId.Value.ToString()),
                                                        FromDate = a.FromDate,
                                                        StartTime = a.StartTime,
                                                        EndTime = a.EndTime,
                                                        SchoolId = c.SectionId,
                                                        SchoolName = d.SchoolName,
                                                        ClassId = c.Class_Id.Value,
                                                        ClassName = e.Class_Name,
                                                        BoardId = e.Board_Id,
                                                        BoardName = f.Board_Name,
                                                        SectionId = a.SectionId,
                                                        SectionName = c.SectionName,
                                                        SubjectId = a.SubjectId,
                                                        SubjectName = g.Subject,
                                                        Zoom_Password = a.Zoom_Password,
                                                        Zoom_Url = a.Zoom_Url,
                                                        ToDate = a.ToDate,
                                                        Date = a.FromDate.Value.ToString("dd MMM yyyy") + " to " + a.ToDate.Value.ToString("dd MMM yyyy"),
                                                        Time = DateTime.ParseExact(a.StartTime.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt") + "-" + DateTime.ParseExact(a.EndTime.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt")
                                                    }).ToList();



            if (subjectId != null && subjectId != 0)
            {
                result = result.Where(a => a.SubjectId == subjectId).ToList();
            }

            if (date != null)
            {
                result = result.Where(a => a.FromDate <= date.Value.Date && date.Value.Date <= a.ToDate).ToList();
            }

            return Request.CreateResponse(HttpStatusCode.OK, new { onlineClassList = result });
        }
        public int converttoint(string id)
        {
            var s = Convert.ToInt32(id);
            return s;
        }
        [HttpGet]
        public HttpResponseMessage GetOnlineClassVideos(int studentId, int subjectId, int chapterId)
        {
            var studentDetails = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Regd_ID == studentId).FirstOrDefault().SectionId;
            var onlineClassList = DbContext.OnlineClasses.Where(a => a.Active == true && a.SectionId == studentDetails && a.SubjectId == subjectId).ToList();
            var onlineclassvideos = DbContext.ZoomVideos.Where(a => a.IsActive == true && a.ChapterId == chapterId && a.SectionId == studentDetails).ToList();
            var teacherList = DbContext.Teachers.Where(a => a.Active == 1);
            var videolinklist = DbContext.ZoomVideoLinks.Where(a => a.IsActive == true).ToList();
            var subjectlist = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
            var chapterlist = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
            onlineclassvideosapi onlineapi = new onlineclassvideosapi();
            onlineapi.ChapterId = chapterId;
            onlineapi.ChapterName = chapterlist.Where(a => a.Chapter_Id == chapterId).FirstOrDefault().Chapter;
            onlineapi.SubjectId = subjectId;
            onlineapi.SubjectName = subjectlist.Where(a => a.Subject_Id == subjectId).FirstOrDefault().Subject;

            List<OnlineclassVideo> result = (from a in onlineclassvideos
                                             join b in videolinklist
                                             on a.ZoomId equals b.ZoomId
                                             select new OnlineclassVideo
                                             {
                                                 ChapterId = a.ChapterId,
                                                 Video_Link = b.VideoLink,
                                                 Topic_Name = b.TopicName,
                                                 Password = b.Password
                                             }

                                                    ).Distinct().ToList();
            onlineapi.VideoLink = result;
            return Request.CreateResponse(HttpStatusCode.OK, new { videolist = onlineapi });
        }
        #region-----------------All worksheet Report-----------------
        public class marksheetreport
        {
            public long examTypeId { get; set; }
            public string examTypeName { get; set; }
            public totalscoreclass totalscoreclass { get; set; }
        }

        public class totalscoreclass
        {
            public long examTypeId { get; set; }
            public string examTypeName { get; set; }
            public int Subject_Id { get; set; }
            public string Subject { get; set; }
            public decimal? subjectivescore { get; set; }
            public decimal? objectivescore { get; set; }
            public decimal? worksheetscore { get; set; }
            public decimal TotalMark { get; set; }
            public int TotalWorksheetMark { get; set; }
            public decimal TotalSubjectiveMark { get; set; }
            public decimal TotalObjectiveMark { get; set; }
        }

        [HttpGet]
        public HttpResponseMessage AllMarkWorksheetReport(int studentid, int? subject, int? examtype)
        {
            try
            {
                var regs = DbContext.VW_Student_With_Subject.Where(a => a.Regd_ID == studentid).ToList();

                var onlineexamans = DbContext.OnlineExamAnswerSheets.Where(a => a.Is_Active == true && a.StudentId == studentid).ToList();
                var examids = onlineexamans.Select(a => a.OnlineExamId).Distinct().ToList();
                var onlineexams = DbContext.OnlineExams.Where(a => a.Is_Active == true && examids.Contains(a.OnlineExamId)).ToList();

                var OnlineObjectiveStudents = DbContext.OnlineObjectiveExamStudents.Where(a => a.Is_Active == true && a.StudentId == studentid).ToList();
                var objids = OnlineObjectiveStudents.Select(a => a.OnlineObjectiveExamId).Distinct().ToList();
                var onlineobjective = DbContext.OnlineObjectiveExams.Where(a => a.Is_Active == true && objids.Contains(a.OnlineObjectiveExamId)).ToList();

                var worksheets = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Regd_ID == studentid).ToList();
                var chapters = worksheets.Select(a => a.Chapter_Id).Distinct().ToList();

                var modules = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true && chapters.Contains(a.Chapter_Id.Value) && a.Question_PDF != null).ToList();


                var examtypes = onlineexams.Select(a => new { type = a.OnlineExamTypeId, subject = a.SubjectId }).Distinct().ToList().Union(onlineobjective.Select(a => new { type = a.OnlineExamTypeId, subject = a.SubjectId }).Distinct().ToList()).Union(modules.Select(a => new { type = a.Exam_Type, subject = a.Subject_Id }).Distinct().ToList());

                var examtypelist = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
                var subjectlist = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();


                var res = (from a in examtypes.Distinct().ToList()
                           join b in examtypelist on a.type equals b.OnlineExamTypeId
                           join c in subjectlist on a.subject equals c.Subject_Id
                           select new totalscoreclass
                           {
                               examTypeId = a.type.Value,
                               examTypeName = b.OnlineExamTypeName,
                               Subject = c.Subject,
                               Subject_Id = a.subject.Value,
                               subjectivescore = GetOnlineSubjectiveExamscore(onlineexamans.ToList(), onlineexams.Where(m => m.SubjectId == a.subject).ToList(), a.type.Value),
                               objectivescore = GetOnlineObjectiveExamscore(OnlineObjectiveStudents.ToList(), onlineobjective.Where(m => m.SubjectId == a.subject).ToList(), a.type.Value),
                               worksheetscore = Convert.ToDecimal(GetOnlineWorksheetExamscore(worksheets.Where(m => m.Subject_Id == a.subject).ToList(), modules.Where(m => m.Subject_Id == a.subject && m.Question_PDF != null).ToList(), a.type.Value)),
                               TotalObjectiveMark = 20,
                               TotalSubjectiveMark = 20,
                               TotalWorksheetMark = 10
                           }).ToList();
                foreach (var a in res)
                {
                    a.TotalMark = (a.objectivescore == null ? 0 : a.objectivescore.Value) + (a.subjectivescore.Value == null ? 0 : a.subjectivescore.Value) + (a.worksheetscore.Value == null ? 0 : a.worksheetscore.Value);
                }

                if (subject != null && subject != 0)
                {
                    res = res.Where(a => a.Subject_Id == subject).ToList();

                }
                if (examtype != null && examtype != 0)
                {
                    res = res.Where(a => a.examTypeId == examtype).ToList();

                }
                return Request.CreateResponse(HttpStatusCode.OK, new { WorkSheetReportList = res });

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
            }
        }
        public decimal? GetOnlineObjectiveExamscore(List<OnlineObjectiveExamStudent> examStudents, List<OnlineObjectiveExam> exam, long? examtype)
        {
            // var answers = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();
            if (examtype != null)
            {
                exam = exam.Where(a => a.OnlineExamTypeId == examtype).ToList();
            }
            if (exam == null || examStudents == null)
            {
                return 0;
            }
            var examques = DbContext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true).ToList();

            var ress = (from a in examques
                        join b in exam on a.OnlineObjectiveExamId equals b.OnlineObjectiveExamId
                        select a).ToList();
            //var Totalmark = ress.Sum(a => a.Mark);

            var res = (from a in ress
                       join b in examStudents on a.OnlineObjectiveExamId equals b.OnlineObjectiveExamId
                       select new
                       {
                           examid = a.OnlineObjectiveExamId,
                           iscorrect = b.IsCorrect,
                           score = a.Mark
                       }).ToList();
            if (res.Count == 0)
            {
                return 0;
            }
            decimal? totalscore = 0;
            foreach (var a in res.Select(m => m.examid).Distinct().ToList())
            {
                var correct = res.Where(m => m.iscorrect == true && m.examid == a.Value).ToList().Sum(m => m.score);
                var wrong = res.Where(m => m.iscorrect == false && m.examid == a.Value).ToList().Count();
                if (exam.Where(m => m.OnlineObjectiveExamId == a.Value).FirstOrDefault().IsNagativeMarking == true)
                {
                    var negativelist = DbContext.Negativemarks.Where(m => m.NegativeMarkID == exam.Where(h => h.OnlineObjectiveExamId == a.Value).FirstOrDefault().NagativeMarkingId).FirstOrDefault();
                    var negvmark = negativelist.NegativeMark1 * wrong;
                    var scoreres = correct - negvmark;
                    totalscore += scoreres;
                }
                else
                {
                    totalscore += correct;
                }
            }
            return totalscore / res.Count();
        }
        public decimal? GetOnlineSubjectiveExamscore(List<OnlineExamAnswerSheet> examStudents, List<OnlineExam> exam, long? examtype)
        {
            // var answers = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();
            if (examtype != null)
            {
                exam = exam.Where(a => a.OnlineExamTypeId == examtype).ToList();
            }
            if (exam == null || examStudents == null)
            {
                return 0;
            }
            var examques = DbContext.OnlineExamAnswerSheetRemarks.Where(a => a.Is_Active == true).ToList();
            var examans = (from b in examStudents
                           join c in exam on b.OnlineExamId equals c.OnlineExamId
                           select new
                           {
                               total = examques.Where(a => a.OnlineExamAnswerSheetId == b.OnlineExamAnswerSheetId).Sum(a => a.GivenMarks)
                           }).ToList();

            var Totalmark = examans.Average(a => a.total);
            if (Totalmark == null)
            {
                return 0;
            }
            else
            {
                return Totalmark;
            }
        }
        public double GetOnlineWorksheetExamscore(List<tbl_DC_Worksheet> examStudents, List<tbl_DC_Module> modules, long? examtype)
        {
            if (examtype != null)
            {
                modules = modules.Where(a => a.Exam_Type == examtype).ToList();
            }
            if (modules == null || examStudents == null)
            {
                return 0;
            }
            var res = (from a in examStudents
                       join b in modules on a.Chapter_Id equals b.Chapter_Id
                       select a).ToList();
            if (res.Count == 0)
            {
                return 0;
            }
            var Totalmark = res.Average(a => a.Total_Mark);
            if (Totalmark == null)
            {
                return 0;
            }
            else
            {
                return Totalmark;
            }
        }


        #endregion
    }
}
