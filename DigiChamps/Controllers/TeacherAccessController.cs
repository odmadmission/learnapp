﻿using CrystalDecisions.Shared.Json;
using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Drawing.Imaging;
using ClosedXML.Excel;
using System.Data;
using System.Collections;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Threading;

namespace DigiChamps.Controllers
{
    public class TeacherAccessController : Controller
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }
        #region ---------- Teacher rDashboard ------------
        public ActionResult TeacherDashboard()
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }

        }
        #endregion

        #region ---------------- ---Login ------------------

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Login(string User_name, string password)
        {
            try
            {
                var obj = DbContext.Teachers.Where(x => x.Mobile == User_name && x.Password == password).FirstOrDefault();

                if (obj != null)
                {
                    Session["TeacherId"] = obj.TeacherId;
                    Session["SchoolId"] = obj.SchoolId;
                    Session["Name"] = obj.Name;
                    Session["Email"] = obj.Email;
                    Session["Mobile"] = obj.Mobile;
                    Session["DepartmentName"] = obj.DepartmentName;
                    Session["Photo"] = obj.Photo;
                    Session["IsHead"] = obj.IsHead;

                    return RedirectToAction("TeacherDashboard");
                }
                else
                {
                    TempData["ErrorMessage"] = "Invalid credential for Teacher login.";
                    return View();

                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View();
        }
        #endregion

        #region ------------ MyProfile-------------------
        [HttpGet]
        public ActionResult MyProfile()
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                var TeacherId = Convert.ToInt32(Session["TeacherId"]);
                Teacher _teachr = DbContext.Teachers.Where(x => x.TeacherId == TeacherId).FirstOrDefault();
                ViewBag.TeacherId = _teachr.TeacherId;
                ViewBag.Name = _teachr.Name;
                ViewBag.Email = _teachr.Email;
                //string date = Convert.ToString(_teachr.DateOfBirth).Substring(0, 10);
                //ViewBag.dateofbirth = date;
                ViewBag.Mobile = _teachr.Mobile;
                ViewBag.DepartmentName = _teachr.DepartmentName;
                ViewBag.IsHead = _teachr.IsHead;
                ViewBag.image = _teachr.Photo;

                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult EditProfile(long TeacherId, HttpPostedFileBase Photo, string Name, string Mobile, string Email, string DepartmentName, string IsHead)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                try
                {
                    var teacherid = Convert.ToInt32(Session["TeacherId"]);
                    var alldetail = DbContext.Teachers.Where(x => x.TeacherId == TeacherId).FirstOrDefault();
                    string image = string.Empty;
                    if (alldetail != null)
                    {
                        alldetail.Name = Name;
                        alldetail.ModifiedOn = today;
                        alldetail.Mobile = Mobile;
                        alldetail.Email = Email;
                        alldetail.DepartmentName = DepartmentName;
                        if (IsHead == "on")
                        {
                            alldetail.IsHead = true;
                        }
                        else
                        {
                            alldetail.IsHead = false;
                        }
                        string guid = Guid.NewGuid().ToString();
                        if (Photo != null)
                        {
                            var fileName = Path.GetFileName(Photo.FileName.Replace(Photo.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            image = Convert.ToString(fileName);
                            var path = Path.Combine(Server.MapPath("~/Images/TeacherAccess/"), fileName);
                            Photo.SaveAs(path);
                            alldetail.Photo = image;
                        }

                        DbContext.Entry(alldetail).State = EntityState.Modified;
                        DbContext.SaveChanges();
                        TempData["SuccessMessage"] = " Successfully Edit Profile ";
                        return RedirectToAction("MyProfile");
                    }
                }
                catch
                {
                    return View();
                }
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }


        [HttpGet]
        public ActionResult ChangePassword()
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        [HttpPost]
        public ActionResult TeacherChangePassword(string Old_Password, string New_Password, string Conf_Password)
        {
            string message = string.Empty;
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                if (New_Password != "" && Conf_Password != "" && Old_Password != "")
                {
                    var data = DbContext.Teachers.Where(x => x.Password == Old_Password).FirstOrDefault();

                    if (data != null)
                    {
                        if (New_Password == Conf_Password)
                        {
                            data.Password = New_Password;
                            DbContext.Entry(data).State = EntityState.Modified;
                            DbContext.SaveChanges();
                            TempData["SuccessMessage"] = " Successfully Edit Profile ";
                            return RedirectToAction("ChangePassword");
                        }
                        else
                        {
                            TempData["ErrorMessage"] = " New Password And Confirm Password Did Not Match, ";
                            return RedirectToAction("ChangePassword");
                        }
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Please Enter Old Password, Did Not Match, ";
                        return RedirectToAction("ChangePassword");
                    }


                }
                else
                {
                    TempData["WarningMessage"] = "Please Enter Password. ";
                    return RedirectToAction("ChangePassword");
                }
            }
            else
            {
                return RedirectToAction("Login");
            }

        }


        #endregion

        #region  -----------------Logout ---------------------

        public ActionResult Logout()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
        #endregion

        #region -----------------Assigned Subject------------------
        public ActionResult AssignedSubject()
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {

                var teacherid = Convert.ToInt32(Session["TeacherId"]);
                var Schoollist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
                var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var classlist = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var sectionlist = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var subjectlist = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var teacherslist = DbContext.Teachers.Where(a => a.Active == 1).ToList();
                var assignteacherlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
                var res = (from a in assignteacherlist
                           join b in teacherslist on a.Teacher_ID equals b.TeacherId
                           join c in subjectlist on a.Subject_Id equals c.Subject_Id
                           join d in sectionlist on a.SectionId equals d.SectionId
                           join e in classlist on a.Class_Id equals e.Class_Id
                           join f in boardlist on a.Board_Id equals f.Board_Id

                           select new AssignTeacherCls
                           {
                               AssignId = a.AssignSubjectToTeacher_Id,
                               TeacherId = a.Teacher_ID,
                               BoardId = a.Board_Id,
                               BoardName = f.Board_Name,
                               ClassId = a.Class_Id,
                               ClassName = e.Class_Name,
                               SchoolId = b.SchoolId,
                               SchoolName = Schoollist.Where(z => z.SchoolId == b.SchoolId).Select(z => z.SchoolName).FirstOrDefault(),
                               SectionId = a.SectionId,
                               SectionName = d.SectionName,
                               SubjectId = a.Subject_Id,
                               SubjectName = c.Subject,
                               TeacherName = b.Name
                           }).ToList();

                return View(res);
            }
            else
            {
                return RedirectToAction("Login");
            }

        }
        #endregion

        #region ------------- Student Details By Section Id ------------
        public ActionResult StudentDetails(Guid? SectionId)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {

                var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.SectionId == SectionId).ToList();
                var std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var schools = DbContext.tbl_DC_School_Info.ToList();
                var classs = DbContext.tbl_DC_Class.ToList();
                var sections = DbContext.tbl_DC_Class_Section.ToList();
                var boards = DbContext.tbl_DC_Board.ToList();


                var res = (from a in students
                           join b in std_dtls on a.Regd_ID equals b.Regd_ID

                           select new StudentDetails_Class
                           {
                               Regd_ID = a.Regd_ID,
                               Regd_No = a.Regd_No,
                               Customer_Name = a.Customer_Name,
                               SchoolId = (a.SchoolId == null && a.SchoolId == Guid.Empty) ? Guid.Empty : a.SchoolId,
                               Class_ID = b.Class_ID == null ? null : b.Class_ID,
                               SectionId = (a.SectionId == null && a.SectionId == Guid.Empty) ? Guid.Empty : a.SectionId,
                               Board_ID = b.Board_ID == null ? null : b.Board_ID,
                               boardname = b.Board_ID == null ? "" : boards.Where(c => c.Board_Id == b.Board_ID).FirstOrDefault().Board_Name,
                               schoolname = (a.SchoolId == null && a.SchoolId == Guid.Empty) ? "" : schools.Where(c => c.SchoolId == a.SchoolId).FirstOrDefault().SchoolName,
                               Class_Name = b.Class_ID == null ? "" : classs.Where(c => c.Class_Id == b.Class_ID).FirstOrDefault().Class_Name,
                               SectionName = (a.SectionId == Guid.Empty && a.SectionId == null) ? "" : sections.Where(c => c.SectionId == a.SectionId).FirstOrDefault().SectionName,
                               Mobile = a.Mobile,
                               Email = a.Email
                           }).ToList();
                return View(res);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public class StudentDetails_Class
        {
            public int? Regd_ID { set; get; }
            public string Regd_No { get; set; }
            public string Customer_Name { get; set; }
            public Nullable<Guid> SchoolId { get; set; }
            public Nullable<int> Class_ID { get; set; }
            public Nullable<Guid> SectionId { get; set; }
            public Nullable<int> Board_ID { get; set; }
            public string boardname { get; set; }
            public string schoolname { get; set; }
            public string Class_Name { get; set; }
            public string SectionName { get; set; }
            public string Mobile { get; set; }
            public string Email { get; set; }

        }
        #endregion

        #region---------------Student Dashboard -------------
        public ActionResult StudentDashboard(Guid? School, int? board, int? cls, Guid? sec, int? subject, int? Chapter)
        {
            var username = Convert.ToString(Session["Mobile"]);
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            if (username != null && username != "")
            {
                ViewBag.school = null;
                ViewBag.board = null;
                ViewBag.cls = null;
                ViewBag.sec = null;
                ViewBag.subject = null;
                ViewBag.chapter = null;
                Guid id = new Guid(Session["SchoolId"].ToString());

                var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
                var boardlistss = assigndet.Select(a => a.Board_Id).Distinct().ToList();
                var classlist = assigndet.Select(a => a.Class_Id).Distinct().ToList();
                var sectionlist = assigndet.Select(a => a.SectionId).Distinct().ToList();
                var subjectlist = assigndet.Select(a => a.Subject_Id).Distinct().ToList();

                var wrk = DbContext.VW_Worksheet_Dashboard.ToList();


                var res = (from a in wrk
                           where boardlistss.Contains(a.Board_Id) && classlist.Contains(a.Class_Id) && sectionlist.Contains(a.SectionId) && subjectlist.Contains(a.Subject_Id)
                           select a).ToList().Distinct();

                if (board != null)
                {
                    res = res.Where(a => a.Board_Id == board).ToList();
                    ViewBag.board = board.Value;
                }

                if (cls != null)
                {
                    res = res.Where(a => a.Class_Id == cls).ToList();
                    ViewBag.cls = cls.Value;

                }
                if (sec != null)
                {
                    res = res.Where(a => a.SectionId == sec).ToList();
                    ViewBag.sec = sec.Value;
                }
                if (subject != null)
                {
                    res = res.Where(a => a.Subject_Id == subject).ToList();
                    ViewBag.subject = subject.Value;
                }
                if (Chapter != null)
                {
                    res = res.Where(a => a.Chapter_Id == Chapter).ToList();
                    ViewBag.chapter = Chapter.Value;
                }
                if (School != null)
                {
                    ViewBag.school = School.Value;
                }

                return View(res);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }


        public JsonResult GetSchoolList()
        {
            var SchoolId = new Guid(Session["SchoolId"].ToString());
            var school = DbContext.tbl_DC_School_Info.Where(x => x.IsActive == true && x.SchoolId == SchoolId).ToList();
            var res = (from a in school
                       select new
                       {
                           SchoolId = a.SchoolId,
                           School_Name = a.SchoolName,

                       }).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBoardList()
        {
            var board = DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
            var res = (from a in board
                       select new
                       {
                           Board_Id = a.Board_Id,
                           Board_Name = a.Board_Name,

                       }).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetClassListdtl()
        {
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            var classlist = DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false).ToList();
            var assignteacherlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
            var res = (from a in assignteacherlist
                       join b in classlist on a.Class_Id equals b.Class_Id

                       select new
                       {
                           Class_Id = a.Class_Id,
                           Class_Name = b.Class_Name,

                       }).Distinct().ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSectionListdtl(int id)
        {

            Guid? schoolid = new Guid(Session["SchoolId"].ToString());
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            var scetionlist = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.School_Id == schoolid && x.Class_Id == id).ToList();
            var assignteacherlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
            var res = (from a in assignteacherlist
                       join b in scetionlist on a.SectionId equals b.SectionId

                       select new
                       {
                           SectionId = a.SectionId,
                           SectionName = b.SectionName

                       }).Distinct().ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSubjectData(int id)
        {

            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            var subjectlist = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Class_Id == id).ToList();
            var assignteacherlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).Select(a => new { a.Subject_Id }).Distinct().ToList();
            var res = (from a in assignteacherlist
                       join b in subjectlist on a.Subject_Id equals b.Subject_Id

                       select new
                       {
                           Subject_Id = a.Subject_Id,
                           Subject = b.Subject

                       }).ToList();
            return Json(res.ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetChapterData(int id)
        {
            var s = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Subject_Id == id).ToList();
            return Json(s, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region--------------student Report---------------------------

        public class worksheetDetails
        {
            public int Worksheet_Id { get; set; }
            public int Regd_ID { get; set; }
            public string Student_Name { get; set; }
            public string Student_Mobile { get; set; }
            public int Board_Id { get; set; }
            public string Board_Name { get; set; }
            public int Class_Id { get; set; }
            public string Class_Name { get; set; }
            public Nullable<System.Guid> SectionId { get; set; }
            public string Section_Name { get; set; }
            public int Subject_Id { get; set; }
            public string Subject_Name { get; set; }
            public int Chapter_Id { get; set; }
            public string Chapter_Name { get; set; }
            public bool Verified { get; set; }
            public int Verified_By { get; set; }
            public string Verified_By_Name { get; set; }
            public int Total_Mark { get; set; }
            public Nullable<System.DateTime> Inserted_Date { get; set; }

            public Nullable<System.DateTime> Modified_Date { get; set; }
            public string Upload_Date { get; set; }
            public string Verify_Date { get; set; }
            public int Module_Id { get; set; }
            public string Module_Name { get; set; }
            public string File_Type { get; set; }
            public List<tbl_DC_Worksheet_Attachment> attachments { get; set; }
            public int TotalVerified { get; set; }
            public int TotalPending { get; set; }
            public int TotalSubmitted { get; set; }

            public string Status { get; set; }

        }

        public ActionResult StudentWorksheetReport(Guid? School, int? board, int? cls, Guid? sec, int? subject, int? Chapter, string Status)
        {
            var username = Convert.ToString(Session["Mobile"]);
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            if (username != null && username != "")
            {

                ViewBag.school = null;
                ViewBag.board = null;
                ViewBag.cls = null;
                ViewBag.sec = null;
                ViewBag.subject = null;
                ViewBag.chapter = null;
                ViewBag.Status = "A";

                var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
                var boardlistss = assigndet.Select(a => a.Board_Id).ToList();
                var classlist = assigndet.Select(a => a.Class_Id).ToList();
                var sectionlist = assigndet.Select(a => a.SectionId).ToList();
                var subjectlist = assigndet.Select(a => a.Subject_Id).ToList();
                Guid id = new Guid(Session["SchoolId"].ToString());
                ViewBag.Status = Status;

                List<tbl_DC_Worksheet> wrk = null;
                wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true).ToList();

                var wrksheet = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                // var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.SchoolId == id).ToList();
                var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true).ToList();
                var res = (from a in wrk
                           join b in boardlist on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           //  join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           where boardlistss.Contains(a.Board_Id) && classlist.Contains(a.Class_Id) && sectionlist.Contains(a.SectionId) && subjectlist.Contains(a.Subject_Id)
                           select new worksheetDetails
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Module_Name = "",
                               Regd_ID = a.Regd_ID,
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Student_Mobile = h.Mobile,
                               Total_Mark = a.Total_Mark,
                               Status = a.Verified == true ? "Verified" : "Uploaded",
                               Upload_Date = a.Inserted_Date != null ? Convert.ToDateTime(a.Inserted_Date).ToString("dd-MMM-yyyy") : "NA",
                               Verify_Date = a.Verified == true ? Convert.ToDateTime(a.Modified_Date).ToString("dd-MMM-yyyy") : "NA",
                               TotalVerified = wrk.Where(z => z.Verified == true).ToList().Count(),
                               TotalPending = wrk.Where(z => z.Verified == false).ToList().Count(),
                               TotalSubmitted = wrk.ToList().Count(),
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               Worksheet_Id = a.Worksheet_Id,
                               attachments = (from k in worksheetattachments
                                              where k.Worksheet_Id == a.Worksheet_Id
                                              select new tbl_DC_Worksheet_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Worksheet/" + k.Path,
                                                  Worksheet_Attachment_Id = k.Worksheet_Attachment_Id,
                                                  Worksheet_Id = k.Worksheet_Id
                                              }).ToList(),
                           }).ToList();
                if (board != null)
                {
                    res = res.Where(a => a.Board_Id == board).ToList();
                    ViewBag.board = board.Value;
                }

                if (cls != null)
                {
                    res = res.Where(a => a.Class_Id == cls).ToList();
                    ViewBag.cls = cls.Value;
                }
                if (sec != null)
                {
                    res = res.Where(a => a.SectionId == sec).ToList();
                    ViewBag.sec = sec.Value;
                }
                if (subject != null)
                {
                    res = res.Where(a => a.Subject_Id == subject).ToList();
                    ViewBag.subject = subject.Value;
                }
                if (Chapter != null)
                {
                    res = res.Where(a => a.Chapter_Id == Chapter).ToList();
                    ViewBag.chapter = Chapter.Value;
                }
                if (Status == "V")
                {
                    res = res.Where(a => a.Verified == true).ToList();
                    ViewBag.Status = "V";
                }
                if (Status == "U")
                {
                    res = res.Where(a => a.Verified == false).ToList();
                    ViewBag.Status = "U";
                }
                if (School != null)
                {
                    ViewBag.school = School.Value;
                }
                Session["submittedworksheet"] = res;
                return View(res);

            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        public ActionResult DownloadSubmittedWorksheetExcel()
        {
            XLWorkbook oWB = new XLWorkbook();

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Student_Name");
            validationTable.Columns.Add("Mobile");
            validationTable.Columns.Add("Board");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Section");
            validationTable.Columns.Add("Subject");
            validationTable.Columns.Add("Chapter");
            validationTable.Columns.Add("Mark");
            validationTable.Columns.Add("Status");
            validationTable.Columns.Add("Verify Date");
            validationTable.Columns.Add("Verify By");
            validationTable.Columns.Add("File Name");
            validationTable.Columns.Add("Attachment");
            validationTable.Columns.Add("Uploaded Date");
            List<worksheetDetails> worksheets = new List<worksheetDetails>();
            if (Session["submittedworksheet"] != null)
            {
                worksheets = Session["submittedworksheet"] as List<worksheetDetails>;
            }
            foreach (var a in worksheets)
            {
                if (a.attachments.Count > 0)
                {
                    DataRow dr = validationTable.NewRow();
                    dr["Student_Name"] = a.Student_Name;
                    dr["Mobile"] = a.Student_Mobile;
                    dr["Board"] = a.Board_Name;
                    dr["Class"] = a.Class_Name;
                    dr["Section"] = a.Section_Name;
                    dr["Subject"] = a.Subject_Name;
                    dr["Chapter"] = a.Chapter_Name;
                    dr["Mark"] = a.Total_Mark;
                    dr["Status"] = a.Status;
                    dr["Verify Date"] = a.Verify_Date;
                    dr["Verify By"] = a.Verified_By_Name;
                    dr["File Name"] = a.attachments.FirstOrDefault().File_Name;
                    dr["Attachment"] = "http://learn.odmps.org" + a.attachments.FirstOrDefault().Path;
                    dr["Uploaded Date"] = a.attachments.FirstOrDefault().Modified_Date.Value.ToString("dd-MM-yyyy");
                    validationTable.Rows.Add(dr);
                }
            }
            validationTable.TableName = "Student_Details";
            var worksheet = oWB.AddWorksheet(validationTable);

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "Worksheetsubmitted.xlsx");
        }

        public ActionResult StudentNotUploadWorksheetReport(Guid? School, int? board, int? cls, Guid? sec, int? subject, int? Chapter)
        {
            var username = Convert.ToString(Session["Mobile"]);
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            if (username != null && username != "")
            {
                ViewBag.school = null;
                ViewBag.board = null;
                ViewBag.cls = null;
                ViewBag.sec = null;
                ViewBag.subject = null;
                ViewBag.chapter = null;
                Guid id = new Guid(Session["SchoolId"].ToString());

                var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
                var boardlistss = assigndet.Select(a => a.Board_Id).ToList();
                var classlist = assigndet.Select(a => a.Class_Id).ToList();
                var sectionlist = assigndet.Select(a => a.SectionId).ToList();
                var subjectlist = assigndet.Select(a => a.Subject_Id).ToList();

                var wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true).ToList();
                var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                //   var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true).ToList();

                if (wrk != null)
                {
                    var res = (from a in wrk
                               join b in boardlist on a.Board_Id equals b.Board_Id
                               join c in classes on a.Class_Id equals c.Class_Id
                               join d in subjects on a.Subject_Id equals d.Subject_Id
                               join e in chapters on a.Chapter_Id equals e.Chapter_Id
                               //   join f in modules on a.Module_Id equals f.Module_ID
                               join g in sections on a.SectionId equals g.SectionId
                               join h in students on a.Regd_ID equals h.Regd_ID
                               where boardlistss.Contains(a.Board_Id) && classlist.Contains(a.Class_Id) && sectionlist.Contains(a.SectionId) && subjectlist.Contains(a.Subject_Id)
                               select new worksheetDetails
                               {
                                   Board_Id = a.Board_Id,
                                   Board_Name = b.Board_Name,
                                   Chapter_Id = a.Chapter_Id,
                                   Chapter_Name = e.Chapter,
                                   Class_Id = a.Class_Id,
                                   Class_Name = c.Class_Name,
                                   File_Type = a.File_Type,
                                   Inserted_Date = a.Inserted_Date,
                                   Modified_Date = a.Modified_Date,
                                   Module_Id = a.Module_Id,
                                   Module_Name = "",
                                   // Status = (a.is_Read == false) ? "NEW" : (((a.is_Read == true || a.is_Read == null) && a.Verified == true) ? "SCORED" : "PENDING"),
                                   Regd_ID = a.Regd_ID,
                                   Section_Name = g.SectionName,
                                   SectionId = a.SectionId,
                                   Student_Name = h.Customer_Name,
                                   Student_Mobile = h.Mobile,
                                   Subject_Id = a.Subject_Id,
                                   Subject_Name = d.Subject,
                                   Total_Mark = a.Total_Mark,
                                   Verified = a.Verified,
                                   Verified_By = a.Verified_By,
                                   Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                                   Worksheet_Id = a.Worksheet_Id,
                                   attachments = (from k in worksheetattachments
                                                  where k.Worksheet_Id == a.Worksheet_Id
                                                  select new tbl_DC_Worksheet_Attachment
                                                  {
                                                      File_Name = k.File_Name,
                                                      Modified_Date = k.Modified_Date,
                                                      Path = "/Images/Worksheet/" + k.Path,
                                                      Worksheet_Attachment_Id = k.Worksheet_Attachment_Id,
                                                      Worksheet_Id = k.Worksheet_Id
                                                  }).ToList(),
                               }).ToList();

                    if (board != null)
                    {
                        res = res.Where(a => a.Board_Id == board).ToList();
                        ViewBag.board = board.Value;
                    }

                    if (cls != null)
                    {
                        res = res.Where(a => a.Class_Id == cls).ToList();
                        ViewBag.cls = cls.Value;
                    }
                    if (sec != null)
                    {
                        res = res.Where(a => a.SectionId == sec).ToList();
                        ViewBag.sec = sec.Value;
                    }
                    if (subject != null)
                    {
                        res = res.Where(a => a.Subject_Id == subject).ToList();
                        ViewBag.subject = subject.Value;
                    }
                    if (Chapter != null)
                    {
                        res = res.Where(a => a.Chapter_Id == Chapter).ToList();
                        ViewBag.chapter = Chapter.Value;
                    }
                    if (School != null)
                    {
                        ViewBag.school = School.Value;
                    }
                    var studentslist = (from a in students.Where(a => a.SectionId == sec && !res.Select(b => b.Regd_ID).ToList().Contains(a.Regd_ID))

                                        select new StdName
                                        {
                                            Name = a.Customer_Name,
                                            Mobile = a.Mobile,
                                            Email = a.Email != null ? a.Email : "NA",
                                            SectionId = a.SectionId,
                                            Section = sections.Where(x => x.SectionId == a.SectionId).Select(x => x.SectionName).FirstOrDefault(),
                                        }
                                          ).ToList();
                    Session["Notsubmittedworksheet"] = studentslist;
                    return View(studentslist);
                }
                else
                {
                    return View();
                }



            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult DownloadNotSubmittedWorksheetExcel()
        {
            XLWorkbook oWB = new XLWorkbook();

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Student_Name");
            validationTable.Columns.Add("Mobile");
            validationTable.Columns.Add("Email");
            validationTable.Columns.Add("Section");

            List<StdName> worksheets = new List<StdName>();
            if (Session["Notsubmittedworksheet"] != null)
            {
                worksheets = Session["Notsubmittedworksheet"] as List<StdName>;
            }
            foreach (var a in worksheets)
            {
                DataRow dr = validationTable.NewRow();
                dr["Student_Name"] = a.Name;
                dr["Mobile"] = a.Mobile;
                dr["Email"] = a.Email;
                dr["Section"] = a.Section;
                validationTable.Rows.Add(dr);

            }
            validationTable.TableName = "Student_Details";
            var worksheet = oWB.AddWorksheet(validationTable);

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "Worksheetnotsubmitted.xlsx");
        }
        public class WorksheetHolder
        {
            public List<worksheetDetails> data { get; set; }

            public List<StdName> students { get; set; }


            public int total { get; set; }

            public int uploaded { get; set; }

            public int NotUploaded { get; set; }

        }
        public class StdName
        {
            public string Name { get; set; }
            public string Mobile { get; set; }
            public Guid? SectionId { get; set; }
            public string Section { get; set; }
            public string Email { get; set; }

        }

        [HttpGet]
        public ActionResult EditWorksheet(int id)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                Guid? schoolid = new Guid(Session["SchoolId"].ToString());
                ViewBag.Board_details = DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
                ViewBag.Class_details = DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
                ViewBag.Section_details = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.Class_Id == id && x.School_Id == schoolid).Distinct().ToList();
                ViewBag.Subject_details = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                ViewBag.Chapter_details = DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false).ToList();


                try
                {
                    var wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Worksheet_Id == id).ToList();
                    var board = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                    var classes = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                    var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                    var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                    // var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                    var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                    var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                    var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
                    var worksheetattachments = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true).ToList();
                    var res = (from a in wrk
                               join b in board on a.Board_Id equals b.Board_Id
                               join c in classes on a.Class_Id equals c.Class_Id
                               join d in subjects on a.Subject_Id equals d.Subject_Id
                               join e in chapters on a.Chapter_Id equals e.Chapter_Id
                               //  join f in modules on a.Module_Id equals f.Module_ID
                               join g in sections on a.SectionId equals g.SectionId
                               join h in students on a.Regd_ID equals h.Regd_ID
                               select new worksheetDetails
                               {
                                   Board_Id = a.Board_Id,
                                   Board_Name = b.Board_Name,
                                   Chapter_Id = a.Chapter_Id,
                                   Chapter_Name = e.Chapter,
                                   Class_Id = a.Class_Id,
                                   Class_Name = c.Class_Name,
                                   File_Type = a.File_Type,
                                   Inserted_Date = a.Inserted_Date,
                                   Modified_Date = a.Modified_Date,
                                   Module_Id = a.Module_Id,
                                   Module_Name = "",
                                   Regd_ID = a.Regd_ID,
                                   Section_Name = g.SectionName,
                                   SectionId = a.SectionId,
                                   Student_Name = h.Customer_Name,
                                   Subject_Id = a.Subject_Id,
                                   Subject_Name = d.Subject,
                                   Total_Mark = a.Total_Mark,
                                   Upload_Date = a.Inserted_Date != null ? Convert.ToDateTime(a.Inserted_Date).ToString("dd-MMM-yyyy") : "NA",
                                   Verify_Date = a.Verified == true ? Convert.ToDateTime(a.Modified_Date).ToString("dd-MMM-yyyy") : "NA",
                                   Verified = a.Verified,
                                   Verified_By = a.Verified_By,
                                   Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                                   Worksheet_Id = a.Worksheet_Id,
                                   attachments = (from k in worksheetattachments
                                                  where k.Worksheet_Id == a.Worksheet_Id
                                                  select new tbl_DC_Worksheet_Attachment
                                                  {
                                                      File_Name = k.File_Name,
                                                      Modified_Date = k.Modified_Date,
                                                      Path = "/Images/Worksheet/" + k.Path,
                                                      Worksheet_Attachment_Id = k.Worksheet_Attachment_Id,
                                                      Worksheet_Id = k.Worksheet_Id
                                                  }).ToList(),
                               }).FirstOrDefault();

                    ViewBag.WorksheetDetails = res;


                    return View(res);
                }

                catch
                {
                    return View();
                }

            }
            else
            {
                return RedirectToAction("Login");
            }
        }


        [HttpPost]
        public ActionResult EditWorksheet(tbl_DC_Worksheet worksheet)
        {
            try
            {
                var teacherid = Convert.ToInt32(Session["TeacherId"]);
                var alldetail = DbContext.tbl_DC_Worksheet.Where(x => x.Worksheet_Id == worksheet.Worksheet_Id).FirstOrDefault();
                if (alldetail != null)
                {
                    alldetail.Total_Mark = worksheet.Total_Mark;
                    alldetail.Modified_Date = today;
                    alldetail.Modified_By = teacherid;
                    DbContext.Entry(alldetail).State = EntityState.Modified;
                    DbContext.SaveChanges();
                    TempData["SuccessMessage"] = " Successfully Edit Worksheet ";
                    return RedirectToAction("StudentWorksheetReport");
                }
            }
            catch
            {
                return View();
            }
            return View();

        }


        #endregion

        #region-----------------------Ticket--------------------

        public ActionResult TeacherTicket(Guid? sec, Guid? School, int? board, int? cls, int? subject, int? Chapter, string Status, string f_Date, string t_Date)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                try
                {
                    ViewBag.school = null;
                    ViewBag.board = null;
                    ViewBag.cls = null;
                    ViewBag.sec = null;
                    ViewBag.subject = null;
                    ViewBag.chapter = null;
                    ViewBag.status = null;
                    ViewBag.fdt = null;
                    ViewBag.tdt = null;



                    var teacherid = Convert.ToInt32(Session["TeacherId"]);
                    //  var assignteacherSubjectlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).Select(a => a.Subject_Id).Distinct().ToList();

                    var tickets = DbContext.View_DC_All_Tickets_Details.Where(r => r.Is_Active == true && r.Teach_ID == teacherid).ToList();
                    //  List<View_DC_Tickets_and_Teacher> data = null;
                    // data = DbContext.View_DC_Tickets_and_Teacher.Where(r => assignteacherSubjectlist.Contains(r.Subject_ID.Value)).OrderByDescending(x => x.Ticket_ID).ToList();

                    if (School != null)
                    {
                        ViewBag.school = School.Value;
                    }

                    if (board != null && board != 0)
                    {
                        tickets = tickets.Where(a => a.Board_ID == board).ToList();
                        ViewBag.board = board.Value;
                    }
                    if (cls != null && cls != 0)
                    {
                        tickets = tickets.Where(a => a.Class_ID == cls).ToList();
                        ViewBag.cls = cls.Value;
                    }
                    if (sec != null)
                    {
                        tickets = tickets.Where(a => a.SectionId == sec).ToList();
                        ViewBag.sec = sec.Value;
                    }
                    if (subject != null && subject != 0)
                    {
                        tickets = tickets.Where(a => a.Subject_ID == subject).ToList();
                        ViewBag.subject = subject.Value;
                    }
                    if (Chapter != null && Chapter != 0)
                    {
                        tickets = tickets.Where(a => a.Chapter_Id == Chapter).ToList();
                        ViewBag.chapter = Chapter.Value;
                    }
                    if (Status != null && Status != "A")
                    {
                        ViewBag.status = Status;
                        if (Status == "O")
                        {
                            tickets = tickets.Where(a => a.Status == "O" && a.Answer == "False").ToList();
                        }
                        if (Status == "S")
                        {
                            tickets = tickets.Where(a => a.Status == "O" && a.Answer == "True").ToList();
                        }
                        if (Status == "R")
                        {
                            tickets = tickets.Where(a => a.Status == "R").ToList();
                        }
                        //if (Status == "U")
                        //{
                        //    tickets = tickets.Where(a => a.Status == "U").ToList();
                        //}
                        if (Status == "C")
                        {
                            tickets = tickets.Where(a => a.Status == "C").ToList();
                        }
                    }
                    if (f_Date != "" && t_Date != "" && f_Date != null && t_Date != null)
                    {
                        if (Convert.ToDateTime(f_Date) <= Convert.ToDateTime(t_Date))
                        {
                            string fdtt = f_Date + " 00:00:00 AM";
                            string tdtt = t_Date + " 23:59:59 PM";
                            DateTime fdt = Convert.ToDateTime(fdtt);
                            DateTime tdt = Convert.ToDateTime(tdtt);
                            ViewBag.fdt = Convert.ToDateTime(f_Date).ToString("yyyy-MM-dd");
                            ViewBag.tdt = Convert.ToDateTime(t_Date).ToString("yyyy-MM-dd");
                            tickets = tickets.Where(a => a.Inserted_Date >= fdt && tdt >= a.Inserted_Date).ToList();

                        }
                    }
                    ViewBag.Ticket = tickets;
                    Session["Tickets"] = tickets;

                    ViewBag.teachernames_tickets = DbContext.View_DC_CourseAssign.ToList();
                }
                catch (Exception ex)
                {
                    TempData["ErrorMessage"] = "Something went wrong.";
                }
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }

        }
        public ActionResult ViewDoughtDetail(int? id)
        {
            try
            {
                ViewBag.Breadcrumb = "Ticket";
                if (id != null)
                {
                    var get = DbContext.View_DC_All_Tickets_Details.Where(x => x.Ticket_ID == id).ToList();
                    if (get.Count > 0)
                    {
                        ViewBag.check_answer = DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        ViewBag.teacher = get.FirstOrDefault().Teach_ID;
                        ViewBag.status = get.FirstOrDefault().Status;
                        if (get.FirstOrDefault().Teach_ID != null)
                        {
                            int tr = Convert.ToInt32(get.FirstOrDefault().Teach_ID);
                            var tdata = DbContext.Teachers.Where(x => x.TeacherId == tr).FirstOrDefault();
                            if (tdata != null)
                            {
                                if (tdata.Name != null)
                                {
                                    ViewBag.teacher = tdata.Name;
                                }
                                else
                                {
                                    ViewBag.teacher = null;
                                }

                            }
                            else
                            {
                                ViewBag.teacher = null;
                            }
                        }
                        ViewBag.viewticket = get.ToList();
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Invalid ticket details.";
                        return RedirectToAction("TeacherTicket");
                    }


                }
                else
                {
                    TempData["ErrorMessage"] = "Invalid ticket details.";
                    return RedirectToAction("TeacherTicket");
                }
            }
            catch (Exception ex)
            {

                TempData["ErrorMessage"] = "Something went wrong.";
                return RedirectToAction("TeacherTicket");
            }
            return View();
        }



        [HttpGet]
        public ActionResult RejectTicket(int? id)
        {
            try
            {

                ViewBag.Breadcrumb = "Ticket";
                if (id != null)
                {
                    var tdata = DbContext.View_DC_All_Tickets_Details.Where(x => x.Ticket_ID == id).FirstOrDefault();


                    ViewBag.studentname = tdata.Customer_Name;


                    ViewBag.classname = tdata.Class_Name;

                    ViewBag.question = tdata.Question.ToString();
                    ViewBag.tkid = tdata.Ticket_ID;
                    ViewBag.status = tdata.Status;
                    ViewBag.remark = tdata.Remark;
                }
                else
                {
                    TempData["ErrorMessage"] = "Invalid ticket details.";
                    return RedirectToAction("TeacherTicket");
                }
            }
            catch (Exception ex)
            {

                TempData["ErrorMessage"] = "Something went wrong.";

            }

            return View();
        }

        [HttpPost]
        public ActionResult RejectTicket(string Remark_Reject, string h_tkid)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                try
                {
                    if (Remark_Reject.Trim() != "")
                    {
                        int id = Convert.ToInt32(h_tkid);
                        tbl_DC_Ticket tkt_rj = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        tkt_rj.Status = "R";
                        tkt_rj.Remark = Remark_Reject;
                        DbContext.SaveChanges();
                        var get_student = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == tkt_rj.Student_ID).FirstOrDefault();
                        //if (get_student.Email != null)
                        //{
                        //    //Close_ticket_mail(tkt_rj.Student_ID.ToString(), tkt_rj.Ticket_No);
                        //    sendMail_close_reject("Ticket_close", get_student.Email.ToString(), get_student.Customer_Name, tkt_rj.Ticket_No.ToString(), "R", Remark_Reject.ToString());
                        //}
                        TempData["SuccessMessage"] = "Question is rejected.";
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Please provide reason of rejection.";
                    }
                }
                catch (Exception ex)
                {
                    TempData["WarningMessage"] = "Something went wrong.";

                }

                return RedirectToAction("RejectTicket");
            }
            else
            {
                return RedirectToAction("Logout", "TeacherAccess");
            }

        }

        [HttpGet]
        public ActionResult AnswerTicket(int? id)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                try
                {

                    ViewBag.Breadcrumb = "Answer Ticket";
                    if (id != null)
                    {

                        var ticket_qsn = DbContext.View_DC_All_Tickets_Details.Where(x => x.Ticket_ID == id).FirstOrDefault();
                        ViewBag.student_name = ticket_qsn.Customer_Name;
                        ViewBag.TClass_name = ticket_qsn.Class_Name;
                        ViewBag.tquestion = ticket_qsn.Question;
                        ViewBag.status = ticket_qsn.Status;
                        ViewBag.h_tkid = ticket_qsn.Ticket_ID;
                        ViewBag.ticketno = ticket_qsn.Ticket_No;
                        ViewBag.studentname = ticket_qsn.Student_ID;
                        var ticket_answer = DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList();


                        ViewBag.all_ticketansr = DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList();
                        ViewBag.comments = DbContext.tbl_DC_Ticket_Thread.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList();
                        ViewBag.isclosed = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Status).FirstOrDefault();
                        ViewBag.check_answer = DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    }
                    else
                    {
                        return RedirectToAction("TeacherTicket", "TeacherAccess");
                    }
                }
                catch (Exception ex)
                {

                    TempData["ErrorMessage"] = "Something went wrong.";
                    return RedirectToAction("Viewticekts", "TeacherAccess");
                }
            }
            else
            {
                return RedirectToAction("Logout", "TeacherAccess");
            }

            return View();
        }
        [HttpPost]
        public JsonResult AnswerReply(int Ticket_id, int Ticket_answerid, string msgbody, string close, string remark)
        {

            string message = string.Empty;
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                if (remark != "" || msgbody != "")
                {
                    try
                    {
                        var teacherid = Convert.ToInt32(Session["TeacherId"]);
                        tbl_DC_Ticket_Thread _ticket_thred = new tbl_DC_Ticket_Thread();
                        _ticket_thred.Ticket_ID = Ticket_id;
                        _ticket_thred.Ticket_Dtl_ID = Ticket_answerid;
                        _ticket_thred.User_Comment = msgbody;
                        _ticket_thred.User_Comment_Date = today;
                        _ticket_thred.User_Id = teacherid;
                        _ticket_thred.Is_Teacher = true;
                        _ticket_thred.Is_Active = true;
                        _ticket_thred.Is_Deleted = false;
                        DbContext.tbl_DC_Ticket_Thread.Add(_ticket_thred);
                        DbContext.SaveChanges();
                        if (close == "on")
                        {
                            tbl_DC_Ticket_Assign _tbl_close = DbContext.tbl_DC_Ticket_Assign.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                            _tbl_close.Is_Close = true;
                            _tbl_close.Remark = remark;
                            _tbl_close.Close_Date = today;
                            _tbl_close.Modified_By = teacherid;
                            _tbl_close.Modified_Date = today;
                            DbContext.SaveChanges();
                            tbl_DC_Ticket _tbl_status = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                            _tbl_status.Status = "C";
                            _tbl_status.Modified_By = teacherid;
                            _tbl_status.Modified_Date = today;
                            DbContext.SaveChanges();
                            var get_student = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _tbl_close.Student_ID).FirstOrDefault();
                            //if (get_student.Email != null)
                            //{
                            //    sendMail_close_reject("Ticket_close", get_student.Email.ToString(), get_student.Customer_Name, _tbl_close.Ticket_No.ToString(), "C", remark);
                            //}
                        }
                        message = "1";
                    }
                    catch (Exception ex)
                    {
                        message = "2";

                    }
                }
                else
                {
                    message = "3";
                }
            }
            else
            {
                message = "4";
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AnswerTicket(string Answer_Ticket, HttpPostedFileBase RegImage3, string h_tkid)
        {

            if (h_tkid != "")
            {
                if (Answer_Ticket.Trim() != "")
                {
                    int id = Convert.ToInt32(h_tkid);

                    tbl_DC_Ticket_Dtl tk_dtl = new tbl_DC_Ticket_Dtl();
                    tk_dtl.Ticket_ID = id;
                    tk_dtl.Answer = Answer_Ticket;
                    tk_dtl.Replied_By = 1;//hard_coded
                    tk_dtl.Replied_Date = today;
                    tk_dtl.Is_Active = true;
                    tk_dtl.Is_Deleted = false;
                    if (RegImage3 != null)
                    {

                        string guid = Guid.NewGuid().ToString();
                        var fileName = Path.GetFileName(RegImage3.FileName.Replace(RegImage3.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                        var path = Path.Combine(Server.MapPath("~/Images/Qusetionimages/"), fileName);
                        RegImage3.SaveAs(path);
                        tk_dtl.Answer_Image = fileName.ToString();

                    }
                    DbContext.tbl_DC_Ticket_Dtl.Add(tk_dtl);
                    DbContext.SaveChanges();
                    TempData["SuccessMessage"] = "You have answerd successfully.";
                    return RedirectToAction("AnswerTicket", "TeacherAccess", new { id = h_tkid });
                }
                else
                {
                    TempData["WarningMessage"] = "Insert a answer to the question.";
                    return RedirectToAction("AnswerTicket", "TeacherAccess", new { id = h_tkid });
                }
            }

            else
            {
                TempData["ErrorMessage"] = "Something went wrong.";

            }

            return View();
        }






        public ActionResult TicketDashboard(Guid? sec, Guid? School, int? board, int? cls, int? subject, int? Chapter)
        {
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            ViewBag.school = null;
            ViewBag.board = null;
            ViewBag.cls = null;
            ViewBag.sec = null;
            ViewBag.subject = null;
            ViewBag.chapter = null;


            var tickets = DbContext.View_DC_All_Tickets_Details.Where(r => r.Is_Active == true && r.Teach_ID == teacherid).ToList();
            if (board != null && board != 0)
            {
                tickets = tickets.Where(a => a.Board_ID == board).ToList();
                ViewBag.board = board.Value;
            }
            if (cls != null && cls != 0)
            {
                tickets = tickets.Where(a => a.Class_ID == cls).ToList();
                ViewBag.cls = cls.Value;
            }
            if (sec != null)
            {
                tickets = tickets.Where(a => a.SectionId == sec).ToList();
                ViewBag.sec = sec.Value;
            }
            if (subject != null && subject != 0)
            {
                tickets = tickets.Where(a => a.Subject_ID == subject).ToList();
                ViewBag.subject = subject.Value;
            }
            if (Chapter != null && Chapter != 0)
            {
                tickets = tickets.Where(a => a.Chapter_Id == Chapter).ToList();
                ViewBag.chapter = Chapter.Value;
            }
            if (School != null)
            {
                ViewBag.school = School.Value;
            }

            Session["tickets"] = tickets;
            ViewBag.Doubt = tickets.ToList().Count();
            ViewBag.PendingDoubt = tickets.Where(r => r.Status == "O" && r.Answer == "False").ToList().Count();
            ViewBag.AnsweredDoubt = tickets.Where(r => r.Status == "O" && r.Answer == "True").ToList().Count();
            ViewBag.RejectedDoubt = tickets.Where(r => r.Status == "R").ToList().Count();
            ViewBag.CloseddDoubt = tickets.Where(r => r.Status == "C").ToList().Count();

            //if (board != null && cls != null && subject != null && Chapter != null)
            //{
            //Session["BoardId"] = board;
            //Session["ClassId"] = cls;
            //Session["SubjectId"] = subject;
            //Session["ChapterId"] = Chapter;

            //    ViewBag.Dought = DbContext.View_DC_All_Tickets_Details.Where(r => r.Is_Active == true && r.Teach_ID == teacherid && r.Board_ID == board && r.Class_ID == cls && r.Subject_ID == subject && r.Chapter_Id == Chapter).ToList().Count();
            //    ViewBag.UnsolvedDought = DbContext.View_DC_All_Tickets_Details.Where(r => r.Is_Active == true && r.Teach_ID == teacherid && r.Status == "O" && r.Board_ID == board && r.Class_ID == cls && r.Subject_ID == subject && r.Chapter_Id == Chapter).ToList().Count();
            //    ViewBag.SolvedDought = DbContext.View_DC_All_Tickets_Details.Where(r => r.Is_Active == true && r.Teach_ID == teacherid && r.Status == "C" && r.Board_ID == board && r.Class_ID == cls && r.Subject_ID == subject && r.Chapter_Id == Chapter).ToList().Count();
            //    ViewBag.RejectedDought = DbContext.View_DC_All_Tickets_Details.Where(r => r.Is_Active == true && r.Teach_ID == teacherid && r.Status == "R" && r.Board_ID == board && r.Class_ID == cls && r.Subject_ID == subject && r.Chapter_Id == Chapter).ToList().Count();

            //}


            return View();
        }

        public ActionResult ExcelforTicketDashboard()
        {
            var board = Convert.ToInt32(Session["BoardId"]);
            var cls = Convert.ToInt32(Session["ClassId"]);
            var subject = Convert.ToInt32(Session["SubjectId"]);
            var Chapter = Convert.ToInt32(Session["ChapterId"]);



            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            XLWorkbook oWB = new XLWorkbook();
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Sl. No.");
            validationTable.Columns.Add("Total Doubt");
            validationTable.Columns.Add("Pending Doubt");
            validationTable.Columns.Add("Answered Doubt");
            validationTable.Columns.Add("Rejected Doubt");
            validationTable.Columns.Add("Closed Doubt");
            validationTable.TableName = "Dought_Details";

            int i = 0;




            List<View_DC_All_Tickets_Details> tickets = Session["tickets"] as List<View_DC_All_Tickets_Details>;
            ViewBag.Doubt = tickets.ToList().Count();
            ViewBag.PendingDoubt = tickets.Where(r => r.Status == "O" && r.Answer == "False").ToList().Count();
            ViewBag.AnsweredDoubt = tickets.Where(r => r.Status == "O" && r.Answer == "True").ToList().Count();
            ViewBag.RejectedDoubt = tickets.Where(r => r.Status == "R").ToList().Count();
            ViewBag.CloseddDoubt = tickets.Where(r => r.Status == "C").ToList().Count();

            DataRow dr = validationTable.NewRow();
            dr["Sl. No."] = i + 1;
            dr["Total Doubt"] = ViewBag.Doubt;
            dr["Pending Doubt"] = ViewBag.PendingDoubt;
            dr["Answered Doubt"] = ViewBag.AnsweredDoubt;
            dr["Rejected Doubt"] = ViewBag.RejectedDoubt;
            dr["Closed Doubt"] = ViewBag.CloseddDoubt;

            validationTable.Rows.Add(dr);
            i += 1;
            oWB.Worksheets.Add(validationTable);
            //oWB.SaveAs("User_Details.xlsx");
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Doubt_Details.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            TempData["SuccessMessage"] = "Successfully Downloaded Excel File";
            return RedirectToAction("TeacherTicket", "TeacherAccess");

        }

        public ActionResult ExcelforTicketList()
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();
            List<View_DC_All_Tickets_Details> tickets_Details = Session["Tickets"] as List<View_DC_All_Tickets_Details>;

            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Sl. No.");
            validationTable.Columns.Add("Ticket No");
            validationTable.Columns.Add("Student Name");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Subject");
            validationTable.Columns.Add("Status");
            validationTable.TableName = "DoubtList_Details";
            int i = 0;

            foreach (var gs in tickets_Details.ToList())
            {
                DataRow dr = validationTable.NewRow();
                dr["Sl. No."] = i + 1;
                // var strTicket_No = gs.Ticket_No.Replace('#', '_');                
                dr["Ticket No"] = gs.Ticket_No;
                dr["Student Name"] = gs.Customer_Name;
                dr["Class"] = gs.Class_Name;
                dr["Subject"] = gs.Subject;

                if (gs.Status == "O" && gs.Answer == "True")
                {
                    dr["Status"] = "Answered";
                }
                else if (gs.Status == "O" && gs.Answer == "False")
                {
                    dr["Status"] = "Pending";
                }
                else if (gs.Status == "C")
                {
                    dr["Status"] = "Close";
                }
                else if (gs.Status == "R")
                {
                    dr["Status"] = "Reject";
                }


                validationTable.Rows.Add(dr);
                i += 1;
            }
            oWB.Worksheets.Add(validationTable);
            //oWB.SaveAs("User_Details.xlsx");
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=DoubtList_Details.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }

            return RedirectToAction("TeacherTicket", "TeacherAccess");
        }


        #endregion


        #region--------------------Gravience--------------------

        public ActionResult GetGrievance()
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {

                var teacherid = Convert.ToInt32(Session["TeacherId"]);



                var grievanceList = DbContext.Grievances.Where(a => a.Active == true).ToList();
                var grievanceAttachmentList = DbContext.GrievanceAttachments.Where(a => a.Active == true).ToList();
                //   var moduleList = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var SubjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                var classList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var SectionList = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();


                var res = (from a in grievanceList
                               //  join c in moduleList on a.Module equals c.Module_ID
                           select new GrievanceTeacherModel
                           {
                               ID = a.ID,
                               ModuleId = a.Module,
                               //  ModuleName = c.Module_Name,
                               Title = a.Title,
                               Description = a.Description,
                               FileName = grievanceAttachmentList.Where(s => s.GrivanceId == a.ID).Select(s => s.ID).FirstOrDefault() != 0 ?
                                             grievanceAttachmentList.Where(s => s.GrivanceId == a.ID).FirstOrDefault().FileName : "NA",
                               StudentId = a.StudentId,
                               TeacherId = a.TeacherId,
                               InsertedId = a.InsertedId,
                               SectionName = SectionList.Where(x => x.SectionId == a.Section).Select(x => x.SectionName).FirstOrDefault(),
                               ClassName = classList.Where(x => x.Class_Id == a.Class).Select(x => x.Class_Name).FirstOrDefault(),
                               SubjectnName = SubjectList.Where(x => x.Subject_Id == a.Subject).Select(x => x.Subject).FirstOrDefault(),

                           }).Where(a => a.TeacherId == 1).ToList();

                return View(res);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }


        public class GrievanceTeacherModel
        {
            public int ID { get; set; }
            public int? TeacherId { get; set; }
            public int? StudentId { get; set; }
            public string ModuleId { get; set; }
            //public string ModuleName { get; set; }
            public string ClassName { get; set; }
            public int ClassId { get; set; }
            public int SectionId { get; set; }
            public string SectionName { get; set; }

            public int SubjectId { get; set; }
            public string SubjectnName { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string Text { get; set; }
            public int ResolvedId { get; set; }
            public Nullable<DateTime> ResolvedDateTime { get; set; }
            public string InsertedId { get; set; }
            public Nullable<DateTime> InsertedDateTime { get; set; }
            public Nullable<bool> Active { get; set; }

            public HttpPostedFileBase[] Griv_img { get; set; }
            public string FileName { get; set; }
        }
        [HttpGet]
        public ActionResult CreateGrievance()
        {
            // ViewBag.ModuleList = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true);
            return View();
        }

        [HttpPost]
        public ActionResult SaveGrievance(GrievanceModel gvModel)
        {
            var teacherid = Convert.ToInt32(Session["TeacherId"]);

            Grievance grievanceModel = new Grievance();
            grievanceModel.Module = gvModel.ModuleId;
            grievanceModel.Title = gvModel.Title;
            grievanceModel.Description = gvModel.Description;
            grievanceModel.ResolvedDateTime = null;
            grievanceModel.InsertedDateTime = today;
            grievanceModel.Active = true;
            grievanceModel.InsertedId = teacherid.ToString();
            grievanceModel.TeacherId = Convert.ToInt32(1);
            grievanceModel.StudentId = Convert.ToInt32(0);
            grievanceModel.ResolvedId = "0";
            grievanceModel.Text = " ";
            DbContext.Grievances.Add(grievanceModel);
            DbContext.SaveChanges();

            int id = grievanceModel.ID;

            if (gvModel.Griv_img[0] != null)
            {

                GrievanceAttachment attachment = new GrievanceAttachment();

                for (int i = 0; i < gvModel.Griv_img.Length; i++)
                {
                    if (gvModel.Griv_img[i] != null)
                    {
                        string guid = Guid.NewGuid().ToString();
                        var fileName = Path.GetFileName(gvModel.Griv_img[i].FileName.Replace(gvModel.Griv_img[i].FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                        var path = Path.Combine(Server.MapPath("~/Content/Grievance/"), fileName);
                        gvModel.Griv_img[i].SaveAs(path);
                        attachment.FileName = fileName.ToString();
                    }
                }
                attachment.InsertedId = teacherid.ToString();
                attachment.GrivanceId = id;
                attachment.InsertedDateTime = DateTime.Now;
                attachment.Active = true;

                DbContext.GrievanceAttachments.Add(attachment);
                DbContext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Sucessfully Created Grievance";
            return View("CreateGrievance");

        }

        public ActionResult GrievanceDetails(int id)
        {
            var grievanceList = DbContext.Grievances.Where(a => a.Active == true).ToList();
            ViewBag.attachmentList = DbContext.GrievanceAttachments.Where(a => a.Active == true && a.GrivanceId == id).ToList();
            //   var moduleList = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();

            var res = (from a in grievanceList
                           //join c in moduleList on a.Module equals c.Module_ID
                       select new GrievanceModel
                       {
                           ID = a.ID,
                           ModuleId = a.Module,
                           // ModuleName = c.Module_Name,
                           Title = a.Title,
                           Description = a.Description,
                           StudentId = a.StudentId,
                           TeacherId = a.TeacherId,
                       }).Where(p => p.ID == id).FirstOrDefault();

            return View(res);
        }
        public ActionResult DeleteAttachments(int id)
        {
            var res = DbContext.GrievanceAttachments.Where(a => a.ID == id).FirstOrDefault();

            if (res != null)
            {
                GrievanceAttachment att = new GrievanceAttachment();
                att.ID = res.ID;
                att.FileName = res.FileName;
                att.GrivanceId = res.GrivanceId;
                att.InsertedId = res.InsertedId;
                att.InsertedDateTime = res.InsertedDateTime;
                att.Active = false;

                var sp = DbContext.UpdateGrievanceAttachment(att.ID, att.GrivanceId, att.FileName, att.InsertedId, att.InsertedDateTime, att.Active);

            }

            return RedirectToAction("GetGrievance");
        }

        [HttpPost]
        public ActionResult AddGrievanceAttachment(HttpPostedFileBase grivimg, int grivId)
        {
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            GrievanceAttachment attachment = new GrievanceAttachment();

            string guid = Guid.NewGuid().ToString();
            var fileName = Path.GetFileName(grivimg.FileName.Replace(grivimg.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
            var path = Path.Combine(Server.MapPath("~/Content/Grievance/"), fileName);
            grivimg.SaveAs(path);
            attachment.FileName = fileName.ToString();

            attachment.InsertedId = teacherid.ToString();
            attachment.GrivanceId = grivId;
            attachment.InsertedDateTime = DateTime.Now;
            attachment.Active = true;

            DbContext.GrievanceAttachments.Add(attachment);
            DbContext.SaveChanges();
            TempData["SuccessMessage"] = "Sucessfully Uploaded File.";
            return RedirectToAction("GetGrievance");
        }

        #endregion


        #region----------------------Student Data-------------------------
        public ActionResult StudentData(int? board, int? cls, Guid? sec, Guid? School)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                try
                {
                    ViewBag.school = null;
                    ViewBag.board = null;
                    ViewBag.cls = null;
                    ViewBag.sec = null;


                    var Schoollist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
                    var SectionList = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                    var ClassList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();

                    var BoardNm = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Board_Id == board).Select(a => a.Board_Name).FirstOrDefault();
                    var teacherid = Convert.ToInt32(Session["TeacherId"]);
                    var assignteacherSubjectlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).Select(a => a.SectionId).Distinct().ToList();

                    List<tbl_DC_Registration> data = null;
                    data = DbContext.tbl_DC_Registration.Where(r => assignteacherSubjectlist.Contains(r.SectionId.Value)).OrderByDescending(x => x.Customer_Name).ToList();
                    if (sec != null)
                    {
                        data = DbContext.tbl_DC_Registration.Where(r => assignteacherSubjectlist.Contains(r.SectionId.Value) && r.SectionId == sec).OrderByDescending(x => x.Customer_Name).ToList();
                        ViewBag.sec = sec.Value;
                    }
                    if (board != null && board != 0)
                    {

                        ViewBag.board = board.Value;
                    }
                    if (cls != null && cls != 0)
                    {
                        ViewBag.cls = cls.Value;
                    }

                    if (School != null)
                    {
                        ViewBag.school = School.Value;
                    }

                    var res = (from a in data
                               select new StudentDetailsModel
                               {
                                   BoardName = BoardNm,
                                   RegdId = a.Regd_ID,
                                   SectionId = a.SectionId,
                                   EmailId = a.Email,
                                   Mobile = a.Mobile,
                                   SchoolName = Schoollist.Where(x => x.SchoolId == a.SchoolId).Select(x => x.SchoolName).FirstOrDefault(),
                                   SectionName = SectionList.Where(x => x.SectionId == a.SectionId).Select(x => x.SectionName).FirstOrDefault(),
                                   ClassId = SectionList.Where(x => x.SectionId == a.SectionId).Select(x => x.Class_Id.Value).FirstOrDefault(),
                                   StudentName = a.Customer_Name,
                                   ClassName = ClassList.Where(z => z.Class_Id == SectionList.Where(x => x.SectionId == a.SectionId).Select(x => x.Class_Id.Value).FirstOrDefault()).Select(z => z.Class_Name).FirstOrDefault(),
                               }).ToList();



                    return View(res);



                }
                catch (Exception ex)
                {
                    TempData["ErrorMessage"] = "Something went wrong.";
                }
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }



        }

        public ActionResult StudentWorksheet(int? Regdid, int? Classid, int? subject, int? Chapter)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {

                Session["Regdid"] = Regdid;
                Session["ClassId"] = Classid;
                Session["SubjectId"] = subject;
                Session["ChapterId"] = Chapter;


                ViewBag.Class = Classid;
                ViewBag.StudentId = Regdid;


                Guid? schoolid = new Guid(Session["SchoolId"].ToString());
                List<tbl_DC_Worksheet> wrk = null;
                wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Regd_ID == Regdid).ToList();

                if (subject != null)
                {
                    wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Regd_ID == Regdid && a.Subject_Id == subject).ToList();
                    ViewBag.subject = subject;
                }
                if (Chapter != null)
                {
                    wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Regd_ID == Regdid && a.Chapter_Id == Chapter).ToList();
                    ViewBag.chapter = Chapter;
                }
                if (subject != null && Chapter != null)
                {
                    wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Regd_ID == Regdid && a.Subject_Id == subject && a.Chapter_Id == Chapter).ToList();
                }


                var wrksheet = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                // var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.SchoolId == schoolid).ToList();
                var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true).ToList();
                var res = (from a in wrk
                           join b in boardlist on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           //  join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           select new worksheetDetails
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Module_Name = "",
                               Regd_ID = a.Regd_ID,
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Student_Mobile = h.Mobile,
                               Total_Mark = a.Total_Mark,
                               Status = a.Verified == true ? "Verified" : "Upload",
                               Upload_Date = a.Inserted_Date != null ? Convert.ToDateTime(a.Inserted_Date).ToString("dd-MMM-yyyy") : "NA",
                               Verify_Date = a.Verified == true ? Convert.ToDateTime(a.Modified_Date).ToString("dd-MMM-yyyy") : "NA",
                               TotalVerified = wrk.Where(z => z.Verified == true).ToList().Count(),
                               TotalPending = wrk.Where(z => z.Verified == false).ToList().Count(),
                               TotalSubmitted = wrk.ToList().Count(),
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "NA",
                               Worksheet_Id = a.Worksheet_Id,
                               attachments = (from k in worksheetattachments
                                              where k.Worksheet_Id == a.Worksheet_Id
                                              select new tbl_DC_Worksheet_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Worksheet/" + k.Path,
                                                  Worksheet_Attachment_Id = k.Worksheet_Attachment_Id,
                                                  Worksheet_Id = k.Worksheet_Id
                                              }).ToList(),
                           }).ToList();


                return View(res);


            }
            else
            {
                return RedirectToAction("Login");
            }

        }




        public ActionResult StudentDoubt(int? Regdid, int? cls, int? subject, int? Chapter, string Status)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                try
                {


                    Session["Regdid"] = Regdid;
                    Session["ClassId"] = cls;
                    Session["SubjectId"] = subject;
                    Session["ChapterId"] = Chapter;
                    Session["Status"] = Status;
                    ViewBag.ClassId = cls;
                    ViewBag.StudentId = Regdid;
                    var teacherid = Convert.ToInt32(Session["TeacherId"]);
                    ViewBag.Status = "A";

                    IOrderedQueryable<View_DC_Tickets_and_Teacher> data = null;
                    data = DbContext.View_DC_Tickets_and_Teacher.Where(r => r.Regd_ID == Regdid).OrderByDescending(x => x.Ticket_ID);
                    ViewBag.Ticket = data;

                    if (cls != null && subject != null && subject != 0)
                    {
                        data = DbContext.View_DC_Tickets_and_Teacher.Where(r => r.Regd_ID == Regdid && r.Class_ID == cls && r.Subject_ID == subject).OrderByDescending(x => x.Ticket_ID);

                        if (data != null)
                        {
                            ViewBag.Ticket = data;
                        }
                        ViewBag.subject = subject;
                    }

                    if (cls != null && subject != null && Chapter != null && Status == "A")
                    {

                        data = DbContext.View_DC_Tickets_and_Teacher.Where(r => r.Regd_ID == Regdid && r.Class_ID == cls && r.Subject_ID == subject && r.Chapter_Id == Chapter).OrderByDescending(x => x.Ticket_ID);

                        if (data != null)
                        {
                            ViewBag.Ticket = data;
                        }
                        ViewBag.chapter = Chapter;
                        ViewBag.subject = subject;
                        ViewBag.Status = "A";
                    }
                    else if (cls != null && subject != null && Chapter != null && Status == "U")
                    {
                        data = DbContext.View_DC_Tickets_and_Teacher.Where(r => r.Regd_ID == Regdid && r.Teach_ID == null && r.Class_ID == cls && r.Subject_ID == subject && r.Chapter_Id == Chapter).OrderByDescending(x => x.Ticket_ID);

                        if (data != null)
                        {
                            ViewBag.Ticket = data;
                        }
                        ViewBag.chapter = Chapter;
                        ViewBag.subject = subject;
                        ViewBag.Status = "U";
                    }
                    else if (cls != null && subject != null && Chapter != null && Status != "")
                    {
                        data = DbContext.View_DC_Tickets_and_Teacher.Where(r => r.Regd_ID == Regdid && r.Status == Status && r.Class_ID == cls && r.Subject_ID == subject && r.Chapter_Id == Chapter).OrderByDescending(x => x.Ticket_ID);
                        if (data != null)
                        {
                            ViewBag.Ticket = data;
                        }
                        ViewBag.chapter = Chapter;
                        ViewBag.subject = subject;
                        ViewBag.Status = Status;
                    }

                    ViewBag.teachernames_tickets = DbContext.View_DC_CourseAssign.ToList();
                }
                catch (Exception ex)
                {
                    TempData["ErrorMessage"] = "Something went wrong.";
                }
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }

        }
        public ActionResult ViewStudentDoughtDetail(int? id, int? Regdid)
        {
            try
            {
                ViewBag.StudentId = Regdid;
                ViewBag.Breadcrumb = "Ticket";
                if (id != null)
                {
                    var get = DbContext.View_DC_All_Tickets_Details.Where(x => x.Ticket_ID == id).ToList();
                    if (get.Count > 0)
                    {
                        ViewBag.check_answer = DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        ViewBag.teacher = get.FirstOrDefault().Teach_ID;
                        ViewBag.status = get.FirstOrDefault().Status;
                        ViewBag.Student = get.FirstOrDefault().Regd_ID;
                        if (get.FirstOrDefault().Teach_ID != null)
                        {
                            int tr = Convert.ToInt32(get.FirstOrDefault().Regd_ID);
                            var tdata = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == tr).FirstOrDefault();
                            if (tdata != null)
                            {
                                if (tdata.Customer_Name != null)
                                {
                                    ViewBag.teacher = tdata.Customer_Name;
                                }
                                else
                                {
                                    ViewBag.teacher = null;
                                }

                            }
                            else
                            {
                                ViewBag.teacher = null;
                            }
                        }
                        ViewBag.viewticket = get.ToList();
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Invalid ticket details.";
                        return RedirectToAction("StudentDoubt");
                    }


                }

            }
            catch (Exception ex)
            {

                TempData["ErrorMessage"] = "Something went wrong.";
                return RedirectToAction("StudentDoubt");
            }
            return View();
        }

        public class StudentDetailsModel
        {
            public int RegdId { get; set; }
            public string EmailId { get; set; }
            public string StudentName { get; set; }
            public string Mobile { get; set; }
            public string ClassName { get; set; }
            public int ClassId { get; set; }
            public string SectionName { get; set; }
            public Guid? SectionId { get; set; }
            public string BoardName { get; set; }
            public string SchoolName { get; set; }

        }




        public ActionResult ExcelforStudentDoubtList()
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();
            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Sl. No.");
            validationTable.Columns.Add("Student Name");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Subject");
            validationTable.Columns.Add("Chapter");
            validationTable.Columns.Add("Created Date");
            validationTable.Columns.Add("Status");
            validationTable.TableName = "StudentDoubtList_Details";
            int i = 0;

            var Regdid = Convert.ToInt32(Session["Regdid"]);
            var cls = Convert.ToInt32(Session["ClassId"]);
            var subject = Convert.ToInt32(Session["SubjectId"]);
            var Chapter = Convert.ToInt32(Session["ChapterId"]);
            var Status = Convert.ToString(Session["Status"]);


            var teacherid = Convert.ToInt32(Session["TeacherId"]);

            IOrderedQueryable<View_DC_Tickets_and_Teacher> data = null;
            data = DbContext.View_DC_Tickets_and_Teacher.Where(r => r.Regd_ID == Regdid).OrderByDescending(x => x.Ticket_ID);

            if (cls != 0 && subject != 0 && Chapter != 0 && Status == "A")
            {
                data = DbContext.View_DC_Tickets_and_Teacher.Where(r => r.Regd_ID == Regdid && r.Class_ID == cls && r.Subject_ID == subject && r.Chapter_Id == Chapter).OrderByDescending(x => x.Ticket_ID);

            }
            else if (cls != 0 && subject != 0 && Chapter != 0 && Status == "U")
            {
                data = DbContext.View_DC_Tickets_and_Teacher.Where(r => r.Regd_ID == Regdid && r.Teach_ID == null && r.Class_ID == cls && r.Subject_ID == subject && r.Chapter_Id == Chapter).OrderByDescending(x => x.Ticket_ID);

            }
            else if (cls != 0 && subject != 0 && Chapter != 0 && Status != "")
            {
                data = DbContext.View_DC_Tickets_and_Teacher.Where(r => r.Regd_ID == Regdid && r.Status == Status && r.Class_ID == cls && r.Subject_ID == subject && r.Chapter_Id == Chapter).OrderByDescending(x => x.Ticket_ID);

            }
            foreach (var gs in data.ToList())
            {
                DataRow dr = validationTable.NewRow();
                dr["Sl. No."] = i + 1;
                dr["Student Name"] = gs.Customer_Name;
                dr["Class"] = gs.Class_Name;
                dr["Subject"] = gs.Subject;
                dr["Chapter"] = gs.Chapter;
                dr["Created Date"] = gs.Inserted_Date.Value.ToString("dd-MMM-yyyy");

                if (gs.Status == "O")
                {
                    dr["Status"] = "Open";
                }
                else if (gs.Status == "C")
                {
                    dr["Status"] = "Close";
                }
                else if (gs.Status == "R")
                {
                    dr["Status"] = "Reject";
                }
                else
                {
                    dr["Status"] = "Overdue";
                }

                validationTable.Rows.Add(dr);
                i += 1;
            }
            oWB.Worksheets.Add(validationTable);
            //oWB.SaveAs("User_Details.xlsx");
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=StudentDoubtList_Details.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            TempData["SuccessMessage"] = "Successfully Downloaded Excel File";
            return RedirectToAction("StudentData");
        }

        public ActionResult ExcelforStudentWorkSheetList()
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();
            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Sl. No.");
            validationTable.Columns.Add("Board");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Section");
            validationTable.Columns.Add("Subject");
            validationTable.Columns.Add("Chapter");
            validationTable.Columns.Add("Verified Name");
            validationTable.Columns.Add("Total Mark");
            validationTable.Columns.Add("Verify Date");
            validationTable.Columns.Add("Upload Date");
            validationTable.TableName = "StudentWorksheet_Details";
            int i = 0;

            var Regdid = Convert.ToInt32(Session["Regdid"]);
            var cls = Convert.ToInt32(Session["ClassId"]);
            var subject = Convert.ToInt32(Session["SubjectId"]);
            var Chapter = Convert.ToInt32(Session["ChapterId"]);


            Guid? schoolid = new Guid(Session["SchoolId"].ToString());
            var teacherid = Convert.ToInt32(Session["TeacherId"]);

            List<tbl_DC_Worksheet> wrk = null;
            wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Regd_ID == Regdid).ToList();

            if (subject != 0)
            {
                wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Regd_ID == Regdid && a.Subject_Id == subject).ToList();

            }
            if (Chapter != 0)
            {
                wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Regd_ID == Regdid && a.Chapter_Id == Chapter).ToList();
            }
            if (subject != 0 && Chapter != 0)
            {
                wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Regd_ID == Regdid && a.Subject_Id == subject && a.Chapter_Id == Chapter).ToList();
            }


            var wrksheet = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            var classes = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
            var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
            var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
            // var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
            var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.SchoolId == schoolid).ToList();
            var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
            var worksheetattachments = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true).ToList();
            var data = (from a in wrk
                        join b in boardlist on a.Board_Id equals b.Board_Id
                        join c in classes on a.Class_Id equals c.Class_Id
                        join d in subjects on a.Subject_Id equals d.Subject_Id
                        join e in chapters on a.Chapter_Id equals e.Chapter_Id
                        //  join f in modules on a.Module_Id equals f.Module_ID
                        join g in sections on a.SectionId equals g.SectionId
                        join h in students on a.Regd_ID equals h.Regd_ID
                        select new worksheetDetails
                        {

                            Board_Name = b.Board_Name,
                            Chapter_Name = e.Chapter,
                            Class_Name = c.Class_Name,
                            Regd_ID = a.Regd_ID,
                            Section_Name = g.SectionName,
                            Student_Name = h.Customer_Name,
                            Subject_Name = d.Subject,
                            Student_Mobile = h.Mobile,
                            Total_Mark = a.Total_Mark,
                            Status = a.Verified == true ? "Verified" : "Upload",
                            Upload_Date = a.Inserted_Date != null ? Convert.ToDateTime(a.Inserted_Date).ToString("dd-MMM-yyyy") : "NA",
                            Verify_Date = a.Verified == true ? Convert.ToDateTime(a.Modified_Date).ToString("dd-MMM-yyyy") : "NA",
                            TotalVerified = wrk.Where(z => z.Verified == true).ToList().Count(),
                            TotalPending = wrk.Where(z => z.Verified == false).ToList().Count(),
                            TotalSubmitted = wrk.ToList().Count(),
                            Verified = a.Verified,
                            Verified_By = a.Verified_By,
                            Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "NA",
                            Worksheet_Id = a.Worksheet_Id,

                        }).ToList();
            foreach (var gs in data.ToList())
            {
                DataRow dr = validationTable.NewRow();
                dr["Sl. No."] = i + 1;
                dr["Board"] = gs.Board_Name;
                dr["Class"] = gs.Class_Name;
                dr["Section"] = gs.Section_Name;
                dr["Subject"] = gs.Student_Name;
                dr["Chapter"] = gs.Chapter_Name;
                dr["Verified Name"] = gs.Verified_By_Name;
                dr["Total Mark"] = gs.Total_Mark;
                dr["Verify Date"] = gs.Verify_Date;
                dr["Upload Date"] = gs.Upload_Date;
                validationTable.Rows.Add(dr);
                i += 1;
            }
            oWB.Worksheets.Add(validationTable);
            //oWB.SaveAs("User_Details.xlsx");
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=StudentWorksheetList_Details.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            TempData["SuccessMessage"] = "Successfully Downloaded Excel File";
            return RedirectToAction("StudentData");
        }



        #endregion


        #region ------------------------- Online Classes --------------------
        public class classcls
        {
            public int id { get; set; }
            public string name { get; set; }
            public string sts { get; set; }
        }
        public class sectioncls
        {
            public Guid? id { get; set; }
            public string name { get; set; }
        }



        public ActionResult CreateOnlineClass(int? id, string type, Guid? schoolid, int? boardid, int? classid, Guid? sec)
        {
            if (id == null)
            {
                ViewBag.school = null;
                ViewBag.board = null;
                ViewBag.cls = null;
                ViewBag.sec = null;
                ViewBag.subject = null;
                ViewBag.teacher = null;
                ViewBag.startdate = null;
                ViewBag.enddate = null;
                ViewBag.id = null;
                ViewBag.start = null;
                ViewBag.end = null;
                ViewBag.zoomurl = "";
                ViewBag.zoompassword = "";
            }
            else
            {
                OnlineClass onlineClass = DbContext.OnlineClasses.Where(a => a.ID == id).FirstOrDefault();
                ViewBag.school = schoolid;
                ViewBag.board = boardid;
                ViewBag.cls = classid;
                ViewBag.sec = sec;
                ViewBag.subject = onlineClass.SubjectId;
                ViewBag.teacher = onlineClass.TeacherId;
                ViewBag.startdate = onlineClass.FromDate;
                ViewBag.enddate = onlineClass.ToDate;
                ViewBag.id = id;
                ViewBag.start = onlineClass.StartTime;
                ViewBag.end = onlineClass.EndTime;
                ViewBag.zoomurl = onlineClass.Zoom_Url;
                ViewBag.zoompassword = onlineClass.Zoom_Password;
            }
            //if (type == null)
            //{
            //  //  ViewBag.classesdet = null;
            //}
            //else if (type == "N")
            //{
            //ViewBag.school = schoolid;
            //ViewBag.board = boardid;
            //ViewBag.cls = classid;
            //ViewBag.sec = sec;
            //ViewBag.startdate = startdate.Value.ToString("yyyy-MM-dd");
            //ViewBag.enddate = enddate.Value.ToString("yyyy-MM-dd");
            // ViewBag.date = date.Value.ToString("yyyy-MM-dd");
            //var onlineClsList = DbContext.OnlineClasses.Where(a => a.Active == true);
            //var schoolList = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true);
            //var boardList = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true);
            //var classList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true);
            //var sectionList = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true);
            //var teacherList = DbContext.Teachers.Where(a => a.Active == 1);
            //var subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true);

            //var res = (from a in onlineClsList
            //           join b in teacherList on a.TeacherId equals b.TeacherId
            //           join c in subjectList on a.SubjectId equals c.Subject_Id
            //           join d in sectionList on a.SectionId equals d.SectionId
            //           join e in schoolList on b.SchoolId equals e.SchoolId
            //           join f in classList on c.Class_Id equals f.Class_Id
            //           join g in boardList on f.Board_Id equals g.Board_Id
            //           select new OnlineClassModel
            //           {
            //               ID = a.ID,
            //               TeacherId = a.TeacherId,
            //               TeacherName = b.Name,
            //               SubjectId = a.SubjectId,
            //               SubjectName = c.Subject,
            //               FromDate = a.FromDate,
            //               ToDate = a.ToDate,
            //               StartTime = a.StartTime,
            //               EndTime = a.EndTime,
            //               SectionId = a.SectionId,
            //               SectionName = d.SectionName,
            //               ClassId = c.Class_Id,
            //               ClassName = f.Class_Name,
            //               BoardId = f.Board_Id,
            //               BoardName = g.Board_Name,
            //               SchoolId = b.SchoolId,
            //               SchoolName = e.SchoolName
            //           }).ToList();

            //if (schoolid != null && schoolid != Guid.Empty)
            //{
            //    res = res.Where(a => a.SchoolId == schoolid.Value).ToList();
            //}

            //if (boardid != null && boardid != 0)
            //{
            //    res = res.Where(a => a.BoardId == boardid.Value).ToList();
            //}
            //if (classid != null && classid != 0)
            //{
            //    res = res.Where(a => a.ClassId == classid.Value).ToList();
            //}
            //if (sec != null && sec != Guid.Empty)
            //{
            //    res = res.Where(a => a.SectionId == sec.Value).ToList();
            //}
            //if (date != null)
            //{
            //    res = res.Where(a => a.Date.Value.Date == date.Value.Date).ToList();
            //}
            //ViewBag.classesdet = res;
            //}
            return View();
        }


        [HttpPost]
        public ActionResult SaveOnlineClass(OnlineClassModel classModel, string Submit)
        {
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            if (classModel.ID == 0)
            {
                OnlineClass onlineClass = new OnlineClass();
                onlineClass.TeacherId = teacherid;
                onlineClass.FromDate = classModel.FromDate;
                onlineClass.ToDate = classModel.ToDate;
                onlineClass.StartTime = classModel.StartTime;
                onlineClass.EndTime = classModel.EndTime;
                onlineClass.Status = "Created";
                onlineClass.InsertedId = 1;
                onlineClass.InsertedDate = DateTime.Now;
                onlineClass.ModifiedId = 1;
                onlineClass.ModifiedDate = DateTime.Now;
                onlineClass.SubjectId = classModel.SubjectId;
                onlineClass.SectionId = classModel.SectionId;
                onlineClass.Zoom_Password = classModel.Zoom_Password;
                onlineClass.Zoom_Url = classModel.Zoom_Url;
                onlineClass.Active = true;
                DbContext.OnlineClasses.Add(onlineClass);
                DbContext.SaveChanges();
            }
            else
            {
                OnlineClass onlineClass = DbContext.OnlineClasses.Where(a => a.ID == classModel.ID).FirstOrDefault();
                onlineClass.TeacherId = teacherid;
                onlineClass.FromDate = classModel.FromDate;
                onlineClass.ToDate = classModel.ToDate;
                onlineClass.StartTime = classModel.StartTime;
                onlineClass.EndTime = classModel.EndTime;
                onlineClass.Status = "Updated";
                onlineClass.ModifiedId = 1;
                onlineClass.ModifiedDate = DateTime.Now;
                onlineClass.SubjectId = classModel.SubjectId;
                onlineClass.SectionId = classModel.SectionId;
                onlineClass.Zoom_Password = classModel.Zoom_Password;
                onlineClass.Zoom_Url = classModel.Zoom_Url;
                DbContext.SaveChanges();
            }
            if (Submit == "Assign")
            {
                return RedirectToAction("OnlineClasses");
            }
            else
            {
                TempData["SuccessMessage"] = "successfully Created Online Classes..";
                return RedirectToAction("CreateOnlineClass", new { type = "N", schoolid = classModel.SchoolId, boardid = classModel.BoardId, classid = classModel.ClassId, sec = classModel.SectionId });
            }
        }

        public ActionResult OnlineClasses(Guid? schoolid, int? boardid, int? classid, Guid? sec, DateTime? date, int? Subjectid)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {

                var teacherid = Convert.ToInt32(Session["TeacherId"]);

                var onlineClsList = DbContext.OnlineClasses.Where(a => a.Active == true).ToList();
                var schoolList = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
                var boardList = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var sectionList = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var teacherList = DbContext.Teachers.Where(a => a.Active == 1).ToList();
                var subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();

                var res = (from a in onlineClsList
                           join b in teacherList on a.TeacherId equals b.TeacherId
                           join c in subjectList on a.SubjectId equals c.Subject_Id
                           join d in sectionList on a.SectionId equals d.SectionId
                           join e in schoolList on b.SchoolId equals e.SchoolId
                           join f in classList on c.Class_Id equals f.Class_Id
                           join g in boardList on f.Board_Id equals g.Board_Id
                           select new OnlineClassModel
                           {
                               ID = a.ID,
                               TeacherId = (int)a.TeacherId,
                               TeacherName = b.Name,
                               SubjectId = a.SubjectId,
                               SubjectName = c.Subject,
                               FromDate = a.FromDate,
                               ToDate = a.ToDate,
                               StartTime = a.StartTime,
                               EndTime = a.EndTime,
                               SectionId = a.SectionId,
                               SectionName = d.SectionName,
                               ClassId = c.Class_Id,
                               ClassName = f.Class_Name,
                               BoardId = f.Board_Id,
                               BoardName = g.Board_Name,
                               SchoolId = b.SchoolId,
                               SchoolName = e.SchoolName
                           }).ToList();
                ViewBag.school = null;
                ViewBag.board = null;
                ViewBag.cls = null;
                ViewBag.sec = null;
                ViewBag.subject = null;
                ViewBag.date = null;
                if (schoolid != null && schoolid != Guid.Empty)
                {
                    res = res.Where(a => a.SchoolId == schoolid.Value).ToList();
                    ViewBag.school = schoolid;
                }

                if (boardid != null && boardid != 0)
                {
                    res = res.Where(a => a.BoardId == boardid.Value).ToList();
                    ViewBag.board = boardid;
                }
                if (classid != null && classid != 0)
                {
                    res = res.Where(a => a.ClassId == classid.Value).ToList();
                    ViewBag.cls = classid;
                }
                if (sec != null && sec != Guid.Empty)
                {
                    res = res.Where(a => a.SectionId == sec.Value).ToList();
                    ViewBag.sec = sec;
                }
                if (date != null)
                {
                    res = res.Where(a => a.FromDate.Value.Date <= date.Value.Date && a.ToDate.Value.Date >= date.Value.Date).ToList();
                    ViewBag.date = date;
                }
                if (Subjectid != null && Subjectid != 0)
                {
                    res = res.Where(a => a.SubjectId == Subjectid).ToList();
                    ViewBag.subject = Subjectid;
                }

                return View(res);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }



        public ActionResult RemoveOnlineClass(int? id, string type, Guid? schoolid, int? boardid, int? classid, Guid? sec)
        {
            OnlineClass onlineClass = DbContext.OnlineClasses.Where(a => a.ID == id).FirstOrDefault();
            onlineClass.Active = false;
            onlineClass.ModifiedDate = DateTime.Now;
            DbContext.SaveChanges();
            if (type == null)
            {
                return RedirectToAction("OnlineClasses");
            }
            else if (type == "N")
            {
                return RedirectToAction("CreateOnlineClass", new { type = "N", schoolid = schoolid, boardid = boardid, classid = classid, sec = sec });
            }
            return RedirectToAction("OnlineClasses");
        }


        public JsonResult GetBoard()
        {
            try
            {
                var teacherId = Convert.ToInt32(Session["TeacherId"]);
                var assignsubjectlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherId).Select(a => new { a.Board_Id }).ToList().Distinct();
                var getboards = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var res = (from a in assignsubjectlist
                           join b in getboards on a.Board_Id equals b.Board_Id
                           select new classcls
                           {
                               id = a.Board_Id,
                               name = b.Board_Name
                           }).ToList().Distinct();
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetClasses(int id)
        {
            try
            {
                var teacherId = Convert.ToInt32(Session["TeacherId"]);
                var assignsubjectlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherId).Select(a => new { a.Class_Id }).ToList().Distinct();
                var getclasses = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Board_Id == id).ToList();
                var res = (from a in assignsubjectlist
                           join b in getclasses on a.Class_Id equals b.Class_Id
                           select new classcls
                           {
                               id = a.Class_Id,
                               name = b.Class_Name
                           }).ToList().Distinct();
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetSectionwithsubjects(int id)
        {
            try
            {
                var teacherId = Convert.ToInt32(Session["TeacherId"]);
                var assignsubjectlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherId).ToList();
                var getsections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true && a.Class_Id == id).ToList();
                var sectionlist = (from a in assignsubjectlist.Select(m => new { m.SectionId }).ToList().Distinct()
                                   join b in getsections on a.SectionId equals b.SectionId
                                   select new sectioncls
                                   {
                                       id = a.SectionId,
                                       name = b.SectionName
                                   }).ToList().Distinct();
                var getsubjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Class_Id == id).ToList();
                var subjectslist = (from a in assignsubjectlist.Select(m => new { m.Subject_Id }).ToList().Distinct()
                                    join b in getsubjects on a.Subject_Id equals b.Subject_Id
                                    select new classcls
                                    {
                                        id = a.Subject_Id,
                                        name = b.Subject
                                    }).ToList().Distinct();
                return Json(new { sectionlist = sectionlist, subjectslist = subjectslist }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ViewOnlineClassDetails(long? id)
        {
            var onlineClsList = DbContext.OnlineClasses.Where(a => a.Active == true).ToList();
            var schoolList = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var boardList = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            var classList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
            var sectionList = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var teacherList = DbContext.Teachers.Where(a => a.Active == 1).ToList();
            var subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
            adminviewonlineclasses adminviewonlineclasses = new adminviewonlineclasses();
            var onlineClasses = (from a in onlineClsList
                                 join b in teacherList on a.TeacherId equals b.TeacherId
                                 join c in subjectList on a.SubjectId equals c.Subject_Id
                                 join d in sectionList on a.SectionId equals d.SectionId
                                 join e in schoolList on b.SchoolId equals e.SchoolId
                                 join f in classList on c.Class_Id equals f.Class_Id
                                 join g in boardList on f.Board_Id equals g.Board_Id
                                 where a.ID == id
                                 select new OnlineClassModel
                                 {
                                     ID = a.ID,
                                     TeacherId = Convert.ToInt32(a.TeacherId),
                                     TeacherName = b.Name,
                                     SubjectId = a.SubjectId,
                                     SubjectName = c.Subject,
                                     FromDate = a.FromDate,
                                     ToDate = a.ToDate,
                                     StartTime = a.StartTime,
                                     EndTime = a.EndTime,
                                     SectionId = a.SectionId,
                                     SectionName = d.SectionName,
                                     ClassId = c.Class_Id,
                                     ClassName = f.Class_Name,
                                     BoardId = f.Board_Id,
                                     BoardName = g.Board_Name,
                                     SchoolId = b.SchoolId,
                                     SchoolName = e.SchoolName,
                                     Zoom_Password = a.Zoom_Password,
                                     Zoom_Url = a.Zoom_Url
                                 }).FirstOrDefault();
            var onlineClassSyllabus = DbContext.OnlineClassSyllabus.Where(a => a.Active == true && a.OnlineClassId == id).ToList();
            var chapterList = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();

            var syllabus = (from a in onlineClassSyllabus
                            join b in chapterList on a.ChapterId equals b.Chapter_Id
                            select new OnlineClassSyllabusModel
                            {
                                ID = a.ID,
                                OnlineClassId = a.OnlineClassId.Value,
                                SyllabusTitle = a.SyllabusTitle,
                                SyllabusDescription = a.SyllabusDescription,
                                ChapterId = a.ChapterId.Value,
                                ChapterName = b.Chapter,
                                status = a.Status,
                                modifieddate = a.ModifiedDate
                            }).ToList();
            var studentattendance = DbContext.OnlineClassAttendences.ToList().Where(a => a.Active == true && a.OnlineClassId == id).ToList();
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
            var studentfeedback = DbContext.OnlineClassFeedbacks.ToList().Count() > 0 ? null : DbContext.OnlineClassFeedbacks.ToList().Where(a => a.Active == true).ToList();
            if (studentattendance.ToList().Count > 0)
            {
                var attendance = (from a in studentattendance
                                  join b in students on a.StudentId equals b.Regd_ID
                                  select new onlinestudentattendance
                                  {
                                      ID = a.ID,
                                      OnlineClassId = a.OnlineClassId.Value,
                                      StudentId = a.StudentId.Value,
                                      Student_Name = b.Customer_Name,
                                      Rating = studentfeedback != null ? (studentfeedback.Where(m => m.OnlineClassId == a.OnlineClassId && m.StudentId == a.StudentId).FirstOrDefault().Rating.Value) : 0,
                                      Review = studentfeedback != null ? (studentfeedback.Where(m => m.OnlineClassId == a.OnlineClassId && m.StudentId == a.StudentId).FirstOrDefault().Review) : "",
                                      IsPresent = a.IsPresent.Value,
                                      modifieddate = a.ModifiedDate
                                  }).ToList();
                adminviewonlineclasses.onlinestudentattendance = attendance;
            }
            else
            {
                adminviewonlineclasses.onlinestudentattendance = null;
            }
            var studentHomeWork = DbContext.OnlineClassStudentHomeWorks.Where(a => a.Active == true);
            var homeWorkList = DbContext.OnlineClassHomeWorks.Where(a => a.Active == true && a.OnlineClassId == id);
            var registrationList = students.Where(a => a.Is_Active == true && a.SectionId == onlineClasses.SectionId).Count();

            var res2 = (from a in homeWorkList
                        select new OnlineHomeWorkModel
                        {
                            ID = a.ID,
                            HomeWorkPdf = "/Images/Homeworkquespdf/" + a.HomeWorkPdf,
                            InsertedDate = a.InsertedDate.Value,
                            TotalStudentCount = registrationList,
                            UploadedStudentCount = studentHomeWork.Where(m => m.OnlineClassHomeWorkId == a.ID).ToList().Count(),
                            NotUploaded = (registrationList - (studentHomeWork.Where(m => m.OnlineClassHomeWorkId == a.ID).ToList().Count()))
                        }).ToList();

            var onlinevideos = DbContext.OnlineclassVideoes.Where(a => a.IsActive == true).ToList();
            var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
            var videolinks = DbContext.OnlineclassvideoLinks.Where(a => a.IsActive == true).ToList();
            var videodet = (from a in onlinevideos
                            join b in chapters on a.ChapterId equals b.Chapter_Id
                            where a.OnlineClassId == id
                            select new onlineclassvideos
                            {
                                ChapterId = a.ChapterId,
                                ChapterName = b.Chapter,
                                OnlineClassId = a.OnlineClassId,
                                OnlineClassVideoId = a.OnlineClassVideoId,
                                Inserted_On = a.Inserted_On,
                                Topic_Description = a.Topic_Description,
                                Topic_Name = a.Topic_Name,
                                Video_Link = a.Video_Link,
                                Password = a.Password
                                // VideoLink = videolinks.Where(m => m.OnlineClassVideoId == a.OnlineClassVideoId).ToList()
                            }).ToList();


            adminviewonlineclasses.onlineclassvideos = videodet;
            adminviewonlineclasses.classSyllabusModel = syllabus;
            adminviewonlineclasses.onlineClass = onlineClasses;
            adminviewonlineclasses.HomeWorks = res2;
            return View(adminviewonlineclasses);

        }
        //public ActionResult CreateSyllabus(long? id, int onlineclassid)
        //{
        //    var online = DbContext.OnlineClasses.Where(a => a.ID == onlineclassid).FirstOrDefault();
        //    ViewBag.Chapter = DbContext.tbl_DC_Chapter.Where(b => b.Subject_Id == online.SubjectId && b.Is_Active == true).ToList();
        //    ViewBag.OnlineClassId = onlineclassid;
        //    ViewBag.id = id;
        //    if (id != null)
        //    {
        //        ViewBag.syllabus = DbContext.OnlineClassSyllabus.Where(a => a.ID == id).FirstOrDefault();
        //    }
        //    else
        //    {
        //        ViewBag.syllabus = null;
        //    }
        //    return View();
        //}
        //public ActionResult RemoveSyllabus(int id)
        //{
        //    OnlineClassSyllabu syllabus = DbContext.OnlineClassSyllabus.Where(a => a.ID == id).FirstOrDefault();
        //    syllabus.Active = false;
        //    syllabus.ModifiedDate = DateTime.Now;
        //    DbContext.SaveChanges();
        //    return RedirectToAction("ViewOnlineClassDetails", new { id = syllabus.OnlineClassId });
        //}

        //[HttpPost]
        //public ActionResult SaveSyllabus(OnlineClassModel onlineClass, long? syllabusid)
        //{
        //    var teacherId = Convert.ToInt32(Session["TeacherId"]);
        //    // string u_code = Session["USER_CODE"].ToString();
        //    if (syllabusid == null)
        //    {
        //        OnlineClassSyllabu syllabus = new OnlineClassSyllabu();
        //        syllabus.OnlineClassId = onlineClass.ID;
        //        syllabus.SyllabusTitle = onlineClass.SyllabusTitle;
        //        syllabus.SyllabusDescription = onlineClass.SyllabusDescription;
        //        syllabus.ChapterId = onlineClass.ChapterId;
        //        syllabus.Status = "Created";
        //        syllabus.InsertedId = teacherId;
        //        syllabus.InsertedDate = DateTime.Now;
        //        syllabus.ModifiedId = teacherId;
        //        syllabus.ModifiedDate = DateTime.Now;
        //        syllabus.Active = true;
        //        DbContext.OnlineClassSyllabus.Add(syllabus);
        //        DbContext.SaveChanges();
        //        TempData["SuccessMessage"] = "successfully Created Syllabus.";
        //    }
        //    else
        //    {
        //        OnlineClassSyllabu syllabus = DbContext.OnlineClassSyllabus.Where(a => a.ID == syllabusid).FirstOrDefault();
        //        syllabus.OnlineClassId = onlineClass.ID;
        //        syllabus.SyllabusTitle = onlineClass.SyllabusTitle;
        //        syllabus.SyllabusDescription = onlineClass.SyllabusDescription;
        //        syllabus.ChapterId = onlineClass.ChapterId;
        //        syllabus.Status = "Updated";
        //        syllabus.ModifiedId = teacherId;
        //        syllabus.ModifiedDate = DateTime.Now;
        //        syllabus.Active = true;
        //        DbContext.SaveChanges();
        //        TempData["SuccessMessage"] = "successfully Updated Syllabus..";
        //    }


        //    return RedirectToAction("ViewOnlineClassDetails", new { id = onlineClass.ID });
        //}

        //[HttpPost]
        //public ActionResult UpdateStudentAttendance(long onlineid, int[] studentid, Guid schoolid, Guid secid, int classid, int boardid)
        //{
        //    List<long> studentslist = new List<long>();
        //    foreach(int a in studentid)
        //    {
        //        studentslist.Add(a);
        //    }
        //    var teacherId = Convert.ToInt32(Session["TeacherId"]);
        //    var attend = DbContext.OnlineClassAttendences.Where(a => a.Active == true).ToList();
        //    var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
        //    var studentdetails = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true).ToList();
        //    var attendance = (from a in students
        //                      join b in studentdetails on a.Regd_ID equals b.Regd_ID
        //                      where a.SchoolId == schoolid && a.SectionId == secid && b.Class_ID == classid && b.Board_ID == boardid
        //                      select new onlinestudentattendance
        //                      {
        //                          StudentId = a.Regd_ID
        //                      }).ToList();
        //    OnlineClassAttendence onlinestudent = new OnlineClassAttendence();
        //    foreach (var m in attendance)
        //    {
        //        onlinestudent = attend.Where(a => a.OnlineClassId == onlineid && a.StudentId == m.StudentId && a.Active == true).FirstOrDefault();
        //        if(onlinestudent==null)
        //        {
        //            onlinestudent = new OnlineClassAttendence();
        //            onlinestudent.Active = true;
        //            onlinestudent.InsertedDate = DateTime.Now;
        //            onlinestudent.InsertedId = teacherId;
        //            onlinestudent.ModifiedId = teacherId;
        //            onlinestudent.ModifiedDate = DateTime.Now;
        //            onlinestudent.IsPresent = studentslist.Contains(m.StudentId)==false?false:true;
        //            onlinestudent.OnlineClassId = onlineid;
        //            onlinestudent.Status = "CREATED";
        //            onlinestudent.StudentId = m.StudentId;
        //            DbContext.OnlineClassAttendences.Add(onlinestudent);
        //            DbContext.SaveChanges();
        //        }
        //        else
        //        {
        //            onlinestudent.ModifiedId = teacherId;
        //            onlinestudent.ModifiedDate = DateTime.Now;
        //            onlinestudent.IsPresent = studentslist.Contains(m.StudentId) == false ? false : true;
        //            onlinestudent.OnlineClassId = onlineid;
        //            onlinestudent.Status = "UPDATED";
        //            onlinestudent.StudentId = m.StudentId;
        //            DbContext.SaveChanges();
        //        }
        //    }
        //    return RedirectToAction("ViewOnlineClassDetails", new { id = onlineid });
        //}
        //public ActionResult HomeWorkDetails(long id, Nullable<System.Guid> SectionId)
        //{
        //    OnlineClassHomeWorkDetails onlineHwModel = new OnlineClassHomeWorkDetails();

        //    var studentHomeWork = DbContext.OnlineClassStudentHomeWorks.Where(a => a.Active == true && a.OnlineClassHomeWorkId == id).ToList();
        //    var homeWorkList = DbContext.OnlineClassHomeWorks.Where(a => a.Active == true && a.ID == id).ToList();
        //    var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
        //    var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
        //    var registrationList = students.Where(a => a.SectionId == SectionId).ToList();

        //    OnlineHomeWorkModel HomeWork = new OnlineHomeWorkModel();
        //    HomeWork.ID = homeWorkList.FirstOrDefault().ID;
        //    HomeWork.HomeWorkPdf = "/Images/Homeworkquespdf/" +homeWorkList.FirstOrDefault().HomeWorkPdf;
        //    HomeWork.InsertedDate = homeWorkList.FirstOrDefault().InsertedDate.Value;
        //    HomeWork.TotalStudentCount = registrationList.Count();
        //    HomeWork.UploadedStudentCount = studentHomeWork.ToList().Count();
        //    HomeWork.NotUploaded = (HomeWork.TotalStudentCount - HomeWork.UploadedStudentCount);



        //    var notUploaded = registrationList.Where(a => !studentHomeWork.Select(m=>m.StudentId).Contains(a.Regd_ID)).ToList();
        //    var uploadedStudent = registrationList.Where(a => studentHomeWork.Select(m => m.StudentId).Contains(a.Regd_ID)).ToList();

        //    onlineHwModel.WorkModel = HomeWork;

        //    var uploadedStudentDetails = (from a in uploadedStudent
        //                                  join b in registrationList on a.Regd_ID equals b.Regd_ID
        //                                  join c in studentHomeWork on a.Regd_ID equals c.StudentId
        //                                  select new StudentDetails
        //                                  {
        //                                      ID = c.ID,
        //                                      StudentId = a.Regd_ID,
        //                                      StudentName = b.Customer_Name,
        //                                      Mobile = b.Mobile,
        //                                      UplodedDate = c.InsertedDate,
        //                                      uploadedpdf= "/Images/HomeworkAnswerPDF/"+c.UploadedPdf,
        //                                      Verified=c.Verified,
        //                                      VerifiedTeacherId = c.VerifiedTeacherId,
        //                                      verifiedteachername = c.VerifiedTeacherId==0?"":teachers.Where(m=>m.TeacherId==c.VerifiedTeacherId).FirstOrDefault().Name
        //                                  }).ToList();

        //    onlineHwModel.UploadedDetails = uploadedStudentDetails;

        //    var notUploadedStudent = (from a in notUploaded
        //                              join b in registrationList on a.Regd_ID equals b.Regd_ID
        //                              select new StudentDetails
        //                              {
        //                                  ID = a.Regd_ID,
        //                                  StudentId = a.Regd_ID,
        //                                  StudentName = b.Customer_Name,
        //                                  Mobile = b.Mobile
        //                              }).ToList();

        //    onlineHwModel.NotUploadedDetails = notUploadedStudent;

        //    return View(onlineHwModel);
        //}
        //public ActionResult CreateOnlineClassHomeWork(long? id,long onlineclassid)
        //{

        //    ViewBag.onlineclassid = onlineclassid;
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult SaveOnlineClassHomeWork(HttpPostedFileBase HW_pdf,long onlineclassid)
        //{
        //    int u_code =Convert.ToInt32(Session["TeacherId"]); ;


        //    OnlineClassHomeWork homeWork = new OnlineClassHomeWork();
        //    if (HW_pdf != null)
        //    {
        //        string guid = Guid.NewGuid().ToString();
        //        string path = string.Empty;
        //        var fileName = Path.GetFileName(HW_pdf.FileName.
        //            Replace(HW_pdf.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
        //        path = Path.Combine(Server.MapPath("/Images/Homeworkquespdf/"), fileName);
        //        HW_pdf.SaveAs(path);
        //        homeWork.HomeWorkPdf = fileName.ToString();
        //    }

        //    homeWork.OnlineClassId = onlineclassid;
        //    homeWork.Status = "Created";
        //    homeWork.InsertedId = u_code;
        //    homeWork.InsertedDate = DateTime.Now;
        //    homeWork.ModifiedId = u_code;
        //    homeWork.ModifiedDate = DateTime.Now;
        //    homeWork.Active = true;

        //    DbContext.OnlineClassHomeWorks.Add(homeWork);
        //    DbContext.SaveChanges();

        //    return RedirectToAction("ViewOnlineClassDetails",new { id= onlineclassid });
        //}
        //public ActionResult Removeonlineclasshomework(long id)
        //{
        //    int u_code = Convert.ToInt32(Session["TeacherId"]);
        //    OnlineClassHomeWork homeWork = DbContext.OnlineClassHomeWorks.Where(a => a.ID == id).FirstOrDefault();
        //    homeWork.Active = false;
        //    homeWork.ModifiedDate = DateTime.Now;
        //    homeWork.ModifiedId = u_code;
        //    DbContext.SaveChanges();
        //    return RedirectToAction("ViewOnlineClassDetails", new { id = homeWork.OnlineClassId });
        //}
        //public JsonResult VerifyStudentHomework(long id,bool verified)
        //{
        //    var teacherId = Convert.ToInt32(Session["TeacherId"]);

        //    OnlineClassStudentHomeWork onlinestudenthomework = DbContext.OnlineClassStudentHomeWorks.Where(a => a.ID == id).FirstOrDefault();
        //    onlinestudenthomework.Verified = verified;
        //    onlinestudenthomework.VerifiedTeacherId = verified==false?0:teacherId;
        //    onlinestudenthomework.ModifiedId = teacherId;
        //    onlinestudenthomework.ModifiedDate = DateTime.Now;
        //    DbContext.SaveChanges();
        //    if (verified == true)
        //    {
        //        var teachers = DbContext.Teachers.Where(a => a.Active == 1 && a.TeacherId == teacherId).FirstOrDefault();
        //        return Json(teachers.Name, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        return Json("", JsonRequestBehavior.AllowGet);
        //    }
        //}


        public ActionResult CreateOnlineVideos(long? id, int onlineclassid)
        {
            Session["classvideos"] = null;
            var online = DbContext.OnlineClasses.Where(a => a.ID == onlineclassid).FirstOrDefault();
            ViewBag.Chapter = DbContext.tbl_DC_Chapter.Where(b => b.Subject_Id == online.SubjectId && b.Is_Active == true).ToList();
            ViewBag.OnlineClassId = onlineclassid;
            ViewBag.id = id;
            if (id != null)
            {
                var onlinevideos = DbContext.OnlineclassVideoes.Where(a => a.IsActive == true).ToList();
                var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var videolinks = DbContext.OnlineclassvideoLinks.Where(a => a.IsActive == true).ToList();
                var videodet = (from a in onlinevideos
                                join b in chapters on a.ChapterId equals b.Chapter_Id
                                where a.OnlineClassVideoId == id
                                select a).FirstOrDefault();
                ViewBag.videodet = videodet;
                //List<OnlineclassvideoLink> am = new List<OnlineclassvideoLink>();
                //if (videodet.VideoLink.Count > 0)
                //{
                //    int i = 0;
                //    foreach (var a in videodet.VideoLink)
                //    {
                //        OnlineclassvideoLink link = new OnlineclassvideoLink();
                //        link.VideoLink = a.VideoLink;
                //        link.Password = a.Password;
                //        link.ID = a.ID;
                //        link.InsertedId = i + 1;
                //        link.IsActive = true;
                //        am.Add(link);
                //    }
                //}
                //Session["classvideos"] = am;
            }
            else
            {
                ViewBag.videodet = null;
            }
            return View();
        }
        public ActionResult RemoveOnlineVideos(int id)
        {
            OnlineclassVideo syllabus = DbContext.OnlineclassVideoes.Where(a => a.OnlineClassVideoId == id).FirstOrDefault();
            syllabus.IsActive = false;
            syllabus.Modified_On = DateTime.Now;
            DbContext.SaveChanges();
            var videolinks = DbContext.OnlineclassvideoLinks.Where(a => a.OnlineClassVideoId == id).ToList();
            foreach (OnlineclassvideoLink link in videolinks)
            {
                link.IsActive = false;
                link.Modified_Date = DateTime.Now;
                DbContext.SaveChanges();
            }

            return RedirectToAction("ViewOnlineClassDetails", new { id = syllabus.OnlineClassId });
        }

        [HttpPost]
        public ActionResult SaveOnlineVideos(long? videoid, int ChapterId, string Title, string Description, DateTime date, int onlineClassId, string videourl, string password)
        {
            //var teacherId = Convert.ToInt32(Session["TeacherId"]);
            // string u_code = Session["USER_CODE"].ToString();
            if (videoid == null)
            {
                OnlineclassVideo syllabus = new OnlineclassVideo();
                syllabus.OnlineClassId = onlineClassId;
                syllabus.ChapterId = ChapterId;
                syllabus.Topic_Description = Description;
                syllabus.Topic_Name = Title;
                syllabus.Is_Taken = true;
                syllabus.Taken_Date = date;
                syllabus.IsActive = true;
                syllabus.InsertedId = 1;
                syllabus.Inserted_On = DateTime.Now;
                syllabus.ModifiedId = 1;
                syllabus.Modified_On = DateTime.Now;
                syllabus.Video_Link = videourl;
                syllabus.Password = password;
                DbContext.OnlineclassVideoes.Add(syllabus);
                DbContext.SaveChanges();
                var id = syllabus.OnlineClassVideoId;
                //List<OnlineclassvideoLink> am = new List<OnlineclassvideoLink>();
                //if (Session["classvideos"] != null)
                //{
                //    am = Session["classvideos"] as List<OnlineclassvideoLink>;
                //}
                //if (am.Count > 0)
                //{
                //    foreach (var a in am)
                //    {
                //        OnlineclassvideoLink link = new OnlineclassvideoLink();
                //        link.OnlineClassVideoId = id;
                //        link.Password = a.Password;
                //        link.VideoLink = a.VideoLink;
                //        link.IsActive = true;
                //        link.InsertedId = 1;
                //        link.Inserted_Date = DateTime.Now;
                //        link.ModififedId = 1;
                //        link.Modified_Date = DateTime.Now;
                //        DbContext.OnlineclassvideoLinks.Add(link);
                //        DbContext.SaveChanges();
                //    }
                //}
            }
            else
            {
                OnlineclassVideo syllabus = DbContext.OnlineclassVideoes.Where(a => a.OnlineClassVideoId == videoid && a.IsActive == true).FirstOrDefault();
                syllabus.ChapterId = ChapterId;
                syllabus.Topic_Description = Description;
                syllabus.Topic_Name = Title;
                syllabus.Is_Taken = true;
                syllabus.Taken_Date = date;
                syllabus.IsActive = true;
                syllabus.ModifiedId = 1;
                syllabus.Modified_On = DateTime.Now;
                DbContext.SaveChanges();
                var id = videoid;
                //List<OnlineclassvideoLink> am = new List<OnlineclassvideoLink>();
                //if (Session["classvideos"] != null)
                //{
                //    am = Session["classvideos"] as List<OnlineclassvideoLink>;
                //}
                //if (am.Count > 0)
                //{
                //    foreach (var a in am)
                //    {
                //        if (a.ID == 0)
                //        {
                //            OnlineclassvideoLink link = new OnlineclassvideoLink();
                //            link.OnlineClassVideoId = id;
                //            link.Password = a.Password;
                //            link.VideoLink = a.VideoLink;
                //            link.IsActive = true;
                //            link.InsertedId = 1;
                //            link.Inserted_Date = DateTime.Now;
                //            link.ModififedId = 1;
                //            link.Modified_Date = DateTime.Now;
                //            DbContext.OnlineclassvideoLinks.Add(link);
                //            DbContext.SaveChanges();
                //        }
                //        else
                //        {
                //            OnlineclassvideoLink link = DbContext.OnlineclassvideoLinks.Where(m => m.ID == a.ID).FirstOrDefault();
                //            link.OnlineClassVideoId = id;
                //            link.Password = a.Password;
                //            link.VideoLink = a.VideoLink;
                //            link.IsActive = a.IsActive;
                //            link.ModififedId = 1;
                //            link.Modified_Date = DateTime.Now;
                //            DbContext.SaveChanges();
                //        }
                //    }
                //}
            }

            return RedirectToAction("ViewOnlineClassDetails", new { id = onlineClassId });
        }


        public JsonResult SaveTempVideos(string videourl, string password)
        {
            List<OnlineclassvideoLink> am = new List<OnlineclassvideoLink>();
            if (Session["classvideos"] != null)
            {
                am = Session["classvideos"] as List<OnlineclassvideoLink>;
            }
            OnlineclassvideoLink link = new OnlineclassvideoLink();
            link.VideoLink = videourl;
            link.Password = password;
            link.ID = 0;
            link.InsertedId = am.Count() + 1;
            link.IsActive = true;
            am.Add(link);
            Session["classvideos"] = am;
            return Json(am.Where(a => a.IsActive == true).ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemoveTempVideos(int id)
        {
            List<OnlineclassvideoLink> am = new List<OnlineclassvideoLink>();
            if (Session["classvideos"] != null)
            {
                am = Session["classvideos"] as List<OnlineclassvideoLink>;
            }
            OnlineclassvideoLink link = am.Where(a => a.InsertedId == id).FirstOrDefault();

            link.IsActive = false;
            Session["classvideos"] = am;
            return Json(am.Where(a => a.IsActive == true).ToList(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region-------------------------zoom vedio ----------------
        public class zoomvideoscls
        {
            public long ZoomId { get; set; }
            public Nullable<int> BoardId { get; set; }
            public Nullable<System.Guid> SchoolId { get; set; }
            public Nullable<int> ClassId { get; set; }
            public Nullable<System.Guid> SectionId { get; set; }
            public Nullable<int> SubjectId { get; set; }
            public Nullable<int> ChapterId { get; set; }

            public string BoardName { get; set; }
            public string SchoolName { get; set; }
            public string ClassName { get; set; }
            public string SectionName { get; set; }
            public string SubjectName { get; set; }
            public string ChapterName { get; set; }



            public Nullable<System.DateTime> InsertedOn { get; set; }
            public Nullable<System.DateTime> ModifiedOn { get; set; }
            public List<ZoomVideoLink> videoLinks { get; set; }
        }

        public ActionResult ZoomVideoUploadList(Guid? schoolid, int? boardid, int? classid, Guid? sec, int? Subjectid, int? Chapterid)
        {
            var zoomvideos = DbContext.ZoomVideos.Where(a => a.IsActive == true).ToList();
            var zoomvideolinks = DbContext.ZoomVideoLinks.Where(a => a.IsActive == true).ToList();
            var schoollist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            var classlist = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
            var sectionlist = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var subjectlist = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
            var chapterlist = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();

            var res = (from a in zoomvideos
                       join b in schoollist on a.SchoolId equals b.SchoolId
                       join c in boardlist on a.BoardId equals c.Board_Id
                       join d in classlist on a.ClassId equals d.Class_Id
                       join e in sectionlist on a.SectionId equals e.SectionId
                       join f in subjectlist on a.SubjectId equals f.Subject_Id
                       join g in chapterlist on a.ChapterId equals g.Chapter_Id
                       select new zoomvideoscls
                       {
                           BoardId = a.BoardId,
                           BoardName = c.Board_Name,
                           ChapterId = a.ChapterId,
                           ChapterName = g.Chapter,
                           ClassId = a.ClassId,
                           ClassName = d.Class_Name,
                           InsertedOn = a.InsertedOn,
                           ModifiedOn = a.ModifiedOn,
                           SchoolId = a.SchoolId,
                           SchoolName = b.SchoolName,
                           SectionId = a.SectionId,
                           SectionName = e.SectionName,
                           SubjectId = a.SubjectId,
                           SubjectName = f.Subject,
                           videoLinks = zoomvideolinks.Where(m => m.ZoomId == a.ZoomId).ToList(),
                           ZoomId = a.ZoomId
                       }).ToList();

            if (schoolid != null && schoolid != Guid.Empty)
            {
                res = res.Where(a => a.SchoolId == schoolid).ToList();
                ViewBag.school = schoolid;
            }
            if (boardid != null && boardid != 0)
            {
                res = res.Where(a => a.BoardId == boardid).ToList();
                ViewBag.board = boardid;
            }
            if (classid != null && classid != 0)
            {
                res = res.Where(a => a.ClassId == classid).ToList();
                ViewBag.cls = classid;
            }
            if (sec != null && sec != Guid.Empty)
            {
                res = res.Where(a => a.SectionId == sec).ToList();
                ViewBag.sec = sec;
            }
            if (Subjectid != null && Subjectid != 0)
            {
                res = res.Where(a => a.SubjectId == Subjectid).ToList();
                ViewBag.subject = Subjectid;
            }
            if (Chapterid != null && Chapterid != 0)
            {
                res = res.Where(a => a.ChapterId == Chapterid).ToList();
                ViewBag.chapter = Chapterid;
            }


            return View(res);
        }
        public ActionResult RemoveZoomVideoUpload(long id)
        {
            ZoomVideo zoomvideos = DbContext.ZoomVideos.Where(a => a.ZoomId == id).FirstOrDefault();
            zoomvideos.IsActive = false;
            zoomvideos.ModifiedOn = DateTime.Now;
            DbContext.SaveChanges();

            List<ZoomVideoLink> zoomvideolinks = DbContext.ZoomVideoLinks.Where(a => a.ZoomId == id).ToList();
            foreach (var a in zoomvideolinks)
            {
                a.IsActive = false;
                a.ModifiedOn = DateTime.Now;
                DbContext.SaveChanges();
            }
            return RedirectToAction("ZoomVideoUploadList");
        }
        public ActionResult CreateZoomVideoUpload()
        {
            Session["classvideos"] = null;
            ViewBag.schoollist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            ViewBag.boardlist = boardlist;
            var boardid = boardlist.FirstOrDefault().Board_Id;
            ViewBag.classlist = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Board_Id == boardid).ToList();
            //ViewBag.sectionlist = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            string SectionName = "A,B,C,D,E,F,G,RA,RB,RC,RD,N40M,N40B,D1,D2,D3,DB,P1,P2,P3,P4,COMM-A,COMM-B,FRUIT,FLOWER,R1,R2,R3,R4";
            var sections = SectionName.Split(',');
            sections = sections.Distinct().ToArray();
            List<string> sec = new List<string>();
            foreach (var a in sections)
            {
                sec.Add(a);
            }
            ViewBag.sectionlist = sec;

            return View();
        }
        public ActionResult EditZoomVideoUpload(long id)
        {
            Session["classvideos"] = null;


            var zoomvideos = DbContext.ZoomVideos.Where(a => a.ZoomId == id).FirstOrDefault();
            var zoomvideolinks = DbContext.ZoomVideoLinks.Where(a => a.IsActive == true && a.ZoomId == id).ToList();
            ViewBag.schoollist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            ViewBag.classlist = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Board_Id == zoomvideos.BoardId).ToList();
            ViewBag.sectionlist = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true && a.School_Id == zoomvideos.SchoolId && a.Class_Id == zoomvideos.ClassId).ToList();
            ViewBag.subjectlist = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Class_Id == zoomvideos.ClassId).ToList();
            ViewBag.chapterlist = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true && a.Subject_Id == zoomvideos.SubjectId).ToList();
            Session["classvideos"] = null;
            ViewBag.zoomdet = zoomvideos;
            List<ZoomVideoLink> am = new List<ZoomVideoLink>();
            foreach (var a in zoomvideolinks)
            {
                ZoomVideoLink link = new ZoomVideoLink();
                link.TopicName = a.TopicName;
                link.VideoLink = a.VideoLink;
                link.Password = a.Password;
                link.ZoomVideoId = a.ZoomVideoId;
                if (am == null)
                {
                    link.InsertedId = 1;
                }
                else
                {
                    link.InsertedId = am.Count() + 1;
                }
                link.IsActive = true;
                am.Add(link);
            }

            Session["classvideos"] = am;

            return View();
        }
        [HttpPost]
        public ActionResult UpdateVideoUpload(int id, int board, Guid school, int cls, Guid sec, int Subject, int chapter)
        {
            if (id != 0)
            {
                // var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                //foreach (var a in school)
                //{
                //    foreach (var b in sec)
                //    {
                ZoomVideo zoom = DbContext.ZoomVideos.Where(a => a.ZoomId == id).FirstOrDefault();
                zoom.BoardId = board;
                zoom.ClassId = cls;
                zoom.ChapterId = chapter;
                zoom.IsActive = true;
                zoom.ModifiedId = 1;
                zoom.ModifiedOn = DateTime.Now;
                zoom.SchoolId = school;
                zoom.SectionId = sec;
                zoom.SubjectId = Subject;
                DbContext.SaveChanges();
                long zoomid = zoom.ZoomId;
                List<ZoomVideoLink> am = new List<ZoomVideoLink>();
                if (Session["classvideos"] != null)
                {
                    am = Session["classvideos"] as List<ZoomVideoLink>;
                }
                var am1 = am.Where(m => m.IsActive == true).ToList();
                if (am1 != null)
                {
                    foreach (var v in am1)
                    {
                        if (v.ZoomVideoId == 0)
                        {
                            ZoomVideoLink zoomVideo = new ZoomVideoLink();
                            zoomVideo.InsertedId = 1;
                            zoomVideo.ModifiedId = 1;
                            zoomVideo.InsertedOn = DateTime.Now;
                            zoomVideo.ModifiedOn = DateTime.Now;
                            zoomVideo.IsActive = true;
                            zoomVideo.Password = v.Password;
                            zoomVideo.TopicName = v.TopicName;
                            zoomVideo.VideoLink = v.VideoLink;
                            zoomVideo.ZoomId = zoomid;
                            DbContext.ZoomVideoLinks.Add(zoomVideo);
                            DbContext.SaveChanges();
                        }
                        else
                        {
                            ZoomVideoLink zoomVideo = DbContext.ZoomVideoLinks.Where(a => a.ZoomVideoId == v.ZoomVideoId).FirstOrDefault();

                            zoomVideo.ModifiedId = 1;
                            zoomVideo.ModifiedOn = DateTime.Now;
                            zoomVideo.IsActive = true;
                            zoomVideo.Password = v.Password;
                            zoomVideo.TopicName = v.TopicName;
                            zoomVideo.VideoLink = v.VideoLink;
                            DbContext.SaveChanges();
                        }
                    }
                    //    }
                    //}
                }
                var am2 = am.Where(a => a.IsActive == false && a.ZoomVideoId != 0).ToList();
                if (am2 != null)
                {
                    foreach (var v in am2)
                    {
                        ZoomVideoLink zoomVideo = DbContext.ZoomVideoLinks.Where(a => a.ZoomVideoId == v.ZoomVideoId).FirstOrDefault();

                        zoomVideo.ModifiedId = 1;
                        zoomVideo.ModifiedOn = DateTime.Now;
                        zoomVideo.IsActive = false;
                        DbContext.SaveChanges();
                    }
                }

            }
            TempData["SuccessMessage"] = "Sucessfully Updated zoom Vedio Details..";
            return RedirectToAction("ZoomVideoUploadList");
        }


        [HttpPost]
        public ActionResult SaveUpdateVideoUpload(int? id, int board, Guid[] school, int cls, string[] sec, int Subject, int chapter)
        {
            if (id == null)
            {
                var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                foreach (var a in school)
                {
                    foreach (var b in sec)
                    {
                        ZoomVideo zoom = new ZoomVideo();
                        zoom.BoardId = board;
                        zoom.ClassId = cls;
                        zoom.ChapterId = chapter;
                        zoom.InsertedId = 2;
                        zoom.InsertedOn = DateTime.Now;
                        zoom.IsActive = true;
                        zoom.ModifiedId = 2;
                        zoom.ModifiedOn = DateTime.Now;
                        zoom.SchoolId = a;
                        zoom.SectionId = sections.Where(m => m.SectionName == b && m.School_Id == a && m.Class_Id == cls).FirstOrDefault().SectionId;
                        zoom.SubjectId = Subject;
                        DbContext.ZoomVideos.Add(zoom);
                        DbContext.SaveChanges();
                        long zoomid = zoom.ZoomId;
                        List<ZoomVideoLink> am = new List<ZoomVideoLink>();
                        if (Session["classvideos"] != null)
                        {
                            am = Session["classvideos"] as List<ZoomVideoLink>;
                        }
                        foreach (var v in am.Where(m => m.IsActive == true))
                        {
                            ZoomVideoLink zoomVideo = new ZoomVideoLink();
                            zoomVideo.InsertedId = 2;
                            zoomVideo.ModifiedId = 2;
                            zoomVideo.InsertedOn = DateTime.Now;
                            zoomVideo.ModifiedOn = DateTime.Now;
                            zoomVideo.IsActive = true;
                            zoomVideo.Password = v.Password;
                            zoomVideo.TopicName = v.TopicName;
                            zoomVideo.VideoLink = v.VideoLink;
                            zoomVideo.ZoomId = zoomid;
                            DbContext.ZoomVideoLinks.Add(zoomVideo);
                            DbContext.SaveChanges();
                        }
                    }
                }

            }
            TempData["SuccessMessage"] = "Sucessfully Uploaed zoom Vedio..";
            return RedirectToAction("ZoomVideoUploadList");
        }
        public JsonResult SaveZoomVideos(string topic, string videourl, string password)
        {
            List<ZoomVideoLink> am = new List<ZoomVideoLink>();
            if (Session["classvideos"] != null)
            {
                am = Session["classvideos"] as List<ZoomVideoLink>;
            }
            ZoomVideoLink link = new ZoomVideoLink();
            link.TopicName = topic;
            link.VideoLink = videourl;
            link.Password = password;
            link.ZoomVideoId = 0;
            if (am == null)
            {
                link.InsertedId = 1;
            }
            else
            {
                link.InsertedId = am.Count() + 1;
            }
            link.IsActive = true;
            am.Add(link);
            Session["classvideos"] = am;
            return Json(am.Where(a => a.IsActive == true).ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemoveZoomVideos(int id)
        {
            List<ZoomVideoLink> am = new List<ZoomVideoLink>();
            if (Session["classvideos"] != null)
            {
                am = Session["classvideos"] as List<ZoomVideoLink>;
            }
            ZoomVideoLink link = am.Where(a => a.InsertedId == id).FirstOrDefault();

            link.IsActive = false;
            Session["classvideos"] = am;
            return Json(am.Where(a => a.IsActive == true).ToList(), JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region---------------------Online Classes-------------------

        public JsonResult GetStudentListdtl(Guid? id)
        {

            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.SectionId == id).ToList();

            var res = (from a in students
                       select new StudentDetails_Class
                       {
                           Regd_ID = a.Regd_ID,
                           Customer_Name = a.Customer_Name,
                           Mobile = a.Mobile
                       }).ToList();

            return Json(res.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExamTypeList()
        {
            var examtype = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true);

            var res = (from a in examtype
                       select new
                       {
                           OnlineExamTypeId = a.OnlineExamTypeId,
                           OnlineExamTypeName = a.OnlineExamTypeName
                       }).ToList();
            return Json(res.ToList(), JsonRequestBehavior.AllowGet);

        }

        public class AnswerSheetModel
        {

            public Nullable<long> OnlineExamAnswerSheetId { get; set; }
            public Nullable<long> OnlineExamId { get; set; }
            public string QuestionNo { get; set; }
            public Nullable<decimal> GivenMarks { get; set; }
            public Nullable<int> TeacherId { get; set; }

            public Nullable<int> StudentId { get; set; }
            public string StudentName { get; set; }
            public Nullable<System.DateTime> AppearedOn { get; set; }
            public string Type { get; set; }
            public Nullable<System.DateTime> SubmittedOn { get; set; }
            public string Status { get; set; }
            public Nullable<int> VerifiedTeacherId { get; set; }
            public string VerifiedTeacherName { get; set; }
            public Nullable<System.DateTime> VerifiedOn { get; set; }
            public string AnswerPDF { get; set; }
            public Nullable<bool> IsVerified { get; set; }
            public Nullable<bool> IsAppeared { get; set; }
            public bool IsSubmitted { get; set; }
            public string OnlineExamCode { get; set; }
            public Nullable<long> OnlineExamTypeId { get; set; }
            public string OnlineExamTypeName { get; set; }
            public Nullable<long> OnlineExamDateId { get; set; }
            public Nullable<long> OnlineExamTimeId { get; set; }
            public Nullable<System.Guid> SchoolId { get; set; }
            public string SchoolName { get; set; }
            public Nullable<int> ClassId { get; set; }
            public string ClassName { get; set; }
            public Nullable<int> SubjectId { get; set; }
            public string SubjectName { get; set; }
            public Nullable<Guid> SectionId { get; set; }
            public string SectionName { get; set; }
            public string Exam_Instruction { get; set; }
            public string PDF { get; set; }
            public Nullable<System.DateTime> InsertedOn { get; set; }
            public Nullable<System.DateTime> ModifiedOn { get; set; }

        }


        public ActionResult AnswerSheetList(int? cls, Guid? sec, int? subject, int? ExamType, string Status, int? Student)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                var teacherid = Convert.ToInt32(Session["TeacherId"]);
                ViewBag.cls = null;
                ViewBag.sec = null;
                ViewBag.subject = null;
                ViewBag.student = null;
                ViewBag.status = null;
                ViewBag.examtype = null;

                var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
                var boardlist = assigndet.Select(a => a.Board_Id).ToList();
                var classlist = assigndet.Select(a => a.Class_Id).ToList();
                var sectionlist = assigndet.Select(a => a.SectionId).ToList();
                var subjectlist = assigndet.Select(a => a.Subject_Id).ToList();
                List<VW_All_OnlineExam_Attachment> onlineexamdetails = DbContext.VW_All_OnlineExam_Attachment.Where(a => a.Is_Active == true).ToList();


                var res = (from a in onlineexamdetails
                           where boardlist.Contains(a.Board_ID.Value) && classlist.Contains(a.Class_ID.Value) && sectionlist.Contains(a.SectionId.Value) && subjectlist.Contains(a.SubjectId.Value)
                           select new AnswerSheetModel
                           {
                               OnlineExamAnswerSheetId = a.OnlineExamAnswerSheetId,
                               OnlineExamId = a.OnlineExamId,
                               GivenMarks = a.score,
                               StudentId = a.StudentId,
                               AppearedOn = a.AppearedOn,
                               StudentName = a.Customer_Name,
                               Type = a.Type,
                               SubmittedOn = a.SubmittedOn,
                               Status = a.IsVerified == true ? "V" : "N",
                               VerifiedTeacherId = a.VerifiedTeacherId,
                               VerifiedOn = a.VerifiedOn,
                               AnswerPDF = a.AnswerPDF,
                               IsVerified = a.IsVerified,
                               IsAppeared = a.IsAppeared,
                               IsSubmitted = a.IsSubmitted,
                               OnlineExamCode = a.OnlineExamCode,
                               OnlineExamTypeId = a.OnlineExamTypeId,
                               OnlineExamTypeName = a.OnlineExamTypeName,
                               SchoolId = a.SchoolId,
                               ClassId = a.Class_ID,
                               SubjectId = a.SubjectId,
                               Exam_Instruction = a.Exam_Instruction,
                               PDF = a.PDF,
                               ClassName = a.Class_Name,
                               SchoolName = a.SchoolName,
                               SubjectName = a.Subject,
                               VerifiedTeacherName = a.VerifiedTeacher,
                               SectionName = a.SectionName,
                               SectionId = a.SectionId
                           }).ToList();
                if (cls != null && cls != 0)
                {
                    res = res.Where(a => a.ClassId == cls).ToList();
                    ViewBag.cls = cls;
                }
                if (sec != null)
                {
                    res = res.Where(a => a.SectionId == sec).ToList();
                    ViewBag.sec = sec;
                }
                if (subject != null && subject != 0)
                {
                    res = res.Where(a => a.SubjectId == subject).ToList();
                    ViewBag.subject = subject;
                }
                if (ExamType != null && ExamType != 0)
                {
                    res = res.Where(a => a.OnlineExamTypeId == ExamType).ToList();
                    ViewBag.examtype = ExamType;
                }
                if (Status != null && Status != "")
                {
                    res = res.Where(a => a.Status == Status).ToList();
                    ViewBag.status = Status;
                }
                if (Student != null && Student != 0)
                {
                    res = res.Where(a => a.StudentId == Student).ToList();
                    ViewBag.student = Student;
                }
                return View(res);

            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        public ActionResult AnswersheetDetails(long id)
        {
            var answer = DbContext.VW_All_OnlineExam_Attachment.Where(a => a.OnlineExamAnswerSheetId == id).FirstOrDefault();
            var remarks = DbContext.OnlineExamAnswerSheetRemarks.Where(a => a.Is_Active == true && a.OnlineExamAnswerSheetId == id).ToList();
            ViewBag.answer = answer;
            ViewBag.remarks = remarks;
            ViewBag.Score = 0;
            if (remarks.Count > 0)
            {
                ViewBag.Score = remarks.Sum(a => a.GivenMarks);
            }
            return View();
        }
        public ActionResult EditAnswersheetDetails(long id)
        {
            var answer = DbContext.VW_All_OnlineExam_Attachment.Where(a => a.OnlineExamAnswerSheetId == id).FirstOrDefault();
            var remarks = DbContext.OnlineExamAnswerSheetRemarks.Where(a => a.Is_Active == true && a.OnlineExamAnswerSheetId == id).ToList();
            ViewBag.answer = answer;
            ViewBag.remarks = remarks;
            ViewBag.Score = 0;
            if (remarks.Count > 0)
            {
                ViewBag.Score = remarks.Sum(a => a.GivenMarks);
            }
            return View();
        }
        public class onlinemarkscls
        {
            public string quesname { get; set; }
            public string quesmarks { get; set; }
            public string quesremarks { get; set; }

        }
        public class marddetcls
        {
            public long onlineexamid { get; set; }
            public long onlinesheetid { get; set; }
            public List<onlinemarkscls> marks { get; set; }
        }
        [HttpPost]
        public JsonResult savemarks(List<onlinemarkscls> marks, long onlineexamid, long onlinesheetid)
        {
            var TeacherId = Convert.ToInt32(Session["TeacherId"]);

            if (marks.Count > 0)
            {
                foreach (var item in marks)
                {
                    if (item.quesmarks != null && item.quesname != null)
                    {
                        OnlineExamAnswerSheetRemark sheetRemark = new OnlineExamAnswerSheetRemark();
                        sheetRemark.GivenMarks = Convert.ToDecimal(item.quesmarks);
                        sheetRemark.InsertedId = TeacherId;
                        sheetRemark.InsertedOn = DateTime.Now;
                        sheetRemark.Is_Active = true;
                        sheetRemark.ModifiedId = TeacherId;
                        sheetRemark.ModifiedOn = DateTime.Now;
                        sheetRemark.OnlineExamAnswerSheetId = onlinesheetid;
                        sheetRemark.OnlineExamId = onlineexamid;
                        sheetRemark.QuestionNo = item.quesname;
                        sheetRemark.Remarks = item.quesremarks;
                        sheetRemark.TeacherId = TeacherId;
                        sheetRemark.TotalMarks = 0;
                        DbContext.OnlineExamAnswerSheetRemarks.Add(sheetRemark);
                        DbContext.SaveChanges();
                        OnlineExamAnswerSheet answerSheet = DbContext.OnlineExamAnswerSheets.Where(a => a.OnlineExamAnswerSheetId == onlinesheetid).FirstOrDefault();
                        answerSheet.IsVerified = true;
                        answerSheet.VerifiedOn = DateTime.Now;
                        answerSheet.VerifiedTeacherId = TeacherId;
                        DbContext.SaveChanges();
                    }
                }
                try
                {
                    var onlineans = DbContext.OnlineExamAnswerSheets.Where(a => a.OnlineExamAnswerSheetId == onlinesheetid).FirstOrDefault();
                    var onlineexam = DbContext.OnlineExams.Where(a => a.OnlineExamId == onlineexamid).FirstOrDefault();
                    var examtype = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == onlineexam.OnlineExamTypeId).FirstOrDefault();
                    var subject = DbContext.tbl_DC_Subject.Where(a => a.Subject_Id == onlineexam.SubjectId).FirstOrDefault();
                    var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == onlineans.StudentId)

                                   select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                    string body = "ESCORE#You have scored {{Marks}} marks in {{Subject_name}} | {{Exam_name}}";
                    string msg = body.ToString().Replace("{{Exam_name}}", examtype.OnlineExamTypeName.ToString())
                        .Replace("{{Marks}}", marks.Sum(a => Convert.ToDecimal(a.quesmarks)) + "").Replace("{{Subject_name}}", subject.Subject + "");

                    if (pushnot != null)
                    {
                        if (pushnot.Device_id != null)
                        {
                            var note = new PushNotiStatus("Exam Score", msg, pushnot.Device_id);
                        }
                    }
                }
                catch (Exception)
                {


                }
            }
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult updatemarks(List<onlinemarkscls> marks, long onlineexamid, long onlinesheetid)
        {
            var TeacherId = Convert.ToInt32(Session["TeacherId"]);

            if (marks.Count > 0)
            {
                var sheetRemark1 = DbContext.OnlineExamAnswerSheetRemarks.Where(a => a.OnlineExamAnswerSheetId == onlinesheetid).ToList();
                if (sheetRemark1.Count > 0)
                {
                    foreach (OnlineExamAnswerSheetRemark item in sheetRemark1)
                    {
                        DbContext.OnlineExamAnswerSheetRemarks.Remove(item);
                        DbContext.SaveChanges();
                    }
                }
                foreach (var item in marks)
                {
                    if (item.quesmarks != null && item.quesname != null)
                    {
                        OnlineExamAnswerSheetRemark sheetRemark = new OnlineExamAnswerSheetRemark();
                        sheetRemark.GivenMarks = Convert.ToDecimal(item.quesmarks);
                        sheetRemark.InsertedId = TeacherId;
                        sheetRemark.InsertedOn = DateTime.Now;
                        sheetRemark.Is_Active = true;
                        sheetRemark.ModifiedId = TeacherId;
                        sheetRemark.ModifiedOn = DateTime.Now;
                        sheetRemark.OnlineExamAnswerSheetId = onlinesheetid;
                        sheetRemark.OnlineExamId = onlineexamid;
                        sheetRemark.QuestionNo = item.quesname;
                        sheetRemark.Remarks = item.quesremarks;
                        sheetRemark.TeacherId = TeacherId;
                        sheetRemark.TotalMarks = 0;
                        DbContext.OnlineExamAnswerSheetRemarks.Add(sheetRemark);
                        DbContext.SaveChanges();
                        OnlineExamAnswerSheet answerSheet = DbContext.OnlineExamAnswerSheets.Where(a => a.OnlineExamAnswerSheetId == onlinesheetid).FirstOrDefault();
                        answerSheet.IsVerified = true;
                        answerSheet.VerifiedOn = DateTime.Now;
                        answerSheet.VerifiedTeacherId = TeacherId;
                        DbContext.SaveChanges();
                    }
                }
            }
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Objective Exam
        public ActionResult OnlineObjectiveExamList(Guid? schoolid, int? boardid, int? classid, int? Subjectid, int? ExamType, int? ExamDate, Nullable<TimeSpan> StartTime, Nullable<TimeSpan> EndTime)
        {

            try
            {
                var OnlineObjectiveExamList = DbContext.OnlineObjectiveExams.Where(a => a.Is_Active == true).ToList();
                var OnlineObjectiveExamQuestionList = DbContext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true).ToList();

                var OnlineExamDateList = DbContext.OnlineExamDates.Where(a => a.Is_Active == true).ToList();
                var OnlineExamTimesList = DbContext.OnlineExamTimes.Where(a => a.Is_Active == true).ToList();
                var OnlineExamTypessList = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
                //var StudentList = Dbcontext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var SubjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                var SchoolList = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
                var ClassList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var BoardList = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();



                var res = (from a in OnlineObjectiveExamList
                           join d in OnlineExamDateList on a.OnlineExamDateId equals d.OnlineExamDateId
                           join e in OnlineExamTimesList on a.OnlineExamTimeId equals e.OnlineExamTimeId
                           join f in OnlineExamTypessList on a.OnlineExamTypeId equals f.OnlineExamTypeId
                           join g in SubjectList on a.SubjectId equals g.Subject_Id
                           join h in SchoolList on a.SchoolId equals h.SchoolId
                           join j in ClassList on a.ClassId equals j.Class_Id
                           join k in BoardList on a.BoardId equals k.Board_Id


                           select new ObjectiveExamDetailsModel
                           {
                               OnlineObjectiveExamId = a.OnlineObjectiveExamId,
                               SchoolId = a.SchoolId,
                               SchoolName = h.SchoolName,
                               ClassId = a.ClassId,
                               ClassName = j.Class_Name,
                               BoardId = a.BoardId,
                               BoardName = k.Board_Name,
                               OnlineExamDate = d.OnlineExamDate1,
                               OnlineExamDateId = a.OnlineExamDateId,
                               OnlineExamTypeId = a.OnlineExamTypeId,
                               OnlineExamDateName = d.OnlineExamDateName,
                               StartTime = e.Start_Time,
                               StartTimeName = e.Start_Time_Name,
                               EndTime = e.End_Time,
                               EndTimeName = e.End_Time_Name,
                               OnlineExamTypeName = f.OnlineExamTypeName,
                               SubjectName = g.Subject,
                           }).ToList();
                if (schoolid != null)
                {
                    res = res.Where(a => a.SchoolId == schoolid).ToList();
                    ViewBag.school = schoolid;

                }
                if (boardid != null && boardid != 0)
                {
                    res = res.Where(a => a.BoardId == boardid).ToList();
                    ViewBag.board = boardid;

                }
                if (classid != null && classid != 0)
                {
                    res = res.Where(a => a.ClassId == classid).ToList();
                    ViewBag.cls = classid;
                }
                if (Subjectid != null && Subjectid != 0)
                {
                    res = res.Where(a => a.SubjectId == Subjectid).ToList();
                    ViewBag.subject = Subjectid;
                }
                if (ExamType != null && ExamType != 0)
                {
                    res = res.Where(a => a.OnlineExamTypeId == ExamType).ToList();
                    ViewBag.examtype = ExamType;


                }
                if (ExamDate != null && ExamDate != 0)
                {
                    res = res.Where(a => a.OnlineExamDateId == ExamDate).ToList();
                    ViewBag.examdate = ExamDate;

                }
                if (StartTime != null && EndTime != null)
                {
                    res = res.Where(a => a.StartTime >= StartTime && a.EndTime <= EndTime).ToList();
                    ViewBag.startime = StartTime;
                    ViewBag.endtime = EndTime;
                }
                return View(res.OrderByDescending(a => a.OnlineObjectiveExamId).ToList());
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public ActionResult RemoveObjectiveExam(long id)
        {
            OnlineObjectiveExam online = DbContext.OnlineObjectiveExams.Where(a => a.OnlineObjectiveExamId == id).FirstOrDefault();
            online.Is_Active = false;
            online.ModifiedOn = DateTime.Now;
            DbContext.SaveChanges();
            return RedirectToAction("OnlineObjectiveExamList");
        }
        public ActionResult CreateObjectiveExam()
        {
            ViewBag.boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            ViewBag.schoollist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.examtypes = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            ViewBag.examdates = DbContext.OnlineExamDates.Where(a => a.Is_Active == true).OrderByDescending(a => a.OnlineExamDateName).ToList();
            ViewBag.negativemark = DbContext.Negativemarks.Where(a => a.Is_Active == true).ToList();
            return View();
        }
        public ActionResult ViewQuestionObjectiveExam(long id)
        {
            var OnlineObjectiveExamList = DbContext.OnlineObjectiveExams.Where(a => a.Is_Active == true && a.OnlineObjectiveExamId == id).ToList();
            var OnlineObjectiveExamQuestionList = DbContext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true && a.OnlineObjectiveExamId == id).ToList();
            var OnlineObjectiveExamAswerList = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();

            var OnlineExamDateList = DbContext.OnlineExamDates.Where(a => a.Is_Active == true).OrderByDescending(a => a.OnlineExamDateName).ToList();
            var OnlineExamTimesList = DbContext.OnlineExamTimes.Where(a => a.Is_Active == true).ToList();
            var OnlineExamTypessList = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            //var StudentList = Dbcontext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var SubjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
            var SchoolList = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var ClassList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var BoardList = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var negativemarkList = DbContext.Negativemarks.Where(a => a.Is_Active == true).ToList();

            var objexam = (from a in OnlineObjectiveExamList
                           join d in OnlineExamDateList on a.OnlineExamDateId equals d.OnlineExamDateId
                           join e in OnlineExamTimesList on a.OnlineExamTimeId equals e.OnlineExamTimeId
                           join f in OnlineExamTypessList on a.OnlineExamTypeId equals f.OnlineExamTypeId
                           join g in SubjectList on a.SubjectId equals g.Subject_Id
                           join h in SchoolList on a.SchoolId equals h.SchoolId
                           join j in ClassList on a.ClassId equals j.Class_Id
                           join k in BoardList on a.BoardId equals k.Board_Id
                           select new ObjectiveExamDetailsModel
                           {
                               OnlineObjectiveExamId = a.OnlineObjectiveExamId,
                               SchoolId = a.SchoolId,
                               SchoolName = h.SchoolName,
                               ClassId = a.ClassId,
                               ClassName = j.Class_Name,
                               BoardId = a.BoardId,
                               BoardName = k.Board_Name,
                               OnlineExamDate = d.OnlineExamDate1,
                               OnlineExamDateId = a.OnlineExamDateId,
                               OnlineExamTypeId = a.OnlineExamTypeId,
                               OnlineExamDateName = d.OnlineExamDateName,
                               StartTime = e.Start_Time,
                               StartTimeName = e.Start_Time_Name,
                               EndTime = e.End_Time,
                               EndTimeName = e.End_Time_Name,
                               OnlineExamTypeName = f.OnlineExamTypeName,
                               SubjectName = g.Subject,
                               IsNegative = a.IsNagativeMarking.Value,
                               NegativeMark = a.IsNagativeMarking == true ? (negativemarkList.Where(m => m.NegativeMarkID == a.NagativeMarkingId).FirstOrDefault().NegativeMark1.ToString()) : ""
                           }).FirstOrDefault();
            var objques = (from a in OnlineObjectiveExamQuestionList
                           select new objectiveallquescls
                           {
                               examQuestion = a,
                               examAnswers = OnlineObjectiveExamAswerList.Where(m => m.OnlineObjectiveExamQuestionId == a.OnlineObjectiveExamQuestionId).ToList()
                           }).ToList();
            ViewObjectiveExamcls viewObjective = new ViewObjectiveExamcls();
            viewObjective.objectiveExam = objexam;
            viewObjective.questions = objques;

            return View(viewObjective);
        }
        public ActionResult EditObjectiveExam(long id)
        {
            ViewBag.boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            ViewBag.schoollist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.examtypes = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            ViewBag.examdates = DbContext.OnlineExamDates.Where(a => a.Is_Active == true).ToList();
            ViewBag.negativemark = DbContext.Negativemarks.Where(a => a.Is_Active == true).ToList();
            var OnlineObjectiveExamQuestionList = DbContext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true && a.OnlineObjectiveExamId == id).ToList();
            var OnlineObjectiveExamAswerList = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();

            editobjectiveexamcls editobjective = new editobjectiveexamcls();
            editobjective.exam = DbContext.OnlineObjectiveExams.Where(a => a.OnlineObjectiveExamId == id).FirstOrDefault();
            var objques = (from a in OnlineObjectiveExamQuestionList
                           select new objectiveallquescls
                           {
                               examQuestion = a,
                               examAnswers = OnlineObjectiveExamAswerList.Where(m => m.OnlineObjectiveExamQuestionId == a.OnlineObjectiveExamQuestionId).ToList()
                           }).ToList();
            editobjective.questions = objques;
            return View(editobjective);
        }
        #endregion

        #region-----------------All worksheet Report-----------------

        public JsonResult GetAllExamType()
        {
            var examtypelist = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();

            var res = (from a in examtypelist
                       select new
                       {
                           OnlineExamTypeId = a.OnlineExamTypeId,
                           OnlineExamTypeName = a.OnlineExamTypeName
                       }
                       ).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public class totalscoreclass
        {
            public int Regd_ID { get; set; }
            public string Customer_Name { get; set; }
            public Guid SectionId { get; set; }
            public Guid SchoolId { get; set; }
            public int Board_ID { get; set; }
            public int Class_ID { get; set; }
            public string Board_Name { get; set; }
            public string SchoolName { get; set; }
            public string Class_Name { get; set; }
            public string SectionName { get; set; }
            public int Subject_Id { get; set; }
            public string Subject { get; set; }
            public decimal? subjectivescore { get; set; }
            public decimal? objectivescore { get; set; }
            public double worksheetscore { get; set; }
        }

        public ActionResult AllWorksheetResultList(Guid? School, int? board, int? cls, Guid? sec, int? subject, int? examtype)
        {
            var username = Convert.ToString(Session["Mobile"]);
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            Guid? schoolid = new Guid(Session["SchoolId"].ToString());
            if (username != null && username != "")
            {

                var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
                var boardlistss = assigndet.Select(a => a.Board_Id).ToList();
                var classlist = assigndet.Select(a => a.Class_Id).ToList();
                var sectionlist = assigndet.Select(a => a.SectionId).ToList();
                var subjectlist = assigndet.Select(a => a.Subject_Id).ToList();


                var regs = DbContext.VW_Student_With_Subject.Where(a => boardlistss.Contains(a.Board_ID.Value) && classlist.Contains(a.Class_ID.Value) && sectionlist.Contains(a.SectionId) && subjectlist.Contains(a.Subject_Id) && a.SchoolId == schoolid.Value).ToList();
                var onlineexams = DbContext.OnlineExams.Where(a => a.Is_Active == true).ToList();
                var onlineexamans = DbContext.OnlineExamAnswerSheets.Where(a => a.Is_Active == true).ToList();
                var onlineobjective = DbContext.OnlineObjectiveExams.Where(a => a.Is_Active == true).ToList();
                var OnlineObjectiveStudents = DbContext.OnlineObjectiveExamStudents.Where(a => a.Is_Active == true).ToList();
                var worksheets = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true).ToList();
                var modules = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var res = (from a in regs
                           select new totalscoreclass
                           {
                               Board_ID = a.Board_ID.Value,
                               Board_Name = a.Board_Name,
                               Class_ID = a.Class_ID.Value,
                               Class_Name = a.Class_Name,
                               Customer_Name = a.Customer_Name,
                               Regd_ID = a.Regd_ID,
                               SchoolId = a.SchoolId.Value,
                               SchoolName = a.SchoolName,
                               SectionId = a.SectionId.Value,
                               SectionName = a.SectionName,
                               Subject = a.Subject,
                               Subject_Id = a.Subject_Id,
                               subjectivescore = GetOnlineSubjectiveExamscore(onlineexamans.Where(m => m.StudentId == a.Regd_ID).ToList(), onlineexams.Where(m => m.SubjectId == a.Subject_Id && m.SchoolId == a.SchoolId && m.ClassId == a.Class_ID).ToList(), examtype),
                               objectivescore = GetOnlineObjectiveExamscore(OnlineObjectiveStudents.Where(m => m.StudentId == a.Regd_ID).ToList(), onlineobjective.Where(m => m.SubjectId == a.Subject_Id && m.SchoolId == a.SchoolId && m.ClassId == a.Class_ID).ToList(), examtype),
                               worksheetscore = GetOnlineWorksheetExamscore(worksheets.Where(m => m.Class_Id == a.Class_ID && m.Subject_Id == a.Subject_Id && m.Regd_ID == a.Regd_ID).ToList(), modules.Where(m => m.Class_Id == a.Class_ID && m.Subject_Id == a.Subject_Id && m.Question_PDF != null).ToList(), examtype)
                           }).ToList();
                if (examtype != null && examtype != 0)
                {

                    ViewBag.examtype = examtype.Value;
                }

                if (School != null)
                {
                    res = res.Where(a => a.SchoolId == School.Value).ToList();
                    ViewBag.school = School.Value;
                }

                if (board != null && board != 0)
                {
                    res = res.Where(a => a.Board_ID == board.Value).ToList();
                    ViewBag.board = board.Value;
                }
                if (cls != null && cls != 0)
                {
                    res = res.Where(a => a.Class_ID == cls.Value).ToList();
                    ViewBag.cls = cls.Value;
                }
                if (sec != null)
                {
                    res = res.Where(a => a.SectionId == sec.Value).ToList();
                    ViewBag.sec = sec.Value;
                }
                if (subject != null && subject != 0)
                {
                    res = res.Where(a => a.SectionId == sec.Value).ToList();
                    ViewBag.subject = subject.Value;
                }
                return View(res);
            }
            else
            {
                return RedirectToAction("Logout");
            }
        }
        public decimal? GetOnlineObjectiveExamscore(List<OnlineObjectiveExamStudent> examStudents, List<OnlineObjectiveExam> exam, long? examtype)
        {
            // var answers = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();
            if (examtype != null)
            {
                exam = exam.Where(a => a.OnlineExamTypeId == examtype).ToList();
            }
            if (exam == null || examStudents == null)
            {
                return 0;
            }
            var examques = DbContext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true).ToList();

            var ress = (from a in examques
                        join b in exam on a.OnlineObjectiveExamId equals b.OnlineObjectiveExamId
                        select a).ToList();
            //var Totalmark = ress.Sum(a => a.Mark);

            var res = (from a in ress
                       join b in examStudents on a.OnlineObjectiveExamId equals b.OnlineObjectiveExamId
                       select new
                       {
                           examid = a.OnlineObjectiveExamId,
                           iscorrect = b.IsCorrect,
                           score = a.Mark
                       }).ToList();
            if (res.Count == 0)
            {
                return 0;
            }
            decimal? totalscore = 0;
            foreach (var a in res.Select(m => m.examid).Distinct().ToList())
            {
                var correct = res.Where(m => m.iscorrect == true && m.examid == a.Value).ToList().Sum(m => m.score);
                var wrong = res.Where(m => m.iscorrect == false && m.examid == a.Value).ToList().Count();
                if (exam.Where(m => m.OnlineObjectiveExamId == a.Value).FirstOrDefault().IsNagativeMarking == true)
                {
                    var negativelist = DbContext.Negativemarks.Where(m => m.NegativeMarkID == exam.Where(h => h.OnlineObjectiveExamId == a.Value).FirstOrDefault().NagativeMarkingId).FirstOrDefault();
                    var negvmark = negativelist.NegativeMark1 * wrong;
                    var scoreres = correct - negvmark;
                    totalscore += scoreres;
                }
                else
                {
                    totalscore += correct;
                }
            }
            return totalscore / res.Count();
        }
        public decimal? GetOnlineSubjectiveExamscore(List<OnlineExamAnswerSheet> examStudents, List<OnlineExam> exam, long? examtype)
        {
            // var answers = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();
            if (examtype != null)
            {
                exam = exam.Where(a => a.OnlineExamTypeId == examtype).ToList();
            }
            if (exam == null || examStudents == null)
            {
                return 0;
            }
            var examques = DbContext.OnlineExamAnswerSheetRemarks.Where(a => a.Is_Active == true).ToList();
            var examans = (from b in examStudents
                           join c in exam on b.OnlineExamId equals c.OnlineExamId
                           select new
                           {
                               total = examques.Where(a => a.OnlineExamAnswerSheetId == b.OnlineExamAnswerSheetId).Sum(a => a.GivenMarks)
                           }).ToList();

            var Totalmark = examans.Average(a => a.total);
            if (Totalmark == null)
            {
                return 0;
            }
            else
            {
                return Totalmark;
            }
        }
        public double GetOnlineWorksheetExamscore(List<tbl_DC_Worksheet> examStudents, List<tbl_DC_Module> modules, long? examtype)
        {
            if (examtype != null)
            {
                modules = modules.Where(a => a.Exam_Type == examtype).ToList();
            }
            if (modules == null || examStudents == null)
            {
                return 0;
            }
            var res = (from a in examStudents
                       join b in modules on a.Chapter_Id equals b.Chapter_Id
                       select a).ToList();
            if (res.Count == 0)
            {
                return 0;
            }
            var Totalmark = res.Average(a => a.Total_Mark);
            if (Totalmark == null)
            {
                return 0;
            }
            else
            {
                return Totalmark;
            }
        }

        #endregion

       #region------------------Question Bank---------------------

        public ActionResult QuestionBankListReport(Guid? School, int? board, int? cls, Guid? sec, int? subject, int? Chapter)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                var teacherid = Convert.ToInt32(Session["TeacherId"]);
                Guid? schoolid = new Guid(Session["SchoolId"].ToString());
                var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
                var boardlistss = assigndet.Select(a => a.Board_Id).ToList();
                var classlist = assigndet.Select(a => a.Class_Id).ToList();
                var sectionlist = assigndet.Select(a => a.SectionId).ToList();
                var subjectlist = assigndet.Select(a => a.Subject_Id).ToList();

                var regs = DbContext.VW_Student_With_Subject.Where(a => boardlistss.Contains(a.Board_ID.Value) && classlist.Contains(a.Class_ID.Value) && sectionlist.Contains(a.SectionId) && subjectlist.Contains(a.Subject_Id) && a.SchoolId == schoolid.Value).ToList();
                var wrk = DbContext.tbl_DC_QuestionBank.Where(a => a.Is_Active == true && a.Is_Deleted == false && boardlistss.Contains(a.Board_Id) && classlist.Contains(a.Class_Id) && sectionlist.Contains(a.SectionId) && subjectlist.Contains(a.Subject_Id)).ToList();

                // var wrk = DbContext.tbl_DC_QuestionBank.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();


                var boards = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                var modules = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
                var questionbankattachments = DbContext.tbl_DC_QuestionBank_Attachment.Where(a => a.Is_Active == true).ToList();



                var res = (from a in wrk
                           join b in boards on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId

                           select new QuestionBankModel
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Module_Name = f.Module_Name,
                               Regd_ID = a.Regd_ID,
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = a.Regd_ID != 0 ? students.Where(s => s.Regd_ID == a.Regd_ID).FirstOrDefault().Customer_Name : "",
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Student_Mobile = a.Regd_ID != 0 ? students.Where(s => s.Regd_ID == a.Regd_ID).FirstOrDefault().Mobile : "",
                               Total_Mark = a.Total_Mark,
                               Status = a.Verified == true ? "Verified" : "Uploaded",
                               Upload_Date = a.Inserted_Date != null ? Convert.ToDateTime(a.Inserted_Date).ToString("dd-MMM-yyyy") : "NA",
                               Verify_Date = a.Verified == true ? Convert.ToDateTime(a.Modified_Date).ToString("dd-MMM-yyyy") : "NA",
                               TotalVerified = wrk.Where(z => z.Verified == true).ToList().Count(),
                               TotalPending = wrk.Where(z => z.Verified == false).ToList().Count(),
                               TotalSubmitted = wrk.ToList().Count(),
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               QuestionBank_Id = a.QuestionBank_Id,
                               attachments = (from k in questionbankattachments
                                              where k.QuestionBank_Id == a.QuestionBank_Id
                                              select new tbl_DC_QuestionBank_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Module/Questionbank/" + k.Path,
                                                  QuestionBank_Attachment_Id = k.QuestionBank_Attachment_Id,
                                                  QuestionBank_Id = k.QuestionBank_Id
                                              }).ToList(),
                           }).ToList();


                if (board != null)
                {
                    res = res.Where(a => a.Board_Id == board).ToList();
                    ViewBag.board = board.Value;
                }

                if (cls != null)
                {
                    res = res.Where(a => a.Class_Id == cls).ToList();
                    ViewBag.cls = cls.Value;
                }
                if (sec != null)
                {
                    res = res.Where(a => a.SectionId == sec).ToList();
                    ViewBag.sec = sec.Value;
                }
                if (subject != null)
                {
                    res = res.Where(a => a.Subject_Id == subject).ToList();
                    ViewBag.subject = subject.Value;
                }
                if (Chapter != null)
                {
                    res = res.Where(a => a.Chapter_Id == Chapter).ToList();
                    ViewBag.chapter = Chapter.Value;
                }
                if (School != null)
                {
                    ViewBag.school = School.Value;
                }
                return View(res);
            }
            else
            {
                return RedirectToAction("Logout");
            }
        }
        [HttpPost]
        public ActionResult EditQuestionBankMark(int QuestionBank_Id, int Total_Mark)
        {
            try
            {
                var teacherid = Convert.ToInt32(Session["TeacherId"]);
                var alldetail = DbContext.tbl_DC_QuestionBank.Where(x => x.QuestionBank_Id == QuestionBank_Id).FirstOrDefault();
                if (alldetail != null)
                {
                    alldetail.Total_Mark = Total_Mark;
                    alldetail.Modified_Date = today;
                    alldetail.Modified_By = teacherid;
                    DbContext.Entry(alldetail).State = EntityState.Modified;
                    DbContext.SaveChanges();
                    TempData["SuccessMessage"] = " Successfully Edit Score ";
                    return RedirectToAction("QuestionBankListReport");
                }
            }
            catch
            {
                return View();
            }
            return View();

        }
        //[HttpGet]
        //public ActionResult CreateQuestionBank()
        //{
        //    var username = Convert.ToString(Session["Mobile"]);
        //    if (username != null && username != "")
        //    {
        //        return View();
        //    }
        //    else
        //    {
        //        return RedirectToAction("Login");
        //    }

        //}
        //[HttpPost]
        //public ActionResult SaveQuestionBank(int board, int cls, Guid sec, int Subject, int chapter, HttpPostedFileBase[] Files)
        //{
        //    var teacherid = Convert.ToInt32(Session["TeacherId"]);



        //    tbl_DC_QuestionBank ques = new tbl_DC_QuestionBank();
        //    ques.Regd_ID = 0;
        //    ques.Board_Id = board;
        //    ques.Class_Id = cls;
        //    ques.SectionId = sec;
        //    ques.Subject_Id = Subject;
        //    ques.Chapter_Id = chapter;
        //    ques.Module_Id = 0;
        //    ques.Verified = false;
        //    ques.Verified_By = 0;
        //    ques.Total_Mark = 0;

        //    ques.Inserted_Date = DateTime.Now;
        //    ques.Inserted_By = teacherid;
        //    ques.Modified_By = teacherid;
        //    ques.Modified_Date = DateTime.Now;
        //    ques.Is_Active = true;
        //    ques.Is_Deleted = false;
        //    ques.File_Type = "pdf";
        //    ques.is_Read = false;
        //    DbContext.tbl_DC_QuestionBank.Add(ques);
        //    DbContext.SaveChanges();
        //    var quesbankid = ques.QuestionBank_Id;
        //    string guid = Guid.NewGuid().ToString();
        //    if (Files.Length > 0)
        //    {
        //        int k = 1;
        //        foreach (HttpPostedFileBase file in Files)
        //        {
        //            tbl_DC_QuestionBank_Attachment att = new tbl_DC_QuestionBank_Attachment();
        //            att.File_Name = k + ".pdf";
        //            att.QuestionBank_Id = quesbankid;
        //            att.Modified_By = teacherid;
        //            att.Modified_Date = DateTime.Now;
        //            att.Inserted_Date = DateTime.Now;
        //            att.Inserted_By = teacherid;
        //            att.Is_Active = true;
        //            att.Is_Deleted = false;

        //            var pathlink = Path.Combine(Server.MapPath("~/Module/Questionbank/"), (guid + att.File_Name));
        //            file.SaveAs(pathlink);
        //            att.Path = guid + att.File_Name;
        //            DbContext.tbl_DC_QuestionBank_Attachment.Add(att);
        //            DbContext.SaveChanges();

        //            k = k + 1;

        //        }
        //    }


        //    TempData["SuccessMessage"] = "Sucessfully Uploaed Question Bank..";
        //    return RedirectToAction("CreateQuestionBank");
        //}


        //[HttpGet]
        //public ActionResult EditQuestionBank(int id)
        //{
        //    var username = Convert.ToString(Session["Mobile"]);
        //    if (username != null && username != "")
        //    {
        //        Guid? schoolid = new Guid(Session["SchoolId"].ToString());
        //        ViewBag.Board_details = DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
        //        ViewBag.Class_details = DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
        //        ViewBag.Section_details = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.Class_Id == id && x.School_Id == schoolid).Distinct().ToList();
        //        ViewBag.Subject_details = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
        //        ViewBag.Chapter_details = DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false).ToList();


        //        try
        //        {
        //            var wrk = DbContext.tbl_DC_QuestionBank.Where(a => a.Is_Active == true && a.QuestionBank_Id == id).ToList();
        //            var board = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
        //            var classes = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
        //            var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
        //            var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
        //            var modules = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
        //            var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
        //            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
        //            var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
        //            var questionbankattachments = DbContext.tbl_DC_QuestionBank_Attachment.Where(a => a.Is_Active == true).ToList();

        //            var res = (from a in wrk
        //                       join b in board on a.Board_Id equals b.Board_Id
        //                       join c in classes on a.Class_Id equals c.Class_Id
        //                       join d in subjects on a.Subject_Id equals d.Subject_Id
        //                       join e in chapters on a.Chapter_Id equals e.Chapter_Id
        //                       join f in modules on a.Module_Id equals f.Module_ID
        //                       join g in sections on a.SectionId equals g.SectionId

        //                       select new QuestionBankModel
        //                       {
        //                           Board_Id = a.Board_Id,
        //                           Board_Name = b.Board_Name,
        //                           Chapter_Id = a.Chapter_Id,
        //                           Chapter_Name = e.Chapter,
        //                           Class_Id = a.Class_Id,
        //                           Class_Name = c.Class_Name,
        //                           File_Type = a.File_Type,
        //                           Inserted_Date = a.Inserted_Date,
        //                           Modified_Date = a.Modified_Date,
        //                           Module_Id = a.Module_Id,
        //                           Module_Name = "",
        //                           Regd_ID = a.Regd_ID,
        //                           Status = a.Verified == true ? "Verified" : "Uploaded",
        //                           Section_Name = g.SectionName,
        //                           SectionId = a.SectionId,
        //                           Student_Name = a.Regd_ID != 0 ? students.Where(s => s.Regd_ID == a.Regd_ID).FirstOrDefault().Customer_Name : "",
        //                           Student_Mobile = a.Regd_ID != 0 ? students.Where(s => s.Regd_ID == a.Regd_ID).FirstOrDefault().Mobile : "",
        //                           Subject_Id = a.Subject_Id,
        //                           Subject_Name = d.Subject,
        //                           Total_Mark = a.Total_Mark,
        //                           Upload_Date = a.Inserted_Date != null ? Convert.ToDateTime(a.Inserted_Date).ToString("dd-MMM-yyyy") : "NA",
        //                           Verify_Date = a.Verified == true ? Convert.ToDateTime(a.Modified_Date).ToString("dd-MMM-yyyy") : "NA",
        //                           Verified = a.Verified,
        //                           Verified_By = a.Verified_By,
        //                           Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
        //                           QuestionBank_Id = a.QuestionBank_Id,
        //                           attachments = (from k in questionbankattachments
        //                                          where k.QuestionBank_Id == a.QuestionBank_Id
        //                                          select new tbl_DC_QuestionBank_Attachment
        //                                          {
        //                                              File_Name = k.File_Name,
        //                                              Modified_Date = k.Modified_Date,
        //                                              Path = "/Module/Questionbank/" + k.Path,
        //                                              QuestionBank_Attachment_Id = k.QuestionBank_Attachment_Id,
        //                                              QuestionBank_Id = k.QuestionBank_Id
        //                                          }).ToList(),
        //                       }).FirstOrDefault();

        //            ViewBag.QuestionBankDetails = res;


        //            return View(res);
        //        }

        //        catch
        //        {
        //            return View();
        //        }

        //    }
        //    else
        //    {
        //        return RedirectToAction("Login");
        //    }
        //}

        public ActionResult DeleteQuestionBank(int id)
        {
            try
            {

                var alldetail = DbContext.tbl_DC_QuestionBank.Where(x => x.QuestionBank_Id == id).FirstOrDefault();
                if (alldetail != null)
                {
                    alldetail.Is_Deleted = true;
                    alldetail.Modified_Date = today;
                    alldetail.Modified_By = 1;
                    DbContext.Entry(alldetail).State = EntityState.Modified;
                    DbContext.SaveChanges();
                    TempData["SuccessMessage"] = " Successfully Delete Worksheet ";
                    return RedirectToAction("QuestionBankList");
                }
            }
            catch
            {
                return View();
            }
            return View();

        }



        public List<List<string>> splitList(List<string> locations, int nSize = 1000)
        {
            var list = new List<List<string>>();

            for (int i = 0; i < locations.Count; i += nSize)
            {
                list.Add(locations.GetRange(i, Math.Min(nSize, locations.Count - i)));
            }

            return list;
        }
        #endregion----------------End Question Bank----------------
        #region--------------- Module---------------

        #region ...........Worksheet Upload..............
        [HttpGet]
        public ActionResult ViewWorksheet(int? board, int? cls, int? subject, int? chapter)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Worksheet";
            var res = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                       join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on a.Board_Id equals b.Board_Id

                       join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on b.Class_Id equals c.Class_Id
                       join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on c.Subject_Id equals d.Subject_Id
                       join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on d.Chapter_Id equals f.Chapter_Id
                       where f.Question_PDF != null
                       select new DigiChampsModel.DigiChampsModuleModel
                       {
                           Module_ID = f.Module_ID,
                           Module_Name = f.Module_Name,
                           Chapter_Id = f.Chapter_Id,
                           Chapter = d.Chapter,
                           Subject_Id = d.Subject_Id,
                           Subject = c.Subject,
                           Board_Id = b.Board_Id,
                           Board_Name = a.Board_Name,
                           Class_Id = c.Class_Id,
                           Class_Name = b.Class_Name,
                           Module_Desc = f.Module_Desc,
                           Module_Content = f.Module_Content,
                           Module_video = f.Module_video,
                           Is_Free = f.Is_Free,
                           Question_PDF_Name = f.Question_PDF_Name,
                           Upload_PDF = f.Module_Content_Name,
                           Video_Type = f.Video_Type,
                           Class_PPT = f.Class_PPT,
                           Class_PPT_Name = f.Class_PPT_Name,
                           NCERT_PDF = f.NCERT_PDF,
                           NCERT_PDF_Name = f.NCERT_PDF_Name,
                           ExamTypeName = f.Exam_Type == null ? "" : DbContext.OnlineExamTypes.Where(m => m.OnlineExamTypeId == f.Module_ID).FirstOrDefault().OnlineExamTypeName,
                           Question_PDF = f.Question_PDF,
                           Module_Image = f.Module_Image
                       }).OrderByDescending(x => x.Module_ID).ToList();
            ViewBag.board = 0;
            ViewBag.cls = 0;
            ViewBag.subject = 0;
            ViewBag.chapter = 0;
            if (board != null)
            {
                res = res.Where(a => a.Board_Id == board).ToList();
                ViewBag.board = board;
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_Id == cls).ToList();
                ViewBag.cls = cls;
            }
            if (subject != null)
            {
                res = res.Where(a => a.Subject_Id == subject).ToList();
                ViewBag.subject = subject;
            }
            if (chapter != null)
            {
                res = res.Where(a => a.Chapter_Id == chapter).ToList();
                ViewBag.chapter = chapter;
            }
            // ViewBag.moduledata = res;
            return View(res);
        }
        [HttpGet]
        public ActionResult CreateOrEditWorksheet(string id, string returnurl)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id1 = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id1 && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Worksheet";
            ViewBag.returnurl = returnurl;
            ViewBag.examtypes = new SelectList(DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList(), "OnlineExamTypeId", "OnlineExamTypeName");
            ViewBag.upload_data = "";//otp_data;
            ViewBag.board = new SelectList(DbContext.tbl_DC_Board.Where(b => b.Is_Active == true && b.Is_Deleted == false), "Board_Id", "Board_Name");
            if (id != "" && id != null)
            {
                UInt32 moduleid;
                if (UInt32.TryParse(id, out moduleid) == true)
                {
                    int mid = Convert.ToInt32(moduleid);
                    DigiChampsModel.DigiChampsModuleModel obj = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                 join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on a.Board_Id equals b.Board_Id
                                                                 join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on b.Class_Id equals c.Class_Id
                                                                 join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on c.Subject_Id equals d.Subject_Id
                                                                 join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on d.Chapter_Id equals f.Chapter_Id
                                                                 select new DigiChampsModel.DigiChampsModuleModel
                                                                 {
                                                                     Board_Id = a.Board_Id,
                                                                     Class_Id = b.Class_Id,
                                                                     Subject_Id = c.Subject_Id,
                                                                     Chapter_Id = d.Chapter_Id,
                                                                     Module_ID = f.Module_ID,
                                                                     Module_Name = f.Module_Name,
                                                                     Module_Desc = f.Module_Desc,
                                                                     Module_Content = f.Module_Content,
                                                                     Module_video = f.Module_video,
                                                                     Is_Free = f.Is_Free,
                                                                     Validity = f.Validity,
                                                                     Question_PDF = f.Question_PDF,
                                                                     Is_Free_Test = f.Is_Free_Test,
                                                                     No_Question = f.No_Of_Question,
                                                                     Upload_PDF = f.Module_Content_Name,
                                                                     Question_PDF_Name = f.Question_PDF_Name,
                                                                     Video_Type = f.Video_Type,
                                                                     NCERT_PDF = f.NCERT_PDF,
                                                                     NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                                     Class_PPT = f.Class_PPT,
                                                                     Class_PPT_Name = f.Class_PPT_Name,
                                                                     OnlineExamTypeId = f.Exam_Type,
                                                                     videoid = f.Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + f.Module_video) : f.Module_video
                                                                 }).Where(x => x.Module_ID == mid).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.classid = obj.Class_Id;
                        ViewBag.subid = obj.Subject_Id;
                        ViewBag.chapterid = obj.Chapter_Id;
                        ViewBag.Modelvideo = obj.Module_video;
                        ViewBag.modelcontent = obj.Module_Content;
                        ViewBag.qstnpdf = obj.Question_PDF;
                        ViewBag.ncertpdf = obj.NCERT_PDF;
                        ViewBag.classppt = obj.Class_PPT;
                        return View(obj);
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Invalid module details.";
                    }
                }
                else
                {
                    TempData["WarningMessage"] = "Invalid module details.";
                }
            }

            return View();
        }
        [HttpPost]
        public ActionResult SaveOrUpdateWorksheet(DigiChampsModel.DigiChampsModuleModel obj,
            HttpPostedFileBase Question_PDF,
            string returnurl)
        {
            //Menupermission();
            ViewBag.Breadcrumb = "Worksheet";
            string msg = string.Empty;
            try
            {
                ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
                if (ModelState.IsValid)
                {
                    //if (obj.Module_Name.Trim() != "")
                    //{
                    if (obj.Module_ID == null)
                    {
                        var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Question_PDF != null && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (module != null)
                        {
                            TempData["WarningMessage"] = "Worksheet already exists.";
                        }
                        else
                        {
                            tbl_DC_Module obj1 = new tbl_DC_Module();
                            obj1.Module_Name = obj.Module_Name;
                            obj1.Module_Desc = obj.Module_Desc;
                            obj1.Board_Id = obj.Board_Id;
                            obj1.Class_Id = obj.Class_Id;
                            obj1.Subject_Id = obj.Subject_Id;
                            obj1.Chapter_Id = obj.Chapter_Id;
                            obj1.Exam_Type = obj.OnlineExamTypeId;
                            obj1.Is_Free = true;
                            if (obj1.Is_Free == true)
                            {
                                obj1.Validity = obj.Validity;
                            }
                            if (Question_PDF != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(Question_PDF.FileName.Replace(Question_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Module/Question_PDF/"), fileName);
                                Question_PDF.SaveAs(path);
                                obj1.Question_PDF = fileName;
                                obj1.No_Of_Question = obj.No_Question;
                            }
                            string extramsg = "";
                            obj1.Question_PDF_Name = obj.Question_PDF_Name;
                            //if (obj.Is_Free_Test == true)
                            //{
                            //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id ==
                            //        obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                            //    if (iftest_chap == null)
                            //    {
                            obj1.Is_Free_Test = true;
                            //    }
                            //    else
                            //    {
                            //        obj1.Is_Free_Test = false;
                            //        extramsg = " Each chapter can only contain single free test.";
                            //    }
                            //}
                            obj1.Inserted_By = HttpContext.User.Identity.Name;
                            obj1.Is_Active = true;
                            obj1.Is_Deleted = false;
                            DbContext.tbl_DC_Module.Add(obj1);
                            DbContext.SaveChanges();
                            try
                            {
                                var deciceid = DbContext.tbl_DC_Registration.
                                    Where(x => x.Device_id != null && !x.Device_id.Equals("null")
                                        && obj1.Class_Id == obj.Class_Id).Select(x => x.Device_id).ToList();
                                // var regid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Class_ID == obj.Class_Id && x.Regd_ID != null).Select(x => x.Regd_ID).ToList();
                                //var deciceid = DbContext.tbl_DC_Registration.Where
                                //    (x => x.Device_id == "ePEyVbqUrBA:APA91bGzpOkF_jPnlBKlQFydnOkcy52DY7Wl-6ks1izlpZDfd4Jq8RN5gQNUquScLd0LzIB91nVwG5NPXxlJazQRXvbTFZ_COIS4KgumbuIsYfN41n5ezij4UKNMGwk9rUfCfxQKWUtZ" ||
                                //     x.Device_id == "c6Ixravhr_Q:APA91bFoGlTItg7aEe2Sz6_80fv9Rjy7IhSDAsaYVOVnh75891RFcJ-dlUGK4V9Pfxbj0GeS_sf_lC3cE-l9C_wzlsGYV2VdrQzmKeGMfTS2dpo1Q_R2Tuv3hSotogJy9yuONkywMcw5"
                                //     ).Select(x => x.Device_id).ToList();

                                //string body = "Body";.Replace("{{video}}", obj1.Module_video)
                                string title = "New Video";
                                string body_header = "New Video#{{Module_ID}}# Upload New Video : Hello {{Name}},! You have New Video.";
                                string body = body_header.ToString().Replace("{{Name}}",
                                    obj1.Module_Name).Replace("{{image}}", obj1.Module_Image).Replace("{{Feed_ID}}", obj1.Module_ID.ToString());
                                body = "NVID#{{Module_ID}}#New Video#Don't miss the new video. Check it out";

                                List<List<string>> ids = splitList(deciceid);

                                for (int i = 0; i < ids.Count; i++)
                                {
                                    if (ids[i] != null)
                                    {
                                        var note = new PushNotiStatusVideo(title, body, obj1.Module_Image, ids[i]);
                                    }
                                }
                            }
                            catch (Exception)
                            {

                            }
                            msg = "1";      //"Module details saved successfully.";
                            TempData["SuccessMessage"] = "Worksheet details saved successfully." + extramsg;
                        }
                    }
                    else
                    {
                        var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Question_PDF != null && x.Module_ID != obj.Module_ID && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (module != null)
                        {
                            TempData["WarningMessage"] = "Worksheet Already exist.";
                        }
                        else
                        {
                            int mod_id = Convert.ToInt32(obj.Module_ID);
                            tbl_DC_Module obj1 = DbContext.tbl_DC_Module.Where(x => x.Module_ID == mod_id).FirstOrDefault();
                            obj1.Module_Name = obj.Module_Name;
                            obj1.Module_Desc = obj.Module_Desc;
                            obj1.Is_Free = true;
                            obj1.Board_Id = obj.Board_Id;
                            obj1.Class_Id = obj.Class_Id;
                            obj1.Subject_Id = obj.Subject_Id;
                            obj1.Chapter_Id = obj.Chapter_Id;
                            obj1.Exam_Type = obj.OnlineExamTypeId;
                            if (obj1.Is_Free == true)
                            {
                                obj1.Validity = obj.Validity;
                            }
                            if (Question_PDF != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(Question_PDF.FileName.Replace(Question_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Module/Question_PDF/"), fileName);
                                Question_PDF.SaveAs(path);
                                obj1.Question_PDF = fileName;
                                obj1.No_Of_Question = obj.No_Question;
                            }
                            obj1.Question_PDF_Name = obj.Question_PDF_Name;
                            //if (obj.Is_Free_Test == true)
                            //{
                            //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                            //    if (iftest_chap == null)
                            //    {
                            obj1.Is_Free_Test = true;
                            //    }
                            //    else
                            //    {
                            //        obj1.Is_Free_Test = false;
                            //    }
                            //}

                            obj1.Inserted_By = HttpContext.User.Identity.Name;
                            obj1.Is_Active = true;
                            obj1.Is_Deleted = false;
                            obj1.Modified_Date = today;
                            obj1.Modified_By = HttpContext.User.Identity.Name;
                            DbContext.Entry(obj1).State = EntityState.Modified;
                            DbContext.SaveChanges();
                            msg = "11";         // "Module details updated successfully.";
                            TempData["SuccessMessage"] = "Worksheet details updated successfully";
                        }
                    }
                    //}
                    //else
                    //{
                    //    msg = "00";         // "Please enter module name.";
                    //}
                }
            }
            catch (Exception ex)
            {
                //msg = "Something went wrong.";
                throw ex;
            }
            //return Json(msg, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("ViewModule");
            return Redirect(returnurl);
        }
        [HttpPost]
        public ActionResult MultipleDeleteWorksheet(int[] chkselect)
        {
            foreach (int a in chkselect)
            {
                tbl_DC_Module md = DbContext.tbl_DC_Module.Where(m => m.Module_ID == a).FirstOrDefault();
                md.Is_Active = false;
                md.Modified_Date = DateTime.Now;
                DbContext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Worksheet deleted successfully";
            return RedirectToAction("ViewWorksheet", "Admin");
        }
        public ActionResult DeleteWorksheet(int? id, string returnurl)
        {
            try
            {
                if (id != null)
                {
                    tbl_DC_Module obj = DbContext.tbl_DC_Module.Where(x => x.Module_ID == id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Is_Active = false;
                        obj.Is_Deleted = true;
                        obj.Modified_By = HttpContext.User.Identity.Name;
                        obj.Modified_Date = today;
                        DbContext.Entry(obj).State = EntityState.Modified;
                        DbContext.SaveChanges();

                        TempData["SuccessMessage"] = "Worksheet deleted successfully.";
                    }
                    return Redirect(returnurl);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View("ViewWorksheet");
        }
        #endregion

        #region ...........Study Note Upload..............
        [HttpGet]
        public ActionResult ViewStudyNotes(int? board, int? cls, int? subject, int? chapter)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Study Notes";
            var res = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                       join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on a.Board_Id equals b.Board_Id

                       join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on b.Class_Id equals c.Class_Id
                       join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on c.Subject_Id equals d.Subject_Id
                       join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on d.Chapter_Id equals f.Chapter_Id
                       where f.Module_Content != null
                       select new DigiChampsModel.DigiChampsModuleModel
                       {
                           Module_ID = f.Module_ID,
                           Module_Name = f.Module_Name,
                           Chapter_Id = f.Chapter_Id,
                           Chapter = d.Chapter,
                           Subject_Id = d.Subject_Id,
                           Subject = c.Subject,
                           Board_Id = b.Board_Id,
                           Board_Name = a.Board_Name,
                           Class_Id = c.Class_Id,
                           Class_Name = b.Class_Name,
                           Module_Desc = f.Module_Desc,
                           Module_Content = f.Module_Content,
                           Module_Content_Name = f.Module_Content_Name,
                           Module_video = f.Module_video,
                           Is_Free = f.Is_Free,
                           Question_PDF_Name = f.Question_PDF_Name,
                           Upload_PDF = f.Module_Content_Name,
                           Video_Type = f.Video_Type,
                           Class_PPT = f.Class_PPT,
                           Class_PPT_Name = f.Class_PPT_Name,
                           NCERT_PDF = f.NCERT_PDF,
                           NCERT_PDF_Name = f.NCERT_PDF_Name
                       }).OrderByDescending(x => x.Module_ID).ToList();
            ViewBag.board = 0;
            ViewBag.cls = 0;
            ViewBag.subject = 0;
            ViewBag.chapter = 0;
            if (board != null)
            {
                res = res.Where(a => a.Board_Id == board).ToList();
                ViewBag.board = board;
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_Id == cls).ToList();
                ViewBag.cls = cls;
            }
            if (subject != null)
            {
                res = res.Where(a => a.Subject_Id == subject).ToList();
                ViewBag.subject = subject;
            }
            if (chapter != null)
            {
                res = res.Where(a => a.Chapter_Id == chapter).ToList();
                ViewBag.chapter = chapter;
            }
            // ViewBag.moduledata = res;
            return View(res);
        }
        [HttpGet]
        public ActionResult CreateOrEditStudyNotes(string id, string returnurl)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id1 = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id1 && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Worksheet";
            ViewBag.returnurl = returnurl;

            ViewBag.upload_data = "";//otp_data;
            ViewBag.board = new SelectList(DbContext.tbl_DC_Board.Where(b => b.Is_Active == true && b.Is_Deleted == false), "Board_Id", "Board_Name");
            if (id != "" && id != null)
            {
                UInt32 moduleid;
                if (UInt32.TryParse(id, out moduleid) == true)
                {
                    int mid = Convert.ToInt32(moduleid);
                    DigiChampsModel.DigiChampsModuleModel obj = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                 join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on a.Board_Id equals b.Board_Id
                                                                 join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on b.Class_Id equals c.Class_Id
                                                                 join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on c.Subject_Id equals d.Subject_Id
                                                                 join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on d.Chapter_Id equals f.Chapter_Id
                                                                 select new DigiChampsModel.DigiChampsModuleModel
                                                                 {
                                                                     Board_Id = a.Board_Id,
                                                                     Class_Id = b.Class_Id,
                                                                     Subject_Id = c.Subject_Id,
                                                                     Chapter_Id = d.Chapter_Id,
                                                                     Module_ID = f.Module_ID,
                                                                     Module_Name = f.Module_Name,
                                                                     Module_Desc = f.Module_Desc,
                                                                     Module_Content = f.Module_Content,
                                                                     Module_video = f.Module_video,
                                                                     Is_Free = f.Is_Free,
                                                                     Validity = f.Validity,
                                                                     Question_PDF = f.Question_PDF,
                                                                     Is_Free_Test = f.Is_Free_Test,
                                                                     No_Question = f.No_Of_Question,
                                                                     Upload_PDF = f.Module_Content_Name,
                                                                     Question_PDF_Name = f.Question_PDF_Name,
                                                                     Video_Type = f.Video_Type,
                                                                     NCERT_PDF = f.NCERT_PDF,
                                                                     NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                                     Class_PPT = f.Class_PPT,
                                                                     Class_PPT_Name = f.Class_PPT_Name,
                                                                     videoid = f.Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + f.Module_video) : f.Module_video
                                                                 }).Where(x => x.Module_ID == mid).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.classid = obj.Class_Id;
                        ViewBag.subid = obj.Subject_Id;
                        ViewBag.chapterid = obj.Chapter_Id;
                        ViewBag.Modelvideo = obj.Module_video;
                        ViewBag.modelcontent = obj.Module_Content;
                        ViewBag.qstnpdf = obj.Question_PDF;
                        ViewBag.ncertpdf = obj.NCERT_PDF;
                        ViewBag.classppt = obj.Class_PPT;
                        return View(obj);
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Invalid module details.";
                    }
                }
                else
                {
                    TempData["WarningMessage"] = "Invalid module details.";
                }
            }

            return View();
        }
        [HttpPost]
        public ActionResult SaveOrUpdateStudyNotes(DigiChampsModel.DigiChampsModuleModel obj,
            HttpPostedFileBase module_pdf,
            string returnurl)
        {
            //Menupermission();
            ViewBag.Breadcrumb = "Study Notes";
            string msg = string.Empty;
            try
            {
                ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
                if (ModelState.IsValid)
                {
                    //if (obj.Module_Name.Trim() != "")
                    //{
                    if (obj.Module_ID == null)
                    {
                        var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Module_Content != null && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (module != null)
                        {
                            TempData["WarningMessage"] = "Study Notes already exists.";
                        }
                        else
                        {
                            tbl_DC_Module obj1 = new tbl_DC_Module();
                            obj1.Module_Name = obj.Module_Name;
                            obj1.Module_Desc = obj.Module_Desc;
                            obj1.Board_Id = obj.Board_Id;
                            obj1.Class_Id = obj.Class_Id;
                            obj1.Subject_Id = obj.Subject_Id;
                            obj1.Chapter_Id = obj.Chapter_Id;
                            obj1.Is_Free = true;
                            if (obj1.Is_Free == true)
                            {
                                obj1.Validity = obj.Validity;
                            }
                            if (module_pdf != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(module_pdf.FileName.
                                    Replace(module_pdf.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Module/PDF/"), fileName);
                                module_pdf.SaveAs(path);
                                obj1.Module_Content = fileName;
                            }
                            string extramsg = "";
                            obj1.Module_Content_Name = obj.Upload_PDF;
                            //if (obj.Is_Free_Test == true)
                            //{
                            //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id ==
                            //        obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                            //    if (iftest_chap == null)
                            //    {
                            obj1.Is_Free_Test = true;
                            //    }
                            //    else
                            //    {
                            //        obj1.Is_Free_Test = false;
                            //        extramsg = " Each chapter can only contain single free test.";
                            //    }
                            //}
                            obj1.Inserted_By = HttpContext.User.Identity.Name;
                            obj1.Is_Active = true;
                            obj1.Is_Deleted = false;
                            DbContext.tbl_DC_Module.Add(obj1);
                            DbContext.SaveChanges();
                            try
                            {
                                var deciceid = DbContext.tbl_DC_Registration.
                                    Where(x => x.Device_id != null && !x.Device_id.Equals("null")
                                        && obj1.Class_Id == obj.Class_Id).Select(x => x.Device_id).ToList();
                                // var regid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Class_ID == obj.Class_Id && x.Regd_ID != null).Select(x => x.Regd_ID).ToList();
                                //var deciceid = DbContext.tbl_DC_Registration.Where
                                //    (x => x.Device_id == "ePEyVbqUrBA:APA91bGzpOkF_jPnlBKlQFydnOkcy52DY7Wl-6ks1izlpZDfd4Jq8RN5gQNUquScLd0LzIB91nVwG5NPXxlJazQRXvbTFZ_COIS4KgumbuIsYfN41n5ezij4UKNMGwk9rUfCfxQKWUtZ" ||
                                //     x.Device_id == "c6Ixravhr_Q:APA91bFoGlTItg7aEe2Sz6_80fv9Rjy7IhSDAsaYVOVnh75891RFcJ-dlUGK4V9Pfxbj0GeS_sf_lC3cE-l9C_wzlsGYV2VdrQzmKeGMfTS2dpo1Q_R2Tuv3hSotogJy9yuONkywMcw5"
                                //     ).Select(x => x.Device_id).ToList();

                                //string body = "Body";.Replace("{{video}}", obj1.Module_video)
                                string title = "New Video";
                                string body_header = "New Video#{{Module_ID}}# Upload New Video : Hello {{Name}},! You have New Video.";
                                string body = body_header.ToString().Replace("{{Name}}",
                                    obj1.Module_Name).Replace("{{image}}", obj1.Module_Image).Replace("{{Feed_ID}}", obj1.Module_ID.ToString());
                                body = "NVID#{{Module_ID}}#New Video#Don't miss the new video. Check it out";

                                List<List<string>> ids = splitList(deciceid);

                                for (int i = 0; i < ids.Count; i++)
                                {
                                    if (ids[i] != null)
                                    {
                                        var note = new PushNotiStatusVideo(title, body, obj1.Module_Image, ids[i]);
                                    }
                                }
                            }
                            catch (Exception)
                            {

                            }
                            msg = "1";      //"Module details saved successfully.";
                            TempData["SuccessMessage"] = "Worksheet details saved successfully." + extramsg;
                        }
                    }
                    else
                    {
                        var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Module_Content != null && x.Module_ID != obj.Module_ID && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (module != null)
                        {
                            TempData["WarningMessage"] = "Study Notes already exist.";
                        }
                        else
                        {
                            int mod_id = Convert.ToInt32(obj.Module_ID);
                            tbl_DC_Module obj1 = DbContext.tbl_DC_Module.Where(x => x.Module_ID == mod_id).FirstOrDefault();
                            obj1.Module_Name = obj.Module_Name;
                            obj1.Module_Desc = obj.Module_Desc;
                            obj1.Is_Free = true;
                            obj1.Board_Id = obj.Board_Id;
                            obj1.Class_Id = obj.Class_Id;
                            obj1.Subject_Id = obj.Subject_Id;
                            obj1.Chapter_Id = obj.Chapter_Id;

                            if (obj1.Is_Free == true)
                            {
                                obj1.Validity = obj.Validity;
                            }
                            if (module_pdf != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(module_pdf.FileName.
                                    Replace(module_pdf.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Module/PDF/"), fileName);
                                module_pdf.SaveAs(path);
                                obj1.Module_Content = fileName;
                            }
                            string extramsg = "";
                            obj1.Module_Content_Name = obj.Upload_PDF;
                            //if (obj.Is_Free_Test == true)
                            //{
                            //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                            //    if (iftest_chap == null)
                            //    {
                            obj1.Is_Free_Test = true;
                            //    }
                            //    else
                            //    {
                            //        obj1.Is_Free_Test = false;
                            //    }
                            //}

                            obj1.Inserted_By = HttpContext.User.Identity.Name;
                            obj1.Is_Active = true;
                            obj1.Is_Deleted = false;
                            obj1.Modified_Date = today;
                            obj1.Modified_By = HttpContext.User.Identity.Name;
                            DbContext.Entry(obj1).State = EntityState.Modified;
                            DbContext.SaveChanges();
                            msg = "11";         // "Module details updated successfully.";
                            TempData["SuccessMessage"] = "Worksheet details updated successfully";
                            TempData["SuccessMessage"] = "Worksheet details updated successfully";
                        }
                    }
                    //}
                    //else
                    //{
                    //    msg = "00";         // "Please enter module name.";
                    //}
                }
            }
            catch (Exception ex)
            {
                //msg = "Something went wrong.";
                throw ex;
            }
            //return Json(msg, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("ViewModule");
            return Redirect(returnurl);
        }
        [HttpPost]
        public ActionResult MultipleDeleteStudyNotes(int[] chkselect)
        {
            foreach (int a in chkselect)
            {
                tbl_DC_Module md = DbContext.tbl_DC_Module.Where(m => m.Module_ID == a).FirstOrDefault();
                md.Is_Active = false;
                md.Modified_Date = DateTime.Now;
                DbContext.SaveChanges();

            }
            TempData["SuccessMessage"] = "Study notes details deleted successfully";
            return RedirectToAction("ViewStudyNotes", "Admin");
        }
        public ActionResult DeleteStudyNotes(int? id, string returnurl)
        {
            try
            {
                if (id != null)
                {
                    tbl_DC_Module obj = DbContext.tbl_DC_Module.Where(x => x.Module_ID == id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Is_Active = false;
                        obj.Is_Deleted = true;
                        obj.Modified_By = HttpContext.User.Identity.Name;
                        obj.Modified_Date = today;
                        DbContext.Entry(obj).State = EntityState.Modified;
                        DbContext.SaveChanges();

                        TempData["SuccessMessage"] = "Worksheet deleted successfully.";
                    }
                    return Redirect(returnurl);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View("ViewStudyNotes");
        }
        #endregion

        #region ...........Teacher PPT Upload..............
        [HttpGet]
        public ActionResult ViewTeacherppt(int? board, int? cls, int? subject, int? chapter)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Teacher PPT";
            var res = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                       join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on a.Board_Id equals b.Board_Id

                       join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on b.Class_Id equals c.Class_Id
                       join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on c.Subject_Id equals d.Subject_Id
                       join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on d.Chapter_Id equals f.Chapter_Id
                       where f.Class_PPT != null
                       select new DigiChampsModel.DigiChampsModuleModel
                       {
                           Module_ID = f.Module_ID,
                           Module_Name = f.Module_Name,
                           Chapter_Id = f.Chapter_Id,
                           Chapter = d.Chapter,
                           Subject_Id = d.Subject_Id,
                           Subject = c.Subject,
                           Board_Id = b.Board_Id,
                           Board_Name = a.Board_Name,
                           Class_Id = c.Class_Id,
                           Class_Name = b.Class_Name,
                           Module_Desc = f.Module_Desc,
                           Module_Content = f.Module_Content,
                           Module_Content_Name = f.Module_Content_Name,
                           Module_video = f.Module_video,
                           Is_Free = f.Is_Free,
                           Question_PDF_Name = f.Question_PDF_Name,
                           Upload_PDF = f.Module_Content_Name,
                           Video_Type = f.Video_Type,
                           Class_PPT = f.Class_PPT,
                           Class_PPT_Name = f.Class_PPT_Name,
                           NCERT_PDF = f.NCERT_PDF,
                           NCERT_PDF_Name = f.NCERT_PDF_Name
                       }).OrderByDescending(x => x.Module_ID).ToList();
            ViewBag.board = 0;
            ViewBag.cls = 0;
            ViewBag.subject = 0;
            ViewBag.chapter = 0;
            if (board != null)
            {
                res = res.Where(a => a.Board_Id == board).ToList();
                ViewBag.board = board;
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_Id == cls).ToList();
                ViewBag.cls = cls;
            }
            if (subject != null)
            {
                res = res.Where(a => a.Subject_Id == subject).ToList();
                ViewBag.subject = subject;
            }
            if (chapter != null)
            {
                res = res.Where(a => a.Chapter_Id == chapter).ToList();
                ViewBag.chapter = chapter;
            }
            // ViewBag.moduledata = res;
            return View(res);
        }
        [HttpGet]
        public ActionResult CreateOrEditTeacherppt(string id, string returnurl)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id1 = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id1 && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Teacher PPT";
            ViewBag.returnurl = returnurl;

            ViewBag.upload_data = "";//otp_data;
            ViewBag.board = new SelectList(DbContext.tbl_DC_Board.Where(b => b.Is_Active == true && b.Is_Deleted == false), "Board_Id", "Board_Name");
            if (id != "" && id != null)
            {
                UInt32 moduleid;
                if (UInt32.TryParse(id, out moduleid) == true)
                {
                    int mid = Convert.ToInt32(moduleid);
                    DigiChampsModel.DigiChampsModuleModel obj = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                 join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on a.Board_Id equals b.Board_Id
                                                                 join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on b.Class_Id equals c.Class_Id
                                                                 join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on c.Subject_Id equals d.Subject_Id
                                                                 join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on d.Chapter_Id equals f.Chapter_Id
                                                                 select new DigiChampsModel.DigiChampsModuleModel
                                                                 {
                                                                     Board_Id = a.Board_Id,
                                                                     Class_Id = b.Class_Id,
                                                                     Subject_Id = c.Subject_Id,
                                                                     Chapter_Id = d.Chapter_Id,
                                                                     Module_ID = f.Module_ID,
                                                                     Module_Name = f.Module_Name,
                                                                     Module_Desc = f.Module_Desc,
                                                                     Module_Content = f.Module_Content,
                                                                     Module_video = f.Module_video,
                                                                     Is_Free = f.Is_Free,
                                                                     Validity = f.Validity,
                                                                     Question_PDF = f.Question_PDF,
                                                                     Is_Free_Test = f.Is_Free_Test,
                                                                     No_Question = f.No_Of_Question,
                                                                     Upload_PDF = f.Module_Content_Name,
                                                                     Question_PDF_Name = f.Question_PDF_Name,
                                                                     Video_Type = f.Video_Type,
                                                                     NCERT_PDF = f.NCERT_PDF,
                                                                     NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                                     Class_PPT = f.Class_PPT,
                                                                     Class_PPT_Name = f.Class_PPT_Name,
                                                                     videoid = f.Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + f.Module_video) : f.Module_video
                                                                 }).Where(x => x.Module_ID == mid).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.classid = obj.Class_Id;
                        ViewBag.subid = obj.Subject_Id;
                        ViewBag.chapterid = obj.Chapter_Id;
                        ViewBag.Modelvideo = obj.Module_video;
                        ViewBag.modelcontent = obj.Module_Content;
                        ViewBag.qstnpdf = obj.Question_PDF;
                        ViewBag.ncertpdf = obj.NCERT_PDF;
                        ViewBag.classppt = obj.Class_PPT;
                        return View(obj);
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Invalid module details.";
                    }
                }
                else
                {
                    TempData["WarningMessage"] = "Invalid module details.";
                }
            }

            return View();
        }
        [HttpPost]
        public ActionResult SaveOrUpdateTeacherppt(DigiChampsModel.DigiChampsModuleModel obj,
            HttpPostedFileBase Class_PPT,
            string returnurl)
        {
            //Menupermission();
            ViewBag.Breadcrumb = "Teacher PPT";
            string msg = string.Empty;
            try
            {
                ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
                if (ModelState.IsValid)
                {
                    //if (obj.Module_Name.Trim() != "")
                    //{
                    if (obj.Module_ID == null)
                    {
                        //var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Class_PPT != null && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        //if (module != null)
                        //{
                        //    TempData["WarningMessage"] = "Teacher PPT already exists.";
                        //}
                        //else
                        //{
                        tbl_DC_Module obj1 = new tbl_DC_Module();
                        obj1.Module_Name = obj.Module_Name;
                        obj1.Module_Desc = obj.Module_Desc;
                        obj1.Board_Id = obj.Board_Id;
                        obj1.Class_Id = obj.Class_Id;
                        obj1.Subject_Id = obj.Subject_Id;
                        obj1.Chapter_Id = obj.Chapter_Id;
                        obj1.Is_Free = true;
                        if (obj1.Is_Free == true)
                        {
                            obj1.Validity = obj.Validity;
                        }
                        if (Class_PPT != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(Class_PPT.FileName.
                                Replace(Class_PPT.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Module/PPT/"), fileName);
                            Class_PPT.SaveAs(path);
                            obj1.Class_PPT = fileName;
                        }
                        obj1.Class_PPT_Name = obj.Class_PPT_Name;
                        string extramsg = "";
                        //if (obj.Is_Free_Test == true)
                        //{
                        //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id ==
                        //        obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                        //    if (iftest_chap == null)
                        //    {
                        obj1.Is_Free_Test = true;
                        //    }
                        //    else
                        //    {
                        //        obj1.Is_Free_Test = false;
                        //        extramsg = " Each chapter can only contain single free test.";
                        //    }
                        //}
                        obj1.Inserted_By = HttpContext.User.Identity.Name;
                        obj1.Is_Active = true;
                        obj1.Is_Deleted = false;
                        DbContext.tbl_DC_Module.Add(obj1);
                        DbContext.SaveChanges();
                        try
                        {
                            var deciceid = DbContext.tbl_DC_Registration.
                                Where(x => x.Device_id != null && !x.Device_id.Equals("null")
                                    && obj1.Class_Id == obj.Class_Id).Select(x => x.Device_id).ToList();
                            // var regid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Class_ID == obj.Class_Id && x.Regd_ID != null).Select(x => x.Regd_ID).ToList();
                            //var deciceid = DbContext.tbl_DC_Registration.Where
                            //    (x => x.Device_id == "ePEyVbqUrBA:APA91bGzpOkF_jPnlBKlQFydnOkcy52DY7Wl-6ks1izlpZDfd4Jq8RN5gQNUquScLd0LzIB91nVwG5NPXxlJazQRXvbTFZ_COIS4KgumbuIsYfN41n5ezij4UKNMGwk9rUfCfxQKWUtZ" ||
                            //     x.Device_id == "c6Ixravhr_Q:APA91bFoGlTItg7aEe2Sz6_80fv9Rjy7IhSDAsaYVOVnh75891RFcJ-dlUGK4V9Pfxbj0GeS_sf_lC3cE-l9C_wzlsGYV2VdrQzmKeGMfTS2dpo1Q_R2Tuv3hSotogJy9yuONkywMcw5"
                            //     ).Select(x => x.Device_id).ToList();

                            //string body = "Body";.Replace("{{video}}", obj1.Module_video)
                            string title = "New Video";
                            string body_header = "New Video#{{Module_ID}}# Upload New Video : Hello {{Name}},! You have New Video.";
                            string body = body_header.ToString().Replace("{{Name}}",
                                obj1.Module_Name).Replace("{{image}}", obj1.Module_Image).Replace("{{Feed_ID}}", obj1.Module_ID.ToString());
                            body = "NVID#{{Module_ID}}#New Video#Don't miss the new video. Check it out";

                            List<List<string>> ids = splitList(deciceid);

                            for (int i = 0; i < ids.Count; i++)
                            {
                                if (ids[i] != null)
                                {
                                    var note = new PushNotiStatusVideo(title, body, obj1.Module_Image, ids[i]);
                                }
                            }
                        }
                        catch (Exception)
                        {

                        }
                        msg = "1";      //"Module details saved successfully.";
                        TempData["SuccessMessage"] = "Class PPT details saved successfully." + extramsg;
                        //}
                    }
                    else
                    {
                        //var module = DbContext.tbl_DC_Module.Where(x => x.Module_ID == obj.Module_ID && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        //if (module == null)
                        //{
                        //    TempData["WarningMessage"] = "Class PPT does not exist.";
                        //}
                        //else
                        //{
                        int mod_id = Convert.ToInt32(obj.Module_ID);
                        tbl_DC_Module obj1 = DbContext.tbl_DC_Module.Where(x => x.Module_ID == mod_id).FirstOrDefault();
                        obj1.Module_Name = obj.Module_Name;
                        obj1.Module_Desc = obj.Module_Desc;
                        obj1.Is_Free = true;
                        obj1.Board_Id = obj.Board_Id;
                        obj1.Class_Id = obj.Class_Id;
                        obj1.Subject_Id = obj.Subject_Id;
                        obj1.Chapter_Id = obj.Chapter_Id;

                        if (obj1.Is_Free == true)
                        {
                            obj1.Validity = obj.Validity;
                        }
                        if (Class_PPT != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(Class_PPT.FileName.
                                Replace(Class_PPT.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Module/PPT/"), fileName);
                            Class_PPT.SaveAs(path);
                            obj1.Class_PPT = fileName;
                        }
                        obj1.Class_PPT_Name = obj.Class_PPT_Name;
                        string extramsg = "";
                        //if (obj.Is_Free_Test == true)
                        //{
                        //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                        //    if (iftest_chap == null)
                        //    {
                        obj1.Is_Free_Test = true;
                        //    }
                        //    else
                        //    {
                        //        obj1.Is_Free_Test = false;
                        //    }
                        //}

                        obj1.Inserted_By = HttpContext.User.Identity.Name;
                        obj1.Is_Active = true;
                        obj1.Is_Deleted = false;
                        obj1.Modified_Date = today;
                        obj1.Modified_By = HttpContext.User.Identity.Name;
                        DbContext.Entry(obj1).State = EntityState.Modified;
                        DbContext.SaveChanges();
                        msg = "11";         // "Module details updated successfully.";
                        TempData["SuccessMessage"] = "Class PPT details updated successfully";
                        TempData["SuccessMessage"] = "Class PPT details updated successfully";
                        //}
                    }
                    //}
                    //else
                    //{
                    //    msg = "00";         // "Please enter module name.";
                    //}
                }
            }
            catch (Exception ex)
            {
                //msg = "Something went wrong.";
                throw ex;
            }
            //return Json(msg, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("ViewModule");
            return Redirect(returnurl);
        }
        [HttpPost]
        public ActionResult MultipleDeleteTeacherppt(int[] chkselect)
        {
            foreach (int a in chkselect)
            {
                tbl_DC_Module md = DbContext.tbl_DC_Module.Where(m => m.Module_ID == a).FirstOrDefault();
                md.Is_Active = false;
                md.Modified_Date = DateTime.Now;
                DbContext.SaveChanges();

            }
            TempData["SuccessMessage"] = "Class PPT details updated successfully";
            return RedirectToAction("ViewTeacherppt", "Admin");
        }
        public ActionResult DeleteTeacherppt(int? id, string returnurl)
        {
            try
            {
                if (id != null)
                {
                    tbl_DC_Module obj = DbContext.tbl_DC_Module.Where(x => x.Module_ID == id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Is_Active = false;
                        obj.Is_Deleted = true;
                        obj.Modified_By = HttpContext.User.Identity.Name;
                        obj.Modified_Date = today;
                        DbContext.Entry(obj).State = EntityState.Modified;
                        DbContext.SaveChanges();

                        TempData["SuccessMessage"] = "Class PPT deleted successfully.";
                    }
                    return Redirect(returnurl);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View("ViewTeacherppt");
        }
        #endregion

        #region ...........NCERT PDF Upload..............
        [HttpGet]
        public ActionResult ViewNCERTpdf(int? board, int? cls, int? subject, int? chapter)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "NCERT PDF";
            var res = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                       join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on a.Board_Id equals b.Board_Id

                       join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on b.Class_Id equals c.Class_Id
                       join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on c.Subject_Id equals d.Subject_Id
                       join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on d.Chapter_Id equals f.Chapter_Id
                       where f.NCERT_PDF != null
                       select new DigiChampsModel.DigiChampsModuleModel
                       {
                           Module_ID = f.Module_ID,
                           Module_Name = f.Module_Name,
                           Chapter_Id = f.Chapter_Id,
                           Chapter = d.Chapter,
                           Subject_Id = d.Subject_Id,
                           Subject = c.Subject,
                           Board_Id = b.Board_Id,
                           Board_Name = a.Board_Name,
                           Class_Id = c.Class_Id,
                           Class_Name = b.Class_Name,
                           Module_Desc = f.Module_Desc,
                           Module_Content = f.Module_Content,
                           Module_Content_Name = f.Module_Content_Name,
                           Module_video = f.Module_video,
                           Is_Free = f.Is_Free,
                           Question_PDF_Name = f.Question_PDF_Name,
                           Upload_PDF = f.Module_Content_Name,
                           Video_Type = f.Video_Type,
                           Class_PPT = f.Class_PPT,
                           Class_PPT_Name = f.Class_PPT_Name,
                           NCERT_PDF = f.NCERT_PDF,
                           NCERT_PDF_Name = f.NCERT_PDF_Name
                       }).OrderByDescending(x => x.Module_ID).ToList();
            ViewBag.board = 0;
            ViewBag.cls = 0;
            ViewBag.subject = 0;
            ViewBag.chapter = 0;
            if (board != null)
            {
                res = res.Where(a => a.Board_Id == board).ToList();
                ViewBag.board = board;
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_Id == cls).ToList();
                ViewBag.cls = cls;
            }
            if (subject != null)
            {
                res = res.Where(a => a.Subject_Id == subject).ToList();
                ViewBag.subject = subject;
            }
            if (chapter != null)
            {
                res = res.Where(a => a.Chapter_Id == chapter).ToList();
                ViewBag.chapter = chapter;
            }
            // ViewBag.moduledata = res;
            return View(res);
        }
        [HttpGet]
        public ActionResult CreateOrEditNCERTpdf(string id, string returnurl)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id1 = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id1 && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "NCERT PDF";
            ViewBag.returnurl = returnurl;

            ViewBag.upload_data = "";//otp_data;
            ViewBag.board = new SelectList(DbContext.tbl_DC_Board.Where(b => b.Is_Active == true && b.Is_Deleted == false), "Board_Id", "Board_Name");
            if (id != "" && id != null)
            {
                UInt32 moduleid;
                if (UInt32.TryParse(id, out moduleid) == true)
                {
                    int mid = Convert.ToInt32(moduleid);
                    DigiChampsModel.DigiChampsModuleModel obj = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                 join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on a.Board_Id equals b.Board_Id
                                                                 join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on b.Class_Id equals c.Class_Id
                                                                 join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on c.Subject_Id equals d.Subject_Id
                                                                 join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on d.Chapter_Id equals f.Chapter_Id
                                                                 select new DigiChampsModel.DigiChampsModuleModel
                                                                 {
                                                                     Board_Id = a.Board_Id,
                                                                     Class_Id = b.Class_Id,
                                                                     Subject_Id = c.Subject_Id,
                                                                     Chapter_Id = d.Chapter_Id,
                                                                     Module_ID = f.Module_ID,
                                                                     Module_Name = f.Module_Name,
                                                                     Module_Desc = f.Module_Desc,
                                                                     Module_Content = f.Module_Content,
                                                                     Module_video = f.Module_video,
                                                                     Is_Free = f.Is_Free,
                                                                     Validity = f.Validity,
                                                                     Question_PDF = f.Question_PDF,
                                                                     Is_Free_Test = f.Is_Free_Test,
                                                                     No_Question = f.No_Of_Question,
                                                                     Upload_PDF = f.Module_Content_Name,
                                                                     Question_PDF_Name = f.Question_PDF_Name,
                                                                     Video_Type = f.Video_Type,
                                                                     NCERT_PDF = f.NCERT_PDF,
                                                                     NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                                     Class_PPT = f.Class_PPT,
                                                                     Class_PPT_Name = f.Class_PPT_Name,
                                                                     videoid = f.Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + f.Module_video) : f.Module_video
                                                                 }).Where(x => x.Module_ID == mid).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.classid = obj.Class_Id;
                        ViewBag.subid = obj.Subject_Id;
                        ViewBag.chapterid = obj.Chapter_Id;
                        ViewBag.Modelvideo = obj.Module_video;
                        ViewBag.modelcontent = obj.Module_Content;
                        ViewBag.qstnpdf = obj.Question_PDF;
                        ViewBag.ncertpdf = obj.NCERT_PDF;
                        ViewBag.classppt = obj.Class_PPT;
                        return View(obj);
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Invalid module details.";
                    }
                }
                else
                {
                    TempData["WarningMessage"] = "Invalid module details.";
                }
            }

            return View();
        }
        [HttpPost]
        public ActionResult SaveOrUpdateNCERTpdf(DigiChampsModel.DigiChampsModuleModel obj,
            HttpPostedFileBase NCERT_PDF,
            string returnurl)
        {
            //Menupermission();
            ViewBag.Breadcrumb = "NCERT PDF";
            string msg = string.Empty;
            try
            {
                ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
                if (ModelState.IsValid)
                {
                    //if (obj.Module_Name.Trim() != "")
                    //{
                    if (obj.Module_ID == null)
                    {
                        var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.NCERT_PDF != null && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (module != null)
                        {
                            TempData["WarningMessage"] = "NCERT PDF already exists.";
                        }
                        else
                        {
                            tbl_DC_Module obj1 = new tbl_DC_Module();
                            obj1.Module_Name = obj.Module_Name;
                            obj1.Module_Desc = obj.Module_Desc;
                            obj1.Board_Id = obj.Board_Id;
                            obj1.Class_Id = obj.Class_Id;
                            obj1.Subject_Id = obj.Subject_Id;
                            obj1.Chapter_Id = obj.Chapter_Id;
                            obj1.Is_Free = true;
                            if (obj1.Is_Free == true)
                            {
                                obj1.Validity = obj.Validity;
                            }
                            if (NCERT_PDF != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(NCERT_PDF.FileName.
                                    Replace(NCERT_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Module/NCERT/"), fileName);
                                NCERT_PDF.SaveAs(path);
                                obj1.NCERT_PDF = fileName;
                            }
                            obj1.NCERT_PDF_Name = obj.NCERT_PDF_Name;
                            string extramsg = "";
                            //if (obj.Is_Free_Test == true)
                            //{
                            //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id ==
                            //        obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                            //    if (iftest_chap == null)
                            //    {
                            obj1.Is_Free_Test = true;
                            //    }
                            //    else
                            //    {
                            //        obj1.Is_Free_Test = false;
                            //        extramsg = " Each chapter can only contain single free test.";
                            //    }
                            //}
                            obj1.Inserted_By = HttpContext.User.Identity.Name;
                            obj1.Is_Active = true;
                            obj1.Is_Deleted = false;
                            DbContext.tbl_DC_Module.Add(obj1);
                            DbContext.SaveChanges();
                            try
                            {
                                var deciceid = DbContext.tbl_DC_Registration.
                                    Where(x => x.Device_id != null && !x.Device_id.Equals("null")
                                        && obj1.Class_Id == obj.Class_Id).Select(x => x.Device_id).ToList();
                                // var regid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Class_ID == obj.Class_Id && x.Regd_ID != null).Select(x => x.Regd_ID).ToList();
                                //var deciceid = DbContext.tbl_DC_Registration.Where
                                //    (x => x.Device_id == "ePEyVbqUrBA:APA91bGzpOkF_jPnlBKlQFydnOkcy52DY7Wl-6ks1izlpZDfd4Jq8RN5gQNUquScLd0LzIB91nVwG5NPXxlJazQRXvbTFZ_COIS4KgumbuIsYfN41n5ezij4UKNMGwk9rUfCfxQKWUtZ" ||
                                //     x.Device_id == "c6Ixravhr_Q:APA91bFoGlTItg7aEe2Sz6_80fv9Rjy7IhSDAsaYVOVnh75891RFcJ-dlUGK4V9Pfxbj0GeS_sf_lC3cE-l9C_wzlsGYV2VdrQzmKeGMfTS2dpo1Q_R2Tuv3hSotogJy9yuONkywMcw5"
                                //     ).Select(x => x.Device_id).ToList();

                                //string body = "Body";.Replace("{{video}}", obj1.Module_video)
                                string title = "New Video";
                                string body_header = "New Video#{{Module_ID}}# Upload New Video : Hello {{Name}},! You have New Video.";
                                string body = body_header.ToString().Replace("{{Name}}",
                                    obj1.Module_Name).Replace("{{image}}", obj1.Module_Image).Replace("{{Feed_ID}}", obj1.Module_ID.ToString());
                                body = "NVID#{{Module_ID}}#New Video#Don't miss the new video. Check it out";

                                List<List<string>> ids = splitList(deciceid);

                                for (int i = 0; i < ids.Count; i++)
                                {
                                    if (ids[i] != null)
                                    {
                                        var note = new PushNotiStatusVideo(title, body, obj1.Module_Image, ids[i]);
                                    }
                                }
                            }
                            catch (Exception)
                            {

                            }
                            msg = "1";      //"Module details saved successfully.";
                            TempData["SuccessMessage"] = "NCERT PDF details saved successfully." + extramsg;
                        }
                    }
                    else
                    {
                        var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.NCERT_PDF != null && x.Module_ID != obj.Module_ID && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (module != null)
                        {
                            TempData["WarningMessage"] = "NCERT PDF already exist.";
                        }
                        else
                        {
                            int mod_id = Convert.ToInt32(obj.Module_ID);
                            tbl_DC_Module obj1 = DbContext.tbl_DC_Module.Where(x => x.Module_ID == mod_id).FirstOrDefault();
                            obj1.Module_Name = obj.Module_Name;
                            obj1.Module_Desc = obj.Module_Desc;
                            obj1.Is_Free = true;
                            obj1.Board_Id = obj.Board_Id;
                            obj1.Class_Id = obj.Class_Id;
                            obj1.Subject_Id = obj.Subject_Id;
                            obj1.Chapter_Id = obj.Chapter_Id;

                            if (obj1.Is_Free == true)
                            {
                                obj1.Validity = obj.Validity;
                            }
                            if (NCERT_PDF != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(NCERT_PDF.FileName.
                                    Replace(NCERT_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Module/NCERT/"), fileName);
                                NCERT_PDF.SaveAs(path);
                                obj1.NCERT_PDF = fileName;
                            }
                            obj1.NCERT_PDF_Name = obj.NCERT_PDF_Name;
                            string extramsg = "";
                            //if (obj.Is_Free_Test == true)
                            //{
                            //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                            //    if (iftest_chap == null)
                            //    {
                            obj1.Is_Free_Test = true;
                            //    }
                            //    else
                            //    {
                            //        obj1.Is_Free_Test = false;
                            //    }
                            //}

                            obj1.Inserted_By = HttpContext.User.Identity.Name;
                            obj1.Is_Active = true;
                            obj1.Is_Deleted = false;
                            obj1.Modified_Date = today;
                            obj1.Modified_By = HttpContext.User.Identity.Name;
                            DbContext.Entry(obj1).State = EntityState.Modified;
                            DbContext.SaveChanges();
                            msg = "11";         // "Module details updated successfully.";
                            TempData["SuccessMessage"] = "NCERT PDF details updated successfully";
                            TempData["SuccessMessage"] = "NCERT PDF details updated successfully";
                        }
                    }
                    //}
                    //else
                    //{
                    //    msg = "00";         // "Please enter module name.";
                    //}
                }
            }
            catch (Exception ex)
            {
                //msg = "Something went wrong.";
                throw ex;
            }
            //return Json(msg, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("ViewModule");
            return Redirect(returnurl);
        }
        [HttpPost]
        public ActionResult MultipleDeleteNCERTpdf(int[] chkselect)
        {
            foreach (int a in chkselect)
            {
                tbl_DC_Module md = DbContext.tbl_DC_Module.Where(m => m.Module_ID == a).FirstOrDefault();
                md.Is_Active = false;
                md.Modified_Date = DateTime.Now;
                DbContext.SaveChanges();

            }
            TempData["SuccessMessage"] = "NCERT PDF details deleted successfully";
            return RedirectToAction("ViewNCERTpdf", "Admin");
        }
        public ActionResult DeleteNCERTpdf(int? id, string returnurl)
        {
            try
            {
                if (id != null)
                {
                    tbl_DC_Module obj = DbContext.tbl_DC_Module.Where(x => x.Module_ID == id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Is_Active = false;
                        obj.Is_Deleted = true;
                        obj.Modified_By = HttpContext.User.Identity.Name;
                        obj.Modified_Date = today;
                        DbContext.Entry(obj).State = EntityState.Modified;
                        DbContext.SaveChanges();

                        TempData["SuccessMessage"] = "NCERT PDF deleted successfully.";
                    }
                    return Redirect(returnurl);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View("ViewNCERTpdf");
        }
        #endregion

        #region ----------------------------------------- Video upload --------------------------------------------
        [HttpGet]
        public ActionResult ViewVideomodule(int? board, int? cls, int? subject, int? chapter)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Video";
            var res = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                       join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on a.Board_Id equals b.Board_Id

                       join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on b.Class_Id equals c.Class_Id
                       join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on c.Subject_Id equals d.Subject_Id
                       join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on d.Chapter_Id equals f.Chapter_Id
                       where f.Module_video != null
                       select new DigiChampsModel.DigiChampsModuleModel
                       {
                           Module_ID = f.Module_ID,
                           Module_Name = f.Module_Name,
                           Chapter_Id = f.Chapter_Id,
                           Chapter = d.Chapter,
                           Subject_Id = d.Subject_Id,
                           Subject = c.Subject,
                           Board_Id = b.Board_Id,
                           Board_Name = a.Board_Name,
                           Class_Id = c.Class_Id,
                           Class_Name = b.Class_Name,
                           Module_Desc = f.Module_Desc,
                           Module_Content = f.Module_Content,
                           Module_video = f.Module_video,
                           Is_Free = f.Is_Free,
                           Question_PDF_Name = f.Question_PDF_Name,
                           Upload_PDF = f.Module_Content_Name,
                           Video_Type = f.Video_Type,
                           Class_PPT = f.Class_PPT,
                           Class_PPT_Name = f.Class_PPT_Name,
                           NCERT_PDF = f.NCERT_PDF,
                           NCERT_PDF_Name = f.NCERT_PDF_Name
                       }).OrderByDescending(x => x.Module_ID).ToList();
            ViewBag.board = 0;
            ViewBag.cls = 0;
            ViewBag.subject = 0;
            ViewBag.chapter = 0;
            if (board != null)
            {
                res = res.Where(a => a.Board_Id == board).ToList();
                ViewBag.board = board;
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_Id == cls).ToList();
                ViewBag.cls = cls;
            }
            if (subject != null)
            {
                res = res.Where(a => a.Subject_Id == subject).ToList();
                ViewBag.subject = subject;
            }
            if (chapter != null)
            {
                res = res.Where(a => a.Chapter_Id == chapter).ToList();
                ViewBag.chapter = chapter;
            }
            //ViewBag.moduledata = res;
            return View(res);
        }
        [HttpGet]
        public ActionResult CreateVideomodule(string id, string returnurl)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id1 = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id1 && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Video";
            ViewBag.returnurl = returnurl;


            //string new_video_title = "VIDEO_TITLE";          // This should be obtained from DB
            //string api_secret = "d87d725cc2f9c4d0ae4fbf956e797089bdbcac7929143d478e9c60bb0d98629c";
            //string uri = "https://api.vdocipher.com/v2/uploadPolicy/";
            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            //request.Method = "POST";
            //request.ContentType = "application/x-www-form-urlencoded";
            //using (StreamWriter writer = new StreamWriter(request.GetRequestStream(), Encoding.ASCII))
            //{
            //    writer.Write("clientSecretKey=" + api_secret + "&title=" + new_video_title);
            //}
            //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //dynamic otp_data;
            //using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            //{
            //    string json_otp = reader.ReadToEnd();
            //    otp_data = JObject.Parse(json_otp);
            //}
            ViewBag.upload_data = "";//otp_data;
            ViewBag.board = new SelectList(DbContext.tbl_DC_Board.Where(b => b.Is_Active == true && b.Is_Deleted == false), "Board_Id", "Board_Name");
            if (id != "" && id != null)
            {
                UInt32 moduleid;
                if (UInt32.TryParse(id, out moduleid) == true)
                {
                    int mid = Convert.ToInt32(moduleid);
                    DigiChampsModel.DigiChampsModuleModel obj = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                 join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on a.Board_Id equals b.Board_Id
                                                                 join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on b.Class_Id equals c.Class_Id
                                                                 join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on c.Subject_Id equals d.Subject_Id
                                                                 join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on d.Chapter_Id equals f.Chapter_Id
                                                                 select new DigiChampsModel.DigiChampsModuleModel
                                                                 {
                                                                     Board_Id = a.Board_Id,
                                                                     Class_Id = b.Class_Id,
                                                                     Subject_Id = c.Subject_Id,
                                                                     Chapter_Id = d.Chapter_Id,
                                                                     Module_ID = f.Module_ID,
                                                                     Module_Name = f.Module_Name,
                                                                     Module_Desc = f.Module_Desc,
                                                                     Module_Content = f.Module_Content,
                                                                     Module_video = f.Module_video,
                                                                     Is_Free = f.Is_Free,
                                                                     Validity = f.Validity,
                                                                     Question_PDF = f.Question_PDF,
                                                                     Is_Free_Test = f.Is_Free_Test,
                                                                     No_Question = f.No_Of_Question,
                                                                     Upload_PDF = f.Module_Content_Name,
                                                                     Question_PDF_Name = f.Question_PDF_Name,
                                                                     Video_Type = f.Video_Type,
                                                                     NCERT_PDF = f.NCERT_PDF,
                                                                     NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                                     Class_PPT = f.Class_PPT,
                                                                     Class_PPT_Name = f.Class_PPT_Name,
                                                                     videoid = f.Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + f.Module_video) : f.Module_video
                                                                 }).Where(x => x.Module_ID == mid).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.classid = obj.Class_Id;
                        ViewBag.subid = obj.Subject_Id;
                        ViewBag.chapterid = obj.Chapter_Id;
                        ViewBag.Modelvideo = obj.Module_video;
                        ViewBag.modelcontent = obj.Module_Content;
                        ViewBag.qstnpdf = obj.Question_PDF;
                        ViewBag.ncertpdf = obj.NCERT_PDF;
                        ViewBag.classppt = obj.Class_PPT;
                        return View(obj);
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Invalid module details.";
                    }
                }
                else
                {
                    TempData["WarningMessage"] = "Invalid module details.";
                }
            }

            return View();
        }

        // HttpPostedFileBase RegImage3,
        //string key, string XAmzCredential, 
        //   string XAmzAlgorithm, string XAmzDate, string Policy,
        //   string XAmzSignature, string success_action_status,
        //   string success_action_redirect, string url,
        [HttpPost]
        public ActionResult MultipleDeleteVideomodule(int[] chkselect)
        {
            foreach (int a in chkselect)
            {
                tbl_DC_Module md = DbContext.tbl_DC_Module.Where(m => m.Module_ID == a).FirstOrDefault();
                md.Is_Active = false;
                DbContext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Video deleted successfully.";
            return RedirectToAction("ViewVideomodule", "Admin");
        }
        [HttpPost]
        public ActionResult CreateVideomodule(DigiChampsModel.DigiChampsModuleModel obj,
            string returnurl,
            string videoid)
        {

            //Menupermission();
            ViewBag.Breadcrumb = "Video";
            string msg = string.Empty;





            try
            {
                ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
                if (ModelState.IsValid)
                {
                    if (obj.Module_Name.Trim() != "")
                    {
                        if (obj.Module_ID == null)
                        {
                            var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Module_video != null && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                            if (module != null)
                            {
                                TempData["WarningMessage"] = "Module Video already exists.";
                            }
                            else
                            {
                                tbl_DC_Module obj1 = new tbl_DC_Module();
                                obj1.Module_Name = obj.Module_Name;
                                obj1.Module_Desc = obj.Module_Desc;
                                //if (RegImage3 != null)
                                //{
                                //    int filelength = RegImage3.ContentLength;
                                //    string guid = Guid.NewGuid().ToString();
                                //    string path = string.Empty;
                                //    var fileName = Path.GetFileName(RegImage3.FileName.Replace(RegImage3.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                //    path = Path.Combine(Server.MapPath("../Module/Video/"), fileName);
                                //    RegImage3.SaveAs(path);
                                //    Program pr = new Program();
                                //    string retun_msg = pr.Programs(path, RegImage3.InputStream, filelength);
                                //    XmlDocument doc = new XmlDocument();
                                //    doc.LoadXml(retun_msg);
                                //    var element = ((XmlElement)doc.GetElementsByTagName("video")[0]); //null
                                //    var video_key = element.GetAttribute("key"); //cannot get values
                                //    obj1.Module_video = video_key;
                                //    //Delete the file from local server
                                //    if (System.IO.File.Exists(Server.MapPath("../Module/Video/" + fileName)))
                                //    {
                                //        System.IO.File.Delete(Server.MapPath("../Module/Video/" + fileName));
                                //    }
                                //}
                                obj1.Video_Type = obj.Video_Type;
                                if (videoid != null)
                                {
                                    //    int filelength = RegImage3.ContentLength;
                                    //    string guid = Guid.NewGuid().ToString();
                                    //    string path = string.Empty;
                                    //    var fileName = Path.GetFileName(
                                    //        RegImage3.FileName.Replace(RegImage3.FileName.Split('.').FirstOrDefault().ToString(),
                                    //        guid.ToString()));
                                    //    Vdocipher vdcip = new Vdocipher();
                                    //    string response= vdcip.videoupload(url, key, XAmzCredential, XAmzAlgorithm, XAmzDate, Policy, XAmzSignature, success_action_status, success_action_redirect, RegImage3.InputStream, RegImage3.FileName);

                                    if (obj.Video_Type == "Y")
                                    {
                                        string imgLink = "http://img.youtube.com/vi/";
                                        string[] temp = obj.videoid.Split(new char[] { '=', '&' });
                                        string video__id = temp[0].ToString();
                                        if (video__id.Split('.')[0].ToString() == "https://youtu")
                                        {
                                            video__id = temp[0].Split('.')[1].ToString().Split('/')[1].ToString();
                                        }
                                        else
                                        {
                                            video__id = temp[1];
                                        }
                                        obj1.Module_Image = imgLink + video__id + "/2.jpg";
                                        obj1.Module_video = video__id;
                                    }
                                    else
                                    {
                                        obj1.Module_video = videoid;
                                    }
                                }

                                obj1.Board_Id = obj.Board_Id;
                                obj1.Class_Id = obj.Class_Id;
                                obj1.Subject_Id = obj.Subject_Id;
                                obj1.Chapter_Id = obj.Chapter_Id;
                                obj1.Is_Free = true;
                                if (obj1.Is_Free == true)
                                {
                                    obj1.Validity = obj.Validity;
                                }
                                //if (Question_PDF != null)
                                //{
                                //    string guid = Guid.NewGuid().ToString();
                                //    string path = string.Empty;
                                //    var fileName = Path.GetFileName(Question_PDF.FileName.Replace(Question_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                //    path = Path.Combine(Server.MapPath("~/Module/Question_PDF/"), fileName);
                                //    Question_PDF.SaveAs(path);
                                //    obj1.Question_PDF = fileName;
                                //    obj1.No_Of_Question = obj.No_Question;
                                //}
                                string extramsg = "";
                                //if (obj.Is_Free_Test == true)
                                //{
                                //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id ==
                                //        obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                                //    if (iftest_chap == null)
                                //    {
                                obj1.Is_Free_Test = true;
                                //    }
                                //    else
                                //    {
                                //        obj1.Is_Free_Test = false;
                                //        extramsg = " Each chapter can only contain single free test.";
                                //    }
                                //}
                                obj1.Inserted_By = HttpContext.User.Identity.Name;
                                obj1.Is_Active = true;
                                obj1.Is_Deleted = false;
                                DbContext.tbl_DC_Module.Add(obj1);
                                DbContext.SaveChanges();
                                try
                                {
                                    var deciceid = DbContext.tbl_DC_Registration.
                                        Where(x => x.Device_id != null && !x.Device_id.Equals("null")
                                            && obj1.Class_Id == obj.Class_Id).Select(x => x.Device_id).ToList();
                                    // var regid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Class_ID == obj.Class_Id && x.Regd_ID != null).Select(x => x.Regd_ID).ToList();
                                    //var deciceid = DbContext.tbl_DC_Registration.Where
                                    //    (x => x.Device_id == "ePEyVbqUrBA:APA91bGzpOkF_jPnlBKlQFydnOkcy52DY7Wl-6ks1izlpZDfd4Jq8RN5gQNUquScLd0LzIB91nVwG5NPXxlJazQRXvbTFZ_COIS4KgumbuIsYfN41n5ezij4UKNMGwk9rUfCfxQKWUtZ" ||
                                    //     x.Device_id == "c6Ixravhr_Q:APA91bFoGlTItg7aEe2Sz6_80fv9Rjy7IhSDAsaYVOVnh75891RFcJ-dlUGK4V9Pfxbj0GeS_sf_lC3cE-l9C_wzlsGYV2VdrQzmKeGMfTS2dpo1Q_R2Tuv3hSotogJy9yuONkywMcw5"
                                    //     ).Select(x => x.Device_id).ToList();

                                    //string body = "Body";.Replace("{{video}}", obj1.Module_video)
                                    string title = "New Video";
                                    string body_header = "New Video#{{Module_ID}}# Upload New Video : Hello {{Name}},! You have New Video.";
                                    string body = body_header.ToString().Replace("{{Name}}",
                                        obj1.Module_Name).Replace("{{image}}", obj1.Module_Image).Replace("{{Feed_ID}}", obj1.Module_ID.ToString());
                                    body = "NVID#{{Module_ID}}#New Video#Don't miss the new video. Check it out";

                                    List<List<string>> ids = splitList(deciceid);

                                    for (int i = 0; i < ids.Count; i++)
                                    {
                                        if (ids[i] != null)
                                        {
                                            var note = new PushNotiStatusVideo(title, body, obj1.Module_Image, ids[i]);
                                        }
                                    }
                                }
                                catch (Exception)
                                {

                                }
                                msg = "1";      //"Module details saved successfully.";
                                TempData["SuccessMessage"] = "Video details saved successfully." + extramsg;
                            }
                        }
                        else
                        {
                            var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Module_video != null && x.Module_ID != obj.Module_ID && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                            if (module != null)
                            {
                                TempData["WarningMessage"] = "Module video already exist.";
                            }
                            else
                            {
                                int mod_id = Convert.ToInt32(obj.Module_ID);
                                tbl_DC_Module obj1 = DbContext.tbl_DC_Module.Where(x => x.Module_ID == mod_id).FirstOrDefault();
                                obj1.Module_Name = obj.Module_Name;
                                obj1.Module_Desc = obj.Module_Desc;
                                obj1.Is_Free = obj.Is_Free;
                                //if (RegImage3 != null)
                                //{
                                //    int filelength = RegImage3.ContentLength;
                                //    string guid = Guid.NewGuid().ToString();
                                //    string path = string.Empty;
                                //    var fileName = Path.GetFileName(RegImage3.FileName.Replace(RegImage3.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                //    path = Path.Combine(Server.MapPath("~/Module/Video/"), fileName);
                                //    RegImage3.SaveAs(path);
                                //    Program pr = new Program();
                                //    string retun_msg = pr.Programs(path, RegImage3.InputStream, filelength);
                                //    XmlDocument doc = new XmlDocument();
                                //    doc.LoadXml(retun_msg);
                                //    var element = ((XmlElement)doc.GetElementsByTagName("video")[0]); //null
                                //    var video_key = element.GetAttribute("key"); //cannot get values
                                //    obj1.Module_video = video_key;
                                //    //Delete the file from local server
                                //    if (System.IO.File.Exists(path))
                                //    {
                                //        System.IO.File.Delete(path);
                                //    }
                                //}

                                // if (RegImage3 != null)
                                //{
                                //    int filelength = RegImage3.ContentLength;
                                //    string guid = Guid.NewGuid().ToString();
                                //    string path = string.Empty;
                                //    var fileName = Path.GetFileName(RegImage3.FileName.Replace(RegImage3.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                //    Vdocipher vdcip = new Vdocipher();
                                //     //vdcip.videoupload(url, key, XAmzCredential, XAmzAlgorithm, XAmzDate, Policy, XAmzSignature, success_action_status, success_action_redirect, RegImage3.InputStream, RegImage3.FileName);

                                obj1.Video_Type = obj.Video_Type;
                                if (obj.Video_Type == "Y")
                                {
                                    string imgLink = "http://img.youtube.com/vi/";
                                    string[] temp = obj.videoid.Split(new char[] { '=', '&' });
                                    string video__id = temp[0].ToString();
                                    if (video__id.Split('.')[0].ToString() == "https://youtu")
                                    {
                                        video__id = temp[0].Split('.')[1].ToString().Split('/')[1].ToString();
                                    }
                                    else
                                    {
                                        video__id = temp[1];
                                    }
                                    obj1.Module_Image = imgLink + video__id + "/2.jpg";
                                    obj1.Module_video = video__id;
                                }
                                else
                                {
                                    obj1.Module_video = videoid;
                                }
                                if (obj.Video_Type == "V")
                                {
                                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://dev.vdocipher.com/api/videos?q=" + videoid);

                                    request.PreAuthenticate = true;
                                    request.Headers.Add("Authorization", "Apisecret d87d725cc2f9c4d0ae4fbf956e797089bdbcac7929143d478e9c60bb0d98629c");
                                    request.Accept = "application/json";
                                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                                    Console.WriteLine("Content length is {0}", response.ContentLength);
                                    Console.WriteLine("Content type is {0}", response.ContentType);

                                    // Get the stream associated with the response.
                                    Stream receiveStream = response.GetResponseStream();

                                    // Pipes the stream to a higher level stream reader with the required encoding format. 
                                    StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                                    //Console.WriteLine("Response stream received.");
                                    var resp = readStream.ReadToEnd();



                                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                                    DigiChamps.Controllers.AdminController.VideoData routes_list =
                                            json_serializer.Deserialize<DigiChamps.Controllers.AdminController.VideoData>(resp);
                                    // Console.WriteLine(readStream.ReadToEnd());
                                    response.Close();
                                    readStream.Close();
                                    if (routes_list != null && routes_list.rows != null && routes_list.rows.Count > 0)
                                    {
                                        obj1.Module_video = routes_list.rows[0].id;
                                        obj1.Module_Image = routes_list.rows[0].poster;

                                    }
                                    else
                                    {
                                        return null;
                                    }
                                }


                                //}

                                obj1.Board_Id = obj.Board_Id;
                                obj1.Class_Id = obj.Class_Id;
                                obj1.Subject_Id = obj.Subject_Id;
                                obj1.Chapter_Id = obj.Chapter_Id;

                                if (obj1.Is_Free == true)
                                {
                                    obj1.Validity = obj.Validity;
                                }

                                //if (obj.Is_Free_Test == true)
                                //{
                                //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                                //    if (iftest_chap == null)
                                //    {
                                obj1.Is_Free_Test = true;
                                //    }
                                //    else
                                //    {
                                //        obj1.Is_Free_Test = false;
                                //    }
                                //}

                                obj1.Inserted_By = HttpContext.User.Identity.Name;
                                obj1.Is_Active = true;
                                obj1.Is_Deleted = false;
                                obj1.Modified_Date = today;
                                DbContext.Entry(obj1).State = EntityState.Modified;
                                DbContext.SaveChanges();
                                msg = "11";         // "Module details updated successfully.";
                                TempData["SuccessMessage"] = "Video details updated successfully";
                            }
                        }
                    }
                    else
                    {
                        msg = "00";         // "Please enter module name.";
                    }
                }
            }
            catch (Exception ex)
            {
                //msg = "Something went wrong.";
                throw ex;
            }
            //return Json(msg, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("ViewModule");
            return Redirect(returnurl);
        }

        public ActionResult DeleteVideomodule(int? id, string returnurl)
        {
            try
            {
                if (id != null)
                {
                    tbl_DC_Module obj = DbContext.tbl_DC_Module.Where(x => x.Module_ID == id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Is_Active = false;
                        obj.Is_Deleted = true;
                        obj.Modified_By = HttpContext.User.Identity.Name;
                        obj.Modified_Date = today;
                        DbContext.Entry(obj).State = EntityState.Modified;
                        DbContext.SaveChanges();

                        BotR.API.BotRAPI ab = new BotR.API.BotRAPI("rhhqK8sD", "M8sVGyMY0tpIftRBJ2PPVuzz");
                        if (obj.Module_video != null)
                        {
                            NameValueCollection shw = new NameValueCollection(){
                                                                                    {"video_key",obj.Module_video}
                                                                               };
                            string xml1 = ab.Call("/videos/delete", shw);
                        }
                        TempData["SuccessMessage"] = "Video deleted successfully.";
                    }
                    return Redirect(returnurl);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View("ViewVideomodule");
        }
        #endregion
        #region------------------Question Bank---------------------

        public ActionResult QuestionBankList(int? board, int? cls, int? subject, int? chapter)
        {

            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Question Bank PDF";
            var res = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                       join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on a.Board_Id equals b.Board_Id

                       join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on b.Class_Id equals c.Class_Id
                       join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on c.Subject_Id equals d.Subject_Id
                       join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on d.Chapter_Id equals f.Chapter_Id
                       where f.Question_Bank_PDF != null
                       select new DigiChampsModel.DigiChampsModuleModel
                       {
                           Module_ID = f.Module_ID,
                           Module_Name = f.Module_Name,
                           Chapter_Id = f.Chapter_Id,
                           Chapter = d.Chapter,
                           Subject_Id = d.Subject_Id,
                           Subject = c.Subject,
                           Board_Id = b.Board_Id,
                           Board_Name = a.Board_Name,
                           Class_Id = c.Class_Id,
                           Class_Name = b.Class_Name,
                           Module_Desc = f.Module_Desc,
                           Module_Content = f.Module_Content,
                           Module_Content_Name = f.Module_Content_Name,
                           Module_video = f.Module_video,
                           Is_Free = f.Is_Free,
                           Question_PDF_Name = f.Question_PDF_Name,
                           Upload_PDF = f.Module_Content_Name,
                           Video_Type = f.Video_Type,
                           Class_PPT = f.Class_PPT,
                           Class_PPT_Name = f.Class_PPT_Name,
                           NCERT_PDF = f.NCERT_PDF,
                           NCERT_PDF_Name = f.NCERT_PDF_Name,
                           Question_Bank_PDF = f.Question_Bank_PDF,
                           Question_Bank_PDF_Name = f.Question_Bank_PDF_Name
                       }).OrderByDescending(x => x.Module_ID).ToList();
            ViewBag.board = 0;
            ViewBag.cls = 0;
            ViewBag.subject = 0;
            ViewBag.chapter = 0;
            if (board != null)
            {
                res = res.Where(a => a.Board_Id == board).ToList();
                ViewBag.board = board;
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_Id == cls).ToList();
                ViewBag.cls = cls;
            }
            if (subject != null)
            {
                res = res.Where(a => a.Subject_Id == subject).ToList();
                ViewBag.subject = subject;
            }
            if (chapter != null)
            {
                res = res.Where(a => a.Chapter_Id == chapter).ToList();
                ViewBag.chapter = chapter;
            }
            // ViewBag.moduledata = res;
            return View(res);

        }
        [HttpGet]
        public ActionResult CreateQuestionBank(string id, string returnurl)
        {
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id1 = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id1 && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Question Bank PDF";
            ViewBag.returnurl = returnurl;

            ViewBag.upload_data = "";//otp_data;
            ViewBag.board = new SelectList(DbContext.tbl_DC_Board.Where(b => b.Is_Active == true && b.Is_Deleted == false), "Board_Id", "Board_Name");
            if (id != "" && id != null)
            {
                UInt32 moduleid;
                if (UInt32.TryParse(id, out moduleid) == true)
                {
                    int mid = Convert.ToInt32(moduleid);
                    DigiChampsModel.DigiChampsModuleModel obj = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                 join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on a.Board_Id equals b.Board_Id
                                                                 join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on b.Class_Id equals c.Class_Id
                                                                 join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on c.Subject_Id equals d.Subject_Id
                                                                 join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on d.Chapter_Id equals f.Chapter_Id
                                                                 select new DigiChampsModel.DigiChampsModuleModel
                                                                 {
                                                                     Board_Id = a.Board_Id,
                                                                     Class_Id = b.Class_Id,
                                                                     Subject_Id = c.Subject_Id,
                                                                     Chapter_Id = d.Chapter_Id,
                                                                     Module_ID = f.Module_ID,
                                                                     Module_Name = f.Module_Name,
                                                                     Module_Desc = f.Module_Desc,
                                                                     Module_Content = f.Module_Content,
                                                                     Module_video = f.Module_video,
                                                                     Is_Free = f.Is_Free,
                                                                     Validity = f.Validity,
                                                                     Question_PDF = f.Question_PDF,
                                                                     Is_Free_Test = f.Is_Free_Test,
                                                                     No_Question = f.No_Of_Question,
                                                                     Upload_PDF = f.Module_Content_Name,
                                                                     Question_PDF_Name = f.Question_PDF_Name,
                                                                     Video_Type = f.Video_Type,
                                                                     NCERT_PDF = f.NCERT_PDF,
                                                                     NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                                     Class_PPT = f.Class_PPT,
                                                                     Class_PPT_Name = f.Class_PPT_Name,
                                                                     Question_Bank_PDF = f.Question_Bank_PDF,
                                                                     Question_Bank_PDF_Name = f.Question_Bank_PDF_Name,
                                                                     videoid = f.Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + f.Module_video) : f.Module_video
                                                                 }).Where(x => x.Module_ID == mid).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.classid = obj.Class_Id;
                        ViewBag.subid = obj.Subject_Id;
                        ViewBag.chapterid = obj.Chapter_Id;
                        ViewBag.Modelvideo = obj.Module_video;
                        ViewBag.modelcontent = obj.Module_Content;
                        ViewBag.qstnpdf = obj.Question_PDF;
                        ViewBag.ncertpdf = obj.NCERT_PDF;
                        ViewBag.questionbankpdf = obj.Question_Bank_PDF;
                        ViewBag.classppt = obj.Class_PPT;
                        return View(obj);
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Invalid module details.";
                    }
                }
                else
                {
                    TempData["WarningMessage"] = "Invalid module details.";
                }
            }

            return View();
        }
        [HttpPost]
        public ActionResult SaveQuestionBank(DigiChampsModel.DigiChampsModuleModel obj,
            HttpPostedFileBase Question_Bank_PDF,
            string returnurl)
        {

            ViewBag.Breadcrumb = "Question Bank PDF";
            string msg = string.Empty;
            try
            {
                ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
                if (ModelState.IsValid)
                {
                    //if (obj.Module_Name.Trim() != "")
                    //{
                    if (obj.Module_ID == null)
                    {
                        //var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Question_Bank_PDF != null && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        //if (module != null)
                        //{
                        //    TempData["WarningMessage"] = "Question Bank  PDF already exists.";
                        //}
                        //else
                        //{
                        tbl_DC_Module obj1 = new tbl_DC_Module();
                        obj1.Module_Name = obj.Module_Name;
                        obj1.Module_Desc = obj.Module_Desc;
                        obj1.Board_Id = obj.Board_Id;
                        obj1.Class_Id = obj.Class_Id;
                        obj1.Subject_Id = obj.Subject_Id;
                        obj1.Chapter_Id = obj.Chapter_Id;
                        obj1.Is_Free = true;
                        if (obj1.Is_Free == true)
                        {
                            obj1.Validity = obj.Validity;
                        }
                        if (Question_Bank_PDF != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(Question_Bank_PDF.FileName.
                                Replace(Question_Bank_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Module/Questionbank/"), fileName);
                            Question_Bank_PDF.SaveAs(path);
                            obj1.Question_Bank_PDF = fileName;
                        }
                        obj1.Question_Bank_PDF_Name = obj.Question_Bank_PDF_Name;
                        string extramsg = "";
                        //if (obj.Is_Free_Test == true)
                        //{
                        //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id ==
                        //        obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                        //    if (iftest_chap == null)
                        //    {
                        obj1.Is_Free_Test = true;
                        //    }
                        //    else
                        //    {
                        //        obj1.Is_Free_Test = false;
                        //        extramsg = " Each chapter can only contain single free test.";
                        //    }
                        //}
                        obj1.Inserted_By = HttpContext.User.Identity.Name;
                        obj1.Is_Active = true;
                        obj1.Is_Deleted = false;
                        DbContext.tbl_DC_Module.Add(obj1);
                        DbContext.SaveChanges();
                        try
                        {
                            var deciceid = DbContext.tbl_DC_Registration.
                                Where(x => x.Device_id != null && !x.Device_id.Equals("null")
                                    && obj1.Class_Id == obj.Class_Id).Select(x => x.Device_id).ToList();
                            // var regid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Class_ID == obj.Class_Id && x.Regd_ID != null).Select(x => x.Regd_ID).ToList();
                            //var deciceid = DbContext.tbl_DC_Registration.Where
                            //    (x => x.Device_id == "ePEyVbqUrBA:APA91bGzpOkF_jPnlBKlQFydnOkcy52DY7Wl-6ks1izlpZDfd4Jq8RN5gQNUquScLd0LzIB91nVwG5NPXxlJazQRXvbTFZ_COIS4KgumbuIsYfN41n5ezij4UKNMGwk9rUfCfxQKWUtZ" ||
                            //     x.Device_id == "c6Ixravhr_Q:APA91bFoGlTItg7aEe2Sz6_80fv9Rjy7IhSDAsaYVOVnh75891RFcJ-dlUGK4V9Pfxbj0GeS_sf_lC3cE-l9C_wzlsGYV2VdrQzmKeGMfTS2dpo1Q_R2Tuv3hSotogJy9yuONkywMcw5"
                            //     ).Select(x => x.Device_id).ToList();

                            //string body = "Body";.Replace("{{video}}", obj1.Module_video)
                            string title = "New Question Bank";
                            string body_header = "New Question Bank#{{Module_ID}}# Upload New Question Bank : Hello {{Name}},! You have New Question Bank.";
                            string body = body_header.ToString().Replace("{{Name}}",
                                obj1.Module_Name).Replace("{{image}}", obj1.Module_Image).Replace("{{Feed_ID}}", obj1.Module_ID.ToString());
                            body = "NVID#{{Module_ID}}#New Question Bank#Don't miss the new Question Bank. Check it out";

                            List<List<string>> ids = splitList(deciceid);

                            for (int i = 0; i < ids.Count; i++)
                            {
                                if (ids[i] != null)
                                {
                                    var note = new PushNotiStatusVideo(title, body, obj1.Module_Image, ids[i]);
                                }
                            }
                        }
                        catch (Exception)
                        {

                        }
                        msg = "1";      //"Module details saved successfully.";
                        TempData["SuccessMessage"] = "Question Bank details saved successfully." + extramsg;
                        //}
                    }
                    else
                    {
                        //var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Question_Bank_PDF != null && x.Module_ID != obj.Module_ID && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        //if (module != null)
                        //{
                        //    TempData["WarningMessage"] = "Question Bank PDF already exist.";
                        //}
                        //else
                        //{
                        int mod_id = Convert.ToInt32(obj.Module_ID);
                        tbl_DC_Module obj1 = DbContext.tbl_DC_Module.Where(x => x.Module_ID == mod_id).FirstOrDefault();
                        obj1.Module_Name = obj.Module_Name;
                        obj1.Module_Desc = obj.Module_Desc;
                        obj1.Is_Free = true;
                        obj1.Board_Id = obj.Board_Id;
                        obj1.Class_Id = obj.Class_Id;
                        obj1.Subject_Id = obj.Subject_Id;
                        obj1.Chapter_Id = obj.Chapter_Id;

                        if (obj1.Is_Free == true)
                        {
                            obj1.Validity = obj.Validity;
                        }
                        if (Question_Bank_PDF != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(Question_Bank_PDF.FileName.
                                Replace(Question_Bank_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Module/Questionbank/"), fileName);
                            Question_Bank_PDF.SaveAs(path);
                            obj1.Question_Bank_PDF = fileName;
                        }
                        obj1.Question_Bank_PDF_Name = obj.Question_Bank_PDF_Name;
                        string extramsg = "";
                        //if (obj.Is_Free_Test == true)
                        //{
                        //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                        //    if (iftest_chap == null)
                        //    {
                        obj1.Is_Free_Test = true;
                        //    }
                        //    else
                        //    {
                        //        obj1.Is_Free_Test = false;
                        //    }
                        //}

                        obj1.Inserted_By = HttpContext.User.Identity.Name;
                        obj1.Is_Active = true;
                        obj1.Is_Deleted = false;
                        obj1.Modified_Date = today;
                        obj1.Modified_By = HttpContext.User.Identity.Name;
                        DbContext.Entry(obj1).State = EntityState.Modified;
                        DbContext.SaveChanges();
                        msg = "11";         // "Module details updated successfully.";
                        TempData["SuccessMessage"] = "Question Bank details updated successfully";
                        TempData["SuccessMessage"] = "Question Bank details updated successfully";
                        //}
                    }
                    //}
                    //else
                    //{
                    //    msg = "00";         // "Please enter module name.";
                    //}
                }
            }
            catch (Exception ex)
            {
                //msg = "Something went wrong.";
                throw ex;
            }
            //return Json(msg, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("ViewModule");
            return Redirect(returnurl);
        }
        public ActionResult MultipleDeleteQuestionBank(int[] chkselect)
        {
            foreach (int a in chkselect)
            {
                tbl_DC_Module md = DbContext.tbl_DC_Module.Where(m => m.Module_ID == a).FirstOrDefault();
                md.Is_Active = false;
                md.Modified_Date = DateTime.Now;
                DbContext.SaveChanges();

            }
            TempData["SuccessMessage"] = "Question Bank details deleted successfully";
            return RedirectToAction("QuestionBankList", "Admin");
        }
        public ActionResult DeleteQuestionBank(int? id, string returnurl)
        {
            try
            {
                if (id != null)
                {
                    tbl_DC_Module obj = DbContext.tbl_DC_Module.Where(x => x.Module_ID == id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Is_Active = false;
                        obj.Is_Deleted = true;
                        obj.Modified_By = HttpContext.User.Identity.Name;
                        obj.Modified_Date = today;
                        DbContext.Entry(obj).State = EntityState.Modified;
                        DbContext.SaveChanges();

                        TempData["SuccessMessage"] = "Question Bank deleted successfully.";
                    }
                    return Redirect(returnurl);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View("QuestionBankList");

        }

        #endregion----------------End Question Bank----------------

        #endregion

        #region doubt List Report
        public ActionResult DoubtListReport(int? board, int? cls, Guid? sec, int? subject, int? chapter, DateTime? fdt, DateTime? tdt, string status)
        {
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            var doubts = DbContext.View_DC_All_Tickets_Details.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
            var boardlistss = assigndet.Select(a => a.Board_Id).ToList();
            var classlist = assigndet.Select(a => a.Class_Id).ToList();
            var sectionlist = assigndet.Select(a => a.SectionId).ToList();
            var subjectlist = assigndet.Select(a => a.Subject_Id).ToList();
            doubts = doubts.Where(a => boardlistss.Contains(a.Board_ID.Value) && classlist.Contains(a.Class_ID.Value) && sectionlist.Contains(a.SectionId) && subjectlist.Contains(a.Subject_ID.Value)).ToList();
            ViewBag.board = board;
            ViewBag.cls = cls;
            ViewBag.sec = sec;
            ViewBag.subject = subject;
            ViewBag.chapter = chapter;
            ViewBag.status = status;
            ViewBag.msg = "";
            ViewBag.fdt = fdt;
            ViewBag.tdt = tdt;
            if (board != null && board != 0)
            {
                doubts = doubts.Where(a => a.Board_ID == board).ToList();
            }
            if (cls != null && cls != 0)
            {
                doubts = doubts.Where(a => a.Class_ID == cls).ToList();
            }
            if (sec != null && sec != Guid.Empty)
            {
                doubts = doubts.Where(a => a.SectionId == sec).ToList();
            }
            if (subject != null && subject != 0)
            {
                doubts = doubts.Where(a => a.Subject_ID == subject).ToList();
            }
            if (chapter != null && chapter != 0)
            {
                doubts = doubts.Where(a => a.Chapter_Id == chapter).ToList();
            }

            if (fdt != null && tdt != null)
            {
                if (fdt > tdt)
                {
                    ViewBag.msg = "From Date must be less than To date";
                }
                else
                {
                    doubts = doubts.Where(a => a.Inserted_Date.Value.Date >= fdt.Value.Date && a.Inserted_Date.Value.Date <= tdt.Value.Date).ToList();
                }
            }
            if (status != null && status != "")
            {
                if (status == "O")
                {
                    doubts = doubts.Where(a => a.Status == "O" && a.Answer == "False").ToList();
                }
                else if (status == "A")
                {
                    doubts = doubts.Where(a => a.Status == "O" && a.Answer == "True").ToList();
                }
                else if (status == "R")
                {
                    doubts = doubts.Where(a => a.Status == "R").ToList();
                }
                else if (status == "C")
                {
                    doubts = doubts.Where(a => a.Status == "C").ToList();
                }
            }
            Session["Doubts_Det"] = doubts;
            return View(doubts);
        }

        public class doubtmodel
        {
            public int Board_ID { get; set; }
            public string BoardName { get; set; }
            public int Class_ID { get; set; }
            public string Class_Name { get; set; }
            
            public Guid SectionId { get; set; }
            public string SectionName { get; set; }
            public string TeacherName { get; set; }
            public int Subject_ID { get; set; }
            public string Subject { get; set; }
            public DateTime? Inserted_Date { get; set; }
            public string Ticket_No { get; set; }

            
            public int Chapter_Id { get; set; }
            public string Chapter { get; set; }
            public long TotalDoubts { get; set; }
            public long PendingDoubts { get; set; }
            public long AnsweredDoubts { get; set; }
            public long ClosedCount { get; set; }
            public long RejectDoubts { get; set; }
            public long SlovedCount { get; set; }
        }
        public ActionResult DoubtDashboardReport(int? board, int? cls, Guid? sec, int? subject, int? chapter, DateTime? fdt, DateTime? tdt)
        {
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            var doubts = DbContext.View_DC_All_Tickets_Details.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
            var boardlistss = assigndet.Select(a => a.Board_Id).ToList();
            var classlist = assigndet.Select(a => a.Class_Id).ToList();
            var sectionlist = assigndet.Select(a => a.SectionId).ToList();
            var subjectlist = assigndet.Select(a => a.Subject_Id).ToList();
            doubts = doubts.Where(a => boardlistss.Contains(a.Board_ID.Value) && classlist.Contains(a.Class_ID.Value) && sectionlist.Contains(a.SectionId) && subjectlist.Contains(a.Subject_ID.Value)).ToList();
            var ticket_dtl = DbContext.tbl_DC_Ticket_Dtl.Where(a => a.Is_Active == true).Select(a => new { a.Ticket_Dtls_ID, a.Ticket_ID }).ToList().Distinct();
            var ticket_thread = DbContext.tbl_DC_Ticket_Thread.Where(a => a.Is_Active == true).Select(a => new { a.Ticket_Dtl_ID, a.Comment_ID }).ToList().Distinct();
           
            ViewBag.board = board;
            ViewBag.cls = cls;
            ViewBag.sec = sec;
            ViewBag.subject = subject;
            ViewBag.chapter = chapter;            
            ViewBag.msg = "";
            ViewBag.fdt = fdt;
            ViewBag.tdt = tdt;
            var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();

            if (board != null && board != 0)
            {
                doubts = doubts.Where(a => a.Board_ID == board).ToList();
            }
            if (cls != null && cls != 0)
            {
                doubts = doubts.Where(a => a.Class_ID == cls).ToList();
            }
            if (sec != null && sec != Guid.Empty)
            {
                doubts = doubts.Where(a => a.SectionId == sec).ToList();
            }
            if (subject != null && subject != 0)
            {
                doubts = doubts.Where(a => a.Subject_ID == subject).ToList();
            }
            if (chapter != null && chapter != 0)
            {
                doubts = doubts.Where(a => a.Chapter_Id == chapter).ToList();
            }

            if (fdt != null && tdt != null)
            {
                if (fdt > tdt)
                {
                    ViewBag.msg = "From Date must be less than To date";
                }
                else
                {
                    doubts = doubts.Where(a => a.Inserted_Date.Value.Date >= fdt.Value.Date && a.Inserted_Date.Value.Date <= tdt.Value.Date).ToList();
                }
            }





            //var res = (from a in doubts
            //           select new doubtmodel
            //           {
            //               Board_ID=a.Board_ID.Value,
            //               BoardName=a.Board_Name,
            //               Class_ID =a.Class_ID.Value,
            //               Class_Name=a.Class_Name,
            //               SectionId =a.SectionId.Value,
            //               Subject_ID=a.Subject_ID.Value,
            //               Inserted_Date=a.Inserted_Date.Value,
            //               Subject =a.Subject,
            //               Chapter=a.Chapter,
            //               Ticket_No=a.Ticket_No,
            //               SectionName= sections.Where(x => x.SectionId == a.SectionId).FirstOrDefault().SectionName,
            //               Chapter_Id =a.Chapter_Id,
            //               TotalDoubts = doubts.ToList().Count(),
            //               RejectDoubts= doubts.Where(d => d.Status == "R").ToList().Count(),
            //               ClosedCount= doubts.Where(d => d.Status == "C").ToList().Count(),
            //               SlovedCount = doubts.Where(d => d.Status == "SOC").ToList().Count(),
            //               AnsweredDoubts = ticket_dtl.Where(z=>z.Ticket_ID==a.Ticket_ID).ToList().Count(),
            //               PendingDoubts= ((doubts.ToList().Count())-(ticket_dtl.Where(z => z.Ticket_ID == a.Ticket_ID).ToList().Count() + doubts.Where(d => d.Status == "R").ToList().Count())),
            //           }
            //           );


            doubtmodel cnt = new doubtmodel();
            cnt.TotalDoubts = doubts.ToList().Count();
            cnt.RejectDoubts = doubts.Where(a => a.Status == "R").ToList().Count();
            cnt.AnsweredDoubts = (from a in doubts
                                  join b in ticket_dtl on a.Ticket_ID equals b.Ticket_ID
                                  where a.Status=="O"
                                  select a).ToList().Count();
            cnt.ClosedCount = doubts.Where(a => a.Status == "C").ToList().Count();
            cnt.SlovedCount = doubts.Where(a => a.Status == "SOC").ToList().Count();
            cnt.PendingDoubts = cnt.TotalDoubts - (cnt.AnsweredDoubts + cnt.RejectDoubts + cnt.ClosedCount + cnt.SlovedCount);
                            
            Session["Doubts_Detdashboard"] = cnt;
            return View(cnt);
        }




        public JsonResult GetTeacherList(int? board, int? cls, Guid? sec, int? subject)
        {
            var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true).ToList();
            if (board != null && board != 0)
            {
                assigndet = assigndet.Where(a => a.Board_Id == board).ToList();
            }
            if (cls != null && cls != 0)
            {
                assigndet = assigndet.Where(a => a.Class_Id == cls).ToList();
            }
            if (sec != null && sec != Guid.Empty)
            {
                assigndet = assigndet.Where(a => a.SectionId == sec).ToList();
            }
            if (subject != null && subject != 0)
            {
                assigndet = assigndet.Where(a => a.Subject_Id == subject).ToList();
            }
            var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
            var res = (from a in assigndet
                       join b in teachers on a.Teacher_ID equals b.TeacherId
                       select b).ToList();
            return Json(teachers, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Exportalldoubtreportdata()
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();
            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Sl. No.");
            validationTable.Columns.Add("Student Name");
            validationTable.Columns.Add("Board");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Section");
            validationTable.Columns.Add("Subject");
            validationTable.Columns.Add("Chapter");
            validationTable.Columns.Add("Ticket No");
            validationTable.Columns.Add("Teacher Name");
            validationTable.Columns.Add("Status");
            validationTable.Columns.Add("Raised On");

            validationTable.TableName = "Doubt_Details";
            List<View_DC_All_Tickets_Details> res = (Session["Doubts_Det"]) as List<View_DC_All_Tickets_Details>;
            int i = 0;
            var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();

            foreach (var gs in res.ToList())
            {
                DataRow dr = validationTable.NewRow();
                dr["Sl. No."] = i + 1;
                dr["Student Name"] = gs.Customer_Name;
                dr["Board"] = gs.Board_Name;
                dr["Class"] = gs.Class_Name;
                dr["Section"] = sections.Where(a => a.SectionId == gs.SectionId).FirstOrDefault().SectionName;
                dr["Subject"] = gs.Subject;
                dr["Chapter"] = gs.Chapter;
                dr["Ticket No"] = gs.Ticket_No;
                dr["Teacher Name"] = (gs.Teach_ID != 0 && gs.Teach_ID != null) ? teachers.Where(a => a.TeacherId == gs.Teach_ID).FirstOrDefault().Name : "";
                dr["Status"] = (gs.Status == "O" && gs.Answer == "False") ? "Pending" : ((gs.Status == "O" && gs.Answer == "True") ? "Answered" : (gs.Status == "R" ? "Rejected" : (gs.Status == "C" ? "Closed" : "")));
                dr["Raised On"] = gs.Inserted_Date.Value.ToString("dd-MM-yyyy hh:mm tt");
                validationTable.Rows.Add(dr);
                i += 1;
            }



            oWB.Worksheets.Add(validationTable);
            //oWB.SaveAs("User_Details.xlsx");
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Doubt_Details.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            TempData["Message"] = "Successfully Downloaded Excel File";
            return RedirectToAction("DoubtListReport", "SchoolReport");
        }
        public class Doubtcls
        {
            public string statusname { get; set; }
            public string TeacherName { get; set; }
            public bool Answers { get; set; }
            public int RegdId { get; set; }
            public int doubtId { get; set; }
            public string studentName { get; set; }
            public string mobile { get; set; }
            public string zoomId { get; set; }
            public string zoomCode { get; set; }
            public string zoomMailId { get; set; }
            public string zoomPassword { get; set; }
            public string tquestion { get; set; }
            public string questionImage { get; set; }
            public string className { get; set; }
            public string sectionName { get; set; }
            public string subjectName { get; set; }
            public string chapterName { get; set; }
            public string raiseDate { get; set; }
            public string ticketNo { get; set; }
            public string status { get; set; }
            public int teacherId { get; set; }
        }
        public class TicketDetailscls
        {
            //public string student_name { get; set; }
            //public string TClass_name { get; set; }
            public Doubtcls doubt { get; set; }

            //public string classData { get; set; }
            //public string status { get; set; }
            //public int TicketId { get; set; }
            //public string ticketno { get; set; }
            //public int? studentId { get; set; }
            public List<tbl_DC_Ticket_Dtl> all_ticket_answer { get; set; }
            public List<tbl_DC_Ticket_Thread> comments { get; set; }
            public string isclosed { get; set; }
            public tbl_DC_Ticket_Dtl check_answer { get; set; }
        }
        public ActionResult ViewDoubtDetails(int id)
        {
            var ticket_qsn = DbContext.View_DC_All_Tickets_Details.Where(x => x.Ticket_ID == id).ToList();
            int reg = ticket_qsn.ToList()[0].Regd_ID;
            var regd = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Regd_ID == reg).FirstOrDefault();
            var regddet = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Regd_ID == reg).FirstOrDefault();

            var Section = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            int sub = ticket_qsn.ToList()[0].Subject_ID.Value;
            var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.SectionId == regd.SectionId && a.Board_Id == regddet.Board_ID && a.Class_Id == regddet.Class_ID && a.Subject_Id == sub).FirstOrDefault();
            var teachers = assigndet == null ? null : DbContext.Tbl_Teacher_Zoom_Det.Where(a => a.TeacherId == assigndet.Teacher_ID).FirstOrDefault();
            var teacherdet = assigndet == null ? null : DbContext.Teachers.Where(a => a.Active == 1 && a.TeacherId == assigndet.Teacher_ID).FirstOrDefault();
            // var cData = ClassName + " / " + "Section- " + Section + " / " + ticket_qsn.Subject + " / " + ticket_qsn.Chapter;
            try
            {
                var checkstudentchat = DbContext.tbl_DC_Ticket_Thread.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false && x.Is_Teacher == false).ToList();
                if (checkstudentchat != null)
                {
                    foreach (var a in checkstudentchat)
                    {
                        a.Is_Taecher_Read = true;
                        a.Modified_By = assigndet.Teacher_ID.ToString();
                        a.Modified_Date = DateTime.Now;
                        DbContext.SaveChanges();
                    }
                }
            }
            catch
            {

            }
            var res = (from a in ticket_qsn
                       select new Doubtcls
                       {
                           chapterName = a.Chapter,
                           className = a.Class_Name,
                           raiseDate = a.Inserted_Date.Value.ToString("dd-MM-yyyy"),
                           RegdId = a.Regd_ID,
                           sectionName = Section.Where(m => m.SectionId == a.SectionId).FirstOrDefault().SectionName,
                           statusname = a.Status == "O" ? (Convert.ToBoolean(a.Answer) == false ? "PENDING" : "ANSWERED") : (a.Status == "C" ? "CLOSED" : "REJECTED"),
                           status = a.Status,
                           studentName = a.Customer_Name,
                           subjectName = a.Subject,
                           ticketNo = a.Ticket_No,
                           doubtId = a.Ticket_ID,
                           mobile = regd.Mobile,
                           zoomCode = teachers == null ? "" : teachers.Zoom_Code,
                           zoomId = teachers == null ? "" : teachers.ZooMId,
                           zoomMailId = teachers == null ? "" : teachers.Zoom_MailId,
                           zoomPassword = teachers == null ? "" : teachers.Zoom_Password,
                           Answers = Convert.ToBoolean(a.Answer),
                           tquestion = a.Question,
                           TeacherName = teacherdet == null ? "" : teacherdet.Name,
                           questionImage = a.Question_Image == null ? "" : "/Images/Qusetionimages/" + a.Question_Image
                       }).FirstOrDefault();
            TicketDetailscls ob = new TicketDetailscls();
            ob.doubt = res;
            var tick = DbContext.tbl_DC_Ticket.Where(a => a.Ticket_ID == res.doubtId).FirstOrDefault();
            List<tbl_DC_Ticket_Thread> threadslist = new List<tbl_DC_Ticket_Thread>();
            tbl_DC_Ticket_Thread th = new tbl_DC_Ticket_Thread();
            th.Is_Teacher = false;
            th.User_Comment = res.tquestion;
            th.R_image = res.questionImage;
            th.Is_Active = true;
            th.Is_Deleted = false;
            th.User_Id = res.RegdId;
            th.User_Comment_Date = ticket_qsn.FirstOrDefault().Inserted_Date;
            th.Ticket_Dtl_ID = 0;
            th.Ticket_ID = res.doubtId;
            th.Is_Audio = tick.Is_Audio;
            th.Is_Taecher_Read = true;
            th.Comment_ID = 0;
            threadslist.Add(th);
            var dtl = (from a in DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList()
                       select new tbl_DC_Ticket_Thread
                       {
                           Is_Teacher = true,
                           User_Comment = a.Answer,
                           R_image = a.Answer_Image != null ? ("/Images/Answerimage/" + a.Answer_Image) : "",
                           Is_Active = a.Is_Active,
                           Is_Deleted = a.Is_Deleted,
                           User_Id = a.Replied_By,
                           User_Comment_Date = a.Replied_Date,
                           Ticket_Dtl_ID = (a.Ticket_Dtls_ID) as Nullable<int>,
                           Ticket_ID = a.Ticket_ID,
                           Is_Audio = a.Is_Audio,
                           Is_Taecher_Read = true,
                           Comment_ID = 0
                       }).ToList();
            threadslist.AddRange(dtl);
            var comments = (from a in DbContext.tbl_DC_Ticket_Thread.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList()
                            select new tbl_DC_Ticket_Thread
                            {
                                Comment_ID = a.Comment_ID,
                                Is_Active = a.Is_Active,
                                Is_Deleted = a.Is_Deleted,
                                Is_Taecher_Read = a.Is_Taecher_Read,
                                Is_Teacher = a.Is_Teacher,
                                Modified_By = a.Modified_By,
                                Modified_Date = a.Modified_Date,
                                R_image = a.R_image != null ? ("/Images/Qusetionimages/" + a.R_image) : "",
                                Ticket_Dtl_ID = a.Ticket_Dtl_ID,
                                Ticket_ID = a.Ticket_ID,
                                User_Comment = a.User_Comment,
                                User_Comment_Date = a.User_Comment_Date,
                                User_Id = a.User_Id,
                                Is_Audio = a.Is_Audio
                            }).ToList();
            threadslist.AddRange(comments);
            ob.comments = threadslist;
            ob.isclosed = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Status).FirstOrDefault();
            ob.check_answer = (from a in DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList()
                               select new tbl_DC_Ticket_Dtl
                               {
                                   Answer = a.Answer,
                                   Answer_Image = a.Answer_Image != null ? ("/Images/Answerimage/" + a.Answer_Image) : "",
                                   Is_Active = a.Is_Active,
                                   Is_Deleted = a.Is_Deleted,
                                   Replied_By = a.Replied_By,
                                   Replied_Date = a.Replied_Date,
                                   Ticket_Dtls_ID = a.Ticket_Dtls_ID,
                                   Ticket_ID = a.Ticket_ID,
                                   Is_Audio = a.Is_Audio
                               }).FirstOrDefault();

            return View(ob);
        }

        public class ticketcls
        {
            public int Ticket_ID { get; set; }
            public int? Ticket_answerid { get; set; }
            public string Answer_Doubt { get; set; }
            public int Regd_id { get; set; }
            public string Ticket_No { get; set; }
            public bool isaudio { get; set; }
            public HttpPostedFileBase file { get; set; }
        }


        [HttpPost]
        public JsonResult SaveDoubtAnswer(ticketcls mb)
        {
            string h_tkid = mb.Ticket_ID.ToString();
            string Answer_Ticket = mb.Answer_Doubt;
            int replied_byid = Convert.ToInt32(Session["TeacherId"]);
            string h_sname = mb.Regd_id.ToString();
            string h_ticno = mb.Ticket_No.ToString();
            int id = mb.Ticket_ID;
            bool isaudio = mb.isaudio;
            HttpPostedFileBase file = mb.file;
            int Ticket_answerid = mb.Ticket_answerid.Value;
            try
            {
                if (Ticket_answerid == 0)
                {

                    var _find_assin_or_not = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (_find_assin_or_not.Teach_ID != null)
                    {


                        tbl_DC_Ticket_Dtl tk_dtl = new tbl_DC_Ticket_Dtl();
                        tk_dtl.Ticket_ID = id;
                        tk_dtl.Answer = Answer_Ticket;
                        tk_dtl.Replied_By = replied_byid;
                        tk_dtl.Replied_Date = DateTime.Now;
                        tk_dtl.Is_Active = true;
                        tk_dtl.Is_Deleted = false;
                        if (file != null)
                        {

                            string guid = Guid.NewGuid().ToString();
                            var fileName = Path.GetFileName(file.FileName.Replace(file.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            var path = Path.Combine(Server.MapPath("~/Images/Answerimage/"), fileName);
                            file.SaveAs(path);
                            tk_dtl.Answer_Image = fileName.ToString();

                        }
                        tk_dtl.Is_Audio = isaudio;
                        DbContext.tbl_DC_Ticket_Dtl.Add(tk_dtl);
                        DbContext.SaveChanges();
                        if (_find_assin_or_not.Status == "D")
                        {
                            _find_assin_or_not.Status = "O";
                            DbContext.SaveChanges();
                        }
                        int student_id_ = Convert.ToInt32(h_sname);

                        var student_mail = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _find_assin_or_not.Student_ID && x.Is_Active == true).FirstOrDefault();
                        if (student_mail != null)
                        {
                            try
                            {
                                var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _find_assin_or_not.Student_ID)

                                               select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                                string body = "DBTANS#{{tktid}}#Doubt Answered#Your teacher just answered your doubt(" + _find_assin_or_not.Ticket_No + ")";
                                string msg = body.ToString().Replace("{{tktid}}", tk_dtl.Ticket_ID.ToString())
                                   ;

                                if (pushnot != null)
                                {
                                    if (pushnot.Device_id != null)
                                    {
                                        var note = new PushNotiStatus("Doubt Answered", msg, pushnot.Device_id);
                                    }
                                }
                            }
                            catch (Exception e)
                            {


                            }
                        }
                        return Json(new { sts = "S", ticketId = h_tkid });

                    }
                    else
                    {
                        tbl_DC_Ticket_Assign _ticket_Assign = new tbl_DC_Ticket_Assign();
                        _ticket_Assign.Ticket_ID = id;
                        _ticket_Assign.Ticket_No = _find_assin_or_not.Ticket_No;
                        _ticket_Assign.Student_ID = _find_assin_or_not.Student_ID;
                        _ticket_Assign.Teach_ID = replied_byid;
                        _ticket_Assign.Assign_Date = DateTime.Now;
                        _ticket_Assign.Is_Close = false;
                        _ticket_Assign.Inserted_By = replied_byid;
                        _ticket_Assign.Inserted_Date = DateTime.Now;
                        _ticket_Assign.Is_Active = true;
                        _ticket_Assign.Is_Deleted = false;
                        DbContext.tbl_DC_Ticket_Assign.Add(_ticket_Assign);
                        DbContext.SaveChanges();
                        tbl_DC_Ticket _ticket = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id).FirstOrDefault();

                        _ticket.Teach_ID = replied_byid;
                        _ticket.Assign_Date = DateTime.Now;
                        DbContext.SaveChanges();
                        #region ticketanswer
                        tbl_DC_Ticket_Dtl tk_dtl = new tbl_DC_Ticket_Dtl();
                        tk_dtl.Ticket_ID = id;
                        tk_dtl.Answer = Answer_Ticket;
                        tk_dtl.Replied_By = replied_byid;
                        tk_dtl.Replied_Date = DateTime.Now;
                        tk_dtl.Is_Active = true;
                        tk_dtl.Is_Deleted = false;
                        tk_dtl.Is_Audio = isaudio;
                        if (file != null)
                        {

                            string guid = Guid.NewGuid().ToString();
                            var fileName = Path.GetFileName(file.FileName.Replace(file.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            var path = Path.Combine(Server.MapPath("~/Images/Answerimage/"), fileName);
                            file.SaveAs(path);
                            tk_dtl.Answer_Image = fileName.ToString();

                        }

                        DbContext.tbl_DC_Ticket_Dtl.Add(tk_dtl);
                        DbContext.SaveChanges();
                        if (_find_assin_or_not.Status == "D")
                        {
                            _find_assin_or_not.Status = "O";
                            DbContext.SaveChanges();
                        }
                        int student_id_ = Convert.ToInt32(h_sname);
                        var student_mail = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _find_assin_or_not.Student_ID && x.Is_Active == true).FirstOrDefault();
                        if (student_mail != null)
                        {
                            try
                            {
                                var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _find_assin_or_not.Student_ID)

                                               select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                                string body = "DBTANS#{{tktid}}#Doubt Answered#Your teacher just answered your doubt(" + _find_assin_or_not.Ticket_No + ")";
                                string msg = body.ToString().Replace("{{tktid}}", tk_dtl.Ticket_ID.ToString())
                                   ;

                                if (pushnot != null)
                                {
                                    if (pushnot.Device_id != null)
                                    {
                                        var note = new PushNotiStatus("Doubt Answered", msg, pushnot.Device_id);
                                    }
                                }
                            }
                            catch (Exception e)
                            {


                            }
                        }
                        return Json(new { sts = "S", ticketId = h_tkid }, JsonRequestBehavior.AllowGet);

                        #endregion

                    }

                }

                else
                {
                    tbl_DC_Ticket_Thread _ticket_thred = new tbl_DC_Ticket_Thread();

                    _ticket_thred.Ticket_ID = id;
                    _ticket_thred.Ticket_Dtl_ID = Ticket_answerid;
                    _ticket_thred.User_Comment = Answer_Ticket;
                    _ticket_thred.User_Comment_Date = DateTime.Now;
                    _ticket_thred.User_Id = replied_byid;
                    _ticket_thred.Is_Teacher = true;
                    _ticket_thred.Is_Taecher_Read = false;
                    _ticket_thred.Is_Active = true;
                    _ticket_thred.Is_Deleted = false;
                    _ticket_thred.Is_Audio = isaudio;
                    _ticket_thred.Modified_By = replied_byid.ToString();
                    _ticket_thred.Modified_Date = DateTime.Now;


                    if (file != null)
                    {

                        string guid = Guid.NewGuid().ToString();
                        var fileName = Path.GetFileName(file.FileName.Replace(file.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                        var path = Path.Combine(Server.MapPath("~/Images/Qusetionimages/"), fileName);
                        file.SaveAs(path);
                        _ticket_thred.R_image = fileName.ToString();

                    }
                    DbContext.tbl_DC_Ticket_Thread.Add(_ticket_thred);
                    DbContext.SaveChanges();

                    tbl_DC_Ticket _tbl_status2 = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id).FirstOrDefault();



                    var student_mail = DbContext.tbl_DC_Registration.
                        Where(x => x.Regd_ID == _tbl_status2.Student_ID && x.Is_Active == true).FirstOrDefault();

                    if (student_mail != null)
                    {
                        try
                        {
                            var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student_mail.Regd_ID)

                                           select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                            string body = "DBTRPLY#{{tktid}}#Doubt Reply#Your teacher just replied your doubt(" + _tbl_status2.Ticket_No + ")";
                            string msg = body.ToString().Replace("{{tktid}}", _tbl_status2.Ticket_ID.ToString());

                            if (pushnot != null)
                            {
                                if (pushnot.Device_id != null)
                                {
                                    var note = new PushNotiStatus("Doubt Reply", msg, pushnot.Device_id);
                                }
                            }
                        }
                        catch (Exception)
                        {


                        }
                    }
                    return Json(new { sts = "S", ticketId = id }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json(new { sts = "W" + ex.Message, ticketId = h_tkid }, JsonRequestBehavior.AllowGet); //Something went wrong

            }
        }
        public JsonResult RejectDoubt(string Remark_Reject, string h_tkid)
        {

            try
            {
                var teacherid = Convert.ToInt32(Session["TeacherId"]);
                if (Remark_Reject.Trim() != "")
                {
                    int id = Convert.ToInt32(h_tkid);
                    tbl_DC_Ticket tkt_rj = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    tkt_rj.Status = "R";
                    tkt_rj.Teach_ID = teacherid;
                    tkt_rj.Remark = Remark_Reject;
                    DbContext.SaveChanges();
                    var get_student = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == tkt_rj.Student_ID).FirstOrDefault();
                    if (get_student != null)
                    {
                        try
                        {
                            string body = "DBTRJT#{{tktid}}#Doubt Rejected#Your teacher just rejected your doubt (" + tkt_rj.Ticket_No + ")";
                            string msg = body.ToString().Replace("{{tktid}}", tkt_rj.Ticket_ID.ToString());


                            if (get_student.Device_id != null)
                            {
                                var note = new PushNotiStatus("Doubt Rejected", msg, get_student.Device_id);
                            }

                        }
                        catch (Exception)
                        {


                        }
                        return Json(new { sts = "S", id = h_tkid }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { sts = "S", id = h_tkid }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { sts = "R", id = h_tkid }, JsonRequestBehavior.AllowGet);//Please provide reason of rejection.
                }
            }
            catch (Exception ex)
            {
                return Json(new { sts = "W", id = h_tkid }, JsonRequestBehavior.AllowGet);//Something went wrong.

            }
        }
        public ActionResult DeleteReplyComment(int id)
        {
            tbl_DC_Ticket_Thread s = DbContext.tbl_DC_Ticket_Thread.Where(a => a.Comment_ID == id).FirstOrDefault();
            int tickid = s.Ticket_ID.Value;
            s.Is_Active = false;
            s.Is_Deleted = true;
            s.Modified_Date = today;
            DbContext.SaveChanges();
            return RedirectToAction("ViewDoubtDetails", new { id = tickid });
        }
        public ActionResult ReplyOnDoubtAnswer(int Ticket_ID, int Answer_Id, string Reply_Message, HttpPostedFileBase audiofile, HttpPostedFileBase imagefile)
        {
            int Ticket_id = Ticket_ID;
            int Ticket_answerid = Answer_Id;
            int _Teacher_id = Convert.ToInt32(Session["TeacherId"]);
            string msgbody = Reply_Message;
            string message = string.Empty;
            //bool isaudio = audiofile!=null?true:false;

            if (msgbody != "")
            {
                try
                {

                    tbl_DC_Ticket_Thread _ticket_thred = new tbl_DC_Ticket_Thread();

                    _ticket_thred.Ticket_ID = Ticket_id;
                    _ticket_thred.Ticket_Dtl_ID = Ticket_answerid;
                    _ticket_thred.User_Comment = msgbody;
                    _ticket_thred.User_Comment_Date = DateTime.Now;
                    _ticket_thred.User_Id = _Teacher_id;
                    _ticket_thred.Is_Teacher = true;
                    _ticket_thred.Is_Active = true;
                    _ticket_thred.Is_Deleted = false;

                    if (audiofile != null)
                    {
                        _ticket_thred.Is_Audio = true;
                        string guid = Guid.NewGuid().ToString();
                        var fileName = Path.GetFileName(audiofile.FileName.Replace(audiofile.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                        var path = Path.Combine(Server.MapPath("~/Images/Qusetionimages/"), fileName);
                        audiofile.SaveAs(path);
                        _ticket_thred.R_image = fileName.ToString();
                        DbContext.tbl_DC_Ticket_Thread.Add(_ticket_thred);
                        DbContext.SaveChanges();

                    }
                    if (imagefile != null)
                    {
                        _ticket_thred.Is_Audio = false;
                        string guid = Guid.NewGuid().ToString();
                        var fileName = Path.GetFileName(imagefile.FileName.Replace(imagefile.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                        var path = Path.Combine(Server.MapPath("~/Images/Qusetionimages/"), fileName);
                        imagefile.SaveAs(path);
                        _ticket_thred.R_image = fileName.ToString();
                        DbContext.tbl_DC_Ticket_Thread.Add(_ticket_thred);
                        DbContext.SaveChanges();
                    }
                    if (audiofile == null && imagefile == null)
                    {
                        DbContext.tbl_DC_Ticket_Thread.Add(_ticket_thred);
                        DbContext.SaveChanges();
                    }
                    //if (close == "on")
                    //{
                    //    tbl_DC_Ticket_Assign _tbl_close = DbContext.tbl_DC_Ticket_Assign.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                    //    _tbl_close.Is_Close = true;
                    //    _tbl_close.Remark = remark;
                    //    _tbl_close.Close_Date = DateTime.Now;
                    //    _tbl_close.Modified_By = _Teacher_id;
                    //    _tbl_close.Modified_Date = DateTime.Now;
                    //    DbContext.SaveChanges();
                    //    tbl_DC_Ticket _tbl_status = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                    //    _tbl_status.Status = "C";
                    //    _tbl_status.Modified_By = _Teacher_id;
                    //    _tbl_status.Modified_Date = DateTime.Now;
                    //    DbContext.SaveChanges();
                    //    var get_student = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _tbl_close.Student_ID).FirstOrDefault();
                    //    if (get_student != null)
                    //    {
                    //        //Close_ticket_mail(_tbl_close.Student_ID.ToString(), _tbl_close.Ticket_No);
                    //        //   sendMail_close_reject("Ticket_close", get_student.Email.ToString(), get_student.Customer_Name, _tbl_close.Ticket_No.ToString(), "C", remark.ToString());
                    //    }
                    //}
                    tbl_DC_Ticket _tbl_status2 = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();



                    var student_mail = DbContext.tbl_DC_Registration.
                        Where(x => x.Regd_ID == _tbl_status2.Student_ID && x.Is_Active == true).FirstOrDefault();

                    if (student_mail != null)
                    {
                        try
                        {
                            var pushnot = (from c in DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student_mail.Regd_ID)

                                           select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                            string body = "DBTRPLY#{{tktid}}#Doubt Reply#Your teacher just replied your doubt(" + _tbl_status2.Ticket_No + ")";
                            string msg = body.ToString().Replace("{{tktid}}", _tbl_status2.Ticket_ID.ToString());

                            if (pushnot != null)
                            {
                                if (pushnot.Device_id != null)
                                {
                                    var note = new PushNotiStatus("Doubt Reply", msg, pushnot.Device_id);
                                }
                            }
                        }
                        catch (Exception)
                        {


                        }
                    }

                    return RedirectToAction("ViewDoubtDetails", new { id = Ticket_id });

                }
                catch (Exception ex)
                {
                    TempData["ErrorMessage"] = "Something went wrong";
                    return RedirectToAction("ViewDoubtDetails", new { id = Ticket_id });

                }
            }
            else
            {
                TempData["ErrorMessage"] = "Reply Should not be empty";
                return RedirectToAction("ViewDoubtDetails", new { id = Ticket_id });
            }
        }
        public JsonResult CloseDoubt(string Remark, int h_tkid)
        {
            try
            {
                if (Remark != null && Remark != "")
                {
                    int _Teacher_id = Convert.ToInt32(Session["TeacherId"]);
                    tbl_DC_Ticket_Assign _tbl_close = DbContext.tbl_DC_Ticket_Assign.Where(x => x.Ticket_ID == h_tkid).FirstOrDefault();
                    _tbl_close.Is_Close = true;
                    _tbl_close.Remark = Remark;
                    _tbl_close.Close_Date = DateTime.Now;
                    _tbl_close.Modified_By = _Teacher_id;
                    _tbl_close.Modified_Date = DateTime.Now;
                    DbContext.SaveChanges();
                    tbl_DC_Ticket _tbl_status = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == h_tkid).FirstOrDefault();
                    _tbl_status.Status = "C";
                    _tbl_status.Modified_By = _Teacher_id;
                    _tbl_status.Modified_Date = DateTime.Now;
                    DbContext.SaveChanges();
                    return Json(new { sts = "S" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { sts = "R" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { sts = "W" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region Upload worksheet marks
        public ActionResult AllStudentWorksheet(Guid? School, int? board, int? cls, Guid? sec, int? subject, int? Chapter)
        {
            var username = Convert.ToString(Session["Mobile"]);
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            if (username != null && username != "")
            {

                ViewBag.school = null;
                ViewBag.board = null;
                ViewBag.cls = null;
                ViewBag.sec = null;
                ViewBag.subject = null;
                ViewBag.chapter = null;

                var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
                var boardlistss = assigndet.Select(a => a.Board_Id).ToList();
                var classlist = assigndet.Select(a => a.Class_Id).ToList();
                var sectionlist = assigndet.Select(a => a.SectionId).ToList();
                var subjectlist = assigndet.Select(a => a.Subject_Id).ToList();
                Guid id = new Guid(Session["SchoolId"].ToString());

                List<tbl_DC_Worksheet> wrk = null;
                wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true).ToList();

                // var wrksheet = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                // var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.SchoolId == id).ToList();
                var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true).ToList();
                var res = (from a in wrk
                           join b in boardlist on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           //  join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           where boardlistss.Contains(a.Board_Id) && classlist.Contains(a.Class_Id) && sectionlist.Contains(a.SectionId) && subjectlist.Contains(a.Subject_Id)
                           select new worksheetDetails
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Module_Name = "",
                               Regd_ID = a.Regd_ID,
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Student_Mobile = h.Mobile,
                               Total_Mark = a.Total_Mark,
                               Status = a.Verified == true ? "Verified" : "Uploaded",
                               Upload_Date = a.Inserted_Date != null ? Convert.ToDateTime(a.Inserted_Date).ToString("dd-MMM-yyyy") : "NA",
                               Verify_Date = a.Verified == true ? Convert.ToDateTime(a.Modified_Date).ToString("dd-MMM-yyyy") : "NA",
                               TotalVerified = wrk.Where(z => z.Verified == true).ToList().Count(),
                               TotalPending = wrk.Where(z => z.Verified == false).ToList().Count(),
                               TotalSubmitted = wrk.ToList().Count(),
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               Worksheet_Id = a.Worksheet_Id,
                               attachments = (from k in worksheetattachments
                                              where k.Worksheet_Id == a.Worksheet_Id
                                              select new tbl_DC_Worksheet_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Worksheet/" + k.Path,
                                                  Worksheet_Attachment_Id = k.Worksheet_Attachment_Id,
                                                  Worksheet_Id = k.Worksheet_Id
                                              }).ToList(),
                           }).ToList();
                if (board != null)
                {
                    res = res.Where(a => a.Board_Id == board).ToList();
                    ViewBag.board = board.Value;
                }

                if (cls != null)
                {
                    res = res.Where(a => a.Class_Id == cls).ToList();
                    ViewBag.cls = cls.Value;
                }
                if (sec != null)
                {
                    res = res.Where(a => a.SectionId == sec).ToList();
                    ViewBag.sec = sec.Value;
                }
                if (subject != null)
                {
                    res = res.Where(a => a.Subject_Id == subject).ToList();
                    ViewBag.subject = subject.Value;
                }
                if (Chapter != null)
                {
                    res = res.Where(a => a.Chapter_Id == Chapter).ToList();
                    ViewBag.chapter = Chapter.Value;
                }

                if (School != null)
                {
                    ViewBag.school = School.Value;
                }
                Session["markworksheet"] = res;
                return View(res);

            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        public ActionResult DownloadWorksheetExcel()
        {
            XLWorkbook oWB = new XLWorkbook();

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Worksheet_Id");
            validationTable.Columns.Add("Student_Name");
            validationTable.Columns.Add("Mobile");
            validationTable.Columns.Add("Board");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Section");
            validationTable.Columns.Add("Subject");
            validationTable.Columns.Add("Chapter");
            validationTable.Columns.Add("File Name");
            validationTable.Columns.Add("Attachment");
            validationTable.Columns.Add("Uploaded Date");
            validationTable.Columns.Add("Mark");
            List<worksheetDetails> worksheets = new List<worksheetDetails>();
            if (Session["markworksheet"] != null)
            {
                worksheets = Session["markworksheet"] as List<worksheetDetails>;
            }
            foreach (var a in worksheets)
            {
                if (a.attachments.Count > 0)
                {
                    DataRow dr = validationTable.NewRow();
                    dr["Worksheet_Id"] = a.Worksheet_Id;
                    dr["Student_Name"] = a.Student_Name;
                    dr["Mobile"] = a.Student_Mobile;
                    dr["Board"] = a.Board_Name;
                    dr["Class"] = a.Class_Name;
                    dr["Section"] = a.Section_Name;
                    dr["Subject"] = a.Subject_Name;
                    dr["Chapter"] = a.Chapter_Name;
                    dr["File Name"] = a.attachments.FirstOrDefault().File_Name;
                    dr["Attachment"] = "http://learn.odmps.org" + a.attachments.FirstOrDefault().Path;
                    dr["Uploaded Date"] = a.attachments.FirstOrDefault().Modified_Date.Value.ToString("dd-MM-yyyy");
                    dr["Mark"] = a.Total_Mark;
                    validationTable.Rows.Add(dr);
                }
            }
            validationTable.TableName = "Worksheet_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "Worksheetsubmitted.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadStudentWorksheetExcel(HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("AllStudentWorksheet");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);
                        XLWorkbook workbook = new XLWorkbook(path);


                        IXLWorksheet xLWorksheet = workbook.Worksheet(1);
                        DataTable dt = new DataTable();
                        bool firstrow = false;
                        foreach (IXLRow row in xLWorksheet.Rows())
                        {
                            if (firstrow == false)
                            {
                                for (int a = 0; a < row.Cells().Count(); a++)
                                {

                                    dt.Columns.Add(row.Cells().ToList()[a].Value.ToString().Trim());

                                }
                                firstrow = true;
                            }
                            else
                            {
                                dt.Rows.Add();
                                int j = 0;
                                foreach (IXLCell cell in row.Cells())
                                {
                                    dt.Rows[dt.Rows.Count - 1][j] = cell.Value.ToString();
                                    j++;
                                }
                            }

                        }

                        var data = dt;

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string worksheetid = dr["Worksheet_Id"].ToString();

                                if (worksheetid != "")
                                {
                                    int workid = Convert.ToInt32(worksheetid);
                                    var teacherid = Convert.ToInt32(Session["TeacherId"]);
                                    var alldetail = DbContext.tbl_DC_Worksheet.Where(x => x.Worksheet_Id == workid).FirstOrDefault();
                                    if (alldetail != null)
                                    {
                                        string mark = dr["Mark"].ToString();
                                        if (mark != "")
                                        {
                                            int mk = Convert.ToInt32(mark);
                                            if (mk != alldetail.Total_Mark)
                                            {
                                                alldetail.Total_Mark = mk;
                                                alldetail.Verified = true;
                                                alldetail.Verified_By = teacherid;

                                                alldetail.Modified_Date = today;
                                                alldetail.Modified_By = teacherid;
                                                DbContext.Entry(alldetail).State = EntityState.Modified;
                                                DbContext.SaveChanges();
                                            }

                                        }

                                    }
                                }
                            }

                        }
                        TempData["SuccessMessage"] = "Worksheet Details Entered Successfully";
                        return RedirectToAction("AllStudentWorksheet");
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("AllStudentWorksheet");
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("AllStudentWorksheet");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("uploadTeachers");
            }
        }
        #endregion
        #region Exam Name
        public ActionResult CreateExam(long? id)
        {
            ViewBag.id = 0;
            ViewBag.name = "";
            if (id != null && id != 0)
            {
                tbl_ExamCategory examCategory = DbContext.tbl_ExamCategory.Where(a => a.ExamCategoryID == id && a.Is_Active == true).FirstOrDefault();
                ViewBag.id = examCategory.ExamCategoryID;
                ViewBag.name = examCategory.ExamCategoryName;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveExam(long? id, string name)
        {
            try
            {
                tbl_ExamCategory examCategory = new tbl_ExamCategory();
                if (id == 0)
                {
                    examCategory = DbContext.tbl_ExamCategory.Where(a => a.ExamCategoryName == name && a.Is_Active == true).FirstOrDefault();

                    if (examCategory != null)
                    {
                        TempData["ErrorMessage"] = "Already Exists";
                        return RedirectToAction("CreateExam");
                    }

                }
                if (id != 0)
                {
                    examCategory = DbContext.tbl_ExamCategory.Where(a => a.ExamCategoryName == name && a.ExamCategoryID != id && a.Is_Active == true).FirstOrDefault();

                    if (examCategory != null)
                    {
                        TempData["ErrorMessage"] = "Already Exists";
                        return RedirectToAction("CreateExam");
                    }

                }
                examCategory = new tbl_ExamCategory();
                if (id != 0)
                {
                    examCategory = DbContext.tbl_ExamCategory.Where(a => a.ExamCategoryID == id && a.Is_Active == true).FirstOrDefault();
                }
                examCategory.ExamCategoryName = name;

                examCategory.Is_Active = true;
                if (id == 0)
                {
                    examCategory.InsertedId = 1;
                    examCategory.InsertedOn = DateTime.Now;
                }
                examCategory.ModifiedId = 1;
                examCategory.ModifiedOn = DateTime.Now;
                if (id == 0)
                {
                    DbContext.tbl_ExamCategory.Add(examCategory);
                }
                DbContext.SaveChanges();
                TempData["SuccessMessage"] = "Saved Successfully";
                return RedirectToAction("ExamList");
            }
            catch
            {
                return null;
            }
        }

        public ActionResult ExamList()
        {
            try
            {
                var examlist = DbContext.tbl_ExamCategory.Where(a => a.Is_Active == true).ToList();

                return View(examlist);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ActionResult DeleteExam(long Id)
        {
            try
            {
                var examcatg = DbContext.tbl_ExamCategory.Where(a => a.Is_Active == true && a.ExamCategoryID == Id).FirstOrDefault();

                //Negativemark negativemark = new Negativemark();
                examcatg.Is_Active = false;
                examcatg.ModifiedOn = DateTime.Now;
                DbContext.SaveChanges();
                return RedirectToAction("ExamList");
            }
            catch
            {
                return null;
            }
        }

        #endregion
        #region E-Report Upload
        public class Schoolwise_User_Class
        {
            public int? Regd_ID { set; get; }
            public string Regd_No { get; set; }
            public string Customer_Name { get; set; }
            public Nullable<Guid> SchoolId { get; set; }
            public Nullable<int> Class_ID { get; set; }
            public Nullable<Guid> SectionId { get; set; }
            public Nullable<int> Board_ID { get; set; }
            public string boardname { get; set; }
            public string schoolname { get; set; }
            public string Class_Name { get; set; }
            public string SectionName { get; set; }
            public string Mobile { get; set; }
            public string Email { get; set; }
            public string School_No { get; set; }
            public string zoom_mail_id { get; set; }
            public string whatsapp_no { get; set; }
            public long CategoryId { get; set; }
            public long EReportId { get; set; }
            public string EReportPath { get; set; }
            public string categoryName { get; set; }
        }
        public ActionResult GetRegisteredUsersListForEReport(Guid? school, int? board, int? cls, Guid? sec, long? catgid)
            {
                // Guid? school = new Guid(Session["id"].ToString());
                var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
                var classs = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var boards = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true).ToList();
                var examcatg = DbContext.tbl_ExamCategory.Where(a => a.Is_Active == true).ToList();
                ViewBag.examlist = examcatg;
                ViewBag.schoolist = schools;
                ViewBag.s = school;
                ViewBag.b = board;
                ViewBag.c = cls;
                ViewBag.se = sec;
                ViewBag.cg = catgid;
                var ereport = DbContext.tbl_EReportCard.Where(a => a.Is_Active == true).ToList();
                if (catgid != null && catgid != 0)
                {
                    var res = (from a in students
                               join b in std_dtls on a.Regd_ID equals b.Regd_ID
                               // where a.SchoolId == school
                               //&& a.Regd_ID == 258
                               select new Schoolwise_User_Class
                               {
                                   Regd_ID = a.Regd_ID,
                                   Regd_No = a.Regd_No,
                                   Customer_Name = a.Customer_Name,
                                   SchoolId = (a.SchoolId == null && a.SchoolId == Guid.Empty) ? Guid.Empty : a.SchoolId,
                                   Class_ID = b.Class_ID == null ? null : b.Class_ID,
                                   SectionId = (a.SectionId == null && a.SectionId == Guid.Empty) ? Guid.Empty : a.SectionId,
                                   Board_ID = b.Board_ID == null ? null : b.Board_ID,
                                   boardname = b.Board_ID == null ? "" : boards.Where(c => c.Board_Id == b.Board_ID).FirstOrDefault().Board_Name,
                                   schoolname = (a.SchoolId == null && a.SchoolId == Guid.Empty) ? "" : schools.Where(c => c.SchoolId == a.SchoolId).FirstOrDefault().SchoolName,
                                   Class_Name = b.Class_ID == null ? "" : classs.Where(c => c.Class_Id == b.Class_ID).FirstOrDefault().Class_Name,
                                   SectionName = ((a.SectionId == null && a.SectionId == Guid.Empty) ? "" : sections.Where(c => c.SectionId == a.SectionId).FirstOrDefault().SectionName),
                                   Mobile = a.Mobile,
                                   Email = a.Email,
                                   EReportId = ereport == null ? 0 : (ereport.Where(m => m.Regd_Id == a.Regd_ID && m.ExamCategoryID == catgid && m.Is_Active == true).FirstOrDefault() == null ? 0 : ereport.Where(m => m.Regd_Id == a.Regd_ID && m.ExamCategoryID == catgid && m.Is_Active == true).FirstOrDefault().EReportCardID),
                                   EReportPath = ereport == null ? null : (ereport.Where(m => m.Regd_Id == a.Regd_ID && m.ExamCategoryID == catgid && m.Is_Active == true).FirstOrDefault() == null ? null : ereport.Where(m => m.Regd_Id == a.Regd_ID && m.ExamCategoryID == catgid && m.Is_Active == true).FirstOrDefault().DocumentPath),
                                   CategoryId = examcatg.Where(m => m.ExamCategoryID == catgid).FirstOrDefault().ExamCategoryID,
                                   categoryName = examcatg.Where(m => m.ExamCategoryID == catgid).FirstOrDefault().ExamCategoryName,
                                   School_No = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault().School_No : "") : "",
                                   whatsapp_no = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault().Whatsapp_Mobile_No : "") : "",
                                   zoom_mail_id = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault().Zoom_Mail_ID : "") : "",
                               }).ToList();
                    if (school != null && school != Guid.Empty)
                    {
                        res = res.Where(a => a.SchoolId == school).ToList();
                    }
                    if (board != null)
                    {
                        res = res.Where(a => a.Board_ID == board).ToList();
                    }
                    if (cls != null)
                    {
                        res = res.Where(a => a.Class_ID == cls).ToList();
                    }
                    if (sec != null)
                    {
                        res = res.Where(a => a.SectionId == sec).ToList();
                    }
                    Session["Get_EReport_User"] = res;
                    var json = Json(res, JsonRequestBehavior.AllowGet);
                    json.MaxJsonLength = int.MaxValue;
                    return View(res);
                }
                else
                {
                    return View();
                }
            }
        public class Encodings
        {
            public static string Base64EncodingMethod(string Data)
            {
                byte[] toEncodeAsBytes = System.Text.Encoding.UTF8.GetBytes(Data);
                string sReturnValues = System.Convert.ToBase64String(toEncodeAsBytes);
                return sReturnValues;
            }
            //Decoding
            public static string Base64DecodingMethod(string Data)
            {
                byte[] encodedDataAsBytes = System.Convert.FromBase64String(Data);
                string returnValue = System.Text.Encoding.UTF8.GetString(encodedDataAsBytes);
                return returnValue;
            }
        }
        [HttpPost]
        public ActionResult UploadEReportCardDetails(int? id,int? regdid, long? catgid, HttpPostedFileBase file)
        {
            tbl_EReportCard _EReportCard = new tbl_EReportCard();
            if (id == null)
            {
                _EReportCard = new tbl_EReportCard();
                _EReportCard.ExamCategoryID = catgid;
                _EReportCard.Regd_Id = regdid;
                _EReportCard.Is_Active = true;
                _EReportCard.InsertedId = 1;
                _EReportCard.InsertedOn = DateTime.Now;
                _EReportCard.ModifiedId = 1;
                _EReportCard.ModifiedOn = DateTime.Now;
                if (file != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    string path = string.Empty;
                    var fileName = Path.GetFileName(file.FileName.
                        Replace(file.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    path = Path.Combine(Server.MapPath("~/Images/EReportCardPDF/"), fileName);
                    file.SaveAs(path);
                    _EReportCard.DocumentPath = fileName;
                }
                DbContext.tbl_EReportCard.Add(_EReportCard);
                DbContext.SaveChanges();
            }
            else
            {
                _EReportCard = DbContext.tbl_EReportCard.Where(a => a.EReportCardID == id).FirstOrDefault();
                _EReportCard.ExamCategoryID = catgid;
                _EReportCard.Regd_Id = regdid;
                _EReportCard.Is_Active = true;
                _EReportCard.ModifiedId = 1;
                _EReportCard.ModifiedOn = DateTime.Now;
                if (file != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    string path = string.Empty;
                    var fileName = Path.GetFileName(file.FileName.
                        Replace(file.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    path = Path.Combine(Server.MapPath("~/Images/EReportCardPDF/"), fileName);
                    file.SaveAs(path);
                    _EReportCard.DocumentPath = fileName;
                }
                DbContext.SaveChanges();
            }
            long ereportid = _EReportCard.EReportCardID;
            var docpath = _EReportCard.DocumentPath;
            var student = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Regd_ID == regdid).FirstOrDefault();
            var examtype = DbContext.tbl_ExamCategory.Where(a => a.Is_Active == true && a.ExamCategoryID == catgid).FirstOrDefault();

            //var empid = _EReportCard.EReportCardID;
            //string encode = Encodings.Base64EncodingMethod(empid.ToString());
            string url = "http://learn.odmps.org/Images/EReportCardPDF/" + _EReportCard.DocumentPath;
            //bitly b = new bitly();
            string shortlink = ToTinyURLS(url);
            if (student.Mobile != "")
            {
                Sendsmstoemployee(student.Mobile, "Dear " + student.Customer_Name + ", \nPlease find the E-Report Card of your exam " + examtype.ExamCategoryName + ". Click on the link and save the Report Card.- " + shortlink + " \n\n Regards\n ODM Educational Group");
            }

            if (student.Email != "")
            {
                string body = PopulateBody(student.Customer_Name, examtype.ExamCategoryName, shortlink);
                SendHtmlFormattedEmail(student.Email, "E-Report Card | ODM Public School", body);
            }

            var zoomemailid = DbContext.tbl_Regd_BasicDetl.Where(m => m.Is_Active == true && m.Regd_ID == regdid).FirstOrDefault().Zoom_Mail_ID;

            if (zoomemailid != " " && zoomemailid != null)
            {
                string body = PopulateBody(student.Customer_Name, examtype.ExamCategoryName, shortlink);
                SendHtmlFormattedEmail(zoomemailid, "E-Report Card | ODM Public School", body);
            }

            return RedirectToAction("GetRegisteredUsersListForEReport", new { catgid = catgid });
        }

        public ActionResult GetAllEReportDetails(Guid? school, int? board, int? cls, Guid? sec)
        {
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.schoolist = schools;
            ViewBag.s = school;
            ViewBag.b = board;
            ViewBag.c = cls;
            ViewBag.se = sec;
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var ereport = DbContext.tbl_EReportCard.Where(a => a.Is_Active == true).ToList();
            var examcatg = DbContext.tbl_ExamCategory.Where(a => a.Is_Active == true).ToList();
            var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true).ToList();
            var classs = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
            var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var boards = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            var res = (from a in ereport
                       join b in students on a.Regd_Id equals b.Regd_ID
                       join d in std_dtls on b.Regd_ID equals d.Regd_ID
                       select new Schoolwise_User_Class
                       {
                           SchoolId = (b.SchoolId == null && b.SchoolId == Guid.Empty) ? Guid.Empty : b.SchoolId,
                           Class_ID = d.Class_ID == null ? null : d.Class_ID,
                           SectionId = (b.SectionId == null && b.SectionId == Guid.Empty) ? Guid.Empty : b.SectionId,
                           Board_ID = d.Board_ID == null ? null : d.Board_ID,
                           boardname = d.Board_ID == null ? "" : boards.Where(c => c.Board_Id == d.Board_ID).FirstOrDefault().Board_Name,
                           schoolname = (b.SchoolId == null && b.SchoolId == Guid.Empty) ? "" : schools.Where(c => c.SchoolId == b.SchoolId).FirstOrDefault().SchoolName,
                           Class_Name = d.Class_ID == null ? "" : classs.Where(c => c.Class_Id == d.Class_ID).FirstOrDefault().Class_Name,
                           SectionName = ((b.SectionId == null && b.SectionId == Guid.Empty) ? "" : sections.Where(c => c.SectionId == b.SectionId).FirstOrDefault().SectionName),
                           EReportId = a.EReportCardID,
                           Email = b.Email,
                           Customer_Name = b.Customer_Name,
                           Mobile = b.Mobile,
                           EReportPath = "/Images/EReportCardPDF/" + a.DocumentPath,
                           Regd_ID = a.Regd_Id,
                           School_No = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_Id).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_Id).FirstOrDefault().School_No : "") : "",
                           whatsapp_no = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_Id).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_Id).FirstOrDefault().Whatsapp_Mobile_No : "") : "",
                           zoom_mail_id = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_Id).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_Id).FirstOrDefault().Zoom_Mail_ID : "") : "",

                       });
            if (school != null && school != Guid.Empty)
            {
                res = res.Where(a => a.SchoolId == school).ToList();
            }
            if (board != null)
            {
                res = res.Where(a => a.Board_ID == board).ToList();
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_ID == cls).ToList();
            }
            if (sec != null)
            {
                res = res.Where(a => a.SectionId == sec).ToList();
            }
            return View(res.ToList());

        }


        #region Mail and Messages

        public bool Sendsmstoemployee(string mobile, string message)
        {
            try
            {
                string baseurl = "http://api.msg91.com/api/sendhttp.php?sender=ODMEGR&route=4&mobiles=" + mobile + "&authkey=232520Aycx7OOpF5b790d71"
                + "&country=91&message=" + message;

                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);

                HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                string responseString = respStreamReader.ReadToEnd();
                respStreamReader.Close();
                myResp.Close();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        private string PopulateBody(string student, string classname, string url)
        {
            string body = string.Empty;
            // string encode = Encodings.Base64EncodingMethod(id.ToString());
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/EReportTemplate.html")))
            {

                body = reader.ReadToEnd().Replace("{{Student_Name}}", student).Replace("{{Class}}", classname).Replace("{{href}}", url);

            }

            return body;
        }
        private string PopulateBodyResultSheet(string student, string classname, string url)
        {
            string body = string.Empty;
            // string encode = Encodings.Base64EncodingMethod(id.ToString());
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/ResultSheetTemplate.html")))
            {

                body = reader.ReadToEnd().Replace("{{Student_Name}}", student).Replace("{{Class}}", classname).Replace("{{href}}", url);

            }

            return body;
        }


        private void SendHtmlFormattedEmail(string recepientEmail, string subject, string body)
        {
            try
            {
                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress("tracq@odmegroup.org");
                    mailMessage.Subject = subject;
                    mailMessage.Body = body;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.To.Add(new MailAddress(recepientEmail));
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = ConfigurationManager.AppSettings["smtpServer"];
                    smtp.EnableSsl = true;
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    NetworkCred.UserName = "tracq@odmegroup.org";
                    NetworkCred.Password = "odm@1234";
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = int.Parse(ConfigurationManager.AppSettings["smtpPort"]);
                    smtp.Send(mailMessage);
                }
            }
            catch { }
        }

        #endregion
        #region Bitly

        protected string ToTinyURLS(string txt)
        {
            Regex regx = new Regex("http://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);

            MatchCollection mactches = regx.Matches(txt);

            foreach (Match match in mactches)
            {
                string tURL = MakeTinyUrl(match.Value);
                txt = txt.Replace(match.Value, tURL);
            }

            return txt;
        }

        public static string MakeTinyUrl(string Url)
        {
            try
            {
                if (Url.Length <= 12)
                {
                    return Url;
                }
                if (!Url.ToLower().StartsWith("http") && !Url.ToLower().StartsWith("ftp"))
                {
                    Url = "http://" + Url;
                }
                var request = WebRequest.Create("http://tinyurl.com/api-create.php?url=" + Url);
                var res = request.GetResponse();
                string text;
                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }
                return text;
            }
            catch (Exception)
            {
                return Url;
            }
        }

        #endregion
        #endregion
        #region Upload Online Exam Report
        public class OnlineExamReportModel
        {
            public long StudentmarksheetId { get; set; }
            public Nullable<long> OnlineExamTypeId { get; set; }
            public Nullable<int> RegdId { get; set; }
            public string Session { get; set; }
            public string Batch { get; set; }
            public string SubjectCombination { get; set; }
            public string Total_Mark_Subjective { get; set; }
            public string Total_Mark_Objective { get; set; }
            public string Percentage_Subjective { get; set; }
            public string Percentage_Objective { get; set; }
            public Nullable<bool> Is_Active { get; set; }
            public Nullable<int> InsertedId { get; set; }
            public Nullable<int> ModifiedId { get; set; }
            public string InsertedOn { get; set; }
            public Nullable<System.DateTime> ModifiedOn { get; set; }
            public string Roll_No { get; set; }
            public string School_No { get; set; }
            public string StudentName { get; set; }
            public string ClassName { get; set; }
            public string section { get; set; }
            public Nullable<int> typeId { get; set; }
            public string OnlineExamTypeName { get; set; }
        }
        public ActionResult OnlineExamReportList(Guid? school, int? board, int? cls, Guid? sec, int? student, int? type, string session)
        {
            var examtype = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            ViewBag.examtype = examtype;
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.schoolist = schools;
            ViewBag.s = school;
            ViewBag.b = board;
            ViewBag.c = cls;
            ViewBag.se = sec;
            ViewBag.f = student;
            ViewBag.g = type;
            var Marksheet = DbContext.Tbl_Student_Marksheet.Where(a => a.Is_Active == true).ToList();

            var res = (from a in Marksheet
                       join b in examtype on a.OnlineExamTypeId equals b.OnlineExamTypeId
                       select new OnlineExamReportModel
                       {
                           StudentmarksheetId = a.StudentmarksheetId,
                           OnlineExamTypeId = a.OnlineExamTypeId,
                           OnlineExamTypeName = b.OnlineExamTypeName,
                           RegdId = a.RegdId,
                           Session = a.Session,
                           section = a.section,
                           Total_Mark_Objective = a.Total_Mark_Objective,
                           Total_Mark_Subjective = a.Total_Mark_Subjective,
                           Percentage_Subjective = a.Percentage_Subjective,
                           Percentage_Objective = a.Percentage_Objective,
                           Roll_No = a.Roll_No,
                           School_No = a.School_No,
                           StudentName = a.StudentName,
                           ClassName = a.ClassName,
                           typeId = a.typeId,
                           ModifiedOn = a.ModifiedOn,
                           InsertedOn = a.InsertedOn.Value.ToString("yyyy-MM-dd")
                       }
                      ).ToList();

            if (cls != null)
            {
                string classname = DbContext.tbl_DC_Class.Where(v => v.Class_Id == cls).FirstOrDefault().Class_Name;
                res = res.Where(a => a.ClassName == classname).ToList();
            }
            if (sec != null)
            {
                string sessionname = DbContext.tbl_DC_Class_Section.Where(v => v.SectionId == sec).FirstOrDefault().SectionName;
                res = res.Where(a => a.section == sessionname).ToList();
            }
            if (student != null)
            {
                res = res.Where(a => a.RegdId == student).ToList();
            }
            if (type != null)
            {
                res = res.Where(a => a.OnlineExamTypeId == type).ToList();
            }

            if (session != null && session != "All")
            {
                res = res.Where(a => a.Session == session).ToList();
            }
            Session["marksheetreport"] = res;
            return View(res);




            //var examtype = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            //ViewBag.examtype = examtype;
            //var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            //ViewBag.schoolist = schools;
            //ViewBag.s = school;
            //ViewBag.b = board;
            //ViewBag.c = cls;
            //ViewBag.se = sec;
            //ViewBag.f = student;
            //ViewBag.g = type;
            //var Marksheet = DbContext.Tbl_Student_Marksheet.Where(a => a.Is_Active == true).ToList();

            //var res = (from a in Marksheet
            //           join b in examtype on a.OnlineExamTypeId equals b.OnlineExamTypeId
            //           select new OnlineExamReportModel
            //           {
            //               StudentmarksheetId = a.StudentmarksheetId,
            //               OnlineExamTypeId = a.OnlineExamTypeId,
            //               OnlineExamTypeName = b.OnlineExamTypeName,
            //               RegdId = a.RegdId,
            //               Session = a.Session,
            //               section = a.section,
            //               Total_Mark_Objective = a.Total_Mark_Objective,
            //               Total_Mark_Subjective = a.Total_Mark_Subjective,
            //               Percentage_Subjective = a.Percentage_Subjective,
            //               Percentage_Objective = a.Percentage_Objective,
            //               Roll_No = a.Roll_No,
            //               School_No = a.School_No,
            //               StudentName = a.StudentName,
            //               ClassName = a.ClassName,
            //               typeId = a.typeId,
            //               ModifiedOn = a.ModifiedOn,
            //               InsertedOn = a.InsertedOn.Value.ToString("yyyy-MM-dd")
            //           }
            //          ).ToList();

            //if (cls != null)
            //{
            //    string classname = DbContext.tbl_DC_Class.Where(v => v.Class_Id == cls).FirstOrDefault().Class_Name;
            //    res = res.Where(a => a.ClassName == classname).ToList();
            //}
            //if (sec != null)
            //{
            //    string sessionname = DbContext.tbl_DC_Class_Section.Where(v => v.SectionId == sec).FirstOrDefault().SectionName;
            //    res = res.Where(a => a.section == sessionname).ToList();
            //}
            //if (student != null)
            //{
            //    res = res.Where(a => a.RegdId == student).ToList();
            //}
            //if (type != null)
            //{
            //    res = res.Where(a => a.OnlineExamTypeId == type).ToList();
            //}

            //if (session != null && session != "All")
            //{
            //    res = res.Where(a => a.Session == session).ToList();
            //}
            //Session["marksheetreport"] = res;
            //return View(res);
        }
        public ActionResult UploadOnlineExamReport()
        {
            var OnlineExamTypessList = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            ViewBag.Examtype = OnlineExamTypessList;
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.schools = schools;


            return View();
        }
        [HttpPost]
        public ActionResult UploadstudentmarksheetReport(int typ, int examtype, Guid school, int board, int cls, string session, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("UploadOnlineExamReport");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);

                        XLWorkbook workbook = new XLWorkbook(path);


                        IXLWorksheet xLWorksheet = workbook.Worksheet(1);
                        DataTable dt = new DataTable();
                        var i = 0;
                        foreach (IXLRow row in xLWorksheet.Rows())
                        {
                            if (i == 1)
                            {
                                //foreach (IXLCell cell in row.Cells())
                                //{
                                int m = 1;
                                for (int a = 0; a < row.Cells().Count(); a++)
                                {
                                    //if (m <= 3)
                                    //{
                                    if (row.Cells().ToList()[a].Value.ToString() == "")
                                    {
                                        dt.Columns.Add(row.Cells().ToList()[a - m].Value.ToString().Trim() + m);
                                        m++;
                                    }
                                    else
                                    {
                                        dt.Columns.Add(row.Cells().ToList()[a].Value.ToString().Trim());
                                        m = 1;
                                    }
                                    //}
                                }
                                //}
                            }
                            else if (i > 1)
                            {
                                dt.Rows.Add();
                                int j = 0;
                                foreach (IXLCell cell in row.Cells())
                                {
                                    dt.Rows[dt.Rows.Count - 1][j] = cell.Value.ToString();
                                    j++;
                                }
                            }
                            i = i + 1;
                        }

                        var data = dt;
                        bool firstrow = true;
                        bool firstrow1 = true;
                        List<string> schnos = new List<string>();
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (firstrow1 == true)
                            {
                                firstrow1 = false;
                            }
                            else
                            {
                                string schoolno = dr["SCHOOL NO."].ToString();
                                var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                if (regdetlist == null)
                                {
                                    schnos.Add(schoolno);
                                }
                            }
                        }

                        var result = schnos;

                        clsmarkslist clsmarks = new clsmarkslist();
                        List<marksheetmailcls> ss = new List<marksheetmailcls>();

                        foreach (DataRow dr in dt.Rows)
                        {
                            if (firstrow == true)
                            {
                                firstrow = false;
                            }
                            else
                            {
                                if (typ == 2)
                                {

                                    string schoolno = dr["SCHOOL NO."].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Marksheet _Marksheet = new Tbl_Student_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamTypeId = examtype;
                                        _Marksheet.Percentage_Objective = dr["% OF MARK1"].ToString();
                                        _Marksheet.Percentage_Subjective = dr["% OF MARK"].ToString();
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["ROLL NO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC"].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Objective = dr["TOTAL\nMARK1"].ToString();
                                        _Marksheet.Total_Mark_Subjective =dr["TOTAL\nMARK"].ToString();
                                        DbContext.Tbl_Student_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 5; y <= (dt.Columns.Count - 5); y = y + 2)
                                        {
                                            Tbl_StudentMarksheetDet marksheetDet = new Tbl_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.ObjectiveScore = dr[dt.Columns[y].ColumnName + "1"].ToString();
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        //sajdgj
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtypename;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }

                                }
                                else if (typ == 1)
                                {
                                    string schoolno = dr["SCL.\nNO."].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Marksheet _Marksheet = new Tbl_Student_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamTypeId = examtype;
                                        _Marksheet.Percentage_Objective = dr["TOTAL (%)1"].ToString();
                                        _Marksheet.Percentage_Subjective =dr["TOTAL (%)"].ToString();
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["R.\nNO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC"].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Objective = dr["TOTAL1"].ToString();
                                        _Marksheet.Total_Mark_Subjective = dr["TOTAL"].ToString();
                                        DbContext.Tbl_Student_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 4; y <= (dt.Columns.Count - 5); y = y + 2)
                                        {
                                            Tbl_StudentMarksheetDet marksheetDet = new Tbl_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.ObjectiveScore = dr[dt.Columns[y].ColumnName + "1"].ToString();
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtypename;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);
                                        //string url = "http://learn.odmps.org/Reports/Reports.aspx?type=School_Marksheet_Report&Id=" + markid;
                                        ////bitly b = new bitly();
                                        //string shortlink = ToTinyURLS(url);
                                        //if (regdlist.Mobile != "")
                                        //{
                                        //    Sendsmstoemployee(regdlist.Mobile, "Dear " + regdlist.Customer_Name + ", \nPlease find the Online Test Result of your exam " + examtypename + ". Click on the link and save the Test Result.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                                        //}

                                        //if (regdlist.Email != "")
                                        //{
                                        //    string body = PopulateBodyResultSheet(regdlist.Customer_Name, examtypename, shortlink);
                                        //    SendHtmlFormattedEmail(regdlist.Email, "Online Test Result | ODM Public School", body);
                                        //}
                                        //if (stdbasicdtl.Zoom_Mail_ID != "" && stdbasicdtl.Zoom_Mail_ID != null)
                                        //{
                                        //    string body = PopulateBodyResultSheet(regdlist.Customer_Name, examtypename, shortlink);
                                        //    SendHtmlFormattedEmail(stdbasicdtl.Zoom_Mail_ID, "Online Test Result | ODM Public School", body);
                                        //}
                                    }
                                }
                                else if (typ == 3)
                                {
                                    string schoolno = dr["ScL.\nNo."].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Marksheet _Marksheet = new Tbl_Student_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamTypeId = examtype;
                                        _Marksheet.Percentage_Objective = dr["TOT OBJ     %"].ToString();
                                        _Marksheet.Percentage_Subjective = dr["TOT SUB    %"].ToString();
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["SL.\nNO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC."].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Objective = dr["TOT OBJ (500)"].ToString();
                                        _Marksheet.Total_Mark_Subjective = dr["TOT SUB (500)"].ToString();
                                        DbContext.Tbl_Student_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 4; y <= (dt.Columns.Count - 7); y = y + 3)
                                        {
                                            Tbl_StudentMarksheetDet marksheetDet = new Tbl_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.ObjectiveScore = dr[dt.Columns[y].ColumnName + "1"].ToString();
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtypename;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);
                                        //string url = "http://learn.odmps.org/Reports/Reports.aspx?type=School_Marksheet_Report&Id=" + markid;
                                        ////bitly b = new bitly();
                                        //string shortlink = ToTinyURLS(url);
                                        //if (regdlist.Mobile != "")
                                        //{
                                        //    Sendsmstoemployee(regdlist.Mobile, "Dear " + regdlist.Customer_Name + ", \nPlease find the Online Test Result of your exam " + examtypename + ". Click on the link and save the Test Result.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                                        //}

                                        //if (regdlist.Email != "")
                                        //{
                                        //    string body = PopulateBodyResultSheet(regdlist.Customer_Name, examtypename, shortlink);
                                        //    SendHtmlFormattedEmail(regdlist.Email, "Online Test Result | ODM Public School", body);
                                        //}
                                        //if (stdbasicdtl.Zoom_Mail_ID != "" && stdbasicdtl.Zoom_Mail_ID != null)
                                        //{
                                        //    string body = PopulateBodyResultSheet(regdlist.Customer_Name, examtypename, shortlink);
                                        //    SendHtmlFormattedEmail(stdbasicdtl.Zoom_Mail_ID, "Online Test Result | ODM Public School", body);
                                        //}
                                    }
                                }
                                else if (typ == 4)
                                {
                                    string schoolno = dr["ScL.\nNo."].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        var section = DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == regdlist.SectionId).FirstOrDefault().SectionName;
                                        Tbl_Student_Marksheet _Marksheet = new Tbl_Student_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamTypeId = examtype;
                                        _Marksheet.Percentage_Objective = dr["TOT OBJ     %"].ToString();
                                        _Marksheet.Percentage_Subjective = dr["TOT SUB    %"].ToString();
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["SL.\nNO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = section;
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Objective = dr["TOT-OBJ (500)"].ToString();
                                        _Marksheet.Total_Mark_Subjective = dr["TOT SUB (500)"].ToString();
                                        DbContext.Tbl_Student_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 9; y <= (dt.Columns.Count - 7); y = y + 3)
                                        {
                                            Tbl_StudentMarksheetDet marksheetDet = new Tbl_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.ObjectiveScore = dr[dt.Columns[y].ColumnName + "1"].ToString();
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtypename;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);
                                        //string url = "http://learn.odmps.org/Reports/Reports.aspx?type=School_Marksheet_Report&Id=" + markid;
                                        ////bitly b = new bitly();
                                        //string shortlink = ToTinyURLS(url);
                                        //if (regdlist.Mobile != "")
                                        //{
                                        //    Sendsmstoemployee(regdlist.Mobile, "Dear " + regdlist.Customer_Name + ", \nPlease find the Online Test Result of your exam " + examtypename + ". Click on the link and save the Test Result.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                                        //}

                                        //if (regdlist.Email != "")
                                        //{
                                        //    string body = PopulateBodyResultSheet(regdlist.Customer_Name, examtypename, shortlink);
                                        //    SendHtmlFormattedEmail(regdlist.Email, "Online Test Result | ODM Public School", body);
                                        //}
                                        //if (stdbasicdtl.Zoom_Mail_ID != "" && stdbasicdtl.Zoom_Mail_ID != null)
                                        //{
                                        //    string body = PopulateBodyResultSheet(regdlist.Customer_Name, examtypename, shortlink);
                                        //    SendHtmlFormattedEmail(stdbasicdtl.Zoom_Mail_ID, "Online Test Result | ODM Public School", body);
                                        //}
                                    }
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                        clsmarks.marksheetmailcls = ss;
                        SendEmailInBackgroundThread(clsmarks);
                        TempData["SuccessMessage"] = "Student Details Entered Successfully";
                        return RedirectToAction("UploadOnlineExamReport");
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("UploadOnlineExamReport");
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("UploadOnlineExamReport");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("UploadOnlineExamReport");
            }
        }
        public ActionResult sendmarksheetmail()
        {
            clsmarkslist clsmarks = new clsmarkslist();
            List<marksheetmailcls> ss = new List<marksheetmailcls>();
            List<OnlineExamReportModel> ob = (List<OnlineExamReportModel>)Session["marksheetreport"];
            var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true).ToList();

            var regdlist = DbContext.tbl_DC_Registration.ToList();
            foreach (var a in ob)
            {
                var sm = regdetlist.Where(m => m.School_No == a.School_No).FirstOrDefault();
                if (sm != null)
                {
                    var reg = regdlist.Where(m => m.Regd_ID == sm.Regd_ID).FirstOrDefault();
                    marksheetmailcls marksheetmailcls = new marksheetmailcls();
                    marksheetmailcls.customername = a.StudentName;
                    marksheetmailcls.examtypename = a.OnlineExamTypeName;
                    marksheetmailcls.mail = reg.Email;
                    marksheetmailcls.markId = a.StudentmarksheetId;
                    marksheetmailcls.mobile = reg.Mobile;
                    marksheetmailcls.zoommail = sm.Zoom_Mail_ID;
                    ss.Add(marksheetmailcls);
                }
            }
            clsmarks.marksheetmailcls = ss;
            SendEmailInBackgroundThread(clsmarks);

            TempData["SuccessMessage"] = "Mail Send Successfully";
            return RedirectToAction("OnlineExamReportList");
        }
        public class marksheetmailcls
        {
            public long markId { get; set; }
            public string mobile { get; set; }
            public string mail { get; set; }
            public string zoommail { get; set; }
            public string examtypename { get; set; }
            public string customername { get; set; }

        }
        public class clsmarkslist
        {
            public List<marksheetmailcls> marksheetmailcls { get; set; }
        }
        public void SendEmailInBackgroundThread(clsmarkslist mailMessage)
        {
            Thread bgThread = new Thread(new ParameterizedThreadStart(sendbackgroundmail));
            bgThread.IsBackground = true;
            bgThread.Start(mailMessage);
        }
        public void sendbackgroundmail(Object obj1)
        {
            clsmarkslist objt = (clsmarkslist)obj1;
            foreach (var obj in objt.marksheetmailcls)
            {
                string url = "http://learn.odmps.org/Reports/Reports.aspx?type=School_Marksheet_Report&Id=" + obj.markId;
                //bitly b = new bitly();
                string shortlink = ToTinyURLS(url);
                if (obj.mobile != "")
                {
                    Sendsmstoemployee(obj.mobile, "Dear " + obj.customername + ", \nPlease find the Online Test Result of your exam " + obj.examtypename + ". Click on the link and save the Test Result.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                }

                if (obj.mail != "")
                {
                    string body = PopulateBodyResultSheet(obj.customername, obj.examtypename, shortlink);
                    SendHtmlFormattedEmail(obj.mail, "Online Test Result | ODM Public School", body);
                }

                if (obj.zoommail != "" && obj.zoommail != null)
                {
                    string body = PopulateBodyResultSheet(obj.customername, obj.examtypename, shortlink);
                    SendHtmlFormattedEmail(obj.zoommail, "Online Test Result | ODM Public School", body);
                }
            }
        }
        #endregion

        #region Upload Half-Yearly Online Exam Report

        public ActionResult OnlineHalfYearlyExamReportList(Guid? school, int? board, int? cls, Guid? sec, int? student, string session)
        {
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.schoolist = schools;
            ViewBag.s = school;
            ViewBag.b = board;
            ViewBag.c = cls;
            ViewBag.se = sec;
            ViewBag.f = student;
            var Marksheet = DbContext.Tbl_Student_Halfyearly_Marksheet.Where(a => a.Is_Active == true).ToList();

            var res = (from a in Marksheet
                       select new OnlineExamReportModel
                       {
                           StudentmarksheetId = a.StudentmarksheetId,
                           OnlineExamTypeName = a.OnlineExamType,
                           RegdId = a.RegdId,
                           Session = a.Session,
                           section = a.section,
                           Total_Mark_Objective = a.Total_Mark_Objective.ToString(),
                           Total_Mark_Subjective = a.Total_Mark_Subjective.ToString(),
                           Percentage_Subjective = a.Percentage_Subjective.ToString(),
                           Percentage_Objective = a.Percentage_Objective.ToString(),
                           Roll_No = a.Roll_No,
                           School_No = a.School_No,
                           StudentName = a.StudentName,
                           ClassName = a.ClassName,
                           typeId = a.typeId,
                           ModifiedOn = a.ModifiedOn,
                           InsertedOn = a.InsertedOn.Value.ToString("yyyy-MM-dd"),
                           Batch = a.Batch,
                           SubjectCombination = a.SubjectCombination
                       }
                      ).ToList();

            if (cls != null)
            {
                string classname = DbContext.tbl_DC_Class.Where(v => v.Class_Id == cls).FirstOrDefault().Class_Name;
                res = res.Where(a => a.ClassName == classname).ToList();
            }
            if (sec != null)
            {
                string sessionname = DbContext.tbl_DC_Class_Section.Where(v => v.SectionId == sec).FirstOrDefault().SectionName;
                res = res.Where(a => a.section == sessionname).ToList();
            }
            if (student != null)
            {
                res = res.Where(a => a.RegdId == student).ToList();
            }


            if (session != null && session != "All")
            {
                res = res.Where(a => a.Session == session).ToList();
            }
            Session["marksheetreport"] = res;
            return View(res);
        }
        public ActionResult UploadHalfYearlyOnlineExamReport()
        {
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.schools = schools;


            return View();
        }
        [HttpPost]
        public ActionResult UploadstudentHalfYearlymarksheetReport(int typ, string examtype, string remark, Guid school, int board, int total, int cls, string session, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("UploadHalfYearlyOnlineExamReport");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);

                        XLWorkbook workbook = new XLWorkbook(path);


                        IXLWorksheet xLWorksheet = workbook.Worksheet(1);
                        DataTable dt = new DataTable();
                        var i = 0;
                        foreach (IXLRow row in xLWorksheet.Rows())
                        {
                            if (i == 1)
                            {
                                //foreach (IXLCell cell in row.Cells())
                                //{
                                int m = 1;
                                for (int a = 0; a < row.Cells().Count(); a++)
                                {
                                    //if (m <= 3)
                                    //{
                                    if (row.Cells().ToList()[a].Value.ToString() == "")
                                    {
                                        dt.Columns.Add(row.Cells().ToList()[a - m].Value.ToString().Trim() + m);
                                        m++;
                                    }
                                    else
                                    {
                                        dt.Columns.Add(row.Cells().ToList()[a].Value.ToString().Trim());
                                        m = 1;
                                    }
                                    //}
                                }
                                //}
                            }
                            else if (i > 1)
                            {
                                dt.Rows.Add();
                                int j = 0;
                                foreach (IXLCell cell in row.Cells())
                                {
                                    dt.Rows[dt.Rows.Count - 1][j] = cell.Value.ToString();
                                    j++;
                                }
                            }
                            i = i + 1;
                        }

                        var data = dt;
                        bool firstrow = true;
                        bool firstrow1 = true;
                        List<string> schnos = new List<string>();
                        //foreach (DataRow dr in dt.Rows)
                        //{
                        //    if (firstrow1 == true)
                        //    {
                        //        firstrow1 = false;
                        //    }
                        //    else
                        //    {
                        //        string schoolno = dr["SCHOOL NO."].ToString();
                        //        var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                        //        if (regdetlist == null)
                        //        {
                        //            schnos.Add(schoolno);
                        //        }
                        //    }
                        //}

                        var result = schnos;

                        clsmarkslist clsmarks = new clsmarkslist();
                        List<marksheetmailcls> ss = new List<marksheetmailcls>();

                        foreach (DataRow dr in dt.Rows)
                        {
                            if (firstrow == true)
                            {
                                firstrow = false;
                            }
                            else
                            {
                                if (typ == 2)
                                {

                                    string schoolno = dr["SCHOOL NO."].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Halfyearly_Marksheet _Marksheet = new Tbl_Student_Halfyearly_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Percentage_Subjective = Convert.ToDecimal(dr["% OF MARK"].ToString());
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["ROLL NO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC"].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Subjective = Convert.ToDecimal(dr["TOTAL MARK"].ToString());
                                        _Marksheet.Total_Mark_Objective = total;
                                        _Marksheet.Remarks = remark;

                                        DbContext.Tbl_Student_Halfyearly_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 5; y < (dt.Columns.Count - 2); y++)
                                        {
                                            Tbl_Halfyearly_StudentMarksheetDet marksheetDet = new Tbl_Halfyearly_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;

                                            DbContext.Tbl_Halfyearly_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        // var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        //sajdgj
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtype;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }

                                }
                                else if (typ == 1)
                                {
                                    string schoolno = dr["SCHOOL NO"].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Halfyearly_Marksheet _Marksheet = new Tbl_Student_Halfyearly_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Percentage_Subjective = Convert.ToDecimal(dr["% Of Mark"].ToString());
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["ROLL NO"].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC"].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Subjective = Convert.ToDecimal(dr["TOTAL MARK(500)"].ToString());
                                        _Marksheet.Total_Mark_Objective = total;
                                        _Marksheet.Remarks = remark;
                                        DbContext.Tbl_Student_Halfyearly_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 4; y < (dt.Columns.Count - 2); y++)
                                        {
                                            Tbl_Halfyearly_StudentMarksheetDet marksheetDet = new Tbl_Halfyearly_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_Halfyearly_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        //   var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtype;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }
                                }
                                else if (typ == 3)
                                {
                                    string schoolno = dr["SCHOOL NO"].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Halfyearly_Marksheet _Marksheet = new Tbl_Student_Halfyearly_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Percentage_Subjective = Convert.ToDecimal(dr["% Of Mark"].ToString());
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["SL.NO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC."].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Subjective = Convert.ToDecimal(dr["TOTAL MARK (500)"].ToString());
                                        _Marksheet.Total_Mark_Objective = total;
                                        _Marksheet.Remarks = remark;
                                        DbContext.Tbl_Student_Halfyearly_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 4; y < (dt.Columns.Count - 2); y++)
                                        {
                                            Tbl_Halfyearly_StudentMarksheetDet marksheetDet = new Tbl_Halfyearly_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_Halfyearly_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        // var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtype;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }
                                }
                                else if (typ == 4)
                                {
                                    string schoolno = dr["SCHOOL NO"].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        var section = DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == regdlist.SectionId).FirstOrDefault().SectionName;
                                        Tbl_Student_Halfyearly_Marksheet _Marksheet = new Tbl_Student_Halfyearly_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Percentage_Subjective = Convert.ToDecimal(dr["% Of Mark"].ToString());
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["SL.NO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = section;
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Subjective = Convert.ToDecimal(dr["TOTAL MARK (500)"].ToString());
                                        _Marksheet.Total_Mark_Objective = total;
                                        _Marksheet.Remarks = remark;


                                        DbContext.Tbl_Student_Halfyearly_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 9; y < (dt.Columns.Count - 2); y++)
                                        {
                                            Tbl_Halfyearly_StudentMarksheetDet marksheetDet = new Tbl_Halfyearly_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_Halfyearly_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        // var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtype;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }
                                }
                                else if (typ == 5)
                                {
                                    string schoolno = dr["SCHOOL NO"].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Halfyearly_Marksheet _Marksheet = new Tbl_Student_Halfyearly_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Batch = dr["BATCH"].ToString();
                                        //_Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Percentage_Subjective = Convert.ToDecimal(dr["% Of Mark"].ToString());
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["SL.NO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC."].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Subjective = Convert.ToDecimal(dr["TOTAL MARK (500)"].ToString());
                                        _Marksheet.Total_Mark_Objective = total;
                                        _Marksheet.Remarks = remark;
                                        DbContext.Tbl_Student_Halfyearly_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 5; y < (dt.Columns.Count - 2); y++)
                                        {
                                            Tbl_Halfyearly_StudentMarksheetDet marksheetDet = new Tbl_Halfyearly_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;

                                            DbContext.Tbl_Halfyearly_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        // var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtype;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }
                                }
                                else if (typ == 6)
                                {
                                    string schoolno = dr["SCHOOL NO"].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        var section = DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == regdlist.SectionId).FirstOrDefault().SectionName;
                                        Tbl_Student_Halfyearly_Marksheet _Marksheet = new Tbl_Student_Halfyearly_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.Batch = dr["BATCH"].ToString();
                                        _Marksheet.SubjectCombination = dr["SUB"].ToString();
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Percentage_Subjective = Convert.ToDecimal(dr["% Of Mark"].ToString());
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["SL.NO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = section;
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Subjective = Convert.ToDecimal(dr["TOTAL MARK (500)"].ToString());
                                        _Marksheet.Total_Mark_Objective = total;
                                        _Marksheet.Remarks = remark;
                                        DbContext.Tbl_Student_Halfyearly_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 10; y < (dt.Columns.Count - 2); y++)
                                        {
                                            Tbl_Halfyearly_StudentMarksheetDet marksheetDet = new Tbl_Halfyearly_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_Halfyearly_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        // var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtype;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                        clsmarks.marksheetmailcls = ss;
                        SendHalfYearlyEmailInBackgroundThread(clsmarks);
                        TempData["SuccessMessage"] = "Student Details Entered Successfully";
                        return RedirectToAction("UploadHalfYearlyOnlineExamReport");
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("UploadHalfYearlyOnlineExamReport");
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("UploadHalfYearlyOnlineExamReport");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("UploadHalfYearlyOnlineExamReport");
            }
        }
        public ActionResult HalfYearlysendmarksheetmail()
        {
            clsmarkslist clsmarks = new clsmarkslist();
            List<marksheetmailcls> ss = new List<marksheetmailcls>();
            List<OnlineExamReportModel> ob = (List<OnlineExamReportModel>)Session["marksheetreport"];
            var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true).ToList();

            var regdlist = DbContext.tbl_DC_Registration.ToList();
            foreach (var a in ob)
            {
                var sm = regdetlist.Where(m => m.School_No == a.School_No).FirstOrDefault();
                if (sm != null)
                {
                    var reg = regdlist.Where(m => m.Regd_ID == sm.Regd_ID).FirstOrDefault();
                    marksheetmailcls marksheetmailcls = new marksheetmailcls();
                    marksheetmailcls.customername = a.StudentName;
                    marksheetmailcls.examtypename = a.OnlineExamTypeName;
                    marksheetmailcls.mail = reg.Email;
                    marksheetmailcls.markId = a.StudentmarksheetId;
                    marksheetmailcls.mobile = reg.Mobile;
                    marksheetmailcls.zoommail = sm.Zoom_Mail_ID;
                    ss.Add(marksheetmailcls);
                }
            }
            clsmarks.marksheetmailcls = ss;
            SendHalfYearlyEmailInBackgroundThread(clsmarks);

            TempData["SuccessMessage"] = "Mail Send Successfully";
            return RedirectToAction("OnlineHalfYearlyExamReportList");
        }


        public void SendHalfYearlyEmailInBackgroundThread(clsmarkslist mailMessage)
        {
            Thread bgThread = new Thread(new ParameterizedThreadStart(sendHalfYearlybackgroundmail));
            bgThread.IsBackground = true;
            bgThread.Start(mailMessage);
        }
        public void sendHalfYearlybackgroundmail(Object obj1)
        {
            clsmarkslist objt = (clsmarkslist)obj1;
            foreach (var obj in objt.marksheetmailcls)
            {
                string url = "http://learn.odmps.org/Reports/Reports.aspx?type=School_Half_Yearly_Marksheet_Report&Id=" + obj.markId;
                //bitly b = new bitly();
                string shortlink = ToTinyURLS(url);
                if (obj.mobile != "")
                {
                    Sendsmstoemployee(obj.mobile, "Dear " + obj.customername + ", \nPlease find the Online Test Result of your exam " + obj.examtypename + ". Click on the link and save the Test Result.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                }

                if (obj.mail != "")
                {
                    string body = PopulateBodyResultSheet(obj.customername, obj.examtypename, shortlink);
                    SendHtmlFormattedEmail(obj.mail, "Online Test Result | ODM Public School", body);
                }

                if (obj.zoommail != "" && obj.zoommail != null)
                {
                    string body = PopulateBodyResultSheet(obj.customername, obj.examtypename, shortlink);
                    SendHtmlFormattedEmail(obj.zoommail, "Online Test Result | ODM Public School", body);
                }
            }
        }
        #endregion

    }
}