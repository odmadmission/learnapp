﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class BookMarkController : ApiController
    {
        DigiChampsEntities db = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public class ErrorResult
        {
            public ErrorResponse Error { get; set; }
        }

        public class ErrorResponse
        {
            public string Message { get; set; }
        }

        public class SuccessResponse
        {
            public string Message { get; set; }
        }
        public class SuccessResult
        {
            public List<BookMark> BookMarks { get; set; }
        }
        public class BookMark
        {
            public int? BookMark_ID { get; set; }
            public string BookMark_Name { get; set; }
            public string BookMark_Path { get; set; }
            public string BookMark_Description { get; set; }
        }
        public class BookMark_List
        {
            public List<BookMark> list { get; set; }
        }
        public class success_BookMark
        {
            public BookMark_List Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetBookMark()
        {
            try
            {
                var obj = new success_BookMark
                {
                    Success = new BookMark_List
                    {
                        list = (from c in db.tbl_DC_BookMark.Where(x => x.Is_Active == true && x.Is_Delete == false)
                                select new BookMark
                                {

                                    BookMark_ID = c.BookMark_ID,
                                    BookMark_Path = c.BookMark_Path,
                                    BookMark_Description = c.BookMark_Description
                                }).ToList(),
                    }
                };

                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }
    }
}
