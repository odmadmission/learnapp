﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class FeedController : ApiController
    {
        DigiChampsEntities db = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public class ErrorResult
        {
            public ErrorResponse Error { get; set; }
        }

        public class ErrorResponse
        {
            public string Message { get; set; }
        }

        public class SuccessResponse
        {
            public string Message { get; set; }
        }
        public class SuccessResult
        {
            public List<Feed> Feeds { get; set; }
        }
        public class Feed
        {
            public int? Feed_ID { get; set; }
            public string Feed_Name { get; set; }
            public string Feed_Images { get; set; }
            public string Feed_Path { get; set; }
            public string Feed_Description { get; set; }
            public string FeedOnline { get; set; }
            public string FeedBeta { get; set; }

        }
        public class Feed_List
        {
            public List<Feed> list { get; set; }
        }
        public class success_Feed
        {
            public Feed_List Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetFeed()
        {
            try
            {
                var obj = new success_Feed
                {
                    Success = new Feed_List
                    {
                        list = (from c in db.tbl_DC_Feed.Where(x => x.Is_Active == true && x.Is_Delete == false)
                                select new Feed
                                {

                                    Feed_ID = c.Feed_ID,
                                    Feed_Name = c.Feed_Name,
                                    Feed_Images = c.Feed_Images,
                                    FeedOnline = "https://learn.odmps.org/Images/Feed/" + c.Feed_Images + "",
                                    FeedBeta = "http://beta.thedigichamps.com/Images/Feed/" + c.Feed_Images + "",
                                    Feed_Path = c.Feed_Path,
                                    Feed_Description = c.Feed_Description
                                }).OrderByDescending(c => c.Feed_ID).ToList(),
                    }
                };

                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }
    }
}
