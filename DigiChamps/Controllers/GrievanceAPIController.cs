﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace DigiChamps.Controllers
{
    public class GrievanceAPIController : ApiController
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();

        [HttpGet]
        public HttpResponseMessage Index()
        {
            return Request.CreateResponse(HttpStatusCode.OK,new { msg = "Index"});
        }

        [HttpGet]
        public HttpResponseMessage GetGrievanceByUserId(string userCode)
        {
            var grievanceList = DbContext.Grievances.Where(a => a.Active == true && a.InsertedId == userCode).ToList();
            var grievanceAttachmentList = DbContext.GrievanceAttachments.Where(a => a.Active == true).ToList();
          //  var moduleList = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();

            var res = (from a in grievanceList
                      // join c in moduleList on a.Module equals c.Module_ID
                       select new GrievanceModel
                       {
                           ID = a.ID,
                           ModuleId = a.Module,
                           Title = a.Title,
                           Description = a.Description,
                           FileName = grievanceAttachmentList.Where(s => s.GrivanceId == a.ID).Select(s => s.ID).FirstOrDefault() != 0 ?
                                         grievanceAttachmentList.Where(s => s.GrivanceId == a.ID).FirstOrDefault().FileName : "NA",
                           StudentId = a.StudentId,
                           TeacherId = a.TeacherId,
                           InsertedId = a.InsertedId,
                       }).ToList();
             

            return Request.CreateResponse(HttpStatusCode.OK, new { msg = "Success", GrievanceList = res });
        }

        [HttpPost]
        public HttpResponseMessage SaveGrievance(GrievanceModel gvModel)
        {
            try
            {
                Grievance grievanceModel = new Grievance();
                grievanceModel.Module = gvModel.ModuleId;
                grievanceModel.Title = gvModel.Title;
                grievanceModel.Description = gvModel.Description;
                grievanceModel.ResolvedDateTime = null;
                grievanceModel.InsertedDateTime = DateTime.Now;
                grievanceModel.Active = true;
                grievanceModel.InsertedId = gvModel.UserCode;
                grievanceModel.TeacherId = Convert.ToInt32(gvModel.TeacherId);
                grievanceModel.StudentId = Convert.ToInt32(gvModel.StudentId);
                grievanceModel.Class = gvModel.ClassId;
                grievanceModel.Subject = gvModel.SubjectId;
                grievanceModel.Section = gvModel.SectionId;
                grievanceModel.Text = gvModel.Text;

                DbContext.Grievances.Add(grievanceModel);
                DbContext.SaveChanges();

                int id = grievanceModel.ID;

                if (gvModel.Griv_img[0] != null)
                {

                    GrievanceAttachment attachment = new GrievanceAttachment();
                    string guid = Guid.NewGuid().ToString();
                    HttpResponseMessage result = null;
                    var httpRequest = HttpContext.Current.Request;
                    if (httpRequest.Files.Count > 0)
                    {
                        var docfiles = new List<string>();
                        foreach (string file in httpRequest.Files)
                        {
                            var postedFile = httpRequest.Files[file];
                            attachment.InsertedId = gvModel.UserCode;
                            attachment.GrivanceId = id;
                            attachment.InsertedDateTime = DateTime.Now;
                            attachment.Active = true;
                            var filePath = HttpContext.Current.Server.MapPath("~/Content/Grievance/" + postedFile.FileName);
                          //  var fileName = Path.GetFileName(gvModel.Griv_img[i].FileName.Replace(gvModel.Griv_img[i].FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            postedFile.SaveAs(filePath);
                            attachment.FileName = guid;
                          
                        }

                        DbContext.GrievanceAttachments.Add(attachment);
                        DbContext.SaveChanges();
                        result = Request.CreateResponse(HttpStatusCode.Created, docfiles);

                        return result;
                    }
                    else
                    {
                        result = Request.CreateResponse(HttpStatusCode.BadRequest);
                    }
                    
                   }

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "Success" });
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "Error" });
            }

        }

    }
}
