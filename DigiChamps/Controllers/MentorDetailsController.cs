﻿using ClosedXML.Excel;
using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DigiChamps.Controllers
{
    public class MentorDetailsController : Controller
    {
        //
        // GET: /MentorDetails/
        DigiChampsEntities DbContext = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();
        public ActionResult Index()
        {
            ViewBag.Closures = 19;
            ViewBag.mentorsalloted = 18;
            ViewBag.summary = 17;
            ViewBag.Mentors = 14;
            ViewBag.Mentorship = 11;
            return View();
        }
        public ActionResult UploadDetails()
        {
            return View();
        }
        public ActionResult DownloadSalesStudentExcel()
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();
            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Name", typeof(string));
            validationTable.Columns.Add("Mobile", typeof(string));
            validationTable.Columns.Add("AltMobile", typeof(string));
            validationTable.Columns.Add("Email", typeof(string));
            validationTable.Columns.Add("School", typeof(string));
            validationTable.Columns.Add("Board", typeof(Int32));
            validationTable.Columns.Add("Class", typeof(Int32));
            validationTable.Columns.Add("Section", typeof(string));
            validationTable.Columns.Add("DataSource", typeof(Int32));
            validationTable.Columns.Add("CollectedOn", typeof(DateTime));
            validationTable.Columns.Add("OtherData", typeof(string));
            validationTable.Columns.Add("Active", typeof(Int32));
            validationTable.Columns.Add("Status", typeof(string));
            validationTable.Columns.Add("ClassName", typeof(string));
            validationTable.Columns.Add("BoardName", typeof(string));
            validationTable.Columns.Add("Remarks", typeof(string));
            validationTable.TableName = "Sales_Details";
            var worksheet = oWB.AddWorksheet(validationTable);


            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=SalesStudent.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            TempData["SuccessMessage"] = "Successfully Downloaded Excel File";
            return RedirectToAction("SalesStudents");
        }
        public ActionResult UploadSalesStudent(DigiChamps.Controllers.AdminController.PDF_Class po)
        {
            if (ModelState.IsValid)
            {
                if (po.files.ToList().Count > 0)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(po.files[0].FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("SalesStudents");
                    }


                    if (po.files[0] != null && po.files[0].ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/mentor/pdf/28072018/" + po.files[0].FileName));
                        //save File
                        po.files[0].SaveAs(path);
                        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                        OleDbConnection connection = new OleDbConnection();
                        connection.ConnectionString = excelConnectionString;
                        OleDbDataAdapter command = new OleDbDataAdapter("select * from [Sales_Details$]", connection);
                        // connection.Open();
                        // Create DbDataReader to Data Worksheet
                        DataTable dt = new DataTable();
                        command.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {

                            foreach (DataRow dr in dt.Rows)
                            {
                                string mobile = dr["Mobile"].ToString();
                                if (DbContext.SalesAppStudents.Any(x => x.Mobile == mobile.Trim()))
                                {
                                    TempData["ErrorMessage"] += dr["Mobile"].ToString() + " Already Exists.";
                                }
                                else
                                {
                                    SalesAppStudent s = new SalesAppStudent();
                                    s.Name = dr["Name"].ToString();
                                    s.Mobile = dr["Mobile"].ToString();
                                    s.OtherData = dr["OtherData"].ToString();
                                    s.School = dr["School"].ToString();
                                    s.Section = dr["Section"].ToString();
                                    s.Status = 0;
                                    if (dr["Active"].ToString() != null && dr["Active"].ToString() != "NULL" && dr["Active"].ToString() != "")
                                    {
                                        s.Active = Convert.ToInt32(dr["Active"].ToString());
                                    }
                                    s.AltMobile = dr["AltMobile"].ToString();
                                    if (dr["Board"].ToString() != null && dr["Board"].ToString() != "NULL" && dr["Board"].ToString() != "")
                                    {
                                        s.Board = Convert.ToInt32(dr["Board"].ToString());
                                    }
                                    if (dr["Class"].ToString() != null && dr["Class"].ToString() != "NULL" && dr["Class"].ToString() != "")
                                    {
                                        s.Class = Convert.ToInt32(dr["Class"].ToString());
                                    }
                                    if (dr["CollectedOn"].ToString() != null && dr["CollectedOn"].ToString() != "NULL" && dr["CollectedOn"].ToString() != "")
                                    {
                                        s.CollectedOn = Convert.ToDateTime(dr["CollectedOn"].ToString());
                                    }
                                    if (dr["DataSource"].ToString() != null && dr["DataSource"].ToString() != "NULL" && dr["DataSource"].ToString() != "")
                                    {
                                        s.DataSource = Convert.ToInt32(dr["DataSource"].ToString());
                                    }
                                    s.Email = dr["Email"].ToString();
                                    s.InsertedOn = DateTime.Now;
                                    s.BoardName = dr["BoardName"].ToString();
                                    s.ClassName = dr["ClassName"].ToString();
                                    s.Remarks = dr["Remarks"].ToString();
                                    DbContext.SalesAppStudents.Add(s);
                                    DbContext.SaveChanges();
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                    }

                    TempData["SuccessMessage"] = "Sales Student Details Entered Successfully";
                    return RedirectToAction("SalesStudents");
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("SalesStudents");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("SalesStudents");
            }
        }

        public ActionResult StudentDetails()
        {
            var std = DbContext.View_All_Student_Details.ToList();
            var students = (from a in std
                            orderby a.Regd_ID descending
                            where a.STATUS=="A" && a.IS_ACCEPTED==true
                            select new DigiChamps.Controllers.AdminController.User_Details_Class
                            {
                                Customer_Name = a.Customer_Name,
                                Regd_ID = a.Regd_ID,
                                Regd_No = a.Regd_No,
                                Email = a.Email,
                                Mobile = a.Mobile,
                                Inserted = Convert.ToDateTime(a.Inserted_Date).ToString("dd/MM/yyyy"),
                                Organisation_Name = a.Organisation_Name == null ? "" : a.Organisation_Name,
                                Class_Name = a.Class_Name                              
                            }).ToList();
            ViewBag.StudentDetails = students;
            return View();
        }
        public ActionResult Firstcallsummary()
        {
            return View();
        }
        public class subject_Cls
        {
            public int Subject_Id { get; set; }
            public string Subject { get; set; }
            public string Classname { get; set; }
        }
        public ActionResult Allotmentor()
        {
            ViewBag.Getmentors = DbContext.tbl_DC_Teacher.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var subjects=DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var classes=DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            ViewBag.GetSubjects = (from a in subjects
                                   join b in classes on a.Class_Id equals b.Class_Id
                                   select new subject_Cls
                                   {
                                       Subject_Id = a.Subject_Id,
                                       Subject = a.Subject,
                                       Classname = b.Class_Name
                                   }).ToList();
            return View();
        }
    }
}
