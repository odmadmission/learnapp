﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class ScholarShipScratchsController : ApiController
    {

        DigiChampsEntities db = new DigiChampsEntities();



        public class Success
        {
            public ScholarShipScratch scratch {get;set;}
            public int? cart { get; set; }
        }

        [HttpGet]
        public HttpResponseMessage GetScratch(int RegId)
        {
            tbl_DC_ScholarShipScratch scholarShips = db.tbl_DC_ScholarShipScratch.
                Where(x => x.StudentID == RegId).FirstOrDefault();
            ScholarShipScratch scratch = null;
            if (scholarShips != null)
            {
                 scratch = new ScholarShipScratch();
                scratch.ID = scholarShips.ID;
                scratch.StudentID = scholarShips.StudentID;
                scratch.ScholarShipID = scholarShips.ScholarShipID;
                scratch.Active = scholarShips.Active;
                scratch.Deleted = scholarShips.Deleted;
                scratch.InsertDate = scholarShips.InsertDate;
                

            }
            int CartSize =
                (from a in db.tbl_DC_Cart.Where(x => x.Regd_ID == RegId
                    && x.Is_Active == true && x.Is_Deleted == false && x.In_Cart == true)
                 join b in db.tbl_DC_Package.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                 on a.Package_ID equals b.Package_ID
                 select new CartData
                 {
                     cart=a.Package_ID
                 }
                ).ToList().Count();
            var obj = new Success()
            {
                scratch = scratch,
                cart = CartSize
            };
            
          
            return Request.CreateResponse(HttpStatusCode.OK, obj);
        }

        public class CartData
        {
            public int? cart;
        }

        [HttpGet]
        public HttpResponseMessage SaveScratch(int RegId,int ScholarId)
        {

            tbl_DC_ScholarShipScratch scratch = new tbl_DC_ScholarShipScratch();
            scratch.Active =true;
            scratch.Deleted = false;
            scratch.ScholarShipID = ScholarId;
            scratch.StudentID = RegId;
            scratch.InsertDate = System.DateTime.Now;
            db.tbl_DC_ScholarShipScratch.Add(scratch);
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, scratch);
        }

       
    }
}
