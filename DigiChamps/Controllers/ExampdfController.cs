﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace DigiChamps.Controllers
{
    public class ExampdfController : ApiController
    {
        DigiChampsEntities dbContext = new DigiChampsEntities();
        [HttpPost]
        public HttpResponseMessage UploadExamPDF()
        {
            try
            {
                var httprequest = HttpContext.Current.Request;
                int Exam_ID = Convert.ToInt32(httprequest.Form["Exam_ID"]);
                int _student_id = Convert.ToInt32(httprequest.Form["Regd_ID"]);
                string guid = Guid.NewGuid().ToString();
                int id = 0;
                tbl_DC_PdfExamStudent ob = dbContext.tbl_DC_PdfExamStudent.Where(a => a.Regd_Id == _student_id && a.PdfExamId == Exam_ID && a.Is_Active == true).FirstOrDefault();
                if (ob != null)
                {
                    ob.Modified_By = _student_id;
                    ob.Modified_Date = DateTime.Now;
                    ob.Is_Active = true;
                    ob.Is_Deleted = false;
                    ob.PdfExamId = Exam_ID;
                    ob.Regd_Id = _student_id;
                    if (httprequest.Files.Count > 0)
                    {
                        foreach (string files in httprequest.Files)
                        {
                            var postedfile = httprequest.Files[files];
                            var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/Answerpdf/"), (guid + postedfile.FileName));
                            postedfile.SaveAs(path);
                            //docfile.Add(path);
                            ob.File_Name = guid + postedfile.FileName;
                        }
                    }
                    dbContext.SaveChanges();

                }
                else
                {
                    ob = new tbl_DC_PdfExamStudent();
                    ob.Inserted_Date = DateTime.Now;
                    ob.Inserted_By = _student_id;
                    ob.Modified_By = _student_id;
                    ob.Modified_Date = DateTime.Now;
                    ob.Is_Active = true;
                    ob.Is_Deleted = false;
                    ob.PdfExamId = Exam_ID;
                    ob.Regd_Id = _student_id;
                    if (httprequest.Files.Count > 0)
                    {
                        foreach (string files in httprequest.Files)
                        {
                            var postedfile = httprequest.Files[files];
                            var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/Answerpdf/"), (guid + postedfile.FileName));
                            postedfile.SaveAs(path);
                            //docfile.Add(path);
                            ob.File_Name = guid + postedfile.FileName;
                        }
                    }
                    dbContext.tbl_DC_PdfExamStudent.Add(ob);
                    dbContext.SaveChanges();
                    id = ob.PdfExamStudent_Id;
                }
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        [HttpGet]
        public HttpResponseMessage DeleteExamPdf(int id)
        {
            try
            {
                var ab = dbContext.tbl_DC_PdfExamStudent.Where(a => a.PdfExamStudent_Id == id).FirstOrDefault();
                ab.Is_Active = false;
                ab.Is_Deleted = true;
                ab.Modified_Date = DateTime.Now;
                dbContext.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        public class exampdfcls
        {
            public int Worksheet_Id { get; set; }
            public int Regd_ID { get; set; }
            public string Student_Name { get; set; }
            public int Board_Id { get; set; }
            public string Board_Name { get; set; }
            public int Class_Id { get; set; }
            public string Class_Name { get; set; }
            public Nullable<System.Guid> SectionId { get; set; }
            public string Section_Name { get; set; }
            public int Subject_Id { get; set; }
            public string Subject_Name { get; set; }
            public int PdfExam_Id { get; set; }
            public string Name { get; set; }
            public string File_Name { get; set; }

            public int Total_Mark { get; set; }
            public Nullable<System.DateTime> Inserted_Date { get; set; }

            public Nullable<System.DateTime> Modified_Date { get; set; }
            public DateTime StartTime { get; set; }

            public DateTime EndTime { get; set; }

            public List<studentanswercls> answers { get; set; }
        }
        public class studentanswercls
        {
            public int? Verified_By { get; set; }
            public string Verified_By_Name { get; set; }

            public string File_Name { get; set; }
            public DateTime? Modified_Date { get; set; }
            public string Path { get; set; }
            public int Total_Mark { get; set; }
            public int PdfExamStudent_Id { get; set; }
            public int PdfExamId { get; set; }
            public int Regd_Id { get; set; }
            public string student_name { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetallExampdf(int classid, int subjectid)
        {
            try
            {
                var pdfexam = dbContext.tbl_DC_PdfExam.Where(a => a.Is_Active == true && a.Class_Id == classid && a.Subject_Id == subjectid).ToList();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();
                var pdfexamstudent = dbContext.tbl_DC_PdfExamStudent.Where(a => a.Is_Active == true).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var res = (from a in pdfexam
                           select new exampdfcls
                           {
                               Board_Id = a.Board_Id,
                               Class_Id = a.Class_Id,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Subject_Id = a.Subject_Id,
                               Total_Mark = a.Total_Mark,
                               PdfExam_Id = a.PdfExam_Id,
                               StartTime = a.StartTime,
                               EndTime = a.EndTime,
                               Name = a.Name,
                               File_Name = "/Images/Exampdf/" + a.File_Name,
                               answers = (from k in pdfexamstudent
                                          join l in students on k.Regd_Id equals l.Regd_ID
                                          where k.PdfExamId == a.PdfExam_Id
                                          select new studentanswercls
                                          {
                                              File_Name = k.File_Name,
                                              Modified_Date = k.Modified_Date,
                                              Path = "/Images/Answerpdf/" + k.File_Name,
                                              PdfExamStudent_Id = k.PdfExamStudent_Id,
                                              PdfExamId = k.PdfExamId,
                                              Regd_Id = k.Regd_Id,
                                              Total_Mark = k.Total_Mark,
                                              Verified_By = k.Verified_By,
                                              Verified_By_Name = k.Verified_By != null ? teachers.Where(m => m.TeacherId == k.Verified_By).FirstOrDefault().Name : "",
                                              student_name = l.Customer_Name
                                          }).ToList(),
                           }).ToList();

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "S", examlist = res });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "E" });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetallExampdfByRegdId(int classid, int subjectid, int regdId)
        {
            try
            {
                var pdfexam = dbContext.tbl_DC_PdfExam.Where(a => a.Is_Active == true && a.Class_Id == classid && a.Subject_Id == subjectid).ToList();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();
                var pdfexamstudent = dbContext.tbl_DC_PdfExamStudent.Where(a => a.Is_Active == true).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var res = (from a in pdfexam
                           select new exampdfcls
                           {
                               Board_Id = a.Board_Id,
                               Class_Id = a.Class_Id,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Subject_Id = a.Subject_Id,
                               Total_Mark = a.Total_Mark,
                               PdfExam_Id = a.PdfExam_Id,
                               StartTime = a.StartTime,
                               EndTime = a.EndTime,
                               Name = a.Name,
                               File_Name = "/Images/Exampdf/" + a.File_Name,
                               answers = (from k in pdfexamstudent
                                          join l in students on k.Regd_Id equals l.Regd_ID
                                          where k.PdfExamId == a.PdfExam_Id && k.Regd_Id == regdId
                                          select new studentanswercls
                                          {
                                              File_Name = k.File_Name,
                                              Modified_Date = k.Modified_Date,
                                              Path = "/Images/Answerpdf/" + k.File_Name,
                                              PdfExamStudent_Id = k.PdfExamStudent_Id,
                                              PdfExamId = k.PdfExamId,
                                              Regd_Id = k.Regd_Id,
                                              Total_Mark = k.Total_Mark,
                                              Verified_By = k.Verified_By,
                                              Verified_By_Name = k.Verified_By != null ? teachers.Where(m => m.TeacherId == k.Verified_By).FirstOrDefault().Name : "",
                                              student_name = l.Customer_Name
                                          }).ToList(),
                           }).ToList();



                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "S", pdflist = res });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "E" });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetExampdfById(int id, int regdid)
        {
            try
            {
                var pdfexam = dbContext.tbl_DC_PdfExam.Where(a => a.Is_Active == true && a.PdfExam_Id == id).ToList();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();
                var pdfexamstudent = dbContext.tbl_DC_PdfExamStudent.Where(a => a.Is_Active == true).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var res = (from a in pdfexam
                           select new exampdfcls
                           {
                               Board_Id = a.Board_Id,
                               Class_Id = a.Class_Id,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Subject_Id = a.Subject_Id,
                               Total_Mark = a.Total_Mark,
                               PdfExam_Id = a.PdfExam_Id,
                               Name = a.Name,
                               StartTime=a.StartTime,
                               EndTime=a.EndTime,
                               File_Name = "/Images/Exampdf/" + a.File_Name,
                               answers = (from k in pdfexamstudent
                                          join l in students on k.Regd_Id equals l.Regd_ID
                                          where k.PdfExamId == a.PdfExam_Id && k.Regd_Id == regdid
                                          select new studentanswercls
                                          {
                                              File_Name = k.File_Name,
                                              Modified_Date = k.Modified_Date,
                                              Path = "/Images/Answerpdf/" + k.File_Name,
                                              PdfExamStudent_Id = k.PdfExamStudent_Id,
                                              PdfExamId = k.PdfExamId,
                                              Regd_Id = k.Regd_Id,
                                              Total_Mark = k.Total_Mark,
                                              Verified_By = k.Verified_By,
                                              Verified_By_Name = k.Verified_By != null ? teachers.Where(m => m.TeacherId == k.Verified_By).FirstOrDefault().Name : "",
                                              student_name = l.Customer_Name
                                          }).ToList(),
                           }).FirstOrDefault();

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "S", worksheet = res });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "E" });
            }
        }
         [HttpPost]
        public HttpResponseMessage UpdateFolder()
        {
            DirectoryInfo di = new DirectoryInfo("E:/learn.odmps.org/Images/Worksheet");
            FileInfo[] files = di.GetFiles("*.pdf");
            string path = "";
            int count = 0;
            foreach (FileInfo f in files)
                {
                    tbl_DC_Worksheet_Attachment attachment = dbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Path == f.Name).FirstOrDefault();

                  
                if(attachment!=null)
                {
                    path = attachment.Worksheet_Attachment_Id+"";
                    f.MoveTo("E:/learn.odmps.org/Images/Worksheet/"+ attachment.Worksheet_Attachment_Id + ".pdf");
                    count++;
                    attachment.Path = attachment.Worksheet_Attachment_Id + ".pdf";
                    attachment.File_Name = attachment.Worksheet_Attachment_Id + ".pdf";

                    dbContext.SaveChanges();
                }
               }


            return Request.CreateResponse(HttpStatusCode.OK, new { msg = files.Count() + ":" + count + ":" + path });
        }

         [HttpPost]
         public HttpResponseMessage UpdateSecSchool()
         {
            var  regs = dbContext.tbl_DC_Registration.ToList();
            foreach (tbl_DC_Registration reg in regs)
             {
                 var regid = reg.Regd_ID;
                 var sectionid = dbContext.tbl_DC_Class_Section.Where(a => a.SectionId==reg.SectionId&& a.IsActive == true).Select(a => a.Class_Id).FirstOrDefault();
                 if (sectionid != null && !sectionid.Equals("null"))
                 {
                     tbl_DC_Registration_Dtl m = dbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == regid && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                     if (m != null)
                     {
                         m.Board_ID = 1;
                         m.Class_ID = sectionid;
                         m.Modified_Date = DateTime.Now;
                         m.Modified_By = reg.Regd_ID.ToString();
                         dbContext.SaveChanges();
                     }

                     var worksheet = dbContext.tbl_DC_Worksheet.Where(a => a.Regd_ID == regid && a.Is_Active == true && a.Is_Deleted == false).ToList();
                     foreach (tbl_DC_Worksheet wrk in worksheet)
                     {
                         wrk.Board_Id = 1;
                         wrk.Class_Id = sectionid.Value;
                         wrk.SectionId = reg.SectionId;
                         dbContext.Entry(wrk).State = System.Data.Entity.EntityState.Modified;

                     }
                     dbContext.SaveChanges();
                     var ticket = dbContext.tbl_DC_Ticket.Where(a => a.Student_ID == regid).ToList();
                     foreach (tbl_DC_Ticket wrk in ticket)
                     {
                         wrk.Board_ID = 1;
                         wrk.Class_ID = sectionid.Value;

                         dbContext.Entry(wrk).State = System.Data.Entity.EntityState.Modified;
                     }
                     dbContext.SaveChanges();
                 }
             }


            return Request.CreateResponse(HttpStatusCode.OK, new { msg = regs.Count()+"" });
         }


        [HttpPost]
        public HttpResponseMessage ScoreToExampdf(int id, int teacherId, int score)
        {
            try
            {
                tbl_DC_PdfExamStudent wrk = dbContext.tbl_DC_PdfExamStudent.Where(a => a.PdfExamStudent_Id == id).FirstOrDefault();
                wrk.Total_Mark = score;
                wrk.Verified_By = teacherId;
                wrk.Modified_Date = DateTime.Now;
                dbContext.SaveChanges();
                //string subject=dbContext.tbl_DC_Subject.Where(a=>a.Subject_Id==wrk.Subject_Id)
                //var note = new PushNotiStatusMaster(title, body, obj1.Feed_Images, ids[i]);
                try
                {
                    var pdf = dbContext.tbl_DC_PdfExam.Where(a => a.PdfExam_Id == wrk.PdfExamId).FirstOrDefault();
                    var subject = dbContext.tbl_DC_Subject.Where(a => a.Subject_Id == pdf.Subject_Id).FirstOrDefault();
                    var pushnot = (from c in dbContext.tbl_DC_Registration.Where(x => x.Regd_ID == wrk.Regd_Id)

                                   select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                    string body = "Exam score#Your teacher has been updated score for {{subject}}";
                    string msg = body.ToString().Replace("{{subject}}", subject.Subject + "");

                    if (pushnot != null)
                    {
                        if (pushnot.Device_id != null)
                        {
                            var note = new PushNotiStatus(msg, pushnot.Device_id);
                        }
                    }
                }
                catch (Exception)
                {


                }
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success", examid = wrk.PdfExamId });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }


    }
}
