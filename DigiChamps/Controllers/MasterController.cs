﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DigiChamps.Controllers
{
    public class MasterController : Controller
    {
        DigiChampsEntities dbContext = new DigiChampsEntities();

        public ActionResult Index()
        {
            return View();
        }

       
        public class AcademicSessionModel
        {
            public AcademicSession Asession { get; set; }
            public long ID { get; set; }
            public DateTime? Fromdate { get; set; }
            public DateTime? Todate { get; set; }

        }
        public class StudentDetailsModel
        {
       
            public int? ClassId { get; set; }
            public long StudentId { get; set; }
            public long SessionId { get; set; }
            public Guid? SectionId { get; set; }
            public int Regd_ID { get; set; }
            public string StudentName { get; set; }
            public string SessionName { get; set; }
            public string SectionName { get; set; }
            public string ClassName { get; set; }
            public bool IsCurrentSession { get; set; }


        }
        #region-------------------Academic  Session------------------------------
        public ActionResult AcademicSession()
        {
            var result = dbContext.AcademicSessions.ToList();
            return View(result);
        }

        public ActionResult CreateAcademicSession()
        {           
            return View();
        }
        [HttpPost]
        public ActionResult SaveAcademicSession( string Active, string name,DateTime? fdate,DateTime? tdate)
        {
            var result = dbContext.AcademicSessions.Where(a => a.Active == true).ToList();
                     
            if (Active == "on")
            {
                for (int i = 0; i < result.ToList().Count(); i++)
                {
                    AcademicSession asm = result.ToList()[i];
                    asm.ModifiedDate = DateTime.Now;
                    asm.Active = false;
                    dbContext.SaveChanges();
                }
            }
                AcademicSession session = new AcademicSession();
                session.InsertedDate = DateTime.Now;
                session.InsertedId = 1;
                session.ModifiedDate = DateTime.Now;
                session.ModifiedId = 1;
                session.Active = true;
                session.Status = "Created";
                session.Name = name;
                session.FromDate = fdate;
                session.ToDate = tdate;
                dbContext.AcademicSessions.Add(session);
                dbContext.SaveChanges();                 

            return RedirectToAction(nameof(AcademicSession));
        }
        public ActionResult EditAcademicSession(long id)
        {
            if (id > 0)
            {
                var session = dbContext.AcademicSessions.Where(a => a.ID == id).FirstOrDefault();
                ViewBag.ID = id;
                ViewBag.Name = session.Name;
                ViewBag.Fromdate = session.FromDate;
                ViewBag.Todate = session.ToDate;
                ViewBag.active = session.Active;
                return View();
            }
            else
            {
                return RedirectToAction(nameof(AcademicSession));
            }
        }
        [HttpPost]
        public ActionResult UpdateAcademicSession(string Active, long id,string name, DateTime? fdate, DateTime? tdate)
        {
            if (Active == "on")
            {
                var res = dbContext.AcademicSessions.Where(a => a.ID != id);
                if (res != null)
                {
                    for (int i = 0; i < res.ToList().Count(); i++)
                    {
                        AcademicSession asm = res.ToList()[i];                      
                        asm.ModifiedDate = DateTime.Now;
                        asm.Active = false;
                        dbContext.SaveChanges();
                    }
                }

            }
            var session = dbContext.AcademicSessions.Where(a =>a.ID == id).FirstOrDefault();
            session.Name = name;
            session.FromDate = fdate;
            session.ToDate = tdate;
            session.Active = true;
            session.ModifiedDate = DateTime.Now;
            session.Status = "Updated";
            dbContext.SaveChanges();
            return RedirectToAction(nameof(AcademicSession));
        }

        public ActionResult DeleteAcademicSession(long id)
        {
            var session = dbContext.AcademicSessions.Where(a => a.Active == true && a.ID == id).FirstOrDefault();
            session.Active = false;
            session.Status = "Deleted";
            dbContext.SaveChanges();
            return RedirectToAction(nameof(AcademicSession));
        }
        #endregion
        #region-------------- Session Student----------------------------

       
        public ActionResult StudentSession()
        {
            var student = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var studentdtl = dbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false );
            var classs = dbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
            var sections = dbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var session = dbContext.AcademicSessions.Where(a => a.Active == true).ToList();
            var Academicsession = dbContext.AcademicSessions.Where(a => a.Active == true).ToList().FirstOrDefault();
            var result = dbContext.StudentSessions.Where(a => a.Active == true).ToList();
            var res = (from a in student
                       join b in studentdtl on a.Regd_ID equals b.Regd_ID
                       select new StudentDetailsModel
                       {
                           ClassId=b.Class_ID.Value,
                           SectionId=a.SectionId.Value,
                           Regd_ID=a.Regd_ID
                       }
                       ).ToList();

            for (var i = 0; i < res.ToList().Count; i++)
            {
                var details = result.Where(z => z.StudentId == res[i].Regd_ID && z.ClassId == res[i].ClassId && z.SessionId == Convert.ToInt64(Academicsession.ID)).FirstOrDefault();
                if (details==null)
                {
                 
                        StudentSession stusession = new StudentSession();
                        stusession.SessionId = Convert.ToInt64(Academicsession.ID);
                        stusession.StudentId = res[i].Regd_ID;
                        stusession.SectionId = res[i].SectionId;
                        stusession.ClassId = res[i].ClassId;
                        stusession.IsCurrentSession = true;
                        stusession.InsertedDate = DateTime.Now;
                        stusession.InsertedId = 1;
                        stusession.ModifiedDate = DateTime.Now;
                        stusession.ModifiedId = 1;
                        stusession.Active = true;
                        stusession.Status = "Created";
                        dbContext.StudentSessions.Add(stusession);
                        dbContext.SaveChanges();
                    
                }              
            }

            var studentsession = (from a in result
                                  join b in student on a.StudentId equals b.Regd_ID
                                  join c in studentdtl on b.Regd_ID equals c.Regd_ID
                                  select new StudentDetailsModel
                       {
                           ClassId = c.Class_ID.Value,
                           SectionId = a.SectionId.Value,
                           Regd_ID = a.StudentId.Value,
                           StudentName=b.Customer_Name,
                        ClassName = c.Class_ID == null ? "" : classs.Where(d => d.Class_Id == c.Class_ID).FirstOrDefault().Class_Name,
                        SectionName = ((b.SectionId == null && b.SectionId == Guid.Empty) ? "" : sections.Where(x => x.SectionId == b.SectionId).FirstOrDefault().SectionName),
                        SessionName= a.SectionId == null ? "" : session.Where(x => x.ID == a.SessionId).FirstOrDefault().Name
                       }
                     ).ToList();

            return View(studentsession);
        }
        #endregion

    }
}
