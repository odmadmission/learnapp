﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DigiChamps.Controllers
{
    public class SchoolAPIController : ApiController
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();

        [HttpPost]
        public SchoolModel.ReportInput SaveReport(SchoolModel.ReportInput input)
        {
            SchoolModel.DiscussionOutPutInsert dop = new SchoolModel.DiscussionOutPutInsert();
            SchoolModel.ReportInput ri = new SchoolModel.ReportInput();
            try
            {
                tbl_DC_UserReport ur = new tbl_DC_UserReport();
                ur.ReportModuleMaster_ID = input.ReportModuleMaster_ID;
                ur.UserReport_ID = input.UserReport_ID;
                ur.StartDate = input.StartDate;
                ur.EndDate = input.EndtDate;
                DbContext.tbl_DC_UserReport.Add(ur);

                tbl_DC_ReportMaster rm = new tbl_DC_ReportMaster();
                rm.UserReport_ID = input.UserReport_ID;
                rm.Reg_ID = input.RegId;
                rm.Inserted_On = System.DateTime.Now;
                DbContext.tbl_DC_ReportMaster.Add(rm);

                tbl_DC_Module mid = new tbl_DC_Module();
                mid.Module_ID = input.ModuleID;
                mid.Module_Name = input.ModuleName;
                DbContext.tbl_DC_Module.Add(mid);

                DbContext.SaveChanges();
                dop.Message = "Successfull";
            }
            catch (Exception ex)
            {
                dop.Message = "An exception occurrenced" + ex.Message;
            }
            return ri;
        }
        #region MobileAPI
        //api/SchoolAPI/AddDiscussion
        public SchoolModel.DiscussionOutPutInsert AddDiscussion(SchoolModel.DiscussionInputModelInsert input)
        {
            SchoolModel.DiscussionOutPutInsert dop = new SchoolModel.DiscussionOutPutInsert();
            try
            {
                //SchoolModel.Discussion discussion = new SchoolModel.Discussion();


                

                if (input.SchoolId != Guid.Empty && input.Title != string.Empty && 
                    !input.SchoolId.ToString().Equals("ebc02c81-e43e-4f85-b84d-7826ff96d061"))
                {
                    tbl_DC_Discussion discussion = new tbl_DC_Discussion();
                    discussion.Title = input.Title;
                    discussion.RoleID = input.RoleID;
                    discussion.CreatedBy = input.CreatedBy;
                    discussion.CreatedDate = DateTime.Now;
                    discussion.IsActive = true;
                    discussion.IsDelete = false;
                    discussion.SchoolID = input.SchoolId;
                    DbContext.tbl_DC_Discussion.Add(discussion);
                    DbContext.SaveChanges();

                    dop.Message = "Successfull";
                    dop.DiscussionID = discussion.DiscussionID;
                    dop.status = true;
                }
                else
                {
                    dop.Message = "please send the required parameter schoolid, title";
                    dop.status = false;

                }
            }
            catch (Exception ex)
            {
                dop.Message = "An exception occurrenced" + ex.Message;
                dop.status = false;
            }

            return dop;
        }

        public SchoolModel.DiscussionDetailOutputInsert AddDiscussionDetail(SchoolModel.DiscussionDetailInputModelInsert input)
        {
            SchoolModel.DiscussionDetailOutputInsert ddop = new SchoolModel.DiscussionDetailOutputInsert();

            try
            {

                tbl_DC_Registration registration = DbContext.tbl_DC_Registration.Where(x=>x.Regd_ID==input.CreatedBy).FirstOrDefault();
                //SchoolModel.Discussion discussion = new SchoolModel.Discussion();

                if (input.DiscussionID != 0 && input.RoleID != 0 && input.DetailText != "" && registration.SchoolId!=null
                    && !registration.SchoolId.ToString().Equals("ebc02c81-e43e-4f85-b84d-7826ff96d061"))
                {
                    tbl_DC_DiscussionDetail discussiondetail = new tbl_DC_DiscussionDetail();
                    discussiondetail.DiscussionID = input.DiscussionID;
                    discussiondetail.DetailText = input.DetailText;
                    discussiondetail.CreatedBy = input.CreatedBy;
                    discussiondetail.CreatedDate = DateTime.Now;
                    discussiondetail.IsActive = true;
                    discussiondetail.RoleID = input.RoleID;

                    DbContext.tbl_DC_DiscussionDetail.Add(discussiondetail);
                    DbContext.SaveChanges();


                    SchoolModel.DiscussionDetail dc = new SchoolModel.DiscussionDetail();
                    dc.DiscussionDetailID = 0;
                    dc.CreatedDate = discussiondetail.CreatedDate;
                    dc.CreatedBy = discussiondetail.CreatedBy;
                    dc.DetailText = input.DetailText;
                    var userDetail = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == input.CreatedBy).SingleOrDefault();
                    dc.CreatedName = userDetail.Customer_Name;
                    dc.photoURL = userDetail.Image;

                    ddop.messageDetail = dc;
                    ddop.Message = "Successfull";
                    ddop.DiscussionDetailID = discussiondetail.DiscussionID;

                }
                else
                {
                    ddop.Message = "please send the required parameter DiscussionID, RoleID and DetailText";

                }
            }
            catch (Exception ex)
            {
                ddop.Message = "An exception occurrenced" + ex.Message;
            }

            return ddop;
        }

        public SchoolModel.DiscussionOutPut DiscussionForumList(SchoolModel.DiscussionInputModel input)
        {
            SchoolModel.DiscussionOutPut dop = new SchoolModel.DiscussionOutPut();

            try
            {
                List<SchoolModel.Discussion> discusslist = new List<SchoolModel.Discussion>(); 
                
                //Empty list
                List<SchoolModel.Discussion> dislist = new List<SchoolModel.Discussion>();                
                SchoolModel.Discussion sd = new SchoolModel.Discussion();
                sd.Title = "";
                sd.CreatedDate = DateTime.Now;
                dislist.Add(sd);
                //Empty list

                if (input.SchoolId != Guid.Empty)
                {
                    var discussionlist = DbContext.tbl_DC_Discussion.Where(x => x.SchoolID == input.SchoolId).ToList();
                    
                    foreach (var lst in discussionlist)
                    {
                        SchoolModel.Discussion dc = new SchoolModel.Discussion();
                        dc.DiscussionID = lst.DiscussionID;
                        dc.Title = lst.Title;
                        dc.CreatedDate = lst.CreatedDate;
                        discusslist.Add(dc);
                    }

                    
                        if (discusslist != null)
                        {
                            discusslist = discusslist.Skip(input.StartIndex - 1).ToList();
                            if (discusslist.Count > 0)
                            {
                                discusslist = discusslist.Take(input.PageSize).DefaultIfEmpty().ToList();
                                if (discusslist != null)
                                    discusslist = discusslist.OrderByDescending(x => x.DiscussionID).ToList();
                                
                                dop.Message = "Successfull";
                                dop.Discussion = discusslist;
                                dop.ResultCount = discusslist.Count;
                                dop.status = true;
                            }
                            else
                            {
                                dop.Discussion = dislist; 
                                dop.Message = "No record found.";
                                dop.status = true;
                            }
                        }
                                        
                    
                }
                else
                {                    
                    dop.Discussion = dislist;
                    dop.Message = "please send the required parameter SchoolID";
                    dop.status = false;
                }
            }
            catch (Exception ex)
            {
                dop.Message = "An exception occurrenced" + ex.Message;
                dop.status = false;
            }


            return dop;
        }

        public SchoolModel.DiscussionDetailOutput DiscussionForumDetail(SchoolModel.DiscussionDetailInputModel input)
        {
            SchoolModel.DiscussionDetailOutput ddop = new SchoolModel.DiscussionDetailOutput();
            //Empty List
            List<SchoolModel.DiscussionDetail> ddlist = new List<SchoolModel.DiscussionDetail>();
            SchoolModel.DiscussionDetail dd = new SchoolModel.DiscussionDetail();
            dd.DetailText = "";
            dd.CreatedDate = DateTime.Now;
            dd.IsActive = 0;
            dd.CreatedName = "";
            dd.photoURL = "";
            ddlist.Add(dd);
            //Empty List    
            try
            {
                if (input.DiscussionID != 0)
                {
                    var discussiondetaillist = DbContext.tbl_DC_DiscussionDetail.Where(x => x.DiscussionID == input.DiscussionID).
                        OrderByDescending(ordy => ordy.CreatedDate).ToList().DefaultIfEmpty();
                    List<SchoolModel.DiscussionDetail> discussdetaillist = new List<SchoolModel.DiscussionDetail>();

                    /*
                    sky blue:  #00ADCA
                    red:       #F44337
                    navy blue: #4051B5
                    green:     #009933
                    */

                    var counter = 0;

                    foreach (var lst in discussiondetaillist)
                    {
                        SchoolModel.DiscussionDetail dc = new SchoolModel.DiscussionDetail();
                        dc.DiscussionDetailID = lst.DiscussionDetailID;
                        dc.CreatedDate = lst.CreatedDate;
                        dc.CreatedBy = lst.CreatedBy;
                        dc.DetailText = lst.DetailText;
                        var userDetail = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == lst.CreatedBy).SingleOrDefault();
                        dc.CreatedName = userDetail.Customer_Name;
                        dc.photoURL = userDetail.Image;


                   
            

                       // colorCode
                        //PhotoPath  -- Needs to know table
                        //CreatedName -- Needs to know table
                        discussdetaillist.Add(dc);
                    }

                   
                        if (discussdetaillist != null)
                        {
                            discussdetaillist = discussdetaillist.Skip(input.StartIndex).ToList();

                            if (discussdetaillist.Count > 0)
                            {
                                discussdetaillist = discussdetaillist.Take(input.PageSize).DefaultIfEmpty().ToList();
                                if (discussdetaillist != null)
                                    discussdetaillist = discussdetaillist.OrderByDescending(x => x.DiscussionID).ToList();

                                ddop.Message = "Successfull";
                                ddop.DiscussionDetail = discussdetaillist;
                                ddop.ResultCount = discussdetaillist.Count;
                            }
                            else
                            {
                                ddop.Message = "No record found.";
                                ddop.DiscussionDetail = ddlist;
                                
                            }
                        }                        
                                       
                }
                else
                {
                    ddop.Message = "please send the required parameter DiscussionID";
                    ddop.DiscussionDetail = ddlist;
                }
            }
            catch (Exception ex)
            {
                ddop.Message = "An exception occurrenced" + ex.Message;
            }

            return ddop;
        }

        public SchoolModel.AssignTeachersOutput GetAssignTeachers(Guid? schoolId, int? ClassId, Guid? SectionId)
        {

            SchoolModel.AssignTeachersOutput objOutput = new SchoolModel.AssignTeachersOutput();


            // var schoolList = new List<tbl_DC_School_Info>();

            //schoolList = DbContext.tbl_DC_School_Info.Where(x => x.SchoolId == schoolId && x.IsActive == true).ToList();

            var assignteacherDetails = new List<tbl_DC_School_AssingTeacher>();

            List<SchoolModel.AssignTeachers> objList = new List<SchoolModel.AssignTeachers>();


            if (schoolId != Guid.Empty)
            {
                try
                {
                    assignteacherDetails = DbContext.tbl_DC_School_AssingTeacher.Where(x => x.Class_Id == ClassId && x.SchoolId == schoolId && x.SectionId == SectionId && x.IsActive == true).ToList();


                    foreach (var assignteacherDetail in assignteacherDetails)
                    {
                        var teacherDetail = DbContext.tbl_DC_SchoolUser.Where(x => x.UserId == assignteacherDetail.TeacherId).SingleOrDefault();

                        var classDetails = DbContext.tbl_DC_Class.Where(x => x.Class_Id == ClassId).SingleOrDefault();
                        if (classDetails != null)
                        {
                            var sectionDetail = DbContext.tbl_DC_Class_Section.Where(x => x.Class_Id == classDetails.Class_Id && x.SectionId == SectionId).SingleOrDefault();
                            var subjectDetail = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == assignteacherDetail.SubjectId).SingleOrDefault();

                            SchoolModel.AssignTeachers objAssignTeacher = new SchoolModel.AssignTeachers();

                            objAssignTeacher.ClassName = classDetails.Class_Name;
                            objAssignTeacher.SectionName = sectionDetail.SectionName;
                            objAssignTeacher.TeacherNameFirstName = teacherDetail.UserFirstname;
                            objAssignTeacher.TeacherNameLastName = teacherDetail.UserLastname;
                            objAssignTeacher.EmailAddress = teacherDetail.UserEmailAddress;
                            objAssignTeacher.SubjectName = subjectDetail.SubjectName;


                            objList.Add(objAssignTeacher);


                            //need to add other field of teachers here
                        }
                        else
                        {

                            // objOutput.schoolInformation = objList;

                        }
                    }


                    objOutput.Message = "Successfull";
                    objOutput.ResultCount = objList.Count;
                    objOutput.AssignDetail = objList;

                }
                catch (Exception ex)
                {

                    objOutput.Message = "There is some error.";
                    objOutput.ResultCount = 0;
                }

            }
            else
            {
                objOutput.Message = "please send the required parameter.";
                objOutput.ResultCount = 0;
                // objOutput.schoolInformation = objList;

            }



            return objOutput;
        }

        public SchoolModel.ExamSchedulesOutput ExamSchedules(SchoolModel.APIInputModel input)
        {
            SchoolModel.ExamSchedulesOutput objOutput = new SchoolModel.ExamSchedulesOutput();

            List<SchoolModel.CreateExamScheduleModel> objList = new List<SchoolModel.CreateExamScheduleModel>();

            if ((input.SchoolId != Guid.Empty) && (input.ClassId != 0))
            {
                try
                {
                    var ExamSchedule = new List<tbl_DC_School_ExamSchedule>();

                    //OrderBy(examDate => examDate.DateOfExam).GroupBy(typeID => typeID.ExamTypeId)
                    ExamSchedule = DbContext.tbl_DC_School_ExamSchedule.
                        Where(x => x.Class_Id == input.ClassId && x.SchoolId == input.SchoolId&&x.IsActive==true).
                        //  GroupBy(gpby => gpby.ExamTypeId).
                        OrderBy(examDates => examDates.DateOfExam).ToList();

                    if (ExamSchedule != null && ExamSchedule.Count > 0)
                    {
                        foreach (var item in ExamSchedule)
                        {
                            SchoolModel.CreateExamScheduleModel objCreateExamModel = new SchoolModel.CreateExamScheduleModel();

                            var examType = DbContext.tbl_DC_School_ExamType.Where(x => x.ExamTypeId == item.ExamTypeId && x.IsActive == true).SingleOrDefault();
                            var subject = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == item.SubjectId && x.IsActive == true).SingleOrDefault();
                            if (examType != null)
                            {
                                objCreateExamModel.ExamTypeId = item.ExamTypeId;

                                objCreateExamModel.ExamName = examType.ExamTypeName;
                                var date = (DateTime)item.DateOfExam;

                                objCreateExamModel.StartDate = date.ToString("yyyy-MM-dd");
                                //objCreateExamModel.SubjectName = subject.SubjectName;
                                objList.Add(objCreateExamModel);
                            }
                        }

                        objOutput.Message = "Successfull";
                        objOutput.ResultCount = objList.Count();
                        objOutput.ExamScheduleList = objList;
                    }
                    else
                    {
                        objOutput.Message = "No result found";
                        objOutput.ResultCount = 0;
                        // objOutput.schoolInformation = objList;

                    }
                }
                catch (Exception ex)
                {
                    objOutput.Message = "There is some error.";
                    objOutput.ResultCount = 0;

                }
            }
            else
            {
                objOutput.Message = "please send the required parameter.";
                objOutput.ResultCount = 0;
                // objOutput.schoolInformation = objList;

            }
            return objOutput;
        }
      
        public SchoolModel.ExamScheduleDetailOutput ExamScheduleByTypeId(SchoolModel.InputModel input)
        {
            SchoolModel.ExamScheduleDetailOutput objOutput = new SchoolModel.ExamScheduleDetailOutput();

            List<SchoolModel.ExamScheduleDetailModel> objList = new List<SchoolModel.ExamScheduleDetailModel>();

            if ((input.ExamId != Guid.Empty))
            {
                try
                {
                    var ExamSchedule = DbContext.tbl_DC_School_ExamSchedule.Where(x => x.ExamTypeId == input.ExamId&&x.IsActive==true).ToList();
                    if (ExamSchedule != null && ExamSchedule.Count > 0)
                    {
                        foreach (var item in ExamSchedule)
                        {
                            SchoolModel.ExamScheduleDetailModel objCreateExamModel = new SchoolModel.ExamScheduleDetailModel();
                            var examType = DbContext.tbl_DC_School_ExamType.Where(x => x.SchoolId == input.SchoolId && x.ExamTypeId == item.ExamTypeId && x.IsActive == true).SingleOrDefault();
                            var subject = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == item.SubjectId && x.IsActive == true).SingleOrDefault();
                            if (examType != null)
                            {
                                var date = (DateTime)item.DateOfExam;


                                objCreateExamModel.StartDate = date.ToString("yyyy-MM-dd");
                                objCreateExamModel.SubjectName = subject.SubjectName;
                                objCreateExamModel.TimeSlot = item.TimeSlot == null ? "" : item.TimeSlot;
                                objList.Add(objCreateExamModel);
                            }
                        }
                        objOutput.Message = "Successfull";
                        objOutput.ResultCount = objList.Count();
                        objOutput.ExamScheduleList = objList;
                    }
                    else
                    {
                        objOutput.Message = "No result found";
                        objOutput.ResultCount = 0;
                        // objOutput.schoolInformation = objList;

                    }
                }
                catch (Exception ex)
                {
                    objOutput.Message = "There is some error.";
                    objOutput.ResultCount = 0;

                }
            }
            else
            {
                objOutput.Message = "please send the required parameter.";
                objOutput.ResultCount = 0;
                // objOutput.schoolInformation = objList;

            }
            return objOutput;
        }



        public SchoolModel.HomeWorkOutputModel HomeWorkList(SchoolModel.HomeWorkInputModel input)
        {
            SchoolModel.HomeWorkOutputModel objOutput = new SchoolModel.HomeWorkOutputModel();


            List<SchoolModel.HomeWorkDetailModel> objList = new List<SchoolModel.HomeWorkDetailModel>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var HomeWorkList = DbContext.tbl_DC_School_Homework.Where(x => x.SchoolId == input.SchoolId && x.Class_Id == input.ClassId
                        && x.SectionId == input.SectionId && x.DateOfHomework == input.HomeWorkDate&&x.IsActive==true).OrderByDescending(x => x.CreatedDate).ToList();

                    if (HomeWorkList != null && HomeWorkList.Count() > 0)
                    {
                        foreach (var item in HomeWorkList)
                        {
                            var HomeWorkSubject = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == item.SubjectId && x.IsActive == true).SingleOrDefault();

                            SchoolModel.HomeWorkDetailModel objHomeWorkDetailModel = new SchoolModel.HomeWorkDetailModel();
                            objHomeWorkDetailModel.HomeworkId = item.HomeworkId;
                            objHomeWorkDetailModel.subject = HomeWorkSubject.SubjectName;
                            objHomeWorkDetailModel.homework = item.HomeworkDetail;
                            objHomeWorkDetailModel.homeWorkDate = item.DateOfHomework;


                            objList.Add(objHomeWorkDetailModel);


                        }
                        objOutput.list = objList;
                        objOutput.status = true;
                        objOutput.message = "success";

                        // objOutput.HomeWork = objList;

                    }
                    else
                    {
                        objOutput.status = false;
                        objOutput.message = "no record found";
                    }
                }
                else
                {
                    objOutput.status = false;
                    objOutput.message = "please send the required parameter.";
                }

            }
            catch (Exception ex)
            {
                objOutput.status = false;
                objOutput.message = "There is some error.";

            }
            return objOutput;

        }


        public SchoolModel.TimeTableOutputModel TimeTableList(SchoolModel.HomeWorkInputModel input)
        {
            SchoolModel.TimeTableOutputModel objOutput = new SchoolModel.TimeTableOutputModel();


            List<SchoolModel.TimeTableDetailModel> objList = new List<SchoolModel.TimeTableDetailModel>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var TimeTableList = DbContext.tbl_DC_Shool_TimeTable.Where(x => x.SchoolId == input.SchoolId &&
                        x.Class_Id == input.ClassId && x.SectionId == input.SectionId&&x.IsActive==true
                      ).OrderByDescending(x => x.CreatedDate).ToList();

                    if (TimeTableList != null && TimeTableList.Count() > 0)
                    {
                        foreach (var item in TimeTableList)
                        {
                            var peroidDetail = DbContext.tbl_DC_Period.Where(x => x.Id == item.PeriodId&&x.IsActive==true).SingleOrDefault();
                            var subjectDetail = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == item.SubjectId&&x.IsActive==true).SingleOrDefault();

                            SchoolModel.TimeTableDetailModel objTimeTableModel = new SchoolModel.TimeTableDetailModel();
                            objTimeTableModel.peroidName = peroidDetail.Title;
                             objTimeTableModel.timeFrom = peroidDetail.FromTime;
                            objTimeTableModel.timeTo = peroidDetail.ToTime;
                            objTimeTableModel.subject = subjectDetail.SubjectName;
                            objTimeTableModel.day = item.Day;

                            objList.Add(objTimeTableModel);


                        }
                        objOutput.list = objList;
                        objOutput.status = true;
                        objOutput.message = "success";

                        // objOutput.HomeWork = objList;

                    }
                    else
                    {
                        objOutput.status = false;
                        objOutput.message = "no record found";
                    }
                }
                else
                {
                    objOutput.status = false;
                    objOutput.message = "please send the required parameter.";
                }

            }
            catch (Exception ex)
            {
                objOutput.status = false;
                objOutput.message = "There is some error.";

            }
            return objOutput;

        }
        [HttpPost]
        public SchoolModel.NoticeOutputModel NoticeList(SchoolModel.HomeWorkInputModel input)
        {
            SchoolModel.NoticeOutputModel objOutput = new SchoolModel.NoticeOutputModel();


            List<SchoolModel.NoticeDetailModel> objList = new List<SchoolModel.NoticeDetailModel>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var NoticeList = DbContext.tbl_DC_School_MessageCreation.Where(x => x.SchoolId == input.SchoolId && x.IsActive==true&&x.Class_Id == input.ClassId && x.SectionId == input.SectionId
                      ).OrderByDescending(x => x.CreatedDate).ToList();

                    if (NoticeList != null && NoticeList.Count() > 0)
                    {
                        foreach (var item in NoticeList)
                        {
                        
                            SchoolModel.NoticeDetailModel objTimeTableModel = new SchoolModel.NoticeDetailModel();
                            objTimeTableModel.title = item.Title;
                             objTimeTableModel.description = item.MassageText;
                             objTimeTableModel.fileURL = item.FilePath == null ? "" : item.FilePath;

                             var date = (DateTime)item.CreatedDate;

                             objTimeTableModel.createdDate = date.ToString("yyyy-MM-dd");



                            objList.Add(objTimeTableModel);


                        }
                        objOutput.list = objList;
                        objOutput.status = true;
                        objOutput.message = "success";

                        // objOutput.HomeWork = objList;

                    }
                    else
                    {
                        objOutput.status = false;
                        objOutput.message = "no record found";
                    }
                }
                else
                {
                    objOutput.status = false;
                    objOutput.message = "please send the required parameter.";
                }

            }
            catch (Exception ex)
            {
                objOutput.status = false;
                objOutput.message = "There is some error.";

            }
            return objOutput;

        }
        
        public SchoolModel.StudyMaterialOutputModel StudyMaterialList(SchoolModel.HomeWorkInputModel input)
        {
            SchoolModel.StudyMaterialOutputModel objOutput = new SchoolModel.StudyMaterialOutputModel();


            List<SchoolModel.StudyMaterialModelAPI> objList = new List<SchoolModel.StudyMaterialModelAPI>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var studyMaterialList = DbContext.tbl_DC_School_StudyMaterial.Where(x => x.SchoolId == input.SchoolId && x.Class_Id == input.ClassId&&x.IsActive==true
                      ).OrderByDescending(x => x.CreatedDate).ToList();

                    if (studyMaterialList != null && studyMaterialList.Count() > 0)
                    {
                        foreach (var item in studyMaterialList)
                        {
                            var subjectDetail = DbContext.tbl_DC_School_Subject.
                                Where(x => x.SubjectId == item.SubjectId && x.IsActive == true).SingleOrDefault();

                            SchoolModel.StudyMaterialModelAPI objTimeTableModel = new SchoolModel.StudyMaterialModelAPI();

                            var date = (DateTime)item.CreatedDate;

                            objTimeTableModel.createdDate = date.ToString("yyyy-MM-dd");


                            objTimeTableModel.fileType = item.FileType == null ? "Text" : item.FileType;
                            objTimeTableModel.fileURL = item.FilePath == null ? "" : item.FilePath;
                            objTimeTableModel.studyMaterial = item.StudyMaterialTxt == null ? "" : item.StudyMaterialTxt; ;

                            objTimeTableModel.topic = item.Topic == null ? "" : item.Topic; ;
                            objTimeTableModel.materialType = item.Title == null ? "" : item.Title; 
                            objTimeTableModel.subject = subjectDetail.SubjectName;

               /*                                       public DateTime? createdDate { get; set; }
                                    public String fileType { get; set; }
                                    public String fileURL { get; set; }
                                    public String studyMaterial { get; set; }
                                    public String topic { get; set; }
                                    public String title { get; set; }
                                    public String subject { get; set; }

                                                    */


                            objList.Add(objTimeTableModel);


                        }
                        objOutput.list = objList;
                        objOutput.status = true;
                        objOutput.message = "success";

                        // objOutput.HomeWork = objList;

                    }
                    else
                    {
                        objOutput.status = false;
                        objOutput.message = "no record found";
                    }
                }
                else
                {
                    objOutput.status = false;
                    objOutput.message = "please send the required parameter.";
                }

            }
            catch (Exception ex)
            {
                objOutput.status = false;
                objOutput.message = "There is some error.";

            }
            return objOutput;

        }

       
        public SchoolModel.ToperWayOutputModel ToppersWay(SchoolModel.HomeWorkInputModel input)
        {
            SchoolModel.ToperWayOutputModel objOutput = new SchoolModel.ToperWayOutputModel();


            List<SchoolModel.TopersWayModelAPI> objList = new List<SchoolModel.TopersWayModelAPI>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var studyMaterialList = DbContext.tbl_DC_ToppersWay.Where(x => x.SchoolId == input.SchoolId && x.Class_Id == input.ClassId&&x.IsActive==true
                      ).OrderByDescending(x => x.CreatedDate).ToList();

                    if (studyMaterialList != null && studyMaterialList.Count() > 0)
                    {
                        foreach (var item in studyMaterialList)
                        {

                            var sectionDetail = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.SectionId == item.SectionId).FirstOrDefault();

                            var classDetail = DbContext.tbl_DC_Class.Where(x => x.Class_Id == item.Class_Id && x.Is_Active == true).SingleOrDefault();

                            SchoolModel.TopersWayModelAPI objTimeTableModel = new SchoolModel.TopersWayModelAPI();

                            var date = (DateTime)item.CreatedDate;

                            objTimeTableModel.createdDate = date.ToString("yyyy-MM-dd");

                            objTimeTableModel.sectionName = sectionDetail.SectionName;
                            objTimeTableModel.className = classDetail.Class_Name;
                            objTimeTableModel.title = item.Title == null ? "" : item.Title;

                            objTimeTableModel.fileType = item.FileType == null ? "Text" : item.FileType;
                            objTimeTableModel.fileURL = item.FileURL == null ? "" : item.FileURL;
                        

                            objList.Add(objTimeTableModel);


                        }
                        objOutput.list = objList;
                        objOutput.status = true;
                        objOutput.message = "success";

                        // objOutput.HomeWork = objList;

                    }
                    else
                    {
                        objOutput.status = false;
                        objOutput.message = "no record found";
                    }
                }
                else
                {
                    objOutput.status = false;
                    objOutput.message = "please send the required parameter.";
                }

            }
            catch (Exception ex)
            {
                objOutput.status = false;
                objOutput.message = "There is some error.";

            }
            return objOutput;

        }
       
        public SchoolModel.LeaderBoardOutputModel LeadersBoardClassList(SchoolModel.HomeWorkInputModel input)
        
        
        {

            SchoolModel.LeaderBoardOutputModel objOutput = new SchoolModel.LeaderBoardOutputModel();

            List<SchoolModel.LeaderBoardModelAPI> objList = new List<SchoolModel.LeaderBoardModelAPI>();

            try
            {
                if ((input.ClassId != 0))
                {
                    var resultList = (from c in DbContext.tbl_DC_Registration.Where(x => x.Is_Active == true && x.Is_Deleted == false&&x.SchoolId==input.SchoolId)
                                      join y in DbContext.tbl_DC_Registration_Dtl.Where(x => x.Is_Active == true && x.Is_Deleted == false && x.Class_ID == input.ClassId)
                                       on c.Regd_ID equals y.Regd_ID
                                 join a in DbContext.tbl_DC_Exam_Result.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                 on c.Regd_ID equals a.Regd_ID
                                 join b in DbContext.tbl_DC_Exam.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                 on a.Exam_ID equals b.Exam_ID
                                 join d in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                 on b.Board_Id equals d.Board_Id
                                 join e in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false && x.Class_Id == input.ClassId)
                                 on b.Class_Id equals e.Class_Id
                                 join f in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                 on b.Subject_Id equals f.Subject_Id
                                 select new SchoolModel.LeaderBoardResultModel
                                 {
                                     Regd_ID = c.Regd_ID,
                                     SchoolId = c.SchoolId,
                                     Customer_Name = c.Customer_Name,
                                     imageURL = c.Image,
                                     marks = a.Total_Correct_Ans
                                 }).ToList();

                    if (resultList != null && resultList.Count() > 0)
                    {

                        foreach (var List in resultList.GroupBy(grp => grp.Regd_ID).ToList())
                        {
                            SchoolModel.LeaderBoardModelAPI objLeaderBoardModel = new SchoolModel.LeaderBoardModelAPI();


                            var totalMark = 0;

                            foreach (var listDetail in List)
                            {

                                
                                    totalMark = totalMark + (int)listDetail.marks;
                                    objLeaderBoardModel.name = listDetail.Customer_Name == null ? "" : listDetail.Customer_Name;
                                    objLeaderBoardModel.imageURL = listDetail.imageURL == null ? "" : listDetail.imageURL;
                                

                            }
                            objLeaderBoardModel.marks = totalMark;

                            objList.Add(objLeaderBoardModel);
                        }

                        objOutput.list = objList.OrderByDescending(x => x.marks).ToList();
                        objOutput.status = true;
                        objOutput.message = "success";
                    }
                    else
                    {
                        objOutput.list = new List<SchoolModel.LeaderBoardModelAPI>();

                        objOutput.status = false;
                        objOutput.message = "no record found";
                    }
                }
                else
                {
                    objOutput.list = new List<SchoolModel.LeaderBoardModelAPI>();

                    objOutput.status = false;
                    objOutput.message = "please send the required parameter.";
                }

            }
            catch (Exception ex)
            {
                objOutput.list = new List<SchoolModel.LeaderBoardModelAPI>();

                objOutput.status = false;
                objOutput.message = "There is some error.";

            }
            



           

            return objOutput;
            
        }

      

        public SchoolModel.ClassListOutputModel ClassList(SchoolModel.HomeWorkInputModel input)
        {
            SchoolModel.ClassListOutputModel objOutput = new SchoolModel.ClassListOutputModel();


            List<SchoolModel.ClassListModelAPI> objList = new List<SchoolModel.ClassListModelAPI>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var studyMaterialList = DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false).ToList();

                    if (studyMaterialList != null && studyMaterialList.Count() > 0)
                    {
                        foreach (var item in studyMaterialList)
                        {

                            var boardDetail = DbContext.tbl_DC_Board.Where(x => x.Board_Id == item.Board_Id).FirstOrDefault();

                            SchoolModel.ClassListModelAPI objTimeTableModel = new SchoolModel.ClassListModelAPI();
                            objTimeTableModel.ClassID = item.Class_Id;
                            objTimeTableModel.ClassName = item.Class_Name ;
                            objTimeTableModel.boardName = boardDetail.Board_Name;
                            objTimeTableModel.boardID = boardDetail.Board_Id;

                            objList.Add(objTimeTableModel);


                        }
                        objOutput.list = objList;
                        objOutput.status = true;
                        objOutput.message = "success";

                        // objOutput.HomeWork = objList;

                    }
                    else
                    {
                        objOutput.status = false;
                        objOutput.message = "no record found";
                    }
                }
                else
                {
                    objOutput.status = false;
                    objOutput.message = "please send the required parameter.";
                }

            }
            catch (Exception ex)
            {
                objOutput.status = false;
                objOutput.message = "There is some error.";

            }
            return objOutput;

        }


        public SchoolModel.BoardListOutputModel BoardList(SchoolModel.HomeWorkInputModel input)
        {
            SchoolModel.BoardListOutputModel objOutput = new SchoolModel.BoardListOutputModel();


            List<SchoolModel.BoardListModelAPI> objList = new List<SchoolModel.BoardListModelAPI>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {




                    var boardList = DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false).ToList();

                    if (boardList != null && boardList.Count() > 0)
                    {
                        foreach (var item in boardList)
                        {


                            SchoolModel.BoardListModelAPI objTimeTableModel = new SchoolModel.BoardListModelAPI();

                            objTimeTableModel.boardName = item.Board_Name;
                            objTimeTableModel.boardID = item.Board_Id;

                            objList.Add(objTimeTableModel);


                        }
                        objOutput.list = objList;
                        objOutput.status = true;
                        objOutput.message = "success";

                        // objOutput.HomeWork = objList;

                    }
                    else
                    {
                        objOutput.status = false;
                        objOutput.message = "no record found";
                    }
                }
                else
                {
                    objOutput.status = false;
                    objOutput.message = "please send the required parameter.";
                }

            }
            catch (Exception ex)
            {
                objOutput.status = false;
                objOutput.message = "There is some error.";

            }
            return objOutput;

        }


        #endregion

        /// <summary>
        /// Get the school detail according to the school id
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        /// 

        public SchoolModel.SchoolInformationOutput GetSchool(Guid? schoolId)
        {

            SchoolModel.SchoolInformationOutput objOutput = new SchoolModel.SchoolInformationOutput();
            List<SchoolModel.SchoolInformation> objList = new List<SchoolModel.SchoolInformation>();

            var schoolList = new List<tbl_DC_School_Info>();
            if (schoolId != null)
            {
                //var schoolUser=  DbContext.tbl_DC_SchoolUser.Where(x => x.UserId == userId).SingleOrDefault();
                ////if(schoolUser!=null && (schoolUser.SchoolId!=null|| schoolUser.SchoolId!=Guid.Empty))
                ////{
                schoolList = DbContext.tbl_DC_School_Info.Where(x => x.SchoolId == schoolId && x.IsActive == true).OrderByDescending(x => x.CreationDate).ToList();
                if (schoolList == null || schoolList.Count == 0)
                {
                    objOutput.Message = "Your school is not active,Please contact to admin.";
                    objOutput.ResultCount = 0;
                    return objOutput;
                }
                ////}

            }
            else
            {
                schoolList = DbContext.tbl_DC_School_Info.Where(x => x.IsActive == true).OrderByDescending(x=>x.CreationDate).ToList();

            }
            if (schoolList != null || schoolList.Count > 0)
            {
                foreach (var item in schoolList)
                {
                    SchoolModel.SchoolInformation objSchoolInformation = new SchoolModel.SchoolInformation();
                    objSchoolInformation.SchoolName = item.SchoolName;
                    objSchoolInformation.DocumentaryVideo = item.SchoolVideo;
                    objSchoolInformation.Information = item.SchoolDescription;
                    objSchoolInformation.IsActive = (bool)item.IsActive;
                    objSchoolInformation.Logo = item.SchoolLogo;
                    objSchoolInformation.SchoolId = item.SchoolId;
                    objSchoolInformation.ThumbnailPath = item.SchoolThumbnail;
                    objList.Add(objSchoolInformation);


                }
                objOutput.Message = "Successfull";
                objOutput.ResultCount = objList.Count;
                objOutput.schoolInformation = objList;
            }
            else
            {
                objOutput.Message = "No result found";
                objOutput.ResultCount = 0;
                // objOutput.schoolInformation = objList;
            }
            return objOutput;
        }

        public SchoolModel.SchoolInformation GetSchoolBySchoolId(Guid? schoolId)
        {
            SchoolModel.SchoolInformation objSchoolInformation = new SchoolModel.SchoolInformation();
            if (schoolId != null)
            {
              var schoolInfoDetail=  DbContext.tbl_DC_School_Info.Where(x => x.SchoolId == schoolId && x.IsActive == true).OrderByDescending(x => x.CreationDate).FirstOrDefault();
              if (schoolInfoDetail != null) 
              {
                  objSchoolInformation.SchoolName = schoolInfoDetail.SchoolName;
                  objSchoolInformation.DocumentaryVideo = schoolInfoDetail.SchoolVideo;
                  objSchoolInformation.Information = schoolInfoDetail.SchoolDescription;
                  objSchoolInformation.IsActive = (bool)schoolInfoDetail.IsActive;
                  objSchoolInformation.Logo = schoolInfoDetail.SchoolLogo;
                  objSchoolInformation.SchoolId = schoolInfoDetail.SchoolId;
                  objSchoolInformation.ThumbnailPath = schoolInfoDetail.SchoolThumbnail;                 
              }
            }

            return objSchoolInformation;
        }
        /// <summary>
        /// Get the school detail according to the school id
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></retur
        /// ns>


        [HttpPost]
        public SchoolModel.AssignTeacherOutput GetAssignTacher(SchoolModel.InputModel input)
        {

            SchoolModel.AssignTeacherOutput objOutput = new SchoolModel.AssignTeacherOutput();
            SchoolModel.AssignTeacher objAssignTeacher = new SchoolModel.AssignTeacher();


            if ((input.SchoolId != Guid.Empty) && (input.SectionId != Guid.Empty))
            {
                try
                {
                    var assignteacherDetail = DbContext.tbl_DC_School_AssingTeacher.Where(
                       x => x.SchoolId == input.SchoolId && x.SectionId == input.SectionId && x.IsActive == true).SingleOrDefault();
                    if (assignteacherDetail != null)
                    {
                        // add teacher id in assign table
                        //var teacherDetail=DbContext.tbl_DC_SchoolUser.Where(x=>x.UserId==assignteacherDetail.tea)  
                        var classDetail = DbContext.tbl_DC_School_Class.Where(x => x.Class_id == assignteacherDetail.Class_Id).SingleOrDefault();
                        if (classDetail != null)
                        {
                            objAssignTeacher.ClassName = classDetail.ClassName;
                            objOutput.Message = "Successfull";
                            objOutput.ResultCount = 1;
                            objOutput.AssignDetail = objAssignTeacher;
                            //need to add other field of teachers here
                        }
                        else
                        {
                            objOutput.Message = "No result found";
                            objOutput.ResultCount = 0;
                            // objOutput.schoolInformation = objList;

                        }
                    }
                    else
                    {
                        objOutput.Message = "No result found";
                        objOutput.ResultCount = 0;
                        // objOutput.schoolInformation = objList;

                    }
                }
                catch (Exception ex)
                {

                    objOutput.Message = "There is some error.";
                    objOutput.ResultCount = 0;
                }

            }
            else
            {
                objOutput.Message = "please send the required parameter.";
                objOutput.ResultCount = 0;
                // objOutput.schoolInformation = objList;

            }



            return objOutput;
        }
        
        [HttpPost]
        public SchoolModel.AssignTeacherOutput GetAssignTacherList(SchoolModel.InputModel input)
        {

            SchoolModel.AssignTeacherOutput objOutput = new SchoolModel.AssignTeacherOutput();
            List<SchoolModel.AssignTeacher> objAssignTeacher = new  List<SchoolModel.AssignTeacher>();


            if ((input.SectionId != Guid.Empty))
            {
                try
                {
                    List<tbl_DC_School_AssingTeacher> assignteacherDetail = DbContext.
                        tbl_DC_School_AssingTeacher.Where(x => x.SectionId == input.SectionId && x.IsActive == true).ToList();
                    if (assignteacherDetail != null && assignteacherDetail.Count>0)
                    {
                        // add teacher id in assign table
                        //var teacherDetail=DbContext.tbl_DC_SchoolUser.Where(x=>x.UserId==assignteacherDetail.tea)  
                        for (int i = 0; i < assignteacherDetail.Count; i++)
                        {
                           // int sid=Convert.ToInt32(assignteacherDetail[i].SubjectId.ToString());
                          //  int tid = Convert.ToInt32(assignteacherDetail[i].TeacherId.ToString());
                            Guid? SID=assignteacherDetail[i].SubjectId;
                            Guid? TID = assignteacherDetail[i].TeacherId;
                            tbl_DC_School_Subject subjectDetail = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == SID).FirstOrDefault();
                            tbl_DC_SchoolUser teacherDetail = DbContext.tbl_DC_SchoolUser.Where(x => x.UserId == TID).FirstOrDefault();
                            if (subjectDetail != null && teacherDetail!=null)
                            {
                                SchoolModel.AssignTeacher ateacher = new SchoolModel.AssignTeacher();
                                ateacher.EmailAddress = teacherDetail.UserEmailAddress;
                                ateacher.TeacherName = teacherDetail.UserLastname + " " + teacherDetail.UserFirstname;

                                ateacher.SubjectName = subjectDetail.SubjectName;
                                ateacher.ImageUrl = "http://learn.odmps.org" + teacherDetail.UserProfilePhoto;
                                objAssignTeacher.Add(ateacher);
                                //need to add other field of teachers here http://learn.odmps.org  http://beta.thedigichamps.com
                            }
                           
                        }
                        objOutput.Message = "Successfull";
                        objOutput.ResultCount = objAssignTeacher.Count;
                        objOutput.AssignDetailList = objAssignTeacher;
                    }
                    else
                    {
                        objOutput.Message = "No result found";
                        objOutput.ResultCount = 0;
                        // objOutput.schoolInformation = objList;

                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                    //objOutput.Message = "There is some error.";
                    //objOutput.ResultCount = 0;
                }

            }
            else
            {
                objOutput.Message = "please send the required parameter.";
                objOutput.ResultCount = 0;
                // objOutput.schoolInformation = objList;

            }



            return objOutput;
        }

        [HttpPost]
        public SchoolModel.ExamScheduleOutput ExamSchedule([FromBody] SchoolModel.InputModel input)
        {
            SchoolModel.ExamScheduleOutput objOutput = new SchoolModel.ExamScheduleOutput();

            List<SchoolModel.CreateExamModel> objList = new List<SchoolModel.CreateExamModel>();

            if ((input.SchoolId != Guid.Empty) && (input.ClassId != Guid.Empty))
            {
                try
                {
                    var ExamSchedule = DbContext.tbl_DC_School_ExamSchedule.
                        Where(x => x.ClassId == input.ClassId && x.SchoolId == input.SchoolId && x.IsActive== true && x.DateOfExam >= DateTime.Now).ToList();
                    if (ExamSchedule != null && ExamSchedule.Count > 0)
                    {
                        foreach (var item in ExamSchedule)
                        {
                            SchoolModel.CreateExamModel objCreateExamModel = new SchoolModel.CreateExamModel();
                            var examType = DbContext.tbl_DC_School_ExamType.Where(x => x.SchoolId == input.SchoolId && x.ExamTypeId == item.ExamTypeId).SingleOrDefault();
                            var subject = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == item.SubjectId).SingleOrDefault();
                            if (examType != null)
                            {
                                objCreateExamModel.ExamTypeName = examType.ExamTypeName;
                                objCreateExamModel.StartDate = (DateTime)item.DateOfExam;
                                objCreateExamModel.SubjectName = subject.SubjectName;
                                objList.Add(objCreateExamModel);
                            }
                        }
                        objOutput.Message = "Successfull";
                        objOutput.ResultCount = objList.Count();
                        objOutput.ExamScheduleList = objList;
                    }
                    else
                    {
                        objOutput.Message = "No result found";
                        objOutput.ResultCount = 0;
                        // objOutput.schoolInformation = objList;

                    }
                }
                catch (Exception ex)
                {
                    objOutput.Message = "There is some error.";
                    objOutput.ResultCount = 0;

                }
            }
            else
            {
                objOutput.Message = "please send the required parameter.";
                objOutput.ResultCount = 0;
                // objOutput.schoolInformation = objList;

            }
            return objOutput;
        }
        /// <summary>
        /// daily homework detail
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="classId"></param>
        /// <param name="sectionId"></param>
        /// <param name="startDate"></param>
        /// <returns></returns>
        public SchoolModel.DailyHomeWorkOutput DailyHome(SchoolModel.InputModel input)
        {
            SchoolModel.DailyHomeWorkOutput objOutput = new SchoolModel.DailyHomeWorkOutput();
            List<SchoolModel.HomeWorkModel> objList = new List<SchoolModel.HomeWorkModel>();

            if ((input.SchoolId != Guid.Empty) && (input.ClassId != Guid.Empty) && (input.SectionId != Guid.Empty))
            {
                var homeWorkList = DbContext.tbl_DC_School_Homework.Where(x => x.ClassId == input.ClassId && x.SchoolId == input.SchoolId && x.SectionId == input.SectionId && x.CreatedDate == input.Startdate).ToList();
                if (homeWorkList != null && homeWorkList.Count > 0)
                {
                    foreach (var item in homeWorkList)
                    {
                        try
                        {
                            var subject = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == item.SchoolId).SingleOrDefault();
                            if (subject != null)
                            {
                                SchoolModel.HomeWorkModel objHomeWorkModel = new SchoolModel.HomeWorkModel();
                                //objHomeWorkModel.Description = item.HomeworkDetail;
                                // objHomeWorkModel.SubjectName = subject.SubjectName;
                                //objHomeWorkModel.Date = (DateTime)item.CreatedDate;
                                objList.Add(objHomeWorkModel);
                            }

                        }
                        catch (Exception ex)
                        {

                            objOutput.Message = "There is some error occurs.";
                            objOutput.ResultCount = 0;
                        }

                    }
                    objOutput.Message = "Successfull";
                    objOutput.ResultCount = objList.Count();
                    objOutput.HomeWorkList = objList;
                }
                else
                {
                    objOutput.Message = "No result found";
                    objOutput.ResultCount = 0;
                }

            }
            else
            {
                objOutput.Message = "please send the required parameter.";
                objOutput.ResultCount = 0;
            }

            return objOutput;
        }
        /// <summary>
        /// Get the study material
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="classId"></param>
        /// <returns></returns>
        public SchoolModel.StudyMaterialOutput StudyMaterial(SchoolModel.InputModel input)
        {
            SchoolModel.StudyMaterialOutput objOutput = new SchoolModel.StudyMaterialOutput();
            List<SchoolModel.StudyMaterialModel> objList = new List<SchoolModel.StudyMaterialModel>();
            try
            {
                if ((input.SchoolId != Guid.Empty) && (input.ClassId != Guid.Empty))
                {

                    var studyMaterial = DbContext.tbl_DC_School_StudyMaterial.Where(x => x.ClassId == input.ClassId && x.IsActive == true && x.SchoolId == input.SchoolId).ToList();
                    if (studyMaterial != null && studyMaterial.Count > 0)
                    {
                        foreach (var item in studyMaterial)
                        {
                            var subject = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == item.SubjectId).SingleOrDefault();
                            if (subject != null)
                            {
                                SchoolModel.StudyMaterialModel objStudyMaterialModel = new SchoolModel.StudyMaterialModel();
                                objStudyMaterialModel.Topic = item.Topic;
                                //objStudyMaterialModel.MaterialType = item.FileType;
                                objStudyMaterialModel.Id = item.StudyMaterialId;
                                objList.Add(objStudyMaterialModel);
                            }
                        }
                        objOutput.Message = "Successfull";
                        objOutput.ResultCount = objList.Count();
                        objOutput.StudyMaterialList = objList;
                    }
                    else
                    {
                        objOutput.Message = "No result found";
                        objOutput.ResultCount = 0;
                    }
                }
                else
                {
                    objOutput.Message = "please send the required parameter.";
                    objOutput.ResultCount = 0;
                }

            }
            catch (Exception ex)
            {

                objOutput.Message = "There is some error occurs.";
                objOutput.ResultCount = 0;
            }

            return objOutput;
        }
        /// <summary>
        /// Get the study material detail on the basis of study material id
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="classId"></param>
        /// <returns></returns>
        public SchoolModel.StudyMaterialDetailOutput StudyMaterialDetail(SchoolModel.InputModel input)
        {
            SchoolModel.StudyMaterialDetailOutput objOutput = new SchoolModel.StudyMaterialDetailOutput();
            List<SchoolModel.StudyMaterialModel> objList = new List<SchoolModel.StudyMaterialModel>();
            try
            {
                if ((input.SchoolId != Guid.Empty) && (input.StudyMaterialId != Guid.Empty))
                {

                    var studyMaterial = DbContext.tbl_DC_School_StudyMaterial.Where(x => x.StudyMaterialId == input.StudyMaterialId && x.SchoolId == input.SchoolId).SingleOrDefault();
                    if (studyMaterial != null)
                    {


                        SchoolModel.StudyMaterialModel objStudyMaterialModel = new SchoolModel.StudyMaterialModel();
                        objStudyMaterialModel.Image = studyMaterial.FilePath;
                        objStudyMaterialModel.MaterialType = studyMaterial.FileType;
                        objList.Add(objStudyMaterialModel);

                        objOutput.Message = "Successfull";
                        objOutput.ResultCount = objList.Count();
                        objOutput.StudyMaterialDetail = objStudyMaterialModel;



                    }
                    else
                    {
                        objOutput.Message = "No result found";
                        objOutput.ResultCount = 0;
                    }
                }
                else
                {
                    objOutput.Message = "please send the required parameter.";
                    objOutput.ResultCount = 0;
                }

            }
            catch (Exception ex)
            {

                objOutput.Message = "There is some error occurs.";
                objOutput.ResultCount = 0;
            }

            return objOutput;
        }
        /// <summary>
        /// Get the school notice according to the class.
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="classId"></param>
        /// <returns></returns>
        public SchoolModel.SchoolNoticeOutput SchoolNotice(SchoolModel.InputModel input)
        {
            SchoolModel.SchoolNoticeOutput objOutput = new SchoolModel.SchoolNoticeOutput();
            List<SchoolModel.MessageCreation> objList = new List<SchoolModel.MessageCreation>();
            try
            {
                if ((input.SchoolId != Guid.Empty) && (input.ClassId != Guid.Empty))
                {
                    var date = DateTime.Now;
                    var schoolNoticeList = DbContext.tbl_DC_School_MessageCreation.Where(x => x.ClassId == input.ClassId && x.SchoolId == input.SchoolId && x.MassageDisplayDate == date).ToList();
                    if (schoolNoticeList != null && schoolNoticeList.Count() > 0)
                    {
                        foreach (var item in schoolNoticeList)
                        {
                            SchoolModel.MessageCreation objMessageCreation = new SchoolModel.MessageCreation();
                            objMessageCreation.MassageText = item.MassageText;
                            objMessageCreation.FilePath = item.FilePath;

                            //objMessageCreation.DisplayDate = (DateTime)item.MassageDisplayDate;
                            objList.Add(objMessageCreation);
                        }
                        objOutput.Message = "Successfull";
                        objOutput.ResultCount = objList.Count();
                        objOutput.MessageCreationList = objList;
                    }

                    else
                    {
                        objOutput.Message = "No result found";
                        objOutput.ResultCount = 0;
                    }
                }
                else
                {
                    objOutput.Message = "please send the required parameter.";
                    objOutput.ResultCount = 0;
                }
            }
            catch (Exception ex)
            {

                objOutput.Message = "There is some error.";
                objOutput.ResultCount = 0;
            }
            return objOutput;
        }


        /// <summary>
        /// Function to get Class list
        /// SK
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public List<SchoolModel.HomeWorkModel> GetHomeWorkList(SchoolModel.InputModel input)
        {


            List<SchoolModel.HomeWorkModel> objList = new List<SchoolModel.HomeWorkModel>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var date = DateTime.Now;
                    var HomeWorkList = DbContext.tbl_DC_School_Homework.Where(x => x.SchoolId == input.SchoolId && x.IsActive == true).OrderByDescending(x=>x.CreatedDate).ToList();
                    if (HomeWorkList != null && HomeWorkList.Count() > 0)
                    {
                        foreach (var item in HomeWorkList)
                        {
                            //var SectionDetail = DbContext.tbl_DC_School_s.Where(x => x.ClassId == item.ClassId && x.IsActive == true).SingleOrDefault();
                            SchoolModel.HomeWorkModel objOutput = new SchoolModel.HomeWorkModel();
                            //var classDetail = DbContext.tbl_DC_School_Class.Where(x => x.ClassId == item.ClassId && x.IsActive == true).SingleOrDefault();
                            //var schoolDetail = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == item.SubjectId && x.IsActive == true).SingleOrDefault();
                            //if (classDetail != null)
                            //{
                            //    objOutput.ClassName = classDetail.ClassName;

                            //}
                            var classDetail = DbContext.tbl_DC_Class.Where(x => x.Class_Id == item.Class_Id && x.Is_Active == true).FirstOrDefault();
                            if (classDetail != null)
                            {

                                objOutput.ClassName = classDetail.Class_Name;
                            }

                            var sectionDetail = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.SectionId == item.SectionId).FirstOrDefault();
                            if (sectionDetail != null)
                            {
                                objOutput.SectionName = sectionDetail.SectionName;
                            }

                            var schoolDetail = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == item.SubjectId && x.SchoolId == input.SchoolId && x.IsActive == true).FirstOrDefault();
                            if (schoolDetail != null)
                            {
                                objOutput.SubjectName = schoolDetail.SubjectName;
                            }
                            var PeriodDetail = DbContext.tbl_DC_Period.Where(x => x.Id == item.PeriodID && x.SchoolId == input.SchoolId && x.IsActive == true).FirstOrDefault();
                            if (PeriodDetail != null)
                            {
                                objOutput.PeriodName = PeriodDetail.Title;
                            }
                            objOutput.HomeworkId = item.HomeworkId;
                            objOutput.HomeworkDetail = item.HomeworkDetail;
                            objOutput.DateOfHomework = item.DateOfHomework;
                            objOutput.CreatedDate = (DateTime)item.CreatedDate;
                            objOutput.SectionId = (Guid)item.SectionId;
                            objOutput.SubjectId = (Guid)item.SubjectId;
                            objOutput.PeriodID = (Guid)item.PeriodID;
                            
                            objOutput.SchoolId = (Guid)item.SchoolId;
                            if (schoolDetail != null)
                            {
                                objOutput.SubjectName = schoolDetail.SubjectName;
                            }

                            objList.Add(objOutput);
                        }
                        // objOutput.HomeWork = objList;

                    }
                }

            }
            catch (Exception ex)
            {


            }
            return objList;
        }



        /// <summary>
        /// Function to get exam type list
        /// AM
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //    public List<SchoolModel.ExamType> GetExamTypeList(SchoolModel.InputModel input)
        //    {

        //        SchoolModel.ExamType objOutput = new SchoolModel.ExamType();
        //        List<SchoolModel.ExamType> objList = new List<SchoolModel.ExamType>();
        //        try
        //        {
        //            if ((input.SchoolId != Guid.Empty))
        //            {
        //                var date = DateTime.Now;
        //                var examTypelist = DbContext.tbl_DC_School_ExamType.Where(x => x.SchoolId == input.SchoolId && x.IsActive == true).OrderBy(x => x.SubjectId).ToList();
        //                if (examTypelist != null && examTypelist.Count() > 0)
        //                {
        //                    foreach (var item in examTypelist)
        //                    {
        //                        objOutput.ExamTypeId = item.ExamTypeId;
        //                        objOutput.ExamTypenname = item.ExamTypeName;
        //                        objList.Add(objOutput);
        //                    }

        //                }
        //            }

        //        }
        //        catch (Exception ex)
        //        {


        //        }
        //        return objList;

        //}




        /// <summary>
        /// Function to get message list
        /// AM
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public List<SchoolModel.MessageCreation> GetMessageList(SchoolModel.InputModel input)
        {


            List<SchoolModel.MessageCreation> objList = new List<SchoolModel.MessageCreation>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var date = DateTime.Now;
                    var messageList = DbContext.tbl_DC_School_MessageCreation.Where(x => x.SchoolId == input.SchoolId && x.IsActive == true).OrderByDescending(x=>x.CreatedDate).ToList();
                    if (messageList != null && messageList.Count() > 0)
                    {
                        foreach (var item in messageList)
                        {
                            SchoolModel.MessageCreation objOutput = new SchoolModel.MessageCreation();

                            var classDetail = DbContext.tbl_DC_Class.Where(x => x.Class_Id == item.Class_Id && x.Is_Active == true).SingleOrDefault();
                            if (classDetail != null)
                            {

                                objOutput.ClassName = classDetail.Class_Name;
                            }
                            var sectionDetail = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.SectionId==item.SectionId).FirstOrDefault();
                            if (sectionDetail != null)
                            {

                                objOutput.SectionName = sectionDetail.SectionName;
                            }

                            objOutput.MassageText = item.MassageText;
                            objOutput.MessageId = item.MessageId;
                            objOutput.Title = item.Title;
                            //objOutput.MassageDisplayDate = (DateTime?)item.MassageDisplayDate;

                            //var SectionDetail = DbContext.tbl_DC_Class_Section.Where(x => x.SectionId == item.Sec && x.IsActive == true).SingleOrDefault();
                            //if (examType != null)
                            //{
                            //    objOutput.ExamTypeName = examType.ExamTypeName;
                            //}




                            objList.Add(objOutput);
                        }

                    }
                }

            }
            catch (Exception ex)
            {


            }
            return objList;
        }
        /// <summary>
        /// GetCLass
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //public List<SchoolModel.CreateClass> GetClassList(SchoolModel.InputModel input)
        //{


        //    List<SchoolModel.CreateClass> objList = new List<SchoolModel.CreateClass>();
        //    try
        //    {
        //        if ((input.SchoolId != Guid.Empty))
        //        {
        //            var date = DateTime.Now;
        //            var classList = DbContext.tbl_DC_School_Class.Where(x => x.SchoolId == input.SchoolId && x.IsActive == true).ToList();
        //            if (classList != null && classList.Count() > 0)
        //            {
        //                foreach (var item in classList)
        //                {
        //                    //item.
        //                    SchoolModel.CreateClass objOutput = new SchoolModel.CreateClass();
        //                    var classDetail = DbContext.tbl_DC_School_Class.Where(x => x.ClassId == item.ClassId && x.IsActive == true).SingleOrDefault();
        //                    if (classDetail != null)
        //                    {

        //                        objOutput.ClassName = classDetail.ClassName;
        //                    }
        //                    //var SectionDetail = DbContext.tbl_DC_Class_Section.Where(x => x.SectionId == item.Sec && x.IsActive == true).SingleOrDefault();
        //                    //if (SectionDetail != null)
        //                    //{
        //                    //    objOutput.SectionName = SectionDetail.SectionName;
        //                    //}
        //                    // objOutput.SectionName = item.;
        //                    objOutput.Id = item.ClassId;
        //                    // objOutput.MessageDisplayDate = (DateTime)item.MassageDisplayDate;
        //                    //





        //                    objList.Add(objOutput);
        //                }

        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {


        //    }
        //    return objList;
        //}




        /// <summary>
        /// Function to get assign teacher list
        /// AM
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public List<SchoolModel.AssignTeacher> GetAssignTeacherList(SchoolModel.InputModel input)
        {


            List<SchoolModel.AssignTeacher> objList = new List<SchoolModel.AssignTeacher>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var date = DateTime.Now;
                    var assingTeacherlist = DbContext.tbl_DC_School_AssingTeacher.Where(x => x.SchoolId == input.SchoolId && x.IsActive == true).ToList();
                    if (assingTeacherlist != null && assingTeacherlist.Count() > 0)
                    {
                        foreach (var item in assingTeacherlist)
                        {
                            SchoolModel.AssignTeacher objOutput = new SchoolModel.AssignTeacher();
                            var schoolDetail = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == item.SubjectId && x.IsActive == true).SingleOrDefault();
                            if (schoolDetail != null)
                            {

                                objOutput.SubjectName = schoolDetail.SubjectName;
                            }

                            var classDetail = DbContext.tbl_DC_School_Class.Where(x => x.ClassId == item.ClassId && x.IsActive == true).SingleOrDefault();
                            if (classDetail != null)
                            {

                                objOutput.ClassName = classDetail.ClassName;
                            }
                            var teacherDetail = DbContext.tbl_DC_SchoolUser.Where(x => x.UserId == item.TeacherId && x.IsActive == true).SingleOrDefault();
                            if (teacherDetail != null)
                            {

                                objOutput.TeacherName = teacherDetail.UserFirstname + teacherDetail.UserLastname;
                            }
                            objOutput.AssignmentId = item.Id;


                            //var SectionDetail = DbContext.tbl_DC_Class_Section.Where(x => x.SectionId == item.Sec && x.IsActive == true).SingleOrDefault();
                            //if (examType != null)
                            //{
                            //    objOutput.ExamTypeName = examType.ExamTypeName;
                            //}




                            objList.Add(objOutput);
                        }

                    }
                }

            }
            catch (Exception ex)
            {


            }
            return objList;
        }



        /// <summary>
        /// Function to get study material list
        /// AM
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public List<SchoolModel.StudyMaterialModel> GetStudyMaterialList(SchoolModel.InputModel input)
        {


            List<SchoolModel.StudyMaterialModel> objList = new List<SchoolModel.StudyMaterialModel>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var date = DateTime.Now;
                    var studyMateriallist = DbContext.tbl_DC_School_StudyMaterial.Where(x => x.SchoolId == input.SchoolId && x.IsActive == true).ToList();
                    if (studyMateriallist != null && studyMateriallist.Count() > 0)
                    {
                        foreach (var item in studyMateriallist)
                        {
                            SchoolModel.StudyMaterialModel objOutput = new SchoolModel.StudyMaterialModel();
                            var schoolDetail = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == item.SubjectId && x.IsActive == true).SingleOrDefault();
                            if (schoolDetail != null)
                            {

                                objOutput.SubjectName = schoolDetail.SubjectName;
                            }

                            var classDetail = DbContext.tbl_DC_Class.Where(x => x.Class_Id == item.Class_Id && x.Is_Active == true).FirstOrDefault();
                            if (classDetail != null)
                            {

                                objOutput.ClassName = classDetail.Class_Name;
                            }
                            objOutput.Id = item.StudyMaterialId;
                            objOutput.Material = item.Topic;
                            objOutput.FilePath = item.FilePath;
                            objOutput.FileType = item.FileType;
                            objOutput.Title = item.Title;
                            objOutput.FileName = !string.IsNullOrEmpty(item.FilePath) ? Path.GetFileName(item.FilePath) : "";
                            //var SectionDetail = DbContext.tbl_DC_Class_Section.Where(x => x.SectionId == item.Sec && x.IsActive == true).SingleOrDefault();
                            //if (examType != null)
                            //{
                            //    objOutput.ExamTypeName = examType.ExamTypeName;
                            //}




                            objList.Add(objOutput);
                        }

                    }
                }

            }
            catch (Exception ex)
            {


            }
            return objList;
        }

        /// <summary>
        /// Function to get topper list Is 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>

        public List<SchoolModel.ToppersWayModel> GetToppersWay(SchoolModel.InputModel input)
        {


            List<SchoolModel.ToppersWayModel> objList = new List<SchoolModel.ToppersWayModel>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var date = DateTime.Now;
                    var Topperswaylist = DbContext.tbl_DC_ToppersWay.Where(x => x.SchoolId == input.SchoolId && x.IsActive == true).OrderByDescending(x=>x.CreatedDate).ToList();
                    if (Topperswaylist != null && Topperswaylist.Count() > 0)
                    {
                        foreach (var item in Topperswaylist)
                        {
                            SchoolModel.ToppersWayModel objOutput = new SchoolModel.ToppersWayModel();
                            //var schoolDetail = DbContext.tbl_DC_School_Subject.Where(x => x.SchoolId == item.SchoolId && x.IsActive == true).SingleOrDefault();
                            //if (schoolDetail != null)
                            //{

                            //    objOutput.SubjectName = schoolDetail.SubjectName;
                            //}

                            var classDetail = DbContext.tbl_DC_Class.Where(x => x.Class_Id == item.Class_Id && x.Is_Active == true).SingleOrDefault();
                            if (classDetail != null)
                            {

                                objOutput.ClassName = classDetail.Class_Name;
                            }
                            objOutput.Id = item.ToppersId;
                            objOutput.path = item.FileURL;
                            objOutput.FileName = item.FileType;
                            objOutput.FileName = item.FileType;
                            objOutput.Title = item.Title;

                            var sectionDetail = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.SectionId == item.SectionId).FirstOrDefault();
                            if (sectionDetail != null) {
                                objOutput.SectionName = sectionDetail.SectionName;
                            }

                            //var SectionDetail = DbContext.tbl_DC_Class_Section.Where(x => x.SectionId == item.Sec && x.IsActive == true).SingleOrDefault();
                            //if (examType != null)
                            //{
                            //    objOutput.ExamTypeName = examType.ExamTypeName;
                            //}




                            objList.Add(objOutput);
                        }

                    }
                }

            }
            catch (Exception ex)
            {


            }
            return objList;
        }


        /// <summary>
        /// Function to get Exam list
        /// AM
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public List<SchoolModel.CreateExamModel> GetExamListList([FromBody] SchoolModel.InputModel input)
        {


            List<SchoolModel.CreateExamModel> objList = new List<SchoolModel.CreateExamModel>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var date = DateTime.Now;
                    var examlist = DbContext.tbl_DC_School_ExamSchedule.Where(x => x.SchoolId == input.SchoolId && x.IsActive == true).OrderByDescending(x=>x.CreatedDate).ToList();
                    if (examlist != null && examlist.Count() > 0)
                    {
                        foreach (var item in examlist)
                        {
                            SchoolModel.CreateExamModel objOutput = new SchoolModel.CreateExamModel();
                            var schoolDetail = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == item.SubjectId && x.SchoolId == input.SchoolId && x.IsActive == true).FirstOrDefault();
                            if (schoolDetail != null)
                            {
                                objOutput.SubjectName = schoolDetail.SubjectName;
                            }
                            var examType = DbContext.tbl_DC_School_ExamType.Where(x => x.ExamTypeId == item.ExamTypeId && x.SchoolId == input.SchoolId && x.IsActive == true).FirstOrDefault();
                            if (examType != null)
                            {
                                objOutput.ExamTypeName = examType.ExamTypeName;
                            }
                            var classDetail = DbContext.tbl_DC_Class.Where(x => x.Class_Id == item.Class_Id && x.Is_Active == true).FirstOrDefault();
                            if (classDetail != null)
                            {

                                objOutput.ClassName = classDetail.Class_Name;
                            }
                            //var SectionDetail = DbContext.tbl_DC_Class_Section.Where(x => x.SectionId == item.Sec && x.IsActive == true).SingleOrDefault();
                            //if (examType != null)
                            //{
                            //    objOutput.ExamTypeName = examType.ExamTypeName;
                            //}
                            objOutput.Id = item.ExamScheduleId;
                            objOutput.StartDate = (DateTime)item.DateOfExam;
                            objOutput.TimeSlot = item.TimeSlot;



                            objList.Add(objOutput);
                        }

                    }
                }

            }
            catch (Exception ex)
            {


            }
            return objList;
        }


        /// <summary>
        /// Function to get Subject list
        /// SK
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public List<SchoolModel.CreateSubject> GetSubjectList(SchoolModel.InputModel input)
        {


            List<SchoolModel.CreateSubject> objList = new List<SchoolModel.CreateSubject>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var date = DateTime.Now;
                    var schoollist = DbContext.tbl_DC_School_Subject.Where(x => x.SchoolId == input.SchoolId && x.IsActive == true).OrderByDescending(x=>x.CreatedDate).ToList();
                    if (schoollist != null && schoollist.Count() > 0)
                    {
                        foreach (var item in schoollist)
                        {
                            SchoolModel.CreateSubject objOutput = new SchoolModel.CreateSubject();
                            objOutput.SubjectId = item.SubjectId;
                            objOutput.SubjectName = item.SubjectName;
                            objOutput.SchoolId = (Guid)item.SchoolId;
                            objList.Add(objOutput);
                        }

                    }
                }

            }
            catch (Exception ex)
            {


            }
            return objList;
        }

        /// <summary>
        /// Function to get exam type list
        /// AM
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public List<SchoolModel.ExamTypeModel> GetExamTypeList([FromBody] SchoolModel.InputModel input)
        {


            List<SchoolModel.ExamTypeModel> objList = new List<SchoolModel.ExamTypeModel>();
            try
            {
                if ((input.SchoolId != Guid.Empty))
                {
                    var date = DateTime.Now;
                    var examTypelist = DbContext.tbl_DC_School_ExamType.Where(x => x.SchoolId == input.SchoolId && x.IsActive == true).OrderByDescending(x=>x.CreatedDate).ToList();
                    if (examTypelist != null && examTypelist.Count() > 0)
                    {
                        foreach (var item in examTypelist)
                        {
                            SchoolModel.ExamTypeModel objOutput = new SchoolModel.ExamTypeModel();
                            objOutput.ExamTypeId = item.ExamTypeId;
                            objOutput.ExamTypeName = item.ExamTypeName;
                            objOutput.SchoolId = (Guid)item.SchoolId;
                            objList.Add(objOutput);
                        }

                    }
                }

            }
            catch (Exception ex)
            {


            }
            return objList;

        }
        #region amit
        public List<SchoolModel.CreateClass> GetClassList(SchoolModel.InputModel objclassmodel)
        {


            List<SchoolModel.CreateClass> objList = new List<SchoolModel.CreateClass>();
            try
            {
                if ((objclassmodel.SchoolId != Guid.Empty))
                {
                    var date = DateTime.Now;
                    var classList = DbContext.tbl_DC_School_Class.Where(x => x.SchoolId == objclassmodel.SchoolId && x.IsActive == true).ToList();
                    if (classList != null && classList.Count() > 0)
                    {
                        foreach (var item in classList)
                        {
                            //item.
                            SchoolModel.CreateClass objOutput = new SchoolModel.CreateClass();
                            var classDetail = DbContext.tbl_DC_School_Class.Where(x => x.ClassId == item.ClassId && x.IsActive == true).SingleOrDefault();
                            if (classDetail != null)
                            {

                                objOutput.ClassName = classDetail.ClassName;
                            }
                            //var SectionDetail = DbContext.tbl_DC_Class_Section.Where(x => x.SectionId == item.Sec && x.IsActive == true).SingleOrDefault();
                            //if (SectionDetail != null)
                            //{
                            //    objOutput.SectionName = SectionDetail.SectionName;
                            //}
                            // objOutput.SectionName = item.;
                            objOutput.Id = item.ClassId;
                            // objOutput.MessageDisplayDate = (DateTime)item.MassageDisplayDate;
                            //





                            objList.Add(objOutput);
                        }

                    }
                }

            }
            catch (Exception ex)
            {


            }
            return objList;
        }

        public List<SchoolModel.CreatePeriod> GetPeriodList(Guid prmSchoolId)
        {


            List<SchoolModel.CreatePeriod> objList = new List<SchoolModel.CreatePeriod>();
            try
            {
                if ((prmSchoolId != Guid.Empty))
                {
                    var date = DateTime.Now;
                    var model = (from p in DbContext.tbl_DC_Period.Where(x => x.SchoolId == prmSchoolId && x.IsActive == true).OrderByDescending(y => y.Create_Date).ToList() // .Includes("Addresses") here?
                                 select new SchoolModel.CreatePeriod()
                                 {
                                     Id = p.Id,
                                     FromTime = p.FromTime,
                                     ToTime = p.ToTime,
                                     Title = p.Title

                                     //Class_Id = 1,
                                     //SectionName = string.Join(",", g.Select(kvp => kvp.SectionName))

                                 });

                    return model.ToList();
                }

            }
            catch (Exception ex)
            {


            }
            return objList;
        }
        public SchoolModel.CreatePeriod GetPeriodById(Guid? prmPeriodId)
        {
            SchoolModel.CreatePeriod objSchoolInformation = new SchoolModel.CreatePeriod();
            if (prmPeriodId != null)
            {
                var schoolInfoDetail = DbContext.tbl_DC_Period.Where(x => x.Id == prmPeriodId && x.IsActive == true).FirstOrDefault();
                if (schoolInfoDetail != null)
                {
                    objSchoolInformation.Id = schoolInfoDetail.Id;
                    objSchoolInformation.Title = schoolInfoDetail.Title;
                    objSchoolInformation.FromTime = schoolInfoDetail.FromTime;
                    objSchoolInformation.IsActive = (bool)schoolInfoDetail.IsActive;
                    objSchoolInformation.ToTime = schoolInfoDetail.ToTime;

                }
            }

            return objSchoolInformation;
        }

        public List<SchoolModel.CreateClass> GetClassList_new(Guid prmSchoolId)
        {


            List<SchoolModel.CreateClass> objList = new List<SchoolModel.CreateClass>();
            try
            {
                if ((prmSchoolId != Guid.Empty))
                {
                    var date = DateTime.Now;
                    // var sectionmodel = (from p in DbContext.tbl_DC_Class_Section.Where(x => x.School_Id == prmSchoolId && x.IsActive == true).OrderByDescending(y => y.CreatedDate).ToList() // .Includes("Addresses") here?

                    var model =
(from i in DbContext.tbl_DC_Class_Section
 where i.School_Id == prmSchoolId
 group i by i.Class_Id into g
 //select new { g.Key, count = g.Count(), Items = string.Join(",", g.Select(kvp => kvp.SectionName)) });


 //                    var model = (from p in DbContext.tbl_DC_School_Class.Where(x => x.SchoolId == prmSchoolId && x.IsActive == true).OrderByDescending(y => y.CreatedDate).ToList() // .Includes("Addresses") here?
 select new SchoolModel.CreateClass()
 {
     Id = g.Select(kvp => kvp.SectionId).FirstOrDefault(),
     Class_Id = 1,
     SectionName = string.Join(",", g.Select(kvp => kvp.SectionName))

 });

                    return model.ToList();

                }


            }
            catch (Exception ex)
            {


            }
            return objList;
        }

        public List<SchoolModel.CreateSection> GetsectionList(Guid prmSchoolId)
        {


            List<SchoolModel.CreateSection> objList = new List<SchoolModel.CreateSection>();
            try
            {
                if ((prmSchoolId != Guid.Empty))
                {
                    var date = DateTime.Now;
                    //string aababa;
                    //var model = (from i in DbContext.tbl_DC_Class_Section
                    //             where i.School_Id == prmSchoolId
                    //             group i by i.Class_Id into g
                    //             select new SchoolModel.CreateSection()
                    //             {
                    //                 Class_Id = g.Key,
                    //                 SectionName = string.Join(",", g.Select(kvp => kvp.SectionName)),

                    //             });


                    var mc = (from p in DbContext.tbl_DC_Class_Section
                              join cl in DbContext.tbl_DC_Class on p.Class_Id equals cl.Class_Id
                              where p.School_Id == prmSchoolId && p.IsActive == true
                              select new SchoolModel.CreateSection()
                              {
                                  ClassName = cl.Class_Name,
                                  Class_Id = p.Class_Id,
                                  SectionName = p.SectionName,
                                  SectionId = p.SectionId
                              }).ToList();

                    var result = (from i in mc
                                  group i by new { i.Class_Id, i.ClassName } into g
                                  select new SchoolModel.CreateSection()
                                  {
                                      Class_Id = g.Select(kvp => kvp.Class_Id).FirstOrDefault(),
                                      ClassName = g.Select(kvp => kvp.ClassName).FirstOrDefault(),
                                      SectionName = string.Join(",", g.Select(kvp => kvp.SectionName)),
                                      Section_Id = string.Join(",", g.Select(kvp => kvp.SectionId))
                                  });
                     List<SchoolModel.CreateSection> list1=result.ToList();
                    
                    List<string> list=new List<string>();
                    foreach (var s in result)
                    {
                        List<string> split=s.Section_Id.Split(',').ToList();
                        string html = "<p>";
                        foreach (var i in split)
                      {
                          html = html + "<span>" + i + "</span><br />";
                      }
                        html = html + "</p>";
                        list.Add(html);
                    }
                     for (int i=0;i<list.Count();i++)
                    {
                        list1[i].Section_Id = list[i];
                     }
                     return list1;

                }


            }
            catch (Exception ex)
            {


            }
            return objList;
        }
        #endregion
        /// <summary>
        /// Function to get Exam list
        /// AM
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        ////public List<SchoolModel.CreateExamModel> GetExamListList(SchoolModel.InputModel input)
        ////{

        ////    SchoolModel.CreateExamModel objOutput = new SchoolModel.CreateExamModel();
        ////    List<SchoolModel.CreateExamModel> objList = new List<SchoolModel.CreateExamModel>();
        ////    try
        ////    {
        ////        if ((input.SchoolId != Guid.Empty))
        ////        {
        ////            var date = DateTime.Now;
        ////            var examlist = DbContext.tbl_DC_School_ExamSchedule.Where(x => x.SchoolId == input.SchoolId && x.IsActive == true).ToList();
        ////            if (examlist != null && examlist.Count() > 0)
        ////            {
        ////                foreach (var item in examlist)
        ////                {
        ////                    var schoolDetail = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == item.SchoolId && x.IsActive == true).SingleOrDefault();
        ////                    if (schoolDetail != null)
        ////                    {
        ////                        objOutput.SubjectName = schoolDetail.SubjectName;
        ////                    }
        ////                    var examType = DbContext.tbl_DC_School_ExamType.Where(x => x.ExamTypeId == item.ExamTypeId && x.IsActive == true).SingleOrDefault();
        ////                    if (examType != null)
        ////                    {
        ////                        objOutput.ExamTypeName = examType.ExamTypeName;
        ////                    }
        ////                    //var SectionDetail = DbContext.tbl_DC_Class_Section.Where(x => x.SectionId == item.Sec && x.IsActive == true).SingleOrDefault();
        ////                    //if (examType != null)
        ////                    //{
        ////                    //    objOutput.ExamTypeName = examType.ExamTypeName;
        ////                    //}
        ////                    objOutput.ExamId = item.ExamScheduleId;
        ////                    objOutput.StartDate =(DateTime) item.DateOfExam;
        ////                    objOutput.TimeSlot = item.TimeSlot;



        ////                    objList.Add(objOutput);
        ////                }

        ////            }
        ////        }

        ////    }
        ////    catch (Exception ex)
        ////    {


        ////    }
        ////    return objList;
        ////}
        //// /// <summary>
        /// Function to get User list depending on user role
        /// SK
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //public List<SchoolModel.SchoolAdminOrPrincipleModel> GetUserList(SchoolModel.InputModel input)
        //{

        //    SchoolModel.SchoolAdminOrPrincipleModel objOutput = new SchoolModel.SchoolAdminOrPrincipleModel();
        //    List<SchoolModel.SchoolAdminOrPrincipleModel> objList = new List<SchoolModel.SchoolAdminOrPrincipleModel>();
        //    try
        //    {
        //        if ((input.SchoolId != Guid.Empty))
        //        {
        //            var date = DateTime.Now;
        //            var userlist = DbContext.tbl_DC_SchoolUser.Where(x => x.SchoolId == input.SchoolId && x.UserRole == input.RoleName && x.IsActive == true).ToList();
        //            if (userlist != null && userlist.Count() > 0)
        //            {
        //                foreach (var item in userlist)
        //                {
        //                    objOutput.Id = item.UserId;
        //                    objOutput.FirstName = item.UserFirstname;
        //                    objOutput.LastName = item.UserLastname;
        //                    objOutput.Image = item.UserProfilePhoto;
        //                    objOutput.EmailAddress = item.UserEmailAddress;
        //                    objOutput.PhoneNumber = item.UserPhoneNumber;

        //                    objList.Add(objOutput);
        //                }

        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {


        //    }
        //    return objList;
        //}


    }

}
