﻿using ClosedXML.Excel;
using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Mvc;
using System.Web;
using System.Data.OleDb;
using System.Collections;
using System.Data.Entity;
using System.Text;
using System.Web.Script.Serialization;
using System.Collections.Specialized;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Threading;
using DocumentFormat.OpenXml.Drawing.ChartDrawing;
using System.Threading.Tasks;
using DigiChamps.SendInBlue;

namespace DigiChamps.Controllers
{
    public class SchoolReportController : Controller
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        #region---------------Login / Logout ----------
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(string User_name, string password)
        {
            try
            {
                if (User_name == "Admin")
                {

                    if (password == "admin@1234")
                    {
                        Session["id"] = 123;
                        return RedirectToAction("Dashboard", "SchoolReport");
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Invalid credential for admin login.";
                        return View();
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Invalid username for admin login.";
                    return View();
                }

            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View();
        }
        public ActionResult Dashboard()
        {
            return View();
        }
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "SchoolReport");
        }
        #endregion

        #region------------Commom Method-----------

        public JsonResult GetSchool()
        {
            var res = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSection(Guid schoolid, int id)
        {
            var board = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.Class_Id == id && x.School_Id == schoolid).Distinct().ToList();
            return Json(board, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region------------------------- Get_Doubt_Tracker-------------------
        public ActionResult Doubt(Guid? school, int? board, int? cls, Guid? sec, int? subject, int? chapter, int? student)
        {
            if (Session["id"] == null)
            {
                return RedirectToAction("Index");
            }
            var res = DbContext.SP_GET_DOUBT_BY_TEACHER_ID(1).ToList();
            ViewBag.board = null;
            ViewBag.cls = null;
            ViewBag.sec = null;
            ViewBag.subject = null;
            ViewBag.chapter = null;
            ViewBag.school = null;
            ViewBag.student = null;
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            ViewBag.studentlist = students;
            int i = 0;
            if (board != null && board != 0)
            {
                res = res.Where(a => a.board_id == board.Value).ToList();
                ViewBag.board = board.Value;
                i = 1;
            }
            if (cls != null && cls != 0)
            {
                res = res.Where(a => a.class_id == cls.Value).ToList();
                ViewBag.cls = cls.Value;
                i = 1;
            }
            if (school != null)
            {
                res = res.Where(a => a.SchoolId == school.Value).ToList();
                ViewBag.school = school.Value;
                i = 1;
            }
            if (sec != null)
            {
                res = res.Where(a => a.SectionId == sec.Value).ToList();
                ViewBag.sec = sec.Value;
                i = 1;
            }
            if (subject != null && subject != 0)
            {
                res = res.Where(a => a.Subject_ID == subject.Value).ToList();
                ViewBag.subject = subject.Value;
                i = 1;
            }
            if (chapter != null && chapter != 0)
            {
                res = res.Where(a => a.Chapter_ID == chapter.Value).ToList();
                ViewBag.chapter = chapter.Value;
                i = 1;
            }
            if (student != null && student != 0)
            {
                res = res.Where(a => a.Student_ID == student.Value).ToList();
                ViewBag.student = student.Value;
                i = 1;
            }
            Session["doubtlist"] = res;
            if (i == 0)
            {
                res = res.Take(20).ToList();
            }
            return View(res);
        }
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        public class Class_DOUBT_BY_TEACHER_ID_Result
        {
            public string Customer_Name { get; set; }
            public string Board_Name { get; set; }
            public string Class_Name { get; set; }
            public string SectionName { get; set; }
            public string Subject { get; set; }
            public string Chapter { get; set; }
            public Nullable<int> Total { get; set; }
            public Nullable<int> Pendingticket { get; set; }
            public Nullable<int> Answerticket { get; set; }
            public Nullable<int> Reject { get; set; }
            public Nullable<int> Closeticket { get; set; }
        }
        public ActionResult DownloadDoubtList()
        {

            XLWorkbook oWB = new XLWorkbook();
            var doubtlist = Session["doubtlist"] as List<SP_GET_DOUBT_BY_TEACHER_ID_Result>;
            var doubt = doubtlist.Select(a => new Class_DOUBT_BY_TEACHER_ID_Result
            {
                Answerticket = a.Answerticket,
                Board_Name = a.Board_Name,
                Chapter = a.Chapter,
                Class_Name = a.Class_Name,
                Closeticket = a.Closeticket,
                Customer_Name = a.Customer_Name,
                Pendingticket = a.Pendingticket,
                Reject = a.Reject,
                SectionName = a.SectionName,
                Subject = a.Subject,
                Total = a.Total
            }).ToList();
            // COUNT TABLE DATA
            DataTable gdt = ToDataTable<Class_DOUBT_BY_TEACHER_ID_Result>(doubt);
            int lastCellNo1 = gdt.Rows.Count + 1;
            gdt.TableName = "Doubt_Details";

            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(gdt);
            var worksheet = oWB.Worksheet(1);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "Doubt Report.xlsx");
        }
        #endregion

        #region -----------------Get_Worksheet_Tracker-------------------------
        public ActionResult Worksheet(Guid? school, int? board, int? cls, Guid? sec, int? subject, int? chapter, int? studentid)
        {

            if (Session["id"] == null)
            {
                return RedirectToAction("Index");
            }
            var res = DbContext.SP_GET_WORKSHEET_REPORT().ToList();
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            ViewBag.studentlist = students;
            ViewBag.board = null;
            ViewBag.cls = null;
            ViewBag.sec = null;
            ViewBag.subject = null;
            ViewBag.chapter = null;
            ViewBag.school = null;
            ViewBag.student = null;
            int i = 0;
            if (board != null && board != 0)
            {
                res = res.Where(a => a.board_id == board.Value).ToList();
                ViewBag.board = board.Value;
                i = 1;
            }
            if (cls != null && cls != 0)
            {
                res = res.Where(a => a.class_id == cls.Value).ToList();
                ViewBag.cls = cls.Value;
                i = 1;
            }
            if (school != null)
            {
                res = res.Where(a => a.SchoolId == school.Value).ToList();
                ViewBag.school = school.Value;
                i = 1;
            }
            if (sec != null)
            {
                res = res.Where(a => a.SectionId == sec.Value).ToList();
                ViewBag.sec = sec.Value;
                i = 1;
            }
            if (subject != null && subject != 0)
            {
                res = res.Where(a => a.Subject_ID == subject.Value).ToList();
                ViewBag.subject = subject.Value;
                i = 1;
            }
            if (chapter != null && chapter != 0)
            {
                res = res.Where(a => a.Chapter_ID == chapter.Value).ToList();
                ViewBag.chapter = chapter.Value;
                i = 1;
            }
            if (studentid != null && studentid != 0)
            {
                res = res.Where(a => a.Regd_ID == studentid.Value).ToList();
                ViewBag.student = studentid.Value;
                i = 1;
            }
            Session["worksheetlist"] = res;
            if (i == 0)
            {
                res = res.Take(20).ToList();
            }
            return View(res);
        }
        [HttpPost]
        public ActionResult SaveScore(int worksheetid, string score)
        {
            tbl_DC_Worksheet _Worksheet = DbContext.tbl_DC_Worksheet.Where(a => a.Worksheet_Id == worksheetid).FirstOrDefault();
            _Worksheet.Total_Mark = Convert.ToInt32(score);
            _Worksheet.Verified = true;
            _Worksheet.Modified_Date = DateTime.Now;
            DbContext.SaveChanges();
            return RedirectToAction("Worksheet");
        }
        public ActionResult DownloadWorksheetList()
        {

            XLWorkbook oWB = new XLWorkbook();
            var worksheetlist = Session["worksheetlist"] as List<SP_GET_WORKSHEET_REPORT_Result>;
            var works = worksheetlist.Select(a => new Class_WORKSHEET_REPORT_Result
            {
                Board_Name = a.Board_Name,
                Chapter = a.Chapter,
                Class_Name = a.Class_Name,
                Customer_Name = a.Customer_Name,
                Mobile = a.Mobile,
                SectionName = a.SectionName,
                Subject = a.Subject,
                TeacherName = a.TeacherName,
                Total_Mark = a.Total_Mark.Value,
                Uploaded_On = a.Inserted_Date,
                Verified = a.Verified.Value,
                Verified_On = a.Modified_Date
            }).ToList();
            // COUNT TABLE DATA
            DataTable gdt = ToDataTable<Class_WORKSHEET_REPORT_Result>(works);
            int lastCellNo1 = gdt.Rows.Count + 1;
            gdt.TableName = "Worksheet_Details";

            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(gdt);
            var worksheet = oWB.Worksheet(1);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "Worksheet Report.xlsx");
        }
        public partial class Class_WORKSHEET_REPORT_Result
        {
            public string Customer_Name { get; set; }
            public string Board_Name { get; set; }
            public string Class_Name { get; set; }
            public string SectionName { get; set; }
            public string Subject { get; set; }
            public string Chapter { get; set; }
            public string TeacherName { get; set; }
            public string Mobile { get; set; }
            public int Total_Mark { get; set; }
            public bool Verified { get; set; }
            public Nullable<System.DateTime> Verified_On { get; set; }
            public Nullable<System.DateTime> Uploaded_On { get; set; }
        }
        #endregion

        #region --------------Upload student Details---------------------
        public ActionResult Uploadstudent()
        {
            return View();
        }
        public ActionResult DownloadSampleStudentExcel()
        {
            XLWorkbook oWB = new XLWorkbook();

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Student_Name");
            validationTable.Columns.Add("School");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Section");
            validationTable.Columns.Add("Mobile");
            validationTable.TableName = "Student_Details";
            var worksheet = oWB.AddWorksheet(validationTable);

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "SampleStudent.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadStudentDetails(HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("Uploadstudent");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);
                        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                        OleDbConnection connection = new OleDbConnection();
                        connection.ConnectionString = excelConnectionString;
                        OleDbDataAdapter command = new OleDbDataAdapter("select * from [Student_Details$]", connection);
                        // connection.Open();
                        // Create DbDataReader to Data Worksheet
                        DataTable dt = new DataTable();
                        command.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string mobile = dr["Mobile"].ToString();
                                string name = dr["Student_Name"].ToString();
                                if (mobile != "" && name != "")
                                {
                                    tbl_DC_Registration reg = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                                    if (reg != null)
                                    {
                                        string schoolname = dr["School"].ToString();
                                        string classname = dr["Class"].ToString();
                                        string sectionname = dr["Section"].ToString();
                                        var schoolid = DbContext.tbl_DC_School_Info.Where(a => a.SchoolName == schoolname && a.IsActive == true).FirstOrDefault().SchoolId;
                                        var classid = DbContext.tbl_DC_Class.Where(a => a.Class_Name == classname && a.Board_Id == 1 && a.Is_Active == true && a.Is_Deleted == false).FirstOrDefault().Class_Id;
                                        var sectionid = DbContext.tbl_DC_Class_Section.Where(a => a.SectionName == sectionname && a.Class_Id == classid && a.School_Id == schoolid && a.IsActive == true).FirstOrDefault().SectionId;

                                        reg.Customer_Name = name;
                                        reg.SchoolId = schoolid;
                                        reg.SectionId = sectionid;
                                        reg.Modified_By = HttpContext.User.Identity.Name;
                                        reg.Modified_Date = DateTime.Now;
                                        DbContext.SaveChanges();
                                        var regid = reg.Regd_ID;

                                        tbl_DC_Registration_Dtl m = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == regid && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();

                                        m.Board_ID = 1;
                                        m.Class_ID = classid;
                                        m.Modified_Date = DateTime.Now;
                                        m.Modified_By = reg.Regd_ID.ToString();
                                        DbContext.SaveChanges();

                                        var worksheet = DbContext.tbl_DC_Worksheet.Where(a => a.Regd_ID == regid && a.Is_Active == true && a.Is_Deleted == false).ToList();
                                        foreach (tbl_DC_Worksheet wrk in worksheet)
                                        {
                                            wrk.Board_Id = 1;
                                            wrk.Class_Id = classid;
                                            wrk.SectionId = sectionid;
                                            DbContext.SaveChanges();

                                        }
                                        var ticket = DbContext.tbl_DC_Ticket.Where(a => a.Student_ID == regid).ToList();
                                        foreach (tbl_DC_Ticket wrk in ticket)
                                        {
                                            wrk.Board_ID = 1;
                                            wrk.Class_ID = classid;
                                            DbContext.SaveChanges();

                                        }
                                    }
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                    }

                    TempData["SuccessMessage"] = "Student Details Entered Successfully";
                    return RedirectToAction("Uploadstudent");
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("Uploadstudent");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("Uploadstudent");
            }
        }

        #endregion

        #region------------------- Assign Subject To teacher----------------------

        public ActionResult UploadAssignSubjectToTeacher()
        {
            return View();
        }
        public ActionResult DownloadSampleAssignSubjectToTeacher(int board, int cls)
        {
            if (board == null)
            {
                TempData["ErrorMessage"] = "Select Board";
                return RedirectToAction("UploadAssignSubjectToTeacher");
            }
            if (cls == null)
            {
                TempData["ErrorMessage"] = "Select Class";
                return RedirectToAction("UploadAssignSubjectToTeacher");
            }
            XLWorkbook oWB = new XLWorkbook();
            Guid id = new Guid(Session["id"].ToString());
            var teacherlist = DbContext.Teachers.Where(a => a.Active == 1 && a.SchoolId == id).Select(a => (a.Name + "-" + a.Mobile)).ToList();

            DataTable gdt = new DataTable("Teacher");
            gdt.Columns.Add("Name");
            //Here i call a method, in this method i store the table data in list format. You can direct write the Linq query here...
            var g = teacherlist;
            foreach (var gs in g.ToList())
            {
                DataRow dr = gdt.NewRow();
                dr["Name"] = gs;
                gdt.Rows.Add(dr);
            }
            // COUNT TABLE DATA
            int lastCellNo1 = gdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(gdt);
            var worksheet1 = oWB.Worksheet(1);






            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Section");
            validationTable.Columns.Add("Subject");
            validationTable.Columns.Add("Teacher");

            var sectionlist = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true && a.Class_Id == cls && a.School_Id == id).ToList();
            var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Class_Id == cls).ToList();

            foreach (var a in sectionlist)
            {
                foreach (var b in subjects)
                {
                    DataRow dr1 = validationTable.NewRow();
                    dr1["Section"] = a.SectionName;
                    dr1["Subject"] = b.Subject;
                    validationTable.Rows.Add(dr1);
                }
            }
            validationTable.TableName = "Assign_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(3).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "SampleAssignSubject.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadAssignSubjectToTeacherList(HttpPostedFileBase file, int board, int cls)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("UploadAssignSubjectToTeacher");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);
                        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                        OleDbConnection connection = new OleDbConnection();
                        connection.ConnectionString = excelConnectionString;
                        OleDbDataAdapter command = new OleDbDataAdapter("select * from [Assign_Details$]", connection);
                        // connection.Open();
                        // Create DbDataReader to Data Worksheet
                        DataTable dt = new DataTable();
                        command.Fill(dt);
                        Guid id = new Guid(Session["id"].ToString());
                        var sectiondata = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true && a.Class_Id == cls && a.School_Id == id).ToList();
                        var subjectdata = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Class_Id == cls).ToList();
                        var teacherdata = DbContext.Teachers.Where(a => a.Active == 1).ToList();

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string subject = dr["Subject"].ToString();
                                string section = dr["Section"].ToString();
                                string teacher = dr["Teacher"].ToString();

                                if (subject != null && teacher != null && section != null)
                                {
                                    var mobile = teacher.Split('-')[1];
                                    var sectionlist = sectiondata.Where(a => a.SectionName == section).FirstOrDefault();
                                    var subjects = subjectdata.Where(a => a.Subject == subject).FirstOrDefault();
                                    var teacherlist = teacherdata.Where(a => a.Mobile == mobile).FirstOrDefault();


                                    tbl_DC_AssignSubjectToTeacher ob = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Teacher_ID == teacherlist.TeacherId && a.Is_Active == true && a.Board_Id == board && a.Class_Id == cls && a.SectionId == sectionlist.SectionId && a.Subject_Id == subjects.Subject_Id).FirstOrDefault();
                                    if (ob != null)
                                    {
                                        ob.Is_Active = false;
                                        ob.Modified_Date = DateTime.Now;
                                        DbContext.SaveChanges();
                                    }
                                    ob = new tbl_DC_AssignSubjectToTeacher();
                                    ob.Board_Id = board;
                                    ob.Class_Id = cls;
                                    ob.Inserted_Date = DateTime.Now;
                                    ob.Is_Active = true;
                                    ob.Modified_Date = DateTime.Now;
                                    ob.SectionId = sectionlist.SectionId;
                                    ob.Subject_Id = subjects.Subject_Id;
                                    ob.Teacher_ID = teacherlist.TeacherId;
                                    DbContext.tbl_DC_AssignSubjectToTeacher.Add(ob);
                                    DbContext.SaveChanges();
                                }


                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                    }

                    TempData["SuccessMessage"] = "Subject Assigned Successfully";
                    return RedirectToAction("UploadAssignSubjectToTeacher");
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("UploadAssignSubjectToTeacher");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("UploadAssignSubjectToTeacher");
            }
        }




        public ActionResult GetAssignSubjectToTeacher(int? board, int? cls, Guid? sec, int? subject, Guid? school)
        {
            //Guid id = new Guid(Session["id"].ToString());
            var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var schoollist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var classlist = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var sectionlist = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var subjectlist = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var teacherslist = DbContext.Teachers.Where(a => a.Active == 1).ToList();
            var assignteacherlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true).ToList();
            var res = (from a in assignteacherlist
                       join b in teacherslist on a.Teacher_ID equals b.TeacherId
                       join c in subjectlist on a.Subject_Id equals c.Subject_Id
                       join d in sectionlist on a.SectionId equals d.SectionId
                       join e in classlist on a.Class_Id equals e.Class_Id
                       join f in boardlist on a.Board_Id equals f.Board_Id
                       join g in schoollist on b.SchoolId equals g.SchoolId
                       //where b.SchoolId == id
                       select new AssignTeacherCls
                       {
                           AssignId = a.AssignSubjectToTeacher_Id,
                           TeacherId = a.Teacher_ID,
                           BoardId = a.Board_Id,
                           BoardName = f.Board_Name,
                           ClassId = a.Class_Id,
                           ClassName = e.Class_Name,
                           SchoolId = b.SchoolId,
                           SectionId = a.SectionId,
                           SectionName = d.SectionName,
                           SubjectId = a.Subject_Id,
                           SubjectName = c.Subject,
                           TeacherName = b.Name,
                           SchoolName = g.SchoolName
                       }).ToList();
            ViewBag.board = null;
            ViewBag.cls = null;
            ViewBag.sec = null;
            ViewBag.subject = null;
            ViewBag.school = null;
            if (board != null && board != 0)
            {
                res = res.Where(a => a.BoardId == board.Value).ToList();
                ViewBag.board = board.Value;
            }
            if (cls != null && cls != 0)
            {
                res = res.Where(a => a.ClassId == cls.Value).ToList();
                ViewBag.cls = cls.Value;
            }
            if (sec != null)
            {
                res = res.Where(a => a.SectionId == sec.Value).ToList();
                ViewBag.sec = sec.Value;
            }
            if (subject != null && subject != 0)
            {
                res = res.Where(a => a.SubjectId == subject.Value).ToList();
                ViewBag.subject = subject.Value;
            }
            if (school != null)
            {
                res = res.Where(a => a.SchoolId == school.Value).ToList();
                ViewBag.school = school.Value;
            }
            return View(res);

        }
        public ActionResult DeleteAssignSubject(int id)
        {
            tbl_DC_AssignSubjectToTeacher ob = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.AssignSubjectToTeacher_Id == id).FirstOrDefault();
            ob.Is_Active = false;
            DbContext.SaveChanges();
            return RedirectToAction("GetAssignSubjectToTeacher");
        }
        public ActionResult CreateAssignSubjectToTeacher()
        {
            // Guid id = new Guid(Session["id"].ToString());

            return View();
        }
        public JsonResult GetTeachersList(Guid id)
        {
            // Guid id = new Guid(Session["id"].ToString());
            var teacherslist = DbContext.Teachers.Where(a => a.Active == 1 && a.SchoolId == id).ToList();

            return Json(teacherslist, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CreateAssignSubjectDetails(int board, int cls, Guid sec, int subject, int tid)
        {
            //ArrayList subjectList = new ArrayList(subject);
            //ArrayList teacherList = new ArrayList(tid);

            //foreach (int sub in subjectList)
            //{

            //    foreach (int teacher in teacherList)
            //    {

            tbl_DC_AssignSubjectToTeacher ob = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Teacher_ID == tid && a.Is_Active == true && a.Board_Id == board && a.Class_Id == cls && a.SectionId == sec && a.Subject_Id == subject).FirstOrDefault();
            if (ob != null)
            {
                ob.Is_Active = false;
                ob.Modified_Date = DateTime.Now;
                DbContext.SaveChanges();
            }
            ob = new tbl_DC_AssignSubjectToTeacher();
            ob.Board_Id = board;
            ob.Class_Id = cls;
            ob.Inserted_Date = DateTime.Now;
            ob.Is_Active = true;
            ob.Modified_Date = DateTime.Now;
            ob.SectionId = sec;
            ob.Subject_Id = subject;
            ob.Teacher_ID = tid;
            DbContext.tbl_DC_AssignSubjectToTeacher.Add(ob);
            DbContext.SaveChanges();
            //    }

            //}
            TempData["SuccessMessage"] = "Assign Subject To Teacher Created successfully.";
            return RedirectToAction("GetAssignSubjectToTeacher", "SchoolReport");
        }
        #endregion

        #region--------------- GetRegisteredUserReport----------------
        public ActionResult GetRegisteredUserReport()
        {
            ViewBag.schoolist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).Distinct().ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadStudentDetails(HttpPostedFileBase file, int board, int cls, Guid sec)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("Uploadstudent");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);
                        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                        OleDbConnection connection = new OleDbConnection();
                        connection.ConnectionString = excelConnectionString;
                        OleDbDataAdapter command = new OleDbDataAdapter("select * from [Student_Details$]", connection);
                        // connection.Open();
                        // Create DbDataReader to Data Worksheet
                        DataTable dt = new DataTable();
                        command.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string mobile = dr["Mobile"].ToString();
                                string name = dr["Student_Name"].ToString();
                                Guid id = new Guid(Session["id"].ToString());
                                if (mobile != "" && name != "")
                                {
                                    var mob = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                                    if (mob == null)
                                    {
                                        tbl_DC_Registration reg = new tbl_DC_Registration();
                                        reg.Mobile = mobile;
                                        reg.Customer_Name = name;
                                        reg.Email = dr["Email"].ToString();
                                        reg.SchoolId = id;
                                        reg.SectionId = sec;
                                        reg.Is_Active = true;
                                        reg.Is_Deleted = false;
                                        reg.Inserted_By = HttpContext.User.Identity.Name;
                                        reg.Inserted_Date = DateTime.Now;
                                        DbContext.tbl_DC_Registration.Add(reg);
                                        DbContext.SaveChanges();

                                        var regno2 = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).Take(1).ToList();
                                        tbl_DC_Registration_Dtl m = new tbl_DC_Registration_Dtl();
                                        m.Regd_ID = regno2.ToList()[0].Regd_ID;
                                        m.Regd_No = regno2.ToList()[0].Regd_No;
                                        m.Board_ID = board;
                                        m.Class_ID = cls;
                                        m.Inserted_Date = DateTime.Now;
                                        m.Inserted_By = regno2.ToList()[0].Regd_ID.ToString();
                                        m.Is_Active = true;
                                        m.Is_Deleted = false;
                                        DbContext.tbl_DC_Registration_Dtl.Add(m);
                                        DbContext.SaveChanges();

                                        int reg_num1 = Convert.ToInt32(regno2.ToList()[0].Regd_ID);
                                        var pri = DbContext.tbl_DC_Prefix.Where(x => x.PrefixType_ID == 2).Select(x => x.Prefix_Name).FirstOrDefault();
                                        string prifix = pri.ToString();

                                        //registration no. of student autogenerate
                                        if (reg_num1 == 0)
                                        {
                                            reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "00000" + 1;
                                        }
                                        else
                                        {
                                            int regnum = Convert.ToInt32(regno2.ToList()[0].Regd_ID);
                                            if (regnum > 0 && regnum <= 9)
                                            {
                                                reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "00000" + Convert.ToString(regnum);
                                            }
                                            if (regnum > 9 && regnum <= 99)
                                            {
                                                reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "0000" + Convert.ToString(regnum);
                                            }
                                            if (regnum > 99 && regnum <= 999)
                                            {
                                                reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "000" + Convert.ToString(regnum);
                                            }
                                            if (regnum > 999 && regnum <= 9999)
                                            {
                                                reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "00" + Convert.ToString(regnum);
                                            }
                                            if (regnum > 9999 && regnum <= 99999)
                                            {
                                                reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "0" + Convert.ToString(regnum);
                                            }
                                        }
                                        DbContext.Entry(reg).State = EntityState.Modified;
                                        DbContext.SaveChanges();

                                        //-----------Add details to User-Security Table
                                        tbl_DC_USER_SECURITY obj1 = new tbl_DC_USER_SECURITY();
                                        obj1.USER_NAME = mobile;
                                        obj1.ROLE_CODE = "C";
                                        obj1.ROLE_TYPE = 1;
                                        obj1.USER_CODE = mobile;
                                        obj1.IS_ACCEPTED = false;                               //-------------To Change---------------------
                                        obj1.STATUS = "D";              //--------------random password------------------
                                        obj1.PASSWORD = DigiChampsModel.Encrypt_Password.HashPassword("12345").ToString();
                                        DbContext.tbl_DC_USER_SECURITY.Add(obj1);
                                        DbContext.SaveChanges();

                                        var regno1 = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).Take(1).ToList();



                                        string ucode = mobile;
                                        tbl_DC_USER_SECURITY obj4 = DbContext.tbl_DC_USER_SECURITY.Where(x => x.USER_CODE == ucode).FirstOrDefault();
                                        obj4.PASSWORD = DigiChampsModel.Encrypt_Password.HashPassword("12345").ToString();
                                        obj4.STATUS = "A";
                                        obj4.IS_ACCEPTED = true;
                                        DbContext.Entry(obj4).State = EntityState.Modified;
                                        //FormsAuthentication.RedirectFromLoginPage(obj4.USER_NAME, false);
                                        DbContext.SaveChanges();
                                        int regd = regno1.ToList()[0].Regd_ID;
                                        tbl_DC_Registration obj5 = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == regd).FirstOrDefault();
                                        obj5.Is_Active = true;
                                        obj5.Is_Deleted = false;
                                        obj5.Modified_Date = DateTime.Now;
                                        DbContext.Entry(obj5).State = EntityState.Modified;
                                        DbContext.SaveChanges();

                                    }
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                    }

                    TempData["SuccessMessage"] = "Student Details Entered Successfully";
                    return RedirectToAction("Uploadstudent");
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("Uploadstudent");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("Uploadstudent");
            }
        }




        public JsonResult GetRegisteredUsersDetails(Guid? school, int? board, int? cls, Guid? sec)
        {
            // Guid? school = new Guid(Session["id"].ToString());
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var classs = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
            var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var boards = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true).ToList();
            var res = (from a in students
                       join b in std_dtls on a.Regd_ID equals b.Regd_ID
                       // where a.SchoolId == school
                       //&& a.Regd_ID == 258
                       select new Schoolwise_User_Class
                       {
                           Regd_ID = a.Regd_ID,
                           Regd_No = a.Regd_No,
                           Customer_Name = a.Customer_Name,
                           SchoolId = (a.SchoolId == null) ? Guid.Empty : a.SchoolId,
                           Class_ID = b.Class_ID == null ? 0 : b.Class_ID,
                           SectionId = (a.SectionId == null) ? Guid.Empty : a.SectionId,
                           Board_ID = b.Board_ID == null ? 0 : b.Board_ID,
                           boardname = b.Board_ID == null ? "" : boards.Where(c => c.Board_Id == b.Board_ID).FirstOrDefault().Board_Name,
                           schoolname = (a.SchoolId == null) ? "" : schools.Where(c => c.SchoolId == a.SchoolId).FirstOrDefault().SchoolName,
                           Class_Name = b.Class_ID == null ? "" : classs.Where(c => c.Class_Id == b.Class_ID).FirstOrDefault().Class_Name,
                           SectionName = ((a.SectionId == null) ? "" : sections.Where(c => c.SectionId == a.SectionId).FirstOrDefault().SectionName),
                           Mobile = a.Mobile,
                           Email = a.Email,
                           School_No = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault().School_No : "") : "",
                           whatsapp_no = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault().Whatsapp_Mobile_No : "") : "",
                           zoom_mail_id = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault().Zoom_Mail_ID : "") : "",
                       }).ToList();
            if (school != null && school != Guid.Empty)
            {
                res = res.Where(a => a.SchoolId == school).ToList();
            }
            if (board != null)
            {
                res = res.Where(a => a.Board_ID == board).ToList();
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_ID == cls).ToList();
            }
            if (sec != null)
            {
                res = res.Where(a => a.SectionId == sec).ToList();
            }
            Session["Get_Registered_User"] = res;
            var json = Json(res, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = int.MaxValue;
            return json;
        }
        public ActionResult EditStudentDetails(int id)
        {
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Regd_ID == id).ToList();
            var std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true).ToList();
            var res = (from a in students
                       join b in std_dtls on a.Regd_ID equals b.Regd_ID
                       select new Schoolwise_User_Class
                       {
                           Regd_ID = a.Regd_ID,
                           Customer_Name = a.Customer_Name,
                           SchoolId = (a.SchoolId == null && a.SchoolId == Guid.Empty) ? Guid.Empty : a.SchoolId,
                           Class_ID = b.Class_ID == null ? null : b.Class_ID,
                           SectionId = (a.SectionId == null && a.SectionId == Guid.Empty) ? Guid.Empty : a.SectionId,
                           Board_ID = b.Board_ID == null ? null : b.Board_ID,
                           Mobile = a.Mobile,
                           Email = a.Email,
                           School_No = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault().School_No : "") : "",
                           whatsapp_no = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault().Whatsapp_Mobile_No : "") : "",
                           zoom_mail_id = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault().Zoom_Mail_ID : "") : "",
                       }).FirstOrDefault();
            ViewBag.studentdet = res;
            ViewBag.schoolist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).Distinct().ToList();
            ViewBag.boardlist = DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
            ViewBag.classlist = DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false && x.Board_Id == res.Board_ID).Distinct().ToList();
            ViewBag.sectionlist = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.Class_Id == res.Class_ID && x.School_Id == res.SchoolId).Distinct().ToList();

            return View();
        }
        [HttpPost]
        public ActionResult UpdateStudentDetails(int Regd_ID, Guid school, int cls, Guid sec, int board, string Mobile, string Email, string Customer_Name, string schoolno, string zoomid, string whatsappno)
        {
            var registration = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
            tbl_DC_Registration students = registration.Where(a => a.Regd_ID == Regd_ID).FirstOrDefault();
            if (students != null)
            {
                var chk = registration.Where(a => a.Regd_ID != Regd_ID && a.Mobile == Mobile).ToList();
                if (chk.Count > 0)
                {
                    TempData["ErrorMessage"] = "Enter Another Mobile";
                    return RedirectToAction("EditStudentDetails", new { id = Regd_ID });
                }
                else
                {
                    tbl_DC_USER_SECURITY security = DbContext.tbl_DC_USER_SECURITY.Where(a => a.USER_CODE == students.Mobile).FirstOrDefault();
                    if (security != null)
                    {
                        var chk1 = DbContext.tbl_DC_USER_SECURITY.Where(a => a.USER_CODE == Mobile).FirstOrDefault();
                        if (chk1 != null && chk1.USER_CODE != students.Mobile)
                        {
                            TempData["ErrorMessage"] = "Enter Another Mobile";
                            return RedirectToAction("EditStudentDetails", new { id = Regd_ID });
                        }
                        else if (students.Mobile != Mobile)
                        {

                            tbl_DC_USER_SECURITY obj1 = new tbl_DC_USER_SECURITY();
                            obj1.USER_NAME = Mobile;
                            obj1.ROLE_CODE = "C";
                            obj1.ROLE_TYPE = 1;
                            obj1.USER_CODE = Mobile;
                            obj1.IS_ACCEPTED = true;                               //-------------To Change---------------------
                            obj1.STATUS = "A";              //--------------random password------------------
                            obj1.PASSWORD = security.PASSWORD;
                            DbContext.tbl_DC_USER_SECURITY.Add(obj1);

                            DbContext.tbl_DC_USER_SECURITY.Remove(security);
                            DbContext.SaveChanges();
                        }
                        else { }
                    }

                    students.Customer_Name = Customer_Name;
                    students.Email = Email;
                    students.Mobile = Mobile;
                    students.SectionId = sec;
                    students.SchoolId = school;
                    DbContext.SaveChanges();
                    tbl_DC_Registration_Dtl std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Regd_ID == Regd_ID).FirstOrDefault();
                    std_dtls.Board_ID = board;
                    std_dtls.Class_ID = cls;
                    std_dtls.Modified_Date = DateTime.Now;
                    DbContext.SaveChanges();

                    var worksheet = DbContext.tbl_DC_Worksheet.Where(a => a.Regd_ID == Regd_ID && a.Is_Active == true && a.Is_Deleted == false).ToList();
                    foreach (tbl_DC_Worksheet wrk in worksheet)
                    {
                        wrk.Board_Id = 1;
                        wrk.Class_Id = cls;
                        wrk.SectionId = sec;
                        wrk.Modified_Date = DateTime.Now;
                        DbContext.SaveChanges();
                    }
                    var ticket = DbContext.tbl_DC_Ticket.Where(a => a.Student_ID == Regd_ID).ToList();
                    foreach (tbl_DC_Ticket wrk in ticket)
                    {
                        wrk.Board_ID = 1;
                        wrk.Class_ID = cls;
                        wrk.Modified_Date = DateTime.Now;
                        DbContext.SaveChanges();

                    }
                    tbl_Regd_BasicDetl tbl_Regd_Basic = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == Regd_ID && a.Is_Active == true).FirstOrDefault();
                    if (tbl_Regd_Basic == null)
                    {
                        tbl_Regd_Basic = new tbl_Regd_BasicDetl();
                    }
                    tbl_Regd_Basic.Modified_By = HttpContext.User.Identity.Name;
                    tbl_Regd_Basic.Modified_Date = DateTime.Now;
                    tbl_Regd_Basic.Is_Active = true;
                    tbl_Regd_Basic.Regd_ID = Regd_ID;
                    tbl_Regd_Basic.School_No = schoolno;
                    tbl_Regd_Basic.Whatsapp_Mobile_No = whatsappno;
                    tbl_Regd_Basic.Zoom_Mail_ID = zoomid;
                    if (tbl_Regd_Basic.Regd_BasicDet_ID == 0)
                    {
                        tbl_Regd_Basic.Inserted_By = HttpContext.User.Identity.Name;
                        tbl_Regd_Basic.Inserted_Date = DateTime.Now;
                        DbContext.tbl_Regd_BasicDetl.Add(tbl_Regd_Basic);
                    }
                    DbContext.SaveChanges();
                    return RedirectToAction("GetRegisteredUserReport", "SchoolReport");

                }
            }

            else
            {
                TempData["ErrorMessage"] = "Already Exist! Please Enter Another Mobile Number";
                return RedirectToAction("EditStudentDetails", new { id = Regd_ID });
            }
        }
        public ActionResult CreateStudentDetails()
        {
            //var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Regd_ID == id).ToList();
            var std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            //var res = (from a in students
            //           join b in std_dtls on a.Regd_ID equals b.Regd_ID
            //           select new Schoolwise_User_Class
            //           {
            //               Regd_ID = a.Regd_ID,
            //               Customer_Name = a.Customer_Name,
            //               SchoolId = (a.SchoolId == null && a.SchoolId == Guid.Empty) ? Guid.Empty : a.SchoolId,
            //               Class_ID = b.Class_ID == null ? null : b.Class_ID,
            //               SectionId = (a.SectionId == null && a.SectionId == Guid.Empty) ? Guid.Empty : a.SectionId,
            //               Board_ID = b.Board_ID == null ? null : b.Board_ID,
            //               Mobile = a.Mobile,
            //               Email = a.Email
            //           }).FirstOrDefault();
            //ViewBag.studentdet = res;
            ViewBag.schoolist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).Distinct().ToList();
            ViewBag.boardlist = DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
            //ViewBag.classlist = DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
            //ViewBag.sectionlist = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true).Distinct().ToList();

            return View();
        }
        [HttpPost]
        public ActionResult SaveStudentDetails(Guid school, int cls, Guid sec, int board, string Mobile, string Email, string Customer_Name, string schoolno, string zoomid, string whatsappno)
        {

            //tbl_DC_Registration students = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == Regd_ID).FirstOrDefault();
            string mobile = Mobile;
            if (mobile != "" && Customer_Name != "")
            {
                var mob = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                if (mob == null)
                {
                    tbl_DC_Registration reg = new tbl_DC_Registration();
                    reg.Mobile = mobile;
                    reg.Customer_Name = Customer_Name;
                    reg.Email = Email.ToString();
                    reg.SchoolId = school;
                    reg.SectionId = sec;
                    reg.Is_Active = true;
                    reg.Is_Deleted = false;
                    reg.Inserted_By = HttpContext.User.Identity.Name;
                    reg.Inserted_Date = DateTime.Now;
                    DbContext.tbl_DC_Registration.Add(reg);
                    DbContext.SaveChanges();

                    var regno2 = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).Take(1).ToList();
                    tbl_DC_Registration_Dtl m = new tbl_DC_Registration_Dtl();
                    m.Regd_ID = regno2.ToList()[0].Regd_ID;
                    m.Regd_No = regno2.ToList()[0].Regd_No;
                    m.Board_ID = board;
                    m.Class_ID = cls;
                    m.Inserted_Date = DateTime.Now;
                    m.Inserted_By = regno2.ToList()[0].Regd_ID.ToString();
                    m.Is_Active = true;
                    m.Is_Deleted = false;
                    DbContext.tbl_DC_Registration_Dtl.Add(m);
                    DbContext.SaveChanges();

                    int reg_num1 = Convert.ToInt32(regno2.ToList()[0].Regd_ID);
                    var pri = DbContext.tbl_DC_Prefix.Where(x => x.PrefixType_ID == 2).Select(x => x.Prefix_Name).FirstOrDefault();
                    string prifix = pri.ToString();

                    //registration no. of student autogenerate
                    if (reg_num1 == 0)
                    {
                        reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "00000" + 1;
                    }
                    else
                    {
                        int regnum = Convert.ToInt32(regno2.ToList()[0].Regd_ID);
                        if (regnum > 0 && regnum <= 9)
                        {
                            reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "00000" + Convert.ToString(regnum);
                        }
                        if (regnum > 9 && regnum <= 99)
                        {
                            reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "0000" + Convert.ToString(regnum);
                        }
                        if (regnum > 99 && regnum <= 999)
                        {
                            reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "000" + Convert.ToString(regnum);
                        }
                        if (regnum > 999 && regnum <= 9999)
                        {
                            reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "00" + Convert.ToString(regnum);
                        }
                        if (regnum > 9999 && regnum <= 99999)
                        {
                            reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "0" + Convert.ToString(regnum);
                        }
                    }
                    DbContext.Entry(reg).State = EntityState.Modified;
                    DbContext.SaveChanges();

                    //-----------Add details to User-Security Table
                    tbl_DC_USER_SECURITY obj1 = new tbl_DC_USER_SECURITY();
                    obj1.USER_NAME = mobile;
                    obj1.ROLE_CODE = "C";
                    obj1.ROLE_TYPE = 1;
                    obj1.USER_CODE = mobile;
                    obj1.IS_ACCEPTED = true;                               //-------------To Change---------------------
                    obj1.STATUS = "A";
                    obj1.PASSWORD = DigiChampsModel.Encrypt_Password.HashPassword("12345").ToString();
                    DbContext.tbl_DC_USER_SECURITY.Add(obj1);
                    DbContext.SaveChanges();

                    var regno1 = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).Take(1).ToList();



                    string ucode = mobile;
                    tbl_DC_USER_SECURITY obj4 = DbContext.tbl_DC_USER_SECURITY.Where(x => x.USER_CODE == ucode).FirstOrDefault();
                    obj4.PASSWORD = DigiChampsModel.Encrypt_Password.HashPassword("12345").ToString();
                    obj4.STATUS = "A";
                    obj4.IS_ACCEPTED = true;
                    DbContext.Entry(obj4).State = EntityState.Modified;
                    //FormsAuthentication.RedirectFromLoginPage(obj4.USER_NAME, false);
                    DbContext.SaveChanges();
                    int regd = regno1.ToList()[0].Regd_ID;
                    tbl_DC_Registration obj5 = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == regd).FirstOrDefault();
                    obj5.Is_Active = true;
                    obj5.Is_Deleted = false;
                    obj5.Modified_Date = DateTime.Now;
                    DbContext.Entry(obj5).State = EntityState.Modified;
                    DbContext.SaveChanges();

                    tbl_Regd_BasicDetl tbl_Regd_Basic = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == reg_num1 && a.Is_Active == true).FirstOrDefault();
                    if (tbl_Regd_Basic == null)
                    {
                        tbl_Regd_Basic = new tbl_Regd_BasicDetl();
                    }
                    tbl_Regd_Basic.Modified_By = HttpContext.User.Identity.Name;
                    tbl_Regd_Basic.Modified_Date = DateTime.Now;
                    tbl_Regd_Basic.Is_Active = true;
                    tbl_Regd_Basic.Regd_ID = reg_num1;
                    tbl_Regd_Basic.School_No = schoolno;
                    tbl_Regd_Basic.Whatsapp_Mobile_No = whatsappno;
                    tbl_Regd_Basic.Zoom_Mail_ID = zoomid;
                    if (tbl_Regd_Basic.Regd_BasicDet_ID == 0)
                    {
                        tbl_Regd_Basic.Inserted_By = HttpContext.User.Identity.Name;
                        tbl_Regd_Basic.Inserted_Date = DateTime.Now;
                        DbContext.tbl_Regd_BasicDetl.Add(tbl_Regd_Basic);
                    }
                    DbContext.SaveChanges();
                }
                else
                {
                    TempData["ErrorMessage"] = "Already Exist! Please Enter Another Mobile Number";
                    return RedirectToAction("CreateStudentDetails");
                }
            }

            else
            {
                TempData["ErrorMessage"] = "Already Exist! Please Enter Another Mobile Number";
                return RedirectToAction("CreateStudentDetails");
            }

            return RedirectToAction("GetRegisteredUserReport");
        }


        public ActionResult DeleteStudentDetails(int id)
        {
            tbl_DC_Registration students = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == id).FirstOrDefault();
            students.Is_Active = false;
            students.Is_Deleted = true;

            DbContext.SaveChanges();
            tbl_DC_Registration_Dtl std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Regd_ID == id).FirstOrDefault();
            std_dtls.Is_Active = false;
            std_dtls.Is_Deleted = true;

            DbContext.SaveChanges();
            return RedirectToAction("GetRegisteredUserReport", "SchoolReport");
        }
        public JsonResult GetBoardList()
        {
            var board = DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
            return Json(board, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetClassListdtl(int id)
        {
            var board = DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false && x.Board_Id == id).Distinct().ToList();
            return Json(board, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSectionListdtl(int id, Guid school)
        {

            var board = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.Class_Id == id && x.School_Id == school).Distinct().ToList();
            return Json(board, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStudentListdtl(Guid sectionid)
        {
            var board = DbContext.tbl_DC_Registration.Where(x => x.Is_Active == true && x.SectionId == sectionid).Distinct().ToList();
            return Json(board, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllStudentListdtl(Guid? schoolid, int? standardid)
        {

            var board = DbContext.tbl_DC_Registration.Where(x => x.Is_Active == true).Distinct().ToList();
            if (schoolid != null)
            {
                board = board.Where(a => a.SchoolId == schoolid).ToList();
            }
            if (standardid != null)
            {
                var regdet = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Class_ID == standardid).Select(a => a.Regd_ID).ToList();
                board = board.Where(a => regdet.Contains(a.Regd_ID)).ToList();
            }
            return Json(board, JsonRequestBehavior.AllowGet);
        }
        public class Schoolwise_User_Class
        {
            public int? Regd_ID { set; get; }
            public string Regd_No { get; set; }
            public string Customer_Name { get; set; }
            public Nullable<Guid> SchoolId { get; set; }
            public Nullable<int> Class_ID { get; set; }
            public Nullable<Guid> SectionId { get; set; }
            public Nullable<int> Board_ID { get; set; }
            public string boardname { get; set; }
            public string schoolname { get; set; }
            public string Class_Name { get; set; }
            public string SectionName { get; set; }
            public string Mobile { get; set; }
            public string Email { get; set; }
            public string School_No { get; set; }
            public string zoom_mail_id { get; set; }
            public string whatsapp_no { get; set; }
            public long CategoryId { get; set; }
            public long EReportId { get; set; }
            public string EReportPath { get; set; }
            public string categoryName { get; set; }
        }
        public ActionResult Exportallregisteredreportdata()
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();
            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Sl. No.");
            validationTable.Columns.Add("School Name");
            validationTable.Columns.Add("Regd No");
            validationTable.Columns.Add("School No");
            validationTable.Columns.Add("Student Name");
            validationTable.Columns.Add("Board");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Section");
            validationTable.Columns.Add("Zoom Mail ID");
            validationTable.Columns.Add("Whatsapp No");
            validationTable.Columns.Add("Mobile");

            validationTable.TableName = "Registered_Details";
            List<Schoolwise_User_Class> res = (Session["Get_Registered_User"]) as List<Schoolwise_User_Class>;
            int i = 0;
            foreach (var gs in res.ToList())
            {
                DataRow dr = validationTable.NewRow();
                dr["Sl. No."] = i + 1;
                dr["School Name"] = gs.schoolname;
                dr["Regd No"] = gs.Regd_No;
                dr["School No"] = gs.School_No;
                dr["Student Name"] = gs.Customer_Name;
                dr["Board"] = gs.boardname;
                dr["Class"] = gs.Class_Name;
                dr["Section"] = gs.SectionName;
                dr["Zoom Mail ID"] = gs.zoom_mail_id;
                dr["Whatsapp No"] = gs.whatsapp_no;
                dr["Mobile"] = gs.Mobile;
                validationTable.Rows.Add(dr);
                i += 1;
            }



            oWB.Worksheets.Add(validationTable);
            //oWB.SaveAs("User_Details.xlsx");
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Registered_User_Details.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            TempData["Message"] = "Successfully Downloaded Excel File";
            return RedirectToAction("GetRegisteredUserReport", "SchoolReport");
        }
        [HttpPost]
        public ActionResult UploadUserBasicDetails(HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("GetRegisteredUserReport");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);
                        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                        OleDbConnection connection = new OleDbConnection();
                        connection.ConnectionString = excelConnectionString;
                        OleDbDataAdapter command = new OleDbDataAdapter("select * from [Registered_Details$]", connection);
                        // connection.Open();
                        // Create DbDataReader to Data Worksheet
                        DataTable dt = new DataTable();
                        command.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string mobile = dr["Mobile"].ToString();
                                string name = dr["Student Name"].ToString();
                                if (mobile != "" && name != "")
                                {
                                    tbl_DC_Registration reg = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                                    if (reg != null)
                                    {
                                        string schoolname = dr["School Name"].ToString();
                                        string classname = dr["Class"].ToString();
                                        string sectionname = dr["Section"].ToString();
                                        var schoolid = DbContext.tbl_DC_School_Info.Where(a => a.SchoolName == schoolname && a.IsActive == true).FirstOrDefault().SchoolId;
                                        var classid = DbContext.tbl_DC_Class.Where(a => a.Class_Name == classname && a.Board_Id == 1 && a.Is_Active == true && a.Is_Deleted == false).FirstOrDefault().Class_Id;
                                        var sectionid = DbContext.tbl_DC_Class_Section.Where(a => a.SectionName == sectionname && a.Class_Id == classid && a.School_Id == schoolid && a.IsActive == true).FirstOrDefault().SectionId;

                                        reg.Customer_Name = name;
                                        reg.SchoolId = schoolid;
                                        reg.SectionId = sectionid;
                                        reg.Modified_By = HttpContext.User.Identity.Name;
                                        reg.Modified_Date = DateTime.Now;
                                        DbContext.SaveChanges();
                                        var regid = reg.Regd_ID;

                                        tbl_DC_Registration_Dtl m = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == regid && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();

                                        m.Board_ID = 1;
                                        m.Class_ID = classid;
                                        m.Modified_Date = DateTime.Now;
                                        m.Modified_By = reg.Regd_ID.ToString();
                                        DbContext.SaveChanges();
                                        tbl_Regd_BasicDetl tbl_Regd_Basic = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regid && a.Is_Active == true).FirstOrDefault();
                                        if (tbl_Regd_Basic == null)
                                        {
                                            tbl_Regd_Basic = new tbl_Regd_BasicDetl();
                                        }
                                        tbl_Regd_Basic.Modified_By = HttpContext.User.Identity.Name;
                                        tbl_Regd_Basic.Modified_Date = DateTime.Now;
                                        tbl_Regd_Basic.Is_Active = true;
                                        tbl_Regd_Basic.Regd_ID = regid;
                                        tbl_Regd_Basic.School_No = dr["School No"].ToString();
                                        tbl_Regd_Basic.Whatsapp_Mobile_No = dr["Whatsapp No"].ToString();
                                        tbl_Regd_Basic.Zoom_Mail_ID = dr["Zoom Mail ID"].ToString();
                                        if (tbl_Regd_Basic.Regd_BasicDet_ID == 0)
                                        {
                                            tbl_Regd_Basic.Inserted_By = HttpContext.User.Identity.Name;
                                            tbl_Regd_Basic.Inserted_Date = DateTime.Now;
                                            DbContext.tbl_Regd_BasicDetl.Add(tbl_Regd_Basic);
                                        }
                                        else
                                        {
                                            tbl_Regd_Basic.Modified_By = HttpContext.User.Identity.Name;
                                            tbl_Regd_Basic.Modified_Date = DateTime.Now;
                                            //DbContext.tbl_Regd_BasicDetl.Add(tbl_Regd_Basic);
                                        }

                                        DbContext.SaveChanges();

                                        var worksheet = DbContext.tbl_DC_Worksheet.Where(a => a.Regd_ID == regid && a.Is_Active == true && a.Is_Deleted == false).ToList();
                                        foreach (tbl_DC_Worksheet wrk in worksheet)
                                        {
                                            wrk.Board_Id = 1;
                                            wrk.Class_Id = classid;
                                            wrk.SectionId = sectionid;
                                            DbContext.SaveChanges();

                                        }
                                        var ticket = DbContext.tbl_DC_Ticket.Where(a => a.Student_ID == regid).ToList();
                                        foreach (tbl_DC_Ticket wrk in ticket)
                                        {
                                            wrk.Board_ID = 1;
                                            wrk.Class_ID = classid;
                                            DbContext.SaveChanges();

                                        }
                                    }
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                    }

                    TempData["SuccessMessage"] = "Student Details Entered Successfully";
                    return RedirectToAction("GetRegisteredUserReport");
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("GetRegisteredUserReport");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("GetRegisteredUserReport");
            }
        }
        #endregion

        //public void Menupermission()
        //{
        //    try
        //    {
        //        if (Convert.ToString(Session["ROLE_CODE"]) == "S")
        //        {
        //            if (Session["Role_Id"] != null)
        //            {
        //                int roleid = Convert.ToInt32(Session["Role_Id"]);
        //                var permission = (from c in DbContext.tbl_DC_Menuitems.Where(x => x.Is_Active == true && x.Is_Deleted == false)
        //                                  join a in DbContext.tbl_DC_Menu_Permission.Where(x => x.ROLE_ID == roleid && x.Is_Active == true && x.Is_Deleted == false)
        //                                  on c.Menu_Key_ID equals a.Menu_Key_ID
        //                                  join b in DbContext.tbl_DC_Role.Where(x => x.ROLE_CODE == "S" && x.IS_ACTIVE == true && x.IS_DELTED == false)
        //                                  on a.ROLE_ID equals b.ROLE_ID
        //                                  select new DigiChampsModel.MenuPermissionModel
        //                                  {
        //                                      Menu_Parameter1 = c.Menu_Parameter1,
        //                                      Menu_Parameter2 = c.Menu_Parameter2,
        //                                      Menu_Parameter3 = c.Menu_Parameter3,
        //                                      Role_Type_ID = a.Role_Type_ID,
        //                                      ROLE_ID = b.ROLE_ID,
        //                                      ROLE_CODE = b.ROLE_CODE,
        //                                      ROLE_TYPE = b.ROLE_TYPE,
        //                                      Menu_Key_ID = a.Menu_Key_ID
        //                                  }).ToList();
        //                ViewBag.permission1 = permission.Count;
        //                ViewBag.permission = permission;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["ErrorMessage"] = "Something went wrong.";
        //    }
        //}

        public List<List<string>> splitList(List<string> locations, int nSize = 1000)
        {
            var list = new List<List<string>>();

            for (int i = 0; i < locations.Count; i += nSize)
            {
                list.Add(locations.GetRange(i, Math.Min(nSize, locations.Count - i)));
            }

            return list;
        }

        #region--------------- Module---------------

        #region ...........Worksheet Upload..............
        [HttpGet]
        public ActionResult ViewWorksheet(int? board, int? cls, int? subject, int? chapter)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Worksheet";
            var res = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                       join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on a.Board_Id equals b.Board_Id

                       join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on b.Class_Id equals c.Class_Id
                       join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on c.Subject_Id equals d.Subject_Id
                       join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on d.Chapter_Id equals f.Chapter_Id
                       where f.Question_PDF != null
                       select new DigiChampsModel.DigiChampsModuleModel
                       {
                           Module_ID = f.Module_ID,
                           Module_Name = f.Module_Name,
                           Chapter_Id = f.Chapter_Id,
                           Chapter = d.Chapter,
                           Subject_Id = d.Subject_Id,
                           Subject = c.Subject,
                           Board_Id = b.Board_Id,
                           Board_Name = a.Board_Name,
                           Class_Id = c.Class_Id,
                           Class_Name = b.Class_Name,
                           Module_Desc = f.Module_Desc,
                           Module_Content = f.Module_Content,
                           Module_video = f.Module_video,
                           Is_Free = f.Is_Free,
                           Question_PDF_Name = f.Question_PDF_Name,
                           Upload_PDF = f.Module_Content_Name,
                           Video_Type = f.Video_Type,
                           Class_PPT = f.Class_PPT,
                           Class_PPT_Name = f.Class_PPT_Name,
                           NCERT_PDF = f.NCERT_PDF,
                           NCERT_PDF_Name = f.NCERT_PDF_Name,
                           ExamTypeName = f.Exam_Type == null ? "" : DbContext.OnlineExamTypes.Where(m => m.OnlineExamTypeId == f.Module_ID).FirstOrDefault().OnlineExamTypeName,
                           Question_PDF = f.Question_PDF,
                           Module_Image = f.Module_Image
                       }).OrderByDescending(x => x.Module_ID).ToList();
            ViewBag.board = 0;
            ViewBag.cls = 0;
            ViewBag.subject = 0;
            ViewBag.chapter = 0;
            if (board != null)
            {
                res = res.Where(a => a.Board_Id == board).ToList();
                ViewBag.board = board;
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_Id == cls).ToList();
                ViewBag.cls = cls;
            }
            if (subject != null)
            {
                res = res.Where(a => a.Subject_Id == subject).ToList();
                ViewBag.subject = subject;
            }
            if (chapter != null)
            {
                res = res.Where(a => a.Chapter_Id == chapter).ToList();
                ViewBag.chapter = chapter;
            }
            // ViewBag.moduledata = res;
            return View(res);
        }
        [HttpGet]
        public ActionResult CreateOrEditWorksheet(string id, string returnurl)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id1 = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id1 && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Worksheet";
            ViewBag.returnurl = returnurl;
            ViewBag.examtypes = new SelectList(DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList(), "OnlineExamTypeId", "OnlineExamTypeName");
            ViewBag.upload_data = "";//otp_data;
            ViewBag.board = new SelectList(DbContext.tbl_DC_Board.Where(b => b.Is_Active == true && b.Is_Deleted == false), "Board_Id", "Board_Name");
            if (id != "" && id != null)
            {
                UInt32 moduleid;
                if (UInt32.TryParse(id, out moduleid) == true)
                {
                    int mid = Convert.ToInt32(moduleid);
                    DigiChampsModel.DigiChampsModuleModel obj = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                 join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on a.Board_Id equals b.Board_Id
                                                                 join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on b.Class_Id equals c.Class_Id
                                                                 join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on c.Subject_Id equals d.Subject_Id
                                                                 join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on d.Chapter_Id equals f.Chapter_Id
                                                                 select new DigiChampsModel.DigiChampsModuleModel
                                                                 {
                                                                     Board_Id = a.Board_Id,
                                                                     Class_Id = b.Class_Id,
                                                                     Subject_Id = c.Subject_Id,
                                                                     Chapter_Id = d.Chapter_Id,
                                                                     Module_ID = f.Module_ID,
                                                                     Module_Name = f.Module_Name,
                                                                     Module_Desc = f.Module_Desc,
                                                                     Module_Content = f.Module_Content,
                                                                     Module_video = f.Module_video,
                                                                     Is_Free = f.Is_Free,
                                                                     Validity = f.Validity,
                                                                     Question_PDF = f.Question_PDF,
                                                                     Is_Free_Test = f.Is_Free_Test,
                                                                     No_Question = f.No_Of_Question,
                                                                     Upload_PDF = f.Module_Content_Name,
                                                                     Question_PDF_Name = f.Question_PDF_Name,
                                                                     Video_Type = f.Video_Type,
                                                                     NCERT_PDF = f.NCERT_PDF,
                                                                     NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                                     Class_PPT = f.Class_PPT,
                                                                     Class_PPT_Name = f.Class_PPT_Name,
                                                                     OnlineExamTypeId = f.Exam_Type,
                                                                     videoid = f.Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + f.Module_video) : f.Module_video
                                                                 }).Where(x => x.Module_ID == mid).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.classid = obj.Class_Id;
                        ViewBag.subid = obj.Subject_Id;
                        ViewBag.chapterid = obj.Chapter_Id;
                        ViewBag.Modelvideo = obj.Module_video;
                        ViewBag.modelcontent = obj.Module_Content;
                        ViewBag.qstnpdf = obj.Question_PDF;
                        ViewBag.ncertpdf = obj.NCERT_PDF;
                        ViewBag.classppt = obj.Class_PPT;
                        return View(obj);
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Invalid module details.";
                    }
                }
                else
                {
                    TempData["WarningMessage"] = "Invalid module details.";
                }
            }

            return View();
        }
        [HttpPost]
        public ActionResult SaveOrUpdateWorksheet(DigiChampsModel.DigiChampsModuleModel obj,
            HttpPostedFileBase Question_PDF,
            string returnurl)
        {
            //Menupermission();
            ViewBag.Breadcrumb = "Worksheet";
            string msg = string.Empty;
            try
            {
                ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
                if (ModelState.IsValid)
                {
                    //if (obj.Module_Name.Trim() != "")
                    //{
                    if (obj.Module_ID == null)
                    {
                        var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Question_PDF != null && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (module != null)
                        {
                            TempData["WarningMessage"] = "Worksheet already exists.";
                        }
                        else
                        {
                            tbl_DC_Module obj1 = new tbl_DC_Module();
                            obj1.Module_Name = obj.Module_Name;
                            obj1.Module_Desc = obj.Module_Desc;
                            obj1.Board_Id = obj.Board_Id;
                            obj1.Class_Id = obj.Class_Id;
                            obj1.Subject_Id = obj.Subject_Id;
                            obj1.Chapter_Id = obj.Chapter_Id;
                            obj1.Exam_Type = obj.OnlineExamTypeId;
                            obj1.Is_Free = true;
                            if (obj1.Is_Free == true)
                            {
                                obj1.Validity = obj.Validity;
                            }
                            if (Question_PDF != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(Question_PDF.FileName.Replace(Question_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Module/Question_PDF/"), fileName);
                                Question_PDF.SaveAs(path);
                                obj1.Question_PDF = fileName;
                                obj1.No_Of_Question = obj.No_Question;
                            }
                            string extramsg = "";
                            obj1.Question_PDF_Name = obj.Question_PDF_Name;
                            //if (obj.Is_Free_Test == true)
                            //{
                            //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id ==
                            //        obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                            //    if (iftest_chap == null)
                            //    {
                            obj1.Is_Free_Test = true;
                            //    }
                            //    else
                            //    {
                            //        obj1.Is_Free_Test = false;
                            //        extramsg = " Each chapter can only contain single free test.";
                            //    }
                            //}
                            obj1.Inserted_By = HttpContext.User.Identity.Name;
                            obj1.Is_Active = true;
                            obj1.Is_Deleted = false;
                            DbContext.tbl_DC_Module.Add(obj1);
                            DbContext.SaveChanges();
                            try
                            {
                                var deciceid = DbContext.tbl_DC_Registration.
                                    Where(x => x.Device_id != null && !x.Device_id.Equals("null")
                                        && obj1.Class_Id == obj.Class_Id).Select(x => x.Device_id).ToList();
                                // var regid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Class_ID == obj.Class_Id && x.Regd_ID != null).Select(x => x.Regd_ID).ToList();
                                //var deciceid = DbContext.tbl_DC_Registration.Where
                                //    (x => x.Device_id == "ePEyVbqUrBA:APA91bGzpOkF_jPnlBKlQFydnOkcy52DY7Wl-6ks1izlpZDfd4Jq8RN5gQNUquScLd0LzIB91nVwG5NPXxlJazQRXvbTFZ_COIS4KgumbuIsYfN41n5ezij4UKNMGwk9rUfCfxQKWUtZ" ||
                                //     x.Device_id == "c6Ixravhr_Q:APA91bFoGlTItg7aEe2Sz6_80fv9Rjy7IhSDAsaYVOVnh75891RFcJ-dlUGK4V9Pfxbj0GeS_sf_lC3cE-l9C_wzlsGYV2VdrQzmKeGMfTS2dpo1Q_R2Tuv3hSotogJy9yuONkywMcw5"
                                //     ).Select(x => x.Device_id).ToList();

                                //string body = "Body";.Replace("{{video}}", obj1.Module_video)
                                string title = "New Video";
                                string body_header = "New Video#{{Module_ID}}# Upload New Video : Hello {{Name}},! You have New Video.";
                                string body = body_header.ToString().Replace("{{Name}}",
                                    obj1.Module_Name).Replace("{{image}}", obj1.Module_Image).Replace("{{Feed_ID}}", obj1.Module_ID.ToString());
                                body = "NVID#{{Module_ID}}#New Video#Don't miss the new video. Check it out";

                                List<List<string>> ids = splitList(deciceid);

                                for (int i = 0; i < ids.Count; i++)
                                {
                                    if (ids[i] != null)
                                    {
                                        var note = new PushNotiStatusVideo(title, body, obj1.Module_Image, ids[i]);
                                    }
                                }
                            }
                            catch (Exception)
                            {

                            }
                            msg = "1";      //"Module details saved successfully.";
                            TempData["SuccessMessage"] = "Worksheet details saved successfully." + extramsg;
                        }
                    }
                    else
                    {
                        var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Question_PDF != null && x.Module_ID != obj.Module_ID && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (module != null)
                        {
                            TempData["WarningMessage"] = "Worksheet Already exist.";
                        }
                        else
                        {
                            int mod_id = Convert.ToInt32(obj.Module_ID);
                            tbl_DC_Module obj1 = DbContext.tbl_DC_Module.Where(x => x.Module_ID == mod_id).FirstOrDefault();
                            obj1.Module_Name = obj.Module_Name;
                            obj1.Module_Desc = obj.Module_Desc;
                            obj1.Is_Free = true;
                            obj1.Board_Id = obj.Board_Id;
                            obj1.Class_Id = obj.Class_Id;
                            obj1.Subject_Id = obj.Subject_Id;
                            obj1.Chapter_Id = obj.Chapter_Id;
                            obj1.Exam_Type = obj.OnlineExamTypeId;
                            if (obj1.Is_Free == true)
                            {
                                obj1.Validity = obj.Validity;
                            }
                            if (Question_PDF != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(Question_PDF.FileName.Replace(Question_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Module/Question_PDF/"), fileName);
                                Question_PDF.SaveAs(path);
                                obj1.Question_PDF = fileName;
                                obj1.No_Of_Question = obj.No_Question;
                            }
                            obj1.Question_PDF_Name = obj.Question_PDF_Name;
                            //if (obj.Is_Free_Test == true)
                            //{
                            //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                            //    if (iftest_chap == null)
                            //    {
                            obj1.Is_Free_Test = true;
                            //    }
                            //    else
                            //    {
                            //        obj1.Is_Free_Test = false;
                            //    }
                            //}

                            obj1.Inserted_By = HttpContext.User.Identity.Name;
                            obj1.Is_Active = true;
                            obj1.Is_Deleted = false;
                            obj1.Modified_Date = today;
                            obj1.Modified_By = HttpContext.User.Identity.Name;
                            DbContext.Entry(obj1).State = EntityState.Modified;
                            DbContext.SaveChanges();
                            msg = "11";         // "Module details updated successfully.";
                            TempData["SuccessMessage"] = "Worksheet details updated successfully";
                        }
                    }
                    //}
                    //else
                    //{
                    //    msg = "00";         // "Please enter module name.";
                    //}
                }
            }
            catch (Exception ex)
            {
                //msg = "Something went wrong.";
                throw ex;
            }
            //return Json(msg, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("ViewModule");
            return Redirect(returnurl);
        }
        [HttpPost]
        public ActionResult MultipleDeleteWorksheet(int[] chkselect)
        {
            foreach (int a in chkselect)
            {
                tbl_DC_Module md = DbContext.tbl_DC_Module.Where(m => m.Module_ID == a).FirstOrDefault();
                md.Is_Active = false;
                md.Modified_Date = DateTime.Now;
                DbContext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Worksheet deleted successfully";
            return RedirectToAction("ViewWorksheet", "Admin");
        }
        public ActionResult DeleteWorksheet(int? id, string returnurl)
        {
            try
            {
                if (id != null)
                {
                    tbl_DC_Module obj = DbContext.tbl_DC_Module.Where(x => x.Module_ID == id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Is_Active = false;
                        obj.Is_Deleted = true;
                        obj.Modified_By = HttpContext.User.Identity.Name;
                        obj.Modified_Date = today;
                        DbContext.Entry(obj).State = EntityState.Modified;
                        DbContext.SaveChanges();

                        TempData["SuccessMessage"] = "Worksheet deleted successfully.";
                    }
                    return Redirect(returnurl);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View("ViewWorksheet");
        }
        #endregion

        #region ...........Study Note Upload..............
        [HttpGet]
        public ActionResult ViewStudyNotes(int? board, int? cls, int? subject, int? chapter)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Study Notes";
            var res = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                       join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on a.Board_Id equals b.Board_Id

                       join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on b.Class_Id equals c.Class_Id
                       join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on c.Subject_Id equals d.Subject_Id
                       join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on d.Chapter_Id equals f.Chapter_Id
                       where f.Module_Content != null
                       select new DigiChampsModel.DigiChampsModuleModel
                       {
                           Module_ID = f.Module_ID,
                           Module_Name = f.Module_Name,
                           Chapter_Id = f.Chapter_Id,
                           Chapter = d.Chapter,
                           Subject_Id = d.Subject_Id,
                           Subject = c.Subject,
                           Board_Id = b.Board_Id,
                           Board_Name = a.Board_Name,
                           Class_Id = c.Class_Id,
                           Class_Name = b.Class_Name,
                           Module_Desc = f.Module_Desc,
                           Module_Content = f.Module_Content,
                           Module_Content_Name = f.Module_Content_Name,
                           Module_video = f.Module_video,
                           Is_Free = f.Is_Free,
                           Question_PDF_Name = f.Question_PDF_Name,
                           Upload_PDF = f.Module_Content_Name,
                           Video_Type = f.Video_Type,
                           Class_PPT = f.Class_PPT,
                           Class_PPT_Name = f.Class_PPT_Name,
                           NCERT_PDF = f.NCERT_PDF,
                           NCERT_PDF_Name = f.NCERT_PDF_Name
                       }).OrderByDescending(x => x.Module_ID).ToList();
            ViewBag.board = 0;
            ViewBag.cls = 0;
            ViewBag.subject = 0;
            ViewBag.chapter = 0;
            if (board != null)
            {
                res = res.Where(a => a.Board_Id == board).ToList();
                ViewBag.board = board;
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_Id == cls).ToList();
                ViewBag.cls = cls;
            }
            if (subject != null)
            {
                res = res.Where(a => a.Subject_Id == subject).ToList();
                ViewBag.subject = subject;
            }
            if (chapter != null)
            {
                res = res.Where(a => a.Chapter_Id == chapter).ToList();
                ViewBag.chapter = chapter;
            }
            // ViewBag.moduledata = res;
            return View(res);
        }
        [HttpGet]
        public ActionResult CreateOrEditStudyNotes(string id, string returnurl)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id1 = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id1 && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Worksheet";
            ViewBag.returnurl = returnurl;

            ViewBag.upload_data = "";//otp_data;
            ViewBag.board = new SelectList(DbContext.tbl_DC_Board.Where(b => b.Is_Active == true && b.Is_Deleted == false), "Board_Id", "Board_Name");
            if (id != "" && id != null)
            {
                UInt32 moduleid;
                if (UInt32.TryParse(id, out moduleid) == true)
                {
                    int mid = Convert.ToInt32(moduleid);
                    DigiChampsModel.DigiChampsModuleModel obj = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                 join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on a.Board_Id equals b.Board_Id
                                                                 join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on b.Class_Id equals c.Class_Id
                                                                 join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on c.Subject_Id equals d.Subject_Id
                                                                 join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on d.Chapter_Id equals f.Chapter_Id
                                                                 select new DigiChampsModel.DigiChampsModuleModel
                                                                 {
                                                                     Board_Id = a.Board_Id,
                                                                     Class_Id = b.Class_Id,
                                                                     Subject_Id = c.Subject_Id,
                                                                     Chapter_Id = d.Chapter_Id,
                                                                     Module_ID = f.Module_ID,
                                                                     Module_Name = f.Module_Name,
                                                                     Module_Desc = f.Module_Desc,
                                                                     Module_Content = f.Module_Content,
                                                                     Module_video = f.Module_video,
                                                                     Is_Free = f.Is_Free,
                                                                     Validity = f.Validity,
                                                                     Question_PDF = f.Question_PDF,
                                                                     Is_Free_Test = f.Is_Free_Test,
                                                                     No_Question = f.No_Of_Question,
                                                                     Upload_PDF = f.Module_Content_Name,
                                                                     Question_PDF_Name = f.Question_PDF_Name,
                                                                     Video_Type = f.Video_Type,
                                                                     NCERT_PDF = f.NCERT_PDF,
                                                                     NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                                     Class_PPT = f.Class_PPT,
                                                                     Class_PPT_Name = f.Class_PPT_Name,
                                                                     videoid = f.Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + f.Module_video) : f.Module_video
                                                                 }).Where(x => x.Module_ID == mid).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.classid = obj.Class_Id;
                        ViewBag.subid = obj.Subject_Id;
                        ViewBag.chapterid = obj.Chapter_Id;
                        ViewBag.Modelvideo = obj.Module_video;
                        ViewBag.modelcontent = obj.Module_Content;
                        ViewBag.qstnpdf = obj.Question_PDF;
                        ViewBag.ncertpdf = obj.NCERT_PDF;
                        ViewBag.classppt = obj.Class_PPT;
                        return View(obj);
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Invalid module details.";
                    }
                }
                else
                {
                    TempData["WarningMessage"] = "Invalid module details.";
                }
            }

            return View();
        }
        [HttpPost]
        public ActionResult SaveOrUpdateStudyNotes(DigiChampsModel.DigiChampsModuleModel obj,
            HttpPostedFileBase module_pdf,
            string returnurl)
        {
            //Menupermission();
            ViewBag.Breadcrumb = "Study Notes";
            string msg = string.Empty;
            try
            {
                ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
                if (ModelState.IsValid)
                {
                    //if (obj.Module_Name.Trim() != "")
                    //{
                    if (obj.Module_ID == null)
                    {
                        var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Module_Content != null && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (module != null)
                        {
                            TempData["WarningMessage"] = "Study Notes already exists.";
                        }
                        else
                        {
                            tbl_DC_Module obj1 = new tbl_DC_Module();
                            obj1.Module_Name = obj.Module_Name;
                            obj1.Module_Desc = obj.Module_Desc;
                            obj1.Board_Id = obj.Board_Id;
                            obj1.Class_Id = obj.Class_Id;
                            obj1.Subject_Id = obj.Subject_Id;
                            obj1.Chapter_Id = obj.Chapter_Id;
                            obj1.Is_Free = true;
                            if (obj1.Is_Free == true)
                            {
                                obj1.Validity = obj.Validity;
                            }
                            if (module_pdf != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(module_pdf.FileName.
                                    Replace(module_pdf.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Module/PDF/"), fileName);
                                module_pdf.SaveAs(path);
                                obj1.Module_Content = fileName;
                            }
                            string extramsg = "";
                            obj1.Module_Content_Name = obj.Upload_PDF;
                            //if (obj.Is_Free_Test == true)
                            //{
                            //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id ==
                            //        obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                            //    if (iftest_chap == null)
                            //    {
                            obj1.Is_Free_Test = true;
                            //    }
                            //    else
                            //    {
                            //        obj1.Is_Free_Test = false;
                            //        extramsg = " Each chapter can only contain single free test.";
                            //    }
                            //}
                            obj1.Inserted_By = HttpContext.User.Identity.Name;
                            obj1.Is_Active = true;
                            obj1.Is_Deleted = false;
                            DbContext.tbl_DC_Module.Add(obj1);
                            DbContext.SaveChanges();
                            try
                            {
                                var deciceid = DbContext.tbl_DC_Registration.
                                    Where(x => x.Device_id != null && !x.Device_id.Equals("null")
                                        && obj1.Class_Id == obj.Class_Id).Select(x => x.Device_id).ToList();
                                // var regid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Class_ID == obj.Class_Id && x.Regd_ID != null).Select(x => x.Regd_ID).ToList();
                                //var deciceid = DbContext.tbl_DC_Registration.Where
                                //    (x => x.Device_id == "ePEyVbqUrBA:APA91bGzpOkF_jPnlBKlQFydnOkcy52DY7Wl-6ks1izlpZDfd4Jq8RN5gQNUquScLd0LzIB91nVwG5NPXxlJazQRXvbTFZ_COIS4KgumbuIsYfN41n5ezij4UKNMGwk9rUfCfxQKWUtZ" ||
                                //     x.Device_id == "c6Ixravhr_Q:APA91bFoGlTItg7aEe2Sz6_80fv9Rjy7IhSDAsaYVOVnh75891RFcJ-dlUGK4V9Pfxbj0GeS_sf_lC3cE-l9C_wzlsGYV2VdrQzmKeGMfTS2dpo1Q_R2Tuv3hSotogJy9yuONkywMcw5"
                                //     ).Select(x => x.Device_id).ToList();

                                //string body = "Body";.Replace("{{video}}", obj1.Module_video)
                                string title = "New Video";
                                string body_header = "New Video#{{Module_ID}}# Upload New Video : Hello {{Name}},! You have New Video.";
                                string body = body_header.ToString().Replace("{{Name}}",
                                    obj1.Module_Name).Replace("{{image}}", obj1.Module_Image).Replace("{{Feed_ID}}", obj1.Module_ID.ToString());
                                body = "NVID#{{Module_ID}}#New Video#Don't miss the new video. Check it out";

                                List<List<string>> ids = splitList(deciceid);

                                for (int i = 0; i < ids.Count; i++)
                                {
                                    if (ids[i] != null)
                                    {
                                        var note = new PushNotiStatusVideo(title, body, obj1.Module_Image, ids[i]);
                                    }
                                }
                            }
                            catch (Exception)
                            {

                            }
                            msg = "1";      //"Module details saved successfully.";
                            TempData["SuccessMessage"] = "Worksheet details saved successfully." + extramsg;
                        }
                    }
                    else
                    {
                        var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Module_Content != null && x.Module_ID != obj.Module_ID && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (module != null)
                        {
                            TempData["WarningMessage"] = "Study Notes already exist.";
                        }
                        else
                        {
                            int mod_id = Convert.ToInt32(obj.Module_ID);
                            tbl_DC_Module obj1 = DbContext.tbl_DC_Module.Where(x => x.Module_ID == mod_id).FirstOrDefault();
                            obj1.Module_Name = obj.Module_Name;
                            obj1.Module_Desc = obj.Module_Desc;
                            obj1.Is_Free = true;
                            obj1.Board_Id = obj.Board_Id;
                            obj1.Class_Id = obj.Class_Id;
                            obj1.Subject_Id = obj.Subject_Id;
                            obj1.Chapter_Id = obj.Chapter_Id;

                            if (obj1.Is_Free == true)
                            {
                                obj1.Validity = obj.Validity;
                            }
                            if (module_pdf != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(module_pdf.FileName.
                                    Replace(module_pdf.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Module/PDF/"), fileName);
                                module_pdf.SaveAs(path);
                                obj1.Module_Content = fileName;
                            }
                            string extramsg = "";
                            obj1.Module_Content_Name = obj.Upload_PDF;
                            //if (obj.Is_Free_Test == true)
                            //{
                            //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                            //    if (iftest_chap == null)
                            //    {
                            obj1.Is_Free_Test = true;
                            //    }
                            //    else
                            //    {
                            //        obj1.Is_Free_Test = false;
                            //    }
                            //}

                            obj1.Inserted_By = HttpContext.User.Identity.Name;
                            obj1.Is_Active = true;
                            obj1.Is_Deleted = false;
                            obj1.Modified_Date = today;
                            obj1.Modified_By = HttpContext.User.Identity.Name;
                            DbContext.Entry(obj1).State = EntityState.Modified;
                            DbContext.SaveChanges();
                            msg = "11";         // "Module details updated successfully.";
                            TempData["SuccessMessage"] = "Worksheet details updated successfully";
                            TempData["SuccessMessage"] = "Worksheet details updated successfully";
                        }
                    }
                    //}
                    //else
                    //{
                    //    msg = "00";         // "Please enter module name.";
                    //}
                }
            }
            catch (Exception ex)
            {
                //msg = "Something went wrong.";
                throw ex;
            }
            //return Json(msg, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("ViewModule");
            return Redirect(returnurl);
        }
        [HttpPost]
        public ActionResult MultipleDeleteStudyNotes(int[] chkselect)
        {
            foreach (int a in chkselect)
            {
                tbl_DC_Module md = DbContext.tbl_DC_Module.Where(m => m.Module_ID == a).FirstOrDefault();
                md.Is_Active = false;
                md.Modified_Date = DateTime.Now;
                DbContext.SaveChanges();

            }
            TempData["SuccessMessage"] = "Study notes details deleted successfully";
            return RedirectToAction("ViewStudyNotes", "Admin");
        }
        public ActionResult DeleteStudyNotes(int? id, string returnurl)
        {
            try
            {
                if (id != null)
                {
                    tbl_DC_Module obj = DbContext.tbl_DC_Module.Where(x => x.Module_ID == id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Is_Active = false;
                        obj.Is_Deleted = true;
                        obj.Modified_By = HttpContext.User.Identity.Name;
                        obj.Modified_Date = today;
                        DbContext.Entry(obj).State = EntityState.Modified;
                        DbContext.SaveChanges();

                        TempData["SuccessMessage"] = "Worksheet deleted successfully.";
                    }
                    return Redirect(returnurl);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View("ViewStudyNotes");
        }
        #endregion

        #region ...........Teacher PPT Upload..............
        [HttpGet]
        public ActionResult ViewTeacherppt(int? board, int? cls, int? subject, int? chapter)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Teacher PPT";
            var res = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                       join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on a.Board_Id equals b.Board_Id

                       join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on b.Class_Id equals c.Class_Id
                       join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on c.Subject_Id equals d.Subject_Id
                       join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on d.Chapter_Id equals f.Chapter_Id
                       where f.Class_PPT != null
                       select new DigiChampsModel.DigiChampsModuleModel
                       {
                           Module_ID = f.Module_ID,
                           Module_Name = f.Module_Name,
                           Chapter_Id = f.Chapter_Id,
                           Chapter = d.Chapter,
                           Subject_Id = d.Subject_Id,
                           Subject = c.Subject,
                           Board_Id = b.Board_Id,
                           Board_Name = a.Board_Name,
                           Class_Id = c.Class_Id,
                           Class_Name = b.Class_Name,
                           Module_Desc = f.Module_Desc,
                           Module_Content = f.Module_Content,
                           Module_Content_Name = f.Module_Content_Name,
                           Module_video = f.Module_video,
                           Is_Free = f.Is_Free,
                           Question_PDF_Name = f.Question_PDF_Name,
                           Upload_PDF = f.Module_Content_Name,
                           Video_Type = f.Video_Type,
                           Class_PPT = f.Class_PPT,
                           Class_PPT_Name = f.Class_PPT_Name,
                           NCERT_PDF = f.NCERT_PDF,
                           NCERT_PDF_Name = f.NCERT_PDF_Name
                       }).OrderByDescending(x => x.Module_ID).ToList();
            ViewBag.board = 0;
            ViewBag.cls = 0;
            ViewBag.subject = 0;
            ViewBag.chapter = 0;
            if (board != null)
            {
                res = res.Where(a => a.Board_Id == board).ToList();
                ViewBag.board = board;
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_Id == cls).ToList();
                ViewBag.cls = cls;
            }
            if (subject != null)
            {
                res = res.Where(a => a.Subject_Id == subject).ToList();
                ViewBag.subject = subject;
            }
            if (chapter != null)
            {
                res = res.Where(a => a.Chapter_Id == chapter).ToList();
                ViewBag.chapter = chapter;
            }
            // ViewBag.moduledata = res;
            return View(res);
        }
        [HttpGet]
        public ActionResult CreateOrEditTeacherppt(string id, string returnurl)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id1 = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id1 && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Teacher PPT";
            ViewBag.returnurl = returnurl;

            ViewBag.upload_data = "";//otp_data;
            ViewBag.board = new SelectList(DbContext.tbl_DC_Board.Where(b => b.Is_Active == true && b.Is_Deleted == false), "Board_Id", "Board_Name");
            if (id != "" && id != null)
            {
                UInt32 moduleid;
                if (UInt32.TryParse(id, out moduleid) == true)
                {
                    int mid = Convert.ToInt32(moduleid);
                    DigiChampsModel.DigiChampsModuleModel obj = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                 join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on a.Board_Id equals b.Board_Id
                                                                 join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on b.Class_Id equals c.Class_Id
                                                                 join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on c.Subject_Id equals d.Subject_Id
                                                                 join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on d.Chapter_Id equals f.Chapter_Id
                                                                 select new DigiChampsModel.DigiChampsModuleModel
                                                                 {
                                                                     Board_Id = a.Board_Id,
                                                                     Class_Id = b.Class_Id,
                                                                     Subject_Id = c.Subject_Id,
                                                                     Chapter_Id = d.Chapter_Id,
                                                                     Module_ID = f.Module_ID,
                                                                     Module_Name = f.Module_Name,
                                                                     Module_Desc = f.Module_Desc,
                                                                     Module_Content = f.Module_Content,
                                                                     Module_video = f.Module_video,
                                                                     Is_Free = f.Is_Free,
                                                                     Validity = f.Validity,
                                                                     Question_PDF = f.Question_PDF,
                                                                     Is_Free_Test = f.Is_Free_Test,
                                                                     No_Question = f.No_Of_Question,
                                                                     Upload_PDF = f.Module_Content_Name,
                                                                     Question_PDF_Name = f.Question_PDF_Name,
                                                                     Video_Type = f.Video_Type,
                                                                     NCERT_PDF = f.NCERT_PDF,
                                                                     NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                                     Class_PPT = f.Class_PPT,
                                                                     Class_PPT_Name = f.Class_PPT_Name,
                                                                     videoid = f.Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + f.Module_video) : f.Module_video
                                                                 }).Where(x => x.Module_ID == mid).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.classid = obj.Class_Id;
                        ViewBag.subid = obj.Subject_Id;
                        ViewBag.chapterid = obj.Chapter_Id;
                        ViewBag.Modelvideo = obj.Module_video;
                        ViewBag.modelcontent = obj.Module_Content;
                        ViewBag.qstnpdf = obj.Question_PDF;
                        ViewBag.ncertpdf = obj.NCERT_PDF;
                        ViewBag.classppt = obj.Class_PPT;
                        return View(obj);
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Invalid module details.";
                    }
                }
                else
                {
                    TempData["WarningMessage"] = "Invalid module details.";
                }
            }

            return View();
        }
        [HttpPost]
        public ActionResult SaveOrUpdateTeacherppt(DigiChampsModel.DigiChampsModuleModel obj,
            HttpPostedFileBase Class_PPT,
            string returnurl)
        {
            //Menupermission();
            ViewBag.Breadcrumb = "Teacher PPT";
            string msg = string.Empty;
            try
            {
                ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
                if (ModelState.IsValid)
                {
                    //if (obj.Module_Name.Trim() != "")
                    //{
                    if (obj.Module_ID == null)
                    {
                        //var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Class_PPT != null && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        //if (module != null)
                        //{
                        //    TempData["WarningMessage"] = "Teacher PPT already exists.";
                        //}
                        //else
                        //{
                        tbl_DC_Module obj1 = new tbl_DC_Module();
                        obj1.Module_Name = obj.Module_Name;
                        obj1.Module_Desc = obj.Module_Desc;
                        obj1.Board_Id = obj.Board_Id;
                        obj1.Class_Id = obj.Class_Id;
                        obj1.Subject_Id = obj.Subject_Id;
                        obj1.Chapter_Id = obj.Chapter_Id;
                        obj1.Is_Free = true;
                        if (obj1.Is_Free == true)
                        {
                            obj1.Validity = obj.Validity;
                        }
                        if (Class_PPT != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(Class_PPT.FileName.
                                Replace(Class_PPT.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Module/PPT/"), fileName);
                            Class_PPT.SaveAs(path);
                            obj1.Class_PPT = fileName;
                        }
                        obj1.Class_PPT_Name = obj.Class_PPT_Name;
                        string extramsg = "";
                        //if (obj.Is_Free_Test == true)
                        //{
                        //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id ==
                        //        obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                        //    if (iftest_chap == null)
                        //    {
                        obj1.Is_Free_Test = true;
                        //    }
                        //    else
                        //    {
                        //        obj1.Is_Free_Test = false;
                        //        extramsg = " Each chapter can only contain single free test.";
                        //    }
                        //}
                        obj1.Inserted_By = HttpContext.User.Identity.Name;
                        obj1.Is_Active = true;
                        obj1.Is_Deleted = false;
                        DbContext.tbl_DC_Module.Add(obj1);
                        DbContext.SaveChanges();
                        try
                        {
                            var deciceid = DbContext.tbl_DC_Registration.
                                Where(x => x.Device_id != null && !x.Device_id.Equals("null")
                                    && obj1.Class_Id == obj.Class_Id).Select(x => x.Device_id).ToList();
                            // var regid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Class_ID == obj.Class_Id && x.Regd_ID != null).Select(x => x.Regd_ID).ToList();
                            //var deciceid = DbContext.tbl_DC_Registration.Where
                            //    (x => x.Device_id == "ePEyVbqUrBA:APA91bGzpOkF_jPnlBKlQFydnOkcy52DY7Wl-6ks1izlpZDfd4Jq8RN5gQNUquScLd0LzIB91nVwG5NPXxlJazQRXvbTFZ_COIS4KgumbuIsYfN41n5ezij4UKNMGwk9rUfCfxQKWUtZ" ||
                            //     x.Device_id == "c6Ixravhr_Q:APA91bFoGlTItg7aEe2Sz6_80fv9Rjy7IhSDAsaYVOVnh75891RFcJ-dlUGK4V9Pfxbj0GeS_sf_lC3cE-l9C_wzlsGYV2VdrQzmKeGMfTS2dpo1Q_R2Tuv3hSotogJy9yuONkywMcw5"
                            //     ).Select(x => x.Device_id).ToList();

                            //string body = "Body";.Replace("{{video}}", obj1.Module_video)
                            string title = "New Video";
                            string body_header = "New Video#{{Module_ID}}# Upload New Video : Hello {{Name}},! You have New Video.";
                            string body = body_header.ToString().Replace("{{Name}}",
                                obj1.Module_Name).Replace("{{image}}", obj1.Module_Image).Replace("{{Feed_ID}}", obj1.Module_ID.ToString());
                            body = "NVID#{{Module_ID}}#New Video#Don't miss the new video. Check it out";

                            List<List<string>> ids = splitList(deciceid);

                            for (int i = 0; i < ids.Count; i++)
                            {
                                if (ids[i] != null)
                                {
                                    var note = new PushNotiStatusVideo(title, body, obj1.Module_Image, ids[i]);
                                }
                            }
                        }
                        catch (Exception)
                        {

                        }
                        msg = "1";      //"Module details saved successfully.";
                        TempData["SuccessMessage"] = "Class PPT details saved successfully." + extramsg;
                        //}
                    }
                    else
                    {
                        //var module = DbContext.tbl_DC_Module.Where(x => x.Module_ID == obj.Module_ID && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        //if (module == null)
                        //{
                        //    TempData["WarningMessage"] = "Class PPT does not exist.";
                        //}
                        //else
                        //{
                        int mod_id = Convert.ToInt32(obj.Module_ID);
                        tbl_DC_Module obj1 = DbContext.tbl_DC_Module.Where(x => x.Module_ID == mod_id).FirstOrDefault();
                        obj1.Module_Name = obj.Module_Name;
                        obj1.Module_Desc = obj.Module_Desc;
                        obj1.Is_Free = true;
                        obj1.Board_Id = obj.Board_Id;
                        obj1.Class_Id = obj.Class_Id;
                        obj1.Subject_Id = obj.Subject_Id;
                        obj1.Chapter_Id = obj.Chapter_Id;

                        if (obj1.Is_Free == true)
                        {
                            obj1.Validity = obj.Validity;
                        }
                        if (Class_PPT != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(Class_PPT.FileName.
                                Replace(Class_PPT.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Module/PPT/"), fileName);
                            Class_PPT.SaveAs(path);
                            obj1.Class_PPT = fileName;
                        }
                        obj1.Class_PPT_Name = obj.Class_PPT_Name;
                        string extramsg = "";
                        //if (obj.Is_Free_Test == true)
                        //{
                        //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                        //    if (iftest_chap == null)
                        //    {
                        obj1.Is_Free_Test = true;
                        //    }
                        //    else
                        //    {
                        //        obj1.Is_Free_Test = false;
                        //    }
                        //}

                        obj1.Inserted_By = HttpContext.User.Identity.Name;
                        obj1.Is_Active = true;
                        obj1.Is_Deleted = false;
                        obj1.Modified_Date = today;
                        obj1.Modified_By = HttpContext.User.Identity.Name;
                        DbContext.Entry(obj1).State = EntityState.Modified;
                        DbContext.SaveChanges();
                        msg = "11";         // "Module details updated successfully.";
                        TempData["SuccessMessage"] = "Class PPT details updated successfully";
                        TempData["SuccessMessage"] = "Class PPT details updated successfully";
                        //}
                    }
                    //}
                    //else
                    //{
                    //    msg = "00";         // "Please enter module name.";
                    //}
                }
            }
            catch (Exception ex)
            {
                //msg = "Something went wrong.";
                throw ex;
            }
            //return Json(msg, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("ViewModule");
            return Redirect(returnurl);
        }
        [HttpPost]
        public ActionResult MultipleDeleteTeacherppt(int[] chkselect)
        {
            foreach (int a in chkselect)
            {
                tbl_DC_Module md = DbContext.tbl_DC_Module.Where(m => m.Module_ID == a).FirstOrDefault();
                md.Is_Active = false;
                md.Modified_Date = DateTime.Now;
                DbContext.SaveChanges();

            }
            TempData["SuccessMessage"] = "Class PPT details updated successfully";
            return RedirectToAction("ViewTeacherppt", "Admin");
        }
        public ActionResult DeleteTeacherppt(int? id, string returnurl)
        {
            try
            {
                if (id != null)
                {
                    tbl_DC_Module obj = DbContext.tbl_DC_Module.Where(x => x.Module_ID == id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Is_Active = false;
                        obj.Is_Deleted = true;
                        obj.Modified_By = HttpContext.User.Identity.Name;
                        obj.Modified_Date = today;
                        DbContext.Entry(obj).State = EntityState.Modified;
                        DbContext.SaveChanges();

                        TempData["SuccessMessage"] = "Class PPT deleted successfully.";
                    }
                    return Redirect(returnurl);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View("ViewTeacherppt");
        }
        #endregion

        #region ...........NCERT PDF Upload..............
        [HttpGet]
        public ActionResult ViewNCERTpdf(int? board, int? cls, int? subject, int? chapter)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "NCERT PDF";
            var res = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                       join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on a.Board_Id equals b.Board_Id

                       join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on b.Class_Id equals c.Class_Id
                       join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on c.Subject_Id equals d.Subject_Id
                       join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on d.Chapter_Id equals f.Chapter_Id
                       where f.NCERT_PDF != null
                       select new DigiChampsModel.DigiChampsModuleModel
                       {
                           Module_ID = f.Module_ID,
                           Module_Name = f.Module_Name,
                           Chapter_Id = f.Chapter_Id,
                           Chapter = d.Chapter,
                           Subject_Id = d.Subject_Id,
                           Subject = c.Subject,
                           Board_Id = b.Board_Id,
                           Board_Name = a.Board_Name,
                           Class_Id = c.Class_Id,
                           Class_Name = b.Class_Name,
                           Module_Desc = f.Module_Desc,
                           Module_Content = f.Module_Content,
                           Module_Content_Name = f.Module_Content_Name,
                           Module_video = f.Module_video,
                           Is_Free = f.Is_Free,
                           Question_PDF_Name = f.Question_PDF_Name,
                           Upload_PDF = f.Module_Content_Name,
                           Video_Type = f.Video_Type,
                           Class_PPT = f.Class_PPT,
                           Class_PPT_Name = f.Class_PPT_Name,
                           NCERT_PDF = f.NCERT_PDF,
                           NCERT_PDF_Name = f.NCERT_PDF_Name
                       }).OrderByDescending(x => x.Module_ID).ToList();
            ViewBag.board = 0;
            ViewBag.cls = 0;
            ViewBag.subject = 0;
            ViewBag.chapter = 0;
            if (board != null)
            {
                res = res.Where(a => a.Board_Id == board).ToList();
                ViewBag.board = board;
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_Id == cls).ToList();
                ViewBag.cls = cls;
            }
            if (subject != null)
            {
                res = res.Where(a => a.Subject_Id == subject).ToList();
                ViewBag.subject = subject;
            }
            if (chapter != null)
            {
                res = res.Where(a => a.Chapter_Id == chapter).ToList();
                ViewBag.chapter = chapter;
            }
            // ViewBag.moduledata = res;
            return View(res);
        }
        [HttpGet]
        public ActionResult CreateOrEditNCERTpdf(string id, string returnurl)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id1 = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id1 && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "NCERT PDF";
            ViewBag.returnurl = returnurl;

            ViewBag.upload_data = "";//otp_data;
            ViewBag.board = new SelectList(DbContext.tbl_DC_Board.Where(b => b.Is_Active == true && b.Is_Deleted == false), "Board_Id", "Board_Name");
            if (id != "" && id != null)
            {
                UInt32 moduleid;
                if (UInt32.TryParse(id, out moduleid) == true)
                {
                    int mid = Convert.ToInt32(moduleid);
                    DigiChampsModel.DigiChampsModuleModel obj = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                 join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on a.Board_Id equals b.Board_Id
                                                                 join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on b.Class_Id equals c.Class_Id
                                                                 join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on c.Subject_Id equals d.Subject_Id
                                                                 join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on d.Chapter_Id equals f.Chapter_Id
                                                                 select new DigiChampsModel.DigiChampsModuleModel
                                                                 {
                                                                     Board_Id = a.Board_Id,
                                                                     Class_Id = b.Class_Id,
                                                                     Subject_Id = c.Subject_Id,
                                                                     Chapter_Id = d.Chapter_Id,
                                                                     Module_ID = f.Module_ID,
                                                                     Module_Name = f.Module_Name,
                                                                     Module_Desc = f.Module_Desc,
                                                                     Module_Content = f.Module_Content,
                                                                     Module_video = f.Module_video,
                                                                     Is_Free = f.Is_Free,
                                                                     Validity = f.Validity,
                                                                     Question_PDF = f.Question_PDF,
                                                                     Is_Free_Test = f.Is_Free_Test,
                                                                     No_Question = f.No_Of_Question,
                                                                     Upload_PDF = f.Module_Content_Name,
                                                                     Question_PDF_Name = f.Question_PDF_Name,
                                                                     Video_Type = f.Video_Type,
                                                                     NCERT_PDF = f.NCERT_PDF,
                                                                     NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                                     Class_PPT = f.Class_PPT,
                                                                     Class_PPT_Name = f.Class_PPT_Name,
                                                                     videoid = f.Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + f.Module_video) : f.Module_video
                                                                 }).Where(x => x.Module_ID == mid).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.classid = obj.Class_Id;
                        ViewBag.subid = obj.Subject_Id;
                        ViewBag.chapterid = obj.Chapter_Id;
                        ViewBag.Modelvideo = obj.Module_video;
                        ViewBag.modelcontent = obj.Module_Content;
                        ViewBag.qstnpdf = obj.Question_PDF;
                        ViewBag.ncertpdf = obj.NCERT_PDF;
                        ViewBag.classppt = obj.Class_PPT;
                        return View(obj);
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Invalid module details.";
                    }
                }
                else
                {
                    TempData["WarningMessage"] = "Invalid module details.";
                }
            }

            return View();
        }
        [HttpPost]
        public ActionResult SaveOrUpdateNCERTpdf(DigiChampsModel.DigiChampsModuleModel obj,
            HttpPostedFileBase NCERT_PDF,
            string returnurl)
        {
            //Menupermission();
            ViewBag.Breadcrumb = "NCERT PDF";
            string msg = string.Empty;
            try
            {
                ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
                if (ModelState.IsValid)
                {
                    //if (obj.Module_Name.Trim() != "")
                    //{
                    if (obj.Module_ID == null)
                    {
                        var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.NCERT_PDF != null && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (module != null)
                        {
                            TempData["WarningMessage"] = "NCERT PDF already exists.";
                        }
                        else
                        {
                            tbl_DC_Module obj1 = new tbl_DC_Module();
                            obj1.Module_Name = obj.Module_Name;
                            obj1.Module_Desc = obj.Module_Desc;
                            obj1.Board_Id = obj.Board_Id;
                            obj1.Class_Id = obj.Class_Id;
                            obj1.Subject_Id = obj.Subject_Id;
                            obj1.Chapter_Id = obj.Chapter_Id;
                            obj1.Is_Free = true;
                            if (obj1.Is_Free == true)
                            {
                                obj1.Validity = obj.Validity;
                            }
                            if (NCERT_PDF != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(NCERT_PDF.FileName.
                                    Replace(NCERT_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Module/NCERT/"), fileName);
                                NCERT_PDF.SaveAs(path);
                                obj1.NCERT_PDF = fileName;
                            }
                            obj1.NCERT_PDF_Name = obj.NCERT_PDF_Name;
                            string extramsg = "";
                            //if (obj.Is_Free_Test == true)
                            //{
                            //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id ==
                            //        obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                            //    if (iftest_chap == null)
                            //    {
                            obj1.Is_Free_Test = true;
                            //    }
                            //    else
                            //    {
                            //        obj1.Is_Free_Test = false;
                            //        extramsg = " Each chapter can only contain single free test.";
                            //    }
                            //}
                            obj1.Inserted_By = HttpContext.User.Identity.Name;
                            obj1.Is_Active = true;
                            obj1.Is_Deleted = false;
                            DbContext.tbl_DC_Module.Add(obj1);
                            DbContext.SaveChanges();
                            try
                            {
                                var deciceid = DbContext.tbl_DC_Registration.
                                    Where(x => x.Device_id != null && !x.Device_id.Equals("null")
                                        && obj1.Class_Id == obj.Class_Id).Select(x => x.Device_id).ToList();
                                // var regid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Class_ID == obj.Class_Id && x.Regd_ID != null).Select(x => x.Regd_ID).ToList();
                                //var deciceid = DbContext.tbl_DC_Registration.Where
                                //    (x => x.Device_id == "ePEyVbqUrBA:APA91bGzpOkF_jPnlBKlQFydnOkcy52DY7Wl-6ks1izlpZDfd4Jq8RN5gQNUquScLd0LzIB91nVwG5NPXxlJazQRXvbTFZ_COIS4KgumbuIsYfN41n5ezij4UKNMGwk9rUfCfxQKWUtZ" ||
                                //     x.Device_id == "c6Ixravhr_Q:APA91bFoGlTItg7aEe2Sz6_80fv9Rjy7IhSDAsaYVOVnh75891RFcJ-dlUGK4V9Pfxbj0GeS_sf_lC3cE-l9C_wzlsGYV2VdrQzmKeGMfTS2dpo1Q_R2Tuv3hSotogJy9yuONkywMcw5"
                                //     ).Select(x => x.Device_id).ToList();

                                //string body = "Body";.Replace("{{video}}", obj1.Module_video)
                                string title = "New Video";
                                string body_header = "New Video#{{Module_ID}}# Upload New Video : Hello {{Name}},! You have New Video.";
                                string body = body_header.ToString().Replace("{{Name}}",
                                    obj1.Module_Name).Replace("{{image}}", obj1.Module_Image).Replace("{{Feed_ID}}", obj1.Module_ID.ToString());
                                body = "NVID#{{Module_ID}}#New Video#Don't miss the new video. Check it out";

                                List<List<string>> ids = splitList(deciceid);

                                for (int i = 0; i < ids.Count; i++)
                                {
                                    if (ids[i] != null)
                                    {
                                        var note = new PushNotiStatusVideo(title, body, obj1.Module_Image, ids[i]);
                                    }
                                }
                            }
                            catch (Exception)
                            {

                            }
                            msg = "1";      //"Module details saved successfully.";
                            TempData["SuccessMessage"] = "NCERT PDF details saved successfully." + extramsg;
                        }
                    }
                    else
                    {
                        var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.NCERT_PDF != null && x.Module_ID != obj.Module_ID && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        if (module != null)
                        {
                            TempData["WarningMessage"] = "NCERT PDF already exist.";
                        }
                        else
                        {
                            int mod_id = Convert.ToInt32(obj.Module_ID);
                            tbl_DC_Module obj1 = DbContext.tbl_DC_Module.Where(x => x.Module_ID == mod_id).FirstOrDefault();
                            obj1.Module_Name = obj.Module_Name;
                            obj1.Module_Desc = obj.Module_Desc;
                            obj1.Is_Free = true;
                            obj1.Board_Id = obj.Board_Id;
                            obj1.Class_Id = obj.Class_Id;
                            obj1.Subject_Id = obj.Subject_Id;
                            obj1.Chapter_Id = obj.Chapter_Id;

                            if (obj1.Is_Free == true)
                            {
                                obj1.Validity = obj.Validity;
                            }
                            if (NCERT_PDF != null)
                            {
                                string guid = Guid.NewGuid().ToString();
                                string path = string.Empty;
                                var fileName = Path.GetFileName(NCERT_PDF.FileName.
                                    Replace(NCERT_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                path = Path.Combine(Server.MapPath("~/Module/NCERT/"), fileName);
                                NCERT_PDF.SaveAs(path);
                                obj1.NCERT_PDF = fileName;
                            }
                            obj1.NCERT_PDF_Name = obj.NCERT_PDF_Name;
                            string extramsg = "";
                            //if (obj.Is_Free_Test == true)
                            //{
                            //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                            //    if (iftest_chap == null)
                            //    {
                            obj1.Is_Free_Test = true;
                            //    }
                            //    else
                            //    {
                            //        obj1.Is_Free_Test = false;
                            //    }
                            //}

                            obj1.Inserted_By = HttpContext.User.Identity.Name;
                            obj1.Is_Active = true;
                            obj1.Is_Deleted = false;
                            obj1.Modified_Date = today;
                            obj1.Modified_By = HttpContext.User.Identity.Name;
                            DbContext.Entry(obj1).State = EntityState.Modified;
                            DbContext.SaveChanges();
                            msg = "11";         // "Module details updated successfully.";
                            TempData["SuccessMessage"] = "NCERT PDF details updated successfully";
                            TempData["SuccessMessage"] = "NCERT PDF details updated successfully";
                        }
                    }
                    //}
                    //else
                    //{
                    //    msg = "00";         // "Please enter module name.";
                    //}
                }
            }
            catch (Exception ex)
            {
                //msg = "Something went wrong.";
                throw ex;
            }
            //return Json(msg, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("ViewModule");
            return Redirect(returnurl);
        }
        [HttpPost]
        public ActionResult MultipleDeleteNCERTpdf(int[] chkselect)
        {
            foreach (int a in chkselect)
            {
                tbl_DC_Module md = DbContext.tbl_DC_Module.Where(m => m.Module_ID == a).FirstOrDefault();
                md.Is_Active = false;
                md.Modified_Date = DateTime.Now;
                DbContext.SaveChanges();

            }
            TempData["SuccessMessage"] = "NCERT PDF details deleted successfully";
            return RedirectToAction("ViewNCERTpdf", "Admin");
        }
        public ActionResult DeleteNCERTpdf(int? id, string returnurl)
        {
            try
            {
                if (id != null)
                {
                    tbl_DC_Module obj = DbContext.tbl_DC_Module.Where(x => x.Module_ID == id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Is_Active = false;
                        obj.Is_Deleted = true;
                        obj.Modified_By = HttpContext.User.Identity.Name;
                        obj.Modified_Date = today;
                        DbContext.Entry(obj).State = EntityState.Modified;
                        DbContext.SaveChanges();

                        TempData["SuccessMessage"] = "NCERT PDF deleted successfully.";
                    }
                    return Redirect(returnurl);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View("ViewNCERTpdf");
        }
        #endregion

        #region ----------------------------------------- Video upload --------------------------------------------
        [HttpGet]
        public ActionResult ViewVideomodule(int? board, int? cls, int? subject, int? chapter)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Video";
            var res = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                       join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on a.Board_Id equals b.Board_Id

                       join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on b.Class_Id equals c.Class_Id
                       join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on c.Subject_Id equals d.Subject_Id
                       join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on d.Chapter_Id equals f.Chapter_Id
                       where f.Module_video != null
                       select new DigiChampsModel.DigiChampsModuleModel
                       {
                           Module_ID = f.Module_ID,
                           Module_Name = f.Module_Name,
                           Chapter_Id = f.Chapter_Id,
                           Chapter = d.Chapter,
                           Subject_Id = d.Subject_Id,
                           Subject = c.Subject,
                           Board_Id = b.Board_Id,
                           Board_Name = a.Board_Name,
                           Class_Id = c.Class_Id,
                           Class_Name = b.Class_Name,
                           Module_Desc = f.Module_Desc,
                           Module_Content = f.Module_Content,
                           Module_video = f.Module_video,
                           Is_Free = f.Is_Free,
                           Question_PDF_Name = f.Question_PDF_Name,
                           Upload_PDF = f.Module_Content_Name,
                           Video_Type = f.Video_Type,
                           Class_PPT = f.Class_PPT,
                           Class_PPT_Name = f.Class_PPT_Name,
                           NCERT_PDF = f.NCERT_PDF,
                           NCERT_PDF_Name = f.NCERT_PDF_Name
                       }).OrderByDescending(x => x.Module_ID).ToList();
            ViewBag.board = 0;
            ViewBag.cls = 0;
            ViewBag.subject = 0;
            ViewBag.chapter = 0;
            if (board != null)
            {
                res = res.Where(a => a.Board_Id == board).ToList();
                ViewBag.board = board;
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_Id == cls).ToList();
                ViewBag.cls = cls;
            }
            if (subject != null)
            {
                res = res.Where(a => a.Subject_Id == subject).ToList();
                ViewBag.subject = subject;
            }
            if (chapter != null)
            {
                res = res.Where(a => a.Chapter_Id == chapter).ToList();
                ViewBag.chapter = chapter;
            }
            //ViewBag.moduledata = res;
            return View(res);
        }
        [HttpGet]
        public ActionResult CreateVideomodule(string id, string returnurl)
        {
            //Menupermission();
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id1 = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id1 && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Video";
            ViewBag.returnurl = returnurl;


            //string new_video_title = "VIDEO_TITLE";          // This should be obtained from DB
            //string api_secret = "d87d725cc2f9c4d0ae4fbf956e797089bdbcac7929143d478e9c60bb0d98629c";
            //string uri = "https://api.vdocipher.com/v2/uploadPolicy/";
            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            //request.Method = "POST";
            //request.ContentType = "application/x-www-form-urlencoded";
            //using (StreamWriter writer = new StreamWriter(request.GetRequestStream(), Encoding.ASCII))
            //{
            //    writer.Write("clientSecretKey=" + api_secret + "&title=" + new_video_title);
            //}
            //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //dynamic otp_data;
            //using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            //{
            //    string json_otp = reader.ReadToEnd();
            //    otp_data = JObject.Parse(json_otp);
            //}
            ViewBag.upload_data = "";//otp_data;
            ViewBag.board = new SelectList(DbContext.tbl_DC_Board.Where(b => b.Is_Active == true && b.Is_Deleted == false), "Board_Id", "Board_Name");
            if (id != "" && id != null)
            {
                UInt32 moduleid;
                if (UInt32.TryParse(id, out moduleid) == true)
                {
                    int mid = Convert.ToInt32(moduleid);
                    DigiChampsModel.DigiChampsModuleModel obj = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                 join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on a.Board_Id equals b.Board_Id
                                                                 join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on b.Class_Id equals c.Class_Id
                                                                 join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on c.Subject_Id equals d.Subject_Id
                                                                 join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on d.Chapter_Id equals f.Chapter_Id
                                                                 select new DigiChampsModel.DigiChampsModuleModel
                                                                 {
                                                                     Board_Id = a.Board_Id,
                                                                     Class_Id = b.Class_Id,
                                                                     Subject_Id = c.Subject_Id,
                                                                     Chapter_Id = d.Chapter_Id,
                                                                     Module_ID = f.Module_ID,
                                                                     Module_Name = f.Module_Name,
                                                                     Module_Desc = f.Module_Desc,
                                                                     Module_Content = f.Module_Content,
                                                                     Module_video = f.Module_video,
                                                                     Is_Free = f.Is_Free,
                                                                     Validity = f.Validity,
                                                                     Question_PDF = f.Question_PDF,
                                                                     Is_Free_Test = f.Is_Free_Test,
                                                                     No_Question = f.No_Of_Question,
                                                                     Upload_PDF = f.Module_Content_Name,
                                                                     Question_PDF_Name = f.Question_PDF_Name,
                                                                     Video_Type = f.Video_Type,
                                                                     NCERT_PDF = f.NCERT_PDF,
                                                                     NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                                     Class_PPT = f.Class_PPT,
                                                                     Class_PPT_Name = f.Class_PPT_Name,
                                                                     videoid = f.Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + f.Module_video) : f.Module_video
                                                                 }).Where(x => x.Module_ID == mid).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.classid = obj.Class_Id;
                        ViewBag.subid = obj.Subject_Id;
                        ViewBag.chapterid = obj.Chapter_Id;
                        ViewBag.Modelvideo = obj.Module_video;
                        ViewBag.modelcontent = obj.Module_Content;
                        ViewBag.qstnpdf = obj.Question_PDF;
                        ViewBag.ncertpdf = obj.NCERT_PDF;
                        ViewBag.classppt = obj.Class_PPT;
                        return View(obj);
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Invalid module details.";
                    }
                }
                else
                {
                    TempData["WarningMessage"] = "Invalid module details.";
                }
            }

            return View();
        }

        // HttpPostedFileBase RegImage3,
        //string key, string XAmzCredential, 
        //   string XAmzAlgorithm, string XAmzDate, string Policy,
        //   string XAmzSignature, string success_action_status,
        //   string success_action_redirect, string url,
        [HttpPost]
        public ActionResult MultipleDeleteVideomodule(int[] chkselect)
        {
            foreach (int a in chkselect)
            {
                tbl_DC_Module md = DbContext.tbl_DC_Module.Where(m => m.Module_ID == a).FirstOrDefault();
                md.Is_Active = false;
                DbContext.SaveChanges();
            }
            TempData["SuccessMessage"] = "Video deleted successfully.";
            return RedirectToAction("ViewVideomodule", "Admin");
        }
        [HttpPost]
        public ActionResult CreateVideomodule(DigiChampsModel.DigiChampsModuleModel obj,
            string returnurl,
            string videoid)
        {

            //Menupermission();
            ViewBag.Breadcrumb = "Video";
            string msg = string.Empty;





            try
            {
                ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
                if (ModelState.IsValid)
                {
                    if (obj.Module_Name.Trim() != "")
                    {
                        if (obj.Module_ID == null)
                        {
                            var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Module_video != null && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                            if (module != null)
                            {
                                TempData["WarningMessage"] = "Module Video already exists.";
                            }
                            else
                            {
                                tbl_DC_Module obj1 = new tbl_DC_Module();
                                obj1.Module_Name = obj.Module_Name;
                                obj1.Module_Desc = obj.Module_Desc;
                                //if (RegImage3 != null)
                                //{
                                //    int filelength = RegImage3.ContentLength;
                                //    string guid = Guid.NewGuid().ToString();
                                //    string path = string.Empty;
                                //    var fileName = Path.GetFileName(RegImage3.FileName.Replace(RegImage3.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                //    path = Path.Combine(Server.MapPath("../Module/Video/"), fileName);
                                //    RegImage3.SaveAs(path);
                                //    Program pr = new Program();
                                //    string retun_msg = pr.Programs(path, RegImage3.InputStream, filelength);
                                //    XmlDocument doc = new XmlDocument();
                                //    doc.LoadXml(retun_msg);
                                //    var element = ((XmlElement)doc.GetElementsByTagName("video")[0]); //null
                                //    var video_key = element.GetAttribute("key"); //cannot get values
                                //    obj1.Module_video = video_key;
                                //    //Delete the file from local server
                                //    if (System.IO.File.Exists(Server.MapPath("../Module/Video/" + fileName)))
                                //    {
                                //        System.IO.File.Delete(Server.MapPath("../Module/Video/" + fileName));
                                //    }
                                //}
                                obj1.Video_Type = obj.Video_Type;
                                if (videoid != null)
                                {
                                    //    int filelength = RegImage3.ContentLength;
                                    //    string guid = Guid.NewGuid().ToString();
                                    //    string path = string.Empty;
                                    //    var fileName = Path.GetFileName(
                                    //        RegImage3.FileName.Replace(RegImage3.FileName.Split('.').FirstOrDefault().ToString(),
                                    //        guid.ToString()));
                                    //    Vdocipher vdcip = new Vdocipher();
                                    //    string response= vdcip.videoupload(url, key, XAmzCredential, XAmzAlgorithm, XAmzDate, Policy, XAmzSignature, success_action_status, success_action_redirect, RegImage3.InputStream, RegImage3.FileName);

                                    if (obj.Video_Type == "Y")
                                    {
                                        string imgLink = "http://img.youtube.com/vi/";
                                        string[] temp = obj.videoid.Split(new char[] { '=', '&' });
                                        string video__id = temp[0].ToString();
                                        if (video__id.Split('.')[0].ToString() == "https://youtu")
                                        {
                                            video__id = temp[0].Split('.')[1].ToString().Split('/')[1].ToString();
                                        }
                                        else
                                        {
                                            video__id = temp[1];
                                        }
                                        obj1.Module_Image = imgLink + video__id + "/2.jpg";
                                        obj1.Module_video = video__id;
                                    }
                                    else
                                    {
                                        obj1.Module_video = videoid;
                                    }
                                }

                                obj1.Board_Id = obj.Board_Id;
                                obj1.Class_Id = obj.Class_Id;
                                obj1.Subject_Id = obj.Subject_Id;
                                obj1.Chapter_Id = obj.Chapter_Id;
                                obj1.Is_Free = true;
                                if (obj1.Is_Free == true)
                                {
                                    obj1.Validity = obj.Validity;
                                }
                                //if (Question_PDF != null)
                                //{
                                //    string guid = Guid.NewGuid().ToString();
                                //    string path = string.Empty;
                                //    var fileName = Path.GetFileName(Question_PDF.FileName.Replace(Question_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                //    path = Path.Combine(Server.MapPath("~/Module/Question_PDF/"), fileName);
                                //    Question_PDF.SaveAs(path);
                                //    obj1.Question_PDF = fileName;
                                //    obj1.No_Of_Question = obj.No_Question;
                                //}
                                string extramsg = "";
                                //if (obj.Is_Free_Test == true)
                                //{
                                //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id ==
                                //        obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                                //    if (iftest_chap == null)
                                //    {
                                obj1.Is_Free_Test = true;
                                //    }
                                //    else
                                //    {
                                //        obj1.Is_Free_Test = false;
                                //        extramsg = " Each chapter can only contain single free test.";
                                //    }
                                //}
                                obj1.Inserted_By = HttpContext.User.Identity.Name;
                                obj1.Is_Active = true;
                                obj1.Is_Deleted = false;
                                DbContext.tbl_DC_Module.Add(obj1);
                                DbContext.SaveChanges();
                                try
                                {
                                    var deciceid = DbContext.tbl_DC_Registration.
                                        Where(x => x.Device_id != null && !x.Device_id.Equals("null")
                                            && obj1.Class_Id == obj.Class_Id).Select(x => x.Device_id).ToList();
                                    // var regid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Class_ID == obj.Class_Id && x.Regd_ID != null).Select(x => x.Regd_ID).ToList();
                                    //var deciceid = DbContext.tbl_DC_Registration.Where
                                    //    (x => x.Device_id == "ePEyVbqUrBA:APA91bGzpOkF_jPnlBKlQFydnOkcy52DY7Wl-6ks1izlpZDfd4Jq8RN5gQNUquScLd0LzIB91nVwG5NPXxlJazQRXvbTFZ_COIS4KgumbuIsYfN41n5ezij4UKNMGwk9rUfCfxQKWUtZ" ||
                                    //     x.Device_id == "c6Ixravhr_Q:APA91bFoGlTItg7aEe2Sz6_80fv9Rjy7IhSDAsaYVOVnh75891RFcJ-dlUGK4V9Pfxbj0GeS_sf_lC3cE-l9C_wzlsGYV2VdrQzmKeGMfTS2dpo1Q_R2Tuv3hSotogJy9yuONkywMcw5"
                                    //     ).Select(x => x.Device_id).ToList();

                                    //string body = "Body";.Replace("{{video}}", obj1.Module_video)
                                    string title = "New Video";
                                    string body_header = "New Video#{{Module_ID}}# Upload New Video : Hello {{Name}},! You have New Video.";
                                    string body = body_header.ToString().Replace("{{Name}}",
                                        obj1.Module_Name).Replace("{{image}}", obj1.Module_Image).Replace("{{Feed_ID}}", obj1.Module_ID.ToString());
                                    body = "NVID#{{Module_ID}}#New Video#Don't miss the new video. Check it out";

                                    List<List<string>> ids = splitList(deciceid);

                                    for (int i = 0; i < ids.Count; i++)
                                    {
                                        if (ids[i] != null)
                                        {
                                            var note = new PushNotiStatusVideo(title, body, obj1.Module_Image, ids[i]);
                                        }
                                    }
                                }
                                catch (Exception)
                                {

                                }
                                msg = "1";      //"Module details saved successfully.";
                                TempData["SuccessMessage"] = "Video details saved successfully." + extramsg;
                            }
                        }
                        else
                        {
                            var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Module_video != null && x.Module_ID != obj.Module_ID && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                            if (module != null)
                            {
                                TempData["WarningMessage"] = "Module video already exist.";
                            }
                            else
                            {
                                int mod_id = Convert.ToInt32(obj.Module_ID);
                                tbl_DC_Module obj1 = DbContext.tbl_DC_Module.Where(x => x.Module_ID == mod_id).FirstOrDefault();
                                obj1.Module_Name = obj.Module_Name;
                                obj1.Module_Desc = obj.Module_Desc;
                                obj1.Is_Free = obj.Is_Free;
                                //if (RegImage3 != null)
                                //{
                                //    int filelength = RegImage3.ContentLength;
                                //    string guid = Guid.NewGuid().ToString();
                                //    string path = string.Empty;
                                //    var fileName = Path.GetFileName(RegImage3.FileName.Replace(RegImage3.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                //    path = Path.Combine(Server.MapPath("~/Module/Video/"), fileName);
                                //    RegImage3.SaveAs(path);
                                //    Program pr = new Program();
                                //    string retun_msg = pr.Programs(path, RegImage3.InputStream, filelength);
                                //    XmlDocument doc = new XmlDocument();
                                //    doc.LoadXml(retun_msg);
                                //    var element = ((XmlElement)doc.GetElementsByTagName("video")[0]); //null
                                //    var video_key = element.GetAttribute("key"); //cannot get values
                                //    obj1.Module_video = video_key;
                                //    //Delete the file from local server
                                //    if (System.IO.File.Exists(path))
                                //    {
                                //        System.IO.File.Delete(path);
                                //    }
                                //}

                                // if (RegImage3 != null)
                                //{
                                //    int filelength = RegImage3.ContentLength;
                                //    string guid = Guid.NewGuid().ToString();
                                //    string path = string.Empty;
                                //    var fileName = Path.GetFileName(RegImage3.FileName.Replace(RegImage3.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                                //    Vdocipher vdcip = new Vdocipher();
                                //     //vdcip.videoupload(url, key, XAmzCredential, XAmzAlgorithm, XAmzDate, Policy, XAmzSignature, success_action_status, success_action_redirect, RegImage3.InputStream, RegImage3.FileName);

                                obj1.Video_Type = obj.Video_Type;
                                if (obj.Video_Type == "Y")
                                {
                                    string imgLink = "http://img.youtube.com/vi/";
                                    string[] temp = obj.videoid.Split(new char[] { '=', '&' });
                                    string video__id = temp[0].ToString();
                                    if (video__id.Split('.')[0].ToString() == "https://youtu")
                                    {
                                        video__id = temp[0].Split('.')[1].ToString().Split('/')[1].ToString();
                                    }
                                    else
                                    {
                                        video__id = temp[1];
                                    }
                                    obj1.Module_Image = imgLink + video__id + "/2.jpg";
                                    obj1.Module_video = video__id;
                                }
                                else
                                {
                                    obj1.Module_video = videoid;
                                }
                                if (obj.Video_Type == "V")
                                {
                                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://dev.vdocipher.com/api/videos?q=" + videoid);

                                    request.PreAuthenticate = true;
                                    request.Headers.Add("Authorization", "Apisecret d87d725cc2f9c4d0ae4fbf956e797089bdbcac7929143d478e9c60bb0d98629c");
                                    request.Accept = "application/json";
                                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                                    Console.WriteLine("Content length is {0}", response.ContentLength);
                                    Console.WriteLine("Content type is {0}", response.ContentType);

                                    // Get the stream associated with the response.
                                    Stream receiveStream = response.GetResponseStream();

                                    // Pipes the stream to a higher level stream reader with the required encoding format. 
                                    StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                                    //Console.WriteLine("Response stream received.");
                                    var resp = readStream.ReadToEnd();



                                    JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                                    DigiChamps.Controllers.AdminController.VideoData routes_list =
                                            json_serializer.Deserialize<DigiChamps.Controllers.AdminController.VideoData>(resp);
                                    // Console.WriteLine(readStream.ReadToEnd());
                                    response.Close();
                                    readStream.Close();
                                    if (routes_list != null && routes_list.rows != null && routes_list.rows.Count > 0)
                                    {
                                        obj1.Module_video = routes_list.rows[0].id;
                                        obj1.Module_Image = routes_list.rows[0].poster;

                                    }
                                    else
                                    {
                                        return null;
                                    }
                                }


                                //}

                                obj1.Board_Id = obj.Board_Id;
                                obj1.Class_Id = obj.Class_Id;
                                obj1.Subject_Id = obj.Subject_Id;
                                obj1.Chapter_Id = obj.Chapter_Id;

                                if (obj1.Is_Free == true)
                                {
                                    obj1.Validity = obj.Validity;
                                }

                                //if (obj.Is_Free_Test == true)
                                //{
                                //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                                //    if (iftest_chap == null)
                                //    {
                                obj1.Is_Free_Test = true;
                                //    }
                                //    else
                                //    {
                                //        obj1.Is_Free_Test = false;
                                //    }
                                //}

                                obj1.Inserted_By = HttpContext.User.Identity.Name;
                                obj1.Is_Active = true;
                                obj1.Is_Deleted = false;
                                obj1.Modified_Date = today;
                                DbContext.Entry(obj1).State = EntityState.Modified;
                                DbContext.SaveChanges();
                                msg = "11";         // "Module details updated successfully.";
                                TempData["SuccessMessage"] = "Video details updated successfully";
                            }
                        }
                    }
                    else
                    {
                        msg = "00";         // "Please enter module name.";
                    }
                }
            }
            catch (Exception ex)
            {
                //msg = "Something went wrong.";
                throw ex;
            }
            //return Json(msg, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("ViewModule");
            return Redirect(returnurl);
        }

        public ActionResult DeleteVideomodule(int? id, string returnurl)
        {
            try
            {
                if (id != null)
                {
                    tbl_DC_Module obj = DbContext.tbl_DC_Module.Where(x => x.Module_ID == id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Is_Active = false;
                        obj.Is_Deleted = true;
                        obj.Modified_By = HttpContext.User.Identity.Name;
                        obj.Modified_Date = today;
                        DbContext.Entry(obj).State = EntityState.Modified;
                        DbContext.SaveChanges();

                        BotR.API.BotRAPI ab = new BotR.API.BotRAPI("rhhqK8sD", "M8sVGyMY0tpIftRBJ2PPVuzz");
                        if (obj.Module_video != null)
                        {
                            NameValueCollection shw = new NameValueCollection(){
                                                                                    {"video_key",obj.Module_video}
                                                                               };
                            string xml1 = ab.Call("/videos/delete", shw);
                        }
                        TempData["SuccessMessage"] = "Video deleted successfully.";
                    }
                    return Redirect(returnurl);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View("ViewVideomodule");
        }
        #endregion
        #region------------------Question Bank---------------------

        public ActionResult QuestionBankList(int? board, int? cls, int? subject, int? chapter)
        {

            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Question Bank PDF";
            var res = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                       join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on a.Board_Id equals b.Board_Id

                       join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on b.Class_Id equals c.Class_Id
                       join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on c.Subject_Id equals d.Subject_Id
                       join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                           on d.Chapter_Id equals f.Chapter_Id
                       where f.Question_Bank_PDF != null
                       select new DigiChampsModel.DigiChampsModuleModel
                       {
                           Module_ID = f.Module_ID,
                           Module_Name = f.Module_Name,
                           Chapter_Id = f.Chapter_Id,
                           Chapter = d.Chapter,
                           Subject_Id = d.Subject_Id,
                           Subject = c.Subject,
                           Board_Id = b.Board_Id,
                           Board_Name = a.Board_Name,
                           Class_Id = c.Class_Id,
                           Class_Name = b.Class_Name,
                           Module_Desc = f.Module_Desc,
                           Module_Content = f.Module_Content,
                           Module_Content_Name = f.Module_Content_Name,
                           Module_video = f.Module_video,
                           Is_Free = f.Is_Free,
                           Question_PDF_Name = f.Question_PDF_Name,
                           Upload_PDF = f.Module_Content_Name,
                           Video_Type = f.Video_Type,
                           Class_PPT = f.Class_PPT,
                           Class_PPT_Name = f.Class_PPT_Name,
                           NCERT_PDF = f.NCERT_PDF,
                           NCERT_PDF_Name = f.NCERT_PDF_Name,
                           Question_Bank_PDF = f.Question_Bank_PDF,
                           Question_Bank_PDF_Name = f.Question_Bank_PDF_Name
                       }).OrderByDescending(x => x.Module_ID).ToList();
            ViewBag.board = 0;
            ViewBag.cls = 0;
            ViewBag.subject = 0;
            ViewBag.chapter = 0;
            if (board != null)
            {
                res = res.Where(a => a.Board_Id == board).ToList();
                ViewBag.board = board;
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_Id == cls).ToList();
                ViewBag.cls = cls;
            }
            if (subject != null)
            {
                res = res.Where(a => a.Subject_Id == subject).ToList();
                ViewBag.subject = subject;
            }
            if (chapter != null)
            {
                res = res.Where(a => a.Chapter_Id == chapter).ToList();
                ViewBag.chapter = chapter;
            }
            // ViewBag.moduledata = res;
            return View(res);

        }
        [HttpGet]
        public ActionResult CreateQuestionBank(string id, string returnurl)
        {
            if (Convert.ToString(Session["ROLE_CODE"]) == "S")
            {
                if (Session["Role_Id"] != null)
                {
                    int id1 = Convert.ToInt32(Session["Role_Id"]);
                    var p_chk = DbContext.tbl_DC_Menu_Permission.Where(x => x.Menu_Key_ID == 4 && x.ROLE_ID == id1 && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (p_chk == null)
                    {
                        return RedirectToAction("checkpermission");
                    }
                }
            }
            ViewBag.Breadcrumb = "Question Bank PDF";
            ViewBag.returnurl = returnurl;

            ViewBag.upload_data = "";//otp_data;
            ViewBag.board = new SelectList(DbContext.tbl_DC_Board.Where(b => b.Is_Active == true && b.Is_Deleted == false), "Board_Id", "Board_Name");
            if (id != "" && id != null)
            {
                UInt32 moduleid;
                if (UInt32.TryParse(id, out moduleid) == true)
                {
                    int mid = Convert.ToInt32(moduleid);
                    DigiChampsModel.DigiChampsModuleModel obj = (from a in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                 join b in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on a.Board_Id equals b.Board_Id
                                                                 join c in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on b.Class_Id equals c.Class_Id
                                                                 join d in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on c.Subject_Id equals d.Subject_Id
                                                                 join f in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                     on d.Chapter_Id equals f.Chapter_Id
                                                                 select new DigiChampsModel.DigiChampsModuleModel
                                                                 {
                                                                     Board_Id = a.Board_Id,
                                                                     Class_Id = b.Class_Id,
                                                                     Subject_Id = c.Subject_Id,
                                                                     Chapter_Id = d.Chapter_Id,
                                                                     Module_ID = f.Module_ID,
                                                                     Module_Name = f.Module_Name,
                                                                     Module_Desc = f.Module_Desc,
                                                                     Module_Content = f.Module_Content,
                                                                     Module_video = f.Module_video,
                                                                     Is_Free = f.Is_Free,
                                                                     Validity = f.Validity,
                                                                     Question_PDF = f.Question_PDF,
                                                                     Is_Free_Test = f.Is_Free_Test,
                                                                     No_Question = f.No_Of_Question,
                                                                     Upload_PDF = f.Module_Content_Name,
                                                                     Question_PDF_Name = f.Question_PDF_Name,
                                                                     Video_Type = f.Video_Type,
                                                                     NCERT_PDF = f.NCERT_PDF,
                                                                     NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                                     Class_PPT = f.Class_PPT,
                                                                     Class_PPT_Name = f.Class_PPT_Name,
                                                                     Question_Bank_PDF = f.Question_Bank_PDF,
                                                                     Question_Bank_PDF_Name = f.Question_Bank_PDF_Name,
                                                                     videoid = f.Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + f.Module_video) : f.Module_video
                                                                 }).Where(x => x.Module_ID == mid).FirstOrDefault();
                    if (obj != null)
                    {
                        ViewBag.classid = obj.Class_Id;
                        ViewBag.subid = obj.Subject_Id;
                        ViewBag.chapterid = obj.Chapter_Id;
                        ViewBag.Modelvideo = obj.Module_video;
                        ViewBag.modelcontent = obj.Module_Content;
                        ViewBag.qstnpdf = obj.Question_PDF;
                        ViewBag.ncertpdf = obj.NCERT_PDF;
                        ViewBag.questionbankpdf = obj.Question_Bank_PDF;
                        ViewBag.classppt = obj.Class_PPT;
                        return View(obj);
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Invalid module details.";
                    }
                }
                else
                {
                    TempData["WarningMessage"] = "Invalid module details.";
                }
            }

            return View();
        }
        [HttpPost]
        public ActionResult SaveQuestionBank(DigiChampsModel.DigiChampsModuleModel obj,
            HttpPostedFileBase Question_Bank_PDF,
            string returnurl)
        {
            ViewBag.Breadcrumb = "Question Bank PDF";
            string msg = string.Empty;
            try
            {
                ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
                if (ModelState.IsValid)
                {
                    //if (obj.Module_Name.Trim() != "")
                    //{
                    if (obj.Module_ID == null)
                    {
                        //var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Question_Bank_PDF != null && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        //if (module != null)
                        //{
                        //    TempData["WarningMessage"] = "Question Bank PDF already exists.";
                        //}
                        //else
                        //{
                        tbl_DC_Module obj1 = new tbl_DC_Module();
                        obj1.Module_Name = obj.Module_Name;
                        obj1.Module_Desc = obj.Module_Desc;
                        obj1.Board_Id = obj.Board_Id;
                        obj1.Class_Id = obj.Class_Id;
                        obj1.Subject_Id = obj.Subject_Id;
                        obj1.Chapter_Id = obj.Chapter_Id;
                        obj1.Is_Free = true;
                        if (obj1.Is_Free == true)
                        {
                            obj1.Validity = obj.Validity;
                        }
                        if (Question_Bank_PDF != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(Question_Bank_PDF.FileName.
                                Replace(Question_Bank_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Module/Questionbank/"), fileName);
                            Question_Bank_PDF.SaveAs(path);
                            obj1.Question_Bank_PDF = fileName;
                        }
                        obj1.Question_Bank_PDF_Name = obj.Question_Bank_PDF_Name;
                        string extramsg = "";
                        //if (obj.Is_Free_Test == true)
                        //{
                        //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id ==
                        //        obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                        //    if (iftest_chap == null)
                        //    {
                        obj1.Is_Free_Test = true;
                        //    }
                        //    else
                        //    {
                        //        obj1.Is_Free_Test = false;
                        //        extramsg = " Each chapter can only contain single free test.";
                        //    }
                        //}
                        obj1.Inserted_By = HttpContext.User.Identity.Name;
                        obj1.Is_Active = true;
                        obj1.Is_Deleted = false;
                        DbContext.tbl_DC_Module.Add(obj1);
                        DbContext.SaveChanges();
                        try
                        {
                            var deciceid = DbContext.tbl_DC_Registration.
                                Where(x => x.Device_id != null && !x.Device_id.Equals("null")
                                    && obj1.Class_Id == obj.Class_Id).Select(x => x.Device_id).ToList();
                            // var regid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Class_ID == obj.Class_Id && x.Regd_ID != null).Select(x => x.Regd_ID).ToList();
                            //var deciceid = DbContext.tbl_DC_Registration.Where
                            //    (x => x.Device_id == "ePEyVbqUrBA:APA91bGzpOkF_jPnlBKlQFydnOkcy52DY7Wl-6ks1izlpZDfd4Jq8RN5gQNUquScLd0LzIB91nVwG5NPXxlJazQRXvbTFZ_COIS4KgumbuIsYfN41n5ezij4UKNMGwk9rUfCfxQKWUtZ" ||
                            //     x.Device_id == "c6Ixravhr_Q:APA91bFoGlTItg7aEe2Sz6_80fv9Rjy7IhSDAsaYVOVnh75891RFcJ-dlUGK4V9Pfxbj0GeS_sf_lC3cE-l9C_wzlsGYV2VdrQzmKeGMfTS2dpo1Q_R2Tuv3hSotogJy9yuONkywMcw5"
                            //     ).Select(x => x.Device_id).ToList();

                            //string body = "Body";.Replace("{{video}}", obj1.Module_video)
                            string title = "New Question Bank";
                            string body_header = "New Question Bank#{{Module_ID}}# Upload New Question Bank : Hello {{Name}},! You have New Question Bank.";
                            string body = body_header.ToString().Replace("{{Name}}",
                                obj1.Module_Name).Replace("{{image}}", obj1.Module_Image).Replace("{{Feed_ID}}", obj1.Module_ID.ToString());
                            body = "NVID#{{Module_ID}}#New Question Bank#Don't miss the new Question Bank. Check it out";

                            List<List<string>> ids = splitList(deciceid);

                            for (int i = 0; i < ids.Count; i++)
                            {
                                if (ids[i] != null)
                                {
                                    var note = new PushNotiStatusVideo(title, body, obj1.Module_Image, ids[i]);
                                }
                            }
                        }
                        catch (Exception)
                        {

                        }
                        msg = "1";      //"Module details saved successfully.";
                        TempData["SuccessMessage"] = "Question Bank details saved successfully." + extramsg;
                        //}
                    }
                    else
                    {
                        //var module = DbContext.tbl_DC_Module.Where(x => x.Class_Id == obj.Class_Id && x.Board_Id == obj.Board_Id && x.Subject_Id == obj.Subject_Id && x.Chapter_Id == obj.Chapter_Id && x.Question_Bank_PDF != null && x.Module_ID != obj.Module_ID && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        //if (module != null)
                        //{
                        //    TempData["WarningMessage"] = "Question Bank PDF already exist.";
                        //}
                        //else
                        //{
                        int mod_id = Convert.ToInt32(obj.Module_ID);
                        tbl_DC_Module obj1 = DbContext.tbl_DC_Module.Where(x => x.Module_ID == mod_id).FirstOrDefault();
                        obj1.Module_Name = obj.Module_Name;
                        obj1.Module_Desc = obj.Module_Desc;
                        obj1.Is_Free = true;
                        obj1.Board_Id = obj.Board_Id;
                        obj1.Class_Id = obj.Class_Id;
                        obj1.Subject_Id = obj.Subject_Id;
                        obj1.Chapter_Id = obj.Chapter_Id;

                        if (obj1.Is_Free == true)
                        {
                            obj1.Validity = obj.Validity;
                        }
                        if (Question_Bank_PDF != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(Question_Bank_PDF.FileName.
                                Replace(Question_Bank_PDF.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Module/Questionbank/"), fileName);
                            Question_Bank_PDF.SaveAs(path);
                            obj1.Question_Bank_PDF = fileName;
                        }
                        obj1.Question_Bank_PDF_Name = obj.Question_Bank_PDF_Name;
                        string extramsg = "";
                        //if (obj.Is_Free_Test == true)
                        //{
                        //    var iftest_chap = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == obj.Chapter_Id && x.Is_Free_Test == true).FirstOrDefault();
                        //    if (iftest_chap == null)
                        //    {
                        obj1.Is_Free_Test = true;
                        //    }
                        //    else
                        //    {
                        //        obj1.Is_Free_Test = false;
                        //    }
                        //}

                        obj1.Inserted_By = HttpContext.User.Identity.Name;
                        obj1.Is_Active = true;
                        obj1.Is_Deleted = false;
                        obj1.Modified_Date = today;
                        obj1.Modified_By = HttpContext.User.Identity.Name;
                        DbContext.Entry(obj1).State = EntityState.Modified;
                        DbContext.SaveChanges();
                        msg = "11";         // "Module details updated successfully.";
                        TempData["SuccessMessage"] = "Question Bank details updated successfully";
                        TempData["SuccessMessage"] = "Question Bank details updated successfully";
                        //}
                    }
                    //}
                    //else
                    //{
                    //    msg = "00";         // "Please enter module name.";
                    //}
                }
            }
            catch (Exception ex)
            {
                //msg = "Something went wrong.";
                throw ex;
            }
            //return Json(msg, JsonRequestBehavior.AllowGet);
            // return RedirectToAction("ViewModule");
            return Redirect(returnurl);
        }
        public ActionResult MultipleDeleteQuestionBank(int[] chkselect)
        {
            foreach (int a in chkselect)
            {
                tbl_DC_Module md = DbContext.tbl_DC_Module.Where(m => m.Module_ID == a).FirstOrDefault();
                md.Is_Active = false;
                md.Modified_Date = DateTime.Now;
                DbContext.SaveChanges();

            }
            TempData["SuccessMessage"] = "Question Bank details deleted successfully";
            return RedirectToAction("QuestionBankList", "Admin");
        }
        public ActionResult DeleteQuestionBank(int? id, string returnurl)
        {
            try
            {
                if (id != null)
                {
                    tbl_DC_Module obj = DbContext.tbl_DC_Module.Where(x => x.Module_ID == id).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Is_Active = false;
                        obj.Is_Deleted = true;
                        obj.Modified_By = HttpContext.User.Identity.Name;
                        obj.Modified_Date = today;
                        DbContext.Entry(obj).State = EntityState.Modified;
                        DbContext.SaveChanges();

                        TempData["SuccessMessage"] = "Question Bank deleted successfully.";
                    }
                    return Redirect(returnurl);
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View("QuestionBankList");

        }

        #endregion----------------End Question Bank----------------

        #endregion

        #region ------------------------Zoom Video Upload-------------------------------
        public class zoomvideoscls
        {
            public long ZoomId { get; set; }
            public Nullable<int> BoardId { get; set; }
            public Nullable<System.Guid> SchoolId { get; set; }
            public Nullable<int> ClassId { get; set; }
            public Nullable<System.Guid> SectionId { get; set; }
            public Nullable<int> SubjectId { get; set; }
            public Nullable<int> ChapterId { get; set; }

            public string BoardName { get; set; }
            public string SchoolName { get; set; }
            public string ClassName { get; set; }
            public string SectionName { get; set; }
            public string SubjectName { get; set; }
            public string ChapterName { get; set; }



            public Nullable<System.DateTime> InsertedOn { get; set; }
            public Nullable<System.DateTime> ModifiedOn { get; set; }
            public List<ZoomVideoLink> videoLinks { get; set; }
        }

        public ActionResult ZoomVideoUploadList(Guid? schoolid, int? boardid, int? classid, Guid? sec, int? Subjectid, int? Chapterid)
        {
            var zoomvideos = DbContext.ZoomVideos.Where(a => a.IsActive == true).ToList();
            var zoomvideolinks = DbContext.ZoomVideoLinks.Where(a => a.IsActive == true).ToList();
            var schoollist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            var classlist = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
            var sectionlist = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var subjectlist = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
            var chapterlist = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();

            var res = (from a in zoomvideos
                       join b in schoollist on a.SchoolId equals b.SchoolId
                       join c in boardlist on a.BoardId equals c.Board_Id
                       join d in classlist on a.ClassId equals d.Class_Id
                       join e in sectionlist on a.SectionId equals e.SectionId
                       join f in subjectlist on a.SubjectId equals f.Subject_Id
                       join g in chapterlist on a.ChapterId equals g.Chapter_Id
                       select new zoomvideoscls
                       {
                           BoardId = a.BoardId,
                           BoardName = c.Board_Name,
                           ChapterId = a.ChapterId,
                           ChapterName = g.Chapter,
                           ClassId = a.ClassId,
                           ClassName = d.Class_Name,
                           InsertedOn = a.InsertedOn,
                           ModifiedOn = a.ModifiedOn,
                           SchoolId = a.SchoolId,
                           SchoolName = b.SchoolName,
                           SectionId = a.SectionId,
                           SectionName = e.SectionName,
                           SubjectId = a.SubjectId,
                           SubjectName = f.Subject,
                           videoLinks = zoomvideolinks.Where(m => m.ZoomId == a.ZoomId).ToList(),
                           ZoomId = a.ZoomId
                       }).ToList();

            if (schoolid != null && schoolid != Guid.Empty)
            {
                res = res.Where(a => a.SchoolId == schoolid).ToList();
            }
            if (boardid != null && boardid != 0)
            {
                res = res.Where(a => a.BoardId == boardid).ToList();
            }
            if (classid != null && classid != 0)
            {
                res = res.Where(a => a.ClassId == classid).ToList();
            }
            if (sec != null && sec != Guid.Empty)
            {
                res = res.Where(a => a.SectionId == sec).ToList();
            }
            if (Subjectid != null && Subjectid != 0)
            {
                res = res.Where(a => a.SubjectId == Subjectid).ToList();
            }
            if (Chapterid != null && Chapterid != 0)
            {
                res = res.Where(a => a.ChapterId == Chapterid).ToList();
            }


            return View(res);
        }
        public ActionResult RemoveZoomVideoUpload(long id)
        {
            ZoomVideo zoomvideos = DbContext.ZoomVideos.Where(a => a.ZoomId == id).FirstOrDefault();
            zoomvideos.IsActive = false;
            zoomvideos.ModifiedOn = DateTime.Now;
            DbContext.SaveChanges();

            List<ZoomVideoLink> zoomvideolinks = DbContext.ZoomVideoLinks.Where(a => a.ZoomId == id).ToList();
            foreach (var a in zoomvideolinks)
            {
                a.IsActive = false;
                a.ModifiedOn = DateTime.Now;
                DbContext.SaveChanges();
            }
            return RedirectToAction("ZoomVideoUploadList");
        }
        public ActionResult CreateZoomVideoUpload()
        {
            Session["classvideos"] = null;
            ViewBag.schoollist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            ViewBag.boardlist = boardlist;
            var boardid = boardlist.FirstOrDefault().Board_Id;
            ViewBag.classlist = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Board_Id == boardid).ToList();
            //ViewBag.sectionlist = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            string SectionName = "A,B,C,D,E,F,G,RA,RB,RC,RD,N40M,N40B,D1,D2,D3,DB,P1,P2,P3,P4,COMM-A,COMM-B,FRUIT,FLOWER,R1,R2,R3,R4";
            var sections = SectionName.Split(',');
            sections = sections.Distinct().ToArray();
            List<string> sec = new List<string>();
            foreach (var a in sections)
            {
                sec.Add(a);
            }
            ViewBag.sectionlist = sec;

            return View();
        }
        public ActionResult EditZoomVideoUpload(long id)
        {
            Session["classvideos"] = null;


            var zoomvideos = DbContext.ZoomVideos.Where(a => a.ZoomId == id).FirstOrDefault();
            var zoomvideolinks = DbContext.ZoomVideoLinks.Where(a => a.IsActive == true && a.ZoomId == id).ToList();
            ViewBag.schoollist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            ViewBag.classlist = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Board_Id == zoomvideos.BoardId).ToList();
            ViewBag.sectionlist = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true && a.School_Id == zoomvideos.SchoolId && a.Class_Id == zoomvideos.ClassId).ToList();
            ViewBag.subjectlist = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Class_Id == zoomvideos.ClassId).ToList();
            ViewBag.chapterlist = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true && a.Subject_Id == zoomvideos.SubjectId).ToList();
            Session["classvideos"] = null;
            ViewBag.zoomdet = zoomvideos;
            List<ZoomVideoLink> am = new List<ZoomVideoLink>();
            foreach (var a in zoomvideolinks)
            {
                ZoomVideoLink link = new ZoomVideoLink();
                link.TopicName = a.TopicName;
                link.VideoLink = a.VideoLink;
                link.Password = a.Password;
                link.ZoomVideoId = a.ZoomVideoId;
                if (am == null)
                {
                    link.InsertedId = 1;
                }
                else
                {
                    link.InsertedId = am.Count() + 1;
                }
                link.IsActive = true;
                am.Add(link);
            }

            Session["classvideos"] = am;

            return View();
        }
        [HttpPost]
        public ActionResult UpdateVideoUpload(int id, int board, Guid school, int cls, Guid sec, int Subject, int chapter)
        {
            if (id != null)
            {
                // var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                //foreach (var a in school)
                //{
                //    foreach (var b in sec)
                //    {
                ZoomVideo zoom = DbContext.ZoomVideos.Where(a => a.ZoomId == id).FirstOrDefault();
                zoom.BoardId = board;
                zoom.ClassId = cls;
                zoom.ChapterId = chapter;
                zoom.IsActive = true;
                zoom.ModifiedId = 1;
                zoom.ModifiedOn = DateTime.Now;
                zoom.SchoolId = school;
                zoom.SectionId = sec;
                zoom.SubjectId = Subject;
                DbContext.SaveChanges();
                long zoomid = zoom.ZoomId;
                List<ZoomVideoLink> am = new List<ZoomVideoLink>();
                if (Session["classvideos"] != null)
                {
                    am = Session["classvideos"] as List<ZoomVideoLink>;
                }
                var am1 = am.Where(m => m.IsActive == true).ToList();
                if (am1 != null)
                {
                    foreach (var v in am1)
                    {
                        if (v.ZoomVideoId == 0)
                        {
                            ZoomVideoLink zoomVideo = new ZoomVideoLink();
                            zoomVideo.InsertedId = 1;
                            zoomVideo.ModifiedId = 1;
                            zoomVideo.InsertedOn = DateTime.Now;
                            zoomVideo.ModifiedOn = DateTime.Now;
                            zoomVideo.IsActive = true;
                            zoomVideo.Password = v.Password;
                            zoomVideo.TopicName = v.TopicName;
                            zoomVideo.VideoLink = v.VideoLink;
                            zoomVideo.ZoomId = zoomid;
                            DbContext.ZoomVideoLinks.Add(zoomVideo);
                            DbContext.SaveChanges();
                        }
                        else
                        {
                            ZoomVideoLink zoomVideo = DbContext.ZoomVideoLinks.Where(a => a.ZoomVideoId == v.ZoomVideoId).FirstOrDefault();

                            zoomVideo.ModifiedId = 1;
                            zoomVideo.ModifiedOn = DateTime.Now;
                            zoomVideo.IsActive = true;
                            zoomVideo.Password = v.Password;
                            zoomVideo.TopicName = v.TopicName;
                            zoomVideo.VideoLink = v.VideoLink;
                            DbContext.SaveChanges();
                        }
                    }
                    //    }
                    //}
                }
                var am2 = am.Where(a => a.IsActive == false && a.ZoomVideoId != 0).ToList();
                if (am2 != null)
                {
                    foreach (var v in am2)
                    {
                        ZoomVideoLink zoomVideo = DbContext.ZoomVideoLinks.Where(a => a.ZoomVideoId == v.ZoomVideoId).FirstOrDefault();

                        zoomVideo.ModifiedId = 1;
                        zoomVideo.ModifiedOn = DateTime.Now;
                        zoomVideo.IsActive = false;
                        DbContext.SaveChanges();
                    }
                }

            }
            return RedirectToAction("ZoomVideoUploadList");
        }


        [HttpPost]
        public ActionResult SaveUpdateVideoUpload(int? id, int board, Guid[] school, int cls, string[] sec, int Subject, int chapter)
        {
            if (id == null)
            {
                var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                foreach (var a in school)
                {
                    foreach (var b in sec)
                    {
                        ZoomVideo zoom = new ZoomVideo();
                        zoom.BoardId = board;
                        zoom.ClassId = cls;
                        zoom.ChapterId = chapter;
                        zoom.InsertedId = 1;
                        zoom.InsertedOn = DateTime.Now;
                        zoom.IsActive = true;
                        zoom.ModifiedId = 1;
                        zoom.ModifiedOn = DateTime.Now;
                        zoom.SchoolId = a;
                        zoom.SectionId = sections.Where(m => m.SectionName == b && m.School_Id == a && m.Class_Id == cls).FirstOrDefault().SectionId;
                        zoom.SubjectId = Subject;
                        DbContext.ZoomVideos.Add(zoom);
                        DbContext.SaveChanges();
                        long zoomid = zoom.ZoomId;
                        List<ZoomVideoLink> am = new List<ZoomVideoLink>();
                        if (Session["classvideos"] != null)
                        {
                            am = Session["classvideos"] as List<ZoomVideoLink>;
                        }
                        foreach (var v in am.Where(m => m.IsActive == true))
                        {
                            ZoomVideoLink zoomVideo = new ZoomVideoLink();
                            zoomVideo.InsertedId = 1;
                            zoomVideo.ModifiedId = 1;
                            zoomVideo.InsertedOn = DateTime.Now;
                            zoomVideo.ModifiedOn = DateTime.Now;
                            zoomVideo.IsActive = true;
                            zoomVideo.Password = v.Password;
                            zoomVideo.TopicName = v.TopicName;
                            zoomVideo.VideoLink = v.VideoLink;
                            zoomVideo.ZoomId = zoomid;
                            DbContext.ZoomVideoLinks.Add(zoomVideo);
                            DbContext.SaveChanges();
                        }
                    }
                }

            }
            return RedirectToAction("ZoomVideoUploadList");
        }
        public JsonResult SaveTempVideos(string topic, string videourl, string password)
        {
            List<ZoomVideoLink> am = new List<ZoomVideoLink>();
            if (Session["classvideos"] != null)
            {
                am = Session["classvideos"] as List<ZoomVideoLink>;
            }
            ZoomVideoLink link = new ZoomVideoLink();
            link.TopicName = topic;
            link.VideoLink = videourl;
            link.Password = password;
            link.ZoomVideoId = 0;
            if (am == null)
            {
                link.InsertedId = 1;
            }
            else
            {
                link.InsertedId = am.Count() + 1;
            }
            link.IsActive = true;
            am.Add(link);
            Session["classvideos"] = am;
            return Json(am.Where(a => a.IsActive == true).ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult RemoveTempVideos(int id)
        {
            List<ZoomVideoLink> am = new List<ZoomVideoLink>();
            if (Session["classvideos"] != null)
            {
                am = Session["classvideos"] as List<ZoomVideoLink>;
            }
            ZoomVideoLink link = am.Where(a => a.InsertedId == id).FirstOrDefault();

            link.IsActive = false;
            Session["classvideos"] = am;
            return Json(am.Where(a => a.IsActive == true).ToList(), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region-------------Online Exam-----------------


        #endregion

        #region--------------- OnlineObjectiveExam ------------------

        public ActionResult CreateNegativeMarks()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SaveNegativeMarks(decimal negativeMark)
        {
            try
            {
                Negativemark negativemark = new Negativemark();

                negativemark.NegativeMark1 = negativeMark;
                negativemark.Is_Active = true;
                negativemark.InsertedId = 1;
                negativemark.ModifiedId = 1;
                negativemark.InsertedOn = DateTime.Now;
                negativemark.ModifiedOn = DateTime.Now;

                DbContext.Negativemarks.Add(negativemark);
                DbContext.SaveChanges();

                return RedirectToAction("NegativeMarkList");
            }
            catch
            {
                return null;
            }
        }

        public ActionResult NegativeMarkList()
        {
            try
            {
                var negativeMarks = DbContext.Negativemarks.Where(a => a.Is_Active == true).ToList();

                return View(negativeMarks);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ActionResult DeleteNagetiveMark(int negativeMarkId)
        {
            try
            {
                var negativeMarks = DbContext.Negativemarks.Where(a => a.Is_Active == true && a.NegativeMarkID == negativeMarkId).FirstOrDefault();

                //Negativemark negativemark = new Negativemark();
                negativeMarks.Is_Active = false;

                DbContext.SaveChanges();
                return RedirectToAction("NegativeMarkList");
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region Exam Name
        public ActionResult CreateExam(long? id)
        {
            ViewBag.id = 0;
            ViewBag.name = "";
            if (id != null && id != 0)
            {
                tbl_ExamCategory examCategory = DbContext.tbl_ExamCategory.Where(a => a.ExamCategoryID == id && a.Is_Active == true).FirstOrDefault();
                ViewBag.id = examCategory.ExamCategoryID;
                ViewBag.name = examCategory.ExamCategoryName;
            }
            return View();
        }
        [HttpPost]
        public ActionResult SaveExam(long? id, string name)
        {
            try
            {
                tbl_ExamCategory examCategory = new tbl_ExamCategory();
                if (id == 0)
                {
                    examCategory = DbContext.tbl_ExamCategory.Where(a => a.ExamCategoryName == name && a.Is_Active == true).FirstOrDefault();

                    if (examCategory != null)
                    {
                        TempData["ErrorMessage"] = "Already Exists";
                        return RedirectToAction("CreateExam");
                    }

                }
                if (id != 0)
                {
                    examCategory = DbContext.tbl_ExamCategory.Where(a => a.ExamCategoryName == name && a.ExamCategoryID != id && a.Is_Active == true).FirstOrDefault();

                    if (examCategory != null)
                    {
                        TempData["ErrorMessage"] = "Already Exists";
                        return RedirectToAction("CreateExam");
                    }

                }
                examCategory = new tbl_ExamCategory();
                if (id != 0)
                {
                    examCategory = DbContext.tbl_ExamCategory.Where(a => a.ExamCategoryID == id && a.Is_Active == true).FirstOrDefault();
                }
                examCategory.ExamCategoryName = name;

                examCategory.Is_Active = true;
                if (id == 0)
                {
                    examCategory.InsertedId = 1;
                    examCategory.InsertedOn = DateTime.Now;
                }
                examCategory.ModifiedId = 1;
                examCategory.ModifiedOn = DateTime.Now;
                if (id == 0)
                {
                    DbContext.tbl_ExamCategory.Add(examCategory);
                }
                DbContext.SaveChanges();
                TempData["SuccessMessage"] = "Saved Successfully";
                return RedirectToAction("ExamList");
            }
            catch
            {
                return null;
            }
        }

        public ActionResult ExamList()
        {
            try
            {
                var examlist = DbContext.tbl_ExamCategory.Where(a => a.Is_Active == true).ToList();

                return View(examlist);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ActionResult DeleteExam(long Id)
        {
            try
            {
                var examcatg = DbContext.tbl_ExamCategory.Where(a => a.Is_Active == true && a.ExamCategoryID == Id).FirstOrDefault();

                //Negativemark negativemark = new Negativemark();
                examcatg.Is_Active = false;
                examcatg.ModifiedOn = DateTime.Now;
                DbContext.SaveChanges();
                return RedirectToAction("ExamList");
            }
            catch
            {
                return null;
            }
        }

        #endregion
        #region E-Report Upload
        public ActionResult GetRegisteredUsersListForEReport(Guid? school, int? board, int? cls, Guid? sec, long? catgid)
        {
            // Guid? school = new Guid(Session["id"].ToString());
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var classs = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
            var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var boards = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true).ToList();
            var examcatg = DbContext.tbl_ExamCategory.Where(a => a.Is_Active == true).ToList();
            ViewBag.examlist = examcatg;
            ViewBag.schoolist = schools;
            ViewBag.s = school;
            ViewBag.b = board;
            ViewBag.c = cls;
            ViewBag.se = sec;
            ViewBag.cg = catgid;
            var ereport = DbContext.tbl_EReportCard.Where(a => a.Is_Active == true).ToList();
            if (catgid != null && catgid != 0)
            {
                var res = (from a in students
                           join b in std_dtls on a.Regd_ID equals b.Regd_ID
                           // where a.SchoolId == school
                           //&& a.Regd_ID == 258
                           select new Schoolwise_User_Class
                           {
                               Regd_ID = a.Regd_ID,
                               Regd_No = a.Regd_No,
                               Customer_Name = a.Customer_Name,
                               SchoolId = (a.SchoolId == null && a.SchoolId == Guid.Empty) ? Guid.Empty : a.SchoolId,
                               Class_ID = b.Class_ID == null ? null : b.Class_ID,
                               SectionId = (a.SectionId == null && a.SectionId == Guid.Empty) ? Guid.Empty : a.SectionId,
                               Board_ID = b.Board_ID == null ? null : b.Board_ID,
                               boardname = b.Board_ID == null ? "" : boards.Where(c => c.Board_Id == b.Board_ID).FirstOrDefault().Board_Name,
                               schoolname = (a.SchoolId == null && a.SchoolId == Guid.Empty) ? "" : schools.Where(c => c.SchoolId == a.SchoolId).FirstOrDefault().SchoolName,
                               Class_Name = b.Class_ID == null ? "" : classs.Where(c => c.Class_Id == b.Class_ID).FirstOrDefault().Class_Name,
                               SectionName = ((a.SectionId == null && a.SectionId == Guid.Empty) ? "" : sections.Where(c => c.SectionId == a.SectionId).FirstOrDefault().SectionName),
                               Mobile = a.Mobile,
                               Email = a.Email,
                               EReportId = ereport == null ? 0 : (ereport.Where(m => m.Regd_Id == a.Regd_ID && m.ExamCategoryID == catgid && m.Is_Active == true).FirstOrDefault() == null ? 0 : ereport.Where(m => m.Regd_Id == a.Regd_ID && m.ExamCategoryID == catgid && m.Is_Active == true).FirstOrDefault().EReportCardID),
                               EReportPath = ereport == null ? null : (ereport.Where(m => m.Regd_Id == a.Regd_ID && m.ExamCategoryID == catgid && m.Is_Active == true).FirstOrDefault() == null ? null : ereport.Where(m => m.Regd_Id == a.Regd_ID && m.ExamCategoryID == catgid && m.Is_Active == true).FirstOrDefault().DocumentPath),
                               CategoryId = examcatg.Where(m => m.ExamCategoryID == catgid).FirstOrDefault().ExamCategoryID,
                               categoryName = examcatg.Where(m => m.ExamCategoryID == catgid).FirstOrDefault().ExamCategoryName,
                               School_No = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault().School_No : "") : "",
                               whatsapp_no = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault().Whatsapp_Mobile_No : "") : "",
                               zoom_mail_id = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_ID).FirstOrDefault().Zoom_Mail_ID : "") : "",
                           }).ToList();
                if (school != null && school != Guid.Empty)
                {
                    res = res.Where(a => a.SchoolId == school).ToList();
                }
                if (board != null)
                {
                    res = res.Where(a => a.Board_ID == board).ToList();
                }
                if (cls != null)
                {
                    res = res.Where(a => a.Class_ID == cls).ToList();
                }
                if (sec != null)
                {
                    res = res.Where(a => a.SectionId == sec).ToList();
                }
                Session["Get_EReport_User"] = res;
                var json = Json(res, JsonRequestBehavior.AllowGet);
                json.MaxJsonLength = int.MaxValue;
                return View(res);
            }
            else
            {
                return View();
            }
        }
        public class Encodings
        {
            public static string Base64EncodingMethod(string Data)
            {
                byte[] toEncodeAsBytes = System.Text.Encoding.UTF8.GetBytes(Data);
                string sReturnValues = System.Convert.ToBase64String(toEncodeAsBytes);
                return sReturnValues;
            }
            //Decoding
            public static string Base64DecodingMethod(string Data)
            {
                byte[] encodedDataAsBytes = System.Convert.FromBase64String(Data);
                string returnValue = System.Text.Encoding.UTF8.GetString(encodedDataAsBytes);
                return returnValue;
            }
        }
        [HttpPost]
        public ActionResult UploadEReportCardDetails(int? id, int? regdid, long? catgid, HttpPostedFileBase file)
        {
            tbl_EReportCard _EReportCard = new tbl_EReportCard();
            if (id == null)
            {
                _EReportCard = new tbl_EReportCard();
                _EReportCard.ExamCategoryID = catgid;
                _EReportCard.Regd_Id = regdid;
                _EReportCard.Is_Active = true;
                _EReportCard.InsertedId = 1;
                _EReportCard.InsertedOn = DateTime.Now;
                _EReportCard.ModifiedId = 1;
                _EReportCard.ModifiedOn = DateTime.Now;
                if (file != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    string path = string.Empty;
                    var fileName = Path.GetFileName(file.FileName.
                        Replace(file.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    path = Path.Combine(Server.MapPath("~/Images/EReportCardPDF/"), fileName);
                    file.SaveAs(path);
                    _EReportCard.DocumentPath = fileName;
                }
                DbContext.tbl_EReportCard.Add(_EReportCard);
                DbContext.SaveChanges();
            }
            else
            {
                _EReportCard = DbContext.tbl_EReportCard.Where(a => a.EReportCardID == id).FirstOrDefault();
                _EReportCard.ExamCategoryID = catgid;
                _EReportCard.Regd_Id = regdid;
                _EReportCard.Is_Active = true;
                _EReportCard.ModifiedId = 1;
                _EReportCard.ModifiedOn = DateTime.Now;
                if (file != null)
                {
                    string guid = Guid.NewGuid().ToString();
                    string path = string.Empty;
                    var fileName = Path.GetFileName(file.FileName.
                        Replace(file.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    path = Path.Combine(Server.MapPath("~/Images/EReportCardPDF/"), fileName);
                    file.SaveAs(path);
                    _EReportCard.DocumentPath = fileName;
                }
                DbContext.SaveChanges();
            }
            long ereportid = _EReportCard.EReportCardID;
            var docpath = _EReportCard.DocumentPath;
            var student = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Regd_ID == regdid).FirstOrDefault();
            var examtype = DbContext.tbl_ExamCategory.Where(a => a.Is_Active == true && a.ExamCategoryID == catgid).FirstOrDefault();

            //var empid = _EReportCard.EReportCardID;
            //string encode = Encodings.Base64EncodingMethod(empid.ToString());
            string url = "https://learn.odmps.org/Images/EReportCardPDF/" + _EReportCard.DocumentPath;
            //bitly b = new bitly();
            string shortlink = ToTinyURLS(url);
            if (student.Mobile != "")
            {
               var ss= Sendsmstoemployee(student.Mobile, "Dear " + student.Customer_Name + ", \nPlease find the E-Report Card of your exam " + examtype.ExamCategoryName + ". Click on the link and save the Report Card.- " + shortlink + " \n\n Regards\n ODM Educational Group");
            }

            if (student.Email != "")
            {
                string body = PopulateBody(student.Customer_Name, examtype.ExamCategoryName, shortlink);
                SendHtmlFormattedEmail(student.Email, "E-Report Card | ODM Public School", body);
            }

            var zoomemailid = DbContext.tbl_Regd_BasicDetl.Where(m => m.Is_Active == true && m.Regd_ID == regdid).FirstOrDefault().Zoom_Mail_ID;

            if (zoomemailid != " " && zoomemailid != null)
            {
                string body = PopulateBody(student.Customer_Name, examtype.ExamCategoryName, shortlink);
                SendHtmlFormattedEmail(zoomemailid, "E-Report Card | ODM Public School", body);
            }

            return RedirectToAction("GetRegisteredUsersListForEReport", new { catgid = catgid });

        }

        public ActionResult GetAllEReportDetails(Guid? school, int? board, int? cls, Guid? sec)
        {
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.schoolist = schools;
            ViewBag.s = school;
            ViewBag.b = board;
            ViewBag.c = cls;
            ViewBag.se = sec;
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var ereport = DbContext.tbl_EReportCard.Where(a => a.Is_Active == true).ToList();
            var examcatg = DbContext.tbl_ExamCategory.Where(a => a.Is_Active == true).ToList();
            var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true).ToList();
            var classs = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
            var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var boards = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            var res = (from a in ereport
                       join b in students on a.Regd_Id equals b.Regd_ID
                       join d in std_dtls on b.Regd_ID equals d.Regd_ID
                       select new Schoolwise_User_Class
                       {
                           SchoolId = (b.SchoolId == null && b.SchoolId == Guid.Empty) ? Guid.Empty : b.SchoolId,
                           Class_ID = d.Class_ID == null ? null : d.Class_ID,
                           SectionId = (b.SectionId == null && b.SectionId == Guid.Empty) ? Guid.Empty : b.SectionId,
                           Board_ID = d.Board_ID == null ? null : d.Board_ID,
                           boardname = d.Board_ID == null ? "" : boards.Where(c => c.Board_Id == d.Board_ID).FirstOrDefault().Board_Name,
                           schoolname = (b.SchoolId == null && b.SchoolId == Guid.Empty) ? "" : schools.Where(c => c.SchoolId == b.SchoolId).FirstOrDefault().SchoolName,
                           Class_Name = d.Class_ID == null ? "" : classs.Where(c => c.Class_Id == d.Class_ID).FirstOrDefault().Class_Name,
                           SectionName = ((b.SectionId == null && b.SectionId == Guid.Empty) ? "" : sections.Where(c => c.SectionId == b.SectionId).FirstOrDefault().SectionName),
                           EReportId = a.EReportCardID,
                           Email = b.Email,
                           Customer_Name = b.Customer_Name,
                           Mobile = b.Mobile,
                           EReportPath = "/Images/EReportCardPDF/" + a.DocumentPath,
                           Regd_ID = a.Regd_Id,
                           School_No = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_Id).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_Id).FirstOrDefault().School_No : "") : "",
                           whatsapp_no = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_Id).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_Id).FirstOrDefault().Whatsapp_Mobile_No : "") : "",
                           zoom_mail_id = stdbasicdtl != null ? (stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_Id).FirstOrDefault() != null ? stdbasicdtl.Where(m => m.Is_Active == true && m.Regd_ID == a.Regd_Id).FirstOrDefault().Zoom_Mail_ID : "") : "",

                       });
            if (school != null && school != Guid.Empty)
            {
                res = res.Where(a => a.SchoolId == school).ToList();
            }
            if (board != null)
            {
                res = res.Where(a => a.Board_ID == board).ToList();
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_ID == cls).ToList();
            }
            if (sec != null)
            {
                res = res.Where(a => a.SectionId == sec).ToList();
            }
            return View(res.ToList());

        }


        #region Mail and Messages

        public bool Sendsmstoemployee(string mobile, string message)
        {
            try
            {
                string baseurl = "https://api-alerts.kaleyra.com/v4/?api_key=A74f94c8d3c8dbc449fdca44d59e931e6&method=sms&message=" + message + "&to=" + mobile + "&sender=ODMEGR";
                
                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);

                HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                string responseString = respStreamReader.ReadToEnd();
                respStreamReader.Close();
                myResp.Close();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        private string PopulateBody(string student, string classname, string url)
        {
            string body = string.Empty;
            // string encode = Encodings.Base64EncodingMethod(id.ToString());
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/EReportTemplate.html")))
            {

                body = reader.ReadToEnd().Replace("{{Student_Name}}", student).Replace("{{Class}}", classname).Replace("{{href}}", url);

            }

            return body;
        }
        private string PopulateBodyResultSheet(string student, string classname, string url)
        {
            string body = string.Empty;
            // string encode = Encodings.Base64EncodingMethod(id.ToString());
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Templates/ResultSheetTemplate.html")))
            {

                body = reader.ReadToEnd().Replace("{{Student_Name}}", student).Replace("{{Class}}", classname).Replace("{{href}}", url);

            }

            return body;
        }


        private void SendHtmlFormattedEmail(string recepientEmail, string subject, string body)
        {
            try
            {
                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress("tracq@odmegroup.org");
                    mailMessage.Subject = subject;
                    mailMessage.Body = body;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.To.Add(new MailAddress(recepientEmail));
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.sendgrid.net";// ConfigurationManager.AppSettings["Host"];
                    smtp.EnableSsl = true;// Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    NetworkCred.UserName = "apikey";
                    NetworkCred.Password = "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";
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;// int.Parse(ConfigurationManager.AppSettings["Port"]);
                    smtp.Send(mailMessage);
                }

                //using (MailMessage mailMessage = new MailMessage())
                //{
                //    mailMessage.From = new MailAddress("tracq@odmegroup.org");
                //    mailMessage.Subject = subject;
                //    mailMessage.Body = body;
                //    mailMessage.IsBodyHtml = true;
                //    mailMessage.To.Add(new MailAddress(recepientEmail));
                //    SmtpClient smtp = new SmtpClient();
                //    smtp.Host = ConfigurationManager.AppSettings["smtpServer"];
                //    smtp.EnableSsl = true;
                //    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                //    NetworkCred.UserName = "tracq@odmegroup.org";
                //    NetworkCred.Password = "odm@1234";
                //    smtp.UseDefaultCredentials = true;
                //    smtp.Credentials = NetworkCred;
                //    smtp.Port = int.Parse(ConfigurationManager.AppSettings["smtpPort"]);

                //    smtp.Send(mailMessage);
                //}
            }
            catch { }
        }

        #endregion
        #region Bitly

        protected string ToTinyURLS(string txt)
        {
            Regex regx = new Regex("https://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);

            MatchCollection mactches = regx.Matches(txt);

            foreach (Match match in mactches)
            {
                string tURL = MakeTinyUrl(match.Value);
                txt = txt.Replace(match.Value, tURL);
            }

            return txt;
        }

        public static string MakeTinyUrl(string Url)
        {
            try
            {
                if (Url.Length <= 12)
                {
                    return Url;
                }
                if (!Url.ToLower().StartsWith("http") && !Url.ToLower().StartsWith("ftp"))
                {
                    Url = "http://" + Url;
                }
                var request = WebRequest.Create("http://tinyurl.com/api-create.php?url=" + Url);
                var res = request.GetResponse();
                string text;
                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }
                return text;
            }
            catch (Exception)
            {
                return Url;
            }
        }

        #endregion
        #endregion
        #region Upload Online Exam Report
        public class OnlineExamReportModel
        {
            public long StudentmarksheetId { get; set; }
            public Nullable<long> OnlineExamTypeId { get; set; }
            public Nullable<int> RegdId { get; set; }
            public string Session { get; set; }
            public string Batch { get; set; }
            public string SubjectCombination { get; set; }
            public string Total_Mark_Subjective { get; set; }
            public string Total_Mark_Objective { get; set; }
            public string Percentage_Subjective { get; set; }
            public string Percentage_Objective { get; set; }
            public Nullable<bool> Is_Active { get; set; }
            public Nullable<int> InsertedId { get; set; }
            public Nullable<int> ModifiedId { get; set; }
            public string InsertedOn { get; set; }
            public Nullable<System.DateTime> ModifiedOn { get; set; }
            public string Roll_No { get; set; }
            public string School_No { get; set; }
            public string StudentName { get; set; }
            public string ClassName { get; set; }
            public string section { get; set; }
            public Nullable<int> typeId { get; set; }
            public string OnlineExamTypeName { get; set; }
        }
        public ActionResult OnlineExamReportList(Guid? school, int? board, int? cls, Guid? sec, int? student, int? type, string session)
        {
            var examtype = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            ViewBag.examtype = examtype;
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.schoolist = schools;
            ViewBag.s = school;
            ViewBag.b = board;
            ViewBag.c = cls;
            ViewBag.se = sec;
            ViewBag.f = student;
            ViewBag.g = type;
            var Marksheet = DbContext.Tbl_Student_Marksheet.Where(a => a.Is_Active == true).ToList();

            var res = (from a in Marksheet
                       join b in examtype on a.OnlineExamTypeId equals b.OnlineExamTypeId
                       select new OnlineExamReportModel
                       {
                           StudentmarksheetId = a.StudentmarksheetId,
                           OnlineExamTypeId = a.OnlineExamTypeId,
                           OnlineExamTypeName = b.OnlineExamTypeName,
                           RegdId = a.RegdId,
                           Session = a.Session,
                           section = a.section,
                           Total_Mark_Objective = a.Total_Mark_Objective,
                           Total_Mark_Subjective = a.Total_Mark_Subjective,
                           Percentage_Subjective = a.Percentage_Subjective,
                           Percentage_Objective = a.Percentage_Objective,
                           Roll_No = a.Roll_No,
                           School_No = a.School_No,
                           StudentName = a.StudentName,
                           ClassName = a.ClassName,
                           typeId = a.typeId,
                           ModifiedOn = a.ModifiedOn,
                           InsertedOn = a.InsertedOn.Value.ToString("yyyy-MM-dd")
                       }
                      ).ToList();

            if (cls != null)
            {
                string classname = DbContext.tbl_DC_Class.Where(v => v.Class_Id == cls).FirstOrDefault().Class_Name;
                res = res.Where(a => a.ClassName == classname).ToList();
            }
            if (sec != null)
            {
                string sessionname = DbContext.tbl_DC_Class_Section.Where(v => v.SectionId == sec).FirstOrDefault().SectionName;
                res = res.Where(a => a.section == sessionname).ToList();
            }
            if (student != null)
            {
                res = res.Where(a => a.RegdId == student).ToList();
            }
            if (type != null)
            {
                res = res.Where(a => a.OnlineExamTypeId == type).ToList();
            }

            if (session != null && session != "All")
            {
                res = res.Where(a => a.Session == session).ToList();
            }
            Session["marksheetreport"] = res;
            return View(res);
        }
        public ActionResult UploadOnlineExamReport()
        {
            var OnlineExamTypessList = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            ViewBag.Examtype = OnlineExamTypessList;
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.schools = schools;


            return View();
        }
        [HttpPost]
        public async Task<ActionResult> UploadstudentmarksheetReport(int typ, int examtype, Guid school, int board, int cls, string session, string Objective, string Subjective, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);

                        XLWorkbook workbook = new XLWorkbook(path);


                        IXLWorksheet xLWorksheet = workbook.Worksheet(1);
                        DataTable dt = new DataTable();
                        var i = 0;
                        foreach (IXLRow row in xLWorksheet.Rows())
                        {
                            if (i == 1)
                            {
                                //foreach (IXLCell cell in row.Cells())
                                //{
                                int m = 1;
                                for (int a = 0; a < row.Cells().Count(); a++)
                                {
                                    //if (m <= 3)
                                    //{
                                    if (row.Cells().ToList()[a].Value.ToString() == "")
                                    {
                                        dt.Columns.Add(row.Cells().ToList()[a - m].Value.ToString().Trim() + m);
                                        m++;
                                    }
                                    else
                                    {
                                        dt.Columns.Add(row.Cells().ToList()[a].Value.ToString().Trim());
                                        m = 1;
                                    }
                                    //}
                                }
                                //}
                            }
                            else if (i > 1)
                            {
                                dt.Rows.Add();
                                int j = 0;
                                foreach (IXLCell cell in row.Cells())
                                {
                                    dt.Rows[dt.Rows.Count - 1][j] = cell.Value.ToString();
                                    j++;
                                }
                            }
                            i = i + 1;
                        }

                        var data = dt;
                        bool firstrow = true;
                        bool firstrow1 = true;
                        List<string> schnos = new List<string>();
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (firstrow1 == true)
                            {
                                firstrow1 = false;
                            }
                            else
                            {
                                
                                //string schoolno = dr["SCHOOL NO."].ToString();
                                //var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                //if (regdetlist == null)
                                //{
                                //    schnos.Add(schoolno);
                                //}
                            }
                        }

                        var result = schnos;

                        clsmarkslist clsmarks = new clsmarkslist();
                        List<marksheetmailcls> ss = new List<marksheetmailcls>();

                        foreach (DataRow dr in dt.Rows)
                        {
                            if (firstrow == true)
                            {
                                firstrow = false;
                            }
                            else
                            {
                                if (typ == 2)
                                {

                                    string schoolno = dr["SCHOOL NO."].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Marksheet _Marksheet = new Tbl_Student_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamTypeId = examtype;
                                        _Marksheet.Percentage_Objective = dr["% OF MARK1"].ToString();
                                        _Marksheet.Percentage_Subjective = dr["% OF MARK"].ToString();
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["ROLL NO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC"].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = regdlist.Customer_Name;
                                        _Marksheet.Total_Mark_Objective = dr["TOTAL\nMARK1"].ToString();
                                        _Marksheet.Total_Mark_Subjective = dr["TOTAL\nMARK"].ToString();
                                        _Marksheet.Total_Objective = Objective;
                                        _Marksheet.Total_Subjective= Subjective;
                                        DbContext.Tbl_Student_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 5; y <= (dt.Columns.Count - 5); y = y + 2)
                                        {
                                            Tbl_StudentMarksheetDet marksheetDet = new Tbl_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.ObjectiveScore = dr[dt.Columns[y].ColumnName + "1"].ToString();
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        //sajdgj
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.whatsappmobile = stdbasicdtl.Whatsapp_Mobile_No;

                                        marksheetmailcls.examtypename = examtypename;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }

                                }
                                else if (typ == 1)
                                {
                                    string schoolno = dr["SCL.\nNO."].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Marksheet _Marksheet = new Tbl_Student_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamTypeId = examtype;
                                        _Marksheet.Percentage_Objective = dr["TOTAL (%)1"].ToString();
                                        _Marksheet.Percentage_Subjective = dr["TOTAL (%)"].ToString();
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["R.\nNO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC"].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = regdlist.Customer_Name;
                                        _Marksheet.Total_Mark_Objective = dr["TOTAL1"].ToString();
                                        _Marksheet.Total_Mark_Subjective = dr["TOTAL"].ToString();
                                        _Marksheet.Total_Objective = Objective;
                                        _Marksheet.Total_Subjective = Subjective;
                                        DbContext.Tbl_Student_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 4; y <= (dt.Columns.Count - 5); y = y + 2)
                                        {
                                            Tbl_StudentMarksheetDet marksheetDet = new Tbl_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.ObjectiveScore = dr[dt.Columns[y].ColumnName + "1"].ToString();
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.whatsappmobile = stdbasicdtl.Whatsapp_Mobile_No;

                                        marksheetmailcls.examtypename = examtypename;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);
                                        //string url = "http://learn.odmps.org/Reports/Reports.aspx?type=School_Marksheet_Report&Id=" + markid;
                                        ////bitly b = new bitly();
                                        //string shortlink = ToTinyURLS(url);
                                        //if (regdlist.Mobile != "")
                                        //{
                                        //    Sendsmstoemployee(regdlist.Mobile, "Dear " + regdlist.Customer_Name + ", \nPlease find the Online Test Result of your exam " + examtypename + ". Click on the link and save the Test Result.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                                        //}

                                        //if (regdlist.Email != "")
                                        //{
                                        //    string body = PopulateBodyResultSheet(regdlist.Customer_Name, examtypename, shortlink);
                                        //    SendHtmlFormattedEmail(regdlist.Email, "Online Test Result | ODM Public School", body);
                                        //}
                                        //if (stdbasicdtl.Zoom_Mail_ID != "" && stdbasicdtl.Zoom_Mail_ID != null)
                                        //{
                                        //    string body = PopulateBodyResultSheet(regdlist.Customer_Name, examtypename, shortlink);
                                        //    SendHtmlFormattedEmail(stdbasicdtl.Zoom_Mail_ID, "Online Test Result | ODM Public School", body);
                                        //}
                                    }
                                }
                                else if (typ == 3)
                                {
                                    string schoolno = dr["ScL.\nNo."].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Marksheet _Marksheet = new Tbl_Student_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamTypeId = examtype;
                                        _Marksheet.Percentage_Objective = dr["TOT OBJ     %"].ToString();
                                        _Marksheet.Percentage_Subjective = dr["TOT SUB    %"].ToString();
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["SL.\nNO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC."].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = regdlist.Customer_Name;
                                        _Marksheet.Total_Mark_Objective = dr["TOT OBJ (500)"].ToString();
                                        _Marksheet.Total_Mark_Subjective = dr["TOT SUB (500)"].ToString();
                                        _Marksheet.Total_Objective = Objective;
                                        _Marksheet.Total_Subjective = Subjective;
                                        DbContext.Tbl_Student_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 4; y <= (dt.Columns.Count - 7); y = y + 3)
                                        {
                                            Tbl_StudentMarksheetDet marksheetDet = new Tbl_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.ObjectiveScore = dr[dt.Columns[y].ColumnName + "1"].ToString();
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtypename;
                                        marksheetmailcls.whatsappmobile = stdbasicdtl.Whatsapp_Mobile_No;

                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);
                                        //string url = "http://learn.odmps.org/Reports/Reports.aspx?type=School_Marksheet_Report&Id=" + markid;
                                        ////bitly b = new bitly();
                                        //string shortlink = ToTinyURLS(url);
                                        //if (regdlist.Mobile != "")
                                        //{
                                        //    Sendsmstoemployee(regdlist.Mobile, "Dear " + regdlist.Customer_Name + ", \nPlease find the Online Test Result of your exam " + examtypename + ". Click on the link and save the Test Result.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                                        //}

                                        //if (regdlist.Email != "")
                                        //{
                                        //    string body = PopulateBodyResultSheet(regdlist.Customer_Name, examtypename, shortlink);
                                        //    SendHtmlFormattedEmail(regdlist.Email, "Online Test Result | ODM Public School", body);
                                        //}
                                        //if (stdbasicdtl.Zoom_Mail_ID != "" && stdbasicdtl.Zoom_Mail_ID != null)
                                        //{
                                        //    string body = PopulateBodyResultSheet(regdlist.Customer_Name, examtypename, shortlink);
                                        //    SendHtmlFormattedEmail(stdbasicdtl.Zoom_Mail_ID, "Online Test Result | ODM Public School", body);
                                        //}
                                    }
                                }
                                else if (typ == 4)
                                {
                                    string schoolno = dr["ScL.\nNo."].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        var section = DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == regdlist.SectionId).FirstOrDefault().SectionName;
                                        Tbl_Student_Marksheet _Marksheet = new Tbl_Student_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamTypeId = examtype;
                                        _Marksheet.Percentage_Objective = dr["TOT OBJ     %"].ToString();
                                        _Marksheet.Percentage_Subjective = dr["TOT SUB    %"].ToString();
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["SL.\nNO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = section;
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = regdlist.Customer_Name;
                                        _Marksheet.Total_Mark_Objective = dr["TOT-OBJ (500)"].ToString();
                                        _Marksheet.Total_Mark_Subjective = dr["TOT SUB (500)"].ToString();
                                        _Marksheet.Total_Objective = Objective;
                                        _Marksheet.Total_Subjective = Subjective;
                                        DbContext.Tbl_Student_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 9; y <= (dt.Columns.Count - 7); y = y + 3)
                                        {
                                            Tbl_StudentMarksheetDet marksheetDet = new Tbl_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.ObjectiveScore = dr[dt.Columns[y].ColumnName + "1"].ToString();
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtypename;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.whatsappmobile = stdbasicdtl.Whatsapp_Mobile_No;

                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);
                                        //string url = "http://learn.odmps.org/Reports/Reports.aspx?type=School_Marksheet_Report&Id=" + markid;
                                        ////bitly b = new bitly();
                                        //string shortlink = ToTinyURLS(url);
                                        //if (regdlist.Mobile != "")
                                        //{
                                        //    Sendsmstoemployee(regdlist.Mobile, "Dear " + regdlist.Customer_Name + ", \nPlease find the Online Test Result of your exam " + examtypename + ". Click on the link and save the Test Result.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                                        //}

                                        //if (regdlist.Email != "")
                                        //{
                                        //    string body = PopulateBodyResultSheet(regdlist.Customer_Name, examtypename, shortlink);
                                        //    SendHtmlFormattedEmail(regdlist.Email, "Online Test Result | ODM Public School", body);
                                        //}
                                        //if (stdbasicdtl.Zoom_Mail_ID != "" && stdbasicdtl.Zoom_Mail_ID != null)
                                        //{
                                        //    string body = PopulateBodyResultSheet(regdlist.Customer_Name, examtypename, shortlink);
                                        //    SendHtmlFormattedEmail(stdbasicdtl.Zoom_Mail_ID, "Online Test Result | ODM Public School", body);
                                        //}
                                    }
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                        clsmarks.marksheetmailcls = ss;
                        SendEmailInBackgroundThread(clsmarks);
                        TempData["SuccessMessage"] = "Student Details Entered Successfully";
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return Json(1,JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult sendmarksheetmail()
        {
            clsmarkslist clsmarks = new clsmarkslist();
            List<marksheetmailcls> ss = new List<marksheetmailcls>();
            List<OnlineExamReportModel> ob = (List<OnlineExamReportModel>)Session["marksheetreport"];
            var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true).ToList();

            var regdlist = DbContext.tbl_DC_Registration.ToList();
            foreach (var a in ob)
            {
                var sm = regdetlist.Where(m => m.School_No == a.School_No).FirstOrDefault();
                if (sm != null)
                {
                    var reg = regdlist.Where(m => m.Regd_ID == sm.Regd_ID).FirstOrDefault();
                    marksheetmailcls marksheetmailcls = new marksheetmailcls();
                    marksheetmailcls.customername = a.StudentName;
                    marksheetmailcls.examtypename = a.OnlineExamTypeName;
                    marksheetmailcls.mail = reg.Email;
                    marksheetmailcls.markId = a.StudentmarksheetId;
                    marksheetmailcls.mobile = reg.Mobile;
                    marksheetmailcls.whatsappmobile = sm.Whatsapp_Mobile_No;
                    marksheetmailcls.zoommail = sm.Zoom_Mail_ID;
                    ss.Add(marksheetmailcls);
                }
            }
            clsmarks.marksheetmailcls = ss;
            SendEmailInBackgroundThread(clsmarks);

            TempData["SuccessMessage"] = "Mail Send Successfully";
            return RedirectToAction("OnlineExamReportList");
        }

        public class marksheetmailcls
        {
            public long markId { get; set; }
            public string mobile { get; set; }
            public string whatsappmobile { get; set; }
            public string mail { get; set; }
            public string zoommail { get; set; }
            public string examtypename { get; set; }
            public string customername { get; set; }

        }
        public class clsmarkslist
        {
            public List<marksheetmailcls> marksheetmailcls { get; set; }
        }
        public ActionResult sendmarksheetmailtostudent(long id)
        {
            clsmarkslist clsmarks = new clsmarkslist();
            List<marksheetmailcls> ss = new List<marksheetmailcls>();
            var res = DbContext.Tbl_Student_Marksheet.Where(a => a.StudentmarksheetId == id).FirstOrDefault();

            var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true).ToList();

            var regdlist = DbContext.tbl_DC_Registration.ToList();

            var sm = regdetlist.Where(m => m.School_No == res.School_No).FirstOrDefault();
            if (sm != null)
            {
                var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == res.OnlineExamTypeId).FirstOrDefault().OnlineExamTypeName;

                var reg = regdlist.Where(m => m.Regd_ID == sm.Regd_ID).FirstOrDefault();
                marksheetmailcls marksheetmailcls = new marksheetmailcls();
                marksheetmailcls.customername = res.StudentName;
                marksheetmailcls.examtypename = examtypename;
                marksheetmailcls.mail = reg.Email;
                marksheetmailcls.markId = res.StudentmarksheetId;
                marksheetmailcls.mobile = reg.Mobile;
                marksheetmailcls.zoommail = sm.Zoom_Mail_ID;
                marksheetmailcls.whatsappmobile = sm.Whatsapp_Mobile_No;
                ss.Add(marksheetmailcls);
            }

            clsmarks.marksheetmailcls = ss;
            SendEmailInBackgroundThread(clsmarks);

            TempData["SuccessMessage"] = "Mail Send Successfully";
            return RedirectToAction("OnlineExamReportList");
        }
        public void SendEmailInBackgroundThread(clsmarkslist mailMessage)
        {
            Thread bgThread = new Thread(new ParameterizedThreadStart(sendbackgroundmail));
            bgThread.IsBackground = true;
            bgThread.Start(mailMessage);
        }
        public void sendbackgroundmail(Object obj1)
        {
            clsmarkslist objt = (clsmarkslist)obj1;
            foreach (var obj in objt.marksheetmailcls)
            {
                string url = "https://learn.odmps.org/Reports/Reports.aspx?type=School_Marksheet_Report&Id=" + obj.markId;
               // string url = "http://localhost:1025/Reports/Reports.aspx?type=School_Marksheet_Report&Id=" + obj.markId;
                //bitly b = new bitly();
                string shortlink =ToTinyURLS(url);
                if (obj.mobile != "")
                {
                  Sendsmstoemployee(obj.mobile, "Dear " + obj.customername + ", \nPlease find the Online Test Result of your exam " + obj.examtypename + ". Click on the link and save the Test Result.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                } 
                if (obj.whatsappmobile != "")
                {
                   Sendsmstoemployee(obj.whatsappmobile, "Dear " + obj.customername + ", \nPlease find the Online Test Result of your exam " + obj.examtypename + ". Click on the link and save the Test Result.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                }

                if (obj.mail != "")
                {
                    SendInBlueEmailManager mail = new SendInBlueEmailManager();
                    mail.SendExammail(obj.customername, obj.examtypename, shortlink, obj.mail,Server.MapPath("~/Templates/ResultSheetTemplate.html"));
                   
                }

                if (obj.zoommail != "" && obj.zoommail != null)
                {
                    SendInBlueEmailManager mail = new SendInBlueEmailManager();
                    mail.SendExammail(obj.customername, obj.examtypename, shortlink, obj.zoommail, Server.MapPath("~/Templates/ResultSheetTemplate.html"));                   
                }
            }
        }
        #endregion
        #region doubt List Report
        public ActionResult DoubtListReport(Guid? school, int? board, int? cls, Guid? sec, int? subject, int? chapter, int? teacherId, DateTime? fdt, DateTime? tdt, string status)
        {
            var doubts = DbContext.View_DC_All_Tickets_Details.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            ViewBag.school = school;
            ViewBag.board = board;
            ViewBag.cls = cls;
            ViewBag.sec = sec;
            ViewBag.subject = subject;
            ViewBag.chapter = chapter;
            ViewBag.teacher = teacherId;
            ViewBag.status = status;
            ViewBag.msg = "";
            ViewBag.fdt = fdt;
            ViewBag.tdt = tdt;
            if (board != null && board != 0)
            {
                doubts = doubts.Where(a => a.Board_ID == board).ToList();
            }
            if (cls != null && cls != 0)
            {
                doubts = doubts.Where(a => a.Class_ID == cls).ToList();
            }
            if (sec != null && sec != Guid.Empty)
            {
                doubts = doubts.Where(a => a.SectionId == sec).ToList();
            }
            if (subject != null && subject != 0)
            {
                doubts = doubts.Where(a => a.Subject_ID == subject).ToList();
            }
            if (chapter != null && chapter != 0)
            {
                doubts = doubts.Where(a => a.Chapter_Id == chapter).ToList();
            }
            if (teacherId != null && teacherId != 0)
            {
                var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherId).ToList();
                var boardlistss = assigndet.Select(a => a.Board_Id).ToList();
                var classlist = assigndet.Select(a => a.Class_Id).ToList();
                var sectionlist = assigndet.Select(a => a.SectionId).ToList();
                var subjectlist = assigndet.Select(a => a.Subject_Id).ToList();
                doubts = doubts.Where(a => boardlistss.Contains(a.Board_ID.Value) && classlist.Contains(a.Class_ID.Value) && sectionlist.Contains(a.SectionId) && subjectlist.Contains(a.Subject_ID.Value)).ToList();
            }
            if (fdt != null && tdt != null)
            {
                if (fdt > tdt)
                {
                    ViewBag.msg = "From Date must be less than To date";
                }
                else
                {
                    doubts = doubts.Where(a => a.Inserted_Date.Value.Date >= fdt.Value.Date && a.Inserted_Date.Value.Date <= tdt.Value.Date).ToList();
                }
            }
            if (status != null && status != "")
            {
                if (status == "O")
                {
                    doubts = doubts.Where(a => a.Status == "O" && a.Answer == "False").ToList();
                }
                else if (status == "A")
                {
                    doubts = doubts.Where(a => a.Status == "O" && a.Answer == "True").ToList();
                }
                else if (status == "R")
                {
                    doubts = doubts.Where(a => a.Status == "R").ToList();
                }
                else if (status == "C")
                {
                    doubts = doubts.Where(a => a.Status == "C").ToList();
                }
            }
            Session["Doubts_Det"] = doubts;
            return View(doubts);
        }
        public JsonResult GetTeacherList(int? board, int? cls, Guid? sec, int? subject)
        {
            var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true).ToList();
            if (board != null && board != 0)
            {
                assigndet = assigndet.Where(a => a.Board_Id == board).ToList();
            }
            if (cls != null && cls != 0)
            {
                assigndet = assigndet.Where(a => a.Class_Id == cls).ToList();
            }
            if (sec != null && sec != Guid.Empty)
            {
                assigndet = assigndet.Where(a => a.SectionId == sec).ToList();
            }
            if (subject != null && subject != 0)
            {
                assigndet = assigndet.Where(a => a.Subject_Id == subject).ToList();
            }
            var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
            var res = (from a in assigndet
                       join b in teachers on a.Teacher_ID equals b.TeacherId
                       select b).ToList();
            return Json(teachers);
        }
        public ActionResult Exportalldoubtreportdata()
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();
            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Sl. No.");
            validationTable.Columns.Add("Student Name");
            validationTable.Columns.Add("Board");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Section");
            validationTable.Columns.Add("Subject");
            validationTable.Columns.Add("Chapter");
            validationTable.Columns.Add("Ticket No");
            validationTable.Columns.Add("Teacher Name");
            validationTable.Columns.Add("Status");
            validationTable.Columns.Add("Raised On");

            validationTable.TableName = "Doubt_Details";
            List<View_DC_All_Tickets_Details> res = (Session["Doubts_Det"]) as List<View_DC_All_Tickets_Details>;
            int i = 0;
            var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();

            foreach (var gs in res.ToList())
            {
                DataRow dr = validationTable.NewRow();
                dr["Sl. No."] = i + 1;
                dr["Student Name"] = gs.Customer_Name;
                dr["Board"] = gs.Board_Name;
                dr["Class"] = gs.Class_Name;
                dr["Section"] = sections.Where(a => a.SectionId == gs.SectionId).FirstOrDefault().SectionName;
                dr["Subject"] = gs.Subject;
                dr["Chapter"] = gs.Chapter;
                dr["Ticket No"] = gs.Ticket_No;
                dr["Teacher Name"] = (gs.Teach_ID != 0 && gs.Teach_ID != null) ? teachers.Where(a => a.TeacherId == gs.Teach_ID).FirstOrDefault().Name : "";
                dr["Status"] = (gs.Status == "O" && gs.Answer == "False") ? "Pending" : ((gs.Status == "O" && gs.Answer == "True") ? "Answered" : (gs.Status == "R" ? "Rejected" : (gs.Status == "C" ? "Closed" : "")));
                dr["Raised On"] = gs.Inserted_Date.Value.ToString("dd-MM-yyyy hh:mm tt");
                validationTable.Rows.Add(dr);
                i += 1;
            }



            oWB.Worksheets.Add(validationTable);
            //oWB.SaveAs("User_Details.xlsx");
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Doubt_Details.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            TempData["Message"] = "Successfully Downloaded Excel File";
            return RedirectToAction("DoubtListReport", "SchoolReport");
        }
        public class Doubtcls
        {
            public string statusname { get; set; }
            public string TeacherName { get; set; }
            public bool Answers { get; set; }
            public int RegdId { get; set; }
            public int doubtId { get; set; }
            public string studentName { get; set; }
            public string mobile { get; set; }
            public string zoomId { get; set; }
            public string zoomCode { get; set; }
            public string zoomMailId { get; set; }
            public string zoomPassword { get; set; }
            public string tquestion { get; set; }
            public string questionImage { get; set; }
            public string className { get; set; }
            public string sectionName { get; set; }
            public string subjectName { get; set; }
            public string chapterName { get; set; }
            public string raiseDate { get; set; }
            public string ticketNo { get; set; }
            public string status { get; set; }
            public int teacherId { get; set; }
        }
        public class TicketDetailscls
        {
            //public string student_name { get; set; }
            //public string TClass_name { get; set; }
            public Doubtcls doubt { get; set; }

            //public string classData { get; set; }
            //public string status { get; set; }
            //public int TicketId { get; set; }
            //public string ticketno { get; set; }
            //public int? studentId { get; set; }
            public List<tbl_DC_Ticket_Dtl> all_ticket_answer { get; set; }
            public List<tbl_DC_Ticket_Thread> comments { get; set; }
            public string isclosed { get; set; }
            public tbl_DC_Ticket_Dtl check_answer { get; set; }
        }
        public ActionResult ViewDoubtDetails(int id)
        {
            var ticket_qsn = DbContext.View_DC_All_Tickets_Details.Where(x => x.Ticket_ID == id).ToList();
            int reg = ticket_qsn.ToList()[0].Regd_ID;
            var regd = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Regd_ID == reg).FirstOrDefault();
            var regddet = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Regd_ID == reg).FirstOrDefault();

            var Section = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            int sub = ticket_qsn.ToList()[0].Subject_ID.Value;
            var assigndet = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.SectionId == regd.SectionId && a.Board_Id == regddet.Board_ID && a.Class_Id == regddet.Class_ID && a.Subject_Id == sub).FirstOrDefault();
            var teachers = assigndet == null ? null : DbContext.Tbl_Teacher_Zoom_Det.Where(a => a.TeacherId == assigndet.Teacher_ID).FirstOrDefault();
            var teacherdet = assigndet == null ? null : DbContext.Teachers.Where(a => a.Active == 1 && a.TeacherId == assigndet.Teacher_ID).FirstOrDefault();
            // var cData = ClassName + " / " + "Section- " + Section + " / " + ticket_qsn.Subject + " / " + ticket_qsn.Chapter;

            var res = (from a in ticket_qsn
                       select new Doubtcls
                       {
                           chapterName = a.Chapter,
                           className = a.Class_Name,
                           raiseDate = a.Inserted_Date.Value.ToString("dd-MM-yyyy"),
                           RegdId = a.Regd_ID,
                           sectionName = Section.Where(m => m.SectionId == a.SectionId).FirstOrDefault().SectionName,
                           statusname = a.Status == "O" ? (Convert.ToBoolean(a.Answer) == false ? "PENDING" : "ANSWERED") : (a.Status == "C" ? "CLOSED" : "REJECTED"),
                           status = a.Status,
                           studentName = a.Customer_Name,
                           subjectName = a.Subject,
                           ticketNo = a.Ticket_No,
                           doubtId = a.Ticket_ID,
                           mobile = regd.Mobile,
                           zoomCode = teachers == null ? "" : teachers.Zoom_Code,
                           zoomId = teachers == null ? "" : teachers.ZooMId,
                           zoomMailId = teachers == null ? "" : teachers.Zoom_MailId,
                           zoomPassword = teachers == null ? "" : teachers.Zoom_Password,
                           Answers = Convert.ToBoolean(a.Answer),
                           tquestion = a.Question,
                           TeacherName = teacherdet == null ? "" : teacherdet.Name,
                           questionImage = a.Question_Image == null ? "" : "/Images/Qusetionimages/" + a.Question_Image
                       }).FirstOrDefault();
            TicketDetailscls ob = new TicketDetailscls();
            ob.doubt = res;
            var tick = DbContext.tbl_DC_Ticket.Where(a => a.Ticket_ID == res.doubtId).FirstOrDefault();
            List<tbl_DC_Ticket_Thread> threadslist = new List<tbl_DC_Ticket_Thread>();
            tbl_DC_Ticket_Thread th = new tbl_DC_Ticket_Thread();
            th.Is_Teacher = false;
            th.User_Comment = res.tquestion;
            th.R_image = res.questionImage;
            th.Is_Active = true;
            th.Is_Deleted = false;
            th.User_Id = res.RegdId;
            th.User_Comment_Date = ticket_qsn.FirstOrDefault().Inserted_Date;
            th.Ticket_Dtl_ID = 0;
            th.Ticket_ID = res.doubtId;
            th.Is_Audio = tick.Is_Audio;
            th.Is_Taecher_Read = true;
            th.Comment_ID = 0;
            threadslist.Add(th);
            var dtl = (from a in DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList()
                       select new tbl_DC_Ticket_Thread
                       {
                           Is_Teacher = true,
                           User_Comment = a.Answer,
                           R_image = a.Answer_Image != null ? ("/Images/Answerimage/" + a.Answer_Image) : "",
                           Is_Active = a.Is_Active,
                           Is_Deleted = a.Is_Deleted,
                           User_Id = a.Replied_By,
                           User_Comment_Date = a.Replied_Date,
                           Ticket_Dtl_ID = (a.Ticket_Dtls_ID) as Nullable<int>,
                           Ticket_ID = a.Ticket_ID,
                           Is_Audio = a.Is_Audio,
                           Is_Taecher_Read = true,
                           Comment_ID = 0
                       }).ToList();
            threadslist.AddRange(dtl);
            var comments = (from a in DbContext.tbl_DC_Ticket_Thread.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList()
                            select new tbl_DC_Ticket_Thread
                            {
                                Comment_ID = a.Comment_ID,
                                Is_Active = a.Is_Active,
                                Is_Deleted = a.Is_Deleted,
                                Is_Taecher_Read = a.Is_Taecher_Read,
                                Is_Teacher = a.Is_Teacher,
                                Modified_By = a.Modified_By,
                                Modified_Date = a.Modified_Date,
                                R_image = a.R_image != null ? ("/Images/Qusetionimages/" + a.R_image) : "",
                                Ticket_Dtl_ID = a.Ticket_Dtl_ID,
                                Ticket_ID = a.Ticket_ID,
                                User_Comment = a.User_Comment,
                                User_Comment_Date = a.User_Comment_Date,
                                User_Id = a.User_Id,
                                Is_Audio = a.Is_Audio
                            }).ToList();
            threadslist.AddRange(comments);
            ob.comments = threadslist;
            ob.isclosed = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Status).FirstOrDefault();
            ob.check_answer = (from a in DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList()
                               select new tbl_DC_Ticket_Dtl
                               {
                                   Answer = a.Answer,
                                   Answer_Image = a.Answer_Image != null ? ("/Images/Answerimage/" + a.Answer_Image) : "",
                                   Is_Active = a.Is_Active,
                                   Is_Deleted = a.Is_Deleted,
                                   Replied_By = a.Replied_By,
                                   Replied_Date = a.Replied_Date,
                                   Ticket_Dtls_ID = a.Ticket_Dtls_ID,
                                   Ticket_ID = a.Ticket_ID,
                                   Is_Audio = a.Is_Audio
                               }).FirstOrDefault();

            return View(ob);
        }
        #endregion

        #region Revision Materials

        public ActionResult RevisionMaterial(Guid? schoolid, int? boardid, int? classid, int? Subjectid, int? chapterid)
        {
            try
            {
                var schoolList = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
                var boardList = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var classList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var sectionList = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var chapterList = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var revisionMaterial = DbContext.RevisionMaterials.Where(a => a.Active == true).ToList();
                var revisionpdf = DbContext.RevisionMaterialPdfs.Where(a => a.Active == true).ToList();
                ViewBag.School = schoolList;
                ViewBag.Board = boardList;
                ViewBag.Class = classList;
                ViewBag.Section = sectionList;
                ViewBag.Subject = subjectList;
                ViewBag.Chapter = chapterList;


                var result = (from a in revisionMaterial
                              join b in revisionpdf on a.ID equals b.RevisionMaterialId
                              join c in schoolList on a.SchoolId equals c.SchoolId
                              join d in boardList on a.BoardId equals d.Board_Id
                              join e in classList on a.ClassId equals e.Class_Id
                              // join f in sectionList on a.SectionId equals f.SectionId
                              join g in subjectList on a.SubjectId equals g.Subject_Id
                              join h in chapterList on a.ChapterId equals h.Chapter_Id
                              select new RevisionMaterialModel
                              {
                                  ID = a.ID,
                                  SchoolId = c.SchoolId,
                                  SchoolName = c.SchoolName,
                                  BoardId = d.Board_Id,
                                  BoardName = d.Board_Name,
                                  ClassId = e.Class_Id,
                                  ClassName = e.Class_Name,
                                  // SectionName = f.SectionName,
                                  SubjectId = g.Subject_Id,
                                  SubjectName = g.Subject,
                                  ChapaterId = h.Chapter_Id,
                                  ChapterName = h.Chapter,
                                  RevisionPdfID = b.ID,
                                  Pdf = b.PdfUrl
                              }).ToList();



                if (schoolid != null)
                {
                    result = result.Where(a => a.SchoolId == schoolid).ToList();
                }
                if (classid != null)
                {
                    if (classid != null && Subjectid != null && chapterid != null)
                    {
                        result = result.Where(a => a.ClassId == classid && a.SubjectId == Subjectid && a.ChapaterId == chapterid).ToList();
                    }
                    else
                    {
                        result = result.Where(a => a.ClassId == classid).ToList();
                    }
                }
                if (Subjectid != null)
                {
                    if (Subjectid != null && chapterid != null)
                    {
                        result = result.Where(a => a.SubjectId == Subjectid && a.ChapaterId == chapterid).ToList();
                    }
                    else
                    {
                        result = result.Where(a => a.SubjectId == Subjectid).ToList();
                    }
                }
                if (chapterid != null)
                {
                    result = result.Where(a => a.ChapaterId == chapterid).ToList();
                }


                return View(result);
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return null;
            }
        }

        public ActionResult CreateRevisionMaterial()
        {
            try
            {
                ViewBag.schoolList = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
                ViewBag.boardList = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                ViewBag.classList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                ViewBag.sectionList = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                ViewBag.subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                ViewBag.chapterList = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();

                return View();

            }
            catch (Exception e)
            {
                string msg = e.Message;
                return null;
            }
        }

        [HttpPost]
        public ActionResult SaveRevisionMaterial(Guid school, int board, int cls, int Subject, int Chapter, HttpPostedFileBase pdfurl)
        {
            try
            {
                if (school != null)
                {
                    RevisionMaterial revisionMaterial = new RevisionMaterial();
                    revisionMaterial.InsertedDate = DateTime.Now;
                    revisionMaterial.InsertedId = 1;
                    revisionMaterial.ModifiedDate = DateTime.Now;
                    revisionMaterial.ModifiedId = 1;
                    revisionMaterial.Active = true;
                    revisionMaterial.Status = "Active";
                    revisionMaterial.SchoolId = school;
                    revisionMaterial.BoardId = board;
                    //revisionMaterial.SectionId = sectionid;
                    revisionMaterial.ClassId = cls;
                    revisionMaterial.SubjectId = Subject;
                    revisionMaterial.ChapterId = Chapter;

                    DbContext.RevisionMaterials.Add(revisionMaterial);
                    DbContext.SaveChanges();
                    var s = DbContext.RevisionMaterials.ToList().LastOrDefault().ID;
                    if (s > 0)
                    {
                        RevisionMaterialPdf materialPdf = new RevisionMaterialPdf();
                        materialPdf.InsertedDate = DateTime.Now;
                        materialPdf.InsertedId = 1;
                        materialPdf.ModifiedDate = DateTime.Now;
                        materialPdf.ModifiedId = 1;
                        materialPdf.Active = true;
                        materialPdf.Status = "Active";
                        materialPdf.PdfName = "RevisionPdf";
                        materialPdf.RevisionMaterialId = s;
                        if (pdfurl != null)
                        {
                            string guid = Guid.NewGuid().ToString();
                            string path = string.Empty;
                            var fileName = Path.GetFileName(pdfurl.FileName.
                                Replace(pdfurl.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            path = Path.Combine(Server.MapPath("~/Images/RevisionPdf/"), fileName);
                            pdfurl.SaveAs(path);
                            materialPdf.PdfUrl = fileName;
                        }

                        DbContext.RevisionMaterialPdfs.Add(materialPdf);
                        DbContext.SaveChanges();
                    }
                }

                return RedirectToAction("RevisionMaterial");
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ActionResult EditRevisionMaterial(long id, long pdfid)
        {
            try
            {
                ViewBag.schoolList = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
                ViewBag.boardList = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                ViewBag.classList = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                ViewBag.sectionList = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                ViewBag.subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                ViewBag.chapterList = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();

                var revisionMaterial = DbContext.RevisionMaterials.Where(a => a.ID == id && a.Active == true).FirstOrDefault();
                var revisionPdf = DbContext.RevisionMaterialPdfs.Where(a => a.RevisionMaterialId == id && a.ID == pdfid && a.Active == true).FirstOrDefault();
                ViewBag.revisionMaterial = revisionMaterial;
                ViewBag.revisionPdfs = revisionPdf;

                return View();

            }
            catch (Exception e)
            {
                string msg = e.Message;
                return null;
            }
        }

        [HttpPost]
        public ActionResult UpdateRevisionMaterial(long id, HttpPostedFileBase pdfurl)
        {
            try
            {
                if (id > 0)
                {
                    var revisionDetails = DbContext.RevisionMaterialPdfs.Where(a => a.RevisionMaterialId == id).FirstOrDefault();
                    revisionDetails.ModifiedDate = DateTime.Now;
                    revisionDetails.Status = "Updated";

                    if (pdfurl != null)
                    {
                        string guid = Guid.NewGuid().ToString();
                        string path = string.Empty;
                        var fileName = Path.GetFileName(pdfurl.FileName.
                            Replace(pdfurl.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                        path = Path.Combine(Server.MapPath("~/Images/RevisionPdf/"), fileName);
                        pdfurl.SaveAs(path);
                        revisionDetails.PdfUrl = fileName;
                    }

                    DbContext.SaveChanges();
                    TempData["SuccessMessage"] = "Updated Successfully";
                }

                return RedirectToAction("RevisionMaterial");
            }
            catch (Exception e)
            {
                return null;
            }
        }




        public ActionResult RemoveRevisionMaterial(long id)
        {
            var res = DbContext.RevisionMaterials.Where(a => a.ID == id).FirstOrDefault();
            res.Active = false;
            res.ModifiedDate = DateTime.Now;
            res.Status = "Deleted";
            DbContext.SaveChanges();

            var result = DbContext.RevisionMaterialPdfs.Where(a => a.RevisionMaterialId == id).ToList();
            foreach (var s in result)
            {
                s.Active = false;
                s.ModifiedDate = DateTime.Now;
                s.Status = "Deleted";
            }
            DbContext.SaveChanges();

            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("RevisionMaterial");
        }

        #endregion

        #region Student AcademicPerformance Report
        public ActionResult StudentAcademicPerformanceReport(Guid? school, int? board, int? cls, Guid? sec, int? student, int? type, string session)
        {
            var examtype = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            ViewBag.examtype = examtype;
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.schoolist = schools;
            ViewBag.s = school;
            ViewBag.b = board;
            ViewBag.c = cls;
            ViewBag.se = sec;
            ViewBag.f = student;
            ViewBag.g = type;
            var Marksheet = DbContext.Tbl_Student_Marksheet.Where(a => a.Is_Active == true).ToList();

            var res = (from a in Marksheet
                       join b in examtype on a.OnlineExamTypeId equals b.OnlineExamTypeId
                       select new OnlineExamReportModel
                       {
                           StudentmarksheetId = a.StudentmarksheetId,
                           OnlineExamTypeId = a.OnlineExamTypeId,
                           OnlineExamTypeName = b.OnlineExamTypeName,
                           RegdId = a.RegdId,
                           Session = a.Session,
                           section = a.section,
                           Total_Mark_Objective = a.Total_Mark_Objective,
                           Total_Mark_Subjective = a.Total_Mark_Subjective,
                           Percentage_Subjective = a.Percentage_Subjective,
                           Percentage_Objective = a.Percentage_Objective,
                           Roll_No = a.Roll_No,
                           School_No = a.School_No,
                           StudentName = a.StudentName,
                           ClassName = a.ClassName,
                           typeId = a.typeId,
                           ModifiedOn = a.ModifiedOn,
                           InsertedOn = a.InsertedOn.Value.ToString("yyyy-MM-dd")
                       }
                      ).ToList();

            if (cls != null)
            {
                string classname = DbContext.tbl_DC_Class.Where(v => v.Class_Id == cls).FirstOrDefault().Class_Name;
                res = res.Where(a => a.ClassName == classname).ToList();
            }
            if (sec != null)
            {
                string sessionname = DbContext.tbl_DC_Class_Section.Where(v => v.SectionId == sec).FirstOrDefault().SectionName;
                res = res.Where(a => a.section == sessionname).ToList();
            }
            if (student != null)
            {
                res = res.Where(a => a.RegdId == student).ToList();
            }
            if (type != null)
            {
                res = res.Where(a => a.OnlineExamTypeId == type).ToList();
            }

            if (session != null && session != "All")
            {
                res = res.Where(a => a.Session == session).ToList();
            }
            return View(res);
        }
        public ActionResult AllStudentAcademicPerformanceReport(Guid? school, int? board, int? cls, Guid? sec, int? student, string session)
        {
            var examtype = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
            ViewBag.examtype = examtype;
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.schoolist = schools;
            ViewBag.s = school;
            ViewBag.b = board;
            ViewBag.c = cls;
            ViewBag.se = sec;
            ViewBag.f = student;

            var Marksheet = DbContext.Tbl_Student_Marksheet.Where(a => a.Is_Active == true).ToList();

            var res = (from a in Marksheet
                       join b in examtype on a.OnlineExamTypeId equals b.OnlineExamTypeId
                       select new OnlineExamReportModel
                       {
                           StudentmarksheetId = a.StudentmarksheetId,
                           OnlineExamTypeId = a.OnlineExamTypeId,
                           OnlineExamTypeName = b.OnlineExamTypeName,
                           RegdId = a.RegdId,
                           Session = a.Session,
                           section = a.section,
                           Total_Mark_Objective = a.Total_Mark_Objective,
                           Total_Mark_Subjective = a.Total_Mark_Subjective,
                           Percentage_Subjective = a.Percentage_Subjective,
                           Percentage_Objective = a.Percentage_Objective,
                           Roll_No = a.Roll_No,
                           School_No = a.School_No,
                           StudentName = a.StudentName,
                           ClassName = a.ClassName,
                           typeId = a.typeId,
                           ModifiedOn = a.ModifiedOn,
                           InsertedOn = a.InsertedOn.Value.ToString("yyyy-MM-dd")
                       }
                      ).ToList();

            if (cls != null)
            {
                string classname = DbContext.tbl_DC_Class.Where(v => v.Class_Id == cls).FirstOrDefault().Class_Name;
                res = res.Where(a => a.ClassName == classname).ToList();
            }
            if (sec != null)
            {
                string sessionname = DbContext.tbl_DC_Class_Section.Where(v => v.SectionId == sec).FirstOrDefault().SectionName;
                res = res.Where(a => a.section == sessionname).ToList();
            }
            if (student != null)
            {
                res = res.Where(a => a.RegdId == student).ToList();
            }
            if (session != null && session != "All")
            {
                res = res.Where(a => a.Session == session).ToList();
            }
            return View(res);
        }
        #endregion

        #region------------------------Notice---------------------------
        public class NoticeModalClass
        {
            public string Titel { get; set; }
            public string Description { get; set; }
            public int Regd_ID { get; set; }
            public Guid School_ID { get; set; }
            public string School_Name { get; set; }
            public int Board_ID { get; set; }
            public string Board_Name { get; set; }
            public int Class_ID { get; set; }
            public string Class_Name { get; set; }
            public Guid Section_Id { get; set; }
            public string Section_Name { get; set; }
            public string Regd_No { get; set; }
            public string Customer_Name { get; set; }
            public string Email { get; set; }
            public string Mobile { get; set; }
            public DateTime Inserted_Date { get; set; }
            public string Inserted_By { get; set; }
            public string attachment { get; set; }
            public string Modified_By { get; set; }
        }
        public JsonResult GetStudent(int boardid, int classid)
        {
            var dtls = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Is_Active == true && x.Class_ID == classid && x.Board_ID == boardid).Distinct().ToList();
            var studentlist = DbContext.tbl_DC_Registration.Where(x => x.Is_Active == true).Distinct().ToList();
            var res = (from a in dtls
                       join b in studentlist on a.Regd_ID equals b.Regd_ID
                       select new
                       {
                           id = a.Regd_ID,
                           name = b.Customer_Name
                       }
                ).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);
        }


        public ActionResult CreateNotice()
        {
            return View();

        }
        [HttpPost]
        public ActionResult SaveNotice(string Titel, string Description, HttpPostedFileBase File, int? student, Guid? school, Guid? sec, int? board, int? cls)
        {
            if (student != null && student != 0 && student.ToString() != " ")
            {
                Guid? schoolid = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Regd_ID == student).FirstOrDefault().SchoolId;
                Guid? secid = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Regd_ID == student).FirstOrDefault().SectionId;
                int? boardid = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Regd_ID == student).FirstOrDefault().Board_ID;
                int? clsid = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Regd_ID == student).FirstOrDefault().Class_ID;

                tbl_DC_Notice obj = new tbl_DC_Notice();
                obj.Board_ID = boardid;
                obj.Class_ID = clsid;
                obj.School_ID = schoolid;
                obj.Section_Id = secid;
                obj.Regd_ID = student;
                obj.Titel = Titel;
                obj.Description = Description;
                obj.Modified_Date = DateTime.Now;
                obj.Inserted_Date = DateTime.Now;
                obj.Modified_By = "1";
                obj.Inserted_By = "1";
                obj.Is_Active = true;
                obj.Is_Deleted = false;
                DbContext.tbl_DC_Notice.Add(obj);
                DbContext.SaveChanges();
                int id = obj.Notice_ID;
                string image = string.Empty;
                string guid = Guid.NewGuid().ToString();

                tbl_DC_Notice_Attachment att = new tbl_DC_Notice_Attachment();

                if (File != null)
                {
                    var fileName = Path.GetFileName(File.FileName.Replace(File.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    image = Convert.ToString(fileName);
                    var path = Path.Combine(Server.MapPath("~/Images/Notice/"), fileName);
                    File.SaveAs(path);
                    att.Path = image;
                    att.File_Name = ".pdf";
                }
                else
                {
                    att.Path = " ";
                    att.File_Name = " ";
                }

                att.Notice_ID = id;
                att.Modified_By = 1;
                att.Modified_Date = DateTime.Now;
                att.Inserted_Date = DateTime.Now;
                att.Inserted_By = 1;
                att.Is_Active = true;
                att.Is_Deleted = false;
                DbContext.tbl_DC_Notice_Attachment.Add(att);
                DbContext.SaveChanges();
            }
            else
            {
                var studentlist = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var studentdtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true).ToList();
                var res = (from a in studentlist
                           join b in studentdtls on a.Regd_ID equals b.Regd_ID
                           select new NoticeModalClass
                           {
                               School_ID = a.SchoolId.Value,
                               Section_Id = a.SectionId.Value,
                               Class_ID = b.Class_ID.Value,
                               Regd_ID = a.Regd_ID,
                               Board_ID = b.Board_ID.Value,
                               Customer_Name = a.Customer_Name
                           }
                           ).ToList();
                if (school != null && school.ToString() != " ")
                {
                    res = res.Where(a => a.School_ID == school).ToList();
                }
                if (sec != null && sec.ToString() != " ")
                {
                    res = res.Where(a => a.Section_Id == sec).ToList();
                }
                if (board != null && board != 0)
                {
                    res = res.Where(a => a.Board_ID == board).ToList();
                }
                if (cls != null && cls != 0)
                {
                    res = res.Where(a => a.Class_ID == cls).ToList();
                }
                foreach (var arrlist in res)
                {
                    tbl_DC_Notice obj = new tbl_DC_Notice();
                    obj.Board_ID = arrlist.Board_ID;
                    obj.Class_ID = arrlist.Class_ID;
                    obj.School_ID = arrlist.School_ID;
                    obj.Section_Id = arrlist.Section_Id;
                    obj.Regd_ID = arrlist.Regd_ID;
                    obj.Titel = Titel;
                    obj.Description = Description;
                    obj.Modified_Date = DateTime.Now;
                    obj.Inserted_Date = DateTime.Now;
                    obj.Modified_By = "1";
                    obj.Inserted_By = "1";
                    obj.Is_Active = true;
                    obj.Is_Deleted = false;
                    DbContext.tbl_DC_Notice.Add(obj);
                    DbContext.SaveChanges();
                    int id = obj.Notice_ID;
                    string image = string.Empty;
                    string guid = Guid.NewGuid().ToString();

                    tbl_DC_Notice_Attachment att = new tbl_DC_Notice_Attachment();

                    if (File != null)
                    {
                        var fileName = Path.GetFileName(File.FileName.Replace(File.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                        image = Convert.ToString(fileName);
                        var path = Path.Combine(Server.MapPath("~/Images/Notice/"), fileName);
                        File.SaveAs(path);
                        att.Path = image;
                        att.File_Name = ".pdf";
                    }

                    else
                    {
                        att.Path = " ";
                        att.File_Name = " ";
                    }
                    att.File_Name = ".pdf";
                    att.Notice_ID = id;
                    att.Modified_By = 1;
                    att.Modified_Date = DateTime.Now;
                    att.Inserted_Date = DateTime.Now;
                    att.Inserted_By = 1;
                    att.Is_Active = true;
                    att.Is_Deleted = false;
                    DbContext.tbl_DC_Notice_Attachment.Add(att);
                    DbContext.SaveChanges();
                }
            }

            TempData["SuccessMessage"] = " Successfully Edit Profile ";
            return RedirectToAction("CreateNotice");

        }
        public JsonResult GetAllStudentById(int? boardid, int? classid, Guid? schoolid, Guid? sectionid)
        {
            var studentlist = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
            var studentdtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true).ToList();
            var res = (from a in studentlist
                       join b in studentdtls on a.Regd_ID equals b.Regd_ID
                       select new NoticeModalClass
                       {
                           School_ID = a.SchoolId.Value,
                           Section_Id = a.SectionId.Value,
                           Class_ID = b.Class_ID.Value,
                           Regd_ID = a.Regd_ID,
                           Board_ID = b.Board_ID.Value,
                           Customer_Name = a.Customer_Name
                       }
                       ).ToList();
            if (schoolid != null && schoolid.ToString() != " ")
            {
                res = res.Where(a => a.School_ID == schoolid).ToList();
            }
            if (sectionid != null && sectionid.ToString() != " ")
            {
                res = res.Where(a => a.Section_Id == sectionid).ToList();
            }
            if (boardid != null && boardid != 0)
            {
                res = res.Where(a => a.Board_ID == boardid).ToList();
            }
            if (classid != null && classid != 0)
            {
                res = res.Where(a => a.Class_ID == classid).ToList();
            }
            return Json(res, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ViewNotice(int? board, int? cls, Guid? sec, Guid? school, int? student, DateTime? fdt, DateTime? tdt)
        {
            var notice = DbContext.tbl_DC_Notice.Where(a => a.Is_Active == true).ToList();
            var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
            var schoollist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            var section = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var classlist = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
            var noticeatt = DbContext.tbl_DC_Notice_Attachment.Where(a => a.Is_Active == true).ToList();
            var studentlist = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
            ViewBag.board = board;
            ViewBag.school = school;
            ViewBag.cls = cls;
            ViewBag.sec = sec;
            ViewBag.student = student;
            ViewBag.fdt = fdt;
            ViewBag.tdt = tdt;
            var res = (from a in notice
                       join b in noticeatt on a.Notice_ID equals b.Notice_ID
                       join c in studentlist on a.Regd_ID equals c.Regd_ID
                       join d in boardlist on a.Board_ID equals d.Board_Id
                       select new NoticeModalClass
                       {
                           Titel = a.Titel,
                           Description = a.Description,
                           School_ID = a.School_ID.Value,
                           Section_Id = a.Section_Id.Value,
                           Class_ID = a.Class_ID.Value,
                           Regd_ID = a.Regd_ID.Value,
                           Board_ID = a.Board_ID.Value,
                           Customer_Name = c.Customer_Name,
                           School_Name = schoollist.Where(x => x.SchoolId == a.School_ID).FirstOrDefault().SchoolName,
                           Section_Name = section.Where(x => x.SectionId == a.Section_Id).FirstOrDefault().SectionName,
                           Class_Name = classlist.Where(x => x.Class_Id == a.Class_ID).FirstOrDefault().Class_Name,
                           Mobile = c.Mobile,
                           Inserted_Date = a.Inserted_Date.Value,
                           Email = c.Email,
                           attachment = b.Path
                       }
                       ).ToList();

            if (board != null && board != 0)
            {
                res = res.Where(a => a.Board_ID == board).ToList();
            }
            if (cls != null && cls != 0)
            {
                res = res.Where(a => a.Class_ID == cls).ToList();
            }
            if (sec != null && sec != Guid.Empty)
            {
                res = res.Where(a => a.Section_Id == sec).ToList();
            }
            if (school != null && school != Guid.Empty)
            {
                res = res.Where(a => a.School_ID == school).ToList();
            }
            if (student != null && student != 0)
            {
                res = res.Where(a => a.Regd_ID == student).ToList();
            }
            if (fdt != null && tdt != null)
            {
                if (fdt > tdt)
                {
                    ViewBag.msg = "From Date must be less than To date";
                }
                else
                {
                    res = res.Where(a => a.Inserted_Date.Date >= fdt.Value.Date && a.Inserted_Date.Date <= tdt.Value.Date).ToList();
                }
            }
            return View(res);

        }

        #endregion----------------------Notice--------------------------


        #region Upload Half-Yearly Online Exam Report

        public ActionResult OnlineHalfYearlyExamReportList(Guid? school, int? board, int? cls, Guid? sec, int? student, string session)
        {
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.schoolist = schools;
            ViewBag.s = school;
            ViewBag.b = board;
            ViewBag.c = cls;
            ViewBag.se = sec;
            ViewBag.f = student;
            var Marksheet = DbContext.Tbl_Student_Halfyearly_Marksheet.Where(a => a.Is_Active == true).ToList();

            var res = (from a in Marksheet
                       select new OnlineExamReportModel
                       {
                           StudentmarksheetId = a.StudentmarksheetId,
                           OnlineExamTypeName = a.OnlineExamType,
                           RegdId = a.RegdId,
                           Session = a.Session,
                           section = a.section,
                           Total_Mark_Objective = a.Total_Mark_Objective.ToString(),
                           Total_Mark_Subjective = a.Total_Mark_Subjective.ToString(),
                           Percentage_Subjective = a.Percentage_Subjective.ToString(),
                           Percentage_Objective = a.Percentage_Objective.ToString(),
                           Roll_No = a.Roll_No,
                           School_No = a.School_No,
                           StudentName = a.StudentName,
                           ClassName = a.ClassName,
                           typeId = a.typeId,
                           ModifiedOn = a.ModifiedOn,
                           InsertedOn = a.InsertedOn.Value.ToString("yyyy-MM-dd"),
                           Batch = a.Batch,
                           SubjectCombination = a.SubjectCombination
                       }
                      ).ToList();

            if (cls != null)
            {
                string classname = DbContext.tbl_DC_Class.Where(v => v.Class_Id == cls).FirstOrDefault().Class_Name;
                res = res.Where(a => a.ClassName == classname).ToList();
            }
            if (sec != null)
            {
                string sessionname = DbContext.tbl_DC_Class_Section.Where(v => v.SectionId == sec).FirstOrDefault().SectionName;
                res = res.Where(a => a.section == sessionname).ToList();
            }
            if (student != null)
            {
                res = res.Where(a => a.RegdId == student).ToList();
            }


            if (session != null && session != "All")
            {
                res = res.Where(a => a.Session == session).ToList();
            }
            Session["marksheetreport"] = res;
            return View(res);
        }
        public ActionResult UploadHalfYearlyOnlineExamReport()
        {
            var schools = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            ViewBag.schools = schools;


            return View();
        }
        [HttpPost]
        public ActionResult UploadstudentHalfYearlymarksheetReport(int typ, string examtype, string remark, Guid school, int board, int total, int cls, string session, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("UploadHalfYearlyOnlineExamReport");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);

                        XLWorkbook workbook = new XLWorkbook(path);


                        IXLWorksheet xLWorksheet = workbook.Worksheet(1);
                        DataTable dt = new DataTable();
                        var i = 0;
                        foreach (IXLRow row in xLWorksheet.Rows())
                        {
                            if (i == 1)
                            {
                                //foreach (IXLCell cell in row.Cells())
                                //{
                                int m = 1;
                                for (int a = 0; a < row.Cells().Count(); a++)
                                {
                                    //if (m <= 3)
                                    //{
                                    if (row.Cells().ToList()[a].Value.ToString() == "")
                                    {
                                        dt.Columns.Add(row.Cells().ToList()[a - m].Value.ToString().Trim() + m);
                                        m++;
                                    }
                                    else
                                    {
                                        dt.Columns.Add(row.Cells().ToList()[a].Value.ToString().Trim());
                                        m = 1;
                                    }
                                    //}
                                }
                                //}
                            }
                            else if (i > 1)
                            {
                                dt.Rows.Add();
                                int j = 0;
                                foreach (IXLCell cell in row.Cells())
                                {
                                    dt.Rows[dt.Rows.Count - 1][j] = cell.Value.ToString();
                                    j++;
                                }
                            }
                            i = i + 1;
                        }

                        var data = dt;
                        bool firstrow = true;
                        bool firstrow1 = true;
                        List<string> schnos = new List<string>();
                        //foreach (DataRow dr in dt.Rows)
                        //{
                        //    if (firstrow1 == true)
                        //    {
                        //        firstrow1 = false;
                        //    }
                        //    else
                        //    {
                        //        string schoolno = dr["SCHOOL NO."].ToString();
                        //        var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                        //        if (regdetlist == null)
                        //        {
                        //            schnos.Add(schoolno);
                        //        }
                        //    }
                        //}

                        var result = schnos;

                        clsmarkslist clsmarks = new clsmarkslist();
                        List<marksheetmailcls> ss = new List<marksheetmailcls>();

                        foreach (DataRow dr in dt.Rows)
                        {
                            if (firstrow == true)
                            {
                                firstrow = false;
                            }
                            else
                            {
                                if (typ == 2)
                                {

                                    string schoolno = dr["SCHOOL NO."].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Halfyearly_Marksheet _Marksheet = new Tbl_Student_Halfyearly_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Percentage_Subjective = Convert.ToDecimal(dr["% OF MARK"].ToString());
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["ROLL NO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC"].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Subjective = Convert.ToDecimal(dr["TOTAL MARK"].ToString());
                                        _Marksheet.Total_Mark_Objective = total;
                                        _Marksheet.Remarks = remark;

                                        DbContext.Tbl_Student_Halfyearly_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 5; y < (dt.Columns.Count - 2); y++)
                                        {
                                            Tbl_Halfyearly_StudentMarksheetDet marksheetDet = new Tbl_Halfyearly_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;

                                            DbContext.Tbl_Halfyearly_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        // var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        //sajdgj
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtype;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }

                                }
                                else if (typ == 1)
                                {
                                    string schoolno = dr["SCHOOL NO"].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Halfyearly_Marksheet _Marksheet = new Tbl_Student_Halfyearly_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Percentage_Subjective = Convert.ToDecimal(dr["% Of Mark"].ToString());
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["ROLL NO"].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC"].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Subjective = Convert.ToDecimal(dr["TOTAL MARK(500)"].ToString());
                                        _Marksheet.Total_Mark_Objective = total;
                                        _Marksheet.Remarks = remark;
                                        DbContext.Tbl_Student_Halfyearly_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 4; y < (dt.Columns.Count - 2); y++)
                                        {
                                            Tbl_Halfyearly_StudentMarksheetDet marksheetDet = new Tbl_Halfyearly_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_Halfyearly_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        //   var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtype;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }
                                }
                                else if (typ == 3)
                                {
                                    string schoolno = dr["SCHOOL NO"].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Halfyearly_Marksheet _Marksheet = new Tbl_Student_Halfyearly_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Percentage_Subjective = Convert.ToDecimal(dr["% Of Mark"].ToString());
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["SL.NO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC."].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Subjective = Convert.ToDecimal(dr["TOTAL MARK (500)"].ToString());
                                        _Marksheet.Total_Mark_Objective = total;
                                        _Marksheet.Remarks = remark;
                                        DbContext.Tbl_Student_Halfyearly_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 4; y < (dt.Columns.Count - 2); y++)
                                        {
                                            Tbl_Halfyearly_StudentMarksheetDet marksheetDet = new Tbl_Halfyearly_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_Halfyearly_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        // var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtype;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }
                                }
                                else if (typ == 4)
                                {
                                    string schoolno = dr["SCHOOL NO"].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        var section = DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == regdlist.SectionId).FirstOrDefault().SectionName;
                                        Tbl_Student_Halfyearly_Marksheet _Marksheet = new Tbl_Student_Halfyearly_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Percentage_Subjective = Convert.ToDecimal(dr["% Of Mark"].ToString());
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["SL.NO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = section;
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Subjective = Convert.ToDecimal(dr["TOTAL MARK (500)"].ToString());
                                        _Marksheet.Total_Mark_Objective = total;
                                        _Marksheet.Remarks = remark;


                                        DbContext.Tbl_Student_Halfyearly_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 8; y < (dt.Columns.Count - 2); y++)
                                        {
                                            Tbl_Halfyearly_StudentMarksheetDet marksheetDet = new Tbl_Halfyearly_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_Halfyearly_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        // var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtype;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }
                                }
                                else if (typ == 5)
                                {
                                    string schoolno = dr["SCHOOL NO"].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        Tbl_Student_Halfyearly_Marksheet _Marksheet = new Tbl_Student_Halfyearly_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Batch = dr["BATCH"].ToString();
                                        //_Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Percentage_Subjective = Convert.ToDecimal(dr["% Of Mark"].ToString());
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["SL.NO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = dr["SEC."].ToString();
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Subjective = Convert.ToDecimal(dr["TOTAL MARK (500)"].ToString());
                                        _Marksheet.Total_Mark_Objective = total;
                                        _Marksheet.Remarks = remark;
                                        DbContext.Tbl_Student_Halfyearly_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 5; y < (dt.Columns.Count - 2); y++)
                                        {
                                            Tbl_Halfyearly_StudentMarksheetDet marksheetDet = new Tbl_Halfyearly_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;

                                            DbContext.Tbl_Halfyearly_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        // var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtype;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }
                                }
                                else if (typ == 6)
                                {
                                    string schoolno = dr["SCHOOL NO"].ToString();
                                    var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true && a.School_No == schoolno).FirstOrDefault();
                                    if (regdetlist != null)
                                    {
                                        var regdlist = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();
                                        var stdbasicdtl = DbContext.tbl_Regd_BasicDetl.Where(a => a.Regd_ID == regdetlist.Regd_ID).FirstOrDefault();

                                        var section = DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == regdlist.SectionId).FirstOrDefault().SectionName;
                                        Tbl_Student_Halfyearly_Marksheet _Marksheet = new Tbl_Student_Halfyearly_Marksheet();
                                        _Marksheet.ClassName = (cls != null && cls != 0) ? DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Class_Id == cls).FirstOrDefault().Class_Name : "";
                                        _Marksheet.InsertedId = 1;
                                        _Marksheet.Is_Active = true;
                                        _Marksheet.InsertedOn = DateTime.Now;
                                        _Marksheet.ModifiedId = 1;
                                        _Marksheet.typeId = typ;
                                        _Marksheet.Batch = dr["BATCH"].ToString();
                                        _Marksheet.SubjectCombination = dr["SUB"].ToString();
                                        _Marksheet.ModifiedOn = DateTime.Now;
                                        _Marksheet.OnlineExamType = examtype;
                                        _Marksheet.Percentage_Subjective = Convert.ToDecimal(dr["% Of Mark"].ToString());
                                        _Marksheet.RegdId = regdetlist.Regd_ID;
                                        _Marksheet.Roll_No = dr["SL.NO."].ToString();
                                        _Marksheet.School_No = regdetlist.School_No;
                                        _Marksheet.section = section;
                                        _Marksheet.Session = session;
                                        _Marksheet.StudentName = dr["NAME OF THE STUDENT"].ToString();
                                        _Marksheet.Total_Mark_Subjective = Convert.ToDecimal(dr["TOTAL MARK (500)"].ToString());
                                        _Marksheet.Total_Mark_Objective = total;
                                        _Marksheet.Remarks = remark;
                                        DbContext.Tbl_Student_Halfyearly_Marksheet.Add(_Marksheet);
                                        DbContext.SaveChanges();
                                        long markid = _Marksheet.StudentmarksheetId;
                                        for (int y = 10; y < (dt.Columns.Count - 2); y++)
                                        {
                                            Tbl_Halfyearly_StudentMarksheetDet marksheetDet = new Tbl_Halfyearly_StudentMarksheetDet();
                                            marksheetDet.Inserted_By = 1;
                                            marksheetDet.Inserted_Date = DateTime.Now;
                                            marksheetDet.IsB = false;
                                            marksheetDet.IsC = false;
                                            marksheetDet.IsH = false;
                                            marksheetDet.IsM = false;
                                            marksheetDet.Is_Active = true;
                                            marksheetDet.Modified_By = 1;
                                            marksheetDet.Modified_Date = DateTime.Now;
                                            marksheetDet.SubjectiveScore = dr[dt.Columns[y].ColumnName].ToString();
                                            marksheetDet.SubjectName = dt.Columns[y].ColumnName.Replace("\n", "");
                                            marksheetDet.StudentmarksheetId = markid;
                                            DbContext.Tbl_Halfyearly_StudentMarksheetDet.Add(marksheetDet);
                                            DbContext.SaveChanges();

                                        }
                                        // var examtypename = DbContext.OnlineExamTypes.Where(a => a.OnlineExamTypeId == examtype).FirstOrDefault().OnlineExamTypeName;
                                        marksheetmailcls marksheetmailcls = new marksheetmailcls();
                                        marksheetmailcls.customername = regdlist.Customer_Name;
                                        marksheetmailcls.examtypename = examtype;
                                        marksheetmailcls.mail = regdlist.Email;
                                        marksheetmailcls.markId = markid;
                                        marksheetmailcls.mobile = regdlist.Mobile;
                                        marksheetmailcls.zoommail = stdbasicdtl.Zoom_Mail_ID;
                                        ss.Add(marksheetmailcls);

                                    }
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                        clsmarks.marksheetmailcls = ss;
                        SendHalfYearlyEmailInBackgroundThread(clsmarks);
                        TempData["SuccessMessage"] = "Student Details Entered Successfully";
                        return RedirectToAction("UploadHalfYearlyOnlineExamReport");
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("UploadHalfYearlyOnlineExamReport");
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("UploadHalfYearlyOnlineExamReport");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("UploadHalfYearlyOnlineExamReport");
            }
        }
        public ActionResult HalfYearlysendmarksheetmail()
        {
            clsmarkslist clsmarks = new clsmarkslist();
            List<marksheetmailcls> ss = new List<marksheetmailcls>();
            List<OnlineExamReportModel> ob = (List<OnlineExamReportModel>)Session["marksheetreport"];
            var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true).ToList();

            var regdlist = DbContext.tbl_DC_Registration.ToList();
            foreach (var a in ob)
            {
                var sm = regdetlist.Where(m => m.School_No == a.School_No).FirstOrDefault();
                if (sm != null)
                {
                    var reg = regdlist.Where(m => m.Regd_ID == sm.Regd_ID).FirstOrDefault();
                    marksheetmailcls marksheetmailcls = new marksheetmailcls();
                    marksheetmailcls.customername = a.StudentName;
                    marksheetmailcls.examtypename = a.OnlineExamTypeName;
                    marksheetmailcls.mail = reg.Email;
                    marksheetmailcls.markId = a.StudentmarksheetId;
                    marksheetmailcls.mobile = reg.Mobile;
                    marksheetmailcls.zoommail = sm.Zoom_Mail_ID;
                    ss.Add(marksheetmailcls);
                }
            }
            clsmarks.marksheetmailcls = ss;
            SendHalfYearlyEmailInBackgroundThread(clsmarks);

            TempData["SuccessMessage"] = "Mail Send Successfully";
            return RedirectToAction("OnlineHalfYearlyExamReportList");
        }
        public ActionResult HalfYearlysendmarksheetmailtostudent(long id)
        {
            clsmarkslist clsmarks = new clsmarkslist();
            List<marksheetmailcls> ss = new List<marksheetmailcls>();
            var res = DbContext.Tbl_Student_Halfyearly_Marksheet.Where(a => a.StudentmarksheetId == id).FirstOrDefault();

            var regdetlist = DbContext.tbl_Regd_BasicDetl.Where(a => a.Is_Active == true).ToList();

            var regdlist = DbContext.tbl_DC_Registration.ToList();

            var sm = regdetlist.Where(m => m.School_No == res.School_No).FirstOrDefault();
            if (sm != null)
            {
                var reg = regdlist.Where(m => m.Regd_ID == sm.Regd_ID).FirstOrDefault();
                marksheetmailcls marksheetmailcls = new marksheetmailcls();
                marksheetmailcls.customername = res.StudentName;
                marksheetmailcls.examtypename = res.OnlineExamType;
                marksheetmailcls.mail = reg.Email;
                marksheetmailcls.markId = res.StudentmarksheetId;
                marksheetmailcls.mobile = reg.Mobile;
                marksheetmailcls.zoommail = sm.Zoom_Mail_ID;
                ss.Add(marksheetmailcls);
            }

            clsmarks.marksheetmailcls = ss;
            SendHalfYearlyEmailInBackgroundThread(clsmarks);

            TempData["SuccessMessage"] = "Mail Send Successfully";
            return RedirectToAction("OnlineHalfYearlyExamReportList");
        }


        public async Task SendHalfYearlyEmailInBackgroundThread(clsmarkslist mailMessage)
        {
            // Task.Run(async () => await sendHalfYearlybackgroundmail(mailMessage));
            Thread bgThread = new Thread(new ParameterizedThreadStart(sendHalfYearlybackgroundmail));
            bgThread.IsBackground = true;
            bgThread.Start(mailMessage);
        }
        public void sendHalfYearlybackgroundmail(Object obj1)
        {
            clsmarkslist objt = (clsmarkslist)obj1;
            foreach (var obj in objt.marksheetmailcls)
            {
                string url = "https://learn.odmps.org/Reports/Reports.aspx?type=School_Half_Yearly_Marksheet_Report&Id=" + obj.markId;
                //bitly b = new bitly();
                string shortlink = ToTinyURLS(url);
                if (obj.mobile != "")
                {
                    Sendsmstoemployee(obj.mobile, "Dear " + obj.customername + ", \nPlease find the Online Test Result of your exam " + obj.examtypename + ". Click on the link and save the Test Result.- " + shortlink + " \n\n Regards\n ODM Educational Group");
                }

                if (obj.mail != "")
                {
                    SendInBlueEmailManager sendInBlue = new SendInBlueEmailManager();
                    sendInBlue.SendExammail(obj.customername, obj.examtypename, shortlink, obj.mail, Server.MapPath("~/Templates/ResultSheetTemplate.html"));
                   
                }

                if (obj.zoommail != "" && obj.zoommail != null)
                {
                    SendInBlueEmailManager sendInBlue = new SendInBlueEmailManager();
                    sendInBlue.SendExammail(obj.customername, obj.examtypename, shortlink, obj.zoommail, Server.MapPath("~/Templates/ResultSheetTemplate.html"));

                }
            }
        }
        #endregion

    }
}
