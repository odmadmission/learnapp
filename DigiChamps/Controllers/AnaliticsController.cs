﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class AnaliticsController : ApiController
    {
        DigiChampsEntities db = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public class ErrorResult
        {
            public ErrorResponse Error { get; set; }
        }

        public class ErrorResponse
        {
            public string Message { get; set; }
        }

        public class SuccessResponse
        {
            public string Message { get; set; }
        }
        public class ReportModule
        {

            public int? playingTime { get; set; }
            public int? subjectId { get; set; }
            public DateTime? ReportDate { get; set; }
        }
        public class Subject
        {
            public int? subjectId { get; set; }
            public string subjectName { get; set; }
        }
        public class DoubtModule
        {
            public int TotalDoubtsAsked { get; set; }
            public int TotalDoubtsAnswerded { get; set; }
            public int TotalDoubtsRejected { get; set; }
        }
        public class TotalExam
        {
            public int TotalExamAppeared { get; set; }
            public int TotalAveragemarks { get; set; }
            public int? Topic_ID { get; set; }
            public int? Exam_ID { get; set; }
        }
        public class Success_AnaliticsModule
        {
            public List<Subject> subjectList { get; set; }

            public List<ReportModule> reportModuleList { get; set; }
            public DoubtModule DoubtModule { get; set; }
            public List<ChapterBasedAnalysis> ChapterBasedAnalysisList;

            public List<SubConceptAnalysisItem> SubConceptAnalysisItemList;
        }

        public class ChapterBasedAnalysis
        {
            public int? subjectId { get; set; }
            public int? TotalExamsAppeared { get; set; }
            public double? AverageMarks { get; set; }
            public double? Accuracy { get; set; }
            public int AverageTimePerQuestion { get; set; }


            public List<ChapterWiseTestDetails> ChapterWiseTestDetailsList { get; set; }

        }

        public class ChapterWiseTestDetails
        {
            public string chapterName { get; set; }
            public List<TestWiseAnalysis> TestWiseAnalysisList { get; set; }

        }
        public class TestWiseAnalysis
        {
            public string testName { get; set; }
            public int? correctAns { get; set; }
            public int? totalQuestions { get; set; }

        }

        public class ExamData
        {

            public int? resultID { get; set; }
            public int? chapterID { get; set; }

        }

        public class SubConceptAnalysisItem
        {
            public int? subjectId { get; set; }
            public int totalSubConceptsAppeared { get; set; }
            public int totalWeakSubConcepts { get; set; }


            public List<WeakSubConceptItem> WeakSubConceptItemList { get; set; }

        }

        public class WeakSubConceptItem
        {
            public string chapterName { get; set; }
            public string subConceptName { get; set; }
            public double Percentage { get; set; }

        }

        public class ChapterData
        {
            public string chapterName { get; set; }

            public List<TopicData> TopicDataList { get; set; }

        }


        public class TopicData
        {
            public string subconceptName { get; set; }

            public int percentage { get; set; }


        }




        public class SuccessAnalytics
        {
            public Success_AnaliticsModule Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }
        //[HttpGet]
        //public HttpResponseMessage GetAnalytics(int? regId, int classId)
        //{
        //    try
        //    {
        //        var subjectList = (from c in db.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false && x.Class_Id == classId)
        //                           select new Subject
        //                           {
        //                               subjectId = c.Subject_Id,
        //                               subjectName = c.Subject
        //                           }).ToList();


        //        int totalDoubtsAsked = db.tbl_DC_Ticket.Where(x => x.Student_ID == regId).Count();
        //        int totalDoubtsRejected = db.tbl_DC_Ticket.Where(x => x.Student_ID == regId && x.Status == "R").Count();
        //        int totalDoubtsAnswered = db.tbl_DC_Ticket.Where(x => x.Student_ID == regId && x.Status == "C").Count();

        //        DoubtModule doubtModule = new DoubtModule();
        //        doubtModule.TotalDoubtsAnswerded = totalDoubtsAnswered;
        //        doubtModule.TotalDoubtsAsked = totalDoubtsAsked;
        //        doubtModule.TotalDoubtsRejected = totalDoubtsRejected;
        //        DateTime reportDate = DateTime.Now.AddDays(-5);

        //        ////All Subjects
        //        List<ChapterBasedAnalysis> examDetailList = new List<ChapterBasedAnalysis>();
        //        ChapterBasedAnalysis examDetailsAll = new ChapterBasedAnalysis();
        //        examDetailsAll.subjectId = 0;
        //        examDetailsAll.AverageTimePerQuestion = 0;
        //        examDetailsAll.Accuracy = 0;
        //        examDetailsAll.TotalExamsAppeared = db.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regId && x.Class_Id == classId).Count();
        //        // int? CorrectAnsSum = db.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regId && x.Class_Id == classId).Sum(x => x.Total_Correct_Ans);
        //        //examDetailsAll.AverageMarks = (int)CorrectAnsSum / examDetailsAll.TotalExamsAppeared;
        //        examDetailsAll.ChapterWiseTestDetailsList = new List<ChapterWiseTestDetails>();


        //        int? correctAll = 0;

        //        int? appearedAll = 0;
        //        int? totalQuestionsAll = 0;

        //        List<int?> examTypes = GetAnalyticsExamTypes();

        //        for (int i = 0; i < subjectList.Count; i++)
        //        {

        //            ChapterBasedAnalysis chapterDetails = new ChapterBasedAnalysis();
        //            chapterDetails.subjectId = subjectList[i].subjectId;
        //            chapterDetails.AverageTimePerQuestion = 0;
        //            chapterDetails.Accuracy = 0;



        //            int? subid = subjectList[i].subjectId;

        //            chapterDetails.TotalExamsAppeared = (from a in db.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regId && x.Class_Id == classId)
        //                                                 join b in db.tbl_DC_Exam.Where(x => examTypes.Contains(x.Exam_type) &&
        //                                                     x.Subject_Id == subid && x.Class_Id == classId)
        //                                                 on a.Exam_ID equals b.Exam_ID
        //                                                 select new TotalExam
        //                                                 {
        //                                                     Exam_ID = a.Exam_ID,
        //                                                 }).Count();
        //            int? CorrectAnsSumChapter = (from a in db.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regId && x.Class_Id == classId)
        //                                         join b in db.tbl_DC_Exam.Where(x => examTypes.Contains(x.Exam_type) &&
        //                                             x.Subject_Id == subid && x.Class_Id == classId)
        //                                                 on a.Exam_ID equals b.Exam_ID
        //                                         select new TotalExam
        //                                         {
        //                                             Exam_ID = a.Total_Correct_Ans,
        //                                         }).Sum(x => x.Exam_ID);
        //            int? totalQuestions = (from a in db.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regId && x.Class_Id == classId)
        //                                   join b in db.tbl_DC_Exam.Where(x => examTypes.Contains(x.Exam_type) &&
        //                                       x.Subject_Id == subid && x.Class_Id == classId)
        //                                           on a.Exam_ID equals b.Exam_ID
        //                                   select new TotalExam
        //                                   {
        //                                       Exam_ID = a.Question_Nos,
        //                                   }).Sum(x => x.Exam_ID);


        //            int? totalAppeared = (from a in db.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regId && x.Class_Id == classId)
        //                                  join b in db.tbl_DC_Exam.Where(x => examTypes.Contains(x.Exam_type) &&
        //                                      x.Subject_Id == subid && x.Class_Id == classId)
        //                                          on a.Exam_ID equals b.Exam_ID
        //                                  select new TotalExam
        //                                  {
        //                                      Exam_ID = a.Question_Attempted,
        //                                  }).Sum(x => x.Exam_ID);


        //            correctAll = correctAll + CorrectAnsSumChapter;
        //            appearedAll = appearedAll + totalAppeared;
        //            totalQuestionsAll = totalQuestionsAll + totalQuestions;
        //            double Correct = 0;
        //            if (CorrectAnsSumChapter != 0 )
        //            {
        //                 Correct = ((double)CorrectAnsSumChapter * 100);
                       
        //            }

        //            double Appeared = 0;
        //            if (totalAppeared != 0)
        //            {
        //                 Appeared = ((double)totalAppeared * 100);
                        
        //            }
        //            if (Correct > 0 && totalQuestions > 0)
        //            chapterDetails.AverageMarks = (Correct / totalQuestions);
        //            if (Correct > 0 && Appeared > 0)
        //            chapterDetails.Accuracy = (Correct / Appeared);
                   


        //            List<tbl_DC_Chapter> cl = db.tbl_DC_Chapter.Where(x => x.Subject_Id == subid && x.Is_Active == true &&
        //                x.Is_Deleted == false).ToList();
        //            List<ChapterWiseTestDetails> ChapterWiseTestDetailsList = new List<ChapterWiseTestDetails>();

        //            for (int j = 0; j < cl.Count; j++)
        //            {
        //                ChapterWiseTestDetails cwtd = new ChapterWiseTestDetails();
        //                cwtd.chapterName = cl[j].Chapter;

        //                int chpId = cl[j].Chapter_Id;
        //                TestWiseAnalysis prtExam = (from a in db.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regId
        //                    && x.Class_Id == classId && x.Chapter_Id == chpId)
        //                                            join b in db.tbl_DC_Exam.Where(x => x.Exam_type == 1 &&
        //                    x.Subject_Id == subid && x.Class_Id == classId)
        //                                            on a.Exam_ID equals b.Exam_ID
        //                                            select new TestWiseAnalysis
        //                                            {
        //                                                testName = "PRT",
        //                                                correctAns = a.Total_Correct_Ans,
        //                                                totalQuestions = a.Question_Nos

        //                                            }).FirstOrDefault();

        //                List<TestWiseAnalysis> cbtExams = (from a in db.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regId
        //                    && x.Class_Id == classId && x.Chapter_Id == chpId)
        //                                                   join b in db.tbl_DC_Exam.Where(x => x.Exam_type == 5 &&
        //                                                 x.Subject_Id == subid && x.Class_Id == classId)
        //                                            on a.Exam_ID equals b.Exam_ID
        //                                                   select new TestWiseAnalysis
        //                                                   {
        //                                                       testName = "CBT",
        //                                                       correctAns = a.Total_Correct_Ans,
        //                                                       totalQuestions = a.Question_Nos

        //                                                   }).ToList();

        //                List<TestWiseAnalysis> allExams = new List<TestWiseAnalysis>();
        //                allExams.Add(prtExam);
        //                allExams.AddRange(cbtExams);
        //                cwtd.TestWiseAnalysisList = allExams;
        //                ChapterWiseTestDetailsList.Add(cwtd);
        //            }

        //            chapterDetails.ChapterWiseTestDetailsList = ChapterWiseTestDetailsList;
        //            examDetailsAll.ChapterWiseTestDetailsList.AddRange(ChapterWiseTestDetailsList);

        //            examDetailList.Add(chapterDetails);
        //        }


        //        double CorrectA = ((double)correctAll * 100);

        //        double AppearedA = ((double)appearedAll * 100);

        //        examDetailsAll.AverageMarks = (CorrectA / totalQuestionsAll);
        //        examDetailsAll.Accuracy = (CorrectA / AppearedA);

        //        examDetailList.Add(examDetailsAll);

        //        //int? Topics = db.tbl_DC_Exam_Result_Dtl.Where(x => x.Is_Active == true && x.Is_Deleted == true).Sum(x => x.Topic_ID);
        //        //int TotalExamAppeared = db.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regId).Count();
        //        //int TotalAveragemarks = db.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regId).Count();
        //        //TotalExam totalExam = new TotalExam();


        //        //subconcept wise analysis




        //        var obj = new SuccessAnalytics
        //        {


        //            Success = new Success_AnaliticsModule
        //            {
        //                subjectList = subjectList,

        //                reportModuleList = (from a in db.tbl_DC_UserReport.Where(x => x.Regd_ID == regId && x.Report_Date >= reportDate)
        //                                    join b in db.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
        //                                    on a.Module_ID equals b.Module_ID
        //                                    select new ReportModule
        //                                    {
        //                                        playingTime = a.Video_Time,
        //                                        ReportDate = a.Report_Date,
        //                                        subjectId = b.Subject_Id
        //                                    }).ToList(),
        //                DoubtModule = doubtModule,
        //                ChapterBasedAnalysisList = examDetailList,
        //                SubConceptAnalysisItemList = GetSubConceptAnalysisItemList(regId, classId),

        //            },
        //        };
        //        return Request.CreateResponse(HttpStatusCode.OK, obj);
        //    }
        //    catch (Exception e)
        //    {

        //        throw e;
        //        //var obj = new Errorresult
        //        //{
        //        //    Error = new Errorresponse
        //        //    {
        //        //        Message = "Something went wrong."+e;
        //        //    }
        //        //};
        //        //return Request.CreateResponse(HttpStatusCode.OK, obj);
        //    }
        //}



        //private List<SubConceptAnalysisItem> GetSubConceptAnalysisItemList(int? regId, int classId)
        //{

        //    List<SubConceptAnalysisItem> list = new List<SubConceptAnalysisItem>();
        //    var subjectList = (from c in db.tbl_DC_Subject.
        //                           Where(x => x.Is_Active == true
        //                               && x.Is_Deleted == false && x.Class_Id == classId)
        //                       select new Subject
        //                       {
        //                           subjectId = c.Subject_Id,
        //                           subjectName = c.Subject
        //                       }).ToList();
        //    List<int?> examTypes = GetAnalyticsExamTypes();

        //    for (int i = 0; i < subjectList.Count; i++)
        //    {
        //        SubConceptAnalysisItem subConceptAnalysisItem = new SubConceptAnalysisItem();

        //        subConceptAnalysisItem.subjectId = subjectList[i].subjectId;
        //        int? subid = subjectList[i].subjectId;


        //        List<ExamData> examList = (from a in db.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regId && x.Class_Id == classId)
        //                                   join b in db.tbl_DC_Exam.Where(x => examTypes.Contains(x.Exam_type) &&
        //                                       x.Subject_Id == subid && x.Class_Id == classId)
        //                                   on a.Exam_ID equals b.Exam_ID
        //                                   select new ExamData
        //                                   {
        //                                       chapterID = a.Chapter_Id,
        //                                       resultID = a.Result_ID,
        //                                   }).ToList();
        //        List<int?> resultIdList = new List<int?>();

        //        List<int?> chapterIdList = new List<int?>();

        //        for (int j = 0; j < examList.Count; j++)
        //        {
        //            resultIdList.Add(examList[j].resultID);

        //            if (!chapterIdList.Contains(examList[j].chapterID))
        //                chapterIdList.Add(examList[j].chapterID);


        //        }

        //        List<WeakSubConceptItem> WeakSubConceptItemList = new List<WeakSubConceptItem>();

        //        for (int k = 0; k < chapterIdList.Count; k++)
        //        {
        //            int? chapterId = chapterIdList[k];
        //            List<int?> topicIdList = db.tbl_DC_Exam_Result_Dtl.
        //                Where(x => resultIdList.Contains(x.Result_ID) && x.Class_Id == classId && x.Chapter_Id == chapterId).
        //                Select(x => x.Topic_ID).Distinct().ToList();

        //            for (int m = 0; m < topicIdList.Count; m++)
        //            {

        //                int? topic = topicIdList[m];
        //                int totalQuestions = db.tbl_DC_Exam_Result_Dtl.
        //                Where(x => resultIdList.Contains(x.Result_ID) && x.Class_Id == classId
        //                    && x.Chapter_Id == chapterId && x.Topic_ID == topic).Count();

        //                int correctAnsQuestions = db.tbl_DC_Exam_Result_Dtl.
        //                Where(x => resultIdList.Contains(x.Result_ID) && x.Class_Id == classId
        //                    && x.Chapter_Id == chapterId && x.Topic_ID == topic && x.Is_Correct == true).Count();

        //                double percentage = ((correctAnsQuestions * 100) / totalQuestions);
        //                if (percentage < 35)
        //                {
        //                    WeakSubConceptItem ws = new WeakSubConceptItem();

        //                    ws.chapterName = db.tbl_DC_Chapter.Where(x => x.Chapter_Id == chapterId).Select(x => x.Chapter).FirstOrDefault();
        //                    ws.subConceptName = db.tbl_DC_Topic.Where(x => x.Topic_ID == topic).Select(x => x.Topic_Name).FirstOrDefault();
        //                    ws.Percentage = percentage;

        //                    WeakSubConceptItemList.Add(ws);
        //                }

        //            }


        //        }


        //        //List<int?> l = db.tbl_DC_Exam_Result_Dtl.Where(x => resultIdList.Contains(x.Result_ID) && x.Class_Id == classId).
        //        //    Select(x => x.Topic_ID).ToList();
        //        subConceptAnalysisItem.totalSubConceptsAppeared =
        //           db.tbl_DC_Exam_Result_Dtl.Where(x => resultIdList.Contains(x.Result_ID) && x.Class_Id == classId).
        //            Select(x => x.Topic_ID).Distinct().Count();
        //        subConceptAnalysisItem.totalWeakSubConcepts = WeakSubConceptItemList.Count;
        //        //var data=
        //        //   db.tbl_DC_Exam_Result_Dtl.Where(x => resultIdList.Contains(x.Result_ID) && x.Class_Id == classId).

        //        //    GroupBy(x => x.Topic_ID).Select(x => new
        //        //    {
        //        //        Total = x.Count(),
        //        //        TopicID = x.Key
        //        //    }).ToList();

        //        subConceptAnalysisItem.WeakSubConceptItemList = WeakSubConceptItemList;
        //        list.Add(subConceptAnalysisItem);

        //    }
        //    SubConceptAnalysisItem subConceptAnalysisItemAll = new SubConceptAnalysisItem();
        //    List<WeakSubConceptItem> WeakSubConceptItemListAll = new List<WeakSubConceptItem>();
        //    subConceptAnalysisItemAll.subjectId = 0;
        //    for (int i = 0; i < list.Count; i++)
        //    {
        //        subConceptAnalysisItemAll.totalSubConceptsAppeared = subConceptAnalysisItemAll.totalSubConceptsAppeared + list[i].totalSubConceptsAppeared;
        //        subConceptAnalysisItemAll.totalWeakSubConcepts = subConceptAnalysisItemAll.totalWeakSubConcepts + list[i].totalWeakSubConcepts;

        //        if (list[i].WeakSubConceptItemList != null)
        //            WeakSubConceptItemListAll.AddRange(list[i].WeakSubConceptItemList);
        //    }


        //    subConceptAnalysisItemAll.totalWeakSubConcepts = WeakSubConceptItemListAll.Count;
        //    subConceptAnalysisItemAll.WeakSubConceptItemList = WeakSubConceptItemListAll;
        //    list.Add(subConceptAnalysisItemAll);
        //    return list;
        //}

        //private List<int?> GetAnalyticsExamTypes()
        //{
        //    List<int?> examTypes = new List<int?>();
        //    examTypes.Add(1);
        //    examTypes.Add(5);
        //    return examTypes;
        //}


      
# region ---------------------------Personalized User Analytics Web API-------------------------------------------------------



        public class SuccessData
        {
            public PersonalizedUserAnalytics success { set; get; }
        }
        public class PersonalizedUserAnalytics
        {
            public List<UserReportAnalytics> userReportAnalyticsList { set; get; }
            public List<TestAnalysisAnalytics> testAnalysisAnalyticsList { set; get; }
            public List<SubConceptAnalytics> subConceptAnalyticsList { set; get; }
            public DoubtsAnalytics doubtsAnalytics { set; get; }
            public List<String> subjectList { set; get; }

        }
       
        public class DoubtsAnalytics
        {
            public int? DoubtsAsked { set; get; }
            public int? DoubtsAnswered { set; get; }
            public int? DoubtsRejected { set; get; }

            public int? SubjectId { set; get; }

            public string Subject { set; get; }
        }
        public class TestAnalysisAnalytics
        {
            public int? TotalExamsAppeared { set; get; }
            public int? AverageMarks { set; get; }
            public int? Accuracy { set; get; }

            public int? SubjectId { set; get; }

            public string Subject { set; get; }
        }
        public class UserReportAnalytics
        {
            public DateTime? ReportDate;
            public int? TotalTime;

            public int? SubjectId { set; get; }

            public string Subject { set; get; }
        }
        public class SubConceptAnalytics
        {
            public int? SubConceptsAppeared { set; get; }
            public int? TotalWeakSubConcepts { set; get; }

            public int? SubjectId { set; get; }

            public string Subject { set; get; }


        }

        
        
        [HttpGet]
        public HttpResponseMessage GetPersonalizedUserAnalytics(int? regId, int classId)
      {

          SuccessData successData = new SuccessData();

          PersonalizedUserAnalytics pua = new PersonalizedUserAnalytics();
          List<UserReportAnalytics> userList = new List<UserReportAnalytics>();
          List<TestAnalysisAnalytics> testList = new List<TestAnalysisAnalytics>();
          List<SubConceptAnalytics> subList = new List<SubConceptAnalytics>();


          List<String> subjectListTotal = new List<String>();
        

         try{

             List<String> subjectList=db.tbl_DC_Subject.Where(x=>x.Class_Id==classId).Select(x=>x.Subject).ToList();

             subjectListTotal.Add("All");
             subjectListTotal.AddRange(subjectList);
             
             var userReportAnalyticsSBW = db.UserReportAnalytics(regId,
                 DateTime.Now.AddDays(-30), DateTime.Now.Date).ToList();

             var userReportAnalyticsAll = db.UserReportAnalyticsAll(regId,
                 DateTime.Now.AddDays(-30), DateTime.Now.Date).ToList();
             if (userReportAnalyticsAll != null)
             {

                 for (int i = 0; i < userReportAnalyticsAll.Count; i++)
                 {
                     UserReportAnalytics u = new UserReportAnalytics();
                     u.ReportDate = userReportAnalyticsAll[i].ReportDate;
                     u.Subject = userReportAnalyticsAll[i].Subject;
                     u.SubjectId = userReportAnalyticsAll[i].Subject_Id;
                     u.TotalTime = userReportAnalyticsAll[i].TotalTime;

                     userList.Add(u);
                 }
             }
             if (userReportAnalyticsSBW != null)
             {
                 for (int i = 0; i < userReportAnalyticsSBW.Count; i++)
                 {
                     UserReportAnalytics u = new UserReportAnalytics();
                     u.ReportDate = userReportAnalyticsSBW[i].ReportDate;
                     u.Subject = userReportAnalyticsSBW[i].Subject;
                     u.SubjectId = userReportAnalyticsSBW[i].Subject_Id;
                     u.TotalTime = userReportAnalyticsSBW[i].TotalTime;

                     userList.Add(u);
                 }
             }
             

             var testAnalyticsSBW = db.SP_TestAnalysisAnalytics(regId).ToList();

             var testAnalyticsAll = db.SP_TestAnalysisAnalyticsAll(regId).ToList();


             if (testAnalyticsAll != null)
             {

                 for (int i = 0; i < testAnalyticsAll.Count; i++)
                 {
                     TestAnalysisAnalytics u = new TestAnalysisAnalytics();
                     u.TotalExamsAppeared = testAnalyticsAll[i].TotalExamAppeared;
                     u.Subject = testAnalyticsAll[i].Subject;
                     u.SubjectId = testAnalyticsAll[i].Subject_Id;
                     u.Accuracy = testAnalyticsAll[i].Accuracy;
                     u.AverageMarks = testAnalyticsAll[i].AverageMarks;

                     testList.Add(u);
                 }
             }
             if (testAnalyticsSBW != null)
             {
                 for (int i = 0; i < testAnalyticsSBW.Count; i++)
                 {
                     TestAnalysisAnalytics u = new TestAnalysisAnalytics();
                     u.TotalExamsAppeared = testAnalyticsSBW[i].TotalExamAppeared;
                     u.Subject = testAnalyticsSBW[i].Subject;
                     u.SubjectId = testAnalyticsSBW[i].Subject_Id;
                     u.Accuracy = testAnalyticsSBW[i].Accuracy;
                     u.AverageMarks = testAnalyticsSBW[i].AverageMarks;

                     testList.Add(u);
                 }
             }

             var subConceptAnalyticsSBW = db.SP_SubConceptAnalytics(regId, 35).ToList();

             var subConceptAnalyticsAll = db.SP_SubConceptAnalyticsAll(regId, 35).ToList();



             if (subConceptAnalyticsAll != null)
             {

                 for (int i = 0; i < subConceptAnalyticsAll.Count; i++)
                 {
                     SubConceptAnalytics u = new SubConceptAnalytics();
                     u.SubConceptsAppeared = subConceptAnalyticsAll[i].SubTopicsAppeared;
                     u.Subject = subConceptAnalyticsAll[i].Subject;
                     u.SubjectId = subConceptAnalyticsAll[i].Subject_Id;
                     u.TotalWeakSubConcepts = subConceptAnalyticsAll[i].TotalWeakSubConcepts;


                     subList.Add(u);
                 }
             }
             if (subConceptAnalyticsSBW != null)
             {
                 for (int i = 0; i < subConceptAnalyticsSBW.Count; i++)
                 {
                     SubConceptAnalytics u = new SubConceptAnalytics();
                     u.SubConceptsAppeared = subConceptAnalyticsSBW[i].SubTopicsAppeared;
                     u.Subject = subConceptAnalyticsSBW[i].Subject;
                     u.SubjectId = subConceptAnalyticsSBW[i].Subject_Id;
                     u.TotalWeakSubConcepts = subConceptAnalyticsSBW[i].TotalWeakSubConcepts;

                     subList.Add(u);
                 }
             }

             var doubtsAnalytics = db.DoubtsAnalytics(regId).FirstOrDefault();
             DoubtsAnalytics doubt = new DoubtsAnalytics();
             if (doubtsAnalytics != null)
             {


                 doubt.DoubtsAnswered = doubtsAnalytics.DoubtsAnswered;
                 doubt.DoubtsAsked = doubtsAnalytics.DoubtsAsked;
                 doubt.DoubtsRejected = doubtsAnalytics.DoubtsRejected;


                    
             }

             pua.doubtsAnalytics = doubt;
             pua.subConceptAnalyticsList =subList ;
             pua.testAnalysisAnalyticsList = testList;
             pua.userReportAnalyticsList = userList;
             pua.subjectList = subjectListTotal;
             successData.success = pua;

             return Request.CreateResponse(HttpStatusCode.OK, successData);

          }
          catch (Exception e)
            {

                throw e;
               
                
            }


         
     }


        public class SubConceptObjectList
        {
            public List<SubConceptObject> success { set; get; }
            public List<String> subjectList { set; get; }
        }
        public class SubConceptObject
        {
            public int? SubjectId { set;get;}

            public string Subject { set;get;}
            public List<ChapterObject> ChapterList { set; get; }
            

        }

        public class ChapterObject
        {
            public int? ChapterId { set; get; }

            public string Chapter { set; get; }

            public int? PRT_TotalQnos { set; get; }
            public int? CBT_TotalQnos { set; get; }

            public int? PRT_TotalAns { set; get; }

            public int? CBT_TotalAns { set; get; }


        }

        [HttpGet]
        public HttpResponseMessage GetSubConceptAnalytics(int? regId,int classId)
        {

            SubConceptObjectList list = new SubConceptObjectList();
            List<SubConceptObject> subList = new List<SubConceptObject>();
            List<String> subjectListTotal = new List<String>();


            try
            {

                List<String> subjectList = db.tbl_DC_Subject.Where(x => x.Class_Id == classId).Select(x => x.Subject).ToList();

                subjectListTotal.Add("All");
                subjectListTotal.AddRange(subjectList);

                var userReportAnalyticsSBW = db.SP_SubConceptCompleteAnalytics(regId).ToList();

                if(userReportAnalyticsSBW!=null)
                {
                    for (int i = 0; i < userReportAnalyticsSBW.Count;i++)
                    {
                        SubConceptObject so = new SubConceptObject();
                        so.SubjectId = userReportAnalyticsSBW[i].Subject_Id;
                        so.Subject = userReportAnalyticsSBW[i].Subject;
                        bool status = false;
                        for (int j = 0;j < subList.Count; j++)
                        {
                            if(subList[j].SubjectId==so.SubjectId)
                            {
                                status = true;
                            }

                        }
                        if (!status)
                        {
                            so.ChapterList = new List<ChapterObject>();
                            subList.Add(so);

                        }
                        

                    }
                    for (int i = 0; i < userReportAnalyticsSBW.Count; i++)
                    {
                        ChapterObject so = new ChapterObject();
                        so.ChapterId=userReportAnalyticsSBW[i].Chapter_Id;
                         so.Chapter=userReportAnalyticsSBW[i].Chapter;
                         so.PRT_TotalQnos=userReportAnalyticsSBW[i].PRT_Question_nos;
                        so.PRT_TotalAns=userReportAnalyticsSBW[i].PRT_Correct_Ans;

                        so.CBT_TotalQnos=userReportAnalyticsSBW[i].CBT_Question_nos;
                        so.CBT_TotalAns=userReportAnalyticsSBW[i].CBT_Correct_Ans;

                        for (int j = 0; j < subList.Count;j++ )
                        {
                            if(subList[j].SubjectId==userReportAnalyticsSBW[i].Subject_Id)
                            {
                                subList[j].ChapterList.Add(so);
                            }
                        }


                    }

                    SubConceptObject soAll = new SubConceptObject();
                    soAll.SubjectId = 0;
                    soAll.Subject = "All";

                    soAll.ChapterList = new List<ChapterObject>();

                    for (int j = 0; j < subList.Count; j++)
                    {
                        soAll.ChapterList.AddRange(subList[j].ChapterList); 
                    }
                    subList.Add(soAll);
                }


                list.success = subList;
                list.subjectList = subjectListTotal;

                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            catch (Exception e)
            {

                throw e;


            }



        }
        public class WeakSubConceptObjectList
        {
            public List<WeakSubConceptObject> success { set; get; }
            public List<String> subjectList { set; get; }

        }
        public class WeakSubConceptObject
        {
            public int? SubjectId { set; get; }

            public string Subject { set; get; }
            public List<WeakChapterObject> ChapterList { set; get; }


        }
        public class WeakChapterObject
        {
            public int? ChapterId { set; get; }

            public string Chapter { set; get; }

            public int? SubConceptId { set; get; }

            public string SubConcept { set; get; }

            public int? Accuracy { set; get; }
            public int? Rank { get; set; }
           


        }
        [HttpGet]
        public HttpResponseMessage GetWeakSubConceptAnalytics(int? regId,int classId) //, int? eid
        {

            WeakSubConceptObjectList list = new WeakSubConceptObjectList();
            List<WeakSubConceptObject> subList = new List<WeakSubConceptObject>();

            List<String> subjectListTotal = new List<String>();


            try
            {

                List<String> subjectList = db.tbl_DC_Subject.Where(x => x.Class_Id == classId).Select(x => x.Subject).ToList();

                subjectListTotal.Add("All");
                subjectListTotal.AddRange(subjectList);
                //int? chapterId = db.tbl_DC_Exam_Result_Dtl.Where(x => x.Result_ID == eid).Select(x => x.Chapter_Id).FirstOrDefault();
                //var startegic = db.SP_DC_Startegic_Report(eid).ToList();

                //var examresult_cal = db.SP_DC_Examresultcalulation(regId, eid).ToList();

                //var question = db.SP_DC_Getallquestion_Appeard(eid);

                //var get_Level = db.SP_DC_Get_Power_Result(eid).ToList();

                //var get_result_data = db.tbl_DC_Exam_Result.Where(x => x.Result_ID == eid).FirstOrDefault();

                //DateTime sdt = Convert.ToDateTime(get_result_data.StartTime);

                //DateTime edt = Convert.ToDateTime(get_result_data.EndTime);

                //var diff = db.DifficultyBasisTestStatistics2(eid);
                //var subConcept = db.SubConceptWiseTestStatistics2(eid);

                //var hours = (edt - sdt).TotalMilliseconds;
                //var Leader = db.SP_DC_lead(eid).ToList();
                //int count = Leader.FindIndex(X => X.Regd_ID == regId);


                //int accuracy = Convert.ToInt32(Convert.ToDecimal(examresult_cal.FirstOrDefault().Total_Correct_Ans)
                //    / Convert.ToDecimal(examresult_cal.FirstOrDefault().Question_Nos) * 100);
                //int myRank = 0;
                //if (accuracy > 0 && accuracy <= 35)
                //{
                //    myRank = 1;
                //}
                //else
                //    if (accuracy > 35 && accuracy <= 60)
                //    {
                //        myRank = 2;
                //    }
                //    else
                //        if (accuracy > 60 && accuracy <= 75)
                //        {
                //            myRank = 3;
                //        }
                //        else
                //            if (accuracy > 75 && accuracy <= 90)
                //            {
                //                myRank = 4;
                //            }
                //            else
                //                if (accuracy > 90 && accuracy <= 100)
                //                {
                //                    myRank = 5;
                //                }

                var userReportAnalyticsSBW = db.SP_SubConceptAnalyticsComplete(regId, 35).ToList();

                if (userReportAnalyticsSBW != null)
                {
                    for (int i = 0; i < userReportAnalyticsSBW.Count; i++)
                    {
                        WeakSubConceptObject so = new WeakSubConceptObject();
                        so.SubjectId = userReportAnalyticsSBW[i].Subject_Id;
                        so.Subject = userReportAnalyticsSBW[i].Subject;
                        bool status = false;
                        for (int j = 0; j < subList.Count; j++)
                        {
                            if (subList[j].SubjectId == so.SubjectId)
                            {
                                status = true;
                            }

                        }
                        if (!status)
                        {
                            so.ChapterList = new List<WeakChapterObject>();
                            subList.Add(so);

                        }


                    }
                    for (int i = 0; i < userReportAnalyticsSBW.Count; i++)
                    {
                        WeakChapterObject so = new WeakChapterObject();
                        so.ChapterId = userReportAnalyticsSBW[i].Chapter_Id;
                        so.Chapter = userReportAnalyticsSBW[i].Chapter;
                        so.Accuracy = userReportAnalyticsSBW[i].Accuracy;
                        so.SubConcept = userReportAnalyticsSBW[i].Topic_Name;
                        so.SubConceptId = userReportAnalyticsSBW[i].Topic_ID;
                        //so.Rank = db.tbl_DC_Exam_Result.Where(x => x.Regd_ID == regId && x.Result_ID == eid).Count();
                     
                        for (int j = 0; j < subList.Count; j++)
                        {
                            if (subList[j].SubjectId == userReportAnalyticsSBW[i].Subject_Id)
                            {
                                subList[j].ChapterList.Add(so);
                            }
                        }


                    }

                    WeakSubConceptObject soAll = new WeakSubConceptObject();
                    soAll.SubjectId = 0;
                    soAll.Subject = "All";

                    soAll.ChapterList = new List<WeakChapterObject>();

                    for (int j = 0; j < subList.Count; j++)
                    {
                        soAll.ChapterList.AddRange(subList[j].ChapterList);
                    }
                    subList.Add(soAll);
                }


                list.success = subList;
                list.subjectList = subjectListTotal;

                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            catch (Exception e)
            {

                throw e;


            }



        }


    }

#endregion



}
