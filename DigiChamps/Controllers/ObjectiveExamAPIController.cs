﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DigiChamps.Controllers
{
    public class ObjectiveExamAPIController : ApiController
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();
        [HttpGet]
        public HttpResponseMessage GetNegativeMarks()
        {
            try
            {
                var NegativeMarkList = DbContext.Negativemarks.Where(a => a.Is_Active == true).Select(a => new { a.NegativeMarkID, a.NegativeMark1 }).ToList();

                return Request.CreateResponse(HttpStatusCode.OK, new { negativeMarkList = NegativeMarkList });
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
            }
        }

        public DateTime Getdate(string date)
        {
            DateTime dt = Convert.ToDateTime(date);
            return dt;
        }

        //[HttpGet]
        //public HttpResponseMessage GetOnlineObjectiveExamList(int studentId, long? examTypeId, int? subjectId)
        //{
        //    try
        //    {
        //        var student = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == studentId).FirstOrDefault();
        //        var student_dtl = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Regd_ID == studentId).FirstOrDefault();
        //        OnlineExamObjectiveResponseModel model = new OnlineExamObjectiveResponseModel();

        //        var onlineExamList = DbContext.OnlineObjectiveExams.Where(a => a.ClassId == student_dtl.Class_ID && a.SchoolId == student.SchoolId && a.Is_Active == true).ToList();
        //        var subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
        //        var examTypeList = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();

        //        var classname = DbContext.tbl_DC_Class.Where(a => a.Class_Id == student_dtl.Class_ID).FirstOrDefault().Class_Name;
        //        var sectionname = DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == student.SectionId).FirstOrDefault().SectionName;
        //        var onlineExamDate = DbContext.OnlineExamDates.Where(a => a.Is_Active == true).ToList();
        //        //  var remarkList = DbContext.OnlineExamAnswerSheetRemarks.Where(a => a.Is_Active == true).ToList();
        //        var examTime = DbContext.OnlineExamTimes.Where(a => a.Is_Active == true).ToList();
        //        var answerSheet = DbContext.OnlineObjectiveExamStudents.Where(a => a.Is_Active == true).ToList();
        //        //var teacherList = DbContext.Teachers.Where(a => a.Active == 1).ToList();

        //        var res1 = (from a in onlineExamList
        //                    join b in subjectList on a.SubjectId equals b.Subject_Id
        //                    join c in examTypeList on a.OnlineExamTypeId equals c.OnlineExamTypeId
        //                    join f in onlineExamDate on a.OnlineExamDateId equals f.OnlineExamDateId
        //                    join g in examTime on a.OnlineExamTimeId equals g.OnlineExamTimeId
        //                    select new OnlineObjectiveExamAPIModel
        //                    {
        //                        ID = a.OnlineObjectiveExamId,
        //                        SubjectName = b.Subject,
        //                        ExampTypeId = a.OnlineExamTypeId.Value,
        //                        SubjectId = a.SubjectId.Value,
        //                        ExampTypeName = c.OnlineExamTypeName,
        //                        ClassName = classname,
        //                        SectionName = sectionname,
        //                        OnlineExamDates = f.OnlineExamDate1,
        //                        StartTime = g.Start_Time,
        //                        StartTimeName = g.Start_Time_Name,
        //                        EndTime = g.End_Time,
        //                        EndTimeName = g.End_Time_Name,
        //                        Instruction = a.Exam_Instruction,
        //                        // Pdf = @"/Images/OnlineExam/" + a.PDF
        //                    }).ToList();









        //        if (examTypeId != null && examTypeId != 0)
        //        {
        //            res1 = res1.Where(a => a.ExampTypeId == examTypeId).ToList();
        //        }
        //        if (subjectId != null && subjectId != 0)
        //        {
        //            res1 = res1.Where(a => a.SubjectId == subjectId).ToList();
        //        }


        //        #region upcoming list
        //        var onlineexamanswer = answerSheet.Where(a => a.StudentId == studentId).Select(a => a.OnlineObjectiveExamId).Distinct().ToList();

        //        var upcoming = res1.Where(a => Getdate(a.OnlineExamDates.Value.Date.ToString("dd/MM/yyyy") + " " + DateTime.ParseExact(a.EndTime.Value.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt")) >= DateTime.Now && !onlineexamanswer.Contains(a.ID)).ToList();

        //        model.UpcommingList = upcoming.OrderBy(a => (Getdate(a.OnlineExamDates.Value.Date.ToString("dd/MM/yyyy") + " " + DateTime.ParseExact(a.StartTime.Value.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt")))).ToList();
        //        #endregion
        //        #region Given Exam list
        //        var onlineexamanswer1 = answerSheet.Where(a => a.StudentId == studentId).ToList();
        //        var given = (from a in res1
        //                     join b in onlineexamanswer1 on a.ID equals b.OnlineObjectiveExamId
        //                     orderby b.InsertedOn descending
        //                     //where b.IsSubmitted == true && b.IsVerified == false
        //                     select a).Distinct().ToList();
        //        model.GivenExamList = given;
        //        #endregion
        //        #region Result list

        //        var results = (from a in res1
        //                           //where b.IsSubmitted == true && b.IsVerified == true
        //                       select new OnlineObjectiveExamAPIModel
        //                       {
        //                           ID = a.ID,
        //                           SubjectName = a.SubjectName,
        //                           ExampTypeName = a.ExampTypeName,
        //                           ClassName = a.ClassName,
        //                           SectionName = a.SectionName,
        //                           OnlineExamDates = a.OnlineExamDates,
        //                           StartTime = a.StartTime,
        //                           StartTimeName = a.StartTimeName,
        //                           EndTime = a.EndTime,
        //                           EndTimeName = a.EndTimeName,
        //                           Score = Getscore(a.ID, onlineexamanswer1, onlineExamList.Where(j => j.OnlineObjectiveExamId == a.ID).FirstOrDefault()),

        //                       }).ToList();
        //        model.Result = results;

        //        #endregion

        //        return Request.CreateResponse(HttpStatusCode.OK, new { Response = model });
        //    }
        //    catch (Exception e1)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
        //    }
        //}
        public decimal? Getscore(List<OnlineObjectiveExamStudent> examStudents, OnlineObjectiveExam exam)
        {
            // var answers = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();
            var examques = DbContext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true && a.OnlineObjectiveExamId == exam.OnlineObjectiveExamId).ToList();
            var Totalmark = examques.Sum(a => a.Mark);

            var res = (from a in examStudents
                       join b in examques on a.OnlineObjectiveExamQuestionId equals b.OnlineObjectiveExamQuestionId
                       where a.IsAttempt == true
                       select new
                       {
                           iscorrect = a.IsCorrect,
                           score = b.Mark
                       }).ToList();
            var correct = res.Where(a => a.iscorrect == true).ToList().Sum(a => a.score);
            var wrong = res.Where(a => a.iscorrect == false).ToList().Count();
            if (exam.IsNagativeMarking == true)
            {
                var negativelist = DbContext.Negativemarks.Where(a => a.NegativeMarkID == exam.NagativeMarkingId).FirstOrDefault();
                var negvmark = negativelist.NegativeMark1 * wrong;
                var scoreres = correct - negvmark;
                return scoreres;
            }
            else
            {
                return correct;
            }
        }
        public class successexamstartcls
        {
            public examstartcls success { get; set; }
        }

        public class examstartcls
        {
            public List<examstart_datacls> ExamData { get; set; }

            public string Start_Time { get; set; }

            public string End_Time { get; set; }

            public int Result_ID { get; set; }
        }

        public class examstart_datacls
        {
            public Nullable<long> RowID { get; set; }
            public Nullable<int> question_id { get; set; }
            public Nullable<int> Board_Id { get; set; }
            public Nullable<int> Class_Id { get; set; }
            public Nullable<int> Subject_Id { get; set; }
            public Nullable<int> ch_id { get; set; }
            public Nullable<int> topicId { get; set; }
            public Nullable<int> power_id { get; set; }
            public string question { get; set; }
            public string Qustion_Desc { get; set; }
            public List<Question_optionscls> Options { get; set; }
            public List<Question_imagecls> Image { get; set; }
            public string Question_desc_Image { get; set; }
        }

        public class Question_optionscls
        {
            public int Answer_ID { get; set; }
            public Nullable<int> Question_ID { get; set; }
            public string Option_Desc { get; set; }
            public string Option_Image { get; set; }
            public string Answer_desc { get; set; }
            public string Answer_Image { get; set; }
            public Nullable<bool> Is_Answer { get; set; }
        }

        public class Question_imagecls
        {
            public string Question_desc_Image { get; set; }
        }

        [HttpGet]
        public HttpResponseMessage GetExamDetailsList(long examid)
        {
            try
            {
                var OnlineObjectiveExamQuestionList = DbContext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true && a.OnlineObjectiveExamId == examid).ToList();

                var OnlineObjectiveExamAnswerList = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();

                var OnlineObjectiveExamList = DbContext.OnlineObjectiveExams.Where(a => a.Is_Active == true && a.OnlineObjectiveExamId == examid).ToList();
                var OnlineExamDateList = DbContext.OnlineExamDates.Where(a => a.Is_Active == true).ToList();
                var OnlineExamTimesList = DbContext.OnlineExamTimes.Where(a => a.Is_Active == true).ToList();
                var OnlineExamTypessList = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();
                var StudentList = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var SubjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();

                var res = (from b in OnlineObjectiveExamList 
                           join c in OnlineExamDateList on b.OnlineExamDateId equals c.OnlineExamDateId
                           join d in OnlineExamTimesList on b.OnlineExamTimeId equals d.OnlineExamTimeId
                           join e in OnlineExamTypessList on b.OnlineExamTypeId equals e.OnlineExamTypeId
                           join f in SubjectList on b.SubjectId equals f.Subject_Id
                           select new ObjectiveExamAPIModel
                           {
                               onlineObjectiveExamId=b.OnlineObjectiveExamId,
                               onlineExamDate = c.OnlineExamDate1,
                               onlineExamDateName = c.OnlineExamDateName,
                               startTime = d.Start_Time,
                               startTimeName = d.Start_Time_Name,
                               endTime = d.End_Time,
                               endTimeName = d.End_Time_Name,
                               onlineExamTypeName = e.OnlineExamTypeName,
                               subjectName = f.Subject,
                               onlineObjectivequesList=(from m in OnlineObjectiveExamQuestionList
                                                        where m.OnlineObjectiveExamId==b.OnlineObjectiveExamId
                                                        select new objectiveexamQuescls {
                                                            mark=m.Mark,
                                                            onlineObjectiveExamId=m.OnlineObjectiveExamId,
                                                            onlineObjectiveExamQuestionId=m.OnlineObjectiveExamQuestionId,
                                                            question=m.Question,
                                                            questionImage=m.QuestionImage!=null?("/Images/ObjectiveQuestion/"+m.QuestionImage):null,
                                                            answers= (from k in OnlineObjectiveExamAnswerList
                                                                      where k.OnlineObjectiveExamQuestionId == m.OnlineObjectiveExamQuestionId
                                                                      select new OnlineObjectiveExamAnswerModel
                                                                      {
                                                                          onlineObjectiveExamAnswerId = k.OnlineObjectiveExamAnswerId,
                                                                          option = k.Option,
                                                                          optionImage = k.OptionImage != null ? ("/Images/ObjectiveOption/" + k.OptionImage) : null,
                                                                          IsAnswer = k.IsCorrect.Value

                                                                      }).ToList()
                                                        }).ToList()
                             

                           }).FirstOrDefault();

                var counter = 1;
                var obj = new successexamstartcls
                {
                    success = new examstartcls
                    {
                        Start_Time = res.startTime.ToString(),
                        End_Time = res.endTime.ToString(),
                        Result_ID =Convert.ToInt32(res.onlineObjectiveExamId.Value),
                        ExamData = (from c in res.onlineObjectivequesList
                                    select new examstart_datacls
                                    {

                                        RowID = counter++,
                                        question_id =Convert.ToInt32(c.onlineObjectiveExamQuestionId),
                                       
                                        question = c.question,
                                        Qustion_Desc = "",
                                        Question_desc_Image = c.questionImage,

                                        Options =c.answers.Count>0 ? (from o in c.answers
                                                   select new Question_optionscls
                                                   {
                                                       Answer_ID = Convert.ToInt32(o.onlineObjectiveExamAnswerId),
                                                       Question_ID = Convert.ToInt32(c.onlineObjectiveExamQuestionId),
                                                       Option_Desc = o.option,
                                                       Option_Image = o.optionImage,
                                                       Answer_desc = c.answers.Where(m => m.IsAnswer == true).FirstOrDefault().option,
                                                       Answer_Image = c.answers.Where(m => m.IsAnswer == true).FirstOrDefault().optionImage,
                                                       Is_Answer = o.IsAnswer
                                                   }).ToList():null
                                    }).ToList()
                    }
                };

                return Request.CreateResponse(HttpStatusCode.OK, obj);
                //return Request.CreateResponse(HttpStatusCode.OK, new { Response = res });

            }
            catch (Exception e1)
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
                //return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
            }
        }

        [HttpGet]
        public HttpResponseMessage ViewQuestionAndAnswar(long studentId, long objectiveExamId)
        {
            try
            {
                var examStudent = DbContext.OnlineObjectiveExamStudents.Where(a => a.StudentId == studentId && a.OnlineObjectiveExamId == objectiveExamId
                                    && a.Is_Active == true).ToList();
                var objectiveExam = DbContext.OnlineObjectiveExams.Where(a => a.Is_Active == true && a.OnlineObjectiveExamId == objectiveExamId).ToList();
                var subject = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var objectiveQuestion = DbContext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true).ToList();
                var objectiveAnswar = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();
                var onlineExamDate = DbContext.OnlineExamDates.Where(a => a.Is_Active == true).ToList();
              //  var answar = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true && a.OnlineObjectiveExamQuestionId == examStudent.FirstOrDefault().OnlineObjectiveExamQuestionId).FirstOrDefault();

                var result = (from b in objectiveExam
                                  //join c in objectiveQuestion on a.OnlineObjectiveExamQuestionId equals c.OnlineObjectiveExamQuestionId
                                  //join d in objectiveAnswar on a.GivenAnswerId equals d.OnlineObjectiveExamAnswerId
                              join e in subject on b.SubjectId equals e.Subject_Id
                              join f in onlineExamDate on b.OnlineExamDateId equals f.OnlineExamDateId
                              select new OnlineObjectiveExamQuestionAndAnswar
                              {
                                  ID = b.OnlineObjectiveExamId,
                                  Subject = e.Subject,
                                  DateOfExam = f.OnlineExamDate1,
                                  TotalCorrectAnswar = examStudent.Where(x => x.IsCorrect == true && x.IsAttempt == true).Count(),
                                  TotalIncorrectAnswar = examStudent.Where(x => x.IsCorrect == false && x.IsAttempt == true).Count(),
                                  Score = Getscore(examStudent, objectiveExam.FirstOrDefault()).Value,
                                  QuestionAndAnswar = (from c in objectiveQuestion
                                                       join d in examStudent on c.OnlineObjectiveExamQuestionId equals d.OnlineObjectiveExamQuestionId
                                                       where c.OnlineObjectiveExamId == b.OnlineObjectiveExamId
                                                       select new QuestionAndAnswarModel
                                                       {
                                                           Question = c.Question,
                                                           QuestionImage=c.QuestionImage==null?null:"/Images/ObjectiveQuestion/"+c.QuestionImage,
                                                           GivenAnswar = d.IsAttempt == true ? objectiveAnswar.Where(m => m.OnlineObjectiveExamAnswerId == d.GivenAnswerId).FirstOrDefault().Option : null,
                                                           GivenAnswarImage = d.IsAttempt == true ? (objectiveAnswar.Where(m => m.OnlineObjectiveExamAnswerId == d.GivenAnswerId).FirstOrDefault().OptionImage == null ? null : "/Images/ObjectiveOption/" + objectiveAnswar.Where(m => m.OnlineObjectiveExamAnswerId == d.GivenAnswerId).FirstOrDefault().OptionImage) : null,
                                                           Answar = objectiveAnswar.Where(m => m.OnlineObjectiveExamQuestionId == d.OnlineObjectiveExamQuestionId && m.IsCorrect == true).FirstOrDefault().Option,
                                                           AnswarImage = objectiveAnswar.Where(m => m.OnlineObjectiveExamQuestionId == d.OnlineObjectiveExamQuestionId && m.IsCorrect == true).FirstOrDefault().OptionImage == null ? null : "/Images/ObjectiveOption/" + objectiveAnswar.Where(m => m.OnlineObjectiveExamQuestionId == d.OnlineObjectiveExamQuestionId && m.IsCorrect == true).FirstOrDefault().OptionImage,
                                                       }).ToList()
                              }).FirstOrDefault();

                return Request.CreateResponse(HttpStatusCode.OK, new { QuestionAnswarDetails = result });
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
            }
        }

        [HttpPost]
        public HttpResponseMessage SubmitResult(BaseExamResultModel exam)
        {
            try
            {
                var res = "";
                var questionList = exam.Question.ToList();
                var objexam = DbContext.OnlineObjectiveExams.Where(a => a.OnlineObjectiveExamId == exam.OnlineObjectiveExamId && a.Is_Active == true).FirstOrDefault();
                //var studentList = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == exam.StudentId && a.Is_Active == true).FirstOrDefault();
                var objques = DbContext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true).ToList();
                var objans = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();
                if (questionList != null && objexam != null)
                {
                    foreach (var q in questionList)
                    {
                        long qID = q.QuestionId;
                        var questionDetails = objques.Where(a => a.OnlineObjectiveExamQuestionId == qID).FirstOrDefault();
                        var answar = q.Answer == 0 ? null : objans.Where(a => a.OnlineObjectiveExamAnswerId == q.Answer).FirstOrDefault();

                        OnlineObjectiveExamStudent student = new OnlineObjectiveExamStudent();
                        student.StudentId = exam.StudentId;
                        student.OnlineObjectiveExamId = exam.OnlineObjectiveExamId;
                        student.OnlineObjectiveExamQuestionId = qID;
                        student.GivenAnswerId = q.Answer;
                        student.IsCorrect =q.Answer==0?false:(answar.IsCorrect == true ? true : false);
                        student.Is_Active = true;
                        student.IsAttempt = q.Answer == 0 ? false : true;

                        student.InsertedId = exam.StudentId;
                        student.ModifiedId = exam.StudentId;
                        student.InsertedOn = DateTime.Now;
                        student.ModifiedOn = DateTime.Now;

                        DbContext.OnlineObjectiveExamStudents.Add(student);
                        DbContext.SaveChanges();
                    }
                    res = "Test is submitted successfully";

                }
                else
                {
                    res = "No data found";
                }

                return Request.CreateResponse(HttpStatusCode.OK, new { result = res });
            }
            catch
            {
                var res = "No data found";
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { result = res });
            }
        }


        [HttpGet]
        public HttpResponseMessage TotalWorksheetReport(int studentid,Guid? School, int? board, int? cls, Guid? sec, int? subject, int? examtype)
        {
            try
            {
                var regs = DbContext.VW_Student_With_Subject.Where(a => a.Regd_ID == studentid).ToList();
                var onlineexams = DbContext.OnlineExams.Where(a => a.Is_Active == true).ToList();
                var onlineexamans = DbContext.OnlineExamAnswerSheets.Where(a => a.Is_Active == true).ToList();
                var onlineobjective = DbContext.OnlineObjectiveExams.Where(a => a.Is_Active == true).ToList();
                var OnlineObjectiveStudents = DbContext.OnlineObjectiveExamStudents.Where(a => a.Is_Active == true).ToList();
                var worksheets = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true).ToList();
                var modules = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var res = (from a in regs
                           select new totalworksheetscoreclass
                           {
                               Board_ID = a.Board_ID.Value,
                               Board_Name = a.Board_Name,
                               Class_ID = a.Class_ID.Value,
                               Class_Name = a.Class_Name,
                               Customer_Name = a.Customer_Name,
                               Regd_ID = a.Regd_ID,
                               SchoolId = a.SchoolId.Value,
                               SchoolName = a.SchoolName,
                               SectionId = a.SectionId.Value,
                               SectionName = a.SectionName,
                               Subject = a.Subject,
                               Subject_Id = a.Subject_Id,
                               subjectivescore = GetOnlineSubjectiveExamscore(onlineexamans.Where(m => m.StudentId == a.Regd_ID).ToList(), onlineexams.Where(m => m.SubjectId == a.Subject_Id && m.SchoolId == a.SchoolId && m.ClassId == a.Class_ID).ToList(), examtype),
                               objectivescore = GetOnlineObjectiveExamscore(OnlineObjectiveStudents.Where(m => m.StudentId == a.Regd_ID).ToList(), onlineobjective.Where(m => m.SubjectId == a.Subject_Id && m.SchoolId == a.SchoolId && m.ClassId == a.Class_ID).ToList(), examtype),
                               worksheetscore = GetOnlineWorksheetExamscore(worksheets.Where(m => m.Class_Id == a.Class_ID && m.Subject_Id == a.Subject_Id && m.Regd_ID == a.Regd_ID).ToList(), modules.Where(m => m.Class_Id == a.Class_ID && m.Subject_Id == a.Subject_Id && m.Question_PDF != null).ToList(), examtype)
                           }).ToList();


                if (School != null)
                {
                    res = res.Where(a => a.SchoolId == School.Value).ToList();

                }

                if (board != null && board != 0)
                {
                    res = res.Where(a => a.Board_ID == board.Value).ToList();

                }
                if (cls != null && cls != 0)
                {
                    res = res.Where(a => a.Class_ID == cls.Value).ToList();

                }
                if (sec != null)
                {
                    res = res.Where(a => a.SectionId == sec.Value).ToList();

                }
                if (subject != null && subject != 0)
                {
                    res = res.Where(a => a.SectionId == sec.Value).ToList();

                }
                return Request.CreateResponse(HttpStatusCode.OK, res);

            }
            catch (Exception e1)
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
               
            }
        }
        public decimal? GetOnlineObjectiveExamscore(List<OnlineObjectiveExamStudent> examStudents, List<OnlineObjectiveExam> exam, long? examtype)
        {
            // var answers = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();
            if (examtype != null)
            {
                exam = exam.Where(a => a.OnlineExamTypeId == examtype).ToList();
            }
            if (exam == null || examStudents == null)
            {
                return 0;
            }
            var examques = DbContext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true).ToList();

            var ress = (from a in examques
                        join b in exam on a.OnlineObjectiveExamId equals b.OnlineObjectiveExamId
                        select a).ToList();
            //var Totalmark = ress.Sum(a => a.Mark);

            var res = (from a in ress
                       join b in examStudents on a.OnlineObjectiveExamId equals b.OnlineObjectiveExamId
                       select new
                       {
                           examid = a.OnlineObjectiveExamId,
                           iscorrect = b.IsCorrect,
                           score = a.Mark
                       }).ToList();
            if (res.Count == 0)
            {
                return 0;
            }
            decimal? totalscore = 0;
            foreach (var a in res.Select(m => m.examid).Distinct().ToList())
            {
                var correct = res.Where(m => m.iscorrect == true && m.examid == a.Value).ToList().Sum(m => m.score);
                var wrong = res.Where(m => m.iscorrect == false && m.examid == a.Value).ToList().Count();
                if (exam.Where(m => m.OnlineObjectiveExamId == a.Value).FirstOrDefault().IsNagativeMarking == true)
                {
                    var negativelist = DbContext.Negativemarks.Where(m => m.NegativeMarkID == exam.Where(h => h.OnlineObjectiveExamId == a.Value).FirstOrDefault().NagativeMarkingId).FirstOrDefault();
                    var negvmark = negativelist.NegativeMark1 * wrong;
                    var scoreres = correct - negvmark;
                    totalscore += scoreres;
                }
                else
                {
                    totalscore += correct;
                }
            }
            return totalscore / res.Count();
        }
        public decimal? GetOnlineSubjectiveExamscore(List<OnlineExamAnswerSheet> examStudents, List<OnlineExam> exam, long? examtype)
        {
            // var answers = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();
            if (examtype != null)
            {
                exam = exam.Where(a => a.OnlineExamTypeId == examtype).ToList();
            }
            if (exam == null || examStudents == null)
            {
                return 0;
            }
            var examques = DbContext.OnlineExamAnswerSheetRemarks.Where(a => a.Is_Active == true).ToList();
            var examans = (from b in examStudents
                           join c in exam on b.OnlineExamId equals c.OnlineExamId
                           select new
                           {
                               total = examques.Where(a => a.OnlineExamAnswerSheetId == b.OnlineExamAnswerSheetId).Sum(a => a.GivenMarks)
                           }).ToList();

            var Totalmark = examans.Average(a => a.total);
            if (Totalmark == null)
            {
                return 0;
            }
            else
            {
                return Totalmark;
            }
        }
        public double GetOnlineWorksheetExamscore(List<tbl_DC_Worksheet> examStudents, List<tbl_DC_Module> modules, long? examtype)
        {
            if (examtype != null)
            {
                modules = modules.Where(a => a.Exam_Type == examtype).ToList();
            }
            if (modules == null || examStudents == null)
            {
                return 0;
            }
            var res = (from a in examStudents
                       join b in modules on a.Chapter_Id equals b.Chapter_Id
                       select a).ToList();
            if (res.Count == 0)
            {
                return 0;
            }
            var Totalmark = res.Average(a => a.Total_Mark);
            if (Totalmark == null)
            {
                return 0;
            }
            else
            {
                return Totalmark;
            }
        }

    }
}
