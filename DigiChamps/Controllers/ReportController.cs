﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class ReportController : ApiController
    {
        DigiChampsEntities db = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public class ErrorResult
        {
            public ErrorResponse Error { get; set; }
        }

        public class ErrorResponse
        {
            public string Message { get; set; }
        }

        public class SuccessResponse
        {
            public string Message { get; set; }
        }
        public class SuccessResult
        {
            public List<ReportModule> ReportModules { get; set; }
            public List<ProgressModule> ProgressModules { get; set; }
        }
        public class ReportModule
        {
            public int? id { get; set; }
            public string moduleName { get; set; }
            public int  moduleId{ get; set; }
            public int class_Id { get; set; }
            public Guid? section_Id { get; set; }
            public int rowId { get; set; }
            public int  regId{ get; set; }
            public int? playingTime { get; set; }
            public long startTime { get; set; }
            public long endTime { get; set; }
            public string reportType { get; set; }
        }
        public class ProgressModule
        {
            public int Id { get; set; }
            public int StudentSubjectProgress_ID { get; set; }
            public int Subject_ID { get; set; }
            public int Chapter_ID { get; set; }
            public int Class_ID { get; set; }
            public Guid? School_ID { get; set; }
            public Guid? Section_ID { get; set; }
            public int Regd_ID { get; set; }
            public int Module_ID { get; set; }
            public string Module_Type { get; set; }
            public DateTime Inserted_On { get; set; }
            public bool Is_Active { get; set; }
            public bool Is_Delete { get; set; }

            public int rowId { get; set; }
        }
        public class ReportModule_List
        {
            public List<ReportModule> list { get; set; }
        }
        public class success_Banner
        {
            public ReportModule_List Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }

        [HttpPost]
        public HttpResponseMessage SaveReportModules([FromBody]SuccessResult rm)
        {
          
            try{

                if (rm != null)
                {
                    if(rm.ProgressModules!=null)
                    {
                        //List<int?> listIds = new List<int?>();
                        //List<tbl_DC_StudentSubjectProgress> list = new List<tbl_DC_StudentSubjectProgress>();
                        //for (int i = 0; i < rm.ProgressModules.Count;i++ )
                        //{
                        //    if(!listIds.Contains(rm.ProgressModules[i].Module_ID))
                        //    listIds.Add(rm.ProgressModules[i].Module_ID);
                        //}
                        //list = db.tbl_DC_StudentSubjectProgress.Where(x => listIds.Contains(x.Module_ID)).ToList();

                        for (int i = 0; i < rm.ProgressModules.Count; i++)
                        {
                            ProgressModule progressModule = rm.ProgressModules[i];
                            tbl_DC_StudentSubjectProgress tbl = db.tbl_DC_StudentSubjectProgress.Where(x => x.Module_ID ==
                              progressModule.Module_ID && x.Module_Type == progressModule.Module_Type
                                && x.Regd_ID == progressModule.Regd_ID &&
                                x.Subject_ID == progressModule.Subject_ID
                                && x.Chapter_ID == progressModule.Chapter_ID
                                && x.Class_ID == progressModule.Class_ID
                                ).FirstOrDefault();
                           // tbl_DC_StudentSubjectProgress tbl = null;
                            if (tbl == null )
                            {
                               
                                tbl = new tbl_DC_StudentSubjectProgress();
                                tbl.Regd_ID = progressModule.Regd_ID;
                                tbl.Subject_ID = progressModule.Subject_ID;
                                tbl.Chapter_ID = progressModule.Chapter_ID;
                                tbl.Inserted_On = System.DateTime.Now;
                                tbl.Module_ID = progressModule.Module_ID;
                                tbl.Module_Type = progressModule.Module_Type;
                                tbl.Class_ID = progressModule.Class_ID;
                                tbl.Section_ID = progressModule.Section_ID;
                                tbl.School_ID = progressModule.School_ID;
                                tbl.Is_Active = true;
                                tbl.Is_Delete = false;
                                db.tbl_DC_StudentSubjectProgress.Add(tbl);

                                db.SaveChanges();
                            }
                            else
                            {
                                tbl.Inserted_On = System.DateTime.Now;
                                // db.tbl_DC_StudentSubjectProgress.u(tbl);

                                //  db.SaveChanges();
                                db.Entry(tbl).State = EntityState.Modified;
                                db.SaveChanges();
                            }

                            progressModule.rowId = -1;

                        }

                       

                        
                    }
                    if (rm.ReportModules != null)
                    {
                        List<ReportModule> list = rm.ReportModules;
                        List<ReportModule> savedList = new List<ReportModule>();
                        for (int i = 0; i < list.Count; i++)
                        {
                            ReportModule reportModule = list[i];

                            Guid? schoolId = db.tbl_DC_Registration.Where(x => x.Regd_ID == reportModule.regId
                                && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.SchoolId).FirstOrDefault();


                            int? reportModuleId = db.tbl_DC_ReportModuleMaster.Where(x => x.ReportName == reportModule.moduleName
                                && x.Is_Active == true && x.Is_Delete == false).Select(x => x.ReportModuleMaster_ID).FirstOrDefault();

                            int? reportTypeId = db.tbl_DC_ReportType.Where(x => x.ReportType_Name == reportModule.reportType &&
                                x.Is_Active == true && x.Is_Delete == false).Select(x => x.ReportType_ID).FirstOrDefault();



                            tbl_DC_UserReport ur = new tbl_DC_UserReport();

                            ur.ReportType_ID = reportTypeId;
                            ur.ReportModuleMaster_ID = reportModuleId;
                            ur.SchoolId = schoolId;
                            //DateTime dt = DateTime.MinValue;
                            //DateTime dtfommls = ;
                            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                            ur.Report_Date = dtDateTime.AddMilliseconds(reportModule.startTime).ToLocalTime();
                            // return dtDateTime;

                            //ur.Report_Date =dt.AddMilliseconds(reportModule.startTime);// new DateTime(reportModule.startTime);
                            ur.Inserted_On = System.DateTime.Now;
                            ur.Module_ID = reportModule.moduleId;
                            ur.Video_Time = Convert.ToInt32(TimeSpan.FromMilliseconds(Convert.ToDouble(reportModule.playingTime)).TotalMinutes);
                            ur.Class_ID = reportModule.class_Id;
                            ur.Regd_ID = reportModule.regId;
                            ur.SectionId = reportModule.section_Id;
                            db.tbl_DC_UserReport.Add(ur);
                            db.SaveChanges();

                            tbl_DC_ReportMaster reportMaster = new tbl_DC_ReportMaster();
                            reportMaster.UserReport_ID = ur.UserReport_ID;
                            reportMaster.Reg_ID = reportModule.regId;
                            reportMaster.Inserted_On = System.DateTime.Now;
                            db.tbl_DC_ReportMaster.Add(reportMaster);

                            db.SaveChanges();
                            if (ur.UserReport_ID > 0)
                            {
                                reportModule.rowId = 1;
                                savedList.Add(reportModule);
                            }
                        }
                        rm.ReportModules = savedList;
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, rm);
            }
            catch (Exception)
            {
                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }
    }






}
