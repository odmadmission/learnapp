﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace DigiChamps.Controllers
{
    public class QuestionbankAPIController : ApiController
    {
        DigiChampsEntities dbContext = new DigiChampsEntities();
        DateTime now = DateTime.Now;
        [HttpPost]
        public HttpResponseMessage UploadQuestionbank()
        {
            try
            {
                var httprequest = HttpContext.Current.Request;
                int Subject_ID = Convert.ToInt32(httprequest.Form["Subject_ID"]);
                int Chapter_ID = Convert.ToInt32(httprequest.Form["Chapter_ID"]);
                int _board_id = Convert.ToInt32(httprequest.Form["Board_id"]);
                int _class_id = Convert.ToInt32(httprequest.Form["Class_id"]);
                int _student_id = Convert.ToInt32(httprequest.Form["Regd_ID"]);
                string _file_type = Convert.ToString(httprequest.Form["File_Type"]);
                int _module_id = Convert.ToInt32(httprequest.Form["Module_ID"]);
                Guid _section_id = Guid.Parse(httprequest.Form["Section_ID"]);
                string guid = Guid.NewGuid().ToString();
                int id = 0;
                tbl_DC_QuestionBank ob = dbContext.tbl_DC_QuestionBank.Where(a => a.Board_Id == _board_id && a.Class_Id == _class_id && a.Regd_ID == _student_id && a.Is_Active == true && a.Subject_Id == Subject_ID && a.Chapter_Id == Chapter_ID && a.Module_Id == _module_id).FirstOrDefault();
                if (ob != null)
                {
                    var attach = dbContext.tbl_DC_QuestionBank_Attachment.Where(a => a.Is_Active == true && a.QuestionBank_Id == ob.QuestionBank_Id).ToList();
                    if (attach.Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new { msg = "Already Exits" });
                    }
                    ob.Board_Id = _board_id;
                    ob.Chapter_Id = Chapter_ID;
                    ob.Class_Id = _class_id;
                    ob.Modified_By = _student_id;
                    ob.Modified_Date = now;
                    ob.Is_Active = true;
                    ob.Is_Deleted = false;
                    ob.File_Type = _file_type;
                    ob.Subject_Id = Subject_ID;
                    ob.SectionId = _section_id;
                    ob.Verified = false;
                    ob.Regd_ID = _student_id;
                    ob.Verified_By = 0;
                    ob.Total_Mark = 0;
                    ob.Module_Id = _module_id;
                    ob.is_Read = false;
                    dbContext.SaveChanges();
                    id = ob.QuestionBank_Id;

                }
                else
                {
                    ob = new tbl_DC_QuestionBank();
                    ob.Board_Id = _board_id;
                    ob.Chapter_Id = Chapter_ID;
                    ob.Class_Id = _class_id;
                    ob.Modified_By = _student_id;
                    ob.Modified_Date = now;
                    ob.Inserted_Date = now;
                    ob.Inserted_By = _student_id;
                    ob.Is_Active = true;
                    ob.Is_Deleted = false;
                    ob.File_Type = _file_type;
                    ob.Subject_Id = Subject_ID;
                    ob.SectionId = _section_id;
                    ob.Verified = false;
                    ob.Module_Id = _module_id;
                    ob.Regd_ID = _student_id;
                    ob.Verified_By = 0;
                    ob.Total_Mark = 0;
                    ob.is_Read = false;
                    dbContext.tbl_DC_QuestionBank.Add(ob);
                    dbContext.SaveChanges();
                    id = ob.QuestionBank_Id;
                }

                if (httprequest.Files.Count > 0)
                {
                    foreach (string files in httprequest.Files)
                    {
                        var postedfile = httprequest.Files[files];
                        tbl_DC_QuestionBank_Attachment att = new tbl_DC_QuestionBank_Attachment();
                        att.File_Name = _file_type == "pdf" ? id + ".pdf" : id + ".pdf";
                        att.QuestionBank_Id = id;
                        att.Modified_By = _student_id;
                        att.Modified_Date = DateTime.Now;
                        att.Inserted_Date = DateTime.Now;
                        att.Inserted_By = _student_id;
                        att.Is_Active = true;
                        att.Is_Deleted = false;

                        // Bitmap bitmapImage = ResizeImage(postedfile.InputStream, 300, 300);
                        // System.IO.MemoryStream stream = new System.IO.MemoryStream();
                        var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/Questionbank/"), (guid + att.File_Name));
                        postedfile.SaveAs(path);
                        //docfile.Add(path);
                        att.Path = guid + att.File_Name;
                        dbContext.tbl_DC_QuestionBank_Attachment.Add(att);
                        dbContext.SaveChanges();

                        try
                        {

                            var chapter = dbContext.tbl_DC_Chapter.Where(a => a.Chapter_Id == Chapter_ID).Select(a => a.Chapter).FirstOrDefault();
                            var subject = dbContext.tbl_DC_Subject.Where(a => a.Subject_Id == Subject_ID).Select(a => a.Subject).FirstOrDefault();
                            var reg = dbContext.tbl_DC_Registration.Where(a => a.Regd_ID == _student_id).FirstOrDefault();

                            var tea = dbContext.tbl_DC_AssignSubjectToTeacher.Where(A => A.Subject_Id == Subject_ID && A.SectionId == _section_id && A.Is_Active == true).FirstOrDefault();
                            var pushnot = (from c in dbContext.Teachers.Where(x => x.TeacherId == tea.Teacher_ID)

                                           select new { c.TeacherId, c.fcmId }).FirstOrDefault();
                            string body = "QBANK#{{ID}}#{{marks}}#{{pdf}}#{{name}} has uploaded answersheet for {{chapter}} worksheet of {{subject}}";
                            string msg = body.ToString().Replace("{{chapter}}", chapter.ToString())
                                .Replace("{{subject}}", subject + "").Replace("{{ID}}", ob.QuestionBank_Id + "").Replace("{{marks}}", ob.Total_Mark + "").Replace("{{pdf}}",
                                "http://learn.odmps.org/Images/Questionbank/" + att.Path + "").Replace("{{name}}", reg.Customer_Name + "");

                            if (pushnot != null)
                            {
                                if (pushnot.fcmId != null)
                                {
                                    var note = new PushNotiStatus("Worksheet New", msg, pushnot.fcmId);
                                }
                            }


                        }
                        catch (Exception e)
                        {

                        }



                    }
                }







                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        [HttpGet]
        public HttpResponseMessage DeleteQuestionbank(int id)
        {
            try
            {
                var wrk = dbContext.tbl_DC_QuestionBank_Attachment.Where(a => a.QuestionBank_Id == id).ToList();
                foreach (tbl_DC_QuestionBank_Attachment a in wrk)
                {
                    a.Is_Active = false;
                    a.Is_Deleted = true;
                    a.Modified_Date = DateTime.Now;
                    dbContext.SaveChanges();
                }
                var wrkk = dbContext.tbl_DC_QuestionBank.Where(a => a.QuestionBank_Id == id).FirstOrDefault();
                wrkk.Is_Active = false;
                wrkk.Is_Deleted = true;
                wrkk.Modified_Date = DateTime.Now;
                dbContext.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        [HttpGet]
        public HttpResponseMessage IsReadQuestionbank(int id)
        {
            try
            {
                var wrk = dbContext.tbl_DC_QuestionBank.Where(a => a.QuestionBank_Id == id).FirstOrDefault();

                wrk.is_Read = true;
                wrk.Modified_Date = DateTime.Now;
                dbContext.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        public class Questionbankcls
        {
            public int QuestionBank_Id { get; set; }
            public int Regd_ID { get; set; }
            public string Student_Name { get; set; }
            public int Board_Id { get; set; }
            public string Board_Name { get; set; }
            public int Class_Id { get; set; }
            public string Class_Name { get; set; }
            public Nullable<System.Guid> SectionId { get; set; }
            public string Section_Name { get; set; }
            public int Subject_Id { get; set; }
            public string Subject_Name { get; set; }
            public int Chapter_Id { get; set; }
            public string Chapter_Name { get; set; }
            public bool Verified { get; set; }
            public int Verified_By { get; set; }
            public string Verified_By_Name { get; set; }
            public int Total_Mark { get; set; }
            public string status { get; set; }
            public Nullable<System.DateTime> Inserted_Date { get; set; }

            public Nullable<System.DateTime> Modified_Date { get; set; }
            public int Module_Id { get; set; }
            public string Question_Bank_PDF_Name { get; set; }
            public string Question_Bank_PDF { get; set; }
            public string File_Type { get; set; }
            public tbl_DC_QuestionBank_Attachment attachments { get; set; }
        }

        public class StdName
        {
            public string Name { get; set; }
            public string Mobile { get; set; }

        }
        public class WorksheetHolder
        {
            public List<Questionbankcls> data { get; set; }

            public List<StdName> students { get; set; }


            public int total { get; set; }

            public int uploaded { get; set; }

            public int NotUploaded { get; set; }

        }
        [HttpGet]
        public HttpResponseMessage GetallQuestionbankWithStatus(int classid, Guid sec, int subjectid, int chapterid, string status)
        {
            try
            {


                var wrk = dbContext.tbl_DC_QuestionBank.Where(a => a.Is_Active == true && a.Class_Id == classid
                    && a.SectionId == sec && a.Subject_Id == subjectid && a.Chapter_Id == chapterid).ToList();



                var board = dbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = dbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = dbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = dbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = dbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = dbContext.tbl_DC_QuestionBank_Attachment.Where(a => a.Is_Active == true).ToList();



                var res = (from a in wrk
                           join b in board on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           //   join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           select new Questionbankcls
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Question_Bank_PDF = "/Module/Questionbank/" + modules.Where(m => m.Module_ID == a.Module_Id).FirstOrDefault().Question_Bank_PDF,
                               Question_Bank_PDF_Name = modules.Where(m => m.Module_ID == a.Module_Id).FirstOrDefault().Question_Bank_PDF_Name,
                               status = (a.is_Read == false && a.Verified == false) ? "NEW" : (((a.is_Read == true && a.is_Read == null) && a.Verified == true) ? "SCORED GIVEN" : "OPENED"),
                               Regd_ID = a.Regd_ID,
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Total_Mark = a.Total_Mark,
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               QuestionBank_Id = a.QuestionBank_Id,
                               attachments = (from k in worksheetattachments
                                              where k.QuestionBank_Id == a.QuestionBank_Id
                                              select new tbl_DC_QuestionBank_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Questionbank/" + k.Path,
                                                  QuestionBank_Attachment_Id = k.QuestionBank_Attachment_Id,
                                                  QuestionBank_Id = k.QuestionBank_Id
                                              }).FirstOrDefault(),
                           }).ToList();

                List<Questionbankcls> list = new List<Questionbankcls>();
                if (status != null && status != "" && status.ToUpper() != "ALL")
                {
                    list.AddRange(res.Where(a => a.status == status).ToList());
                }
                else
                {
                    list.AddRange(res);
                }




                WorksheetHolder holder = new WorksheetHolder();
                holder.data = list;
                holder.students = (from a in students.Where(a => a.SectionId == sec && !res.Select(b => b.Regd_ID).ToList().Contains(a.Regd_ID))

                                   select new StdName
                                   {
                                       Name = a.Customer_Name,
                                       Mobile = a.Mobile
                                   }
                                     ).ToList();
                holder.uploaded = res.Select(a => a.Regd_ID).Distinct().Count();
                holder.NotUploaded = students.Where(a => a.SectionId == sec).Count() - holder.uploaded;

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "S", worksheetlist = holder });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "E" });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetallQuestionbank(int classid, Guid sec, int subjectid, int chapterid)
        {
            try
            {
                var wrk = dbContext.tbl_DC_QuestionBank.Where(a => a.Is_Active == true && a.Class_Id == classid
                    && a.SectionId == sec && a.Subject_Id == a.Subject_Id && a.Chapter_Id == chapterid).ToList();
                var board = dbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = dbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = dbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = dbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = dbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = dbContext.tbl_DC_QuestionBank_Attachment.Where(a => a.Is_Active == true).ToList();
                var res = (from a in wrk
                           join b in board on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           //   join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           select new Questionbankcls
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Question_Bank_PDF = "/Module/Questionbank/" + modules.Where(m => m.Module_ID == a.Module_Id).FirstOrDefault().Question_Bank_PDF,
                               Question_Bank_PDF_Name = modules.Where(m => m.Module_ID == a.Module_Id).FirstOrDefault().Question_Bank_PDF_Name,
                               status = (a.is_Read == false && a.Verified == false) ? "NEW" : (((a.is_Read == true && a.is_Read == null) && a.Verified == true) ? "SCORED GIVEN" : "OPENED"),
                               Regd_ID = a.Regd_ID,
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Total_Mark = a.Total_Mark,
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               QuestionBank_Id = a.QuestionBank_Id,
                               attachments = (from k in worksheetattachments
                                              where k.QuestionBank_Id == a.QuestionBank_Id
                                              select new tbl_DC_QuestionBank_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Questionbank/" + k.Path,
                                                  QuestionBank_Attachment_Id = k.QuestionBank_Attachment_Id,
                                                  QuestionBank_Id = k.QuestionBank_Id
                                              }).FirstOrDefault(),
                           }).ToList();



                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "S", worksheetlist = res });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "E" });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetallQuestionbankByRegdId(int classid, Guid sec, int subjectid, int chapterid, int regdId)
        {
            try
            {
                var wrk = dbContext.tbl_DC_QuestionBank.Where(a => a.Is_Active == true && a.Class_Id == classid && a.SectionId == sec && a.Subject_Id == subjectid && a.Chapter_Id == chapterid && a.Regd_ID == regdId).ToList();
                var board = dbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = dbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = dbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = dbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true && a.Question_Bank_PDF != null && a.Class_Id == classid && a.Subject_Id == subjectid && a.Chapter_Id == chapterid).ToList();
                var sections = dbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Regd_ID == regdId).FirstOrDefault();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = dbContext.tbl_DC_QuestionBank_Attachment.Where(a => a.Is_Active == true).ToList();
                var res = (from a in modules
                           join b in board on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           select new Questionbankcls
                           {
                               Board_Id = a.Board_Id.Value,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id.Value,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id.Value,
                               Class_Name = c.Class_Name,
                               File_Type = wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault() == null ? "" : wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault().File_Type,
                               Inserted_Date = wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault() == null ? null : wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault().Inserted_Date,
                               Modified_Date = wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault() == null ? null : wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault().Modified_Date,
                               Module_Id = a.Module_ID,
                               Question_Bank_PDF = "/Module/Questionbank/" + a.Question_Bank_PDF,
                               Question_Bank_PDF_Name = a.Question_Bank_PDF_Name,
                               Regd_ID = regdId,
                               status = wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault() == null ? null : wrk.Where(m => m.Module_Id == a.Module_ID).Select(m => (m.is_Read == false && m.Verified == false) ? "NEW" : (((m.is_Read == true && m.is_Read == null) && m.Verified == true) ? "SCORED GIVEN" : "OPENED")).FirstOrDefault(),
                               SectionId = sec,
                               Student_Name = students.Customer_Name,
                               Subject_Id = a.Subject_Id.Value,
                               Subject_Name = d.Subject,
                               Total_Mark = wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault() == null ? 0 : wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault().Total_Mark,
                               Verified = wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault() == null ? false : wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault().Verified,
                               Verified_By = wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault() == null ? 0 : wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault().Verified_By,
                               Verified_By_Name = wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault() == null ? "" : (wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault().Verified_By != 0 ? teachers.Where(m => m.TeacherId == wrk.Where(s => s.Module_Id == a.Module_ID).FirstOrDefault().Verified_By).FirstOrDefault().Name : ""),
                               QuestionBank_Id = wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault() == null ? 0 : wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault().QuestionBank_Id,
                               attachments = wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault() == null ? null : (from k in worksheetattachments
                                                                                                                           where k.QuestionBank_Id == wrk.Where(m => m.Module_Id == a.Module_ID).FirstOrDefault().QuestionBank_Id
                                                                                                                           select new tbl_DC_QuestionBank_Attachment
                                                                                                                           {
                                                                                                                               File_Name = k.File_Name,
                                                                                                                               Modified_Date = k.Modified_Date,
                                                                                                                               Path = "/Images/Questionbank/" + k.Path,
                                                                                                                               QuestionBank_Attachment_Id = k.QuestionBank_Attachment_Id,
                                                                                                                               QuestionBank_Id = k.QuestionBank_Id
                                                                                                                           }).FirstOrDefault(),
                           }).ToList();



                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "S", worksheetlist = res });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "E" });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetQuestionbankById(int id)
        {
            try
            {
                var wrk = dbContext.tbl_DC_QuestionBank.Where(a => a.Is_Active == true && a.QuestionBank_Id == id).ToList();
                var board = dbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = dbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = dbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = dbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = dbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = dbContext.tbl_DC_QuestionBank_Attachment.Where(a => a.Is_Active == true).ToList();
                var res = (from a in wrk
                           join b in board on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           //  join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           select new Questionbankcls
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Question_Bank_PDF = "/Module/Questionbank/" + modules.Where(m => m.Module_ID == a.Module_Id).FirstOrDefault().Question_Bank_PDF,
                               Question_Bank_PDF_Name = modules.Where(m => m.Module_ID == a.Module_Id).FirstOrDefault().Question_Bank_PDF_Name,
                               Regd_ID = a.Regd_ID,
                               status = (a.is_Read == false && a.Verified == false) ? "NEW" : (((a.is_Read == true && a.is_Read == null) && a.Verified == true) ? "SCORED GIVEN" : "OPENED"),
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Total_Mark = a.Total_Mark,
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               QuestionBank_Id = a.QuestionBank_Id,
                               attachments = (from k in worksheetattachments
                                              where k.QuestionBank_Id == a.QuestionBank_Id
                                              select new tbl_DC_QuestionBank_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Questionbank/" + k.Path,
                                                  QuestionBank_Attachment_Id = k.QuestionBank_Attachment_Id,
                                                  QuestionBank_Id = k.QuestionBank_Id
                                              }).FirstOrDefault(),
                           }).FirstOrDefault();



                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "S", worksheet = res });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "E" });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetQuestionbankBymoduleId(int moduleid,int regdid)
        {
            try
            {
                var wrk = dbContext.tbl_DC_QuestionBank.Where(a => a.Is_Active == true && a.Module_Id == moduleid && a.Regd_ID==regdid).ToList();
                var board = dbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = dbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = dbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = dbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = dbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = dbContext.tbl_DC_QuestionBank_Attachment.Where(a => a.Is_Active == true).ToList();
                var res = (from a in wrk
                           join b in board on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           select new Questionbankcls
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Question_Bank_PDF = "/Module/Questionbank/" + f.Question_Bank_PDF,
                               Question_Bank_PDF_Name = f.Question_Bank_PDF_Name,
                               Regd_ID = a.Regd_ID,
                               status = (a.is_Read == false && a.Verified == false) ? "NEW" : (((a.is_Read == true && a.is_Read == null) && a.Verified == true) ? "SCORED GIVEN" : "OPENED"),
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Total_Mark = a.Total_Mark,
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = (a.Verified_By != 0 && a.Verified_By!=null) ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               QuestionBank_Id = a.QuestionBank_Id,
                               attachments = (from k in worksheetattachments
                                              where k.QuestionBank_Id == a.QuestionBank_Id
                                              select new tbl_DC_QuestionBank_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Questionbank/" + k.Path,
                                                  QuestionBank_Attachment_Id = k.QuestionBank_Attachment_Id,
                                                  QuestionBank_Id = k.QuestionBank_Id
                                              }).FirstOrDefault(),
                           }).FirstOrDefault();



                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "S", worksheet = res });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "E" });
            }
        }
        [HttpPost]
        public HttpResponseMessage ScoreToQuestionbank(int worksheetId, int teacherId, int score)
        {
            try
            {

                if (score > 20)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
                }

                tbl_DC_QuestionBank wrk = dbContext.tbl_DC_QuestionBank.Where(a => a.QuestionBank_Id == worksheetId).FirstOrDefault();
                wrk.Total_Mark = score;
                wrk.Verified_By = teacherId;
                wrk.Verified = true;
                wrk.Modified_Date = DateTime.Now;
                dbContext.SaveChanges();
                //string subject=dbContext.tbl_DC_Subject.Where(a=>a.Subject_Id==wrk.Subject_Id)
                //var note = new PushNotiStatusMaster(title, body, obj1.Feed_Images, ids[i]);
                try
                {
                    var chapter = dbContext.tbl_DC_Chapter.Where(a => a.Chapter_Id == wrk.Chapter_Id).FirstOrDefault();
                    var subject = dbContext.tbl_DC_Subject.Where(a => a.Subject_Id == wrk.Subject_Id).FirstOrDefault();
                    var pushnot = (from c in dbContext.tbl_DC_Registration.Where(x => x.Regd_ID == wrk.Regd_ID)

                                   select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                    string body = "QBANK#{{ID}}#{{ChapterId}}#{{SubjectId}}#{{ChapterName}}#Your teacher has updated score {{score}} for {{chapter}} answersheet of {{subject}}";
                    string msg = body.ToString().Replace("{{chapter}}", chapter.Chapter.ToString())
                        .Replace("{{subject}}", subject.Subject + "").Replace("{{ID}}", worksheetId + "").Replace("{{score}}", score + "")
                        .Replace("{{ChapterId}}", wrk.Chapter_Id + "").Replace("{{SubjectId}}", wrk.Subject_Id + "").Replace("{{ChapterName}}", chapter.Chapter + "");

                    if (pushnot != null)
                    {
                        if (pushnot.Device_id != null)
                        {
                            var note = new PushNotiStatus("Worksheet Score", msg, pushnot.Device_id);
                        }
                    }
                }
                catch (Exception)
                {


                }
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success", worksheetid = worksheetId });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        public class QuestionbankReviewClass
        {
            public string Attachment { get; set; }
            public DateTime Inserted_On { get; set; }
            public bool Is_Student { get; set; }
            public string Message { get; set; }
            public DateTime Modified_On { get; set; }
            public int Posted_By_ID { get; set; }
            public string Posted_By { get; set; }
            public int QuestionbankReview_ID { get; set; }
            public int Questionbank_ID { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetWorksheetReview(int questionbankid)
        {
            try
            {
                var review = dbContext.Tbl_Questionbank_Review.Where(a => a.Is_Active == true && a.Questionbank_ID == questionbankid).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();

                var res = (from a in review
                           select new QuestionbankReviewClass
                           {
                               Attachment = a.Attachment == null ? null : "/Images/QuestionbankReview/" + a.Attachment,
                               Inserted_On = a.Inserted_On.Value,
                               Is_Student = a.Is_Student.Value,
                               Message = a.Message,
                               Modified_On = a.Modified_On.Value,
                               Posted_By_ID = a.Posted_By_ID.Value,
                               QuestionbankReview_ID = a.QuestionbankReview_ID,
                               Questionbank_ID = a.Questionbank_ID.Value,
                               Posted_By = a.Is_Student == true ? (students.Where(m => m.Regd_ID == a.Posted_By_ID).FirstOrDefault().Customer_Name) : (teachers.Where(m => m.TeacherId == a.Posted_By_ID).FirstOrDefault().Name)
                           }).ToList();

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success", review = res });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        [HttpGet]
        public HttpResponseMessage DeleteQuestionbankReview(int id)
        {
            try
            {
                Tbl_Questionbank_Review review = dbContext.Tbl_Questionbank_Review.Where(a => a.Is_Active == true && a.QuestionbankReview_ID == id).FirstOrDefault();
                review.Is_Active = false;
                review.Modified_On = DateTime.Now;
                dbContext.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        [HttpPost]
        public HttpResponseMessage UploadQuestionbankReview()
        {
            try
            {
                var httprequest = HttpContext.Current.Request;
                int Worksheet_ID = Convert.ToInt32(httprequest.Form["Questionbank_ID"]);
                int Posted_By_ID = Convert.ToInt32(httprequest.Form["Posted_By_ID"]);
                string Message = Convert.ToString(httprequest.Form["Message"]);
                bool Is_student = Convert.ToBoolean(httprequest.Form["Is_Student"]);
                string guid = Guid.NewGuid().ToString();
                int id = 0;
                Tbl_Questionbank_Review att = new Tbl_Questionbank_Review();
                att.Is_Student = Is_student;
                att.Questionbank_ID = Worksheet_ID;
                att.Modified_On = DateTime.Now;
                att.Inserted_On = DateTime.Now;
                att.Is_Active = true;
                att.Message = Message;
                att.Posted_By_ID = Posted_By_ID;
                att.Inserted_By = "1";
                att.Modified_By = "1";
                // Bitmap bitmapImage = ResizeImage(postedfile.InputStream, 300, 300);
                // System.IO.MemoryStream stream = new System.IO.MemoryStream();
                if (httprequest.Files != null && httprequest.Files.Count > 0)
                {
                    foreach (string files in httprequest.Files)
                    {
                        var postedfile = httprequest.Files[files];
                        var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/QuestionbankReview/"), (guid + postedfile.FileName));
                        postedfile.SaveAs(path);
                        //docfile.Add(path);
                        att.Attachment = guid + postedfile.FileName;
                    }

                }
                dbContext.Tbl_Questionbank_Review.Add(att);
                dbContext.SaveChanges();


                try
                {
                    var wrk = dbContext.tbl_DC_QuestionBank.Where(a => a.QuestionBank_Id == Worksheet_ID).FirstOrDefault();
                    var wrkattch = dbContext.tbl_DC_QuestionBank_Attachment.Where(a => a.QuestionBank_Id == Worksheet_ID && a.Is_Active == true && a.Is_Deleted == false).FirstOrDefault();
                    var chapter = dbContext.tbl_DC_Chapter.Where(a => a.Chapter_Id == wrk.Chapter_Id).FirstOrDefault();
                    var subject = dbContext.tbl_DC_Subject.Where(a => a.Subject_Id == wrk.Subject_Id).FirstOrDefault();


                    if (Is_student)
                    {
                        var reg = dbContext.tbl_DC_Registration.Where(x => x.Regd_ID == Posted_By_ID).FirstOrDefault();

                        var tea = dbContext.tbl_DC_AssignSubjectToTeacher.Where(A => A.Subject_Id == wrk.Subject_Id && A.SectionId == reg.SectionId && A.Is_Active == true).FirstOrDefault();
                        var pushnot = (from c in dbContext.Teachers.Where(x => x.TeacherId == tea.Teacher_ID)

                                       select new { c.TeacherId, c.fcmId }).FirstOrDefault();
                        string body = "QBANK#{{ID}}#{{marks}}#{{pdf}}#{{name}} has replied for {{chapter}} questionbank of {{subject}}";
                        string msg = body.ToString().Replace("{{chapter}}", chapter.Chapter.ToString())
                            .Replace("{{subject}}", subject.Subject + "").Replace("{{ID}}", Worksheet_ID + "").Replace("{{marks}}", wrk.Total_Mark + "").Replace("{{pdf}}",
                            "http://learn.odmps.org/Images/Questionbank/" + wrkattch.Path + "").Replace("{{name}}", reg.Customer_Name + "");

                        if (pushnot != null)
                        {
                            if (pushnot.fcmId != null)
                            {
                                var note = new PushNotiStatus("Questionbank Review", msg, pushnot.fcmId);
                            }
                        }
                    }
                    else
                    {

                        var reg = dbContext.tbl_DC_Registration.Where(x => x.Regd_ID == wrk.Regd_ID).FirstOrDefault();

                        // var tea = dbContext.tbl_DC_AssignSubjectToTeacher.Where(A => A.Subject_Id == wrk.Subject_Id && A.SectionId == reg.SectionId && A.Is_Active == true).FirstOrDefault();
                        var pushnot = (from c in dbContext.tbl_DC_Registration.Where(x => x.Regd_ID == wrk.Regd_ID)

                                       select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                        string body = "QBANK#{{ID}}#{{marks}}#{{pdf}}#{{name}} has replied for {{chapter}} questionbank of {{subject}}";
                        string msg = body.ToString().Replace("{{chapter}}", chapter.Chapter.ToString())
                            .Replace("{{subject}}", subject.Subject + "").Replace("{{ID}}", Worksheet_ID + "").Replace("{{marks}}", wrk.Total_Mark + "").Replace("{{pdf}}",
                            "http://learn.odmps.org/Images/Questionbank/" + wrkattch.File_Name + "").Replace("{{name}}", reg.Customer_Name + "");

                        if (pushnot != null)
                        {
                            if (pushnot.Device_id != null)
                            {
                                var note = new PushNotiStatus("Questionbank Review", msg, pushnot.Device_id);
                            }
                        }


                        //var pushnot = (from c in dbContext.tbl_DC_Registration.Where(x => x.Regd_ID == wrk.Regd_ID)

                        //               select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                        //string body = "Worksheet score#{{ID}}#Your student has been replied for {{chapter}} worksheet of {{subject}}";
                        //string msg = body.ToString().Replace("{{chapter}}", chapter.Chapter.ToString())
                        //    .Replace("{{subject}}", subject.Subject + "").Replace("{{ID}}", Worksheet_ID + "");

                        //if (pushnot != null)
                        //{
                        //    if (pushnot.Device_id != null)
                        //    {
                        //        var note = new PushNotiStatus(msg, pushnot.Device_id);
                        //    }
                        //}
                    }
                }
                catch (Exception)
                {


                }

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }

            catch (Exception e1)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
    }
}
