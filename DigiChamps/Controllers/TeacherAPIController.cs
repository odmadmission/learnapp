﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace DigiChamps.Controllers
{
    public class TeacherAPIController : ApiController
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();

        [HttpGet]
        public HttpResponseMessage TeacherProfile(long id)
        {
            var teacherDetails = DbContext.Teachers.Where(a => a.TeacherId == id).Select(a => new { a.TeacherId, a.Name, a.Mobile, a.Email }).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, new { teacherList = teacherDetails});
        }

        [HttpGet]
        public HttpResponseMessage AssignedSubjectListByTeacherId(long teacherId)
        {
            var teacherList = DbContext.Teachers.ToList();
            var assignedSubjectList = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Teacher_ID == teacherId).ToList();
            var boardList = DbContext.tbl_DC_Board.ToList();
            var classList = DbContext.tbl_DC_Class.ToList();
            var subjectList = DbContext.tbl_DC_Subject.ToList();
            var sectionList = DbContext.tbl_DC_Class_Section.ToList(); 
            var schoolList = DbContext.tbl_DC_School_Info.ToList();
            var res = (from a in assignedSubjectList
                       join b in teacherList on a.Teacher_ID equals b.TeacherId
                       join c in boardList on a.Board_Id equals c.Board_Id
                       join d in classList on a.Class_Id equals d.Class_Id
                       join e in sectionList on a.SectionId equals e.SectionId
                       join f in subjectList on a.Subject_Id equals f.Subject_Id
                       join g in schoolList on e.School_Id equals g.SchoolId
                       select new AssignedSubjectListByTeacherId
                       {
                           ID = a.AssignSubjectToTeacher_Id,
                           TeacherId = Convert.ToInt64(a.Teacher_ID),
                           TeacherName = b.Name,
                           BoardId = a.Board_Id,
                           BoardName = c.Board_Name,
                           ClassId = a.Class_Id,
                           ClassName = d.Class_Name,
                           SectionId = a.SectionId,
                           SectionName = e.SectionName,
                           SubjectId = a.Subject_Id,
                           SubjectName = f.Subject,
                           SchoolId = e.School_Id,
                           SchoolName = g.SchoolName
                       }).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, new { subjectList = res });
        }

        [HttpGet]
        public HttpResponseMessage GetStudentList(long schoolId,long boardId,long classId, long sectionId)
        {
            var boardList = DbContext.tbl_DC_Board.ToList();
            var classList = DbContext.tbl_DC_Class.ToList();
            var schoolList = DbContext.tbl_DC_School_Info.ToList();
            var classSection = DbContext.tbl_DC_Class_Section.ToList();
            var regdList = DbContext.tbl_DC_Registration.ToList();

            var res = (from a in regdList
                       join b in classSection on a.SectionId equals b.SectionId
                       join c in classList on b.Class_Id equals c.Class_Id
                       join d in boardList on c.Board_Id equals d.Board_Id
                       join e in schoolList on a.SchoolId equals e.SchoolId
                       select new StudentModel
                       {
                           ID = a.Regd_ID,
                           StudentName = a.Customer_Name,
                           Mobile = a.Mobile,
                           SchoolId = Convert.ToInt64(a.SchoolId),
                           SchoolName = e.SchoolName,
                           BoardId = c.Board_Id,
                           BoardName = d.Board_Name,
                           ClassId = Convert.ToInt64(b.Class_Id),
                           ClassName = c.Class_Name,
                           SectionId = a.SectionId,
                           SectionName = b.SectionName
                       }).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, new { studentList = res });
        }
    }
    
}
