﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DigiChamps.Controllers
{
    public class SchoolAdminController : Controller
    {
        // GET: /SchoolAdmin/
        [HttpGet]
        public ActionResult Login()
        {
            if (Session["USER_CODE"] != null)
            {
                if (Session["ROLE_CODE"].ToString() == "A" || Session["ROLE_CODE"].ToString() == "S")
                {
                    return RedirectToAction("SchoolDashBoard");
                }
                else
                {
                    return RedirectToAction("Logout");
                }
            }
            else
            {
                return View();
            }
        }
       
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }
        public ActionResult Logout()
        {
            Session["USER_CODE"] = null;
            Session["ROLE_CODE"] = null;
            return RedirectToAction("Login");
        }
    }
}
