﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Globalization;

namespace DigiChamps.Controllers
{
    public class OnlineExamAPIController : ApiController
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();

        [HttpGet]
        public HttpResponseMessage GetRevisionMaterialList(int subjectid,int chapterid)
        {
            try
            {
                List<RevisionMaterial> revisionMaterial = null;
                if (subjectid != 0 && chapterid != 0)
                {
                    revisionMaterial = DbContext.RevisionMaterials.Where(a => a.SubjectId == subjectid && a.ChapterId == chapterid && a.Active == true).ToList();


                    var revisionPdf = DbContext.RevisionMaterialPdfs.Where(a => a.Active == true).ToList();
                    var subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                    var chapterList = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();

                    var res = (from a in revisionMaterial
                               join b in revisionPdf on a.ID equals b.RevisionMaterialId
                               join c in subjectList on a.SubjectId equals c.Subject_Id
                               join d in chapterList on a.ChapterId equals d.Chapter_Id
                               select new RevisionMaterialModel
                               {
                                   id = a.ID,
                                   subjectId = a.SubjectId,
                                   subjectName = c.Subject,
                                   chapterId = d.Chapter_Id,
                                   chapterName = d.Chapter,
                                   InsertedDate = a.InsertedDate,
                                   Pdf = "/Images/RevisionPdf/" + b.PdfUrl
                               }).ToList();

                    return Request.CreateResponse(HttpStatusCode.OK, new { revisonList = res });
                }
                else
                {
                    List<RevisionMaterialModel> ob = new List<RevisionMaterialModel>();
                    return Request.CreateResponse(HttpStatusCode.OK, new { revisonList = ob });

                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
            }
            
        }
        [HttpGet]
        public HttpResponseMessage GetRevisionMaterialList123(int subjectid, int chapterid, int classid)
        {
            try
            {
                List<RevisionMaterial> revisionMaterial = null;
                if (subjectid != 0 && chapterid != 0)
                {
                    revisionMaterial = DbContext.RevisionMaterials.Where(a => a.SubjectId == subjectid && a.ChapterId == chapterid && a.ClassId == classid && a.Active == true).ToList();
                }
                else
                {
                    revisionMaterial = DbContext.RevisionMaterials.Where(a => a.ClassId == classid && a.Active == true).ToList();
                }
                var revisionPdf = DbContext.RevisionMaterialPdfs.Where(a => a.Active == true).ToList();
                var subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var chapterList = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();

                var res = (from a in revisionMaterial
                           join b in revisionPdf on a.ID equals b.RevisionMaterialId
                           join c in subjectList on a.SubjectId equals c.Subject_Id
                           join d in chapterList on a.ChapterId equals d.Chapter_Id
                           select new RevisionMaterialModel
                           {
                               id = a.ID,
                               subjectId = a.SubjectId,
                               subjectName = c.Subject,
                               chapterId = d.Chapter_Id,
                               chapterName = d.Chapter,
                               InsertedDate = a.InsertedDate,
                               Pdf = "/Images/RevisionPdf/" + b.PdfUrl
                           }).ToList();

                return Request.CreateResponse(HttpStatusCode.OK, new { revisonList = res });
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
            }

        }

        public class RevisionMaterialModel
        {
            public long id { get; set; }
            public int? subjectId { get; set; }
            public string subjectName { get; set; }
            public int? chapterId { get; set; }
            public string chapterName { get; set; }
            public DateTime? InsertedDate { get; set; }
            public string Pdf { get; set; }
        }

        [HttpGet]
        public HttpResponseMessage GetOnlineExamType()
        {
            try
            {
                var typeList = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).Select(a => new { a.OnlineExamTypeId, a.OnlineExamTypeName }).ToList();

                return Request.CreateResponse(HttpStatusCode.OK, new { TypeList = typeList });
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
            }
        }

                               
        [HttpGet]
        public HttpResponseMessage GetOnlineExamList(int studentId, long? examTypeId, int? subjectId)
        {
            try
            {
                var student = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == studentId).FirstOrDefault();
                var student_dtl = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Regd_ID == studentId).FirstOrDefault();
                OnlineExamResponseModel model = new OnlineExamResponseModel();

                var onlineExamList = DbContext.OnlineExams.Where(a => a.ClassId == student_dtl.Class_ID && a.SchoolId == student.SchoolId && a.Is_Active == true).ToList();
                var subjectList = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var examTypeList = DbContext.OnlineExamTypes.Where(a => a.Is_Active == true).ToList();

                var classname = DbContext.tbl_DC_Class.Where(a => a.Class_Id == student_dtl.Class_ID).FirstOrDefault().Class_Name;
                var sectionname = DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == student.SectionId).FirstOrDefault().SectionName;
                var onlineExamDate = DbContext.OnlineExamDates.Where(a => a.Is_Active == true).ToList();
                var remarkList = DbContext.OnlineExamAnswerSheetRemarks.Where(a => a.Is_Active == true).ToList();
                var examTime = DbContext.OnlineExamTimes.Where(a => a.Is_Active == true).ToList();
                var answerSheet = DbContext.OnlineExamAnswerSheets.Where(a => a.Is_Active == true).ToList();
                var teacherList = DbContext.Teachers.Where(a => a.Active == 1).ToList();

                var res1 = (from a in onlineExamList
                            join b in subjectList on a.SubjectId equals b.Subject_Id
                            join c in examTypeList on a.OnlineExamTypeId equals c.OnlineExamTypeId
                            join f in onlineExamDate on a.OnlineExamDateId equals f.OnlineExamDateId
                            join g in examTime on a.OnlineExamTimeId equals g.OnlineExamTimeId
                            select new OnlineExamAPIModel
                            {
                                ID = a.OnlineExamId,
                                SubjectName = b.Subject,

                                ExampTypeId = a.OnlineExamTypeId.Value,
                                SubjectId = a.SubjectId.Value,
                                ExampTypeName = c.OnlineExamTypeName,
                                ClassName = classname,
                                SectionName = sectionname,
                                OnlineExamDates = f.OnlineExamDate1,
                                StartTime = g.Start_Time,
                                StartTimeName = g.Start_Time_Name,
                                EndTime = g.End_Time,
                                EndTimeName = g.End_Time_Name,
                                Instruction = a.Exam_Instruction,
                                Pdf = @"/Images/OnlineExam/" + a.PDF,
                                Category="Subjective",
                                IsObjective=false
                            }).ToList();

                var objectiveExamList = DbContext.OnlineObjectiveExams.Where(a => a.ClassId == student_dtl.Class_ID && a.SchoolId == student.SchoolId && a.Is_Active == true).ToList();
                var res2 = (from a in objectiveExamList
                            join b in subjectList on a.SubjectId equals b.Subject_Id
                            join c in examTypeList on a.OnlineExamTypeId equals c.OnlineExamTypeId
                            join f in onlineExamDate on a.OnlineExamDateId equals f.OnlineExamDateId
                            join g in examTime on a.OnlineExamTimeId equals g.OnlineExamTimeId
                            select new OnlineExamAPIModel
                            {
                                ID = a.OnlineObjectiveExamId,
                                SubjectName = b.Subject,
                                ExampTypeId = a.OnlineExamTypeId.Value,
                                SubjectId = a.SubjectId.Value,
                                ExampTypeName = c.OnlineExamTypeName,
                                ClassName = classname,
                                SectionName = sectionname,
                                OnlineExamDates = f.OnlineExamDate1,
                                StartTime = g.Start_Time,
                                StartTimeName = g.Start_Time_Name,
                                EndTime = g.End_Time,
                                EndTimeName = g.End_Time_Name,
                                Instruction = a.Exam_Instruction,
                                Category = "Objective",
                                IsObjective = true
                                // Pdf = @"/Images/OnlineExam/" + a.PDF
                            }).ToList();

















                if (examTypeId != null && examTypeId != 0)
                {
                    res1 = res1.Where(a => a.ExampTypeId == examTypeId).ToList();
                    res2= res2.Where(a => a.ExampTypeId == examTypeId).ToList();
                }
                if (subjectId != null && subjectId != 0)
                {
                    res1 = res1.Where(a => a.SubjectId == subjectId).ToList();
                    res2 = res2.Where(a => a.SubjectId == subjectId).ToList();
                }

                var objectiveExamStudents = DbContext.OnlineObjectiveExamStudents.Where(a => a.Is_Active == true).ToList();
                #region upcoming list
                var onlineexamanswer = answerSheet.Where(a => a.StudentId == studentId).Select(a => a.OnlineExamId).ToList();
                var objectiveexamanswer = objectiveExamStudents.Where(a => a.StudentId == studentId).Select(a => a.OnlineObjectiveExamId).Distinct().ToList();

                var upcoming1 = res1.Where(a => Getdate(a.OnlineExamDates.Value.Date.ToString("MM/dd/yyyy") + " " + DateTime.ParseExact(a.EndTime.Value.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt")) >= DateTime.Now && !onlineexamanswer.Contains(a.ID)).ToList();
                var upcoming2= res2.Where(a => Getdate(a.OnlineExamDates.Value.Date.ToString("MM/dd/yyyy") + " " + DateTime.ParseExact(a.EndTime.Value.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt")) >= DateTime.Now && !objectiveexamanswer.Contains(a.ID)).ToList();
                var upcoming = upcoming1.Union(upcoming2);
                model.UpcommingList = upcoming.OrderBy(a => (Getdate(a.OnlineExamDates.Value.Date.ToString("MM/dd/yyyy") + " " + DateTime.ParseExact(a.StartTime.Value.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt")))).ToList();
                #endregion
                #region Given Exam list
                var onlineexamanswer1 = answerSheet.Where(a => a.StudentId == studentId).ToList();
                var given1 = (from a in res1
                             join b in onlineexamanswer1 on a.ID equals b.OnlineExamId
                             orderby b.InsertedOn descending
                             where b.IsSubmitted == true && b.IsVerified == false
                             select a).ToList();

                var given2 = res2.Where(a => Getdate(a.OnlineExamDates.Value.Date.ToString("MM/dd/yyyy") + " " + DateTime.ParseExact(a.EndTime.Value.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt")) >= DateTime.Now && objectiveexamanswer.Contains(a.ID)).ToList().OrderByDescending(a=>a.OnlineExamDates);
                var given = given1.Union(given2);
                model.GivenExamList = given.OrderBy(a => (Getdate(a.OnlineExamDates.Value.Date.ToString("MM/dd/yyyy") + " " + DateTime.ParseExact(a.StartTime.Value.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt")))).ToList();




                #endregion
                #region Result list

                var result1 = (from a in res1
                               join b in onlineexamanswer1 on a.ID equals b.OnlineExamId
                               where b.IsSubmitted == true && b.IsVerified == true
                               select new OnlineExamAPIModel
                               {
                                   ID = a.ID,
                                   SubjectName = a.SubjectName,
                                   ExampTypeName = a.ExampTypeName,
                                   ClassName = a.ClassName,
                                   SectionName = a.SectionName,
                                   OnlineExamDates = a.OnlineExamDates,
                                   StartTime = a.StartTime,
                                   StartTimeName = a.StartTimeName,
                                   EndTime = a.EndTime,
                                   EndTimeName = a.EndTimeName,
                                   RemarkId = b.OnlineExamAnswerSheetId,
                                   Score = remarkList.Where(d => d.OnlineExamAnswerSheetId == b.OnlineExamAnswerSheetId && d.Is_Active == true).ToList().Sum(d => d.GivenMarks).Value,
                                   VarifiedOn = b.VerifiedOn,
                                   VerifiedName = teacherList.Where(d => d.TeacherId == b.VerifiedTeacherId).FirstOrDefault().Name,
                                   IsVerified = b.IsVerified.Value,
                                   IsSubmitted = b.IsSubmitted,
                                   Category = a.Category,
                                   IsObjective = a.IsObjective
                               }).ToList();
                var studentanswers = objectiveExamStudents.Where(j => j.StudentId == studentId).ToList();
                var resultes = res2.Where(a => Getdate(a.OnlineExamDates.Value.Date.ToString("MM/dd/yyyy") + " " + DateTime.ParseExact(a.EndTime.Value.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt")) <= DateTime.Now).ToList();

                var result2 = (from a in resultes
                               join b in objectiveexamanswer on a.ID equals b.Value
                               select new OnlineExamAPIModel
                               {
                                   ID = a.ID,
                                   SubjectName = a.SubjectName,
                                   ExampTypeName = a.ExampTypeName,
                                   ClassName = a.ClassName,
                                   SectionName = a.SectionName,
                                   OnlineExamDates = a.OnlineExamDates,
                                   StartTime = a.StartTime,
                                   StartTimeName = a.StartTimeName,
                                   EndTime = a.EndTime,
                                   EndTimeName = a.EndTimeName,
                                   TotalScore= GetTotalScore(a.ID).Value,
                                   Score = Getscore(studentanswers, objectiveExamList.Where(c=>c.OnlineObjectiveExamId==a.ID).FirstOrDefault()).Value,
                                   VarifiedOn = objectiveExamStudents.Where(m=>m.OnlineObjectiveExamId==a.ID).Select(m=>m.InsertedOn).FirstOrDefault(),
                                   IsVerified = true,
                                   IsSubmitted = true,
                                   Category = a.Category,
                                   IsObjective = a.IsObjective
                               }).ToList();


                model.Result = result1.Union(result2).OrderBy(a => (Getdate(a.OnlineExamDates.Value.Date.ToString("MM/dd/yyyy") + " " + DateTime.ParseExact(a.StartTime.Value.ToString().Substring(0, 5), "HH:mm", CultureInfo.CurrentCulture).ToString("hh:mm tt")))).ToList();

                #endregion


                return Request.CreateResponse(HttpStatusCode.OK, new { Response = model });
            }
            catch (Exception e1)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
            }
        }
        public decimal? GetTotalScore(long OnlineObjectiveExamId)
        {
            var examques = DbContext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true && a.OnlineObjectiveExamId == OnlineObjectiveExamId).ToList();
            var Totalmark = examques.Sum(a => a.Mark);
            return Totalmark;
        }
        public decimal? Getscore(List<OnlineObjectiveExamStudent> examStudents, OnlineObjectiveExam exam)
        {
           // var answers = DbContext.OnlineObjectiveExamAnswers.Where(a => a.Is_Active == true).ToList();
            var examques = DbContext.OnlineObjectiveExamQuestions.Where(a => a.Is_Active == true && a.OnlineObjectiveExamId == exam.OnlineObjectiveExamId).ToList();
            var Totalmark = examques.Sum(a => a.Mark);

            var res = (from a in examStudents
                       join b in examques on a.OnlineObjectiveExamQuestionId equals b.OnlineObjectiveExamQuestionId
                       where a.IsAttempt==true
                       select new
                       {
                           iscorrect = a.IsCorrect,
                           score = b.Mark
                       }).ToList();
            var correct = res.Where(a => a.iscorrect == true).ToList().Sum(a => a.score);
            var wrong = res.Where(a => a.iscorrect == false).ToList().Count();
            if (exam.IsNagativeMarking == true)
            {
                var negativelist = DbContext.Negativemarks.Where(a => a.NegativeMarkID == exam.NagativeMarkingId).FirstOrDefault();
                var negvmark = negativelist.NegativeMark1 * wrong;
                var scoreres = correct - negvmark;
                return scoreres;
            }
            else
            {
                return correct;
            }
        }
        public DateTime Getdate(string date)
        {
            DateTime dt = Convert.ToDateTime(date);
            return dt;
        }

        //[HttpGet]
        //public HttpResponseMessage GetOnlineExaminstruction(int onlineExamId)
        //{
        //    try
        //    {
        //        var res = DbContext.OnlineExams.Where(a => a.Is_Active == true && a.OnlineExamId == onlineExamId).Select(a => new ExamInstructionModel
        //        {
        //            id = a.OnlineExamId,
        //            instruction = a.Exam_Instruction,
        //            pdf = @"~/Images/OnlineExam/" + a.PDF
        //        }).FirstOrDefault();

        //        return Request.CreateResponse(HttpStatusCode.OK, new { OnlineExamInstruction = res });
        //    }
        //    catch
        //    {
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
        //    }
        //}

        [HttpPost]
        public HttpResponseMessage SubmitWorkSheet()
        {
            try
            {
                var httprequest = HttpContext.Current.Request;
                long onlineExamId = Convert.ToInt64(httprequest.Form["onlineExamId"]);
                int studentId = Convert.ToInt32(httprequest.Form["studentId"]);
                Guid guid = Guid.NewGuid();
                OnlineExamAnswerSheet sheet = new OnlineExamAnswerSheet();
                sheet.OnlineExamId = onlineExamId;
                sheet.StudentId = studentId;
                sheet.AppearedOn = DateTime.Now.Date;
                sheet.Type = "Mobile";
                sheet.SubmittedOn = DateTime.Now;
                sheet.Status = "Created";
                sheet.IsAppeared = true;
                sheet.IsSubmitted = true;
                sheet.IsVerified = false;
                sheet.Is_Active = true;
                sheet.InsertedId = studentId;
                sheet.InsertedOn = DateTime.Now;
                sheet.ModifiedId = studentId;
                sheet.ModifiedOn = DateTime.Now;

                if (httprequest.Files.Count > 0)
                {
                    var postedfile = httprequest.Files[0];
                    var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/OnlineExamAnswer/"), (guid + postedfile.FileName));
                    postedfile.SaveAs(path);
                    //docfile.Add(path);
                    sheet.AnswerPDF = guid + postedfile.FileName;

                    //string guid = Guid.NewGuid().ToString();
                    //var fileName = Path.GetFileName(WorkSheetPdf.FileName.Replace(WorkSheetPdf.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                    //var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/OnlineExam/"), fileName);
                    //WorkSheetPdf.SaveAs(path);
                    //sheet.AnswerPDF = fileName.ToString();
                    DbContext.OnlineExamAnswerSheets.Add(sheet);
                    DbContext.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
                }

            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
            }
        }

        [HttpGet]
        public HttpResponseMessage GetRemarksAnswarAndQuestionPaperExamDetails(long onlineExamId, int studentId)
        {
            try
            {
                var onlineExamList = DbContext.OnlineExams.Where(a => a.OnlineExamId == onlineExamId).FirstOrDefault();
                var answerSheet = DbContext.OnlineExamAnswerSheets.Where(a => a.Is_Active == true && a.OnlineExamId == onlineExamId && a.StudentId == studentId).FirstOrDefault();
                var remarkList = DbContext.OnlineExamAnswerSheetRemarks.Where(a => a.Is_Active == true && a.OnlineExamAnswerSheetId == answerSheet.OnlineExamAnswerSheetId).ToList();

                RemarksModel remarks = new RemarksModel();
                remarks.totalScore = remarkList.Sum(a => a.GivenMarks).ToString();
                remarks.sheetID = answerSheet.OnlineExamAnswerSheetId;
                remarks.questionPdf = onlineExamList.PDF;
                remarks.answarPdf = answerSheet.AnswerPDF;
                List<ScoreModel> modellists = new List<ScoreModel>();

                foreach (var list in remarkList)
                {
                    ScoreModel models = new ScoreModel();
                    models.questionNo = list.QuestionNo;
                    models.marks = list.GivenMarks.ToString();
                    models.remarks = list.Remarks;
                    modellists.Add(models);
                }

                remarks.scoreDetails = modellists;

                return Request.CreateResponse(HttpStatusCode.OK, new { AllDetails = remarks, msg = "success" });
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
            }
        }
        [HttpDelete]
        public HttpResponseMessage Removeanswersheet(long onlineExamId, int studentId)
        {
            try
            {
                OnlineExamAnswerSheet answerSheet = DbContext.OnlineExamAnswerSheets.Where(a => a.Is_Active == true && a.OnlineExamId == onlineExamId && a.StudentId == studentId).FirstOrDefault();
                answerSheet.Is_Active = false;
                answerSheet.ModifiedOn = DateTime.Now;
                DbContext.SaveChanges();


                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
            }
        }
        [HttpGet]
        public HttpResponseMessage Getanswersheetdetails(long onlineExamId, int studentId)
        {
            try
            {
                var answerSheet = DbContext.VW_All_OnlineExam_Attachment.Where(a => a.Is_Active == true && a.OnlineExamId == onlineExamId && a.StudentId == studentId).FirstOrDefault();
                answersheetModel answer = new answersheetModel();
                if (answerSheet != null)
                {
                    answer.answersheetId = answerSheet.OnlineExamAnswerSheetId;
                    answer.className = answerSheet.Class_Name;
                    answer.dateOfExam = answerSheet.OnlineExamDate.Value.ToString("dd-MM-yyyy");
                    answer.examType = answerSheet.OnlineExamTypeName;
                    answer.score = "NA";
                    answer.section = answerSheet.SectionName;
                    answer.subject = answerSheet.Subject;
                    answer.status = "SUBMITTED";
                    answer.verifiedBy = "";
                    answer.VerifiedDateTime = "";
                }
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success", sheet = answer });
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "Error" });
            }
        }
    }
}
